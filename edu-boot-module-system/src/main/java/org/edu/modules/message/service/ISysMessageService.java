package org.edu.modules.message.service;

import org.edu.modules.message.entity.SysMessage;
import org.edu.common.system.base.service.BaseService;

/**
 * @Description: 消息
 * @Author: jeecg-boot
 * @Date:  2019-04-09
 * @Version: V1.0
 */
public interface ISysMessageService extends BaseService<SysMessage> {

}
