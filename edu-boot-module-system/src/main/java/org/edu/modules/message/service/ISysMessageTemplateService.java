package org.edu.modules.message.service;

import java.util.List;

import org.edu.modules.message.entity.SysMessageTemplate;
import org.edu.common.system.base.service.BaseService;

/**
 * @Description: 消息模板
 * @Author: jeecg-boot
 * @Date:  2019-04-09
 * @Version: V1.0
 */
public interface ISysMessageTemplateService extends BaseService<SysMessageTemplate> {
    List<SysMessageTemplate> selectByCode(String code);
}
