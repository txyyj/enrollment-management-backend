package org.edu.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.system.entity.AppManage;

/**
 * @Author lbh
 * @Date 2022/1/13 11:54
 * @Version 1.0
 */
public interface AppManageMapper extends BaseMapper<AppManage> {
}
