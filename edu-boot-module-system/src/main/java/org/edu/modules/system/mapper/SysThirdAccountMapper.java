package org.edu.modules.system.mapper;

import org.edu.modules.system.entity.SysThirdAccount;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 第三方登录账号表
 * @Author: jeecg-boot
 * @Date:   2020-11-17
 * @Version: V1.0
 */
public interface SysThirdAccountMapper extends BaseMapper<SysThirdAccount> {

}
