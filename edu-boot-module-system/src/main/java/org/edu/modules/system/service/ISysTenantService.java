package org.edu.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.system.entity.SysTenant;

public interface ISysTenantService extends IService<SysTenant> {

}
