package org.edu.modules.system.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.system.entity.AppManage;

/**
 * 应用信息表
 * @Author lbh
 * @Date 2022/1/13 11:56
 * @Version 1.0
 */
public interface IAppManageService extends IService<AppManage> {
}
