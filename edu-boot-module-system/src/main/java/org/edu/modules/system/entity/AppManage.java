package org.edu.modules.system.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

/**
 * 应用信息表对象
 * @Author lbh
 * @Date 2022/1/13 11:48
 * @Version 1.0
 */
@Data
@TableName(value = "ve_base_app_manage" ,schema = "edu_dev")
public class AppManage {
    /**
     * id
     */
    @TableId(type = IdType.ASSIGN_ID)
    private String id;
    /**
     * 应用名称
     */
    private String appName;
    /**
     * 状态|0:启用,1:禁用
     */
    private String isEnable;
    /**
     * 描述
     */
    private String description;
    /**
     * 应用地址
     */
    private String appAddress;
    /**
     * 秘钥
     */
    private String auth;
    /**
     * 认证方式：1-cas，2-oauth2，3-ldap
     */
    private String authmethod;


}
