package org.edu.modules.enroll;

import lombok.extern.slf4j.Slf4j;
import org.edu.common.util.oConvertUtils;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * @Author:leidq
 * @Date: 2021/02/28 15:35
 */
@Slf4j
@EnableScheduling
@SpringBootApplication(scanBasePackages = "org.edu",exclude = {DataSourceAutoConfiguration.class})
@EnableFeignClients(basePackages = {"org.edu"})
@MapperScan("org.edu.modules.enroll.mapper")
public class EduEnrollApplication {
    public static void main(String[] args) {
        ConfigurableApplicationContext application = SpringApplication.run(EduEnrollApplication.class,args);


        Environment env = application.getEnvironment();
        String port = env.getProperty("server.port");
        String path = oConvertUtils.getString(env.getProperty("server.servlet.context-path"));
        log.info("\n----------------------------------------------------------\n\t" +
                "Application edu-Boot is running! Access URLs:\n\t" +
                "Local: \t\thttp://localhost:" + port + path + "/doc.html\n" +
                "----------------------------------------------------------");
    }
}
