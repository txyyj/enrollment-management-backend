package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeDormStudent;
import org.edu.modules.enroll.mapper.VeDormStudentMapper;
import org.edu.modules.enroll.service.VeDormStudentService;
import org.springframework.stereotype.Service;

@Service
public class VeDormStudentServiceImpl extends ServiceImpl<VeDormStudentMapper, VeDormStudent> implements VeDormStudentService {
}
