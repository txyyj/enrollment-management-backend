package org.edu.modules.enroll.service.impl;

import org.edu.modules.enroll.entity.VeZsAdmissionInformation;
import org.edu.modules.enroll.mapper.VeZsAdmissionInformationMapper;
import org.edu.modules.enroll.service.VeZsAdmissionInformationService;
import org.edu.modules.enroll.vo.AdmissionTicketInfoVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/7/27 20:20
 */
@Service
public class VeZsAdmissionInformationServiceImpl implements VeZsAdmissionInformationService {
    @Resource
    private VeZsAdmissionInformationMapper veZsAdmissionInformationMapper;


    @Override
    public AdmissionTicketInfoVo selectByXmKshSfzh(String XM, String KSH, String SFZH) {
        return veZsAdmissionInformationMapper.selectByXmKshSfzh(XM, KSH, SFZH);
    }

    @Override
    public VeZsAdmissionInformation applyMsg(String XM, String KSH, String SFZH) {
        return veZsAdmissionInformationMapper.applyMsg(XM, KSH, SFZH);
    }
}
