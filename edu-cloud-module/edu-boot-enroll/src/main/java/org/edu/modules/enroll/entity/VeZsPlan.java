package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:  招生计划vo
 * @Author:  zh
 * @Date:  2021-04-22
 * @Version:  V1.0
 */
@Data
@TableName("ve_zs_plan")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsPlan implements Serializable {


        /** id */
        @ApiModelProperty(value = "id")
        private Integer id;

        /** 所属院系ID */
        @Excel(name = "所属院系ID", width = 15)
        @ApiModelProperty(value = "所属院系ID")
        @TableField(value = "falId")
        private Integer falId;

        /** 报名专业ID */
        @Excel(name = "报名专业ID", width = 15)
        @ApiModelProperty(value = "报名专业ID")
        @TableField(value = "specId")
        private Integer specId;

        /** 学制ID */
        @ApiModelProperty(value = "学制ID")
        @TableField(value = "XZ")
        private Integer XZ;

        /** 招生年份 */
        @Excel(name = "招生年份", width = 15)
        @ApiModelProperty(value = "招生年份")
        @TableField(value = "ZSNF")
        private String ZSNF;

        /** 招生季 */
        @ApiModelProperty(value = "招生季")
        @TableField(value = "ZSJ")
        private Integer ZSJ;

        /** 班级数 */
        @Excel(name = "班级数", width = 15)
        @ApiModelProperty(value = "班级数")
        @TableField(value = "BJS")
        private Integer BJS;

        /** 男生人数 */
        @Excel(name = "男生人数", width = 15)
        @ApiModelProperty(value = "男生人数")
        @TableField(value = "NANSRS")
        private Integer NANSRS;

        /** 女生人数 */
        @ApiModelProperty(value = "女生人数")
        @TableField(value = "NVSRS")
        private Integer NVSRS;

        /** 总人数 */
        @ApiModelProperty(value = "总人数")
        @TableField(value = "ZRS")
        private Integer ZRS;

        /** 专业对考生的要求 */
        @ApiModelProperty(value = "专业对考生的要求")
        @TableField(value = "ZYYQ")
        private String ZYYQ;

        /** 培养目标 */
        @ApiModelProperty(value = "培养目标")
        @TableField(value = "PYMB")
        private String PYMB;

        /** 主要专业课程 */
        @Excel(name = "主要专业课程", width = 15)
        @ApiModelProperty(value = "主要专业课程")
        @TableField(value = "ZYZYKC")
        private String ZYZYKC;

        /** 办学条件 */
        @Excel(name = "办学条件", width = 15)
        @ApiModelProperty(value = "办学条件")
        @TableField(value = "BXTJ")
        private String BXTJ;

        /** 附件Id */
        @Excel(name = "附件Id", width = 15)
        @ApiModelProperty(value = "附件Id")
        @TableField(value = "fileId")
        private Integer fileId;

        /** 毕业后去向及备注 */
        @Excel(name = "毕业后去向及备注", width = 15)
        @ApiModelProperty(value = "毕业后去向及备注")
        @TableField(value = "remark")
        private String remark;

        /** 创建时间 */
        @Excel(name = "创建时间", width = 15)
        @ApiModelProperty(value = "创建时间")
        @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
        @TableField(value = "create_time")
        private Date create_time;

        /** 发布用户ID */
        @Excel(name = "发布用户ID", width = 15)
        @ApiModelProperty(value = "发布用户ID")
        @TableField(value = "create_by")
        private String create_by;

        /** 更新时间 */
        @Excel(name = "更新时间", width = 15)
        @ApiModelProperty(value = "更新时间")
        @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
        @TableField(value = "update_time")
        private Date update_time;

        /** 修改人id */
        @Excel(name = "修改人id", width = 15)
        @ApiModelProperty(value = "修改人id")
        @TableField(value = "update_by")
        private String update_by;

        /** 终端ID */
        @Excel(name = "终端ID", width = 15)
        @ApiModelProperty(value = "终端ID")
        @TableField(value = "terminalId")
        private Integer terminalId;

        /** 逻辑删除的标识 */
        @Excel(name = "逻辑删除", width = 15)
        @ApiModelProperty(value = "逻辑删除（0=否，1=是）")
        @TableField(value = "is_deleted")
        @TableLogic
        private Integer is_deleted;
    }

