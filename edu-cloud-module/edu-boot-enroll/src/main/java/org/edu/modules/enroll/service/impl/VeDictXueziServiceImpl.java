package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeDictXuezi;
import org.edu.modules.enroll.mapper.VeDictXueziMapper;
import org.edu.modules.enroll.service.VeDictXueziService;
import org.springframework.stereotype.Service;

@Service
public class VeDictXueziServiceImpl extends ServiceImpl<VeDictXueziMapper, VeDictXuezi> implements VeDictXueziService {
}
