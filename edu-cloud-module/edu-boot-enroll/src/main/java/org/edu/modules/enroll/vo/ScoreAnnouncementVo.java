package org.edu.modules.enroll.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/7/24 10:28
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ScoreAnnouncementVo implements Serializable {
    @Excel(name = "排名" ,width = 30)
    @ApiModelProperty(value = "排名")
    private int PM;

    @Excel(name = "姓名" ,width = 30)
    @ApiModelProperty(value = "姓名")
    private String XM;

    @Excel(name = "准考证号" ,width = 30)
    @ApiModelProperty(value = "准考证号")
    private String ZKZH;

    @Excel(name = "学年" ,width = 30)
    @ApiModelProperty(value = "学年")
    private int XN;

    @Excel(name = "学期" ,width = 30)
    @ApiModelProperty(value = "学期")
    private String XQ;

    @Excel(name = "院系" ,width = 30)
    @ApiModelProperty(value = "院系")
    private String faculty;

    @Excel(name = "专业" ,width = 30)
    @ApiModelProperty(value = "专业")
    private String formalmajor;

    @Excel(name = "科目一" ,width = 30)
    @ApiModelProperty(value = "科目一")
    private Long KM1;

    @Excel(name = "科目二" ,width = 30)
    @ApiModelProperty(value = "科目二")
    private Long KM2;

    @Excel(name = "科目三" ,width = 30)
    @ApiModelProperty(value = "科目三")
    private Long KM3;

    @Excel(name = "总分" ,width = 30)
    @ApiModelProperty(value = "总分")
    private Long ZF;
}
