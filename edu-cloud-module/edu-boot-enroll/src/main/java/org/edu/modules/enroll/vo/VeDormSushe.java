package org.edu.modules.enroll.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Auther 李少君
 * @Date 2021-07-27 16:25
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormSushe implements Serializable {
    /** id */
    @ApiModelProperty(value = "id")
    private Long id;

    /** 可住人数 */
    @ApiModelProperty(value = "可住人数")
    @TableField(value = "kzrs")
    private Integer kzrs;

    /** 已住人数 */
    @ApiModelProperty(value = "已住人数")
    @TableField(value = "yzrs")
    private Integer yzrs;
}
