package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeDictYears;
import org.edu.modules.enroll.vo.VeDictYearsVo;

import java.util.List;

//招生年份业务接口

//执行业务，和Controller对应
public interface VeDictYearsService extends IService<VeDictYears> {
    List<VeDictYearsVo> displayYears(); //显示年份代码和是否未当前年份

    List<VeDictYearsVo> CheckYears(String code, Integer id);//校验年份代码

    //Integer CheckYears1(String code,Integer id);//修改年份代码
    Integer updateById(String code, Integer id);//设置当前招生年份

    Integer addYears(String code,Integer iscur);//添加新的当前招生年份

    Integer newaddYears(String code, Integer id);//当选项编辑未否时

    //删除
    Integer delYears(String code, Integer id);

    //添加用的校验
    List<VeDictYearsVo> AddCheckYear(String code);

}
