package org.edu.modules.enroll.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.modules.enroll.entity.*;
import org.edu.modules.enroll.service.*;
import org.edu.modules.enroll.vo.ReportStatisticsVo;
import org.edu.modules.enroll.vo.VeFindStudentVo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.controller
 * @date 2021/7/15 22:54
 */
@Api(tags="报到情况统计")
@RestController
@RequestMapping("enroll/reportStatistics")
@Slf4j
public class ReportStatisticsController {
    @Resource
    private IVeBaseManageService iVeBaseManageService;

    @Resource
    private VeDictQuarterService veDictQuarterService;

    @Resource
    private VeZsRegistrationService veZsRegistrationService;
    @Resource
    private VeBaseSpecialtyService veBaseSpecialtyService;
    @Resource
    private VeBaseBanjiService veBaseBanjiService;
    @Resource
    private VeBaseFacultyService veBaseFacultyService;
    @Resource
    private VeBaseCampusService veBaseCampusService;

    @ApiOperation(value="根据专业部id获取班级")
    @PostMapping(value = "/selectClassByFacultyId")
    public Result<?> getBanjiByFacultyId(@ApiParam("专业部ID") @RequestParam("facultyId") Long facultyId){
        //根据专业部id获取所有专业信息
//        BasicResponseBO<List<VeBaseSpecialty>> veBaseSpecialties = iVeBaseManageService.getSpecialtyByFalId(facultyId);
//        List<VeBaseSpecialty> specialties = veBaseSpecialties.getResult();
        QueryWrapper<VeBaseSpecialty> specialtyQueryWrapper = new QueryWrapper<>();
        specialtyQueryWrapper.eq("falid",facultyId);
        List<VeBaseSpecialty> specialties = veBaseSpecialtyService.list(specialtyQueryWrapper);//2021.9.1
        //根据专业id查所有班级信息
        List<VeBaseBanji> allBanjiList = new ArrayList<>();//所有班级信息的集合
        for (VeBaseSpecialty v : specialties){
//            BasicResponseBO<List<VeBaseBanji>> basicResponseBO = iVeBaseManageService.getBanjiBySpecId(v.getId());
//            List<VeBaseBanji> banjiList = basicResponseBO.getResult();
            QueryWrapper<VeBaseBanji> banjiQueryWrapper = new QueryWrapper<>();
            banjiQueryWrapper.eq("spec_id",v.getId());
            List<VeBaseBanji> banjiList = veBaseBanjiService.list(banjiQueryWrapper);//2021.9.1
            allBanjiList.addAll(banjiList);
        }

        return Result.OK(allBanjiList);

    }
 /**
     * 查找校区
     */
    @ApiOperation(value="查找校区", notes="")
    @PostMapping(value = "/xiaoQu")
    public Result<?> xiaoQu() {
//    List<VeFindStudentVo> list = veFindTFStudentService.findXiaoQu();
//        BasicResponseBO<List> list = iVeBaseManageService.getCampusMessage(); //李少君
//        List<VeCampusBO> result = JSONObject.parseArray(JSON.toJSONString(list.getResult()),VeCampusBO.class);
        List<VeBaseCampus> result = veBaseCampusService.list();//2021.9.2
        List<VeFindStudentVo> list1 = new ArrayList<>();
        for(int i =0 ;i<result.size();i++){
            VeFindStudentVo c = new VeFindStudentVo();
            c.setId(result.get(i).getId());
            c.setName(result.get(i).getXqmc());
            list1.add(c);
        }

//    List<VeFindStudentVo> list= Constant.VUE_APP_API_BASE_URL+"";
        return Result.OK(list1);
    }
    @ApiOperation(value="获取统计数据")
    @PostMapping(value = "/getStatistics")
    public Result<?> getStatistics(@ApiParam("专业部ID") @RequestParam("facultyId") Long facultyId,
                                   @ApiParam("班级id") @RequestParam("classId") Long classId,@ApiParam("学校") @RequestParam("school") Long school,@ApiParam("时间") @RequestParam("dateTime") String dateTime){
        Integer theoryNum = null;
        Integer realityNum = null;
//        String time = "qq";
        //获取当前招生季
        QueryWrapper<VeDictQuarter> queryWrapper = new QueryWrapper<VeDictQuarter>().eq("isCur", 1);
        VeDictQuarter one = veDictQuarterService.getOne(queryWrapper);
        if(one==null){
            return Result.error("列表加载失败：未启用当前招生季");
        }
        Integer quarterId = one.getId();
        List<ReportStatisticsVo> reportStatisticsVoList = new ArrayList<>();


        if (facultyId != null && facultyId != 0 && classId == 0){//查看某院系的报道情况
            //根据院系id获取专业信息
//            BasicResponseBO<List<VeBaseSpecialty>> specialtyBasicResponseBO = iVeBaseManageService.getSpecialtyByFalId(facultyId);
//            List<VeBaseSpecialty> specialties = specialtyBasicResponseBO.getResult();
            QueryWrapper<VeBaseSpecialty> specialtyQueryWrapper = new QueryWrapper<>();
            specialtyQueryWrapper.eq("falid",facultyId);
            List<VeBaseSpecialty> specialties = veBaseSpecialtyService.list(specialtyQueryWrapper);//2021.9.1
            for (VeBaseSpecialty v : specialties){
                ReportStatisticsVo reportStatisticsVo = new ReportStatisticsVo();
                //获取应到人数
                theoryNum = veZsRegistrationService.getReportStatisticsNum(quarterId,null,null,null,dateTime,v.getId());
                //获取实到人数
                realityNum = veZsRegistrationService.getReportStatisticsNum(quarterId,null,null,1,dateTime,v.getId());
                reportStatisticsVo.setTheoryNum(theoryNum);
                reportStatisticsVo.setRealityNum(realityNum);
                reportStatisticsVo.setSpecialtyId(v.getId());
                reportStatisticsVo.setSpecialtyName(v.getZymc());
                reportStatisticsVoList.add(reportStatisticsVo);
            }
        }else if (classId != 0 && classId != null){//查看某班级报到情况
//            根据id获取班级信息
//            BasicResponseBO<VeBaseBanji> baseBanjiBasicResponseBO = iVeBaseManageService.getBanjiById(classId);
//            VeBaseBanji veBaseBanji = baseBanjiBasicResponseBO.getResult();
            VeBaseBanji veBaseBanji =  veBaseBanjiService.getById(classId);//2021.9.1
            //获取应到人数
            theoryNum = veZsRegistrationService.getReportStatisticsNum(quarterId,null,classId,null,dateTime,null);
            //获取实到人数
            realityNum = veZsRegistrationService.getReportStatisticsNum(quarterId,null,classId,1,dateTime,null);
            ReportStatisticsVo reportStatisticsVo = new ReportStatisticsVo();
            reportStatisticsVo.setRealityNum(realityNum);
            reportStatisticsVo.setTheoryNum(theoryNum);
            reportStatisticsVo.setClassId(classId);
            reportStatisticsVo.setClassName(veBaseBanji.getXzbmc());
            reportStatisticsVoList.add(reportStatisticsVo);
//            return Result.OK(reportStatisticsVo);
        }else if (facultyId == 0 ){
            //获取所有的院系信息
//            BasicResponseBO<List<VeBaseFaculty>> facultyBasicResponseBO = iVeBaseManageService.getFacultyAll();
//            List<VeBaseFaculty> faculties = facultyBasicResponseBO.getResult();
            List<VeBaseFaculty> faculties = veBaseFacultyService.list();//2021.9.1
            for (VeBaseFaculty v : faculties){
                //获取应到人数
                theoryNum = veZsRegistrationService.getReportStatisticsNum(quarterId,v.getId(),null,null,dateTime,null);
                //获取实到人数
                realityNum = veZsRegistrationService.getReportStatisticsNum(quarterId,v.getId(),null,1,dateTime,null);
                ReportStatisticsVo reportStatisticsVo = new ReportStatisticsVo();
                reportStatisticsVo.setRealityNum(realityNum);
                reportStatisticsVo.setTheoryNum(theoryNum);
                reportStatisticsVo.setFacultyId(v.getId());
                reportStatisticsVo.setFacultyName(v.getYxmc());
                reportStatisticsVoList.add(reportStatisticsVo);
            }
        }

        return Result.OK(reportStatisticsVoList);

    }
}
