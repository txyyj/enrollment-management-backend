package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.enroll.entity.SysDict;

public interface SysDictService extends IService<SysDict> {
}
