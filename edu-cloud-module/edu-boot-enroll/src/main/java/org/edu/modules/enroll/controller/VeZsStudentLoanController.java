package org.edu.modules.enroll.controller;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectResult;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import groovy.util.logging.Slf4j;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.apache.shiro.SecurityUtils;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.vo.LoginUser;
import org.edu.modules.enroll.service.VeZsStudentLoanService;
import org.edu.modules.enroll.util.OssDownloadUtil;
import org.edu.modules.enroll.vo.VeZsStudentLoanVo;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/8/5 15:33
 */
@Api(tags="助学贷款")
@RestController
@RequestMapping("enroll/loan")
@Slf4j
public class VeZsStudentLoanController {
    @Resource
    private VeZsStudentLoanService veZsStudentLoanService;

    @AutoLog(value = "获取贷款列表")
    @ApiOperation(value = "获取贷款列表", notes = "获取贷款列表")
    @PostMapping(value = "/getList")
    public Result<?> getList(@ApiParam("状态") @RequestParam("state") Integer state,
                                     @ApiParam("院系ID") @RequestParam("facultyId") Integer facultyId,
                                     @ApiParam("专业ID") @RequestParam("majorId") Integer majorId,
                                     @ApiParam("当前页数") @RequestParam("currentPage") Integer currentPage,
                                     @ApiParam("分页大小") @RequestParam("pageSize") Integer pageSize) {
        HashMap<String, Object> map = veZsStudentLoanService.showList(state, facultyId, majorId, currentPage, pageSize);
        return Result.OK(map);
    }

    @AutoLog(value = "助学贷款申请")
    @ApiOperation(value = "助学贷款申请", notes = "助学贷款申请")
    @PostMapping(value = "/addLoan")
    public Result<?> addLoan(@ApiParam("学生id") @RequestParam("id") Integer id,
                             @ApiParam("开户行") @RequestParam("bank") String bank,
                             @ApiParam("高校账号名称") @RequestParam("bankName") String bankName,
                             @ApiParam("高校账号") @RequestParam("bankAccount") String bankAccount,
                             @ApiParam("贷款金额") @RequestParam("loanAmount") Long loanAmount,
                             @ApiParam("借款合同编号") @RequestParam("contractNo") String contractNo,
                             @ApiParam("url") @RequestParam("bankReceipt") String bankReceipt){
        Boolean addResult = veZsStudentLoanService.addLoan(id, bank, bankName, bankAccount, loanAmount, contractNo,bankReceipt );
        if (addResult) {
            return Result.OK("添加成功");
        }
        return Result.error("添加失败");
    }

    @AutoLog(value = "根据id查找贷款信息")
    @ApiOperation(value = "根据id查找贷款信息", notes = "根据id查找贷款信息")
    @PostMapping(value = "/selectById")
    public Result<?> selectById(@ApiParam("学生id") @RequestParam("id") Integer id) {
        return veZsStudentLoanService.selectById(id);
    }

    @AutoLog(value = "审核不通过")
    @ApiOperation(value = "审核不通过", notes = "审核不通过")
    @PostMapping("/auditFailed")
    public Result<?> auditFailed(@ApiParam("id数组") @RequestParam("id") @Validated @RequestBody Long[] ids,
                                 @ApiParam("不通过理由") @RequestParam("failureReason") String failureReason) {
        boolean result = veZsStudentLoanService.auditFail(ids, failureReason);
        if (result) {
            return Result.OK("审核成功");
        }
        return Result.error("审核失败");
    }

    @AutoLog(value = "审核通过")
    @ApiOperation(value = "审核通过", notes = "审核通过")
    @PostMapping("/auditPass")
    public Result<?> auditPass(@ApiParam("id数组") @RequestParam("id") @Validated @RequestBody  Long[] ids) {
        boolean result = veZsStudentLoanService.auditPass(ids);
        if (result) {
            return Result.OK("审核成功");
        }
        return Result.error("审核失败");
    }

    @AutoLog(value = "修改贷款")
    @ApiOperation(value = "修改贷款", notes = "修改贷款")
    @PostMapping("/updateLoan")
    public Result<?> updateLoan(@ApiParam("贷款id") @RequestParam("id") Integer id,
                                @ApiParam("开户行") @RequestParam("bank") String bank,
                                @ApiParam("高校账号名称") @RequestParam("bankName") String bankName,
                                @ApiParam("高校账号") @RequestParam("bankAccount") String bankAccount,
                                @ApiParam("贷款金额") @RequestParam("loanAmount") Long loanAmount,
                                @ApiParam("url") @RequestParam("bankReceipt") String bankReceipt,
                                @ApiParam("合同编号") @RequestParam("contractNo") String contractNo){
            boolean updateResult = veZsStudentLoanService.updateLoan(id, bank, bankName, bankAccount, loanAmount,bankReceipt,contractNo);
            if (updateResult) {
                return Result.OK("修改成功");
            }
            return Result.error("修改失败");
    }

    @AutoLog(value = "上传图片")
    @ApiOperation(value = "上传图片", notes = "上传图片")
    @PostMapping(value = "/upload")
    public Result<?> upload(@ApiParam("文件") @RequestParam("file") MultipartFile file) throws Exception {
        File document = null;
        //获取文件名
        String fileName = file.getOriginalFilename();
        //用点来切割获取文件类型注意//.需要加双斜杠正则表达式
        String[] arr = fileName.split("\\.");
        //获取文件后缀名
        String suffixName = arr[arr.length - 1];
        if (!suffixName.equalsIgnoreCase("jpg") && !suffixName.equalsIgnoreCase("png")) {
            return Result.error(400, "文件类型不能上传");
        }
        try {
            String originalFilename = file.getOriginalFilename();
            String[] filename = originalFilename.split("\\.");
            document = File.createTempFile(filename[0], filename[1]);
            file.transferTo(document);
            document.deleteOnExit();
        } catch (IOException e) {

            e.printStackTrace();
        }
        String docName = UUID.randomUUID().toString();
        //上传文件到指定的存储空间  第一个参数 bucketName  第二个参数 OSS存储路径  第三个参数 要上传的文件
        OSSClient ossClient = new OSSClient("oss-cn-beijing.aliyuncs.com","LTAI5tPnLGtPS6r8BpncZMrm","BAJbUlVLXD6or53Zcw1nC8uI87UpOs");
        PutObjectResult result = ossClient.putObject("exaplebucket-beijing",docName+"/"+fileName, document);
        ossClient.shutdown();
        return Result.OK("https://exaplebucket-beijing.oss-cn-beijing.aliyuncs.com/"+docName+"/"+fileName);

    }

}
