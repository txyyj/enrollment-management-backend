package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeBaseBanji;

/**
 * 班级的Mapper接口
 * @author yhx
 */
public interface VeBaseBanjiMapper extends BaseMapper<VeBaseBanji> {
    Integer updateNanAndNvrs(@Param("classId") Long classId,@Param("nanrs") Long nanrs,@Param("nvrs") Long nvrs);
}
