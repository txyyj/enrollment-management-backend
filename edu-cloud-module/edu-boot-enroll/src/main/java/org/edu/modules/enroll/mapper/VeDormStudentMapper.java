package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.VeDormStudent;

public interface VeDormStudentMapper extends BaseMapper<VeDormStudent> {
}
