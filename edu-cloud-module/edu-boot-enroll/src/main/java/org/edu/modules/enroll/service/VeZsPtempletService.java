package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.enroll.entity.VeZsPtemplet;

/**
 * 关于通知书模板表业务
 * @author wcj
 */
public interface VeZsPtempletService  extends IService<VeZsPtemplet> {
}
