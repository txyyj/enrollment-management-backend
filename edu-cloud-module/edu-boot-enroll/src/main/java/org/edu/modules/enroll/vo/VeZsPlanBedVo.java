package org.edu.modules.enroll.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Auther 李少君 分宿舍
 * @Date 2021-07-25 22:19
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsPlanBedVo implements Serializable {

    /** id */
    @ApiModelProperty(value = "id")
    private Long id;

    /** 院系 */
    @ApiModelProperty(value = "院系")
    @TableField(value = "ycmc")
    private String yxmc;

    /** 所属年级 */
    @ApiModelProperty(value = "所属年级")
    @TableField(value = "njmc")
    private String njmc;

    /** 班级 */
    @ApiModelProperty(value = "班级")
    @TableField(value = "xzbmc")
    private String xzbmc;

    /** 男生人数 */
    @ApiModelProperty(value = "男生人数")
    @TableField(value = "nansrs")
    private Integer nansrs;

    /** 女生人数 */
    @ApiModelProperty(value = "女生人数")
    @TableField(value = "nvsrs")
    private Integer nvsrs;

    /** 总人数 */
    @ApiModelProperty(value = "总人数(人数上限)")
    @TableField(value = "zrs")
    private Integer zrs;

}
