package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.VeBaseFaculty;


/**院校表接口
 * @author 86158
 */
public interface VeBaseFacultyMapper extends BaseMapper<VeBaseFaculty> {
}
