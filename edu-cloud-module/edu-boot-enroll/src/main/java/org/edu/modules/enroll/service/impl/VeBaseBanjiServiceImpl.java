package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeBaseBanji;
import org.edu.modules.enroll.mapper.VeBaseBanjiMapper;
import org.edu.modules.enroll.service.VeBaseBanjiService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class VeBaseBanjiServiceImpl extends ServiceImpl<VeBaseBanjiMapper, VeBaseBanji> implements VeBaseBanjiService {
    @Resource
    private VeBaseBanjiMapper mapper;
    @Override
    public Integer updateNanAndNvrs(Long classId, Long nanrs, Long nvrs) {
        Integer res = mapper.updateNanAndNvrs(classId,nanrs,nvrs);
        return res;
    }
}
