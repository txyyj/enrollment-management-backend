package org.edu.modules.enroll.service;

import org.edu.modules.enroll.entity.VeZsAdmissionInformation;
import org.edu.modules.enroll.vo.AdmissionTicketInfoVo;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/7/27 20:19
 */
public interface VeZsAdmissionInformationService {
    AdmissionTicketInfoVo selectByXmKshSfzh(String XM, String KSH, String SFZH);

    VeZsAdmissionInformation applyMsg(String XM, String KSH, String SFZH);
}
