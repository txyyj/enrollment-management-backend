package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeDictArea;
import org.edu.modules.enroll.mapper.VeDictAreaMapper;
import org.edu.modules.enroll.service.VeDictAreaService;
import org.springframework.stereotype.Service;

@Service
public class VeDictAreaServiceImpl  extends ServiceImpl<VeDictAreaMapper, VeDictArea> implements VeDictAreaService {
}
