package org.edu.modules.enroll.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.vo.LoginUser;
import org.edu.modules.enroll.service.IVeBaseManageService;
import org.edu.modules.enroll.service.VeZsFenbanService;
import org.edu.modules.enroll.vo.VeZsFenbanExcelVo;
import org.edu.modules.enroll.vo.VeZsFenbanOutExcelVo;
import org.edu.modules.enroll.vo.VeZsFenbanVo;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @Auther 李少君
 * @Date 2021-07-25 13:48
 */
@Api(tags="学生导入")
@RestController
@RequestMapping("enroll/fenbanImport")
@Slf4j
public class VeZsFenbanImportController {

    @Autowired
    private VeZsFenbanService veZsFenbanService;
//    @Autowired
//    private IVeBaseManageService veBaseManageService;

    @AutoLog(value = "学生分班信息导出excel模板")
    @ApiOperation(value = "学生分班信息导出excel模板", notes = "学生导入—学生分班信息导出excel模板")
    @RequestMapping(value = "exportModel")
    public ModelAndView exportModel() {
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, "学生列表");
        mv.addObject(NormalExcelConstants.CLASS, VeZsFenbanExcelVo.class);
        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ExportParams exportParams = new ExportParams("学生列表数据(第一条数据为提示数据)", "导出人:" + user.getRealname(), "导出模板信息");
        List<VeZsFenbanExcelVo> list = new ArrayList<>();
        VeZsFenbanExcelVo v = new VeZsFenbanExcelVo();
        v.setKsh("2165454");
        v.setXm("老八");
        v.setSfzh("11144");
        v.setXb("男");

        v.setCsny("2000/11/11");
        v.setZxmc("衡水中学");
        v.setYzbm("333333");
        v.setYjdz("火星");
        v.setYb("222333");
        v.setLxdh1("13456123");
        v.setLxdh2("23121323");
        v.setTdcj("555");
        v.setZy("北京大学");
        v.setLqzy("机电一体化");
        v.setJjr("老九");
        v.setJjrdh("12134655");
        v.setJjrdz("月球");
        v.setSjr("火星");
        v.setSjrjtdz("火星2号街道");
        v.setYjh("241651");
        v.setZl("150");
        v.setGrade("2021级");
        v.setRxny(20210910L);
        v.setEnrollType("统一招生");
        v.setLuqu("正式录取");
        v.setCampus("南校区");
        v.setFaculty("旅游与服务学院");
        v.setSpec("旅游管理");
        v.setState("未分配");


//        v.setMark(545);
        v.setXh("1321548");
        v.setMz("汉族");
        v.setBmh("45456");
//        v.setJdfs("走读或住校");
//        v.setXsdqztm("5165465");
        v.setXz("两年");
        v.setProvince("辽宁");
        v.setCity("锦州");
        v.setCounty("太和区");
//        v.setSheng("辽宁");
//        v.setShi("锦州");
//        v.setQu("太和区");
//        v.setSfskns("是或否");
//        v.setZkzh("878465");

        list.add(v);
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, list);//李少君
        return mv;
    }

    @AutoLog(value = "学生分班信息导入excel表格")
    @ApiOperation(value = "学生分班信息导入excel表格", notes = "学生导入—学生分班信息导入excel表格")
    @RequestMapping(value = "import")
    public Result<?> excelImport(MultipartFile file) {
        ImportParams params = new ImportParams();
        params.setTitleRows(2);
        params.setHeadRows(1);
        params.setNeedSave(true);
        try {
            List<VeZsFenbanExcelVo> list = ExcelImportUtil.importExcel(file.getInputStream(), VeZsFenbanExcelVo.class, params);

            String a = veZsFenbanService.excelImport(list);
            if("导入成功".equals(a)){
                return Result.OK(a);
            }else{
//                return Result.OK("导入失败，请检查所填年级、校区、专业是否存在!");
                return Result.error(a);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("文件导入失败:" + e.getMessage());
        } finally {
            try {
                file.getInputStream().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //分班信息导出excel表格
    @AutoLog(value = "分班信息导出excel表格")
    @ApiOperation(value = "分班信息导出excel表格", notes = "预分班—分班信息导出excel表格")
    @RequestMapping(value = "export")
    public ModelAndView excelExport( @RequestParam(value = "gradeId") Integer gradeId,
                                     @RequestParam(value = "enrollType") String enrollType,
                                     @RequestParam(value = "falId") Integer falId,
                                     @RequestParam(value = "specId") Integer specId,
                                     @RequestParam(value = "statuId") Integer statuId

    ) {
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        List<VeZsFenbanVo> list = veZsFenbanService.getFenbanImport(gradeId, enrollType, falId, specId, statuId);

        List<VeZsFenbanOutExcelVo> excelList=new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            VeZsFenbanOutExcelVo vo = new VeZsFenbanOutExcelVo();
            vo.setId(list.get(i).getId());
            vo.setXm(list.get(i).getXm());
            vo.setXh(list.get(i).getXh());
            vo.setGrade(list.get(i).getNjmc());
            vo.setMark(list.get(i).getMark());
            vo.setEnrollType(list.get(i).getEnrollType());
            vo.setLuqu(list.get(i).getLuqu());
            vo.setCampus(list.get(i).getXqmc());
            vo.setSpec(list.get(i).getZymc());
            Integer statu = list.get(i).getStatuId();
            String state = null;
            if(statu==0){
                state = "未分配";
            }else{
                state = "分配";
            }
            vo.setState(state);
            excelList.add(vo);

        }

        mv.addObject(NormalExcelConstants.FILE_NAME, "学生分班信息");
        mv.addObject(NormalExcelConstants.CLASS, VeZsFenbanExcelVo.class);
        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ExportParams exportParams = new ExportParams("学生分班信息", "导出人:" + user.getRealname(), "导出信息");
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, excelList);
        return mv;
    }







}
