package org.edu.modules.enroll.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.enroll.entity.VeDictYears;
import org.edu.modules.enroll.service.VeDictQuarterService;
import org.edu.modules.enroll.service.VeDictYearsService;
import org.edu.modules.enroll.vo.VeDictYearsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Description:  当前招生年份数据处理
 * @Author:  zh
 * @Date:  2021-04-20
 * @Version:  V2.0
 */
@Api(tags="招生年份管理")
@RestController
@RequestMapping("enroll/curSeasonSet")
@Slf4j
public class VeDictYearsController extends BaseController<VeDictYears, VeDictYearsService> {
@Autowired
    private  VeDictYearsService veDictYearsService;
@Autowired
private VeDictQuarterService veDictQuarterService;

//招生年份显示
    @AutoLog(value = "招生年份数据-显示表格")
    @ApiOperation(value="显示表格", notes="招生年份数据-显示表格")
    @PostMapping("/cess")
    //传到前端的参数路径 (pageNo-1)*pageSize就是pageNo
    public Result<?> cess(
//            @RequestParam(value = "pageNo") Integer pageNo,
//            @RequestParam(value = "pageSize") Integer pageSize

    ){
        QueryWrapper<VeDictYears> wrapper = new QueryWrapper<>();
        wrapper.eq("code","年份代码");
        List<VeDictYearsVo> curYearlist = veDictYearsService.displayYears();
//        //数据众数
//        int size = veDictYearsService.list().size();
//
//        Map<String,Object> map = new HashMap<>();
//        map.put("count",size);
//        map.put("nflist",curYearlist);
        //发送请求到前端
        return Result.OK(curYearlist);

    }

    //当前招生
    @AutoLog(value = "招生年份数据-编辑当前招生年份")
    @ApiOperation(value="编辑当前招生年份", notes="招生年份数据-编辑当前招生年份")
    @PostMapping("/EditYear")
    public Result<?> EditYear(
            @RequestParam(value = "curYear") Integer curYear,
            @RequestParam(value = "code") String code,
            @RequestParam(value = "key") Integer id
    ){

// //前端会解析初始数据源，也就是未修改前的进行发送过来，此处根据前端传来的进行改写，原数据为是的改为否，源数据为否的改为是
        if(curYear!=1) {
            //全未0
            if (veDictYearsService.updateById(code, id) < 0) {
                return Result.OK("修改年份失败");
            }
        }
        return Result.OK("修改成功");
    }

    @AutoLog(value = "招生年份数据-编辑招生年份")
    @ApiOperation(value="编辑招生年份", notes="招生年份数据-编辑招生年份")
    @PostMapping("/bianji")
    public Result<?> bianji(
            @RequestParam(value = "curYear") Integer curYear,
            @RequestParam(value = "code") String code,
            @RequestParam(value = "key") Integer id
    ){

        System.out.println("收到的参数为"+curYear+"年份代码"+code+"键值"+id);
//校验年份代码
        if (veDictYearsService.CheckYears(code,id).size()>1) {
            return Result.OK("年份已存在");
        }else if(veDictYearsService.CheckYears(code,id).size()<1){
            return Result.OK("修改年份成功");
        }
//编辑年份

        if(curYear==1) {
            //全未0
            if (veDictYearsService.updateById(code, id) < 0) {
                return Result.OK("修改年份失败");
            }
        }else {
            if( veDictYearsService.newaddYears(code, id)<0){
                return Result.OK("修改年份失败");
            }
        }
        return Result.OK("修改成功");
    }

    //删除
    @AutoLog(value = "招生年份数据-删除")
    @ApiOperation(value="删除", notes="招生年份数据-删除")
    @PostMapping("/del")
    //传到前端的参数路径
    public Result<?> del( @RequestParam(value = "code") String code,
                          @RequestParam(value = "key") Integer id){


        VeDictYears veDictYears = veDictYearsService.getById(id);
        if (veDictYears.getCurYear()==1){
            return Result.error("当前年份不能删除");
        }else {
            veDictYearsService.removeById(id);
        }
//        if(veDictYearsService.delYears(code,id)!=1){
//          return Result.OK("删除失败");
//      }
        //发送请求到前端
        return Result.OK("删除成功");

    }

    //新增
    @AutoLog(value = "招生年份数据-新增")
    @ApiOperation(value="新增", notes="招生年份数据-新增")
    @PostMapping("/add")
    //传到前端的参数路径
    public Result<?> add(@RequestParam(value = "code") String code,@RequestParam(value = "iscur") Integer iscur){
        //校验年份代码

        if (veDictYearsService.addYears(code,iscur)==1) {
            return Result.OK("新增年份成功");
        }else {
            return Result.OK("新增年份失败");
        }
//        if(veDictYearsService.addYears(code)!=1){
//            return Result.OK("添加失败");
//        }
        //发送请求到前端



    }


}
