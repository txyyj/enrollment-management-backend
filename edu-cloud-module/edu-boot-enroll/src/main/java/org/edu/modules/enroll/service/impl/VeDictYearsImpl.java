package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeDictYears;
import org.edu.modules.enroll.mapper.VeDictYearsMapper;
import org.edu.modules.enroll.service.VeDictYearsService;
import org.edu.modules.enroll.vo.VeDictYearsVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;
import java.util.List;

@Service
public class VeDictYearsImpl extends ServiceImpl<VeDictYearsMapper, VeDictYears> implements VeDictYearsService {
    @Resource
    private VeDictYearsMapper veDictYearsMapper;

@Override
    public List<VeDictYearsVo> displayYears(){
        return  veDictYearsMapper.displayYears();
    }

    @Override
    @Transactional(rollbackOn = Exception.class)
    public List<VeDictYearsVo> CheckYears(String code,Integer id) {
        //修改年份代码
        veDictYearsMapper.CheckYears1(code,id);
        //校验年份代码
        return veDictYearsMapper.CheckYears(code);
    }



//    @Override
//    public Integer CheckYears1(String code, Integer id) {
//
//
//        return veDictYearsMapper.CheckYears1(code,id);
//    }

    //设置当前招生年份
    @Override
    @Transactional(rollbackOn = Exception.class)
    public Integer updateById(String code,Integer id) {



        veDictYearsMapper.updateByIds();//先执行全变为0
        //然后执行设置
        return veDictYearsMapper.updateById(code,id);
    }

//    //添加新年份
//    @Override
//    public Integer addYears(String code) {
//    if(veDictYearsMapper.AddCheckYear(code).size()==0){
//
//        VeDictYears veDictYears = new VeDictYears();
//        veDictYears.setCode(code);
//        veDictYears.setIs_deleted(0);
//        return  veDictYearsMapper.insert(veDictYears);
//       // return  veDictYearsMapper.addYears(code);
//    }
//
//    return 6;
//    }
//添加新年份林彬辉
@Override
public Integer addYears(String code,Integer iscur) {
    if (iscur==1){
        veDictYearsMapper.updateByIds();//先执行全变为0
    }
    if(veDictYearsMapper.AddCheckYear(code).size()==0){

        VeDictYears veDictYears = new VeDictYears();
        veDictYears.setCurYear(iscur);
        veDictYears.setCode(code);
        veDictYears.setIs_deleted(0);
        return  veDictYearsMapper.insert(veDictYears);
        // return  veDictYearsMapper.addYears(code);
    }

    return 6;
}
//编辑未否的时候
    @Override
    public Integer newaddYears(String code, Integer id) {
        return veDictYearsMapper.newaddYears(code, id);
    }

    //删除
    @Override
    public Integer delYears(String code, Integer id) {
        return veDictYearsMapper.delYears(code, id);
    }
    //添加用的校验
    @Override
   // @Transactional(rollbackOn = Exception.class)
    public List<VeDictYearsVo> AddCheckYear(String code) {
    //添加

        //返回校验结果
        return veDictYearsMapper.AddCheckYear( code);
    }


}
