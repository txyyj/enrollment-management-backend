package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.checkerframework.checker.units.qual.A;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @Auther 李少君
 * @Date 2021-07-27 20:28
 */
@Data
@TableName("ve_zs_single_enrollment_expansion")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsSingleEnrollExpansion {

    /** id */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /** 姓名 */
    @ApiModelProperty(value = "姓名")
    @TableField(value = "XM")
    private String xm;

    /** 性别 */
    @ApiModelProperty(value = "性别")
    @TableField(value = "XB")
    private Integer xb;

    /** 身份证号 */
    @ApiModelProperty(value = "身份证号")
    @TableField(value = "SFZH")
    private String sfzh;

    /** 毕业学校 */
    @ApiModelProperty(value = "毕业学校")
    @TableField(value = "BYXX")
    private String byxx;

    /** 准考证号 */
    @ApiModelProperty(value = "准考证号")
    @TableField(value = "ZKZH")
    private String zkzh;

    /** 考生号 */
    @ApiModelProperty(value = "考生号")
    @TableField(value = "KSH")
    private String ksh;

    /** 招生类型 */
    @ApiModelProperty(value = "招生类型-1单招-2扩招-3大专")
    @TableField(value = "ZSLX")
    private Integer zslx;

    /** 专业一id */
    @ApiModelProperty(value = "专业一id")
    @TableField(value = "specIdOne")
    private Integer specIdOne;

    /** 专业一是否服从调剂 */
    @ApiModelProperty("专业一是否服从调剂")
    @TableField(value = "isAdjustOne")
    private Integer isAdjustOne;

    /** 专业一名称 */
    @ApiModelProperty(value = "专业一名称")
    @TableField(value = "zymcOne")
    private String zymcOne;

    /** 专业二id */
    @ApiModelProperty(value = "专业二id")
    @TableField(value = "specIdTwo")
    private Integer specIdTwo;

    /** 专业二是否服从调剂 */
    @ApiModelProperty("专业二是否服从调剂")
    @TableField(value = "isAdjustTwo")
    private Integer isAdjustTwo;

    /** 专业二名称 */
    @ApiModelProperty(value = "专业二名称")
    @TableField(value = "zymcTwo")
    private String zymcTwo;

    /** 所在省份 */
    @ApiModelProperty(value = "所在省份")
    @TableField(value = "province")
    private String province;

    /** 所在省份id */
    @ApiModelProperty(value = "所在省份id")
    @TableField(value = "provinceId")
    private Integer provinceId;

    /** 所在市 */
    @ApiModelProperty(value = "所在市")
    @TableField(value = "city")
    private String city;

    /** 所在省份id */
    @ApiModelProperty(value = "所在省份id")
    @TableField(value = "cityId")
    private Integer cityId;

    /** 所在区 */
    @ApiModelProperty(value = "所在区")
    @TableField(value = "county")
    private String county;

    /** 所在区id */
    @ApiModelProperty(value = "所在区id")
    @TableField(value = "countyId")
    private Integer countyId;

    /** 家庭地址 */
    @ApiModelProperty(value = "家庭地址")
    @TableField(value = "JTDZ")
    private String jtdz;

    /** 家庭邮编 */
    @ApiModelProperty(value = "家庭邮编")
    @TableField(value = "JTYB")
    private String jtyb;

    /** 收件人电话 */
    @ApiModelProperty(value = "收件人电话")
    @TableField(value = "JTLXDH")
    private String jtlxdh;

    /** 收件人 */
    @ApiModelProperty(value = "收件人")
    @TableField(value = "SJR")
    private String sjr;

    /** 审核 */
    @ApiModelProperty(value = "审核-0未审核-1已审核")
    @TableField(value = "isCheck")
    private Integer isCheck;
    //林彬辉====
    /** 通过 */
    @ApiModelProperty(value = "通过-0未通过-1已通过")
    @TableField(value = "isAdopt")
    private Integer isAdopt;
    //========
    /** creatTime */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;

    /** updateTime */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time",update = "now()")
    private Date updateTime;

    /** 逻辑删除的标识 */
    @ApiModelProperty(value = "逻辑删除")
    @TableField(value = "is_deleted")
    @TableLogic
    private Integer isDeleted;





}
