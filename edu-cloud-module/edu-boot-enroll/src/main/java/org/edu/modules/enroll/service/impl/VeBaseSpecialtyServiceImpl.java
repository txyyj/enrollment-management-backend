package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeBaseSpecialty;
import org.edu.modules.enroll.mapper.VeBaseSpecialtyMapper;
import org.edu.modules.enroll.service.VeBaseSpecialtyService;
import org.springframework.stereotype.Service;

@Service
public class VeBaseSpecialtyServiceImpl extends ServiceImpl<VeBaseSpecialtyMapper,VeBaseSpecialty> implements VeBaseSpecialtyService {

}
