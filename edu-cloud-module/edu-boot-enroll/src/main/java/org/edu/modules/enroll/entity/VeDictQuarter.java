package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
<<<<<<< .mine
 * @Description:  当前招生季对象，Serializable是一个对象序列化的接口，一个类只有实现了Serializable接口，它的对象才是可序列化的
 * 实现Serializable接口用于序列化对象
||||||| .r219
 * @Description:  当前招生季对象
=======
 * @Description:  当前招生季对象接口
>>>>>>> .r232
 * @Author:  wcj
 * @Date:  2021-04-05
 * @Version:  V1.0
 */
@Data
@TableName("ve_dict_quarter")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDictQuarter implements Serializable {

    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Integer id;

    /**招生季代码*/
    @Excel(name = "招生季代码", width = 15)
    @ApiModelProperty(value = "招生季代码")
    @TableField(value = "code")
    private String code;

    /**招生季名称*/
    @Excel(name = "招生季名称", width = 15)
    @ApiModelProperty(value = "招生季名称")
    @TableField(value = "name")
    private String name;

    /**年份*/
    @Excel(name = "年份", width = 15)
    @ApiModelProperty(value = "年份")
    @TableField(value = "year")
    private String year;

    /**终端id*/
    @Excel(name = "终端id", width = 15)
    @ApiModelProperty(value = "终端id")
    @TableField(value = "terminalId")
    private Long terminalId;

    /**是否当前招生季*/
    @Excel(name = "是否当前招生季", width = 15)
    @ApiModelProperty(value = "是否当前招生季（0=否，1=是）")
    @TableField(value = "isCur")
    private Integer isCur;

    /**入学年月*/
    @Excel(name = "入学年月", width = 15)
    @ApiModelProperty(value = "入学年月")
    @TableField(value = "RXNY")
    private String rxny;

    /**报名开始时间*/
    @Excel(name = "报名开始时间", width = 15)
    @ApiModelProperty(value = "报名开始时间")
    @TableField(value = "start_Time")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date start_Time;

    /**报名结束时间*/
    @Excel(name = "报名结束时间", width = 15)
    @ApiModelProperty(value = "报名结束时间")
    @TableField(value = "end_Time")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date end_Time;

    /**创建人id*/
    @Excel(name = "创建人id", width = 15)
    @ApiModelProperty(value = "创建人id")
    @TableField(value = "create_by")
    private String createBy;

    /**创建时间*/
    @Excel(name = "创建时间", width = 15)
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time")
    private String createTime;

    /**修改人*/
    @Excel(name = "修改人id", width = 15)
    @ApiModelProperty(value = "修改人id")
    @TableField(value = "update_by")
    private String updateBy;

    /**修改时间*/
    @Excel(name = "修改时间", width = 15)
    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time")
    private String updateTime;

    /** 逻辑删除的标识 */
    @Excel(name = "逻辑删除", width = 15)
    @ApiModelProperty(value = "逻辑删除（0=否，1=是）")
    @TableField(value = "is_deleted")
    @TableLogic
    private Integer isDeleted;


}
