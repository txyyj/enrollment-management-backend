package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeZsStudentLoanEntity;
import org.edu.modules.enroll.vo.VeZsStudentLoanVo;

import java.util.List;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/8/5 9:56
 */
public interface VeZsStudentLoanMapper extends BaseMapper<VeZsStudentLoanEntity> {
    List<VeZsStudentLoanVo> showList(@Param("state") Integer state);

    Boolean addLoan (@Param("id") Integer id,@Param("bank") String bank,@Param("bankName") String bankName,@Param("bankAccount")String bankAccount,@Param("loanAmount") Long loanAmount,@Param("contractNo") String contractNo,@Param("url") String url);

    VeZsStudentLoanVo selectById(@Param("id")Integer id);

    Boolean auditPass(@Param("id") Long  id);

    Boolean auditFail(@Param("id") Long  id,@Param("failureReason")String failureReason);

    Boolean updateLoan(@Param("id")Integer id,@Param("bank") String bank,@Param("bankName") String bankName,@Param("bankAccount") String bankAccount,@Param("loanAmount") Long loanAmount,@Param("bankReceipt") String bankReceipt,@Param("contractNo") String contractNo);

}
