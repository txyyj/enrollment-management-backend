package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description:  数据字典表对象类接口
 * @Author:  wcj
 * @Date:  2021-04-13
 * @Version:  V1.0
 */

@Data
@TableName("sys_dict")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class SysDict {


    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private String id;

    /** 字典名称 */
    @Excel(name = "字典名称", width = 15)
    @ApiModelProperty(value = "字典名称")
    @TableField(value = "dict_name")
    private String dictName;

    /** 字典编码 */
    @Excel(name = "字典编码", width = 15)
    @ApiModelProperty(value = "字典编码")
    @TableField(value = "dict_code")
    private String dictCode;

    /** 描述 */
    @Excel(name = "描述", width = 15)
    @ApiModelProperty(value = "描述")
    @TableField(value = "description")
    private String description;
}
