package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeFileFiles;
import org.edu.modules.enroll.entity.VeZsPlan;
import org.edu.modules.enroll.vo.VeZsPlanVo;

import java.util.List;

public interface VeZsPlanMapper extends BaseMapper<VeZsPlan> {
    //显示数据的方法
    List<VeZsPlanVo> lianbiao(@Param("name")String name,@Param("falId")Long falId,@Param("specialId")Long specialId);
    //l逻辑删除
    Integer delQuarter(@Param("id")Integer id);
    //添加
    Integer addZsjQuarter (
            @Param("falId")Integer falId,
            @Param("specId")Integer specId,
            @Param("ZSNF")String ZSNF,
            @Param("ZSJ")Integer ZSJ,
            @Param("BJS")Integer BJS,
            @Param("NANSRS")Integer NANSRS,
            @Param("NVSRS")Integer NVSRS,
            @Param("fileId")Integer fileId,
            @Param("ZYYQ")String ZYYQ,
            @Param("PYMB")String PYMB,
            @Param("ZYZYKC")String ZYZYKC,
            @Param("BXTJ")String BXTJ,
            @Param("remark")String remark,
            @Param("ZRS")Integer ZRS


    );
    //编辑
            Integer updateZsQuarter(@Param("falId")Integer falId,
                                    @Param("specId")Integer specId,
                                    @Param("ZSNF")String ZSNF,
                                    @Param("ZSJ")Integer ZSJ,
                                    @Param("BJS")Integer BJS,
                                    @Param("NANSRS")Integer NANSRS,
                                    @Param("NVSRS")Integer NVSRS,
                                    @Param("fileId")Integer fileId,
                                    @Param("ZYYQ")String ZYYQ,
                                    @Param("PYMB")String PYMB,
                                    @Param("ZYZYKC")String ZYZYKC,
                                    @Param("BXTJ")String BXTJ,
                                    @Param("remark")String remark,
                                    @Param("ZRS")Integer ZRS,
                                    @Param("id")Integer id
            );

    List<VeZsPlanVo>  ZsJy ( @Param("id")Integer id);

    //上传附件
    boolean addFujian(VeFileFiles veFileFiles);
    //查刚上传附件的id
    Integer findFileId(@Param("fileUrl") String fileUrl);

    String findUrl(@Param("id") Integer id);
}
