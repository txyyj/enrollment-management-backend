package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.swagger.models.auth.In;
import org.edu.modules.enroll.entity.VeZsReportMsg;
import org.edu.modules.enroll.mapper.VeZsReportMessageMapper;
import org.edu.modules.enroll.service.VeZsReportMessageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.service.impl
 * @date 2021/7/19 21:12
 */
@Service
public class VeZsReportMessageServiceImpl extends ServiceImpl<VeZsReportMessageMapper, VeZsReportMsg> implements VeZsReportMessageService {
    @Resource
    private VeZsReportMessageMapper mapper;
    @Override
    public List<VeZsReportMsg> getReportMessageList(String endPlaceId, String staTime, String endTime, Integer isNeed, Integer currentPage, Integer pageSize) {
        List<VeZsReportMsg> list = mapper.getReportMessageList(endPlaceId,staTime,endTime,isNeed,currentPage,pageSize);
        return list;
    }
    //统计总数
    @Override
    public Integer countReportMessage(String endPlaceId, String staTime, String endTime, Integer isNeed) {
        Integer count = mapper.countReportMessage(endPlaceId,staTime,endTime,isNeed);
        return count;
    }


    //删除
    @Override
    public Integer deletedById(Integer id) {
        Integer res = mapper.deleteById(id);
        return res;
    }


}
