package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeDictQuarter;
import org.edu.modules.enroll.mapper.VeDictQuarterMapper;
import org.edu.modules.enroll.service.VeDictQuarterService;
import org.edu.modules.enroll.vo.VeDictQuarterVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.transaction.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class VeDictQuarterImpl extends ServiceImpl<VeDictQuarterMapper, VeDictQuarter> implements VeDictQuarterService {
   @Resource
   private VeDictQuarterMapper veDictQuarterMapper;




//    @Override
//    @Transactional(rollbackOn = Exception.class)
//   // 修改新的招生季为当前招生季
//    public Integer AddupdateIsCurOne(String code, String name) {
//        veDictQuarterMapper.updateAnyIsCur();//将所有的招生季设为否
//        return 1;
//    }

//    //新增校验
//    @Override
//
//    public List<VeDictQuarterVo> CheckAddQuarter(String code, String name) {
//        return veDictQuarterMapper.CheckAddQuarter(code,name);
//    }



//    //校验相同的招生季代码，招生季名称
//    @Override
//    @Transactional(rollbackOn = Exception.class)
//    public List<VeDictQuarterVo> CheckQuarter(String code, String name,Integer id) {
//        //修改招生季的名称
//        veDictQuarterMapper.updateName(name,id);
//        //修改招生季的代码
//        veDictQuarterMapper.updateCode(code, id);
//        return null;
//    }






////将特定招生季改为非当前招生季
//    @Override
//    public Integer updateIsCur(String code, String name, Integer id) {
//        return veDictQuarterMapper.updateIsCur(code, name, id);
//    }

    /////////////////////////////////////////////////////////////////////////////////
    //编辑功能
    @Override
    public Integer updateQuarter(String code, Integer id, String name, String year, Integer isCur, String rxny, Date start_Time, Date end_Time) {
        List<VeDictQuarterVo> list= veDictQuarterMapper.CheckQuarter(code, id);
        System.out.println("编辑功能的实现类测试"+list);
        if(list.size()>0){
            System.out.println("招生季编辑模块的校验部分");
            //返回0表示已经存在相同数据
            return 0;
        }
        System.out.println("招生季编辑模块的校验部分收到的是否标签"+isCur+"前指");
        if(isCur==1){
            System.out.println("招生季编辑模块的设置部分");
            //将所有的招生季设为否
            veDictQuarterMapper.updateAnyIsCur();
        }
       return veDictQuarterMapper.updateQuarter(code,  id, name, year, isCur, rxny, start_Time, end_Time);
    }

    //新加招生季的方法
    @Override
    public Integer addQuarter(String code, String name, String year, Integer isCur, String rxny, Date start_Time, Date end_Time) {
        List<VeDictQuarterVo> list= veDictQuarterMapper.CheckAddQuarter(code);//进行校验
        if(list.size()>0){
            //返回0表示已经存在相同数据
            return 0;
}
        if(isCur==1){
            System.out.println("招生季编辑模块的设置部分");
            //将所有的招生季设为否
            veDictQuarterMapper.updateAnyIsCur();
        }

        return veDictQuarterMapper.addQuarter( code,name,year, isCur,rxny, start_Time, end_Time);
    }


    //查询所有
    @Override
    public List<VeDictQuarterVo> selectById() {
        return veDictQuarterMapper.selectById();
    }

    //l逻辑删除
    @Override
    public Integer delQuarter(String code, Integer id) {
        return veDictQuarterMapper.delQuarter(code, id);
    }
    //设置当前招生季
    @Override
    public Integer updateIsCurOne(Integer isCur, Integer id) {
        //先执行全变为0
      //  if(isCur==1){
            //将所有的招生季设为否
            veDictQuarterMapper.updateAnyIsCur();
       // }
        return veDictQuarterMapper.updateIsCurOne(isCur,id);
    }


}
