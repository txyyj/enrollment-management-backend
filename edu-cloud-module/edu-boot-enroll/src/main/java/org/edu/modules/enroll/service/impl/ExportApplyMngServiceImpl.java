package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeZsRegistration;
import org.edu.modules.enroll.mapper.ExportApplyMngVoMapper;
import org.edu.modules.enroll.mapper.VeApplyMngImportMapper;
import org.edu.modules.enroll.service.ExportApplyMngService;
import org.edu.modules.enroll.service.VeApplyMngImportService;
import org.edu.modules.enroll.vo.ApplyMngImportVo;
import org.edu.modules.enroll.vo.ExportApplyMngVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class ExportApplyMngServiceImpl extends ServiceImpl<ExportApplyMngVoMapper, VeZsRegistration> implements ExportApplyMngService {

    @Resource
    private ExportApplyMngVoMapper exportApplyMngVoMapper;

    @Override
    public List<ExportApplyMngVo> getExportApplyMsgMngList(Integer quarterId, Long facultyId, Long specialtyId, String keyword, String condition, Integer isAdmit) {
        List<ExportApplyMngVo> list = exportApplyMngVoMapper.getExportApplyMngList(quarterId, facultyId, specialtyId, keyword, condition,isAdmit);
        return list;
    }
}
