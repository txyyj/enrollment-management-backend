package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeBaseStudent;

import java.util.List;

/**
 * 新生表Mapper接口
 * @author
 */
public interface VeBaseStudentMapper extends BaseMapper<VeBaseStudent> {
    Integer saveBaseStudent(@Param("student") VeBaseStudent veBaseStudent);
}
