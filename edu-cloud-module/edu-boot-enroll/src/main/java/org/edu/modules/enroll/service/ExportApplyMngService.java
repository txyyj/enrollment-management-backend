package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.enroll.entity.VeZsRegistration;
import org.edu.modules.enroll.vo.ExportApplyMngVo;

import java.util.List;

public interface ExportApplyMngService extends IService<VeZsRegistration> {

    /**
     * 取报名信息管理集合（已审核）导出
     * @return 新生报到集合
     */
    List<ExportApplyMngVo> getExportApplyMsgMngList(Integer quarterId, Long facultyId, Long specialtyId,
                                                    String keyword, String condition,
                                                    Integer isAdmit);

}
