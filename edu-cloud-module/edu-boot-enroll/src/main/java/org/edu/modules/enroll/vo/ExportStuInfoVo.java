package org.edu.modules.enroll.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description:  导出新生信息VO
 * @Author:  yhx
 * @Date:  2021-04-15
 * @Version:  V1.0
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ExportStuInfoVo implements Serializable {

    /** id */
    @ApiModelProperty(value = "id")
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private String xm;

    /** 学号 */
    @Excel(name = "学号", width = 20)
    @ApiModelProperty(value = "学号")
    private String xh;

    /** 性别码 */
    @Excel(name = "性别码", width = 15)
    @ApiModelProperty(value = "性别码")
    private String xbm;

    /** 学生状态 */
    @Excel(name = "学生状态", width = 15)
    @ApiModelProperty(value = "学生状态")
    private String xszt;

    /** 身份证号 */
    @Excel(name = "身份证号", width = 20)
    @ApiModelProperty(value = "身份证号")
    private String sfzh;

    /** 院系名称 */
    @Excel(name = "所属专业部", width = 15)
    @ApiModelProperty(value = "院系名称")
    private String yxmc;

    /** 专业名称 */
    @Excel(name = "所属专业", width = 15)
    @ApiModelProperty(value = "专业名称")
    private String zymc;

    /** 年级 */
    @Excel(name = "所属年级", width = 15)
    @ApiModelProperty(value = "年级")
    private String njmc;

    /** 班级 */
    @Excel(name = "所属班级", width = 20)
    @ApiModelProperty(value = "班级")
    private String xzbmc;

    @ApiModelProperty(value = "班级id")
    private Long classId;
    @ApiModelProperty(value = "年级id")
    private Long gradeId;

}
