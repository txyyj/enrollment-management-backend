package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.enroll.entity.VeBaseStudent;

public interface VeBaseStudentService extends IService<VeBaseStudent> {
    Integer saveBaseStudent(VeBaseStudent veBaseStudent);
}
