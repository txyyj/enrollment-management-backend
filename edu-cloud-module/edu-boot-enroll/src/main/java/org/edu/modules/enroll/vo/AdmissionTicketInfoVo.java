package org.edu.modules.enroll.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/7/28 17:45
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class AdmissionTicketInfoVo implements Serializable {
    @Excel(name = "id", width = 30)
    @ApiModelProperty(value = "id")
    private Long id;

    @Excel(name = "姓名", width = 30)
    @ApiModelProperty(value = "姓名")
    private String XM;

    @Excel(name = "姓名", width = 30)
    @ApiModelProperty(value = "性别")
    private String XB;

    @Excel(name = "准考证号", width = 30)
    @ApiModelProperty(value = "准考证号")
    private String ZKZH;

    @Excel(name = "身份证号", width = 30)
    @ApiModelProperty(value = "身份证号")
    private String SFZH;

    @Excel(name = "考场", width = 30)
    @ApiModelProperty(value = "考场")
    private String KC;

    @Excel(name = "座位号", width = 30)
    @ApiModelProperty(value = "座位号")
    private String ZWH;

    @Excel(name = "报考学校", width = 30)
    @ApiModelProperty(value = "报考学校")
    private String BKXX;

    @Excel(name = "考试地点", width = 30)
    @ApiModelProperty(value = "考试地点")
    private String KSDD;

    @Excel(name = "考试时间", width = 30)
    @ApiModelProperty(value = "考试时间")
    private String KSSJ;

    @Excel(name = "录取信息查询", width = 30)
    @ApiModelProperty(value = "录取信息查询")
    private String LQXXCX;

    @Excel(name = "考试分数查询", width = 30)
    @ApiModelProperty(value = "考试分数查询")
    private String KSFSCX;

    @Excel(name = "备注", width = 30)
    @ApiModelProperty(value = "备注")
    private String remark;

}
