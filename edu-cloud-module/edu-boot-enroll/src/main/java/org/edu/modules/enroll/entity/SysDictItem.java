package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description:  数据字典选项表对象类接口
 * @Author:  wcj
 * @Date:  2021-04-13
 * @Version:  V1.0
 */

@Data
@TableName("sys_dict_item")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class SysDictItem {


    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private String id;

    /** 字典id */
    @Excel(name = "字典id", width = 15)
    @ApiModelProperty(value = "字典id")
    @TableField(value = "dict_id")
    private String dictId;

    /** 字典项文本 */
    @Excel(name = "字典项文本", width = 15)
    @ApiModelProperty(value = "字典项文本")
    @TableField(value = "item_text")
    private String itemText;

}
