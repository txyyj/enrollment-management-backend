package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeZsFenban;
import org.edu.modules.enroll.entity.VeZsRegistration;
import org.edu.modules.enroll.mapper.VeZsRegistrationMapper;
import org.edu.modules.enroll.service.VeZsRegistrationService;
import org.edu.modules.enroll.vo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class VeZsRegistrationServiceImpl extends ServiceImpl<VeZsRegistrationMapper, VeZsRegistration> implements VeZsRegistrationService {

    @Resource
    private VeZsRegistrationMapper veZsRegistrationMapper;


    @Override
    public List<VeZsRegistrationVo> getReportStuList(Integer quarterId, Long facultyId, Long specialtyId,
                                                     String keyword, String condition,
                                                     Integer currentPage, Integer pageSize) {
        List<VeZsRegistrationVo> list = veZsRegistrationMapper.getReportStuList(quarterId, facultyId, specialtyId, keyword, condition, currentPage, pageSize);
        return list;
    }

    @Override
    public List<VeZsRegistrationVo> getReportedStuList(Integer quarterId, Long facultyId, Long specialtyId, String keyword, String condition, Integer currentPage, Integer pageSize) {
        List<VeZsRegistrationVo> list = veZsRegistrationMapper.getReportedStuList(quarterId, facultyId, specialtyId, keyword, condition, currentPage, pageSize);
        return list;
    }

    @Override
    public List<VeZsRegistrationVo> getStuOfNoDivideClass(Long gradeId, Long facultyId, Long specialtyId, String keyword, String condition, Integer currentPage, Integer pageSize) {
        List<VeZsRegistrationVo> list = veZsRegistrationMapper.getStuOfNoDivideClass(gradeId, facultyId, specialtyId, keyword, condition, currentPage, pageSize);
        return list;
    }

    @Override
    public List<VeZsRegistrationVo> getStuOfDivideByClazzId(Long clazzId, Integer currentPage, Integer pageSize) {
        List<VeZsRegistrationVo> list = veZsRegistrationMapper.getStuOfDivideByClazzId(clazzId, currentPage, pageSize);
        return list;
    }

    @Override
//    public List<VeZsRegistrationVo> getStuInfoList(String curYear, Long facultyId, Long specialtyId, Long clazzId, String keyword, String condition, Integer currentPage, Integer pageSize) {
//        List<VeZsRegistrationVo> list = veZsRegistrationMapper.getStuInfoList(curYear, facultyId, specialtyId, clazzId, keyword, condition, currentPage, pageSize);
//        return list;
//    }
    public List<VeZsRegistrationVo> getStuInfoList(String curYear,Integer isNum, Long facultyId, Long specialtyId, Long clazzId, String keyword, String condition, Integer currentPage, Integer pageSize) {
        List<VeZsRegistrationVo> list = veZsRegistrationMapper.getStuInfoList(curYear, isNum,facultyId, specialtyId, clazzId, keyword, condition, currentPage, pageSize);
        return list;
    }

    @Override
    public List<ExportStuInfoVo> exportStuInfo(Integer isNum, Long facultyId, Long specialtyId, Long clazzId, String keyword, String condition) {
        List<ExportStuInfoVo> list = veZsRegistrationMapper.exportStuInfo(isNum,facultyId, specialtyId, clazzId, keyword, condition);
        return list;
    }

    @Override
    public VeZsRegistrationVo getPrintStuInfo(Long id) {
        VeZsRegistrationVo printStuInfo = veZsRegistrationMapper.getPrintStuInfo(id);
        return printStuInfo;
    }

    @Override
    public List<VeZsRegistrationVo> getAdmissionPrint(Integer quarterId, Long facultyId, Long specialtyId, String keyword, String condition, Integer currentPage, Integer pageSize, Integer isPrint) {
        List<VeZsRegistrationVo> list = veZsRegistrationMapper.getAdmissionPrint(quarterId, facultyId, specialtyId, keyword, condition, currentPage, pageSize, isPrint);
        return list;
    }

    /**获取报名信息业务实现类*/
    @Override
    public List<ApplyCheckVo> getApplyMsgList(Integer quarterId, Long facultyId, Long specialtyId, String keyword, String condition, Integer currentPage, Integer pageSize, Integer isCheck) {
        List<ApplyCheckVo> list = veZsRegistrationMapper.getApplyList(quarterId, facultyId, specialtyId, keyword, condition, currentPage, pageSize,isCheck);
        return list;
    }

    /**获取报名信息管理业务实现类*/
    @Override
    public List<VeZsRegistrationVo> getApplyMsgMngList(Integer quarterId, Long facultyId, Long specialtyId, String keyword, String condition, Integer currentPage, Integer pageSize, Integer isAdmit) {
        List<VeZsRegistrationVo> list = veZsRegistrationMapper.getApplyMngList(quarterId, facultyId, specialtyId, keyword, condition, currentPage, pageSize,isAdmit);
        return list;
    }

    /**获取统计结果*/
    @Override
    public Integer getReportStatisticsNum(Integer quarterId, Long facultyId, Long clazzId, Integer isReport, String time,Long specId) {
        if (time.equals("")){
            time = null;
        }
        Integer reportStatisticsNum = veZsRegistrationMapper.getReportStatisticsNum(quarterId,facultyId,clazzId,isReport,time,specId);
        return reportStatisticsNum;
    }

    //导入excel表格--李少君
    @Override
    @Transactional(rollbackFor = Exception.class)
    public void excelImport(List<ApplyCheckExcelVo> list) throws Exception {

        //字典
        Map<String,Integer> dictMap = new HashMap<>() ;
        dictMap.put("未审核",0);
        dictMap.put("已审核",1);
        dictMap.put("待审核",2);
        dictMap.put("未通过",3);

        int t = 5;
        try{
            for ( ApplyCheckExcelVo ve : list) {
                VeZsRegistration v = new VeZsRegistration();
                BeanUtils.copyProperties(ve, v);
                v.setXm(ve.getXm()).setXbm(ve.getXbm()).setSfzh(ve.getSfzh()).setByxx(ve.getByxx())
                        .setZkzh(ve.getZkzh()).setKsh(ve.getKsh()).setKszf(ve.getKszf()).setZymc(ve.getZymc())
                        .setIsCheck(dictMap.getOrDefault(ve.getIsCheck(),0));
                veZsRegistrationMapper.insert(v);
                t++;
            }


        }catch (Exception e){
            throw new Exception("第"+t+"行数据有误！") ;
        }

    }

    @Override
    public ApplyCheckVo selectByXmKshSfzh(String XM, String KSH, String SFZH) {
        return veZsRegistrationMapper.selectByXmKshSfzh(XM, KSH, SFZH);
    }

}
