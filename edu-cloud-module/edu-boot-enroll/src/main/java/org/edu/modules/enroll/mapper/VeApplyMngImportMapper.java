package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.VeZsRegistration;
import org.edu.modules.enroll.vo.ApplyMngImportVo;


import java.util.List;

public interface VeApplyMngImportMapper extends BaseMapper<VeZsRegistration> {


    /**
     *获取报名信息管理名单集合（导入）
     * @return 报名信息集合（已审核）
     */
    List<ApplyMngImportVo> getApplyMngList();


}
