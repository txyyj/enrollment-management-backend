package org.edu.modules.enroll.entity;

import lombok.Data;
import lombok.experimental.Accessors;


@Data
@Accessors(chain = true)
public class BasicResponseBO<T> {

    T result;

    Integer code;

    Boolean success;

    String message ;

    Long timestamp ;


}
