package org.edu.modules.enroll.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.vo.LoginUser;
import org.edu.modules.enroll.entity.VeDictQuarter;
import org.edu.modules.enroll.entity.VeZsRegistration;
import org.edu.modules.enroll.service.VeDictQuarterService;
import org.edu.modules.enroll.service.VeZsRegistrationService;
import org.edu.modules.enroll.vo.ApplyCheckExcelVo;
import org.edu.modules.enroll.vo.ApplyCheckVo;
import org.edu.modules.enroll.vo.VeZsFenbanExcelVo;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Description:  网上报名审核业务控制
 * @Author:  wcj
 * @Date:  2021-04-20
 * @Version:  V2.0
 *
 */
@Api(tags="网上报名审核")
@RestController
@RequestMapping("enroll/ApplyCheck")
@Slf4j
public class ApplyCheckController {

    @Resource
    private VeZsRegistrationService veZsRegistrationService;


    @Resource
    private VeDictQuarterService veDictQuarterService;

    @ApiOperation(value="获取报名信息集合")
    @PostMapping(value = "/applyMsg")
    public Result<?> getApplyMsgList(@ApiParam("招生季ID") @RequestParam("quarterId") Integer quarterId,
                                     @ApiParam("专业部ID") @RequestParam("facultyId") Long facultyId,
                                     @ApiParam("专业ID") @RequestParam("specialtyId") Long specialtyId,
                                     @ApiParam("关键词") @RequestParam("keyword") String keyword,
                                     @ApiParam("查询条件") @RequestParam("condit") String condit,
                                     @ApiParam("当前页") @RequestParam("currentPage") Integer currentPage,
                                     @ApiParam("条数") @RequestParam("pageSize") Integer pageSize,
                                     @ApiParam("是否审核") @RequestParam("isCheck") Integer isCheck) {


        //获取当前招生季
        if(quarterId == 0){
            QueryWrapper<VeDictQuarter> wrapper = new QueryWrapper<VeDictQuarter>().eq("isCur", 1);
            VeDictQuarter one = veDictQuarterService.getOne(wrapper);
            if(one==null){
                return Result.error("列表加载失败：未启用当前招生季");
            }
            quarterId = one.getId();
        }
        if(facultyId == 0){
            facultyId = null;
        }
        if(specialtyId == 0){
            specialtyId = null;
        }
        if(isCheck == 2){
            isCheck = null;
        }

        Integer current = (currentPage-1)*pageSize;
               List<ApplyCheckVo> list = veZsRegistrationService.getApplyMsgList(quarterId, facultyId, specialtyId, keyword, condit, current, pageSize,isCheck);

        //条件构造器
        QueryWrapper<VeZsRegistration> wrapper = new QueryWrapper<>();

        if(isCheck != null){
            wrapper.eq("isCheck",isCheck);
        }
        if(quarterId != null){
            wrapper.eq("ZSJ",quarterId);
        }

        if(facultyId != null){
            wrapper.eq("falId",facultyId);
        }

        if(specialtyId != null){
            wrapper.eq("specId",specialtyId);
        }

        if(condit != null && keyword != null && (!keyword.trim().equals(""))){
            wrapper.like(condit,keyword);
        }

        //查询总数
        Integer count = veZsRegistrationService.list(wrapper).size();
        //返回前端搜索结果
        HashMap<String,Object> map = new HashMap<>();
        map.put("list",list);
        map.put("count",count);

        return Result.OK(map);
    }



    @ApiOperation(value="批量审核")
    @PostMapping(value = "/batchCheck")
    public Result<?> batchReport(@ApiParam("拼接的ID字符串") @RequestParam(value = "ids") String ids){

        //切割字符串
        String[] split = ids.split(",");
        List<VeZsRegistration> list = new ArrayList<>();
        for(String sp : split){

            Long id = Long.parseLong(sp);
            VeZsRegistration veZsRegistration = new VeZsRegistration();
            veZsRegistration.setId(id);
            //更改状态为待审核--李少君
            veZsRegistration.setIsCheck(1);//李少君将1改为2已审核
            list.add(veZsRegistration);

        }
        veZsRegistrationService.updateBatchById(list);

        return Result.OK("批量审核成功！");

    }

    //李少君
    @ApiOperation(value="批量通过")
    @PostMapping(value = "/batchPass")
    public Result<?> batchPass(@ApiParam("拼接的ID字符串") @RequestParam(value = "ids") String ids){

        //切割字符串
        String[] split = ids.split(",");
        List<VeZsRegistration> list = new ArrayList<>();
        for(String sp : split){

            Long id = Long.parseLong(sp);
            VeZsRegistration veZsRegistration = new VeZsRegistration();
            veZsRegistration.setId(id);
            //更改状态为已审核
            veZsRegistration.setIsCheck(2);//通过
            list.add(veZsRegistration);

        }
        veZsRegistrationService.updateBatchById(list);

        return Result.OK("批量通过成功！");

    }

    //李少君
    @ApiOperation(value="批量不通过")
    @PostMapping(value = "/batchNoPass")
    public Result<?> batchNoPass(@ApiParam("拼接的ID字符串") @RequestParam(value = "ids") String ids){

        //切割字符串
        String[] split = ids.split(",");
        List<VeZsRegistration> list = new ArrayList<>();
        for(String sp : split){

            Long id = Long.parseLong(sp);
            VeZsRegistration veZsRegistration = new VeZsRegistration();
            veZsRegistration.setId(id);
            //更改状态为不通过
            veZsRegistration.setIsCheck(3);//未通过
            list.add(veZsRegistration);

        }
        veZsRegistrationService.updateBatchById(list);

        return Result.OK("批量不通过成功！");

    }

    //李少君
    @AutoLog(value = "网上报名信息导入excel表格")
    @ApiOperation(value = "网上报名信息导入excel表格", notes = "网上报名审核—网上报名信息导入excel表格")
    @PostMapping(value = "importShenhe")
    public Result<?> excelImport(MultipartFile file) {
        ImportParams params = new ImportParams();
        params.setTitleRows(2);
        params.setHeadRows(1);
        params.setNeedSave(true);
        try {
            List<ApplyCheckExcelVo> list = ExcelImportUtil.importExcel(file.getInputStream(), ApplyCheckExcelVo.class, params);

            veZsRegistrationService.excelImport(list);
            return Result.OK();
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("文件导入失败:" + e.getMessage());
        } finally {
            try {
                file.getInputStream().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    //李少君
    @AutoLog(value = "网上报名信息导出excel表格")
    @ApiOperation(value = "网上报名信息导出excel表格", notes = "网上报名审核—网上报名信息导出excel表格")
    @RequestMapping(value = "exportShenhe")
    public ModelAndView excelExport(
            @ApiParam("招生季ID") @RequestParam("quarterId") Integer quarterId,
            @ApiParam("专业部ID") @RequestParam("facultyId") Long facultyId,
            @ApiParam("专业ID") @RequestParam("specialtyId") Long specialtyId,
            @ApiParam("关键词") @RequestParam("keyword") String keyword,
            @ApiParam("查询条件") @RequestParam("condit") String condit,
            @ApiParam("当前页") @RequestParam("currentPage") Integer currentPage,
            @ApiParam("条数") @RequestParam("pageSize") Integer pageSize,
            @ApiParam("是否审核") @RequestParam("isCheck") Integer isCheck
    ){
        //获取当前招生季
//        if(quarterId == 0){
//            QueryWrapper<VeDictQuarter> wrapper = new QueryWrapper<VeDictQuarter>().eq("isCur", 1);
//            VeDictQuarter one = veDictQuarterService.getOne(wrapper);
//            if(one==null){
//                return Result.error("列表加载失败：未启用当前招生季");
//            }
//            quarterId = one.getId();
//        }
        if(facultyId == 0){
            facultyId = null;
        }
        if(specialtyId == 0){
            specialtyId = null;
        }
        if(isCheck == 2){
            isCheck = null;
        }

        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        Integer current = (currentPage-1)*pageSize;
        List<ApplyCheckVo> list = veZsRegistrationService.getApplyMsgList(quarterId, facultyId, specialtyId, keyword, condit, current, pageSize,isCheck);
        List<ApplyCheckExcelVo> excelList = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            ApplyCheckExcelVo vo = new ApplyCheckExcelVo();
            vo.setId(list.get(i).getId());
            vo.setXm(list.get(i).getXm());
            vo.setXbm(list.get(i).getXbm());
            vo.setSfzh(list.get(i).getSfzh());
            vo.setByxx(list.get(i).getByxx());
            vo.setZkzh(list.get(i).getZkzh());
            vo.setKsh(list.get(i).getKsh());
            vo.setKszf(list.get(i).getKszf());
            vo.setZymc(list.get(i).getZymc());
            int isCheck1 = list.get(i).getIsCheck();
            String state = null;
            if(isCheck1==0){
                state = "未审核";
            }else if(isCheck1==1){
                state = "已审核";
            }
            else if(isCheck1==2){
                state = "待审核";
            }else if(isCheck1==3){
                state = "不通过";
            }
            vo.setIsCheck(state);
            excelList.add(vo);

        }

        mv.addObject(NormalExcelConstants.FILE_NAME, "网上报名审核信息");
        mv.addObject(NormalExcelConstants.CLASS, VeZsFenbanExcelVo.class);
        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ExportParams exportParams = new ExportParams("网上报名审核信息", "导出人:" + user.getRealname(), "导出信息");
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, excelList);
        return mv;

    }


}
