package org.edu.modules.enroll.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/** 安排入住的寝室
 * @Auther 李少君
 * @Date 2021-07-26 14:33
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsArrangeSusheVo implements Serializable {

    /** id */
    @ApiModelProperty(value = "id")
    private Long id;

    /** 楼栋 */
    @ApiModelProperty(value = "楼栋")
    @TableField(value = "jzwmc")
    private String jzwmc;

    /** 楼层号 */
    @ApiModelProperty(value = "楼层号")
    @TableField(value = "lch")
    private Integer lch;

    /** 宿舍类型（几人间的） */
    @ApiModelProperty(value = "宿舍类型")
    @TableField(value = "kzrs")
    private Integer kzrs;

    /** 宿舍号 */
    @ApiModelProperty(value = "宿舍号")
    @TableField(value = "fjbm")
    private String fjbm;

    /** 入住性别 */
    @ApiModelProperty(value = "入住性别")
    @TableField(value = "rzxb")
    private Integer rzxb;

    /** 总床位数 */
    @ApiModelProperty(value = "总床位数")
    @TableField(value = "zcws")
    private Integer zcws;

    /** 已入住人数 */
    @ApiModelProperty(value = "已入住人数")
    @TableField(value = "yzrs")
    private Integer yzrs;

    /** 空床位数 */
    @ApiModelProperty(value = "空床位数")
    @TableField(value = "kcws")
    private Integer kcws;


}
