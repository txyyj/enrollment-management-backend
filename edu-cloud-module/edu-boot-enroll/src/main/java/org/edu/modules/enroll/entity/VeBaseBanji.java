package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description:  班级表
 * @Author:  yhx
 * @Date:  2021-04-13
 * @Version:  V1.0
 */

@Data
@TableName(value = "ve_base_banji",schema = "edu_dev")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class  VeBaseBanji implements Serializable {

    /** id */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /** 行政班代码 */
    @Excel(name = "行政班代码", width = 15)
    @ApiModelProperty(value = "行政班代码")
    @TableField(value = "XZBDM")
    private String xzbdm;

    /** 行政班名称 */
    @Excel(name = "行政班名称", width = 15)
    @ApiModelProperty(value = "行政班名称")
    @TableField(value="XZBMC")
    private String  xzbmc;

    /** 年级ID */
    @Excel(name = "年级ID", width = 15)
    @ApiModelProperty(value = "年级ID")
    @TableField(value="grade_id")
    private Long gradeId;

    /** 专业ID */
    @Excel(name = "专业ID", width = 15)
    @ApiModelProperty(value = "专业ID")
    @TableField(value="spec_id")
    private Long specId;

    /** 男生人数 */
    @Excel(name = "男生人数", width = 15)
    @ApiModelProperty(value = "男生人数")
    @TableField(value="NANSRS")
    private Long nansrs;

    /** 女生人数 */
    @Excel(name = "女生人数", width = 15)
    @ApiModelProperty(value = "女生人数")
    @TableField(value="NVSRS")
    private Long nvsrs;

    /** 总人数 */
    @Excel(name = "总人数", width = 15)
    @ApiModelProperty(value = "总人数")
    @TableField(value="ZRS")
    private Long zrs;

    @ApiModelProperty(value = "入学年份")
    @TableField(value="RXNF")
    private String rxnf;

    @ApiModelProperty(value = "班主任id")
//    private Long bzrUserId;
    @TableField(value="bzr_user_id")
    private String  bzrUserId;
}
