package org.edu.modules.enroll.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.vo.LoginUser;
import org.edu.modules.enroll.entity.VeDormStudent;
import org.edu.modules.enroll.entity.VeDormSusheEntity;
import org.edu.modules.enroll.entity.VeZsFenban;
import org.edu.modules.enroll.service.VeDormStudentService;
import org.edu.modules.enroll.service.VeDormSusheService;
import org.edu.modules.enroll.service.VeFenSusheService;
import org.edu.modules.enroll.vo.VeDormSushe;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.WeakHashMap;

/**
 * @Auther 李少君
 * @Date 2021-07-26 9:59
 */
@Api(tags="预分宿舍")
@RestController
@RequestMapping("enroll/fenSushe")
@Slf4j
public class VeZsFenSusheController {
    @Autowired
    private VeFenSusheService veFenSusheService;

    //显示安排床位班级的表格
    @AutoLog(value = "预分宿舍-安排床位班级显示表格")
    @ApiOperation(value="安排床位班级显示表格", notes="预分宿舍-安排床位班级显示表格")
    @PostMapping("/getArrangeClassList")
    public Result<?> getArrangeClassList(
            @ApiParam("年级id") @RequestParam("gradeId") Integer gradeId ,
            @ApiParam("院系id") @RequestParam("falId") Integer falId,
            @ApiParam("班级id") @RequestParam("banjiId") Integer banjiId,
            @ApiParam("当前页数") @RequestParam("currentPage") Integer currentPage,
            @ApiParam("分页大小") @RequestParam("pageSize") Integer pageSize
    ){
        HashMap<String, Object> map = veFenSusheService.getPlanBedList(gradeId, falId, banjiId, currentPage, pageSize);
        return Result.OK(map);
    }

    //显示选择安排入住的寝室表格
    @AutoLog(value = "预分宿舍-安排入住寝室显示表格")
    @ApiOperation(value="安排入住寝室显示表格", notes="预分宿舍-安排入住寝室显示表格")
    @PostMapping("/getArrangeSusheList")
    public Result<?> getArrangeSusheList(
            @RequestParam("jzwmc") Integer jzwmc,
            @RequestParam("lch") Integer lch,
            @RequestParam("kzrs") Integer kzrs,
            @RequestParam("ssrs") Integer ssrs,
            @RequestParam("sfwk") Integer sfwk,
            @RequestParam("xb") Integer xb,
            @RequestParam("fjbm") String fjbm,
            @RequestParam("currentPage") Integer currentPage,
            @RequestParam("pageSize") Integer pageSize
    ){
        HashMap<String, Object> map = veFenSusheService.getArrangeSusheList(jzwmc, lch, fjbm,sfwk,xb, currentPage, pageSize);
        return Result.OK(map);
    }

    //确认班级分配宿舍
    @AutoLog(value = "预分宿舍-确认安排宿舍")
    @ApiOperation(value="确认安排宿舍", notes="预分宿舍-确认安排宿舍")
    @PostMapping("/confirmArrangeSushe")
    public Result<?> confirmFenSushe(
            String susheId,//宿舍id字符串
            String banjiId //班级id字符串

    ){
        //切割字符串
        String[] split1 = banjiId.split(",");
        String[] split2 = susheId.split(",");

        for(String sp1 : split1){
            int id1 = Integer.parseInt(sp1); //班级id
            List<VeZsFenban> list = veFenSusheService.getStudent(id1);//该班级的学生信息
            if(list.size()==0){
                return Result.error("该班级没有学生");
            }
            int stu = list.size();

            for(int i = 0;i<split2.length;i++){
                if (split2[i] != null) {
                    VeDormSushe v = veFenSusheService.getSusheMessage(Integer.valueOf(split2[i]));
                    int kcws = v.getKzrs()-v.getYzrs();//该宿舍的空床位数
                    for (int j = 0;j<kcws;j++){
                        if(stu==0){
                            i = split2.length;
                            break;
                        }
                        Long a = list.get(stu-1).getId();
                        Integer b = v.getId().intValue();
                        veFenSusheService.fenpeiSushe(a,b);

                        stu--;
                    }
                }
            }

        }
        return Result.OK("分配宿舍成功");
    }



}
