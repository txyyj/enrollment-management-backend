package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;


/**
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.entity
 * @date 2021/7/19 14:08
 */
@Data
@TableName("ve_zs_report_msg")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsReportMsg implements Serializable {
    /** id */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Integer id;
    /** stuName */
    @ApiModelProperty(value = "学生名字")
    private String stuName;

    /** stuID */
    @ApiModelProperty(value = "身份证号")
    @TableField(value = "stu_ID")
    private String stuID;
    /** startTime */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "出发时间")
    private Date startTime;
    /** endTime */
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", timezone="GMT+8")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "到达时间")
    private Date endTime;
    /** startPlace */
    @ApiModelProperty(value = "出发地点")
    private String startPlace;

    /** province */
    @ApiModelProperty(value = "省份id")
    private Integer provinceId;
    /** startPlaceId */
    @ApiModelProperty(value = "出发地点id")
    private Integer startPlaceId;

    /** endPlace */
    @ApiModelProperty(value = "到达地点")
    private String endPlace;

    /** endPlaceId */
    @ApiModelProperty(value = "到达地点id")
    private String endPlaceId;

    /** isSchool */
    @ApiModelProperty(value = "是否需要校车接送，1不要，2要")
    private Integer isSchool ;

    /** accompanyNumber */
    @ApiModelProperty(value = "随行人数")
    private Integer accompanyNumber;
    /** vehicleName */
    @ApiModelProperty(value = "搭乘的交通工具")
    private String vehicleName;
    /** vehicleId */
    @ApiModelProperty(value = "搭乘的交通工具id")
    private String vehicleId;

    /** vehicleCode */
    @ApiModelProperty(value = "交通工具信息")
    private String vehicleCode;
    /** creatTime */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;

    /** updateTime */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time",update = "now()")
    private Date updateTime;

}
