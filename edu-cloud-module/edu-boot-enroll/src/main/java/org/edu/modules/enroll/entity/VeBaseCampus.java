package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Auther 李少君
 * @Date 2021-07-25 15:33
 */
@Data
@TableName(value = "ve_base_campus",schema = "edu_dev")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeBaseCampus {

    @ApiModelProperty(value = "id")
    private Long id;

    @TableField(value = "XQDM")
    private String xqdm;
    @TableField(value = "XQMC")
    private String xqmc;
    @TableField(value = "XQDZ")
    private String xqdz;
    @TableField(value = "XQLXDH")
    private String xqlxdh;
    @TableField(value = "XQCZDH")
    private String xqczdh;
    @TableField(value = "fzr_user_id")
    private String fzrUserId;
    @TableField(value = "DZYJ")
    private String dzyj;
    @TableField(value = "XQYZBM")
    private String xqyzbm;
    @TableField(value = "XQSZDXZQHM")
    private String xqszdxzqhm;
    @TableField(value = "XQMJ")
    private Double xqmj;
    @TableField(value = "XQJZMJ")
    private Double xqjzmj;
    @TableField(value = "XQJXKYSBZZ")
    private Double xqjxkysbzz;
    @TableField(value = "XQGDZCZZ")
    private Double xqgdzczz;
    @TableField(value = "terminalid")
    private Long terminalId;
    @TableField(value = "fileid")
    private Long fileId;

}
