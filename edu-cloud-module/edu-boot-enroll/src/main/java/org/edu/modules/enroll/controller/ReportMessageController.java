package org.edu.modules.enroll.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.sun.org.apache.regexp.internal.RE;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.modules.enroll.entity.*;
import org.edu.modules.enroll.service.*;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;


/**
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.controller
 * @date 2021/7/19 14:29
 */
@Api(tags="新生报到信息")
@RestController
@RequestMapping("enroll/reportMessage")
@Slf4j
public class ReportMessageController {
    @Resource
    private VeZsReportMessageService veZsReportMessageService;
    @Resource
    private SysDictService sysDictService;
    @Resource
    private SysDictItemService sysDictItemService;
    @Resource
    private VeDictAreaService veDictAreaService;
    @ApiOperation("添加新生报道信息")
    @PostMapping(value = "/addReportMsg")
    public Result<?> addReportMsg(VeZsReportMsg veZsReportMsg){

        //数据校验
        //身份证号唯一
        QueryWrapper<VeZsReportMsg> wrapper = new QueryWrapper<>();
        wrapper.eq("SFZH",veZsReportMsg.getStuID());
        VeZsReportMsg vreportMsg= veZsReportMessageService.getOne(wrapper);
        if(vreportMsg!=null){
            return Result.error("添加失败，身份证号已存在");
        }else{
            //创建数据库对象
            VeZsReportMsg ve=new VeZsReportMsg();
            ve.setAccompanyNumber(veZsReportMsg.getAccompanyNumber());
            ve.setEndPlace(veZsReportMsg.getEndPlace());
            ve.setEndTime(veZsReportMsg.getEndTime());
//            ve.setId(veZsReportMsg.getId());
            ve.setStuID(veZsReportMsg.getStuID());
            ve.setIsSchool(veZsReportMsg.getIsSchool());
            ve.setStartPlace(veZsReportMsg.getStartPlace());
            ve.setStartTime(veZsReportMsg.getStartTime());
            ve.setStuName(veZsReportMsg.getStuName());
            ve.setVehicleCode(veZsReportMsg.getVehicleCode());
            ve.setVehicleName(veZsReportMsg.getVehicleName());
            ve.setVehicleId(veZsReportMsg.getVehicleId());
            ve.setEndPlaceId(veZsReportMsg.getEndPlaceId());
            ve.setStartPlaceId(veZsReportMsg.getStartPlaceId());
            //添加数据
            boolean result= veZsReportMessageService.save(ve);
            if(result){
                return Result.OK("添加成功");
            }
            return Result.error("添加数据失败");
        }
    }

    @ApiOperation("查询新生报道信息")
    @PostMapping(value = "/selectReportMsg")
    public Result<?> getReportMsg(@RequestParam(value ="endPlaceId")String endPlaceId,@RequestParam(value ="staTime")String staTime,
                                  @RequestParam(value ="endTime")String endTime,@RequestParam(value ="isNeed")Integer isNeed,@ApiParam("当前页") @RequestParam("currentPage")Integer currentPage,
                                  @ApiParam("条数") @RequestParam("pageSize")Integer pageSize){
        if (endPlaceId.equals("")||endPlaceId.equals("0")){
            endPlaceId = null;
        }
        if (staTime.equals("1970-01-01 00:00:00")||staTime.equals("")){
            staTime = null;
        }
        if (endTime.equals("1970-01-01 00:00:00")||endTime.equals("")){
            endTime = null;
        }
        if (isNeed==0){
            isNeed = null;
        }
        //页码条件
        Integer current = (currentPage-1)*pageSize;
        List<VeZsReportMsg> list = veZsReportMessageService.getReportMessageList(endPlaceId,staTime,endTime,isNeed,current,pageSize);
        for (VeZsReportMsg v:list){
//            QueryWrapper<SysDict> wrapper1 = new QueryWrapper<>();
//            wrapper1.eq("dict_code","vehicle_type");
//            SysDict sysDict = sysDictService.getOne(wrapper1);

            QueryWrapper<SysDictItem> vehicleWrapper = new QueryWrapper<>();
            QueryWrapper<SysDictItem> endPlaceWrapper = new QueryWrapper<>();
            vehicleWrapper.eq("id",v.getVehicleId());
            endPlaceWrapper.eq("id",v.getEndPlaceId());
            SysDictItem vehicle = sysDictItemService.getOne(vehicleWrapper);
            SysDictItem endplace = sysDictItemService.getOne(endPlaceWrapper);
            if (vehicle != null){
                v.setVehicleName(vehicle.getItemText());
            }else {
                v.setVehicleName("查无搭乘的交通工具");
            }
            if (endplace != null){
                v.setEndPlace(endplace.getItemText());
            }else {
                v.setEndPlace("查无到达地点");
            }


        }

        Integer count = veZsReportMessageService.countReportMessage(endPlaceId,staTime,endTime,isNeed);

        HashMap<String,Object> map = new HashMap<>();
        map.put("list",list);
        map.put("count",count);
        return Result.OK(map);

    }

    @ApiOperation("按id查询新生报道信息")
    @PostMapping(value = "/selectReportMsgById")
    public Result<?> getReportMsgById(@RequestParam(value ="id")Integer id){
        QueryWrapper<VeZsReportMsg> wrapper = new QueryWrapper<>();
        wrapper.eq("id",id);
        VeZsReportMsg veZsReportMsg = veZsReportMessageService.getOne(wrapper);
//        添加省份id
        VeDictArea area = veDictAreaService.getById(veZsReportMsg.getStartPlaceId());
        if (area != null){
            veZsReportMsg.setProvinceId(area.getPid());
        }else {
            veZsReportMsg.setProvinceId(0);
        }

        return Result.OK(veZsReportMsg);
    }

//    @ApiOperation(value="批量删除")
//    @PostMapping(value = "/deleteMsgs")
//    public Result<?> deleteMsg(@ApiParam("拼接的ID字符串") @RequestParam(value = "ids") String ids){
////pius自带的批量删除
//        boolean b = veZsReportMessageService.removeByIds(Arrays.asList(ids.split(",")));
//        System.out.println(b);
//        return Result.OK("删除成功！");
//
//    }

    //删除
    @ApiOperation(value = "删除")
    @PostMapping("/delete")
    //传到前端的参数路径
    public Result<?>  sczsjh(@RequestParam(value = "key") Integer id) {
        if (veZsReportMessageService.deletedById(id) != 1) {
            return Result.OK("删除失败");
        }
        //发送请求到前端
        return Result.OK("删除成功");

    }
    @ApiOperation(value="保存数据增加")
    @PostMapping(value = "/save")
    public Result<?> saveMsg(@RequestBody VeZsReportMsg veZsReportMsg){
        System.out.println(veZsReportMsg.getEndPlaceId()+"endplace");
        //数据校验
        //身份证号唯一
        QueryWrapper<VeZsReportMsg> wrapper = new QueryWrapper<>();
        wrapper.eq("stu_ID",veZsReportMsg.getStuID());
        VeZsReportMsg vreportMsg= veZsReportMessageService.getOne(wrapper);
        if(vreportMsg!=null){
            return Result.error("添加失败，身份证号已存在");
        }
        //添加交通工具名称，到达地点名称
        //交通工具信息获取
        if (veZsReportMsg.getVehicleId() != null&& !veZsReportMsg.getVehicleId().equals("0")){
            QueryWrapper<SysDictItem> vehicleWrapper = new QueryWrapper<>();
            vehicleWrapper.eq("id",veZsReportMsg.getVehicleId());
            SysDictItem vehicle = sysDictItemService.getOne(vehicleWrapper);
            veZsReportMsg.setVehicleName(vehicle.getItemText());
        }

        QueryWrapper<SysDictItem> endPlaceWrapper = new QueryWrapper<>();
        endPlaceWrapper.eq("id",veZsReportMsg.getEndPlaceId());

        SysDictItem endplace = sysDictItemService.getOne(endPlaceWrapper);
        veZsReportMsg.setEndPlace(endplace.getItemText());

        //添加出发地点名称
        if (veZsReportMsg.getStartPlaceId()!=null){
            VeDictArea area = veDictAreaService.getById(veZsReportMsg.getStartPlaceId());
            veZsReportMsg.setStartPlace(area.getName());
            //添加省份id
            veZsReportMsg.setProvinceId((area.getPid()));
        }


        boolean res = veZsReportMessageService.save(veZsReportMsg);
        if (res==true){
            return Result.OK("添加成功");
        }else {
            return Result.error("添加失败");
        }

    }
    @ApiOperation(value="数据修改")
    @PostMapping(value = "/update")
    public Result<?> updateMsg(@RequestBody VeZsReportMsg veZsReportMsg){
        //添加交通工具名称，到达地点名称
        QueryWrapper<SysDictItem> vehicleWrapper = new QueryWrapper<>();
        QueryWrapper<SysDictItem> endPlaceWrapper = new QueryWrapper<>();
        vehicleWrapper.eq("id",veZsReportMsg.getVehicleId());
        endPlaceWrapper.eq("id",veZsReportMsg.getEndPlaceId());
        SysDictItem vehicle = sysDictItemService.getOne(vehicleWrapper);
        SysDictItem endplace = sysDictItemService.getOne(endPlaceWrapper);
        veZsReportMsg.setEndPlace(endplace.getItemText());
        veZsReportMsg.setVehicleName(vehicle.getItemText());
        //添加出发地点名称
//        VeDictArea area = veDictAreaService.getById(veZsReportMsg.getStartPlaceId());
        veZsReportMsg.setStartPlace(veZsReportMsg.getStartPlace());
        //添加省份id
//        veZsReportMsg.setProvinceId((area.getPid()));
        QueryWrapper<VeZsReportMsg> wrapper = new QueryWrapper<>();
        wrapper.eq("id",veZsReportMsg.getId());
        boolean res = veZsReportMessageService.update(veZsReportMsg,wrapper);

        if (res==true){
            return Result.OK("修改成功");
        }else {
            return Result.error("修改失败");
        }

    }
}
