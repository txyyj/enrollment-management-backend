package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/7/24 9:47
 */
@Data
@TableName("ve_zs_enroll")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsEnroll implements Serializable {

    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "考生号")
    @TableField(value = "KSH")
    private String KSH;

    @ApiModelProperty(value = "姓名")
    @TableField(value = "XM")
    private String XM;
    @TableField(value = "XN")
    @ApiModelProperty(value = "学年")
    private int XN;
    @TableField(value = "XQ")
    @ApiModelProperty(value = "学期")
    private String XQ;
    @TableField(value = "SFZH")
    @ApiModelProperty(value = "身份证号")
    private String SFZH;
    @TableField(value = "ZXMC")
    @ApiModelProperty(value = "中学名称")
    private String ZXMC;
    @TableField(value = "ZF")
    @ApiModelProperty(value = "总分")
    private Long ZF;
    @TableField(value = "KM1")
    @ApiModelProperty(value = "科目一")
    private Long KM1;
    @TableField(value = "KM2")
    @ApiModelProperty(value = "科目二")
    private Long KM2;
    @TableField(value = "KM3")
    @ApiModelProperty(value = "科目三")
    private Long KM3;
    @TableField(value = "LQBS")
    @ApiModelProperty(value = "录取标识")
    private int LQBS;
    @TableField(value = "major")
    @ApiModelProperty(value = "拟录取专业")
    private int major;
    @TableField(value = "prepmajor")
    @ApiModelProperty(value = "拟录取专业")
    private int prepmajor;
    @TableField(value = "formalmajor")
    @ApiModelProperty(value = "正式录取专业")
    private int formalmajor;
    @TableField(value = "faculty")
    @ApiModelProperty(value = "院系")
    private int faculty;
    @TableField(value = "ZKZH")
    @ApiModelProperty(value = "准考证号")
    private String ZKZH;
    @TableField(value = "PM")
    @ApiModelProperty(value = "排名")
    private int PM;
    @TableField(value = "create_time")
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    @TableField(value = "create_by")
    @ApiModelProperty(value = "创建用户ID")
    private int createBy;
    @TableField(value = "update_time")
    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
    @TableField(value = "update_by")
    @ApiModelProperty(value = "更新用户ID")
    private int updateBy;
    @TableField(value = "terminalId")
    @ApiModelProperty(value = "终端id")
    private int terminalId;
    @TableField(value = "is_deleted")
    @ApiModelProperty(value = "逻辑删除")
    private int isDeleted;

}
