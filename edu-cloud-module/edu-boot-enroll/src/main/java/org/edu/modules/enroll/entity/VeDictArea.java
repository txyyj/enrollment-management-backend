package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description:  地区表对象类接口
 * @Author:  wcj
 * @Date:  2021-04-13
 * @Version:  V1.0
 */

@Data
@TableName("ve_dict_area")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDictArea {


    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "地区id")
    private Long id;

    /** 姓名 */
    @Excel(name = "地区名字", width = 15)
    @ApiModelProperty(value = "地区名字")
    @TableField(value = "name")
    private String name;

    /** 上级路径ID*/
    @Excel(name = "上级路径ID", width = 15)
    @ApiModelProperty(value = "上级路径ID")
    @TableField(value = "pid")
    private Integer pid;

    /** 顺序*/
    @Excel(name = "顺序", width = 15)
    @ApiModelProperty(value = "顺序")
    @TableField(value = "listSort")
    private Integer listSort;

}
