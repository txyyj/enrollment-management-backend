package org.edu.modules.enroll.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.edu.common.api.vo.Result;
import org.edu.modules.enroll.entity.VeDictQuarter;
import org.edu.modules.enroll.entity.VeZsRegistration;
import org.edu.modules.enroll.service.VeDictQuarterService;
import org.edu.modules.enroll.service.VeZsRegistrationService;
import org.edu.modules.enroll.vo.VeZsRegistrationVo;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * 已报到新生管理
 * @author yhx
 */
@Api(tags="已报到新生管理")
@RestController
@RequestMapping("enroll/reportedMng")
public class ReportedMngController {

    @Resource
    private VeZsRegistrationService veZsRegistrationService;

    @Resource
    private VeDictQuarterService veDictQuarterService;

    @ApiOperation(value="获取已报到新生集合")
    @PostMapping(value = "/selectReportedStu")
    public Result<?> getReportStuList(@ApiParam("招生季ID") @RequestParam("quarterId") Integer quarterId,
                                      @ApiParam("专业部ID") @RequestParam("facultyId") Long facultyId,
                                      @ApiParam("专业ID") @RequestParam("specialtyId") Long specialtyId,
                                      @ApiParam("关键词") @RequestParam("keyword") String keyword,
                                      @ApiParam("查询条件") @RequestParam("condit") String condit,
                                      @ApiParam("当前页") @RequestParam("currentPage") Integer currentPage,
                                      @ApiParam("条数") @RequestParam("pageSize") Integer pageSize) {

        if(quarterId == 0){
            QueryWrapper<VeDictQuarter> wrapper = new QueryWrapper<VeDictQuarter>().eq("isCur", 1);
            VeDictQuarter one = veDictQuarterService.getOne(wrapper);
            if(one == null){
                return Result.error("列表加载失败：当前招生季未启用！");
            }
            quarterId = one.getId();
        }
        if(facultyId == 0){
            facultyId = null;
        }
        if(specialtyId == 0){
            specialtyId = null;
        }

        //页码条件
        Integer current = (currentPage-1)*pageSize;
        //根据页码查询数据
        List<VeZsRegistrationVo> reportStuList = veZsRegistrationService.getReportedStuList(quarterId, facultyId, specialtyId, keyword, condit, current, pageSize);

        //条件构造器
        QueryWrapper<VeZsRegistration> wrapper = new QueryWrapper<>();
        wrapper.eq("isCheck",1);
        wrapper.eq("isAdmit",1);
        wrapper.eq("isReport",1);
        wrapper.eq("isNum",0);
        wrapper.isNotNull("classId");

        if(quarterId != null){
            wrapper.eq("ZSJ",quarterId);
        }

        if(facultyId != null){
            wrapper.eq("falId",facultyId);
        }

        if(specialtyId != null){
            wrapper.eq("specId",specialtyId);
        }

        if(condit != null && keyword != null && (!keyword.trim().equals(""))){
            wrapper.like(condit,keyword);
        }

        //查询总数
        Integer count = veZsRegistrationService.list(wrapper).size();

        HashMap<String,Object> map = new HashMap<>();
        map.put("list",reportStuList);
        map.put("count",count);

        return Result.OK(map);
    }

}
