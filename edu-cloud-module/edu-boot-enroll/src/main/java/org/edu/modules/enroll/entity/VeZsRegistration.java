package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description:  报名表
 * @Author:  yhx
 * @Date:  2021-04-05
 * @Version:  V1.0
 */

@Data
@TableName("ve_zs_registration")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsRegistration implements Serializable {

    /** id */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /** 报名号 */
    @Excel(name = "报名号", width = 15)
    @ApiModelProperty(value = "报名号，唯一")
    @TableField(value = "BMH")
    private String bmh;

    /** 姓名 */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    @TableField(value = "XM")
    private String xm;

    /** 曾用名 */
    @Excel(name = "曾用名", width = 15)
    @ApiModelProperty(value = "曾用名")
    @TableField(value = "CYM")
    private String cym;

    /** 身份证号 */
    @Excel(name = "身份证号", width = 15)
    @ApiModelProperty(value = "身份证号")
    @TableField(value = "SFZH")
    private String sfzh;

    /** 招生类型 */
    @Excel(name = "招生类型", width = 15)
    @ApiModelProperty(value = "招生类型，1统一招生，2自主招生")
    @TableField(value = "ZSLX")
    private Integer zslx;

    /** 考生号 */
    @Excel(name = "考生号", width = 15)
    @ApiModelProperty(value = "考生号")
    @TableField(value = "KSH")
    private String ksh;

    /** 准考证号 */
    @Excel(name = "准考证号", width = 15)
    @ApiModelProperty(value = "准考证号")
    @TableField(value = "ZKZH")
    private String zkzh;

    /** 考试总分 */
    @Excel(name = "考试总分", width = 15)
    @ApiModelProperty(value = "考试总分")
    @TableField(value = "KSZF")
    private Double kszf;

    /** 性别码 */
    @Excel(name = "性别码", width = 15)
    @ApiModelProperty(value = "性别码")
    @TableField(value = "XBM")
    private String xbm;

    /** 出生日期 */
    @Excel(name = "出生日期", width = 15)
    @ApiModelProperty(value = "出生日期")
    @TableField(value = "CSRQ")
    private String csrq;

    /** 籍贯 */
    @Excel(name = "籍贯", width = 15)
    @ApiModelProperty(value = "籍贯")
    @TableField(value = "JG")
    private String jg;

    /** 民族码 */
    @Excel(name = "民族码", width = 15)
    @ApiModelProperty(value = "民族码")
    @TableField(value = "MZM")
    private String mzm;

    /** 港澳台侨外码 */
    @Excel(name = "港澳台侨外码", width = 15)
    @ApiModelProperty(value = "港澳台侨外码")
    @TableField(value = "GATQWM")
    private String gatqwm;

    /** 健康状况码 */
    @Excel(name = "健康状况码", width = 15)
    @ApiModelProperty(value = "健康状况码")
    @TableField(value = "JKZKM")
    private String jkzkm;

    /** 政治面貌码 */
    @Excel(name = "政治面貌码", width = 15)
    @ApiModelProperty(value = "政治面貌码")
    @TableField(value = "ZZMMM")
    private String zzmmm;

    /** 招生渠道码 */
    @Excel(name = "招生渠道码", width = 15)
    @ApiModelProperty(value = "招生渠道码")
    @TableField(value = "ZSQDM")
    private String zsqdm;

    /** 学生联系电话 */
    @Excel(name = "学生联系电话", width = 15)
    @ApiModelProperty(value = "学生联系电话")
    @TableField(value = "XSLXDH")
    private String xslxdh;

    /** 是否是流动 */
    @Excel(name = "是否是流动", width = 15)
    @ApiModelProperty(value = "是否是流动")
    @TableField(value = "SFSLDRK")
    private String sfsldrk;

    /** 电子信箱 */
    @Excel(name = "电子信箱", width = 15)
    @ApiModelProperty(value = "电子信箱")
    @TableField(value = "DZXX")
    private String dzxx;

    /** 即时通讯号 */
    @Excel(name = "即时通讯号", width = 15)
    @ApiModelProperty(value = "即时通讯号")
    @TableField(value = "JSTXH")
    private String jstxh;

    /** 录取状态 */
    @Excel(name = "录取状态", width = 15)
    @ApiModelProperty(value = "录取状态0未录取1已录取")
    @TableField(value = "isAdmit")
    private Integer isAdmit;

    /** 是否报道 */
    @Excel(name = "是否报道", width = 15)
    @ApiModelProperty(value = "是否报道0否1是")
    @TableField(value = "isReport")
    private Integer isReport;

    /** 打印次数 */
    @Excel(name = "打印次数", width = 15)
    @ApiModelProperty(value = "打印次数")
    @TableField(value = "enrollNum")
    private Long enrollNum;

    /** 报名院系ID */
    @Excel(name = "报名院系ID", width = 15)
    @ApiModelProperty(value = "报名院系ID")
    @TableField(value = "falId")
    private Long falId;

    /** 院系名称 */
    @Excel(name = "院系名称", width = 15)
    @ApiModelProperty(value = "院系名称")
    @TableField(value = "YXMC")
    private String yxmc;

    /** 报名专业ID */
    @Excel(name = "报名专业ID", width = 15)
    @ApiModelProperty(value = "报名专业ID")
    @TableField(value = "specId")
    private Long specId;

    /** 专业名称 */
    @Excel(name = "专业名称", width = 15)
    @ApiModelProperty(value = "专业名称")
    @TableField(value = "ZYMC")
    private String zymc;

    /** 学制ID */
    @Excel(name = "学制ID", width = 15)
    @ApiModelProperty(value = "学制ID")
    @TableField(value = "XZ")
    private Long xz;

    /** 层次码 */
    @Excel(name = "层次码", width = 15)
    @ApiModelProperty(value = "层次码")
    @TableField(value = "CCM")
    private String ccm;

    /** 户口所在省份 */
    @Excel(name = "户口所在省份", width = 15)
    @ApiModelProperty(value = "户口所在省份")
    @TableField(value = "province")
    private String province;

    /** 省份ID */
    @Excel(name = "省份ID", width = 15)
    @ApiModelProperty(value = "省份ID")
    @TableField(value = "provinceId")
    private Long provinceId;

    /** 户口所在市 */
    @Excel(name = "户口所在市", width = 15)
    @ApiModelProperty(value = "户口所在市")
    @TableField(value = "city")
    private String city;

    /** 市ID */
    @Excel(name = "市ID", width = 15)
    @ApiModelProperty(value = "市ID")
    @TableField(value = "cityId")
    private Long cityId;

    /** 户口所在区 */
    @Excel(name = "户口所在区", width = 15)
    @ApiModelProperty(value = "户口所在区")
    @TableField(value = "county")
    private String county;

    /** 区ID */
    @Excel(name = "区ID", width = 15)
    @ApiModelProperty(value = "区ID")
    @TableField(value = "countyId")
    private Long countyId;

    /** 家庭地址 */
    @Excel(name = "家庭地址", width = 15)
    @ApiModelProperty(value = "家庭地址(详细通讯地址)")
    @TableField(value = "JTDZ")
    private String jtdz;

    /** 家庭邮编 */
    @Excel(name = "家庭邮编", width = 15)
    @ApiModelProperty(value = "家庭邮编")
    @TableField(value = "JTYB")
    private String jtyb;

    /** 家庭联系电话 */
    @Excel(name = "家庭联系电话", width = 15)
    @ApiModelProperty(value = "家庭联系电话")
    @TableField(value = "JTLXDH")
    private String jtlxdh;

    /** 特长 */
    @Excel(name = "特长", width = 15)
    @ApiModelProperty(value = "特长")
    @TableField(value = "TC")
    private String tc;

    /** 报名方式码 */
    @Excel(name = "报名方式码", width = 15)
    @ApiModelProperty(value = "报名方式码")
    @TableField(value = "BMFSM")
    private String bmfsm;

    /** 银行卡号 */
    @Excel(name = "银行卡号", width = 15)
    @ApiModelProperty(value = "银行卡号")
    @TableField(value = "YHKH")
    private String yhkh;

    /** 毕业学校 */
    @Excel(name = "毕业学校", width = 15)
    @ApiModelProperty(value = "毕业学校")
    @TableField(value = "BYXX")
    private String byxx;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 15)
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time")
    private Date createTime;

    /** 更新时间 */
    @Excel(name = "更新时间", width = 15)
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time")
    private Date updateTime;

    /** 终端系统ID */
    @Excel(name = "终端系统ID", width = 15)
    @ApiModelProperty(value = "终端系统ID")
    @TableField(value = "terminalId")
    private Long terminalId;

    /** 是否低保 */
    @Excel(name = "是否低保", width = 15)
    @ApiModelProperty(value = "是否低保0否1是")
    @TableField(value = "SFDB")
    private Integer sfdb;

    /** 入学年月 */
    @Excel(name = "入学年月", width = 15)
    @ApiModelProperty(value = "入学年月")
    @TableField(value = "RXNY")
    private String rxny;

    /** 年级ID */
    @Excel(name = "年级ID", width = 15)
    @ApiModelProperty(value = "年级ID")
    @TableField(value = "gradeId")
    private Long gradeId;

    /** 班级ID */
    @Excel(name = "班级ID", width = 15)
    @ApiModelProperty(value = "班级ID")
    @TableField(value = "classId")
    private Long classId;

    /** 入学年份 */
    @Excel(name = "入学年份", width = 15)
    @ApiModelProperty(value = "入学年份")
    @TableField(value = "RXNF")
    private String rxnf;

    /** 就读方式 */
    @Excel(name = "就读方式", width = 15)
    @ApiModelProperty(value = "就读方式：1住校，2走读")
    @TableField(value = "JDFS")
    private Integer jdfs;

    /** 申请报名时间 */
    @Excel(name = "申请报名时间", width = 15)
    @ApiModelProperty(value = "申请报名时间，学生在线报名有效")
    @TableField(value = "apply_time")
    private String applyTime;

    /** 招生季 */
    @Excel(name = "招生季", width = 15)
    @ApiModelProperty(value = "招生季")
    @TableField(value = "ZSJ")
    private Long zsj;

    /** 户口类别码 */
    @Excel(name = "户口类别码", width = 15)
    @ApiModelProperty(value = "户口类别码")
    @TableField(value = "HKLBM")
    private String hklbm;

    /** 二维码地址 */
    @Excel(name = "二维码地址", width = 15)
    @ApiModelProperty(value = "二维码地址")
    @TableField(value = "qrcodeUrl")
    private String qrcodeUrl;

    /** 逻辑删除的标识 */
    @Excel(name = "逻辑删除", width = 15)
    @ApiModelProperty(value = "逻辑删除")
    @TableField(value = "is_deleted")
    @TableLogic
    private Integer isDeleted;

    /** 审核的标识 */
    @Excel(name = "审核的标识", width = 15)
    @ApiModelProperty(value = "审核的标识")
    @TableField(value = "isCheck")
    private Integer isCheck;

}
