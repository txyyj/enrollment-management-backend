package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.VeZsPtemplet;


/**
 * 通知书模板表接口
 * @author wcj
 */
public interface VeZsPtempletMapper extends BaseMapper<VeZsPtemplet> {
}
