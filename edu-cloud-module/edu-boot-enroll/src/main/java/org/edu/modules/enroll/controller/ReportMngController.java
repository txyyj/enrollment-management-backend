package org.edu.modules.enroll.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.modules.enroll.entity.*;
import org.edu.modules.enroll.service.*;
import org.edu.modules.enroll.vo.VeZsRegistrationVo;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * 新生报到管理
 * @author yhx
 */
@Api(tags="新生报到管理")
@RestController
@RequestMapping("enroll/reportMng")
@Slf4j
public class ReportMngController {

    @Resource
    private VeZsRegistrationService veZsRegistrationService;

    @Resource
    private VeBaseFacultyService veBaseFacultyService;

    @Resource
    private VeDictQuarterService veDictQuarterService;

    @Resource
    private VeBaseSpecialtyService veBaseSpecialtyService;

    @Resource
    private SysDictItemService sysDictItemService;
    @Resource
    private IVeBaseManageService iVeBaseManageService;
    @Resource
    private VeBaseStudentService veBaseStudentService;
    @ApiOperation(value="获取新生报到集合")
    @PostMapping(value = "/select")
    public Result<?> getReportStuList(@ApiParam("招生季ID") @RequestParam("quarterId") Integer quarterId,
                                      @ApiParam("专业部ID") @RequestParam("facultyId") Long facultyId,
                                      @ApiParam("专业ID") @RequestParam("specialtyId") Long specialtyId,
                                      @ApiParam("关键词") @RequestParam("keyword") String keyword,
                                      @ApiParam("查询条件") @RequestParam("condit") String condit,
                                      @ApiParam("当前页") @RequestParam("currentPage") Integer currentPage,
                                      @ApiParam("条数") @RequestParam("pageSize") Integer pageSize) {

        if(quarterId == 0){
            QueryWrapper<VeDictQuarter> wrapper = new QueryWrapper<VeDictQuarter>().eq("isCur", 1);
            VeDictQuarter one = veDictQuarterService.getOne(wrapper);
            if(one == null){
                return Result.error("列表加载失败：当前招生季未启用！");
            }
            quarterId = one.getId();
        }
        if(facultyId == 0){
            facultyId = null;
        }
        if(specialtyId == 0){
            specialtyId = null;
        }

        //页码条件
        Integer current = (currentPage-1)*pageSize;
        //根据页码查询数据
        List<VeZsRegistrationVo> reportStuList = veZsRegistrationService.getReportStuList(quarterId, facultyId, specialtyId, keyword, condit, current, pageSize);
//        ---------------林彬辉(获取学号）
        for (VeZsRegistrationVo v : reportStuList){
//            BasicResponseBO<List<VeBaseStudent>> studentAll = iVeBaseManageService.getStudentAll();
//            List<VeBaseStudent> students = studentAll.getResult();
            List<VeBaseStudent> students = veBaseStudentService.list();//2021.9.1
            for (VeBaseStudent student : students){

                if (v.getSfzh().equals(student.getSfzh())){
                    v.setXh(student.getXh());
                    v.setXsdqztm(student.getXsdqztm());
                    if (student.getXh().equals("")){
                        v.setXh("查无学号");
                    }
                    if (student.getXsdqztm().equals("")){
                        v.setXsdqztm("查无当前状态");
                    }
                    break;
                }
            }
//            ---------------
//            BasicResponseBO<VeBaseGrade> basicResponseBO = iVeBaseManageService.getGradeById()
        }


        //条件构造器
        QueryWrapper<VeZsRegistration> wrapper = new QueryWrapper<>();
        wrapper.eq("isCheck",1);
        wrapper.eq("isAdmit",1);
        wrapper.eq("isReport",0);
        wrapper.eq("isNum",0);
        wrapper.isNotNull("classId");

        if(quarterId != null){
            wrapper.eq("ZSJ",quarterId);
        }

        if(facultyId != null){
            wrapper.eq("falId",facultyId);
        }

        if(specialtyId != null){
            wrapper.eq("specId",specialtyId);
        }

        if(condit != null && keyword != null && (!keyword.trim().equals(""))){
            wrapper.like(condit,keyword);
        }

        //查询总数
        Integer count = veZsRegistrationService.list(wrapper).size();

        HashMap<String,Object> map = new HashMap<>();
        map.put("list",reportStuList);
        map.put("count",count);

        return Result.OK(map);
    }

    @ApiOperation(value="获取招生季")
    @PostMapping(value = "/getQuarter")
    public Result<?> getQuarter(){
        //获取招生季集合
        //-------------林彬辉，增加排序
        QueryWrapper<VeDictQuarter> w = new QueryWrapper();
        w.orderByDesc("year");
        List<VeDictQuarter> veDictQuarters = veDictQuarterService.list(w);
//        --------------------
//        List<VeDictQuarter> veDictQuarters = veDictQuarterService.list();

        //将当前招生季放置到首位
        for(int i = 0; i<veDictQuarters.size(); i++){

            if(veDictQuarters.get(i).getIsCur() == 1){
                veDictQuarters.add(0,veDictQuarters.remove(i));
            }
        }
        return Result.OK(veDictQuarters);

    }

    @ApiOperation(value="获取专业部")
    @PostMapping(value = "/getFaculty")
    public Result<?> getFaculty(){
        //获取专业部集合
        List<VeBaseFaculty> list = veBaseFacultyService.list();//2021.9.1
//        ----------------------------林彬辉
//        BasicResponseBO<List<VeBaseFaculty>> facultyList = iVeBaseManageService.getFacultyAll();
//        List<VeBaseFaculty> list = facultyList.getResult();
//        ---------------------------------
        return Result.OK(list);
    }

    @ApiOperation(value="获取专业")
    @PostMapping(value = "/getSpecialty")
    public Result<?> getSpecialty(@ApiParam("专业部ID") @RequestParam("facultyId") Long facultyId){
        //条件构造器
        QueryWrapper<VeBaseSpecialty> wrapper = new QueryWrapper<>();
        wrapper.eq("falId",facultyId);
        //获取专业集合
        List<VeBaseSpecialty> veBaseSpecialties = veBaseSpecialtyService.list(wrapper);//2021.9.1
//        -----------------------林彬辉
//        BasicResponseBO<List<VeBaseSpecialty>> specialtyList = iVeBaseManageService.getSpecialtyByFalId(facultyId);
//        List<VeBaseSpecialty> veBaseSpecialties = specialtyList.getResult();
//-----------------------------------------
        return Result.OK(veBaseSpecialties);
    }

    @ApiOperation(value="批量报到")
    @PostMapping(value = "/batchReport")
    @Transactional(rollbackFor = Exception.class)
    public Result<?> batchReport(@ApiParam("拼接的ID字符串") @RequestParam(value = "ids") String ids){
        //切割字符串
        String[] split = ids.split(",");

        List<VeZsRegistration> list = new ArrayList<>();

        for(String sp : split){

            Long id = Long.parseLong(sp);
            VeZsRegistration veZsRegistration = new VeZsRegistration();
            veZsRegistration.setId(id);
            veZsRegistration.setIsReport(1);

            list.add(veZsRegistration);
        }

        veZsRegistrationService.updateBatchById(list);

        return Result.OK("批量报到成功！");

    }

    @ApiOperation(value="获取报到数量")
    @PostMapping(value = "/getCountBySearch")
    @Transactional(rollbackFor = Exception.class)
    public Result<?> getCountBySearch(@ApiParam("招生季ID") @RequestParam("quarterId") Integer quarterId,
                                      @ApiParam("专业部ID") @RequestParam("facultyId") Long facultyId,
                                      @ApiParam("专业ID") @RequestParam("specialtyId") Long specialtyId){

        if(quarterId == 0){
            QueryWrapper<VeDictQuarter> wrapper = new QueryWrapper<VeDictQuarter>().eq("isCur", 1);
            VeDictQuarter one = veDictQuarterService.getOne(wrapper);
            quarterId = one.getId();
        }
        if(facultyId == 0){
            facultyId = null;
        }
        if(specialtyId == 0){
            specialtyId = null;
        }

        QueryWrapper<VeZsRegistration> wrapper = new QueryWrapper<>();

        wrapper.eq("isCheck",1);
        wrapper.eq("isAdmit",1);
        wrapper.eq("isReport",0);
        wrapper.eq("isNum",0);
        wrapper.isNotNull("classId");

        if(quarterId != null){
            wrapper.eq("ZSJ",quarterId);
        }

        if(facultyId != null){
            wrapper.eq("falId",facultyId);
        }

        if(specialtyId != null){
            wrapper.eq("specId",specialtyId);
        }

        Integer count = veZsRegistrationService.count(wrapper);

        return Result.OK(count);
    }

    @ApiOperation(value="按条件批量报到")
    @PostMapping(value = "/batchReportBySearch")
    @Transactional(rollbackFor = Exception.class)
    public Result<?> batchReport(@ApiParam("招生季ID") @RequestParam("quarterId") Integer quarterId,
                                 @ApiParam("专业部ID") @RequestParam("facultyId") Long facultyId,
                                 @ApiParam("专业ID") @RequestParam("specialtyId") Long specialtyId){

        if(quarterId == 0){
            QueryWrapper<VeDictQuarter> wrapper = new QueryWrapper<VeDictQuarter>().eq("isCur", 1);
            VeDictQuarter one = veDictQuarterService.getOne(wrapper);
            quarterId = one.getId();
        }
        if(facultyId == 0){
            facultyId = null;
        }
        if(specialtyId == 0){
            specialtyId = null;
        }

        UpdateWrapper<VeZsRegistration> wrapper = new UpdateWrapper<>();

        wrapper.eq("isReport",0);
        wrapper.eq("isAdmit",1);
        wrapper.eq("isCheck",1);
        wrapper.eq("isNum",0);
        wrapper.isNotNull("classId");

        if(quarterId != null){
            wrapper.eq("ZSJ",quarterId);
        }

        if(facultyId != null){
            wrapper.eq("falId",facultyId);
        }

        if(specialtyId != null){
            wrapper.eq("specId",specialtyId);
        }

        wrapper.set("isReport",1);

        //设置日期格式
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String time = df.format(new Date());

        wrapper.set("update_time", time);

        veZsRegistrationService.update(wrapper);

        return Result.OK("按条件批量报到成功！");
    }

    @ApiOperation(value="删除")
    @PostMapping(value = "/deleteStu")
    @Transactional(rollbackFor = Exception.class)
    public Result<?> deleteStu(@ApiParam("拼接的ID字符串") @RequestParam(value = "ids") String ids){

        veZsRegistrationService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("删除成功！");

    }

    /**
     * 获取招生信息详情
    * @Author:wcj
     * 修改了数据字典，然后前端对应的axios数据处理有所调整
    * v2.0
     * 4.19
     * */
    @ApiOperation(value="获取招生信息详情")
    @PostMapping(value = "/getInfoByID")
    public Result<?> getInfoByID(@ApiParam("id") @RequestParam("id") Long id ) {

        VeZsRegistration veZsRegistration = veZsRegistrationService.getById(id);

        //数据加工处理
        //户口类型、健康状况、民族、政治面貌
        String hklb="";
        if(veZsRegistration.getHklbm()!="" && veZsRegistration.getHklbm()!=null){
            SysDictItem hklbEntity = sysDictItemService.getById(veZsRegistration.getHklbm());
            if(hklbEntity!=null){
                hklb=hklbEntity.getItemText();
            }

        }
        String healthy="";
        if(veZsRegistration.getJkzkm()!="" && veZsRegistration.getJkzkm()!=null){
            SysDictItem healthyEntity = sysDictItemService.getById(veZsRegistration.getJkzkm());
            if(healthyEntity!=null){
                healthy=healthyEntity.getItemText();
            }

        }
        String nation="";
        if(veZsRegistration.getMzm()!="" && veZsRegistration.getMzm()!=null){

            SysDictItem nationEntity = sysDictItemService.getById(veZsRegistration.getMzm());
            if(nationEntity!=null){
                nation=nationEntity.getItemText();
            }
        }
        String politice="";
        if(veZsRegistration.getZzmmm()!="" && veZsRegistration.getZzmmm()!=null){
            SysDictItem politiceEntity = sysDictItemService.getById(veZsRegistration.getZzmmm());
            if(politiceEntity!=null){
                politice=politiceEntity.getItemText();
            }

        }
        veZsRegistration.setZzmmm(politice);
        veZsRegistration.setMzm(nation);
        veZsRegistration.setJkzkm(healthy);
        veZsRegistration.setHklbm(hklb);
        return Result.OK(veZsRegistration);
    }
}
