package org.edu.modules.enroll.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.modules.enroll.entity.BasicResponseBO;
import org.edu.modules.enroll.entity.VeBaseFaculty;
import org.edu.modules.enroll.entity.VeZsFenban;
import org.edu.modules.enroll.service.IVeBaseManageService;
import org.edu.modules.enroll.service.VeZsFenbanService;
import org.edu.modules.enroll.vo.VeZsFenbanVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-07-24 10:38
 */
@Api(tags="预分班")
@RestController
@RequestMapping("enroll/fenban")
@Slf4j
public class VeZsFenbanController {
    @Autowired
    private VeZsFenbanService veZsFenbanService;
    @Autowired
    private IVeBaseManageService iVeBaseManageService;

    @AutoLog(value = "预分班-显示表格")
    @ApiOperation(value="显示表格", notes="预分班-显示表格")
    @PostMapping("/fenbanShow")
    public Result<?> fenbanShow(
            @RequestParam(value = "gradeId") Integer gradeId,
            @RequestParam(value = "enrollType") String enrollType,
            @RequestParam(value = "falId") Integer falId,
            @RequestParam(value = "specId") Integer specId,
            @RequestParam(value = "statuId") Integer statuId,
            @ApiParam("当前页数") @RequestParam("currentPage") Integer currentPage,
            @ApiParam("分页大小") @RequestParam("pageSize") Integer pageSize
    ){
        HashMap<String, Object> map = veZsFenbanService.getFenbanList(gradeId, enrollType, falId, specId, statuId,currentPage,pageSize);
        return Result.OK(map);
    }

    @AutoLog(value = "为学生指定班级")
    @ApiOperation(value="指定班级", notes="为学生指定班级")
    @PostMapping("/fenbanToStu")
    public Result<?> giveBanji(@RequestParam(value = "id") Integer id,
                               @RequestParam(value = "banjiId") Integer banjiId
                               ){
        boolean b = veZsFenbanService.giveBanji(id, banjiId);
        if(b==true){
            return Result.OK("指定班级成功");
        }else{
            return Result.OK("指定班级失败");
        }
    }

    //李少君
    @AutoLog(value = "为学生批量指定班级")
    @ApiOperation(value="指定班级", notes="为学生批量指定班级")
    @PostMapping("/fenbanMoreToStu")
    public Result<?> giveBanjiMore(@RequestParam(value = "id") String ids,
                               @RequestParam(value = "banjiId") Integer banjiId
    ){
        String[] split = ids.split(",");
        for(String sp : split) {
            int id = Integer.parseInt(sp);
            veZsFenbanService.giveBanji(id, banjiId);
        }
        return Result.OK("批量指定班级成功");
    }

    @AutoLog(value = "为学生退班级")
    @ApiOperation(value="退班级", notes="为学生退班级")
    @PostMapping("/fenbanExit")
    public Result<?> giveBanji(@RequestParam(value = "id") Integer id
    ){
        boolean b = veZsFenbanService.exitBanji(id);
        if(b==true){
            return Result.OK("退班级成功");
        }else{
            return Result.OK("退班级失败");
        }

    }

    //(自动分班)查询计划人数分班表格
    @AutoLog(value = "显示表格")
    @ApiOperation(value="显示表格", notes="显示表格")
    @PostMapping("/ShowList")
    public Result<?> giveBanji(@RequestParam(value = "gradeId") Integer gradeId,
                               @RequestParam(value = "specId") Integer specId,
                               @RequestParam(value = "enrollType") String enrollType,
                               @RequestParam(value = "falId") Integer falId
    ){
        List<VeZsFenbanVo> list = veZsFenbanService.getZidongFenbanList(gradeId, specId, falId, enrollType);
        if(list==null){
            return Result.error("该条件下无班级!");
        }
        return Result.OK(list);

    }


    //当分班人数小于计划人数:一个班级排满再排下一个班级
    @AutoLog(value = "自动分班")
    @ApiOperation(value="自动分班", notes="自动分班")
    @PostMapping("/fenban")
    public Result<?> fenban(
                            @RequestParam(value = "choose1") Integer choose1,
                            @RequestParam(value = "choose2") Integer choose2,
                            @RequestParam(value = "gradeId") Integer gradeId,
                            @RequestParam(value = "specId") Integer specId,
                            @RequestParam(value = "enrollType") String enrollType,
                            @RequestParam(value = "falId") Integer falId
    ){

        Integer jihuaNum = veZsFenbanService.jihuaNum(gradeId, specId, falId, enrollType); //计划人数
        Integer fenbanNum = veZsFenbanService.fenbanNum(specId, falId, gradeId, enrollType); //分班人数
        List<VeZsFenbanVo> list = veZsFenbanService.getBanjiMessage(gradeId, specId); //班级列表
        List<VeZsFenban> stuMsg = veZsFenbanService.fenbanStu(specId, enrollType, falId, gradeId); //分班学生信息
        if(list.size()==0){
            return Result.error("查无此班级!");
        }
        if(stuMsg.size()==0){
            return Result.error("该专业无学生!");
        }
        int a = stuMsg.size();
        if (jihuaNum > fenbanNum) {
            if(choose1==1){
                for (int i =0;i<list.size();i++){
                    for (int j = 0;j<list.get(i).getZrs();j++){
                        int q = list.get(i).getId().intValue();
                        int w = stuMsg.get(a-1).getId().intValue();
                        veZsFenbanService.fenpeiBanji(q,w);
                        a--;
                        if(a<=0){
                            i=list.size();
                            break;
                        }
                    }
                }
            }else if(choose1==2){
                int d = a;
                if (a < list.size()) {
                    for (int i = 0; i < d; i++) {
                        int q = list.get(i).getId().intValue();
                        int w = stuMsg.get(a - 1).getId().intValue();
                        veZsFenbanService.fenpeiBanji(q, w);
                        a--;
                    }
                } else if (a > list.size()) {
                    if(a%list.size()==0){
                        for(int i = 0;i<list.size();i++){
                            for(int j =0;j<a/list.size();j++){
                                int q = list.get(i).getId().intValue();
                                int w = stuMsg.get(a-1).getId().intValue();
                                veZsFenbanService.fenpeiBanji(q,w);
                                a--;
                            }
                        }
                    }else{
                        int b = (int)Math.floor(a/list.size());
                        for(int i = 0;i<list.size();i++){
                            for(int j =0;j<b;j++){
                                int q = list.get(i).getId().intValue();
                                int w = stuMsg.get(a-1).getId().intValue();
                                veZsFenbanService.fenpeiBanji(q,w);
                                a--;
                            }
                        }
                        int c = a%list.size();
                        for (int i = 0; i < c; i++) {
                            int q = list.get(i).getId().intValue();
                            int w = stuMsg.get(c-1).getId().intValue();
                            veZsFenbanService.fenpeiBanji(q,w);
                        }
                    }
                }

            }
        }else if(jihuaNum <= fenbanNum){
            if(choose2==3){
                for(int i = 0;i<list.size();i++){
                    for (int j = 0;j<list.get(i).getZrs();j++){
                        int q = list.get(i).getId().intValue();
                        int w = stuMsg.get(a-1).getId().intValue();
                        veZsFenbanService.fenpeiBanji(q,w);
                        a--;
                    }
                }
            }else if(choose2==4){
                if(a%list.size()==0){
                    for(int i = 0;i<list.size();i++){
                        for(int j =0;j<a/list.size();j++){
                            int q = list.get(i).getId().intValue();
                            int w = stuMsg.get(a-1).getId().intValue();
                            veZsFenbanService.fenpeiBanji(q,w);
                            a--;
                        }
                    }
                }else{
                    int b = (int)Math.floor(a/list.size());
                    for(int i = 0;i<list.size();i++){
                        for(int j =0;j<b;j++){
                            int q = list.get(i).getId().intValue();
                            int w = stuMsg.get(a-1).getId().intValue();
                            veZsFenbanService.fenpeiBanji(q,w);
                            a--;
                        }
                    }
                    int c = a%list.size();
                    for (int i = 0; i < c; i++) {
                        int q = list.get(i).getId().intValue();
                        int w = stuMsg.get(c-1).getId().intValue();
                        veZsFenbanService.fenpeiBanji(q,w);
                    }
                }
            }
        }


        List<VeZsFenban> stumsg = veZsFenbanService.fenbanStu(specId, enrollType, falId, gradeId);
        return Result.OK("分班成功");
//        return Result.OK("分班成功");

    }

    @AutoLog(value = "预分班-显示自动分班表格")
    @ApiOperation(value="显示自动分班表格", notes="预分班-显示自动分班表格")
    @PostMapping("/zidongFenbanShow")
    public Result<?> zidongFenbanShow(
            @RequestParam(value = "gradeId") Integer gradeId,
            @RequestParam(value = "specId") Integer specId,
            @RequestParam(value = "enrollType") String enrollType,
            @RequestParam(value = "falId") Integer falId,
            @ApiParam("当前页数") @RequestParam("currentPage") Integer currentPage,
            @ApiParam("分页大小") @RequestParam("pageSize") Integer pageSize
    ){

        HashMap<String, Object> map = veZsFenbanService.zidongFenbanList(gradeId, enrollType, falId, specId, currentPage, pageSize);
        return Result.OK(map);

    }




}
