package org.edu.modules.enroll.vo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description:  当前招生年份前端交互数据
 * @Author:  zh
 * @Date:  2021-04-05
 * @Version:  V1.0
 */
@Data
@TableName("ve_dict_years")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDictYearsVo implements Serializable {
    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /**系统终端ID*/
    @Excel(name = "系统终端ID", width = 15)
    @ApiModelProperty(value = "系统终端ID")
    @TableField(value = "terminalId")
    private Long terminalId;

    /**是否缴费*/
    @Excel(name = "是否缴费", width = 15)
    @ApiModelProperty(value = "是否缴费（0=否，1=是）")
    @TableField(value = "isPay")
    private Integer isPay;

    /**年份代码*/
    @Excel(name = "年份代码", width = 15)
    @ApiModelProperty(value = "年份代码")
    @TableField(value = "code")
    private String code;

    /**当前入学年份*/
    @Excel(name = "当前入学年份", width = 15)
    @ApiModelProperty(value = "当前入学年份（0=否，1=是）")
    @TableField(value = "curYear")
    private Integer curYear;

    /**逻辑删除*/
    @Excel(name = "逻辑删除", width = 15)
    @ApiModelProperty(value = "逻辑删除（0=未删除，1=已删除）")
    @TableField(value = "is_deleted")
    private Integer is_deleted;

}
