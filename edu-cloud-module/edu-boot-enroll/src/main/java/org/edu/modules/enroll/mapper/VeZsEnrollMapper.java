package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeDictYears;
import org.edu.modules.enroll.entity.VeZsEnroll;
import org.edu.modules.enroll.entity.VeZsSingleEnrollExpansion;
import org.edu.modules.enroll.vo.FormalAdmissionVo;
import org.omg.CORBA.INTERNAL;

import java.util.List;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/7/24 11:19
 */
public interface VeZsEnrollMapper extends BaseMapper<VeZsEnroll> {
    List<VeZsEnroll> showScoreList(@Param("ZKZH") String ZKZH, @Param("XM") String XM, @Param("XN") Integer XN, @Param("XQ") String XQ, @Param("start") Integer start, @Param("pageSize") Integer pageSize);

    Long countScore(@Param("ZKZH") String ZKZH, @Param("XM") String XM, @Param("XN") Integer XN, @Param("XQ") String XQ);

    List<VeZsEnroll> showPreAdmissionList(@Param("KSH") String KSH, @Param("XM") String XM, @Param("XN") Integer XN, @Param("XQ") String XQ, @Param("start") Integer start, @Param("pageSize") Integer pageSize);

    Long countPre(@Param("KSH") String KSH, @Param("XM") String XM, @Param("XN") Integer XN, @Param("XQ") String XQ);

    List<VeZsEnroll> showMockAdmissionList(@Param("KSH") String KSH, @Param("XM") String XM, @Param("XN") Integer XN, @Param("XQ") String XQ, @Param("start") Integer start, @Param("pageSize") Integer pageSize);

    Long countMock(@Param("KSH") String KSH, @Param("XM") String XM, @Param("XN") Integer XN, @Param("XQ") String XQ);

    List<VeZsEnroll> showFormalAdmissionList(@Param("KSH") String KSH, @Param("XM") String XM, @Param("XN") Integer XN, @Param("XQ") String XQ, @Param("start") Integer start, @Param("pageSize") Integer pageSize);

    Long countFormal(@Param("KSH") String KSH, @Param("XM") String XM, @Param("XN") Integer XN, @Param("XQ") String XQ);

    List<VeZsEnroll> exportScore();

    List<VeZsEnroll> exportPreAdmission();

    List<VeZsEnroll> exportMockAdmission();

    Boolean insertScore(VeZsEnroll veZsEnroll);

    Boolean insertPre(VeZsEnroll veZsEnroll);

    Boolean insertMock(VeZsEnroll veZsEnroll);

    Boolean insertFormal(VeZsEnroll veZsEnroll);

    //李少君--网上报名插入数据
    boolean insertBaomingMessage(VeZsSingleEnrollExpansion veZsSingleEnrollExpansion);

    VeZsEnroll getFormalAdmission(@Param("XM") String XM,@Param("KSH") String KSH,@Param("SFZH") String SFZH);
}
