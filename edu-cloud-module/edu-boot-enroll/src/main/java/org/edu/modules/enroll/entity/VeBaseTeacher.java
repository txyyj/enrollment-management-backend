package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.entity
 * @date 2021/7/19 11:56
 */
@Data
@TableName(value = "ve_base_teacher",schema = "edu_dev")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeBaseTeacher {

    @ApiModelProperty(value = "id")
    @TableId(type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "用户id")
    @TableField(value = "user_id")
    private String userId;
    @TableField(value = "GH")
    @ApiModelProperty(value = "工号")
    private String gh;
    @TableField(value = "XM")
    @ApiModelProperty(value = "姓名")
    private String xm;
    @TableField(value = "SFZJLXM")
    @ApiModelProperty(value = "身份证件类型码")
    private String sfzjlxm;
    @TableField(value = "SFZJH")
    @ApiModelProperty(value = "身份证件号")
    private String sfzjh;
    @TableField(value = "CSRQ")
    @ApiModelProperty(value = "出生日期")
    private Long csrq;
    @TableField(value = "dep_id")
    @ApiModelProperty(value = "部门id")
    private Long depId;
    @TableField(value = "jyz_id")
    @ApiModelProperty(value = "教研组id")
    private Long jyzId;
    @TableField(value = "XBM")
    @ApiModelProperty(value = "性别码")
    private String xbm;
    @TableField(value = "MZM")
    @ApiModelProperty(value = "民族码")
    private String mzm;
    @TableField(value = "JKZKM")
    @ApiModelProperty(value = "健康状况码")
    private String jkzkm;
    @TableField(value = "ZZMMM")
    @ApiModelProperty(value = "政治面貌码")
    private String zzmmm;
    @TableField(value = "JG")
    @ApiModelProperty(value = "籍贯")
    private String jg;
    @TableField(value = "SFSLDRK")
    @ApiModelProperty(value = "是否流动人口")
    private String sfsldrk;
    @TableField(value = "CJGZNY")
    @ApiModelProperty(value = "参加工作年月")
    private Long cjgzny;
    @TableField(value = "CJNY")
    @ApiModelProperty(value = "从教年月")
    private Long cjny;
    @TableField(value = "LXNY")
    @ApiModelProperty(value = "来校年月")
    private Long lxny;
    @TableField(value = "BZLBM")
    @ApiModelProperty(value = "编制类别码")
    private String bzlbm;
    @TableField(value = "ZWM")
    @ApiModelProperty(value = "职务码")
    private String zwm;
    @TableField(value = "JZGLBM")
    @ApiModelProperty(value = "教职工类别码")
    private String jzglbm;
    @TableField(value = "GWLBM")
    @ApiModelProperty(value = "岗位类别码")
    private String gwlbm;
    @TableField(value = "user_id")
    @ApiModelProperty(value = "当前状态码")
//    private Long dqztm;
    private String dqztm;
    @TableField(value = "DQZTM")
    @ApiModelProperty(value = "状态，1可用，2不可用")
    private Long status;
    @TableField(value = "terminalid")
    @ApiModelProperty(value = "系统id")
    private Long terminalid;
    @TableField(value = "LXDH")
    @ApiModelProperty(value = "联系电话")
//    private Long lxdh;
    private String lxdh;
}
