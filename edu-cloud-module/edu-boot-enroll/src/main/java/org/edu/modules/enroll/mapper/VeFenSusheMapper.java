package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeZsFenban;
import org.edu.modules.enroll.vo.VeDormSushe;
import org.edu.modules.enroll.vo.VeZsArrangeSusheVo;
import org.edu.modules.enroll.vo.VeZsConfirmFenpeiSusheVo;
import org.edu.modules.enroll.vo.VeZsPlanBedVo;

import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-07-25 20:51
 */
//public interface VeFenSusheMapper extends BaseMapper<VeZsFenban> {
public interface VeFenSusheMapper extends BaseMapper<VeZsFenban> {//林彬辉
    //显示安排床位班级的表格
    List<VeZsPlanBedVo> getPlanBedList(@Param("gradeId")Integer gradeId, @Param("falId")Integer falId,@Param("banjiId") Integer banjiId ,@Param("start") Integer start,@Param("pageSize") Integer pageSize);
    //计算安排床位班级的个数
    Long PlanBedCount(@Param("gradeId")Integer gradeId,@Param("falId") Integer falId,@Param("banjiId") Integer banjiId);
    //显示选择安排入住的寝室表格
    List<VeZsArrangeSusheVo> getArrangeSusheList(@Param("jzwmc")Integer jzwmc,@Param("lch")Integer lch,@Param("fjbm")String fjbm,@Param("sfwk") Integer sfwk,@Param("xb") Integer xb,@Param("start")Integer start,@Param("pageSize")Integer pageSize);
    //计算选择安排入住的寝室数量
    Long ArrangeSusheCount(@Param("jzwmc")Integer jzwmc,@Param("lch")Integer lch,@Param("fjbm")String fjbm,@Param("sfwk") Integer sfwk,@Param("xb") Integer xb);
    //获取指定班级的所有未分配宿舍的学生
    List<VeZsFenban> getStudent(@Param("banjiId") Integer banjiId);
    //给这些班级的学生分配宿舍
    boolean fenpeiSushe(@Param("id") Long id,@Param("susheId") Integer susheId);
    //查询某个宿舍的信息（用于计算空床位数）
    VeDormSushe getSusheMessage(@Param("susheId") Integer susheId);

}
