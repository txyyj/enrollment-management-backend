package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.VeDictXuezi;

/**学制表接口
 * @author 86158
 */
public interface VeDictXueziMapper  extends BaseMapper<VeDictXuezi> {
}
