package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeZsFenban;
import org.edu.modules.enroll.entity.VeZsPayInfo;
import org.edu.modules.enroll.vo.VeZsPayInfoVo;

import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-08-05 17:28
 */
public interface VeZsPayInfoMapper extends BaseMapper<VeZsPayInfo> {
    //添加缴费信息
    boolean addPayInfo(VeZsPayInfo veZsPayInfo);
    //修改缴费信息
    boolean updatePayInfo(VeZsPayInfo veZsPayInfo);
    //显示缴费信息表格
    List<VeZsPayInfoVo> getPayInfoList(@Param("falId") Integer falId,@Param("specId") Integer specId,
                                       @Param("sfzh") String sfzh,@Param("xm") String xm,@Param("state") Integer state,
                                       @Param("start") Integer start,@Param("pageSize")Integer pageSize);
    //计算显示缴费信息表格的个数
    Integer payInfoCount(@Param("falId") Integer falId,@Param("specId") Integer specId,
                      @Param("sfzh") String sfzh,@Param("xm") String xm,@Param("state") Integer state);
    //审核通过
    boolean auditPass(@Param("id") Integer id,@Param("state") Integer state);
    //审核不通过
    boolean auditNoPass(@Param("id") Integer id,@Param("state") Integer state,@Param("failureReason") String failureReason);
    //根据id查看学生缴费信息
    VeZsPayInfoVo showPayInfo(@Param("id") Integer id);
}
