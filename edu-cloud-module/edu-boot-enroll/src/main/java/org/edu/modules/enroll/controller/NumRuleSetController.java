package org.edu.modules.enroll.controller;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.edu.common.api.vo.Result;
import org.edu.modules.enroll.entity.VeBaseXhrule;
import org.edu.modules.enroll.service.VeBaseXhruleService;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

/**
 * 学号规则设置
 * @author yhx
 */
@Api(tags = "学号规则设置")
@RestController
@RequestMapping("enroll/numRuleSet")
public class NumRuleSetController {

    @Resource
    private VeBaseXhruleService veBaseXhruleService;

    @ApiOperation(value = "学号规则查询")
    @PostMapping(value = "/select")
    public Result<?> getNumRule() {
        List<VeBaseXhrule> list = veBaseXhruleService.list();
        return Result.OK(list);
    }

    @ApiOperation("保存")
    @PostMapping(value = "/save")
    @Transactional(rollbackFor = Exception.class)
    public Result<?> save(@ApiParam("年份位数") @RequestParam("yearDigit") Integer yearDigit,
                          @ApiParam("流水号位数") @RequestParam("numDigit") Integer numDigit,
                          @ApiParam("专业位数") @RequestParam("specDigit") Integer specDigit,
                          @ApiParam("班级位数") @RequestParam("clazzDigit") Integer clazzDigit,
                          @ApiParam("年份排序号") @RequestParam("yearSort") Integer yearSort,
                          @ApiParam("专业排序号") @RequestParam("specSort") Integer specSort,
                          @ApiParam("班级排序号") @RequestParam("clazzSort") Integer clazzSort,
                          @ApiParam("流水号排序号") @RequestParam("numSort") Integer numSort,
                          @ApiParam("年份是否启用") @RequestParam("yearVal") Integer yearVal,
                          @ApiParam("专业是否启用") @RequestParam("specVal") Integer specVal,
                          @ApiParam("班级是否启用") @RequestParam("clazzVal") Integer clazzVal,
                          @ApiParam("流水号是否启用") @RequestParam("numVal") Integer numVal){

        //数据校验
        if(yearSort == null || yearSort.equals("") || specSort == null || specSort.equals("")
                || clazzSort == null || clazzSort.equals("") || numSort==null || numSort.equals("")
                || yearDigit==null || yearDigit.equals("") || specDigit==null || specDigit.equals("")
                || clazzDigit==null || clazzDigit.equals("") || numDigit==null || numDigit.equals("")){

            return Result.error("请填写完整！");
        }

        if(!(yearDigit == 2 || yearDigit == 4)){
            return Result.error("年份的位数必须是2位或4位数！");
        }

        if(numDigit < 3){
            return Result.error("流水号的位数不能小于3位数！");
        }

        if(specDigit >= 10 || clazzDigit >= 10 || yearSort >= 10 || specSort >= 10 || clazzSort >= 10 || numSort >= 10 || numDigit >= 10){
            return Result.error("所填位数不能大于9位数！");
        }

        int count = yearVal + specVal + clazzVal + numVal;
        if(count < 3){
            return Result.error("至少需要启用3条规则！");
        }

        //修改年份规则
        UpdateWrapper<VeBaseXhrule> wrapperOne = new UpdateWrapper<>();
        wrapperOne.eq("id",1);
        wrapperOne.set("digit",yearDigit);
        wrapperOne.set("listSort",yearSort);
        wrapperOne.set("status",yearVal);
        veBaseXhruleService.update(wrapperOne);

        //修改专业规则
        UpdateWrapper<VeBaseXhrule> wrapperTwo = new UpdateWrapper<>();
        wrapperTwo.eq("id",2);
        wrapperTwo.set("digit",specDigit);
        wrapperTwo.set("listSort",specSort);
        wrapperTwo.set("status",specVal);
        veBaseXhruleService.update(wrapperTwo);

        //修改班级规则
        UpdateWrapper<VeBaseXhrule> wrapperThree = new UpdateWrapper<>();
        wrapperThree.eq("id",3);
        wrapperThree.set("digit",clazzDigit);
        wrapperThree.set("listSort",clazzSort);
        wrapperThree.set("status",clazzVal);
        veBaseXhruleService.update(wrapperThree);

        //修改流水号规则
        UpdateWrapper<VeBaseXhrule> wrapperFour = new UpdateWrapper<>();
        wrapperFour.eq("id",4);
        wrapperFour.set("digit",numDigit);
        wrapperFour.set("listSort",numSort);
        wrapperFour.set("status",numVal);
        veBaseXhruleService.update(wrapperFour);

        return Result.OK("保存成功!");

    }


}
