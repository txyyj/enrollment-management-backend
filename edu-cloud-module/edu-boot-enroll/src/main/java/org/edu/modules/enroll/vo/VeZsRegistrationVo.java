package org.edu.modules.enroll.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description:  报到管理VO
 * @Author:  yhx
 * @Date:  2021-04-06
 * @Version:  V1.0
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsRegistrationVo implements Serializable {

    /** id */
    @ApiModelProperty(value = "id")
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private String xm;

    /** 身份证号 */
    @Excel(name = "身份证号", width = 15)
    @ApiModelProperty(value = "身份证号")
    private String sfzh;

    /** 毕业学校 李少君*/
    @Excel(name = "毕业学校", width = 15)
    @ApiModelProperty(value = "毕业学校")
    private String byxx;

//    ------------林彬辉
    /** 学号 */
    @Excel(name = "学号", width = 15)
    @ApiModelProperty(value = "学号")
    private String xh;

    @Excel(name = "学生当前状态码", width = 15)
    @ApiModelProperty(value = "学生当前状态码")
    private String xsdqztm;

    @ApiModelProperty(value = "年级id")
    private Long gradeId;

//    ------------
    /** 考试总分 */
    @ApiModelProperty(value = "考试总分")
    private Double kszf;

    /** 性别码 */
    @Excel(name = "性别码", width = 15)
    @ApiModelProperty(value = "性别码")
    private String xbm;

    /** 录取状态 */
    @ApiModelProperty(value = "录取状态0未录取1已录取")
    private Integer isAdmit;

    /** 院系名称 */
    @Excel(name = "所属专业部", width = 15)
    @ApiModelProperty(value = "院系名称")
    private String yxmc;

    /** 专业名称 */
    @Excel(name = "所属专业", width = 15)
    @ApiModelProperty(value = "专业名称")
    private String zymc;

    /** 学制 */
    @ApiModelProperty(value = "学制")
    private String xz;

    /** 入学年份 */
    @ApiModelProperty(value = "入学年份")
    private String rxnf;

    /** 招生季 */
    @ApiModelProperty(value = "招生季")
    private String zsj;

    /** 准考证号 */
    @ApiModelProperty(value = "准考证号")
    private String zkzh;

    /** 年级 */
    @Excel(name = "所属年级", width = 15)
    @ApiModelProperty(value = "年级")
    private String njmc;

    /** 班级 */
    @Excel(name = "所属班级", width = 15)
    @ApiModelProperty(value = "班级")
    private String xzbmc;

    /** 学生联系电话 */
    @Excel(name = "学生联系电话", width = 15)
    @ApiModelProperty(value = "学生联系电话")
    private String xslxdh;

    /** 报名号 */
    @Excel(name = "报名号", width = 15)
    @ApiModelProperty(value = "报名号")
    private String bmh;

    /** 考生号 */
    @Excel(name = "考生号", width = 15)
    @ApiModelProperty(value = "考生号")
    private String ksh;

    /** 班主任 */
    @Excel(name = "班主任", width = 15)
    @ApiModelProperty(value = "班主任")
    private String tea;

    /** 打印次数 */
    @ApiModelProperty(value = "打印次数")
    private Long enrollNum;

    /** 班级id */
    @ApiModelProperty(value = "班级id")
    private Long classId;

}
