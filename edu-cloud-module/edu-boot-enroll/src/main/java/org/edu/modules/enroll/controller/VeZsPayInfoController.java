package org.edu.modules.enroll.controller;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.modules.enroll.entity.VeZsPayInfo;
import org.edu.modules.enroll.service.VeZsPayInfoService;
import org.edu.modules.enroll.util.OssDownloadUtil;
import org.edu.modules.enroll.vo.VeZsPayInfoVo;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * @Auther 李少君
 * @Date 2021-08-05 17:30
 */
@Api(tags="缴费")
@RestController
@RequestMapping("enroll/payInfo")
@Slf4j
public class VeZsPayInfoController {

    @Autowired
    private VeZsPayInfoService veZsPayInfoService;
    @Resource
    private OSS ossClient;

    //提交学生缴费信息
    @AutoLog(value = "缴费-提交学生缴费信息")
    @ApiOperation(value="提交学生缴费信息", notes="缴费-提交学生缴费信息")
    @PostMapping("/addPayInfo")
    public Result<?> addPayInfo(@RequestBody VeZsPayInfo veZsPayInfo) {

        boolean a = veZsPayInfoService.addPayInfo(veZsPayInfo);
        if (a == true) {
            return Result.OK("提交缴费信息成功");
        } else {
            return Result.error("提交缴费信息失败");
        }
    }

    //提交学生缴费信息的图片
    @AutoLog(value = "缴费-提交学生缴费信息图片")
    @ApiOperation(value="提交学生缴费信息", notes="缴费-提交学生缴费信息图片")
    @PostMapping("/addPayInfoPic")
    public Result<?> addPayInfoPic(MultipartFile file)  {
        try {
            // 创建OSS实例。
            OSS ossClient = new OSSClientBuilder().build("oss-cn-beijing.aliyuncs.com","LTAI5tPnLGtPS6r8BpncZMrm","BAJbUlVLXD6or53Zcw1nC8uI87UpOs");

            //获取上传文件输入流
            InputStream inputStream = file.getInputStream();
            //获取文件名称
            String fileName = file.getOriginalFilename();

            //1 在文件名称里面添加随机唯一的值
            String uuid = UUID.randomUUID().toString().replaceAll("-", "");
            // yuy76t5rew01.jpg
            fileName = uuid + fileName;

            //2 把文件按照日期进行分类
            //获取当前日期
            //   2019/11/12
            String datePath = new DateTime().toString("yyyy/MM/dd");

            //拼接
            //  2019/11/12/ewtqr313401.jpg
            fileName = datePath + "/" + fileName;

            //调用oss方法实现上传
            //第一个参数  Bucket名称
            //第二个参数  上传到oss文件路径和文件名称   aa/bb/1.jpg
            //第三个参数  上传文件输入流
            ossClient.putObject("exaplebucket-beijing", fileName, inputStream);

            // 关闭OSSClient。
            ossClient.shutdown();

            //把上传之后文件路径返回
            //需要把上传到阿里云oss路径手动拼接出来
            //  https://edu-guli-1010.oss-cn-beijing.aliyuncs.com/01.jpg
            String url = "https://" + "exaplebucket-beijing" + "." + "oss-cn-beijing.aliyuncs.com" + "/" + fileName;
            return Result.OK(url);
        } catch (Exception e) {
            e.printStackTrace();
            return Result.error("上传失败");
        }
    }


    //修改学生缴费信息
    @AutoLog(value = "缴费-修改学生缴费信息")
    @ApiOperation(value="修改学生缴费信息", notes="缴费-修改学生缴费信息")
    @PostMapping("/updatePayInfo")
    public Result<?> updatePayInfo(@RequestBody VeZsPayInfo veZsPayInfo) {

        boolean a = veZsPayInfoService.updatePayInfo(veZsPayInfo);
        if(a==true){
            return Result.OK("修改缴费信息成功");
        }else{
            return Result.error("修改缴费信息失败");
        }

    }

    //修改学生缴费信息图片
    @AutoLog(value = "缴费-修改学生缴费信息图片")
    @ApiOperation(value="修改学生缴费信息", notes="缴费-修改学生缴费信息图片")
    @PostMapping("/updatePayInfoPic")
    public Result<?> updatePayInfoPic(MultipartFile file) throws Exception {
        File document = null;
        //获取文件名
        String fileName = file.getOriginalFilename();
        //用点来切割获取文件类型注意//.需要加双斜杠正则表达式
        String[] arr = fileName.split("\\.");
        //获取文件后缀名
        String suffixName = arr[arr.length - 1];
        if (!suffixName.equalsIgnoreCase("jpg") && !suffixName.equalsIgnoreCase("png")) {
            return Result.error(400, "文件类型不能上传");
        }
        try {
            String originalFilename = file.getOriginalFilename();
            String[] filename = originalFilename.split("\\.");
            document = File.createTempFile(filename[0], filename[1]);
            file.transferTo(document);
            document.deleteOnExit();
        } catch (IOException e) {
            e.printStackTrace();
        }
        String docName = UUID.randomUUID().toString();
        //上传文件到指定的存储空间  第一个参数 bucketName  第二个参数 OSS存储路径  第三个参数 要上传的文件
        OSSClient ossClient = new OSSClient("oss-cn-beijing.aliyuncs.com","LTAI5tPnLGtPS6r8BpncZMrm","BAJbUlVLXD6or53Zcw1nC8uI87UpOs");
        PutObjectResult result = ossClient.putObject("exaplebucket-beijing",docName+"/"+ fileName, document);
        ossClient.shutdown();
        return Result.OK("https://exaplebucket-beijing.oss-cn-beijing.aliyuncs.com/"+docName+"/"+fileName);
    }


    //显示缴费信息表格
    @AutoLog(value = "缴费-显示缴费信息表格")
    @ApiOperation(value="显示缴费信息表格", notes="缴费-显示缴费信息表格")
    @PostMapping("/showPayInfoList")
    public Result<?> showPayInfoList(
            @ApiParam("专业部id") @RequestParam("falId") Integer falId,
            @ApiParam("专业id") @RequestParam("specId") Integer specId,
            @ApiParam("身份证号") @RequestParam("sfzh") String sfzh,
            @ApiParam("姓名") @RequestParam("xm") String xm,
            @ApiParam("状态") @RequestParam("state") Integer state,
            @ApiParam("当前页数") @RequestParam("currentPage") Integer currentPage,
            @ApiParam("分页大小") @RequestParam("pageSize") Integer pageSize
    ){
        HashMap<String, Object> map = veZsPayInfoService.getPayInfoList(falId, specId, sfzh, xm, state, currentPage, pageSize);
        return Result.OK(map);
    }

    //审核
    @AutoLog(value = "缴费-审核")
    @ApiOperation(value="审核", notes="缴费-审核")
    @PostMapping("/audit")
    public Result<?> audit(
            String ids, //id字符串
            @ApiParam("状态") @RequestParam("state") Integer state,
            @ApiParam("不通过理由") @RequestParam("failureReason") String failureReason
    ){
        String[] split = ids.split(",");
        if(state==2){
            for (String sp : split){
                int id = Integer.parseInt(sp);
                veZsPayInfoService.auditPass(id, state);
            }
            return Result.OK("审核通过成功");
        }else{
            for (String sp : split){
                int id = Integer.parseInt(sp);
                veZsPayInfoService.auditNoPass(id, state,failureReason);
            }
            return Result.OK("审核不通过成功");
        }
    }

    //查看学生缴费信息
    @AutoLog(value = "缴费-查看学生缴费信息")
    @ApiOperation(value="查看学生缴费信息", notes="缴费-查看学生缴费信息")
    @PostMapping("/showPayInfo")
    public Result<?> showPayInfo(Integer id){
        VeZsPayInfoVo veZsPayInfoVo = veZsPayInfoService.showPayInfo(id);
        return Result.OK(veZsPayInfoVo);
    }


}
