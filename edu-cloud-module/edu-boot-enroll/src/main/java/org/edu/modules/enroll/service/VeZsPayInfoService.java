package org.edu.modules.enroll.service;

import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeZsPayInfo;
import org.edu.modules.enroll.vo.VeZsPayInfoVo;

import java.util.HashMap;
import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-08-05 17:27
 */
public interface VeZsPayInfoService {
    //添加缴费信息
    boolean addPayInfo(VeZsPayInfo veZsPayInfo);
    //修改缴费信息
    boolean updatePayInfo(VeZsPayInfo veZsPayInfo);
    //显示缴费信息表格
    HashMap<String, Object> getPayInfoList(Integer falId, Integer specId, String sfzh, String xm, Integer state,
                                           Integer currentPage, Integer pageSize);
    //审核通过
    boolean auditPass(Integer id,Integer state);
    //审核不通过
    boolean auditNoPass(Integer id,Integer state,String failureReason);
    //根据id查看学生缴费信息
    VeZsPayInfoVo showPayInfo(Integer id);
}
