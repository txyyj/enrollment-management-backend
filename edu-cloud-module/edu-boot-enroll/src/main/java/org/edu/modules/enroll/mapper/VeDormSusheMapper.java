package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.VeDormSusheEntity;

public interface VeDormSusheMapper extends BaseMapper<VeDormSusheEntity> {
}
