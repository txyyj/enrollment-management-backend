package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/8/5 9:27
 */
@Data
@TableName("ve_zs_student_loan")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsStudentLoanEntity implements Serializable {
    @ApiModelProperty(value = "id")
    @TableId(type = IdType.AUTO)
    private Long id;

    @ApiModelProperty(value = "预分班学生id")
    @TableField(value = "assign_class_id")
    private Long assignClassId;

    @ApiModelProperty(value = "借款合同编号")
    @TableField(value = "contract_no")
    private String contractNo;

    @ApiModelProperty(value = "审核状态 0未审核 1已审核")
    @TableField(value = "isCheck")
    private Long isCheck;

    @ApiModelProperty(value = "开户行")
    @TableField(value = "bank")
    private String bank;

    @ApiModelProperty(value = "高校账号名称")
    @TableField(value = "bank_name")
    private String bankName;

    @ApiModelProperty(value = "高校账号")
    @TableField(value = "bank_account")
    private String bankAccount;

    @ApiModelProperty(value = "贷款金额")
    @TableField(value = "loan_amount")
    private String loanAmount;

    @ApiModelProperty(value = "银行回执单（存图片url）")
    @TableField(value = "bank_receipt")
    private String bankReceipt;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time")
    private Date createTime;

    @ApiModelProperty(value = "修改时间")
    @TableField(value = "update_time")
    private Date updateTime;
}
