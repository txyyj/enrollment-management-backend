package org.edu.modules.enroll.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.formula.functions.T;
import org.edu.common.api.vo.Result;
import org.edu.modules.enroll.config.HttpURLConnectionUtil;
import org.edu.modules.enroll.entity.*;
import org.edu.modules.enroll.mapper.VeBaseBanjiMapper;
import org.edu.modules.enroll.mapper.VeZsEnrollMapper;
import org.edu.modules.enroll.service.*;
import org.edu.modules.enroll.vo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.awt.LightweightFrame;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/7/24 11:44
 */
@Service
public class VeZsEnrollServiceImpl extends ServiceImpl<VeZsEnrollMapper, VeZsEnroll> implements VeZsEnrollService {

    @Resource
    private VeZsEnrollMapper veZsEnrollMapper;

    @Resource
    private VeBaseFacultyService veBaseFacultyService;
    @Resource
    private VeBaseSpecialtyService veBaseSpecialtyService;
    @Resource
    private VeBaseGradeService veBaseGradeService;
    @Resource
    private VeBaseSemesterService veBaseSemesterService;

    @Value("${common.host}")
    private String dirHost;

    @Autowired
    ObjectMapper mapper;

    //获取成绩公示列表
    @Override
    public HashMap<String, Object> showScoreList(String ZKZH, String XM, Integer XN, String XQ, Integer currentPage, Integer pageSize) {
        List<ScoreAnnouncementVo> result = new ArrayList<>();
        Integer start = (currentPage - 1) * pageSize;
        List<VeZsEnroll> list = veZsEnrollMapper.showScoreList(ZKZH, XM, XN, XQ, start, pageSize);
//        String url = "/common/veCommon/queryFacultyList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
        Long row = veZsEnrollMapper.countScore(ZKZH, XM, XN, XQ);
//        List<VeBaseFaculty> faculties = JSONObject.parseArray(JSON.toJSONString(getUrl(url, List.class).getResult()), VeBaseFaculty.class);
        List<VeBaseFaculty> faculties = veBaseFacultyService.list();//2021.9.2
        for (int i = 0; i < list.size(); i++) {
            ScoreAnnouncementVo scoreAnnouncementVo = new ScoreAnnouncementVo();
            scoreAnnouncementVo.setXM(list.get(i).getXM());
            scoreAnnouncementVo.setPM(list.get(i).getPM());
            scoreAnnouncementVo.setZKZH(list.get(i).getZKZH());
            scoreAnnouncementVo.setXN(list.get(i).getXN());
            scoreAnnouncementVo.setXQ(list.get(i).getXQ());
            scoreAnnouncementVo.setZF(list.get(i).getZF());
            scoreAnnouncementVo.setKM1(list.get(i).getKM1());
            scoreAnnouncementVo.setKM2(list.get(i).getKM2());
            scoreAnnouncementVo.setKM3(list.get(i).getKM3());
            for (int j = 0; j < faculties.size(); j++) {
                if (faculties.get(j).getId() == list.get(i).getFaculty()) {
                    scoreAnnouncementVo.setFaculty(faculties.get(j).getYxmc());
                }
            }
//            String majorUrl = "/common/veCommon/querySpecialtyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67" + "&specialtyId=" + list.get(i).getFormalmajor();
//            BasicResponseBO<VeBaseSpecialty> b = getUrl(majorUrl, VeBaseSpecialty.class);
            BasicResponseBO<VeBaseSpecialty> b = new BasicResponseBO<>();
            b.setResult(veBaseSpecialtyService.getById(list.get(i).getFormalmajor()));//2021.9.2
            if (b.getResult() == null) {
                scoreAnnouncementVo.setFormalmajor("");
            } else {
                scoreAnnouncementVo.setFormalmajor(b.getResult().getZymc());
            }
            result.add(scoreAnnouncementVo);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("list", result);
        map.put("count", row);
        return map;
    }

    //获取预录取列表
    @Override
    public HashMap<String, Object> showPreAdmissionList(String KSH, String XM, Integer XN, String XQ, Integer currentPage, Integer pageSize) {
        List<PreAdmissionVo> result = new ArrayList<>();
        Integer start = (currentPage - 1) * pageSize;
        List<VeZsEnroll> list = veZsEnrollMapper.showPreAdmissionList(KSH, XM, XN, XQ, start, pageSize);
        Long row = veZsEnrollMapper.countPre(KSH, XM, XN, XQ);
        for (int i = 0; i < list.size(); i++) {
            PreAdmissionVo preAdmissionVo = new PreAdmissionVo();
            preAdmissionVo.setKSH(list.get(i).getKSH());
            preAdmissionVo.setXM(list.get(i).getXM());
            preAdmissionVo.setXN(list.get(i).getXN());
            preAdmissionVo.setXQ(list.get(i).getXQ());
            preAdmissionVo.setSFZH(list.get(i).getSFZH());
            preAdmissionVo.setZXMC(list.get(i).getZXMC());
            preAdmissionVo.setZF(list.get(i).getZF());
            preAdmissionVo.setKM1(list.get(i).getKM1());
            preAdmissionVo.setKM2(list.get(i).getKM2());
            preAdmissionVo.setKM3(list.get(i).getKM3());
//            String majorUrl = "/common/veCommon/querySpecialtyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67" + "&specialtyId=" + list.get(i).getPrepmajor();
//            BasicResponseBO<VeBaseSpecialty> b = getUrl(majorUrl, VeBaseSpecialty.class);
            BasicResponseBO<VeBaseSpecialty> b =new BasicResponseBO<>();
            b.setResult(veBaseSpecialtyService.getById(list.get(i).getPrepmajor()));//2021.9.2
            if (b.getResult() == null) {
                preAdmissionVo.setPrepmajor("");
            } else {
                preAdmissionVo.setPrepmajor(b.getResult().getZymc());
            }
            result.add(preAdmissionVo);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("list", result);
        map.put("count", row);
        return map;
    }

    //获取拟录取列表
    @Override
    public HashMap<String, Object> showMockAdmissionList(String KSH, String XM, Integer XN, String XQ, Integer currentPage, Integer pageSize) {
        List<MockAdmissionVo> result = new ArrayList<>();
        Integer start = (currentPage - 1) * pageSize;
        List<VeZsEnroll> list = veZsEnrollMapper.showMockAdmissionList(KSH, XM, XN, XQ, start, pageSize);
        Long row = veZsEnrollMapper.countMock(KSH, XM, XN, XQ);
        for (int i = 0; i < list.size(); i++) {
            MockAdmissionVo mockAdmissionVo = new MockAdmissionVo();
            mockAdmissionVo.setKSH(list.get(i).getKSH());
            mockAdmissionVo.setXM(list.get(i).getXM());
            mockAdmissionVo.setXN(list.get(i).getXN());
            mockAdmissionVo.setXQ(list.get(i).getXQ());
            mockAdmissionVo.setSFZH(list.get(i).getSFZH());
            mockAdmissionVo.setZXMC(list.get(i).getZXMC());
            mockAdmissionVo.setZF(list.get(i).getZF());
            mockAdmissionVo.setKM1(list.get(i).getKM1());
            mockAdmissionVo.setKM2(list.get(i).getKM2());
            mockAdmissionVo.setKM3(list.get(i).getKM3());
//            String majorUrl = "/common/veCommon/querySpecialtyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67" + "&specialtyId=" + list.get(i).getMajor();
//            BasicResponseBO<VeBaseSpecialty> b = getUrl(majorUrl, VeBaseSpecialty.class);
            BasicResponseBO<VeBaseSpecialty> b =new BasicResponseBO<>();
            b.setResult(veBaseSpecialtyService.getById(list.get(i).getMajor()));//2021.9.2
            if (b.getResult() == null) {
                mockAdmissionVo.setMajor("");
            } else {
                mockAdmissionVo.setMajor(b.getResult().getZymc());
            }
            result.add(mockAdmissionVo);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("list", result);
        map.put("count", row);
        return map;
    }

    @Override
    public HashMap<String, Object> showFormalAdmissionList(String KSH, String XM, Integer XN, String XQ, Integer currentPage, Integer pageSize) {
        List<FormalAdmissionVo> result = new ArrayList<>();
        Integer start = (currentPage - 1) * pageSize;
        List<VeZsEnroll> list = veZsEnrollMapper.showFormalAdmissionList(KSH, XM, XN, XQ, start, pageSize);
        Long row = veZsEnrollMapper.countFormal(KSH, XM, XN, XQ);
        for (int i = 0; i < list.size(); i++) {
            FormalAdmissionVo formalAdmissionVo = new FormalAdmissionVo();
            formalAdmissionVo.setKSH(list.get(i).getKSH());
            formalAdmissionVo.setXM(list.get(i).getXM());
            formalAdmissionVo.setXN(list.get(i).getXN());
            formalAdmissionVo.setXQ(list.get(i).getXQ());
            formalAdmissionVo.setSFZH(list.get(i).getSFZH());
            formalAdmissionVo.setZXMC(list.get(i).getZXMC());
            formalAdmissionVo.setZF(list.get(i).getZF());
            formalAdmissionVo.setKM1(list.get(i).getKM1());
            formalAdmissionVo.setKM2(list.get(i).getKM2());
            formalAdmissionVo.setKM3(list.get(i).getKM3());
//            String majorUrl = "/common/veCommon/querySpecialtyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67" + "&specialtyId=" + list.get(i).getFormalmajor();
//            BasicResponseBO<VeBaseSpecialty> b = getUrl(majorUrl, VeBaseSpecialty.class);
            BasicResponseBO<VeBaseSpecialty> b =new BasicResponseBO<>();
            b.setResult(veBaseSpecialtyService.getById(list.get(i).getFormalmajor()));//2021.9.2
            if (b.getResult() == null) {
                formalAdmissionVo.setFormalmajor("");
            } else {
                formalAdmissionVo.setFormalmajor(b.getResult().getZymc());
            }
            result.add(formalAdmissionVo);
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("list", result);
        map.put("count", row);
        return map;
    }

    //导出成绩公示
    @Override
    public List<ScoreAnnouncementVo> exportScore() {
        List<ScoreAnnouncementVo> result = new ArrayList<>();
        List<VeZsEnroll> list = veZsEnrollMapper.exportScore();
//        String url = "/common/veCommon/queryFacultyList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
//        List<VeBaseFaculty> faculties = JSONObject.parseArray(JSON.toJSONString(getUrl(url, List.class).getResult()), VeBaseFaculty.class);
        List<VeBaseFaculty> faculties =  veBaseFacultyService.list();//2021.9.2
        for (int i = 0; i < list.size(); i++) {
            ScoreAnnouncementVo scoreAnnouncementVo = new ScoreAnnouncementVo();
            scoreAnnouncementVo.setXM(list.get(i).getXM());
            scoreAnnouncementVo.setPM(list.get(i).getPM());
            scoreAnnouncementVo.setZKZH(list.get(i).getZKZH());
            scoreAnnouncementVo.setXN(list.get(i).getXN());
            scoreAnnouncementVo.setXQ(list.get(i).getXQ());
            scoreAnnouncementVo.setZF(list.get(i).getZF());
            scoreAnnouncementVo.setKM1(list.get(i).getKM1());
            scoreAnnouncementVo.setKM2(list.get(i).getKM2());
            scoreAnnouncementVo.setKM3(list.get(i).getKM3());
            for (int j = 0; j < faculties.size(); j++) {
                if (faculties.get(j).getId() == list.get(i).getFaculty()) {
                    scoreAnnouncementVo.setFaculty(faculties.get(j).getYxmc());
                }
            }
//            String majorUrl = "/common/veCommon/querySpecialtyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67" + "&specialtyId=" + list.get(i).getFormalmajor();
//            BasicResponseBO<VeBaseSpecialty> b = getUrl(majorUrl, VeBaseSpecialty.class);
            BasicResponseBO<VeBaseSpecialty> b =new BasicResponseBO<>();
            b.setResult(veBaseSpecialtyService.getById(list.get(i).getFormalmajor()));//2021.9.2
            if (b.getResult() == null) {
                scoreAnnouncementVo.setFormalmajor("");
            } else {
                scoreAnnouncementVo.setFormalmajor(b.getResult().getZymc());
            }
            result.add(scoreAnnouncementVo);
        }
        return result;
    }

    @Override
    public List<PreAdmissionVo> exportPreAdmission() {
        List<PreAdmissionVo> result = new ArrayList<>();
        List<VeZsEnroll> list = veZsEnrollMapper.exportPreAdmission();
        for (int i = 0; i < list.size(); i++) {
            PreAdmissionVo preAdmissionVo = new PreAdmissionVo();
            preAdmissionVo.setKSH(list.get(i).getKSH());
            preAdmissionVo.setXM(list.get(i).getXM());
            preAdmissionVo.setXN(list.get(i).getXN());
            preAdmissionVo.setXQ(list.get(i).getXQ());
            preAdmissionVo.setSFZH(list.get(i).getSFZH());
            preAdmissionVo.setZXMC(list.get(i).getZXMC());
            preAdmissionVo.setZF(list.get(i).getZF());
            preAdmissionVo.setKM1(list.get(i).getKM1());
            preAdmissionVo.setKM2(list.get(i).getKM2());
//            preAdmissionVo.setKM3(list.get(i).getKM3());
//            String majorUrl = "/common/veCommon/querySpecialtyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67" + "&specialtyId=" + list.get(i).getPrepmajor();
//            BasicResponseBO<VeBaseSpecialty> b = getUrl(majorUrl, VeBaseSpecialty.class);
            BasicResponseBO<VeBaseSpecialty> b =new BasicResponseBO<>();
            b.setResult(veBaseSpecialtyService.getById(list.get(i).getPrepmajor()));//2021.9.2
            if (b.getResult() == null) {
                preAdmissionVo.setPrepmajor("");
            } else {
                preAdmissionVo.setPrepmajor(b.getResult().getZymc());
            }
            result.add(preAdmissionVo);
        }
        return result;
    }

    @Override
    public List<MockAdmissionVo> exportMockAdmission() {
        List<MockAdmissionVo> result = new ArrayList<>();
        List<VeZsEnroll> list = veZsEnrollMapper.exportMockAdmission();
        for (int i = 0; i < list.size(); i++) {
            MockAdmissionVo mockAdmissionVo = new MockAdmissionVo();
            mockAdmissionVo.setKSH(list.get(i).getKSH());
            mockAdmissionVo.setXM(list.get(i).getXM());
            mockAdmissionVo.setXN(list.get(i).getXN());
            mockAdmissionVo.setXQ(list.get(i).getXQ());
            mockAdmissionVo.setSFZH(list.get(i).getSFZH());
            mockAdmissionVo.setZXMC(list.get(i).getZXMC());
            mockAdmissionVo.setZF(list.get(i).getZF());
            mockAdmissionVo.setKM1(list.get(i).getKM1());
            mockAdmissionVo.setKM2(list.get(i).getKM2());
            mockAdmissionVo.setKM3(list.get(i).getKM3());
//            String majorUrl = "/common/veCommon/querySpecialtyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67" + "&specialtyId=" + list.get(i).getMajor();
//            BasicResponseBO<VeBaseSpecialty> b = getUrl(majorUrl, VeBaseSpecialty.class);
            BasicResponseBO<VeBaseSpecialty> b =new BasicResponseBO<>();
            b.setResult(veBaseSpecialtyService.getById(list.get(i).getMajor()));//2021.9.2
            if (b.getResult() == null) {
                mockAdmissionVo.setMajor("");
            } else {
                mockAdmissionVo.setMajor(b.getResult().getZymc());
            }
            result.add(mockAdmissionVo);
        }
        return result;
    }

    @Override
    public List<FormalAdmissionVo> exportFormalAdmission() {
        List<FormalAdmissionVo> result = new ArrayList<>();
        List<VeZsEnroll> list = veZsEnrollMapper.exportScore();
        for (int i = 0; i < list.size(); i++) {
            FormalAdmissionVo formalAdmissionVo = new FormalAdmissionVo();
            formalAdmissionVo.setKSH(list.get(i).getKSH());
            formalAdmissionVo.setXM(list.get(i).getXM());
            formalAdmissionVo.setXN(list.get(i).getXN());
            formalAdmissionVo.setXQ(list.get(i).getXQ());
            formalAdmissionVo.setSFZH(list.get(i).getSFZH());
            formalAdmissionVo.setZXMC(list.get(i).getZXMC());
            formalAdmissionVo.setZF(list.get(i).getZF());
            formalAdmissionVo.setKM1(list.get(i).getKM1());
            formalAdmissionVo.setKM2(list.get(i).getKM2());
            formalAdmissionVo.setKM3(list.get(i).getKM3());
//            String majorUrl = "/common/veCommon/querySpecialtyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67" + "&specialtyId=" + list.get(i).getFormalmajor();
//            BasicResponseBO<VeBaseSpecialty> b = getUrl(majorUrl, VeBaseSpecialty.class);
            BasicResponseBO<VeBaseSpecialty> b =new BasicResponseBO<>();
            b.setResult(veBaseSpecialtyService.getById(list.get(i).getFormalmajor()));//2021.9.2
            if (b.getResult() == null) {
                formalAdmissionVo.setFormalmajor("");
            } else {
                formalAdmissionVo.setFormalmajor(b.getResult().getZymc());
            }
            result.add(formalAdmissionVo);
        }
        return result;
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result<?> importScore(List<ScoreAnnouncementVo> list) {
        List<VeZsEnroll> result = new ArrayList<>();
//        String url = "/common/veCommon/queryFacultyList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
////        List<VeBaseFaculty> faculties = JSONObject.parseArray(JSON.toJSONString(getUrl(url, List.class).getResult()), VeBaseFaculty.class);
//        BasicResponseBO<List> b = getUrl(url, List.class);
        BasicResponseBO<List<VeBaseFaculty>> responseBO = new BasicResponseBO<>();
        List<VeBaseFaculty> facultieList = veBaseFacultyService.list();//
        responseBO.setResult(facultieList);//2021.9.2
//        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
//                .setResult(b.getResult() == null ? null :
//                        JSON.parseArray(JSON.toJSONString(b.getResult()), VeBaseFaculty.class));

        for (int i = 0; i < list.size(); i++) {
            VeZsEnroll veZsEnroll = new VeZsEnroll();
            veZsEnroll.setXM(list.get(i).getXM());
            veZsEnroll.setZF(list.get(i).getZF());
            veZsEnroll.setKM1(list.get(i).getKM1());
            veZsEnroll.setKM2(list.get(i).getKM2());
            veZsEnroll.setKM3(list.get(i).getKM3());
            veZsEnroll.setZKZH(list.get(i).getZKZH());
            veZsEnroll.setPM(list.get(i).getPM());
            veZsEnroll.setXN(list.get(i).getXN());
            veZsEnroll.setXQ(list.get(i).getXQ());
            for (int j = 0; j < responseBO.getResult().size(); j++) {
                if (list.get(i).getFaculty().equals(responseBO.getResult().get(j).getYxmc())) {
                    veZsEnroll.setFaculty(responseBO.getResult().get(j).getId().intValue());
//                    String majorUrl = "/common/veCommon/querySpecialtyListByFalId?falId=" + responseBO.getResult().get(j).getId() + "&interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
////                    List<VeBaseSpecialty> major = JSONObject.parseArray(JSON.toJSONString(getUrl(majorUrl, List.class).getResult()), VeBaseSpecialty.class);
//                    BasicResponseBO<List> c = getUrl(majorUrl, List.class);
                    BasicResponseBO<List<VeBaseSpecialty>> major = new BasicResponseBO<>();
                    QueryWrapper<VeBaseSpecialty> specialtyQueryWrapper = new QueryWrapper<>();
                    specialtyQueryWrapper.eq("falId",responseBO.getResult().get(j).getId());
                    major.setResult(veBaseSpecialtyService.list(specialtyQueryWrapper));//2021.9.2

//                    major.setSuccess(c.getSuccess()).setMessage(c.getMessage())
//                            .setResult(c.getResult() == null ? null :
//                                    JSON.parseArray(JSON.toJSONString(c.getResult()), VeBaseSpecialty.class));
                    Boolean majorIsTrue = false;
                    for (int k = 0; k < major.getResult().size(); k++) {
                        if (k == major.getResult().size() - 1 && j==responseBO.getResult().size()-1 && !list.get(i).getFormalmajor().equals(major.getResult().get(major.getResult().size() - 1).getZymc())) {
                            return Result.error("专业信息有误");
                        }
                        if (list.get(i).getFormalmajor().equals(major.getResult().get(k).getZymc())) {
                            veZsEnroll.setFormalmajor(major.getResult().get(k).getId().intValue());
                            majorIsTrue = true;
                            break;
                        }
                    }
                    if (majorIsTrue){
                        break;
                    }
                }
            }
//            String gradeUrl = "/common/veCommon/queryGradeList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
//            BasicResponseBO<List> d = getUrl(gradeUrl, List.class);
            BasicResponseBO<List<VeBaseGrade>> grade = new BasicResponseBO<>();
            grade.setResult(veBaseGradeService.list());//2021.9.2
//            grade.setSuccess(d.getSuccess()).setMessage(d.getMessage())
//                    .setResult(d.getResult() == null ? null :
//                            JSON.parseArray(JSON.toJSONString(d.getResult()), VeBaseGrade.class));
            for (int j = 0; j < grade.getResult().size(); j++) {
                if (j == grade.getResult().size() - 1 && list.get(i).getXN() != Integer.parseInt(grade.getResult().get(grade.getResult().size() - 1).getNjdm())) {
                    return Result.error("学年信息有误");
                }
                if (list.get(i).getXN() == Integer.parseInt(grade.getResult().get(j).getNjdm())) {
                    veZsEnroll.setXN(Integer.parseInt(grade.getResult().get(j).getNjdm()));
                    break;
                }
            }
//            String semesterUrl = "/common/veCommon/querySemesterList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
//            BasicResponseBO<List> e = getUrl(semesterUrl, List.class);
            BasicResponseBO<List<VeBaseSemester>> semester = new BasicResponseBO<>();
            semester.setResult(veBaseSemesterService.list());//2021.9.2
//            semester.setSuccess(e.getSuccess()).setMessage(e.getMessage())
//                    .setResult(e.getResult() == null ? null :
//                            JSON.parseArray(JSON.toJSONString(e.getResult()), VeBaseSemester.class));
            for (int j = 0; j < semester.getResult().size(); j++) {
                if (j == semester.getResult().size() - 1 && !list.get(i).getXQ().equals(semester.getResult().get(semester.getResult().size() - 1).getXqmc())) {
                    return Result.error("学期信息有误");
                }
                if (list.get(i).getXQ().equals(semester.getResult().get(j).getXqmc())) {
                    veZsEnroll.setXQ((semester.getResult().get(j).getXqmc()));
                    break;
                }
            }
//            String facultiesUrl = "/common/veCommon/queryFacultyList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
//            BasicResponseBO<List> f = getUrl(facultiesUrl, List.class);
            BasicResponseBO<List<VeBaseFaculty>> faculties = new BasicResponseBO<>();
            faculties.setResult(veBaseFacultyService.list());//2021.9.2
//            faculties.setSuccess(f.getSuccess()).setMessage(f.getMessage())
//                    .setResult(f.getResult() == null ? null :
//                            JSON.parseArray(JSON.toJSONString(f.getResult()), VeBaseFaculty.class));
            for (int j = 0; j < faculties.getResult().size(); j++) {
                if (j == faculties.getResult().size() - 1 && !list.get(i).getFaculty().equals(faculties.getResult().get(faculties.getResult().size() - 1).getYxmc())) {
                    return Result.error("院系信息有误");
                }
                if (list.get(i).getFaculty().equals(faculties.getResult().get(j).getYxmc())) {
                    veZsEnroll.setFaculty((faculties.getResult().get(j).getId().intValue()));
                    break;
                }
            }
            result.add(veZsEnroll);
        }
        for (VeZsEnroll veZsEnroll : result) {
            veZsEnrollMapper.insertMock(veZsEnroll);
        }
        return Result.ok("文件导入成功！数据行数：" + list.size());
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Result<?> preAdmissionImport(List<PreAdmissionVo> list) {
        List<VeZsEnroll> result = new ArrayList<>();
//        String url = "/common/veCommon/queryFacultyList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
////        List<VeBaseFaculty> faculties = JSONObject.parseArray(JSON.toJSONString(getUrl(url, List.class).getResult()), VeBaseFaculty.class);
//        BasicResponseBO<List> b = getUrl(url, List.class);
        BasicResponseBO<List<VeBaseFaculty>> responseBO = new BasicResponseBO<>();
        responseBO.setResult(veBaseFacultyService.list());//2021.9.2
//        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
//                .setResult(b.getResult() == null ? null :
//                        JSON.parseArray(JSON.toJSONString(b.getResult()), VeBaseFaculty.class));
        for (int i = 0; i < list.size(); i++) {
            VeZsEnroll veZsEnroll = new VeZsEnroll();
            veZsEnroll.setKSH(list.get(i).getKSH());
            veZsEnroll.setXN(list.get(i).getXN());
            veZsEnroll.setXQ(list.get(i).getXQ());
            veZsEnroll.setXM(list.get(i).getXM());
            veZsEnroll.setZF(list.get(i).getZF());
            veZsEnroll.setKM1(list.get(i).getKM1());
            veZsEnroll.setKM2(list.get(i).getKM2());
            veZsEnroll.setKM3(list.get(i).getKM3());
            veZsEnroll.setXN(list.get(i).getXN());
            veZsEnroll.setXQ(list.get(i).getXQ());
            veZsEnroll.setZXMC(list.get(i).getZXMC());
            veZsEnroll.setSFZH(list.get(i).getSFZH());
            for (int j = 0; j < responseBO.getResult().size(); j++) {
//                String majorUrl = "/common/veCommon/querySpecialtyListByFalId?falId=" + responseBO.getResult().get(j).getId() + "&interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
////                List<VeBaseSpecialty> major = JSONObject.parseArray(JSON.toJSONString(getUrl(majorUrl, List.class).getResult()), VeBaseSpecialty.class);
//                BasicResponseBO<List> c = getUrl(majorUrl, List.class);
                BasicResponseBO<List<VeBaseSpecialty>> major = new BasicResponseBO<>();
                QueryWrapper<VeBaseSpecialty> veBaseSpecialtyQueryWrapper = new QueryWrapper<>();
                veBaseSpecialtyQueryWrapper.eq("falId",responseBO.getResult().get(j).getId());
                major.setResult(veBaseSpecialtyService.list(veBaseSpecialtyQueryWrapper));//202.9.2
//                major.setSuccess(c.getSuccess()).setMessage(c.getMessage())
//                        .setResult(c.getResult() == null ? null :
//                                JSON.parseArray(JSON.toJSONString(c.getResult()), VeBaseSpecialty.class));
                Boolean majorIsTrue = false;
                for (int k = 0; k < major.getResult().size(); k++) {
                    if (k == major.getResult().size() - 1 &&j == responseBO.getResult().size() -1 && !list.get(i).getPrepmajor().equals(major.getResult().get(major.getResult().size() - 1).getZymc())) {
                        return Result.error("专业信息有误");
                    }
                    if (list.get(i).getPrepmajor().equals(major.getResult().get(k).getZymc())) {
                        veZsEnroll.setPrepmajor(major.getResult().get(k).getId().intValue());
                        majorIsTrue = true;
                        break;
                    }
                }
                if (majorIsTrue){
                    break;
                }
            }
//            String gradeUrl = "/common/veCommon/queryGradeList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
//            BasicResponseBO<List> d = getUrl(gradeUrl, List.class);
            BasicResponseBO<List<VeBaseGrade>> grade = new BasicResponseBO<>();
            grade.setResult(veBaseGradeService.list());//2021.9.2
//            grade.setSuccess(d.getSuccess()).setMessage(d.getMessage())
//                    .setResult(d.getResult() == null ? null :
//                            JSON.parseArray(JSON.toJSONString(d.getResult()), VeBaseGrade.class));
            for (int j = 0; j < grade.getResult().size(); j++) {
                if (j == grade.getResult().size() - 1 && list.get(i).getXN() != Integer.parseInt(grade.getResult().get(grade.getResult().size() - 1).getNjdm())) {
                    return Result.error("学年信息有误");
                }
                if (list.get(i).getXN() == Integer.parseInt(grade.getResult().get(j).getNjdm())) {
                    veZsEnroll.setXN(Integer.parseInt(grade.getResult().get(j).getNjdm()));
                    break;
                }
            }
//            String semesterUrl = "/common/veCommon/querySemesterList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
//            BasicResponseBO<List> e = getUrl(semesterUrl, List.class);
            BasicResponseBO<List<VeBaseSemester>> semester = new BasicResponseBO<>();
            semester.setResult(veBaseSemesterService.list());//2021.9.2
//            semester.setSuccess(e.getSuccess()).setMessage(e.getMessage())
//                    .setResult(e.getResult() == null ? null :
//                            JSON.parseArray(JSON.toJSONString(e.getResult()), VeBaseSemester.class));
            for (int j = 0; j < semester.getResult().size(); j++) {
                if (j == semester.getResult().size() - 1 && !list.get(i).getXQ().equals(semester.getResult().get(semester.getResult().size() - 1).getXqmc())) {
                    return Result.error("学期信息有误");
                }
                if (list.get(i).getXQ().equals(semester.getResult().get(j).getXqmc())) {
                    veZsEnroll.setXQ((semester.getResult().get(j).getXqmc()));
                    break;
                }
            }
            result.add(veZsEnroll);
        }
        for (VeZsEnroll veZsEnroll : result) {
            veZsEnrollMapper.insertMock(veZsEnroll);
        }
        return Result.ok("文件导入成功！数据行数：" + list.size());
    }

    @Override
    public Result<?> formalAdmissionImport(List<FormalAdmissionVo> list) {
        List<VeZsEnroll> result = new ArrayList<>();
//        String url = "/common/veCommon/queryFacultyList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
////        List<VeBaseFaculty> faculties = JSONObject.parseArray(JSON.toJSONString(getUrl(url, List.class).getResult()), VeBaseFaculty.class);
//        BasicResponseBO<List> b = getUrl(url, List.class);
        BasicResponseBO<List<VeBaseFaculty>> responseBO = new BasicResponseBO<>();
        responseBO.setResult(veBaseFacultyService.list());//2021.9.2
//        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
//                .setResult(b.getResult() == null ? null :
//                        JSON.parseArray(JSON.toJSONString(b.getResult()), VeBaseFaculty.class));
        for (int i = 0; i < list.size(); i++) {
            VeZsEnroll veZsEnroll = new VeZsEnroll();
            veZsEnroll.setKSH(list.get(i).getKSH());
            veZsEnroll.setXN(list.get(i).getXN());
            veZsEnroll.setXQ(list.get(i).getXQ());
            veZsEnroll.setXM(list.get(i).getXM());
            veZsEnroll.setZF(list.get(i).getZF());
            veZsEnroll.setKM1(list.get(i).getKM1());
            veZsEnroll.setKM2(list.get(i).getKM2());
            veZsEnroll.setKM3(list.get(i).getKM3());
            veZsEnroll.setXN(list.get(i).getXN());
            veZsEnroll.setXQ(list.get(i).getXQ());
            veZsEnroll.setZXMC(list.get(i).getZXMC());
            veZsEnroll.setSFZH(list.get(i).getSFZH());
            for (int j = 0; j < responseBO.getResult().size(); j++) {
//                String majorUrl = "/common/veCommon/querySpecialtyListByFalId?falId=" + responseBO.getResult().get(j).getId() + "&interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
////                List<VeBaseSpecialty> major = JSONObject.parseArray(JSON.toJSONString(getUrl(majorUrl, List.class).getResult()), VeBaseSpecialty.class);
//                BasicResponseBO<List> c = getUrl(majorUrl, List.class);
                BasicResponseBO<List<VeBaseSpecialty>> major = new BasicResponseBO<>();
                QueryWrapper<VeBaseSpecialty> specialtyQueryWrapper = new QueryWrapper<>();
                specialtyQueryWrapper.eq("falId",responseBO.getResult().get(j).getId());
                major.setResult(veBaseSpecialtyService.list(specialtyQueryWrapper));//2021.9.2
//                major.setSuccess(c.getSuccess()).setMessage(c.getMessage())
//                        .setResult(c.getResult() == null ? null :
//                                JSON.parseArray(JSON.toJSONString(c.getResult()), VeBaseSpecialty.class));
                Boolean majorIsTrue = false;
                for (int k = 0; k < major.getResult().size(); k++) {
                    if (k == major.getResult().size() - 1 &&j == responseBO.getResult().size() -1&& !list.get(i).getFormalmajor().equals(major.getResult().get(major.getResult().size() - 1).getZymc())) {
                        return Result.error("专业信息有误");
                    }
                    if (list.get(i).getFormalmajor().equals(major.getResult().get(k).getZymc())) {
                        veZsEnroll.setFormalmajor(major.getResult().get(k).getId().intValue());
                        majorIsTrue = true;
                        break;
                    }
                }
                if (majorIsTrue){
                    break;
                }

            }
//            String gradeUrl = "/common/veCommon/queryGradeList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
//            BasicResponseBO<List> d = getUrl(gradeUrl, List.class);
            BasicResponseBO<List<VeBaseGrade>> grade = new BasicResponseBO<>();
            grade.setResult(veBaseGradeService.list());//2021.9.2
//            grade.setSuccess(d.getSuccess()).setMessage(d.getMessage())
//                    .setResult(d.getResult() == null ? null :
//                            JSON.parseArray(JSON.toJSONString(d.getResult()), VeBaseGrade.class));
            for (int j = 0; j < grade.getResult().size(); j++) {
                if (j == grade.getResult().size() - 1 && list.get(i).getXN() != Integer.parseInt(grade.getResult().get(grade.getResult().size() - 1).getNjdm())) {
                    return Result.error("学年信息有误");
                }
                if (list.get(i).getXN() == Integer.parseInt(grade.getResult().get(j).getNjdm())) {
                    veZsEnroll.setXN(Integer.parseInt(grade.getResult().get(j).getNjdm()));
                    break;
                }
            }
//            String semesterUrl = "/common/veCommon/querySemesterList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
//            BasicResponseBO<List> e = getUrl(semesterUrl, List.class);
            BasicResponseBO<List<VeBaseSemester>> semester = new BasicResponseBO<>();
            semester.setResult(veBaseSemesterService.list());//202.9.2
//            semester.setSuccess(e.getSuccess()).setMessage(e.getMessage())
//                    .setResult(e.getResult() == null ? null :
//                            JSON.parseArray(JSON.toJSONString(e.getResult()), VeBaseSemester.class));
            for (int j = 0; j < semester.getResult().size(); j++) {
                if (j == semester.getResult().size() - 1 && !list.get(i).getXQ().equals(semester.getResult().get(semester.getResult().size() - 1).getXqmc())) {
                    return Result.error("学期信息有误");
                }
                if (list.get(i).getXQ().equals(semester.getResult().get(j).getXqmc())) {
                    veZsEnroll.setXQ((semester.getResult().get(j).getXqmc()));
                    break;
                }
            }
            result.add(veZsEnroll);
        }
        for (VeZsEnroll veZsEnroll : result) {
            veZsEnrollMapper.insertMock(veZsEnroll);
        }
        return Result.ok("文件导入成功！数据行数：" + list.size());
    }

    //李少君
    @Override
    public boolean insertBaomingMessage(VeZsSingleEnrollExpansion veZsSingleEnrollExpansion) {
        return veZsEnrollMapper.insertBaomingMessage(veZsSingleEnrollExpansion);
    }

    @Override
    public FormalInfoVo getFormalAdmission(String XM, String KSH, String SFZH) {
        FormalInfoVo formalInfoVo = new FormalInfoVo();
        VeZsEnroll veZsEnroll = veZsEnrollMapper.getFormalAdmission(XM, KSH, SFZH);
        formalInfoVo.setKSH(veZsEnroll.getKSH());
        formalInfoVo.setXM(veZsEnroll.getXM());
        formalInfoVo.setXN(veZsEnroll.getXN());
        formalInfoVo.setXQ(veZsEnroll.getXQ());
        formalInfoVo.setSFZH(veZsEnroll.getSFZH());
        formalInfoVo.setZXMC(veZsEnroll.getZXMC());
        formalInfoVo.setZF(veZsEnroll.getZF());
        formalInfoVo.setKM1(veZsEnroll.getKM1());
        formalInfoVo.setKM2(veZsEnroll.getKM2());
        formalInfoVo.setKM3(veZsEnroll.getKM3());
//        String majorUrl = "/common/veCommon/querySpecialtyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67" + "&specialtyId=" + veZsEnroll.getFormalmajor();
//        BasicResponseBO<VeBaseSpecialty> b = getUrl(majorUrl, VeBaseSpecialty.class);
        BasicResponseBO<VeBaseSpecialty> b = new BasicResponseBO<>();
        b.setResult(veBaseSpecialtyService.getById(veZsEnroll.getFormalmajor()));//2021.9.2
        formalInfoVo.setFormalmajor(b.getResult().getZymc());
//        String url = "/common/veCommon/queryFacultyList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
//        List<VeBaseFaculty> faculties = JSONObject.parseArray(JSON.toJSONString(getUrl(url, List.class).getResult()), VeBaseFaculty.class);
        List<VeBaseFaculty> faculties = veBaseFacultyService.list();//2021.9.2
        for (int i = 0; i < faculties.size(); i++) {
            if (veZsEnroll.getFormalmajor() == faculties.get(i).getId()) {
                formalInfoVo.setFaculty(faculties.get(i).getYxmc());
                break;
            }
        }
        return formalInfoVo;
    }

    @Override
    public Result<?> mockAdmissionImport(List<MockAdmissionVo> list) {
        List<VeZsEnroll> result = new ArrayList<>();
//        String url = "/common/veCommon/queryFacultyList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
//        BasicResponseBO<List> b = getUrl(url, List.class);
        BasicResponseBO<List<VeBaseFaculty>> responseBO = new BasicResponseBO<>();
        responseBO.setResult(veBaseFacultyService.list());//2021.9.2
//        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
//                .setResult(b.getResult() == null ? null :
//                        JSON.parseArray(JSON.toJSONString(b.getResult()), VeBaseFaculty.class));
//        List<VeBaseFaculty> faculties = JSONObject.parseArray(JSON.toJSONString(getUrl(url, List.class).getResult()), VeBaseFaculty.class);
        for (int i = 0; i < list.size(); i++) {
            VeZsEnroll veZsEnroll = new VeZsEnroll();
            veZsEnroll.setKSH(list.get(i).getKSH());
            veZsEnroll.setXN(list.get(i).getXN());
            veZsEnroll.setXQ(list.get(i).getXQ());
            veZsEnroll.setXM(list.get(i).getXM());
            veZsEnroll.setZF(list.get(i).getZF());
            veZsEnroll.setKM1(list.get(i).getKM1());
            veZsEnroll.setKM2(list.get(i).getKM2());
            veZsEnroll.setKM3(list.get(i).getKM3());
            veZsEnroll.setXN(list.get(i).getXN());
            veZsEnroll.setXQ(list.get(i).getXQ());
            veZsEnroll.setZXMC(list.get(i).getZXMC());
            veZsEnroll.setSFZH(list.get(i).getSFZH());
            for (int j = 0; j < responseBO.getResult().size(); j++) {
//                String majorUrl = "/common/veCommon/querySpecialtyListByFalId?falId=" + responseBO.getResult().get(j).getId() + "&interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
////                List<VeBaseSpecialty> major = JSONObject.parseArray(JSON.toJSONString(getUrl(majorUrl, List.class).getResult()), VeBaseSpecialty.class);
//                BasicResponseBO<List> c = getUrl(majorUrl, List.class);
                BasicResponseBO<List<VeBaseSpecialty>> major = new BasicResponseBO<>();
                QueryWrapper<VeBaseSpecialty> queryWrapper = new QueryWrapper<>();
                queryWrapper.eq("falId",responseBO.getResult().get(j).getId());
                major.setResult(veBaseSpecialtyService.list(queryWrapper));//2021.9.2
//                major.setSuccess(c.getSuccess()).setMessage(c.getMessage())
//                        .setResult(c.getResult() == null ? null :
//                                JSON.parseArray(JSON.toJSONString(c.getResult()), VeBaseSpecialty.class));
                Boolean majorIsTrue = false;
                for (int k = 0; k < major.getResult().size(); k++) {
                    if (k == major.getResult().size() - 1 &&j == responseBO.getResult().size() -1 && !list.get(i).getMajor().equals(major.getResult().get(major.getResult().size() - 1).getZymc())) {
                        return Result.error("专业信息有误");
                    }
                    if (list.get(i).getMajor().equals(major.getResult().get(k).getZymc())) {
                        veZsEnroll.setMajor(major.getResult().get(k).getId().intValue());
                        majorIsTrue = true;
                        break;
                    }
                }
                if (majorIsTrue){
                    break;
                }
            }
//            String gradeUrl = "/common/veCommon/queryGradeList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
//            BasicResponseBO<List> d = getUrl(gradeUrl, List.class);
            BasicResponseBO<List<VeBaseGrade>> grade = new BasicResponseBO<>();
            grade.setResult(veBaseGradeService.list());
//            grade.setSuccess(d.getSuccess()).setMessage(d.getMessage())
//                    .setResult(d.getResult() == null ? null :
//                            JSON.parseArray(JSON.toJSONString(d.getResult()), VeBaseGrade.class));
            for (int j = 0; j < grade.getResult().size(); j++) {
                if (j == grade.getResult().size() - 1 && list.get(i).getXN() != Integer.parseInt(grade.getResult().get(grade.getResult().size() - 1).getNjdm())) {
                    return Result.error("学年信息有误");
                }
                if (list.get(i).getXN() == Integer.parseInt(grade.getResult().get(j).getNjdm())) {
                    veZsEnroll.setXN(Integer.parseInt(grade.getResult().get(j).getNjdm()));
                    break;
                }
            }
//            String semesterUrl = "/common/veCommon/querySemesterList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
//            BasicResponseBO<List> e = getUrl(semesterUrl, List.class);
            BasicResponseBO<List<VeBaseSemester>> semester = new BasicResponseBO<>();
            semester.setResult(veBaseSemesterService.list());
//            semester.setSuccess(e.getSuccess()).setMessage(e.getMessage())
//                    .setResult(e.getResult() == null ? null :
//                            JSON.parseArray(JSON.toJSONString(e.getResult()), VeBaseSemester.class));
            for (int j = 0; j < semester.getResult().size(); j++) {
                if (j == semester.getResult().size() - 1 && !list.get(i).getXQ().equals(semester.getResult().get(semester.getResult().size() - 1).getXqmc())) {
                    return Result.error("学期信息有误");
                }
                if (list.get(i).getXQ().equals(semester.getResult().get(j).getXqmc())) {
                    veZsEnroll.setXQ((semester.getResult().get(j).getXqmc()));
                    break;
                }
            }
            result.add(veZsEnroll);
        }
        for (VeZsEnroll veZsEnroll : result) {
            veZsEnrollMapper.insertMock(veZsEnroll);
        }
        return Result.ok("文件导入成功！数据行数：" + list.size());
    }

    /**
     * 调用公告数据接口
     */
    private <T> BasicResponseBO<T> getUrl(String url, Class<T> clazz) {
        try {
            url = dirHost + url;
            String result = HttpURLConnectionUtil.doGet(url);
            BasicResponseBO<T> re = mapper.readValue(result, new TypeReference<BasicResponseBO<T>>() {
            });
            T body = mapper.readValue(mapper.writeValueAsString(re.getResult()), clazz);
            re.setResult(body);
            return re;
        } catch (Exception e) {
            return new BasicResponseBO<T>().setSuccess(false).setMessage("操作失败");
        }
    }
}
