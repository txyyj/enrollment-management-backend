package org.edu.modules.enroll.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Auther 李少君
 * @Date 2021-07-25 13:56
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeZsFenbanVo对象", description = "学生分班列表")

public class VeZsFenbanExcelVo {

    /** id */
    @ApiModelProperty(value = "id")
    private Long id;

    /*考生号*/
    @Excel(name = "考生号", width = 15)
    @TableField(value = "KSH")
    private String ksh;

    /** 学生姓名 */
    @Excel(name = "学生姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private String xm;

    /** 身份证号 */
    @Excel(name = "身份证号", width = 15)
    @ApiModelProperty(value = "身份证号")
    @TableField(value = "SFZH")
    private String sfzh;

    /** 学生性别 */
    @Excel(name = "学生性别", width = 15)
    @ApiModelProperty(value = "性别")
    private String xb;

    /** 出生年月 */
    @Excel(name = "出生年月", width = 15)
    @TableField(value = "CSNY")
    private String csny;

    /** 中学名称 */
    @Excel(name = "中学名称", width = 15)
    @TableField(value = "ZXMC")
    private String zxmc;

    /** 邮政编码 */
    @Excel(name = "邮政编码", width = 15)
    @TableField(value = "YZBM")
    private String yzbm;

    /** 邮寄地址 */
    @Excel(name = "邮寄地址", width = 15)
    @TableField(value = "YJDZ")
    private String yjdz;

    /** 邮编 */
    @Excel(name = "邮编", width = 15)
    @TableField(value = "YB")
    private String yb;

    /** 联系电话1 */
    @Excel(name = "联系电话1", width = 15)
    @TableField(value = "LXDH1")
    private String lxdh1;

    /** 联系电话2 */
    @Excel(name = "联系电话2", width = 15)
    @TableField(value = "LXDH2")
    private String lxdh2;

    /** 投档成绩 */
    @Excel(name = "投档成绩", width = 15)
    @TableField(value = "TDCJ")
    private String tdcj;

    /** 志愿 */
    @Excel(name = "志愿", width = 15)
    @TableField(value = "ZY")
    private String zy;

    /** 录取专业 */
    @Excel(name = "录取专业", width = 15)
    @TableField(value = "LQZY")
    private String lqzy;

    /** 寄件人 */
    @Excel(name = "寄件人", width = 15)
    @TableField(value = "JJR")
    private String jjr;

    /** 寄件人电话 */
    @Excel(name = "寄件人电话", width = 15)
    @TableField(value = "JJRDH")
    private String jjrdh;

    /** 寄件人地址 */
    @Excel(name = "寄件人地址", width = 15)
    @TableField(value = "JJRDZ")
    private String jjrdz;

    /** 收件人 */
    @Excel(name = "收件人", width = 15)
    @TableField(value = "SJR")
    private String sjr;

    /** 收件人家庭地址 */
    @Excel(name = "收件人家庭地址", width = 15)
    @TableField(value = "SJRJTDZ")
    private String sjrjtdz;

    /** 邮件号 */
    @Excel(name = "邮件号", width = 15)
    @TableField(value = "YJH")
    private String yjh;

    /** 重量 */
    @Excel(name = "重量", width = 15)
    @TableField(value = "ZL")
    private String zl;

//    /** 学生学号 */
    @Excel(name = "学生学号", width = 15)
    @ApiModelProperty(value = "学号")
    private String xh;

    /** 所属年级 */
    @Excel(name = "所属年级", width = 15)
    @ApiModelProperty(value = "所属年级")
    private String grade;

    /*入学年月*/
    @Excel(name = "入学年月", width = 15)
    @TableField(value = "RXNY")
    private Long rxny;

//    /** 入学总分 */
//    @Excel(name = "入学总分", width = 15)
//    @ApiModelProperty(value = "入学总分")
//    private Integer mark;

    /** 招生类型 */
    @Excel(name = "招生类型", width = 15)
    @ApiModelProperty(value = "招生类型")
    private String enrollType;

    /** 录取批次 */
    @Excel(name = "录取批次", width = 15)
    @ApiModelProperty(value = "录取批次")
    private String luqu;

    /** 录取校区 */
    @Excel(name = "录取校区", width = 15)
    @ApiModelProperty(value = "录取校区")
    @TableField(value = "campusId")
    private String campus;

    /*录取院系*/
    @Excel(name = "录取院系", width = 15)
    @ApiModelProperty(value = "录取院系")
    private String faculty;

    /** 录取专业 */
    @Excel(name = "专业", width = 15)
    @TableField(value = "specId")
    private String spec;

    /** 分配班级 */
    @Excel(name = "分配班级", width = 15)
    @TableField(value = "banji")
    private String banji;

    /** 分配状态 */
    @Excel(name = "分配状态", width = 15)
    @ApiModelProperty(value = "分配状态")
    @TableField(value = "statuId")
    private String state;





    /** 民族 */
    @Excel(name = "民族", width = 15)
    @ApiModelProperty(value = "民族")
    @TableField(value = "item_text")
    private String mz;

    /*报名号*/
    @Excel(name = "报名号", width = 15)
    @TableField(value = "BMH")
    private String bmh;
//
//    /*就读方式*/
//    @Excel(name = "就读方式", width = 15)
//    @TableField(value = "JDFS")
//    private String jdfs;
//
//    /*当前状态码*/
//    @Excel(name = "当前状态码", width = 15)
//    @TableField(value = "XSDQZTM")
//    private String xsdqztm;
//
//
//
    /*学制*/
    @Excel(name = "学制", width = 15)
    @TableField(value = "XZ")
    private String xz;
//
    /*户口所在省份*/
    @Excel(name = "户口所在省份", width = 15)
    @TableField(value = "province")
    private String province;

    /*户口所在市*/
    @Excel(name = "户口所在市", width = 15)
    @TableField(value = "city")
    private String city;

    /*户口所在区*/
    @Excel(name = "户口所在区", width = 15)
    @TableField(value = "county")
    private String county;
//
//    /*生源地省*/
//    @Excel(name = "生源地省", width = 15)
//    @TableField(value = "sheng")
//    private String sheng;
//
//    /*生源地市*/
//    @Excel(name = "生源地市", width = 15)
//    @TableField(value = "shi")
//    private String shi;
//
//    /*生源地区*/
//    @Excel(name = "生源地区", width = 15)
//    @TableField(value = "qu")
//    private String qu;
//
//    /*是否是困难生*/
//    @Excel(name = "是否是困难生", width = 15)
//    @TableField(value = "SFSKNS")
//    private String sfskns;
//
//    /*准考证号*/
//    @Excel(name = "准考证号", width = 15)
//    @TableField(value = "ZKZH")
//    private String zkzh;






}
