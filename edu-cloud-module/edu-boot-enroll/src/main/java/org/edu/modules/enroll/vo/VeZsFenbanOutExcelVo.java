package org.edu.modules.enroll.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Auther 李少君
 * @Date 2021-08-08 21:17
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsFenbanOutExcelVo{

    /** id */
    @ApiModelProperty(value = "id")
    private Long id;

    /** 学生姓名 */
    @Excel(name = "学生姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private String xm;

    /** 学生学号 */
    @Excel(name = "学生学号", width = 15)
    @ApiModelProperty(value = "学号")
    private String xh;

    /** 所属年级 */
    @Excel(name = "所属年级", width = 15)
    @ApiModelProperty(value = "所属年级")
    private String grade;

    /** 入学总分 */
    @Excel(name = "入学总分", width = 15)
    @ApiModelProperty(value = "入学总分")
    private Integer mark;

    /** 招生类型 */
    @Excel(name = "招生类型", width = 15)
    @ApiModelProperty(value = "招生类型")
    private String enrollType;

    /** 录取批次 */
    @Excel(name = "录取批次", width = 15)
    @ApiModelProperty(value = "录取批次")
    private String luqu;

    /** 录取校区 */
    @Excel(name = "录取校区", width = 15)
    @ApiModelProperty(value = "录取校区")
    @TableField(value = "campusId")
    private String campus;

    /** 录取专业 */
    @Excel(name = "录取专业", width = 15)
    @TableField(value = "specId")
    private String spec;

    /** 分配班级 */
    @Excel(name = "分配班级", width = 15)
    @TableField(value = "banji")
    private String banji;

    /** 分配状态 */
    @Excel(name = "分配状态", width = 15)
    @ApiModelProperty(value = "分配状态")
    @TableField(value = "statuId")
    private String state;

    /** 学生性别 */
    @Excel(name = "学生性别", width = 15)
    @ApiModelProperty(value = "性别")
    private String xb;
}
