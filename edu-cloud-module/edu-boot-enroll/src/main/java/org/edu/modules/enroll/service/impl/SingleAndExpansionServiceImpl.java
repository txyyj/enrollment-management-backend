package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeZsSingleEnrollExpansion;
import org.edu.modules.enroll.mapper.SingleAndExpansionMapper;
import org.edu.modules.enroll.service.SingleAndExpansionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.service.impl
 * @date 2021/7/28 18:32
 */
@Service
public class SingleAndExpansionServiceImpl extends ServiceImpl<SingleAndExpansionMapper, VeZsSingleEnrollExpansion> implements SingleAndExpansionService {

    @Resource
    private SingleAndExpansionMapper mapper;

    @Override
    public List<VeZsSingleEnrollExpansion> getSingleAndExpansion(Long specialtyId, String stuName, String sfzh, Integer currentPage, Integer pageSize, Integer isCheck) {
        List<VeZsSingleEnrollExpansion> list = mapper.getSingleEnrollExpansion(specialtyId,stuName,sfzh,currentPage,pageSize,isCheck);
        return list;
    }

    @Override
    public Integer countSingleAndExpansion(Long specialtyId, String stuName, String sfzh, Integer isCheck) {
        Integer res = mapper.countSingleExpansion(specialtyId,stuName,sfzh,isCheck);
        return res;
    }
}
