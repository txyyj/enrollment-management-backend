package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.edu.common.api.vo.Result;
import org.edu.modules.enroll.entity.VeZsEnroll;
import org.edu.modules.enroll.entity.VeZsSingleEnrollExpansion;
import org.edu.modules.enroll.vo.*;

import java.util.HashMap;
import java.util.List;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/7/24 10:59
 */
//public interface VeZsEnrollService {
public interface VeZsEnrollService extends IService<VeZsEnroll> {
    //展示数据的方法
    HashMap<String, Object> showScoreList(String ZKZH, String XM, Integer XN, String XQ, Integer currentPage, Integer pageSize);

    HashMap<String, Object> showPreAdmissionList(String KSH, String XM, Integer XN, String XQ, Integer currentPage, Integer pageSize);

    HashMap<String, Object> showMockAdmissionList(String KSH, String XM, Integer XN, String XQ, Integer currentPage, Integer pageSize);

    HashMap<String, Object> showFormalAdmissionList(String KSH, String XM, Integer XN, String XQ, Integer currentPage, Integer pageSize);

    List<ScoreAnnouncementVo> exportScore();

    List<PreAdmissionVo> exportPreAdmission();

    List<MockAdmissionVo> exportMockAdmission();

    List<FormalAdmissionVo> exportFormalAdmission();

    Result<?> importScore(List<ScoreAnnouncementVo> list) throws Exception;

    Result<?> preAdmissionImport(List<PreAdmissionVo> list) throws Exception;

    Result<?> mockAdmissionImport(List<MockAdmissionVo> list) throws Exception;

    Result<?> formalAdmissionImport(List<FormalAdmissionVo> list) throws Exception;

    //李少君--网上报名插入数据
    boolean insertBaomingMessage(VeZsSingleEnrollExpansion veZsSingleEnrollExpansion);

    FormalInfoVo getFormalAdmission(String XM, String KSH, String SFZH);
}
