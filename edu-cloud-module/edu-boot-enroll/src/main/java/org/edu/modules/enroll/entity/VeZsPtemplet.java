package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 通知书模板表
 * @Author:  wcj
 * @Date:  2021-04-20
 * @Version:  V1.0
 */

@Data
@TableName("ve_zs_ptemplet")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsPtemplet implements Serializable {

    /** id */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private int id;

    /** 模板名称 */
    @Excel(name = "模板名称", width = 15)
    @ApiModelProperty(value = "模板名称")
    @TableField(value = "name")
    private String name;

    /** 宽度 */
    @Excel(name = "宽度", width = 15)
    @ApiModelProperty(value = "宽度")
    @TableField(value = "width")
    private Double width;

    /** 高度 */
    @Excel(name = "高度", width = 15)
    @ApiModelProperty(value = "高度")
    @TableField(value = "height")
    private Double height;

    /** 图片ID */
    @Excel(name = "图片ID", width = 15)
    @ApiModelProperty(value = "图片ID")
    @TableField(value = "fileId")
    private int fileId;

    /** 打印代码 */
    @Excel(name = "打印代码", width = 15)
    @ApiModelProperty(value = "打印代码")
    @TableField(value = "code")
    private String code;

    /** 创建人 */
    @Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
    @TableField(value = "create_by")
    private String createBy;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 15)
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time")
    private Date createTime;

    /** 修改人 */
    @Excel(name = "修改人", width = 15)
    @ApiModelProperty(value = "修改人")
    @TableField(value = "update_by")
    private String updateBy;

    /** 更新时间 */
    @Excel(name = "更新时间", width = 15)
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time")
    private Date updateTime;

    /** 终端系统ID */
    @Excel(name = "终端系统ID", width = 15)
    @ApiModelProperty(value = "终端系统ID")
    @TableField(value = "terminalId")
    private int terminalId;

    /** 逻辑删除 */
    @Excel(name = "逻辑删除", width = 15)
    @ApiModelProperty(value = "逻辑删除 1(true)已删除; 0(false)未删除")
    @TableField(value = "is_deleted")
    @TableLogic
    private int isDeleted;

    /** 启用状态0禁用1启用 */
    @Excel(name = "启用状态", width = 15)
    @ApiModelProperty(value = "启用状态0禁用1启用")
    @TableField(value = "status")
    private int status;

    /**模板类型*/
    @Excel(name = "模板类型", width = 15)
    @ApiModelProperty(value = "模板类型通知书enroll，ems,信封envelope")
    @TableField(value = "type")
    private String type;

    /**打印方向类型*/
    @Excel(name = "打印方向类型", width = 15)
    @ApiModelProperty(value = "打印方向类型")
    @TableField(value = "printType")
    private String printType;

}
