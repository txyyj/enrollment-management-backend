package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.enroll.entity.VeFileFiles;
import org.edu.modules.enroll.entity.VeZsPlan;
import org.edu.modules.enroll.vo.VeZsPlanVo;

import java.util.List;

public interface VeZsPlanService extends IService<VeZsPlan> {
    //显示数据的方法
    List<VeZsPlanVo> lianbiao(String name, Long falId, Long specialId);
    //l逻辑删除
    Integer delQuarter(Integer id);
    //添加
    Integer addZsjQuarter (
           Integer falId,
            Integer specId,
           String ZSNF,
           Integer ZSJ,
        Integer BJS,
          Integer NANSRS,
            Integer NVSRS,
          Integer fileId,
        String ZYYQ,
        String PYMB,
      String ZYZYKC,
      String BXTJ,
           String remark,
        Integer ZRS


    );
    //编辑
    Integer updateZsQuarter(Integer falId,
                            Integer specId,
                            String ZSNF,
                            Integer ZSJ,
                            Integer BJS,
                           Integer NANSRS,
                           Integer NVSRS,
                            Integer fileId,
                           String ZYYQ,
                           String PYMB,
                           String ZYZYKC,
                           String BXTJ,
                           String remark,
                            Integer ZRS,
                            Integer id
    );


    List<VeZsPlanVo>  ZsJy ( Integer id);
    //上传附件
    boolean addFujian(VeFileFiles veFileFiles);
    //查刚上传附件的id
    Integer findFileId(String fileUrl);
    //查url
    String  findUrl(Integer id);
}
