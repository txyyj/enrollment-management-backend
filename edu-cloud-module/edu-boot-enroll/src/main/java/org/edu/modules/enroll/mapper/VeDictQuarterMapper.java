package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeDictQuarter;
import org.edu.modules.enroll.vo.VeDictQuarterVo;
import org.edu.modules.enroll.vo.VeDictYearsVo;

import java.util.Date;
import java.util.List;

public interface VeDictQuarterMapper extends BaseMapper<VeDictQuarter> {
/** @Description: 招生季接口,继承被序列化的招生季表对象
 * @Author: ZH
 * @Date:   2021-04-06
 * @Version: V1.0
 *
 */






//    // 修改新招生季
//    Integer AddupdateIsCurOne (@Param("id")Integer id);
////将特定招生季改为非当前招生季
//Integer  updateIsCur(@Param("code")String code,@Param("name")String name,@Param("id")Integer id);
//
//// 修改招生季的名称
//    Integer updateName(@Param("name")String name,@Param("id")Integer id);
//    //修改招生季的代码
//    Integer updateCode(@Param("code")String code,@Param("id")Integer id);
//
//// 新增校验相同的招生季代码，招生季名称
//List<VeDictQuarterVo>  CheckAddQuarter (@Param("code")String code,@Param("name")String name);
    //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//查询所有 (pageNo-1)*pageSize, pageSize
    List<VeDictQuarterVo> selectById();

    //l逻辑删除
    Integer delQuarter(@Param("code")String code,@Param("id")Integer id);
    //将所有的招生季设为否
    Integer updateAnyIsCur();
    //设置当前招生季
    Integer  updateIsCurOne( @Param("isCur")Integer isCur,@Param("id")Integer id);
    //编辑
    Integer    updateQuarter (@Param("code")String code,
                              @Param("id")Integer id,
                              @Param("name")String name,
                              @Param("year")String year,
                              @Param("isCur")Integer isCur,
                              @Param("rxny")String rxny,
                              @Param("start_Time")Date start_Time,
                              @Param("end_Time")Date end_Time);

    //校验相同的招生季代码，招生季名称
    List<VeDictQuarterVo> CheckQuarter(@Param("code")String code,@Param("id")Integer id);
   // <!--添加新招生季前进行的校验-->
   List<VeDictQuarterVo> CheckAddQuarter(@Param("code")String code);
    //添加新的招生季
    Integer addQuarter(@Param("code")String code,
                       @Param("name")String name,
                       @Param("year")String year,
                       @Param("isCur")Integer isCur,
                       @Param("rxny")String rxny,
                       @Param("start_Time")Date start_Time,
                       @Param("end_Time")Date end_Time);

}
