package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.annotations.ApiParam;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeZsReportMsg;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.mapper
 * @date 2021/7/19 21:13
 */
public interface VeZsReportMessageMapper extends BaseMapper<VeZsReportMsg> {
    List<VeZsReportMsg> getReportMessageList(@Param("endPlaceId")String endPlaceId, @Param("staTime")String staTime,
                                             @Param("endTime")String endTime, @Param("isNeed")Integer isNeed, @Param("currentPage")Integer currentPage,
                                             @Param("pageSize")Integer pageSize);
    Integer countReportMessage(@Param("endPlaceId")String endPlaceId, @Param("staTime")String staTime,
                               @Param("endTime")String endTime, @Param("isNeed")Integer isNeed);
    Integer deleteById(@Param("id")Integer id);
}
