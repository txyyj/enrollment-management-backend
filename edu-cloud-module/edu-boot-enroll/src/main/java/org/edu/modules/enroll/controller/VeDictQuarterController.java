package org.edu.modules.enroll.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.enroll.entity.VeDictQuarter;

import org.edu.modules.enroll.entity.VeDictYears;
import org.edu.modules.enroll.service.VeDictQuarterService;
import org.edu.modules.enroll.service.VeDictYearsService;
import org.edu.modules.enroll.vo.VeDictQuarterVo;
import org.edu.modules.enroll.vo.VeDictYearsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
/**
 * @Description:  当前招生季数据处理
 * @Author:  zh
 * @Date:  2021-04-05
 * @Version:  V1.0
 */

@Api(tags="招生季显示")
@RestController
@RequestMapping("enroll/cur")
@Slf4j
public class VeDictQuarterController extends BaseController<VeDictQuarter, VeDictQuarterService> {
    @Autowired
    private VeDictYearsService veDictYearsService;

    //    @Autowired
     @Resource
    private VeDictQuarterService veDictQuarterService;

    /**
     * 新增信息
     *
     * @param
     * @return
     */



    @AutoLog(value = "招生季管理-新增招生季")
    @ApiOperation(value="新增招生季", notes="招生季管理-新增招生季")
    @PostMapping("/admissions")
    public Result<?> admissions(@RequestParam("code")String code,
                                @RequestParam("name")String name,
                                @RequestParam("year")String year,
                                @RequestParam("isCur")Integer isCur,
                                @RequestParam("RXNY")String rxny,
                                @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")  @RequestParam("start_Time") Date start_Time,
                                @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")   @RequestParam("end_Time") Date end_Time
//                                @RequestParam("is_deleted")Integer is_deleted,
                                  //  @RequestParam(value = "key") Integer id
    ) {

if(veDictQuarterService.addQuarter(code, name, year, isCur, rxny, start_Time, end_Time) == 1){
    return Result.OK("新增招生季成功");
}

        //发送请求到前端
        return Result.OK("新增招生季失败");
    }

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    @AutoLog(value = "招生季管理-编辑招生季")
    @ApiOperation(value = "编辑招生季", notes = "招生季管理-编辑招生季")
    @PostMapping("/EDIT")
    public Result<?> EDIT(@RequestParam("code") String code,
                                @RequestParam("name") String name,
                                @RequestParam("year") String year,
                                @RequestParam("isCur") Integer isCur,
                                @RequestParam("RXNY") String rxny,
                          @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")  @RequestParam("start_Time") Date start_Time,
                          @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")   @RequestParam("end_Time") Date end_Time,
                                @RequestParam("is_deleted") Integer is_deleted,
                              @RequestParam(value = "key") Integer id
   ) {
        if (veDictQuarterService.updateQuarter(code, id, name, year, isCur, rxny, start_Time, end_Time) == 1) {
            System.out.println("测试经过实现类以后的返回值"+veDictQuarterService.updateQuarter(code, id, name, year, isCur, rxny, start_Time, end_Time)+"为");
            return Result.OK("修改招生季的入学年份成功");
        }
        return Result.OK("招生季已存在");
    }

    //显示
    @AutoLog(value = "招生季数据-显示表格")
    @ApiOperation(value = "显示表格", notes = "招生季数据-显示表格")
    @PostMapping("/zsjxs")
    public Result<?> zsjxs() {
//    QueryWrapper<VeDictQuarter> wrapper = new QueryWrapper<>();
//    wrapper.eq("name","招生季名称");
        List<VeDictQuarterVo> list = veDictQuarterService.selectById();
        System.out.println(list);
        //发送请求到前端
        return Result.OK(list);

    }
    //年份下拉框
    @AutoLog(value = "招生季数据-年份")
    @ApiOperation(value = "年份", notes = "招生季数据-年份")
    @PostMapping("/zsjnf")
    public Result<?> zsjnf() {
        QueryWrapper<VeDictYears> wrapper = new QueryWrapper<>();
        wrapper.eq("code", "年份代码");
        List<VeDictYearsVo> curYearlist = veDictYearsService.displayYears();
        //发送请求到前端
        return Result.OK(curYearlist);

    }

    //删除
    @AutoLog(value = "招生季管理-删除")
    @ApiOperation(value = "删除", notes = "招生季管理-删除")
    @PostMapping("/sczsj")
    //传到前端的参数路径
    public Result<?> sczsj(@RequestParam(value = "code") String code,
                           @RequestParam(value = "key") Integer id) {
        if (veDictQuarterService.delQuarter(code, id) != 1) {
            return Result.OK("删除失败");
        }
        //发送请求到前端
        return Result.OK("删除成功");

    }
    //设置当前招生季
    @AutoLog(value = "招生季数据-设置当前招生季")
    @ApiOperation(value = "设置当前招生季", notes = "招生季数据-设置当前招生季")
    @PostMapping("/szdqzsj")
    public Result<?> szdqzsj(
            @RequestParam(value = "isCur") Integer isCur,
            @RequestParam(value = "key") Integer id
    ) {
if(veDictQuarterService.updateIsCurOne(isCur,id)==1){
    return Result.OK("设置当前招生季成功");
}
return Result.OK("设置当前招生季失败");


    }
    }


