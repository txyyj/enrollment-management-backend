package org.edu.modules.common.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.query.QueryGenerator;
import org.edu.common.system.vo.LoginUser;
import org.edu.modules.common.entity.VeBaseInterface;
import org.edu.modules.common.entity.VeBaseInterfaceUser;
import org.edu.modules.common.service.IVeBaseInterfaceService;
import org.edu.modules.common.service.IVeBaseInterfaceUserService;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Api(tags={"数据应用授权"})
@RestController
@RequestMapping({"/common/veAuthorization"})
@ApiSort(60)
public class VeDataApplicationAuthorizationController
{
    private static final Logger log = LoggerFactory.getLogger(VeDataApplicationAuthorizationController.class);
    @Autowired
    private IVeBaseInterfaceUserService veBaseInterfaceUserService;
    @Autowired
    private IVeBaseInterfaceService veBaseInterfaceService;

    @AutoLog("接口用户信息-分页列表查询")
    @ApiOperation(value="接口用户信息-分页列表查询", notes="接口用户信息-分页列表查询")
    @GetMapping({"/getInterfaceUserPageList"})
    public Result<?> getInterfaceUserPageList(VeBaseInterfaceUser veBaseInterfaceUser, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseInterfaceUserService.getInterfaceUserPageList(veBaseInterfaceUser);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.ok(pageInfo);
    }

    @AutoLog("接口信息-分页列表查询")
    @ApiOperation(value="接口信息-分页列表查询", notes="接口信息-分页列表查询")
    @GetMapping({"/getInterfacePageList"})
    public Result<?> getInterfacePageList(VeBaseInterface veBaseInterface, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseInterfaceService.getInterfacePageList(veBaseInterface);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.ok(pageInfo);
    }

    @AutoLog("接口信息列表")
    @ApiOperation(value="接口信息列表", notes="接口信息列表")
    @GetMapping({"/getInterfaceList"})
    public Result<?> getInterfaceList()
    {
        List<Map<String, Object>> list = this.veBaseInterfaceService.getInterfaceList();
        return Result.ok(list);
    }

    @AutoLog("添加-接口用户信息")
    @ApiOperation(value="添加-接口用户信息", notes="添加-接口用户信息")
    @PostMapping({"/addInterfaceUser"})
    public Result<?> addInterfaceUser(@RequestBody VeBaseInterfaceUser veBaseInterfaceUser)
    {
        this.veBaseInterfaceUserService.save(veBaseInterfaceUser);
        return Result.ok("添加成功!");
    }

    @AutoLog("添加-接口信息")
    @ApiOperation(value="添加-接口信息", notes="添加-接口信息")
    @PostMapping({"/addInterface"})
    public Result<?> addInterface(@RequestBody VeBaseInterface veBaseInterface)
            throws ParseException
    {
        veBaseInterface.setId(UUID.randomUUID().toString().replace("-", ""));
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date();
        try
        {
            veBaseInterface.setCreateTime(simpleDateFormat.parse(simpleDateFormat.format(date)));
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        LoginUser sysUser = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        veBaseInterface.setCreateId(sysUser.getId());
        this.veBaseInterfaceService.save(veBaseInterface);
        return Result.ok("添加成功!");
    }

    @AutoLog("接口权限-授权")
    @ApiOperation(value="接口权限-授权", notes="接口权限-授权")
    @PostMapping({"/editSysRoleUser"})
    public Result<?> editSysRoleUser(@RequestBody VeBaseInterfaceUser veBaseInterfaceUser)
    {
        if (veBaseInterfaceUser == null) {
            return Result.error("接口用户不能为空!");
        }
        if ((veBaseInterfaceUser.getId() == null) || ("".equals(veBaseInterfaceUser.getId()))) {
            return Result.error("接口用户不能为空!");
        }
        if ((veBaseInterfaceUser.getInterfaceIds() == null) || ("".equals(veBaseInterfaceUser.getInterfaceIds()))) {
            return Result.error("请选择接口后再行确定!");
        }
        this.veBaseInterfaceUserService.deleteInterfaceUserRelById(veBaseInterfaceUser.getId());
        String[] idArrray = veBaseInterfaceUser.getInterfaceIds().split(",");
        LoginUser sysUser = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        this.veBaseInterfaceUserService.addInterfaceUserRelBatch(veBaseInterfaceUser.getId(), idArrray, sysUser.getId());
        return Result.ok("授权成功!");
    }

    @AutoLog("编辑-接口用户信息")
    @ApiOperation(value="编辑-接口用户信息", notes="编辑-接口用户信息")
    @PostMapping({"/editInterfaceUser"})
    public Result<?> editInterfaceUser(@RequestBody VeBaseInterfaceUser veBaseInterfaceUser)
    {
        this.veBaseInterfaceUserService.updateById(veBaseInterfaceUser);
        return Result.ok("修改成功!");
    }

    @AutoLog("编辑-接口信息")
    @ApiOperation(value="编辑-接口信息", notes="编辑-接口信息")
    @PostMapping({"/editInterface"})
    public Result<?> editInterface(@RequestBody VeBaseInterface veBaseInterface)
    {
        this.veBaseInterfaceService.updateById(veBaseInterface);
        return Result.ok("修改成功!");
    }

    @AutoLog("删除-接口用户信息")
    @ApiOperation(value="删除-接口用户信息", notes="删除-接口用户信息")
    @PostMapping({"/deleteInterfaceUserBatch"})
    public Result<?> deleteInterfaceUserBatch(@RequestParam(name="ids", required=true) String ids)
    {
        this.veBaseInterfaceUserService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    @AutoLog("删除-接口信息")
    @ApiOperation(value="删除-接口信息", notes="删除-接口信息")
    @PostMapping({"/deleteInterfaceBatch"})
    public Result<?> deleteInterfaceBatch(@RequestParam(name="ids", required=true) String ids)
    {
        this.veBaseInterfaceService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    @ApiOperation(value="导出接口用户信息", notes="导出接口用户信息")
    public ModelAndView interfaceUserExportXls(VeBaseInterfaceUser veBaseInterfaceUser, HttpServletRequest request)
    {
        QueryWrapper<VeBaseInterfaceUser> queryWrapper = QueryGenerator.initQueryWrapper(veBaseInterfaceUser, request.getParameterMap());

        ModelAndView modelAndView = new ModelAndView(new JeecgEntityExcelView());
        List<VeBaseInterfaceUser> pageList = this.veBaseInterfaceUserService.list(queryWrapper);

        modelAndView.addObject("fileName", "接口用户信息");
        modelAndView.addObject("entity", VeBaseInterfaceUser.class);
        LoginUser user = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        modelAndView.addObject("params", new ExportParams("接口用户信息列表", "导出人:" + user.getRealname(), "导出信息"));
        modelAndView.addObject("data", pageList);
        return modelAndView;
    }
}
