package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseStudentInfo;

public abstract interface IVeBaseStudentInfoService
        extends IService<VeBaseStudentInfo>
{
    public abstract VeBaseStudentInfo getModelById(Integer paramInteger);

    public abstract List<Map<String, Object>> getStudentPageList(VeBaseStudentInfo paramVeBaseStudentInfo);
}

