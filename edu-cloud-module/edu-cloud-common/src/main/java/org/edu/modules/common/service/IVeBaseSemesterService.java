package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseSemester;

public abstract interface IVeBaseSemesterService
        extends IService<VeBaseSemester>
{
    public abstract List<Map<String, Object>> getSemesterPageList(VeBaseSemester paramVeBaseSemester);

    public abstract VeBaseSemester getSemesterByName(Integer paramInteger, String paramString);

    public abstract VeBaseSemester getSemesterByCode(Integer paramInteger, String paramString);

    public abstract VeBaseSemester getSemesterByIsCurrent(Integer paramInteger);

    public abstract List<VeBaseSemester> getByCodeName(String paramString1, String paramString2);

    public abstract VeBaseSemester getSemesterNew();
}
