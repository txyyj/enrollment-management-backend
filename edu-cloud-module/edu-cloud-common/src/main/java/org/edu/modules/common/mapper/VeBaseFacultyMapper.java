package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseFaculty;

public abstract interface VeBaseFacultyMapper
        extends BaseMapper<VeBaseFaculty>
{
    public abstract List<Map<String, Object>> getFacultyPageList(VeBaseFaculty paramVeBaseFaculty);

    public abstract VeBaseFaculty getFacultyByName(Integer paramInteger, String paramString);

    public abstract VeBaseFaculty getFacultyByCode(Integer paramInteger, String paramString);
}

