package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseAppUser;
import org.edu.modules.common.entity.VeBaseDataApply;
import org.edu.modules.common.entity.VeBaseDataApplyColumn;
import org.edu.modules.common.mapper.VeBaseAppUserMapper;
import org.edu.modules.common.mapper.VeBaseDataApplyColumnMapper;
import org.edu.modules.common.mapper.VeBaseDataApplyMapper;
import org.edu.modules.common.service.IVeBaseDataApplyService;
import org.edu.modules.common.vo.VeBaseDataApplyVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseDataApplyServiceImpl
        extends ServiceImpl<VeBaseDataApplyMapper, VeBaseDataApply>
        implements IVeBaseDataApplyService
{
    @Autowired
    private VeBaseDataApplyMapper veBaseDataApplyMapper;
    @Autowired
    private VeBaseDataApplyColumnMapper veBaseDataApplyColumnMapper;
    @Autowired
    private VeBaseDataApplyServiceImpl veBaseDataApplyService;
    @Autowired
    private VeBaseDataApplyColumnServiceImpl veBaseDataApplyColumnService;
    @Autowired
    private VeBaseAppUserMapper veBaseAppUserMapper;

    public List<Map<String, Object>> queryDataApplyPageList(VeBaseDataApply veBaseDataApply)
    {
        List<Map<String, Object>> list = this.veBaseDataApplyMapper.queryDataApplyPageList(veBaseDataApply);
        List<Map<String, Object>> detailList = this.veBaseDataApplyColumnMapper.queryDataApplyColumnPageList(null);
        if (list.size() > 0) {
            for (Map map : list)
            {
                List<Map<String, Object>> arrayList = new ArrayList();
                if (detailList.size() > 0) {
                    for (Map detailMap : detailList) {
                        if (map.get("id").toString().equals(detailMap.get("applyId").toString())) {
                            arrayList.add(detailMap);
                        }
                    }
                }
                map.put("columnName", arrayList);
            }
        }
        return list;
    }

    public List<Map<String, Object>> queryNotCheckDataApplyPageList(VeBaseDataApply veBaseDataApply)
    {
        List<Map<String, Object>> list = this.veBaseDataApplyMapper.queryNotCheckDataApplyPageList(veBaseDataApply);
        List<Map<String, Object>> detailList = this.veBaseDataApplyColumnMapper.queryDataApplyColumnPageList(null);
        if (list.size() > 0) {
            for (Map map : list)
            {
                List<Map<String, Object>> arrayList = new ArrayList();
                if (detailList.size() > 0) {
                    for (Map detailMap : detailList) {
                        if (map.get("id").toString().equals(detailMap.get("applyId").toString())) {
                            arrayList.add(detailMap);
                        }
                    }
                }
                map.put("columnName", arrayList);
            }
        }
        return list;
    }

    public List<Map<String, Object>> queryDataApplyProcessedPageList(VeBaseDataApply veBaseDataApply)
    {
        if ((!"".equals(veBaseDataApply.getUserId())) && (veBaseDataApply.getUserId() != null))
        {
            VeBaseAppUser veBaseAppUser = this.veBaseAppUserMapper.getAppUserByUserId(veBaseDataApply.getUserId());
            if ((veBaseAppUser != null) &&
                    (veBaseAppUser.getUserType().equals("0"))) {
                veBaseDataApply.setUserId("");
            }
        }
        else
        {
            veBaseDataApply.setUserId("-1");
        }
        return this.veBaseDataApplyMapper.queryDataApplyProcessedPageList(veBaseDataApply);
    }

    public void addDataApply(VeBaseDataApplyVo veBaseDataApplyVo)
    {
        VeBaseDataApply veBaseDataApply = new VeBaseDataApply();


        veBaseDataApply.setReason(veBaseDataApplyVo.getReason());

        veBaseDataApply.setCompanyName(veBaseDataApplyVo.getCompanyName());

        veBaseDataApply.setContent("字段内容");

        veBaseDataApply.setCreateTime(veBaseDataApplyVo.getCreateTime());

        veBaseDataApply.setStatus("0");

        veBaseDataApply.setUser(veBaseDataApplyVo.getUser());

        veBaseDataApply.setDepartmentName(veBaseDataApplyVo.getDepartmentName());

        veBaseDataApply.setSystemName(veBaseDataApplyVo.getSystemName());

        veBaseDataApply.setAppUserId("用户id");

        veBaseDataApply.setUserName("用户名称");

        veBaseDataApply.setTableName(veBaseDataApplyVo.getTableName());

        this.veBaseDataApplyService.save(veBaseDataApply);
    }

    public VeBaseDataApplyVo editEchoDataApply(int id)
    {
        VeBaseDataApply veBaseDataApply = (VeBaseDataApply)this.veBaseDataApplyService.getById(Integer.valueOf(id));

        VeBaseDataApplyVo veBaseDataApplyVo = new VeBaseDataApplyVo();

        List<VeBaseDataApplyColumn> veBaseDataApplyColumnList = this.veBaseDataApplyColumnService.list();
        for (VeBaseDataApplyColumn veBaseDataApplyColumn : veBaseDataApplyColumnList) {
            if (!veBaseDataApplyColumn.getApplyId().equals(veBaseDataApply.getId())) {}
        }
        veBaseDataApplyVo.setCompanyName(veBaseDataApply.getCompanyName());

        veBaseDataApplyVo.setSystemName(veBaseDataApply.getSystemName());

        veBaseDataApplyVo.setReason(veBaseDataApply.getReason());

        veBaseDataApplyVo.setCreateTime(veBaseDataApply.getCreateTime());

        veBaseDataApplyVo.setUser(veBaseDataApply.getUser());

        veBaseDataApplyVo.setDepartmentName(veBaseDataApply.getDepartmentName());

        veBaseDataApplyVo.setTableName(veBaseDataApply.getTableName());

        return veBaseDataApplyVo;
    }

    public void editDataApply(VeBaseDataApplyVo veBaseDataApplyVo)
    {
        VeBaseDataApply veBaseDataApply = (VeBaseDataApply)this.veBaseDataApplyService.getById(veBaseDataApplyVo.getId());

        List<VeBaseDataApplyColumn> veBaseDataApplyColumnList = this.veBaseDataApplyColumnService.list();


        veBaseDataApply.setReason(veBaseDataApplyVo.getReason());

        veBaseDataApply.setCompanyName(veBaseDataApplyVo.getCompanyName());

        veBaseDataApply.setCreateTime(veBaseDataApplyVo.getCreateTime());

        veBaseDataApply.setUser(veBaseDataApplyVo.getUser());

        veBaseDataApply.setDepartmentName(veBaseDataApplyVo.getDepartmentName());

        veBaseDataApply.setSystemName(veBaseDataApplyVo.getSystemName());

        veBaseDataApply.setTableName(veBaseDataApplyVo.getTableName());
        if (veBaseDataApply.getStatus().equals("0")) {
            for (VeBaseDataApplyColumn veBaseDataApplyColumn : veBaseDataApplyColumnList) {
                if (veBaseDataApplyColumn.getApplyId().equals(veBaseDataApply.getId())) {
                    this.veBaseDataApplyColumnService.updateById(veBaseDataApplyColumn);
                }
            }
        }
        this.veBaseDataApplyService.updateById(veBaseDataApply);
    }

    public List<Map<String, Object>> queryCommonListByApplyId(VeBaseDataApply veBaseDataApply)
    {
        List<Map<String, Object>> detailList = this.veBaseDataApplyColumnMapper.getDataApplyColumnByApplyId(veBaseDataApply.getId());
        if (detailList.size() == 0) {
            return null;
        }
        List<String> newList = new ArrayList();
        for (Map map : detailList) {
            newList.add(map.get("columnName").toString());
        }
        List<Map<String, Object>> list = this.veBaseDataApplyColumnMapper.getCommonListByTableName(veBaseDataApply.getTableName(), newList);
        return list;
    }

    public List<Map<String, Object>> queryDataApplyUserId(String userId)
    {
        return this.veBaseDataApplyMapper.queryDataApplyUserId(userId);
    }
}

