package org.edu.modules.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseTeacher;

public abstract interface IVeBaseTeacherService
        extends IService<VeBaseTeacher>
{
    public abstract List<Map<String, Object>> getTeacherPageList(VeBaseTeacher paramVeBaseTeacher);

    public abstract IPage<Map<String, Object>> getTeacherPageListByIPage(Page paramPage, VeBaseTeacher paramVeBaseTeacher);

    public abstract Map getTeacherSexStatistics();

    public abstract Map getTeacherAgeStatistics();

    public abstract VeBaseTeacher getByGH(Integer paramInteger, String paramString);

    public abstract VeBaseTeacher getByUserId(String paramString);

    public abstract List<VeBaseTeacher> getTeacherListBySearch(VeBaseTeacher paramVeBaseTeacher);

    public abstract int stopTeacherByUserId(String paramString);

    public abstract VeBaseTeacher queryBzrByBanjiId(Integer paramInteger);
}
