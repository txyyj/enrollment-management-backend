package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import org.edu.modules.common.entity.VeBaseDictArea;
import org.edu.modules.common.mapper.VeBaseDictAreaMapper;
import org.edu.modules.common.service.IVeBaseDictAreaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseDictAreaServiceImpl
        extends ServiceImpl<VeBaseDictAreaMapper, VeBaseDictArea>
        implements IVeBaseDictAreaService
{
    @Autowired
    private VeBaseDictAreaMapper veBaseDictAreaMapper;

    public List<VeBaseDictArea> getProvinceList()
    {
        return this.veBaseDictAreaMapper.getProvinceList();
    }

    public List<VeBaseDictArea> getCityList(Integer provinceId)
    {
        return this.veBaseDictAreaMapper.getCityList(provinceId);
    }

    public List<VeBaseDictArea> getCountyList(Integer cityId)
    {
        return this.veBaseDictAreaMapper.getCountyList(cityId);
    }

    public VeBaseDictArea getDictAreaByPidAndName(Integer id, Integer pid, String name)
    {
        return this.veBaseDictAreaMapper.getDictAreaByPidAndName(id, pid, name);
    }
}
