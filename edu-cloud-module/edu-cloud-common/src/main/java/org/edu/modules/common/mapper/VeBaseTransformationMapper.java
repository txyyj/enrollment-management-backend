package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseTransformation;

public abstract interface VeBaseTransformationMapper
        extends BaseMapper<VeBaseTransformation>
{
    public abstract List<Map<String, Object>> getTransformationPageList(VeBaseTransformation paramVeBaseTransformation);
}
