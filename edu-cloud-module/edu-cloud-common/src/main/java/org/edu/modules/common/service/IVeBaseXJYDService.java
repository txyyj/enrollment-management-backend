package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseXJYD;

public abstract interface IVeBaseXJYDService
        extends IService<VeBaseXJYD>
{
    public abstract List<Map<String, Object>> getXJYDPageList(VeBaseXJYD paramVeBaseXJYD);
}
