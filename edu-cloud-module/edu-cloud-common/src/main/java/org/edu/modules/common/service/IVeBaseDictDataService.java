package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.common.entity.VeBaseDictData;

public abstract interface IVeBaseDictDataService
        extends IService<VeBaseDictData>
{
    public abstract List<VeBaseDictData> getDictDataByModelCode(String paramString1, String paramString2);

    public abstract List<VeBaseDictData> getDictDataByModelCodeTitle(String paramString1, String paramString2);
}

