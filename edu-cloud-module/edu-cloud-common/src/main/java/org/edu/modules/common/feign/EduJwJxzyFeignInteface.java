package org.edu.modules.common.feign;

import org.edu.common.api.vo.Result;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("edu-schedule")
@Component
public abstract interface EduJwJxzyFeignInteface
{
    @GetMapping({"/jxzy/jwJiaoshi/queryJiaoshiByJzId"})
    public abstract Result<?> queryJiaoshiByJzId(@RequestParam(name="id", required=true) Integer paramInteger);

    @GetMapping({"/jxzy/jwJianzhu/queryJianzhuByCampusId"})
    public abstract Result<?> queryJianzhuByCampusId(@RequestParam(name="id", required=true) Integer paramInteger);
}
