package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseFaculty;

public abstract interface IVeBaseFacultyService
        extends IService<VeBaseFaculty>
{
    public abstract List<Map<String, Object>> getFacultyPageList(VeBaseFaculty paramVeBaseFaculty);

    public abstract VeBaseFaculty getFacultyByName(Integer paramInteger, String paramString);

    public abstract VeBaseFaculty getFacultyByCode(Integer paramInteger, String paramString);

    public abstract List<Map<String, Object>> getTreeList();
}
