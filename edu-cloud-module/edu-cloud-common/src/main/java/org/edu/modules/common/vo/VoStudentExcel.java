package org.edu.modules.common.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

public class VoStudentExcel
        implements Serializable
{
    @ApiModelProperty("id")
    @TableId(type=IdType.AUTO)
    private Integer id;
    @Excel(name="身份证号", width=15.0D)
    @ApiModelProperty("身份证号，唯一")
    private String sfzh;
    @Excel(name="学号", width=15.0D)
    @ApiModelProperty("学号")
    private String xh;
    @Excel(name="姓名", width=15.0D)
    @ApiModelProperty("姓名")
    private String xm;
    @Excel(name="性别", width=15.0D)
    @ApiModelProperty("性别码1男生2女生")
    private String xbm;
    @Excel(name="民族", width=15.0D)
    @ApiModelProperty("民族码")
    private String mzm;
    @Excel(name="年级", width=15.0D)
    @ApiModelProperty("年级名称")
    private String gradeName;
    @Excel(name="入学年月", width=15.0D)
    @ApiModelProperty("入学年月")
    private String rxny;
    @Excel(name="学制", width=15.0D)
    @ApiModelProperty("学制名称")
    private String xzName;
    @Excel(name="班级", width=15.0D)
    @ApiModelProperty("班级名称")
    private String className;
    @Excel(name="户籍所在省", width=15.0D)
    @ApiModelProperty("户口所在省份")
    private String province;
    @Excel(name="户籍所在市", width=15.0D)
    @ApiModelProperty("户口所在市")
    private String city;
    @Excel(name="户籍所在区县", width=15.0D)
    @ApiModelProperty("户口所在区")
    private String county;
    @Excel(name="详细地址", width=15.0D)
    @ApiModelProperty("家庭地址")
    private String jtdz;
    @Excel(name="是否低保", width=15.0D)
    @ApiModelProperty("是否低保0否1是")
    private String sfdb;
    @Excel(name="家长（监护人）电话", width=15.0D)
    @ApiModelProperty("家庭联系电话")
    private String jtlxdh;
    @Excel(name="出生日期", width=15.0D)
    @ApiModelProperty("出生日期")
    private String csrq;
    @Excel(name="学生本人手机号码", width=15.0D)
    @ApiModelProperty("学生本人手机号码")
    private String xsbrsjhm;
    @Excel(name="曾用名", width=15.0D)
    @ApiModelProperty("曾用名")
    private String cym;
    @Excel(name="政治面貌", width=15.0D)
    @ApiModelProperty("政治面貌")
    private String zzmmm;
    @Excel(name="毕业学校", width=15.0D)
    @ApiModelProperty("毕业学校")
    private String byxx;
    @Excel(name="招生类型", width=15.0D)
    private String zslx;
    @Excel(name="修读专业", width=15.0D)
    private String xdzy;
    @Excel(name="所属派出所", width=15.0D)
    private String sspcs;
    @Excel(name="乘火车区间", width=15.0D)
    private String chcqj;
    @Excel(name="户籍性质", width=15.0D)
    private String hjxz;
    @Excel(name="生源类别", width=15.0D)
    private String sylb;
    @Excel(name="家长（监护人）姓名", width=15.0D)
    private String jzxm;
    @Excel(name="邮政编码", width=15.0D)
    private String yzbm;
    @Excel(name="省级电子注册学籍号", width=15.0D)
    private String sjdzzcxjh;
    @Excel(name="港澳台侨胞", width=15.0D)
    private String gatqb;
    @Excel(name="电子邮箱", width=15.0D)
    private String dzyx;
    @Excel(name="中高职对接学校", width=15.0D)
    private String zgzdjxx;
    @Excel(name="就业", width=15.0D)
    private String jy;
    @Excel(name="异动情况", width=15.0D)
    private String ydqk;
    @Excel(name="备注", width=15.0D)
    private String bz;

    public VoStudentExcel setXh(String xh)
    {
        this.xh = xh;return this;
    }

    public VoStudentExcel setSfzh(String sfzh)
    {
        this.sfzh = sfzh;return this;
    }

    public VoStudentExcel setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VoStudentExcel(id=" + getId() + ", sfzh=" + getSfzh() + ", xh=" + getXh() + ", xm=" + getXm() + ", xbm=" + getXbm() + ", mzm=" + getMzm() + ", gradeName=" + getGradeName() + ", rxny=" + getRxny() + ", xzName=" + getXzName() + ", className=" + getClassName() + ", province=" + getProvince() + ", city=" + getCity() + ", county=" + getCounty() + ", jtdz=" + getJtdz() + ", sfdb=" + getSfdb() + ", jtlxdh=" + getJtlxdh() + ", csrq=" + getCsrq() + ", xsbrsjhm=" + getXsbrsjhm() + ", cym=" + getCym() + ", zzmmm=" + getZzmmm() + ", byxx=" + getByxx() + ", zslx=" + getZslx() + ", xdzy=" + getXdzy() + ", sspcs=" + getSspcs() + ", chcqj=" + getChcqj() + ", hjxz=" + getHjxz() + ", sylb=" + getSylb() + ", jzxm=" + getJzxm() + ", yzbm=" + getYzbm() + ", sjdzzcxjh=" + getSjdzzcxjh() + ", gatqb=" + getGatqb() + ", dzyx=" + getDzyx() + ", zgzdjxx=" + getZgzdjxx() + ", jy=" + getJy() + ", ydqk=" + getYdqk() + ", bz=" + getBz() + ")";
    }

    public VoStudentExcel setBz(String bz)
    {
        this.bz = bz;return this;
    }

    public VoStudentExcel setYdqk(String ydqk)
    {
        this.ydqk = ydqk;return this;
    }

    public VoStudentExcel setJy(String jy)
    {
        this.jy = jy;return this;
    }

    public VoStudentExcel setZgzdjxx(String zgzdjxx)
    {
        this.zgzdjxx = zgzdjxx;return this;
    }

    public VoStudentExcel setDzyx(String dzyx)
    {
        this.dzyx = dzyx;return this;
    }

    public VoStudentExcel setGatqb(String gatqb)
    {
        this.gatqb = gatqb;return this;
    }

    public VoStudentExcel setSjdzzcxjh(String sjdzzcxjh)
    {
        this.sjdzzcxjh = sjdzzcxjh;return this;
    }

    public VoStudentExcel setYzbm(String yzbm)
    {
        this.yzbm = yzbm;return this;
    }

    public VoStudentExcel setJzxm(String jzxm)
    {
        this.jzxm = jzxm;return this;
    }

    public VoStudentExcel setSylb(String sylb)
    {
        this.sylb = sylb;return this;
    }

    public VoStudentExcel setHjxz(String hjxz)
    {
        this.hjxz = hjxz;return this;
    }

    public VoStudentExcel setChcqj(String chcqj)
    {
        this.chcqj = chcqj;return this;
    }

    public VoStudentExcel setSspcs(String sspcs)
    {
        this.sspcs = sspcs;return this;
    }

    public VoStudentExcel setXdzy(String xdzy)
    {
        this.xdzy = xdzy;return this;
    }

    public VoStudentExcel setZslx(String zslx)
    {
        this.zslx = zslx;return this;
    }

    public VoStudentExcel setByxx(String byxx)
    {
        this.byxx = byxx;return this;
    }

    public VoStudentExcel setZzmmm(String zzmmm)
    {
        this.zzmmm = zzmmm;return this;
    }

    public VoStudentExcel setCym(String cym)
    {
        this.cym = cym;return this;
    }

    public VoStudentExcel setXsbrsjhm(String xsbrsjhm)
    {
        this.xsbrsjhm = xsbrsjhm;return this;
    }

    public VoStudentExcel setCsrq(String csrq)
    {
        this.csrq = csrq;return this;
    }

    public VoStudentExcel setJtlxdh(String jtlxdh)
    {
        this.jtlxdh = jtlxdh;return this;
    }

    public VoStudentExcel setSfdb(String sfdb)
    {
        this.sfdb = sfdb;return this;
    }

    public VoStudentExcel setJtdz(String jtdz)
    {
        this.jtdz = jtdz;return this;
    }

    public VoStudentExcel setCounty(String county)
    {
        this.county = county;return this;
    }

    public VoStudentExcel setCity(String city)
    {
        this.city = city;return this;
    }

    public VoStudentExcel setProvince(String province)
    {
        this.province = province;return this;
    }

    public VoStudentExcel setClassName(String className)
    {
        this.className = className;return this;
    }

    public VoStudentExcel setXzName(String xzName)
    {
        this.xzName = xzName;return this;
    }

    public VoStudentExcel setRxny(String rxny)
    {
        this.rxny = rxny;return this;
    }

    public VoStudentExcel setGradeName(String gradeName)
    {
        this.gradeName = gradeName;return this;
    }

    public VoStudentExcel setMzm(String mzm)
    {
        this.mzm = mzm;return this;
    }

    public VoStudentExcel setXbm(String xbm)
    {
        this.xbm = xbm;return this;
    }

    public VoStudentExcel setXm(String xm)
    {
        this.xm = xm;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $sfzh = getSfzh();result = result * 59 + ($sfzh == null ? 43 : $sfzh.hashCode());Object $xh = getXh();result = result * 59 + ($xh == null ? 43 : $xh.hashCode());Object $xm = getXm();result = result * 59 + ($xm == null ? 43 : $xm.hashCode());Object $xbm = getXbm();result = result * 59 + ($xbm == null ? 43 : $xbm.hashCode());Object $mzm = getMzm();result = result * 59 + ($mzm == null ? 43 : $mzm.hashCode());Object $gradeName = getGradeName();result = result * 59 + ($gradeName == null ? 43 : $gradeName.hashCode());Object $rxny = getRxny();result = result * 59 + ($rxny == null ? 43 : $rxny.hashCode());Object $xzName = getXzName();result = result * 59 + ($xzName == null ? 43 : $xzName.hashCode());Object $className = getClassName();result = result * 59 + ($className == null ? 43 : $className.hashCode());Object $province = getProvince();result = result * 59 + ($province == null ? 43 : $province.hashCode());Object $city = getCity();result = result * 59 + ($city == null ? 43 : $city.hashCode());Object $county = getCounty();result = result * 59 + ($county == null ? 43 : $county.hashCode());Object $jtdz = getJtdz();result = result * 59 + ($jtdz == null ? 43 : $jtdz.hashCode());Object $sfdb = getSfdb();result = result * 59 + ($sfdb == null ? 43 : $sfdb.hashCode());Object $jtlxdh = getJtlxdh();result = result * 59 + ($jtlxdh == null ? 43 : $jtlxdh.hashCode());Object $csrq = getCsrq();result = result * 59 + ($csrq == null ? 43 : $csrq.hashCode());Object $xsbrsjhm = getXsbrsjhm();result = result * 59 + ($xsbrsjhm == null ? 43 : $xsbrsjhm.hashCode());Object $cym = getCym();result = result * 59 + ($cym == null ? 43 : $cym.hashCode());Object $zzmmm = getZzmmm();result = result * 59 + ($zzmmm == null ? 43 : $zzmmm.hashCode());Object $byxx = getByxx();result = result * 59 + ($byxx == null ? 43 : $byxx.hashCode());Object $zslx = getZslx();result = result * 59 + ($zslx == null ? 43 : $zslx.hashCode());Object $xdzy = getXdzy();result = result * 59 + ($xdzy == null ? 43 : $xdzy.hashCode());Object $sspcs = getSspcs();result = result * 59 + ($sspcs == null ? 43 : $sspcs.hashCode());Object $chcqj = getChcqj();result = result * 59 + ($chcqj == null ? 43 : $chcqj.hashCode());Object $hjxz = getHjxz();result = result * 59 + ($hjxz == null ? 43 : $hjxz.hashCode());Object $sylb = getSylb();result = result * 59 + ($sylb == null ? 43 : $sylb.hashCode());Object $jzxm = getJzxm();result = result * 59 + ($jzxm == null ? 43 : $jzxm.hashCode());Object $yzbm = getYzbm();result = result * 59 + ($yzbm == null ? 43 : $yzbm.hashCode());Object $sjdzzcxjh = getSjdzzcxjh();result = result * 59 + ($sjdzzcxjh == null ? 43 : $sjdzzcxjh.hashCode());Object $gatqb = getGatqb();result = result * 59 + ($gatqb == null ? 43 : $gatqb.hashCode());Object $dzyx = getDzyx();result = result * 59 + ($dzyx == null ? 43 : $dzyx.hashCode());Object $zgzdjxx = getZgzdjxx();result = result * 59 + ($zgzdjxx == null ? 43 : $zgzdjxx.hashCode());Object $jy = getJy();result = result * 59 + ($jy == null ? 43 : $jy.hashCode());Object $ydqk = getYdqk();result = result * 59 + ($ydqk == null ? 43 : $ydqk.hashCode());Object $bz = getBz();result = result * 59 + ($bz == null ? 43 : $bz.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VoStudentExcel;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VoStudentExcel)) {
            return false;
        }
        VoStudentExcel other = (VoStudentExcel)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$sfzh = getSfzh();Object other$sfzh = other.getSfzh();
        if (this$sfzh == null ? other$sfzh != null : !this$sfzh.equals(other$sfzh)) {
            return false;
        }
        Object this$xh = getXh();Object other$xh = other.getXh();
        if (this$xh == null ? other$xh != null : !this$xh.equals(other$xh)) {
            return false;
        }
        Object this$xm = getXm();Object other$xm = other.getXm();
        if (this$xm == null ? other$xm != null : !this$xm.equals(other$xm)) {
            return false;
        }
        Object this$xbm = getXbm();Object other$xbm = other.getXbm();
        if (this$xbm == null ? other$xbm != null : !this$xbm.equals(other$xbm)) {
            return false;
        }
        Object this$mzm = getMzm();Object other$mzm = other.getMzm();
        if (this$mzm == null ? other$mzm != null : !this$mzm.equals(other$mzm)) {
            return false;
        }
        Object this$gradeName = getGradeName();Object other$gradeName = other.getGradeName();
        if (this$gradeName == null ? other$gradeName != null : !this$gradeName.equals(other$gradeName)) {
            return false;
        }
        Object this$rxny = getRxny();Object other$rxny = other.getRxny();
        if (this$rxny == null ? other$rxny != null : !this$rxny.equals(other$rxny)) {
            return false;
        }
        Object this$xzName = getXzName();Object other$xzName = other.getXzName();
        if (this$xzName == null ? other$xzName != null : !this$xzName.equals(other$xzName)) {
            return false;
        }
        Object this$className = getClassName();Object other$className = other.getClassName();
        if (this$className == null ? other$className != null : !this$className.equals(other$className)) {
            return false;
        }
        Object this$province = getProvince();Object other$province = other.getProvince();
        if (this$province == null ? other$province != null : !this$province.equals(other$province)) {
            return false;
        }
        Object this$city = getCity();Object other$city = other.getCity();
        if (this$city == null ? other$city != null : !this$city.equals(other$city)) {
            return false;
        }
        Object this$county = getCounty();Object other$county = other.getCounty();
        if (this$county == null ? other$county != null : !this$county.equals(other$county)) {
            return false;
        }
        Object this$jtdz = getJtdz();Object other$jtdz = other.getJtdz();
        if (this$jtdz == null ? other$jtdz != null : !this$jtdz.equals(other$jtdz)) {
            return false;
        }
        Object this$sfdb = getSfdb();Object other$sfdb = other.getSfdb();
        if (this$sfdb == null ? other$sfdb != null : !this$sfdb.equals(other$sfdb)) {
            return false;
        }
        Object this$jtlxdh = getJtlxdh();Object other$jtlxdh = other.getJtlxdh();
        if (this$jtlxdh == null ? other$jtlxdh != null : !this$jtlxdh.equals(other$jtlxdh)) {
            return false;
        }
        Object this$csrq = getCsrq();Object other$csrq = other.getCsrq();
        if (this$csrq == null ? other$csrq != null : !this$csrq.equals(other$csrq)) {
            return false;
        }
        Object this$xsbrsjhm = getXsbrsjhm();Object other$xsbrsjhm = other.getXsbrsjhm();
        if (this$xsbrsjhm == null ? other$xsbrsjhm != null : !this$xsbrsjhm.equals(other$xsbrsjhm)) {
            return false;
        }
        Object this$cym = getCym();Object other$cym = other.getCym();
        if (this$cym == null ? other$cym != null : !this$cym.equals(other$cym)) {
            return false;
        }
        Object this$zzmmm = getZzmmm();Object other$zzmmm = other.getZzmmm();
        if (this$zzmmm == null ? other$zzmmm != null : !this$zzmmm.equals(other$zzmmm)) {
            return false;
        }
        Object this$byxx = getByxx();Object other$byxx = other.getByxx();
        if (this$byxx == null ? other$byxx != null : !this$byxx.equals(other$byxx)) {
            return false;
        }
        Object this$zslx = getZslx();Object other$zslx = other.getZslx();
        if (this$zslx == null ? other$zslx != null : !this$zslx.equals(other$zslx)) {
            return false;
        }
        Object this$xdzy = getXdzy();Object other$xdzy = other.getXdzy();
        if (this$xdzy == null ? other$xdzy != null : !this$xdzy.equals(other$xdzy)) {
            return false;
        }
        Object this$sspcs = getSspcs();Object other$sspcs = other.getSspcs();
        if (this$sspcs == null ? other$sspcs != null : !this$sspcs.equals(other$sspcs)) {
            return false;
        }
        Object this$chcqj = getChcqj();Object other$chcqj = other.getChcqj();
        if (this$chcqj == null ? other$chcqj != null : !this$chcqj.equals(other$chcqj)) {
            return false;
        }
        Object this$hjxz = getHjxz();Object other$hjxz = other.getHjxz();
        if (this$hjxz == null ? other$hjxz != null : !this$hjxz.equals(other$hjxz)) {
            return false;
        }
        Object this$sylb = getSylb();Object other$sylb = other.getSylb();
        if (this$sylb == null ? other$sylb != null : !this$sylb.equals(other$sylb)) {
            return false;
        }
        Object this$jzxm = getJzxm();Object other$jzxm = other.getJzxm();
        if (this$jzxm == null ? other$jzxm != null : !this$jzxm.equals(other$jzxm)) {
            return false;
        }
        Object this$yzbm = getYzbm();Object other$yzbm = other.getYzbm();
        if (this$yzbm == null ? other$yzbm != null : !this$yzbm.equals(other$yzbm)) {
            return false;
        }
        Object this$sjdzzcxjh = getSjdzzcxjh();Object other$sjdzzcxjh = other.getSjdzzcxjh();
        if (this$sjdzzcxjh == null ? other$sjdzzcxjh != null : !this$sjdzzcxjh.equals(other$sjdzzcxjh)) {
            return false;
        }
        Object this$gatqb = getGatqb();Object other$gatqb = other.getGatqb();
        if (this$gatqb == null ? other$gatqb != null : !this$gatqb.equals(other$gatqb)) {
            return false;
        }
        Object this$dzyx = getDzyx();Object other$dzyx = other.getDzyx();
        if (this$dzyx == null ? other$dzyx != null : !this$dzyx.equals(other$dzyx)) {
            return false;
        }
        Object this$zgzdjxx = getZgzdjxx();Object other$zgzdjxx = other.getZgzdjxx();
        if (this$zgzdjxx == null ? other$zgzdjxx != null : !this$zgzdjxx.equals(other$zgzdjxx)) {
            return false;
        }
        Object this$jy = getJy();Object other$jy = other.getJy();
        if (this$jy == null ? other$jy != null : !this$jy.equals(other$jy)) {
            return false;
        }
        Object this$ydqk = getYdqk();Object other$ydqk = other.getYdqk();
        if (this$ydqk == null ? other$ydqk != null : !this$ydqk.equals(other$ydqk)) {
            return false;
        }
        Object this$bz = getBz();Object other$bz = other.getBz();return this$bz == null ? other$bz == null : this$bz.equals(other$bz);
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getSfzh()
    {
        return this.sfzh;
    }

    public String getXh()
    {
        return this.xh;
    }

    public String getXm()
    {
        return this.xm;
    }

    public String getXbm()
    {
        return this.xbm;
    }

    public String getMzm()
    {
        return this.mzm;
    }

    public String getGradeName()
    {
        return this.gradeName;
    }

    public String getRxny()
    {
        return this.rxny;
    }

    public String getXzName()
    {
        return this.xzName;
    }

    public String getClassName()
    {
        return this.className;
    }

    public String getProvince()
    {
        return this.province;
    }

    public String getCity()
    {
        return this.city;
    }

    public String getCounty()
    {
        return this.county;
    }

    public String getJtdz()
    {
        return this.jtdz;
    }

    public String getSfdb()
    {
        return this.sfdb;
    }

    public String getJtlxdh()
    {
        return this.jtlxdh;
    }

    public String getCsrq()
    {
        return this.csrq;
    }

    public String getXsbrsjhm()
    {
        return this.xsbrsjhm;
    }

    public String getCym()
    {
        return this.cym;
    }

    public String getZzmmm()
    {
        return this.zzmmm;
    }

    public String getByxx()
    {
        return this.byxx;
    }

    public String getZslx()
    {
        return this.zslx;
    }

    public String getXdzy()
    {
        return this.xdzy;
    }

    public String getSspcs()
    {
        return this.sspcs;
    }

    public String getChcqj()
    {
        return this.chcqj;
    }

    public String getHjxz()
    {
        return this.hjxz;
    }

    public String getSylb()
    {
        return this.sylb;
    }

    public String getJzxm()
    {
        return this.jzxm;
    }

    public String getYzbm()
    {
        return this.yzbm;
    }

    public String getSjdzzcxjh()
    {
        return this.sjdzzcxjh;
    }

    public String getGatqb()
    {
        return this.gatqb;
    }

    public String getDzyx()
    {
        return this.dzyx;
    }

    public String getZgzdjxx()
    {
        return this.zgzdjxx;
    }

    public String getJy()
    {
        return this.jy;
    }

    public String getYdqk()
    {
        return this.ydqk;
    }

    public String getBz()
    {
        return this.bz;
    }
}
