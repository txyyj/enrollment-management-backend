package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.edu.modules.common.entity.VeBaseFestival;

public abstract interface VeBaseFestivalMapper
        extends BaseMapper<VeBaseFestival>
{
    public abstract List<VeBaseFestival> getFestivalBySemId(Integer paramInteger);
}

