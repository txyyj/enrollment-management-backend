package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_specialty")
@ApiModel(value="ve_base_specialty对象", description="专业信息表")
public class VeBaseSpecialty
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="专业编号", width=15.0D)
    @ApiModelProperty("专业编号")
    private String zybh;
    @Excel(name="专业代码", width=15.0D)
    @ApiModelProperty("专业代码")
    private String zydm;
    @Excel(name="专业名称", width=15.0D)
    @ApiModelProperty("专业名称")
    private String zymc;
    @Excel(name="专业英文名称", width=15.0D)
    @ApiModelProperty("专业英文名称")
    private String zyywmc;
    @Excel(name="学制Id", width=15.0D)
    @ApiModelProperty("学制Id")
    private String xz;
    @Excel(name="专业方向名称", width=15.0D)
    @ApiModelProperty("专业方向名称")
    private String zyfxmc;
    @Excel(name="专业简称", width=15.0D)
    @ApiModelProperty("专业简称")
    private String zyjc;
    @Excel(name="建立年月", width=15.0D)
    @ApiModelProperty("建立年月")
    private Integer jlny;
    @Excel(name="专业教师数", width=15.0D)
    @ApiModelProperty("专业教师数")
    private Integer zyjss;
    @Excel(name="开设机构号，学校的组织机构部门编号", width=15.0D)
    @ApiModelProperty("开设机构号，学校的组织机构部门编号")
    private String ksjgh;
    @Excel(name="开设机构部门ID", width=15.0D)
    @ApiModelProperty("开设机构部门ID")
    private Integer depid;
    @Excel(name="总学分", width=15.0D)
    @ApiModelProperty("总学分，此专业毕业时所需的总学分")
    private Integer zxf;
    @Excel(name="是否为招生使用", width=15.0D)
    @ApiModelProperty("是否为招生使用，0：否，1：是")
    private Integer iszssy;
    @Excel(name="所属院系ID", width=15.0D)
    @ApiModelProperty("所属院系ID")
    private Integer falid;
    @Excel(name="上级专业ID", width=15.0D)
    @ApiModelProperty("上级专业ID")
    private Integer pid;
    @Excel(name="专业路径", width=15.0D)
    @ApiModelProperty("专业路径")
    private String path;
    @Excel(name="状态", width=15.0D)
    @ApiModelProperty("状态：1=可用，0=不可用学习年限（以年为单位）")
    private Integer status;
    @Excel(name="所属院系代码", width=15.0D)
    @ApiModelProperty("所属院系代码")
    private String yxdm;
    @Excel(name="终端ID", width=15.0D)
    @ApiModelProperty("终端ID")
    private Integer terminalid;
    @Excel(name="专业部名称", width=15.0D)
    @TableField(exist=false)
    private String facultyName;
    @Excel(name="上级专业", width=15.0D)
    @TableField(exist=false)
    private String pidName;
    @Excel(name="学制", width=15.0D)
    @TableField(exist=false)
    private String xuezhiName;
    @TableField(exist=false)
    @ApiModelProperty("建立年月")
    private String jlnyName;

    public VeBaseSpecialty setZydm(String zydm)
    {
        this.zydm = zydm;return this;
    }

    public VeBaseSpecialty setZybh(String zybh)
    {
        this.zybh = zybh;return this;
    }

    public VeBaseSpecialty setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseSpecialty(id=" + getId() + ", zybh=" + getZybh() + ", zydm=" + getZydm() + ", zymc=" + getZymc() + ", zyywmc=" + getZyywmc() + ", xz=" + getXz() + ", zyfxmc=" + getZyfxmc() + ", zyjc=" + getZyjc() + ", jlny=" + getJlny() + ", zyjss=" + getZyjss() + ", ksjgh=" + getKsjgh() + ", depid=" + getDepid() + ", zxf=" + getZxf() + ", iszssy=" + getIszssy() + ", falid=" + getFalid() + ", pid=" + getPid() + ", path=" + getPath() + ", status=" + getStatus() + ", yxdm=" + getYxdm() + ", terminalid=" + getTerminalid() + ", facultyName=" + getFacultyName() + ", pidName=" + getPidName() + ", xuezhiName=" + getXuezhiName() + ", jlnyName=" + getJlnyName() + ")";
    }

    public VeBaseSpecialty setJlnyName(String jlnyName)
    {
        this.jlnyName = jlnyName;return this;
    }

    public VeBaseSpecialty setXuezhiName(String xuezhiName)
    {
        this.xuezhiName = xuezhiName;return this;
    }

    public VeBaseSpecialty setPidName(String pidName)
    {
        this.pidName = pidName;return this;
    }

    public VeBaseSpecialty setFacultyName(String facultyName)
    {
        this.facultyName = facultyName;return this;
    }

    public VeBaseSpecialty setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeBaseSpecialty setYxdm(String yxdm)
    {
        this.yxdm = yxdm;return this;
    }

    public VeBaseSpecialty setStatus(Integer status)
    {
        this.status = status;return this;
    }

    public VeBaseSpecialty setPath(String path)
    {
        this.path = path;return this;
    }

    public VeBaseSpecialty setPid(Integer pid)
    {
        this.pid = pid;return this;
    }

    public VeBaseSpecialty setFalid(Integer falid)
    {
        this.falid = falid;return this;
    }

    public VeBaseSpecialty setIszssy(Integer iszssy)
    {
        this.iszssy = iszssy;return this;
    }

    public VeBaseSpecialty setZxf(Integer zxf)
    {
        this.zxf = zxf;return this;
    }

    public VeBaseSpecialty setDepid(Integer depid)
    {
        this.depid = depid;return this;
    }

    public VeBaseSpecialty setKsjgh(String ksjgh)
    {
        this.ksjgh = ksjgh;return this;
    }

    public VeBaseSpecialty setZyjss(Integer zyjss)
    {
        this.zyjss = zyjss;return this;
    }

    public VeBaseSpecialty setJlny(Integer jlny)
    {
        this.jlny = jlny;return this;
    }

    public VeBaseSpecialty setZyjc(String zyjc)
    {
        this.zyjc = zyjc;return this;
    }

    public VeBaseSpecialty setZyfxmc(String zyfxmc)
    {
        this.zyfxmc = zyfxmc;return this;
    }

    public VeBaseSpecialty setXz(String xz)
    {
        this.xz = xz;return this;
    }

    public VeBaseSpecialty setZyywmc(String zyywmc)
    {
        this.zyywmc = zyywmc;return this;
    }

    public VeBaseSpecialty setZymc(String zymc)
    {
        this.zymc = zymc;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $jlny = getJlny();result = result * 59 + ($jlny == null ? 43 : $jlny.hashCode());Object $zyjss = getZyjss();result = result * 59 + ($zyjss == null ? 43 : $zyjss.hashCode());Object $depid = getDepid();result = result * 59 + ($depid == null ? 43 : $depid.hashCode());Object $zxf = getZxf();result = result * 59 + ($zxf == null ? 43 : $zxf.hashCode());Object $iszssy = getIszssy();result = result * 59 + ($iszssy == null ? 43 : $iszssy.hashCode());Object $falid = getFalid();result = result * 59 + ($falid == null ? 43 : $falid.hashCode());Object $pid = getPid();result = result * 59 + ($pid == null ? 43 : $pid.hashCode());Object $status = getStatus();result = result * 59 + ($status == null ? 43 : $status.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $zybh = getZybh();result = result * 59 + ($zybh == null ? 43 : $zybh.hashCode());Object $zydm = getZydm();result = result * 59 + ($zydm == null ? 43 : $zydm.hashCode());Object $zymc = getZymc();result = result * 59 + ($zymc == null ? 43 : $zymc.hashCode());Object $zyywmc = getZyywmc();result = result * 59 + ($zyywmc == null ? 43 : $zyywmc.hashCode());Object $xz = getXz();result = result * 59 + ($xz == null ? 43 : $xz.hashCode());Object $zyfxmc = getZyfxmc();result = result * 59 + ($zyfxmc == null ? 43 : $zyfxmc.hashCode());Object $zyjc = getZyjc();result = result * 59 + ($zyjc == null ? 43 : $zyjc.hashCode());Object $ksjgh = getKsjgh();result = result * 59 + ($ksjgh == null ? 43 : $ksjgh.hashCode());Object $path = getPath();result = result * 59 + ($path == null ? 43 : $path.hashCode());Object $yxdm = getYxdm();result = result * 59 + ($yxdm == null ? 43 : $yxdm.hashCode());Object $facultyName = getFacultyName();result = result * 59 + ($facultyName == null ? 43 : $facultyName.hashCode());Object $pidName = getPidName();result = result * 59 + ($pidName == null ? 43 : $pidName.hashCode());Object $xuezhiName = getXuezhiName();result = result * 59 + ($xuezhiName == null ? 43 : $xuezhiName.hashCode());Object $jlnyName = getJlnyName();result = result * 59 + ($jlnyName == null ? 43 : $jlnyName.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseSpecialty;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseSpecialty)) {
            return false;
        }
        VeBaseSpecialty other = (VeBaseSpecialty)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$jlny = getJlny();Object other$jlny = other.getJlny();
        if (this$jlny == null ? other$jlny != null : !this$jlny.equals(other$jlny)) {
            return false;
        }
        Object this$zyjss = getZyjss();Object other$zyjss = other.getZyjss();
        if (this$zyjss == null ? other$zyjss != null : !this$zyjss.equals(other$zyjss)) {
            return false;
        }
        Object this$depid = getDepid();Object other$depid = other.getDepid();
        if (this$depid == null ? other$depid != null : !this$depid.equals(other$depid)) {
            return false;
        }
        Object this$zxf = getZxf();Object other$zxf = other.getZxf();
        if (this$zxf == null ? other$zxf != null : !this$zxf.equals(other$zxf)) {
            return false;
        }
        Object this$iszssy = getIszssy();Object other$iszssy = other.getIszssy();
        if (this$iszssy == null ? other$iszssy != null : !this$iszssy.equals(other$iszssy)) {
            return false;
        }
        Object this$falid = getFalid();Object other$falid = other.getFalid();
        if (this$falid == null ? other$falid != null : !this$falid.equals(other$falid)) {
            return false;
        }
        Object this$pid = getPid();Object other$pid = other.getPid();
        if (this$pid == null ? other$pid != null : !this$pid.equals(other$pid)) {
            return false;
        }
        Object this$status = getStatus();Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$zybh = getZybh();Object other$zybh = other.getZybh();
        if (this$zybh == null ? other$zybh != null : !this$zybh.equals(other$zybh)) {
            return false;
        }
        Object this$zydm = getZydm();Object other$zydm = other.getZydm();
        if (this$zydm == null ? other$zydm != null : !this$zydm.equals(other$zydm)) {
            return false;
        }
        Object this$zymc = getZymc();Object other$zymc = other.getZymc();
        if (this$zymc == null ? other$zymc != null : !this$zymc.equals(other$zymc)) {
            return false;
        }
        Object this$zyywmc = getZyywmc();Object other$zyywmc = other.getZyywmc();
        if (this$zyywmc == null ? other$zyywmc != null : !this$zyywmc.equals(other$zyywmc)) {
            return false;
        }
        Object this$xz = getXz();Object other$xz = other.getXz();
        if (this$xz == null ? other$xz != null : !this$xz.equals(other$xz)) {
            return false;
        }
        Object this$zyfxmc = getZyfxmc();Object other$zyfxmc = other.getZyfxmc();
        if (this$zyfxmc == null ? other$zyfxmc != null : !this$zyfxmc.equals(other$zyfxmc)) {
            return false;
        }
        Object this$zyjc = getZyjc();Object other$zyjc = other.getZyjc();
        if (this$zyjc == null ? other$zyjc != null : !this$zyjc.equals(other$zyjc)) {
            return false;
        }
        Object this$ksjgh = getKsjgh();Object other$ksjgh = other.getKsjgh();
        if (this$ksjgh == null ? other$ksjgh != null : !this$ksjgh.equals(other$ksjgh)) {
            return false;
        }
        Object this$path = getPath();Object other$path = other.getPath();
        if (this$path == null ? other$path != null : !this$path.equals(other$path)) {
            return false;
        }
        Object this$yxdm = getYxdm();Object other$yxdm = other.getYxdm();
        if (this$yxdm == null ? other$yxdm != null : !this$yxdm.equals(other$yxdm)) {
            return false;
        }
        Object this$facultyName = getFacultyName();Object other$facultyName = other.getFacultyName();
        if (this$facultyName == null ? other$facultyName != null : !this$facultyName.equals(other$facultyName)) {
            return false;
        }
        Object this$pidName = getPidName();Object other$pidName = other.getPidName();
        if (this$pidName == null ? other$pidName != null : !this$pidName.equals(other$pidName)) {
            return false;
        }
        Object this$xuezhiName = getXuezhiName();Object other$xuezhiName = other.getXuezhiName();
        if (this$xuezhiName == null ? other$xuezhiName != null : !this$xuezhiName.equals(other$xuezhiName)) {
            return false;
        }
        Object this$jlnyName = getJlnyName();Object other$jlnyName = other.getJlnyName();return this$jlnyName == null ? other$jlnyName == null : this$jlnyName.equals(other$jlnyName);
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getZybh()
    {
        return this.zybh;
    }

    public String getZydm()
    {
        return this.zydm;
    }

    public String getZymc()
    {
        return this.zymc;
    }

    public String getZyywmc()
    {
        return this.zyywmc;
    }

    public String getXz()
    {
        return this.xz;
    }

    public String getZyfxmc()
    {
        return this.zyfxmc;
    }

    public String getZyjc()
    {
        return this.zyjc;
    }

    public Integer getJlny()
    {
        return this.jlny;
    }

    public Integer getZyjss()
    {
        return this.zyjss;
    }

    public String getKsjgh()
    {
        return this.ksjgh;
    }

    public Integer getDepid()
    {
        return this.depid;
    }

    public Integer getZxf()
    {
        return this.zxf;
    }

    public Integer getIszssy()
    {
        return this.iszssy;
    }

    public Integer getFalid()
    {
        return this.falid;
    }

    public Integer getPid()
    {
        return this.pid;
    }

    public String getPath()
    {
        return this.path;
    }

    public Integer getStatus()
    {
        return this.status;
    }

    public String getYxdm()
    {
        return this.yxdm;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public String getFacultyName()
    {
        return this.facultyName;
    }

    public String getPidName()
    {
        return this.pidName;
    }

    public String getXuezhiName()
    {
        return this.xuezhiName;
    }

    public String getJlnyName()
    {
        return this.jlnyName;
    }
}
