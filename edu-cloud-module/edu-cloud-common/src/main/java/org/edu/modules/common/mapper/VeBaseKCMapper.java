package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseKC;

public abstract interface VeBaseKCMapper
        extends BaseMapper<VeBaseKC>
{
    public abstract List<Map<String, Object>> getKCPageList(VeBaseKC paramVeBaseKC);
}
