package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.edu.modules.common.entity.VeBaseCalendar;

public abstract interface VeBaseCalendarMapper
        extends BaseMapper<VeBaseCalendar>
{
    public abstract List<VeBaseCalendar> getCalendarListBySemId(Integer paramInteger1, Integer paramInteger2, Integer paramInteger3);

    public abstract VeBaseCalendar getCalenderByDates(String paramString);
}
