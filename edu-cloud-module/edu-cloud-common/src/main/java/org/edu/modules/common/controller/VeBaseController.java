package org.edu.modules.common.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.vo.LoginUser;
import org.edu.modules.common.entity.VeBaseAppUser;
import org.edu.modules.common.entity.VeBaseBanJi;
import org.edu.modules.common.entity.VeBaseBanJiBzr;
import org.edu.modules.common.entity.VeBaseBuild;
import org.edu.modules.common.entity.VeBaseBuildRoom;
import org.edu.modules.common.entity.VeBaseCalendar;
import org.edu.modules.common.entity.VeBaseCampus;
import org.edu.modules.common.entity.VeBaseDepartment;
import org.edu.modules.common.entity.VeBaseDictArea;
import org.edu.modules.common.entity.VeBaseFaculty;
import org.edu.modules.common.entity.VeBaseFestival;
import org.edu.modules.common.entity.VeBaseGrade;
import org.edu.modules.common.entity.VeBaseJYZ;
import org.edu.modules.common.entity.VeBaseSchool;
import org.edu.modules.common.entity.VeBaseSemester;
import org.edu.modules.common.entity.VeBaseSpecialty;
import org.edu.modules.common.entity.VeBaseStudent;
import org.edu.modules.common.entity.VeBaseStudentInfo;
import org.edu.modules.common.entity.VeBaseSysConfig;
import org.edu.modules.common.entity.VeBaseTeacher;
import org.edu.modules.common.entity.VeBaseXueZhi;
import org.edu.modules.common.redis.RedisUtils;
import org.edu.modules.common.service.IVeBaseAppUserService;
import org.edu.modules.common.service.IVeBaseBanJiBzrService;
import org.edu.modules.common.service.IVeBaseBanJiService;
import org.edu.modules.common.service.IVeBaseBuildRoomService;
import org.edu.modules.common.service.IVeBaseBuildService;
import org.edu.modules.common.service.IVeBaseCalendarService;
import org.edu.modules.common.service.IVeBaseCampusService;
import org.edu.modules.common.service.IVeBaseDepartmentService;
import org.edu.modules.common.service.IVeBaseDictAreaService;
import org.edu.modules.common.service.IVeBaseFacultyService;
import org.edu.modules.common.service.IVeBaseFestivalService;
import org.edu.modules.common.service.IVeBaseGradeService;
import org.edu.modules.common.service.IVeBaseJYZService;
import org.edu.modules.common.service.IVeBaseSchoolService;
import org.edu.modules.common.service.IVeBaseSemesterService;
import org.edu.modules.common.service.IVeBaseSpecialtyService;
import org.edu.modules.common.service.IVeBaseStudentInfoService;
import org.edu.modules.common.service.IVeBaseStudentService;
import org.edu.modules.common.service.IVeBaseSysConfigService;
import org.edu.modules.common.service.IVeBaseTeacherService;
import org.edu.modules.common.service.IVeBaseXueZhiService;
import org.edu.modules.common.util.DateTimeUtil;
import org.edu.modules.common.util.EmailUtil;
import org.edu.modules.common.util.IdCardUtil;
import org.edu.modules.common.util.PhoneUtil;
import org.edu.modules.common.vo.VoStudentAndStudentInfo;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

@Api(tags={"基础数据"})
@RestController
@RequestMapping({"/common/veBase"})
@ApiSort(60)
public class VeBaseController
{
    private static final Logger log = LoggerFactory.getLogger(VeBaseController.class);
    @Autowired
    private IVeBaseSchoolService veBaseSchoolService;
    @Autowired
    private IVeBaseCampusService veBaseCampusService;
    @Autowired
    private IVeBaseDepartmentService veBaseDepartmentService;
    @Autowired
    private IVeBaseJYZService veBaseJYZService;
    @Autowired
    private IVeBaseFacultyService veBaseFacultyService;
    @Autowired
    private IVeBaseSpecialtyService veBaseSpecialtyService;
    @Autowired
    private IVeBaseGradeService veBaseGradeService;
    @Autowired
    private IVeBaseSemesterService veBaseSemesterService;
    @Autowired
    private IVeBaseBanJiService veBaseBanJiService;
    @Autowired
    private IVeBaseXueZhiService veBaseXueZhiService;
    @Autowired
    private IVeBaseBuildService veBaseBuildService;
    @Autowired
    private IVeBaseBuildRoomService veBaseBuildRoomService;
    @Autowired
    private IVeBaseFestivalService veBaseFestivalService;
    @Autowired
    private IVeBaseCalendarService veBaseCalendarService;
    @Autowired
    private IVeBaseStudentService veBaseStudentService;
    @Autowired
    private IVeBaseStudentInfoService veBaseStudentInfoService;
    @Autowired
    private IVeBaseDictAreaService veBaseDictAreaService;
    @Autowired
    private IVeBaseTeacherService veBaseTeacherService;
    @Autowired
    private IVeBaseAppUserService veBaseAppUserService;
    @Autowired
    private IVeBaseBanJiBzrService veBaseBanJiBzrService;
    @Autowired
    private IVeBaseSysConfigService veBaseSysConfigService;
    @Autowired
    private RedisUtils redisUtils;

    @AutoLog("学校信息-分页列表查询")
    @ApiOperation(value="学校信息-分页列表查询", notes="学校信息-分页列表查询")
    @GetMapping({"/getSchoolPageList"})
    public Result<?> getSchoolPageList(VeBaseSchool veBaseSchool, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseSchoolService.getSchoolPageList(veBaseSchool);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.ok(pageInfo);
    }

    @AutoLog("校区信息-分页列表查询")
    @ApiOperation(value="校区信息-分页列表查询", notes="校区信息-分页列表查询")
    @GetMapping({"/getCampusPageList"})
    public Result<?> getCampusPageList(VeBaseCampus veBaseCampus, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseCampusService.getCampusPageList(veBaseCampus);
        PageInfo<?> pageInfo = new PageInfo(list);

        this.redisUtils.set("queryCampusList", this.veBaseCampusService.list());
        return Result.ok(pageInfo);
    }

    @AutoLog("部门信息-树形列表查询")
    @ApiOperation(value="部门信息-树形列表查询", notes="部门信息-树形列表查询")
    @GetMapping({"/getDepartmentPageList"})
    public Result<?> getDepartmentPageList(VeBaseDepartment veBaseDepartment, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        List<VeBaseDepartment> list = this.veBaseDepartmentService.getTreeList();


        this.redisUtils.set("queryDepartmentList", list);
        this.redisUtils.set("queryDepartmentAndTeacherList", this.veBaseDepartmentService.getDepartmentAndTeacherList());
        this.redisUtils.set("queryOrganUserTreeList", this.veBaseAppUserService.queryOrganUserTreeList(null));
        return Result.ok(list);
    }

    @AutoLog("教研组(专业组)信息-树形列表查询")
    @ApiOperation(value="教研组(专业组)信息-树形列表查询", notes="教研组(专业组)信息-树形列表查询")
    @GetMapping({"/getJYZPageList"})
    public Result<?> getJYZPageList()
    {
        List<Map<String, Object>> list = this.veBaseJYZService.getJYZTreeList();

        this.redisUtils.set("queryJYZTreeList", list);
        return Result.ok(list);
    }

    @AutoLog("院系信息(专业部)-分页列表查询")
    @ApiOperation(value="院系信息(专业部)-分页列表查询", notes="院系信息(专业部)-分页列表查询")
    @GetMapping({"/getFacultyPageList"})
    public Result<?> getFacultyPageList(VeBaseFaculty veBaseFaculty, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseFacultyService.getFacultyPageList(veBaseFaculty);
        PageInfo<?> pageInfo = new PageInfo(list);

        QueryWrapper queryWrapper3 = new QueryWrapper();
        queryWrapper3.eq("status", Integer.valueOf(1));
        queryWrapper3.orderByAsc("yxdm");
        this.redisUtils.set("queryFacultyList", this.veBaseFacultyService.list(queryWrapper3));
        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", this.veBaseFacultyService.getTreeList());
        return Result.ok(pageInfo);
    }

    @AutoLog("专业信息-列表查询")
    @ApiOperation(value="专业信息-列表查询", notes="专业信息-列表查询")
    @GetMapping({"/getSpecisltyPageList"})
    public Result<?> getSpecisltyPageList(VeBaseSpecialty veBaseSpecialty)
    {
        List<Map<String, Object>> list = this.veBaseSpecialtyService.getSpecialtyPageList(veBaseSpecialty);
        return Result.ok(list);
    }

    @AutoLog("专业信息-树形列表查询")
    @ApiOperation(value="专业信息-树形列表查询", notes="专业信息-树形列表查询")
    @GetMapping({"/getSpecialtyTreeList"})
    public Result<?> getSpecialtyTreeList()
    {
        List<Map<String, Object>> list = this.veBaseSpecialtyService.getSpecialtyTreeList();

        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", this.veBaseFacultyService.getTreeList());
        return Result.ok(list);
    }

    @AutoLog("年级信息-分页列表查询")
    @ApiOperation(value="年级信息-分页列表查询", notes="年级信息-分页列表查询")
    @GetMapping({"/getGradePageList"})
    public Result<?> getGradePageList(VeBaseGrade veBaseGrade, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseGradeService.getGradePageList(veBaseGrade);
        PageInfo<?> pageInfo = new PageInfo(list);

        QueryWrapper queryWrapper2 = new QueryWrapper();
        queryWrapper2.eq("njzt", Integer.valueOf(1));
        queryWrapper2.orderByDesc("rxnf");
        this.redisUtils.set("queryGradeList", this.veBaseGradeService.list(queryWrapper2));
        return Result.ok(pageInfo);
    }

    @AutoLog("学期信息-分页列表查询")
    @ApiOperation(value="学期信息-分页列表查询", notes="学期信息-分页列表查询")
    @GetMapping({"/getSemesterPageList"})
    public Result<?> getSemesterPageList(VeBaseSemester veBaseSemester, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseSemesterService.getSemesterPageList(veBaseSemester);
        PageInfo<?> pageInfo = new PageInfo(list);

        QueryWrapper queryWrapper4 = new QueryWrapper();
        queryWrapper4.orderByDesc("xqm");
        this.redisUtils.set("querySemesterList", this.veBaseSemesterService.list(queryWrapper4));
        return Result.ok(pageInfo);
    }

    @AutoLog("班级信息-分页列表查询")
    @ApiOperation(value="班级信息-分页列表查询", notes="班级信息-分页列表查询")
    @GetMapping({"/getBanJiPageList"})
    public Result<?> getBanJiPageList(VeBaseBanJi veBaseBanJi, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseBanJiService.getBanJiPageList(veBaseBanJi);
        PageInfo<?> pageInfo = new PageInfo(list);

        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", this.veBaseFacultyService.getTreeList());
        return Result.ok(pageInfo);
    }

    @AutoLog("学制信息-分页列表查询")
    @ApiOperation(value="学制信息-分页列表查询", notes="学制信息-分页列表查询")
    @GetMapping({"/getXueZhiPageList"})
    public Result<?> getXueZhiPageList(VeBaseXueZhi veBaseXueZhi, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseXueZhiService.getXueZhiPageList(veBaseXueZhi);
        PageInfo<?> pageInfo = new PageInfo(list);

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByAsc("years");
        this.redisUtils.set("queryXueZhiList", this.veBaseXueZhiService.list(queryWrapper));
        return Result.ok(pageInfo);
    }

    @AutoLog("楼栋信息-列表查询")
    @ApiOperation(value="楼栋信息-列表查询", notes="楼栋信息-列表查询")
    @GetMapping({"/getBuildList"})
    public Result<?> getBuildList(VeBaseBuild veBaseBuild)
    {
        List<VeBaseBuild> list = this.veBaseBuildService.list();
        return Result.ok(list);
    }

    @AutoLog("根据学期id查询节假日信息")
    @ApiOperation(value="根据学期id查询节假日信息", notes="根据学期id查询节假日信息")
    @GetMapping({"/getFestivalListBysemId"})
    public Result<?> getFestivalListBysemId(@RequestParam(name="semId", required=true) Integer semId)
    {
        List<VeBaseFestival> list = this.veBaseFestivalService.getFestivalBySemId(semId);
        return Result.ok(list);
    }

    @AutoLog("根据id获取学生信息")
    @ApiOperation(value="根据id获取学生信息", notes="根据id获取学生信息")
    @GetMapping({"/getStudentListById"})
    public Result<?> getStudentListById(@RequestParam(name="id", required=true) Integer id)
    {
        Map map = this.veBaseStudentService.getStudentAndInfoById(id);
        return Result.ok(map);
    }

    @AutoLog("添加学校信息")
    @ApiOperation(value="添加学校信息", notes="添加学校信息")
    @PostMapping({"/addSchool"})
    public Result<?> addSchool(@RequestBody VeBaseSchool veBaseSchool)
    {
        if (("".equals(veBaseSchool.getXxdm())) || (veBaseSchool.getXxdm() == null)) {
            return Result.error("学校代码不能为空!");
        }
        if (("".equals(veBaseSchool.getXxmc())) || (veBaseSchool.getXxmc() == null)) {
            return Result.error("学校名称不能为空!");
        }
        VeBaseSchool model = this.veBaseSchoolService.getSchoolByName(null, veBaseSchool.getXxmc());
        if (model != null) {
            return Result.error("学校名称已存在!");
        }
        if ((!"".equals(veBaseSchool.getJxnyName())) && (veBaseSchool.getJxnyName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseSchool.getJxnyName()));
            veBaseSchool.setJxny(Integer.valueOf(times.intValue()));
        }
        this.veBaseSchoolService.save(veBaseSchool);
        return Result.ok("添加成功!");
    }

    @AutoLog("添加校区信息")
    @ApiOperation(value="添加校区信息", notes="添加校区信息")
    @PostMapping({"/addCampus"})
    @Transactional
    public Result<?> addCampus(@RequestBody VeBaseCampus veBaseCampus)
    {
        if (("".equals(veBaseCampus.getXqdm())) || (veBaseCampus.getXqdm() == null)) {
            return Result.error("校区代码不能为空!");
        }
        if (("".equals(veBaseCampus.getXqmc())) || (veBaseCampus.getXqmc() == null)) {
            return Result.error("校区名称不能为空!");
        }
        VeBaseCampus model1 = this.veBaseCampusService.getCampusByName(null, veBaseCampus.getXqmc());
        if (model1 != null) {
            return Result.error("校区名称已存在!");
        }
        VeBaseCampus model2 = this.veBaseCampusService.getCampusByCode(null, veBaseCampus.getXqdm());
        if (model2 != null) {
            return Result.error("校区代码已存在!");
        }
        if ((!"".equals(veBaseCampus.getXqlxdh())) && (veBaseCampus.getXqlxdh() != null) && (!PhoneUtil.isMobile(veBaseCampus.getXqlxdh()))) {
            return Result.error("联系电话错误!");
        }
        if ((!"".equals(veBaseCampus.getDzyj())) && (veBaseCampus.getDzyj() != null) && (!EmailUtil.isEmail(veBaseCampus.getDzyj()))) {
            return Result.error("电子邮件错误!");
        }
        this.veBaseCampusService.save(veBaseCampus);

        List<VeBaseCampus> list = this.veBaseCampusService.list();
        this.redisUtils.set("queryCampusList", list);
        return Result.ok("添加成功!");
    }

    @AutoLog("添加部门信息")
    @ApiOperation(value="添加部门信息", notes="添加部门信息")
    @PostMapping({"/addDepartment"})
    public Result<?> addDepartment(@RequestBody VeBaseDepartment veBaseDepartment)
            throws ParseException
    {
        if (("".equals(veBaseDepartment.getJgh())) || (veBaseDepartment.getJgh() == null)) {
            return Result.error("部门号不能为空!");
        }
        if (("".equals(veBaseDepartment.getJgmc())) || (veBaseDepartment.getJgmc() == null)) {
            return Result.error("部门名称不能为空!");
        }
        VeBaseDepartment model1 = this.veBaseDepartmentService.getDepartmentByName(null, veBaseDepartment.getJgmc());
        if (model1 != null) {
            return Result.error("部门名称已存在!");
        }
        VeBaseDepartment model2 = this.veBaseDepartmentService.getDepartmentByCode(null, veBaseDepartment.getJgh());
        if (model2 != null) {
            return Result.error("部门号已存在!");
        }
        if ((!"".equals(veBaseDepartment.getTelephone())) && (veBaseDepartment.getTelephone() != null) && (!PhoneUtil.isMobile(veBaseDepartment.getTelephone()))) {
            return Result.error("电话号码错误!");
        }
        if ((!"".equals(veBaseDepartment.getJlnyName())) && (veBaseDepartment.getJlnyName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseDepartment.getJlnyName()));
            veBaseDepartment.setJlny(Integer.valueOf(times.intValue()));
        }
        else
        {
            veBaseDepartment.setJlny(Integer.valueOf(0));
        }
        this.veBaseDepartmentService.save(veBaseDepartment);

        List<VeBaseDepartment> list = this.veBaseDepartmentService.getTreeList();
        this.redisUtils.set("queryDepartmentList", list);

        List<VeBaseDepartment> listTwo = this.veBaseDepartmentService.getDepartmentAndTeacherList();
        this.redisUtils.set("queryDepartmentAndTeacherList", listTwo);

        List<Map<String, Object>> listTree = this.veBaseAppUserService.queryOrganUserTreeList(null);
        this.redisUtils.set("queryOrganUserTreeList", listTree);
        return Result.ok("添加成功!");
    }

    @AutoLog("添加教研组(专业组)信息")
    @ApiOperation(value="添加教研组(专业组)信息", notes="添加教研组(专业组)信息")
    @PostMapping({"/addJYZ"})
    @Transactional
    public Result<?> addJYZ(@RequestBody VeBaseJYZ veBaseJYZ)
    {
        if (("".equals(veBaseJYZ.getJyzbh())) || (veBaseJYZ.getJyzbh() == null)) {
            return Result.error("教研组编号不能为空!");
        }
        if (("".equals(veBaseJYZ.getJyzmc())) || (veBaseJYZ.getJyzmc() == null)) {
            return Result.error("教研组名称不能为空!");
        }
        VeBaseJYZ model1 = this.veBaseJYZService.getJYZByName(null, veBaseJYZ.getJyzmc());
        if (model1 != null) {
            return Result.error("教研组名称已存在!");
        }
        VeBaseJYZ model2 = this.veBaseJYZService.getJYZByCode(null, veBaseJYZ.getJyzbh());
        if (model2 != null) {
            return Result.error("教研组编号已存在!");
        }
        if ((!"".equals(veBaseJYZ.getTelephone())) && (veBaseJYZ.getTelephone() != null) && (!PhoneUtil.isMobile(veBaseJYZ.getTelephone()))) {
            return Result.error("电话号码错误!");
        }
        if ((!"".equals(veBaseJYZ.getJlnyName())) && (veBaseJYZ.getJlnyName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseJYZ.getJlnyName()));
            veBaseJYZ.setJlny(Integer.valueOf(times.intValue()));
        }
        else
        {
            veBaseJYZ.setJlny(Integer.valueOf(0));
        }
        this.veBaseJYZService.save(veBaseJYZ);

        List<Map<String, Object>> list = this.veBaseJYZService.getJYZTreeList();
        this.redisUtils.set("queryJYZTreeList", list);
        return Result.ok("添加成功!");
    }

    @AutoLog("添加院系(专业部)信息")
    @ApiOperation(value="添加院系(专业部)信息", notes="添加院系(专业部)信息")
    @PostMapping({"/addFaculty"})
    public Result<?> addFaculty(@RequestBody VeBaseFaculty veBaseFaculty)
    {
        if (("".equals(veBaseFaculty.getYxdm())) || (veBaseFaculty.getYxdm() == null)) {
            return Result.error("院系代码不能为空!");
        }
        if (("".equals(veBaseFaculty.getYxmc())) || (veBaseFaculty.getYxmc() == null)) {
            return Result.error("院系名称不能为空!");
        }
        VeBaseFaculty model1 = this.veBaseFacultyService.getFacultyByName(null, veBaseFaculty.getYxmc());
        if (model1 != null) {
            return Result.error("院系名称已存在!");
        }
        VeBaseFaculty model2 = this.veBaseFacultyService.getFacultyByCode(null, veBaseFaculty.getYxdm());
        if (model2 != null) {
            return Result.error("院系代码已存在!");
        }
        this.veBaseFacultyService.save(veBaseFaculty);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("status", Integer.valueOf(1));
        queryWrapper.orderByAsc("yxdm");

        List<VeBaseFaculty> list = this.veBaseFacultyService.list(queryWrapper);
        this.redisUtils.set("queryFacultyList", list);
        List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        return Result.ok("添加成功!");
    }

    @AutoLog("添加专业信息")
    @ApiOperation(value="添加专业信息", notes="添加专业信息")
    @PostMapping({"/addSpecialty"})
    @Transactional
    public Result<?> addSpecialty(@RequestBody VeBaseSpecialty veBaseSpecialty)
    {
        if (("".equals(veBaseSpecialty.getZybh())) || (veBaseSpecialty.getZybh() == null)) {
            return Result.error("专业编号不能为空!");
        }
        if (("".equals(veBaseSpecialty.getZydm())) || (veBaseSpecialty.getZydm() == null)) {
            return Result.error("专业代码不能为空!");
        }
        if (("".equals(veBaseSpecialty.getZymc())) || (veBaseSpecialty.getZymc() == null)) {
            return Result.error("专业名称不能为空!");
        }
        if (("".equals(veBaseSpecialty.getXz())) || (veBaseSpecialty.getXz() == null)) {
            return Result.error("学制不能为空!");
        }
        VeBaseSpecialty model1 = this.veBaseSpecialtyService.getSpecialtyByName(null, veBaseSpecialty.getZymc());
        if (model1 != null) {
            return Result.error("专业名称已存在!");
        }
        VeBaseSpecialty model2 = this.veBaseSpecialtyService.getSpecialtyByBH(null, veBaseSpecialty.getZybh());
        if (model2 != null) {
            return Result.error("专业编号已存在!");
        }
        if (veBaseSpecialty.getPid() == null)
        {
            if (veBaseSpecialty.getFalid() == null) {
                return Result.error("请选择所属专业部!");
            }
            veBaseSpecialty.setPid(Integer.valueOf(0));
        }
        else
        {
            VeBaseSpecialty model3 = (VeBaseSpecialty)this.veBaseSpecialtyService.getById(veBaseSpecialty.getPid());
            veBaseSpecialty.setFalid(model3.getFalid());
        }
        if ((!"".equals(veBaseSpecialty.getJlnyName())) && (veBaseSpecialty.getJlnyName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseSpecialty.getJlnyName()));
            veBaseSpecialty.setJlny(Integer.valueOf(times.intValue()));
        }
        else
        {
            veBaseSpecialty.setJlny(Integer.valueOf(0));
        }
        veBaseSpecialty.setTerminalid(Integer.valueOf(1));
        this.veBaseSpecialtyService.save(veBaseSpecialty);

        List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        return Result.ok("添加成功!");
    }

    @AutoLog("添加年级信息")
    @ApiOperation(value="添加年级信息", notes="添加年级信息")
    @PostMapping({"/addGrade"})
    public Result<?> addGrade(@RequestBody VeBaseGrade veBaseGrade)
    {
        if (("".equals(veBaseGrade.getNjdm())) || (veBaseGrade.getNjdm() == null)) {
            return Result.error("年级代码不能为空!");
        }
        if (veBaseGrade.getNjdm().length() > 4) {
            return Result.error("年级代码过长!");
        }
        if (("".equals(veBaseGrade.getNjmc())) || (veBaseGrade.getNjmc() == null)) {
            return Result.error("年级名称不能为空!");
        }
        if (veBaseGrade.getRxnf() == null) {
            return Result.error("入学年份不能为空!");
        }
        VeBaseGrade model1 = this.veBaseGradeService.getGradeByName(null, veBaseGrade.getNjmc());
        if (model1 != null) {
            return Result.error("年级名称已存在!");
        }
        VeBaseGrade model2 = this.veBaseGradeService.getGradeByCode(null, veBaseGrade.getNjdm());
        if (model2 != null) {
            return Result.error("年级代码已存在!");
        }
        VeBaseGrade model3 = this.veBaseGradeService.getGradeByRxnf(null, veBaseGrade.getRxnf());
        if (model3 != null) {
            return Result.error("入学年份已存在!");
        }
        if ((!"".equals(veBaseGrade.getBmjzrqName())) && (veBaseGrade.getBmjzrqName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseGrade.getBmjzrqName()));
            veBaseGrade.setBmjzrq(Integer.valueOf(times.intValue()));
        }
        this.veBaseGradeService.save(veBaseGrade);
        QueryWrapper queryWrapper = new QueryWrapper();

        queryWrapper.eq("njzt", Integer.valueOf(1));
        queryWrapper.orderByDesc("rxnf");

        List<VeBaseGrade> list = this.veBaseGradeService.list(queryWrapper);
        this.redisUtils.set("queryGradeList", list);
        return Result.ok("添加成功!");
    }

    @AutoLog("添加学期信息")
    @ApiOperation(value="添加学期信息", notes="添加学期信息")
    @PostMapping({"/addSemester"})
    public Result<?> addSemester(@RequestBody VeBaseSemester veBaseSemester)
    {
        if (("".equals(veBaseSemester.getXqm())) || (veBaseSemester.getXqm() == null)) {
            return Result.error("学期码不能为空!");
        }
        if (("".equals(veBaseSemester.getXqmc())) || (veBaseSemester.getXqmc() == null)) {
            return Result.error("学期名称不能为空!");
        }
        VeBaseSemester model1 = this.veBaseSemesterService.getSemesterByName(null, veBaseSemester.getXqmc());
        if (model1 != null) {
            return Result.error("学期名称已存在!");
        }
        VeBaseSemester model2 = this.veBaseSemesterService.getSemesterByCode(null, veBaseSemester.getXqm());
        if (model2 != null) {
            return Result.error("学期码已存在!");
        }
        if (veBaseSemester.getIscurrent().intValue() == 0) {
            try
            {
                VeBaseSemester model3 = this.veBaseSemesterService.getSemesterByIsCurrent(null);
                if (model3 != null) {
                    return Result.error("当前学期已存在!");
                }
            }
            catch (Exception ex)
            {
                return Result.error("当前学期已存在!");
            }
        }
        if ((!"".equals(veBaseSemester.getXqksrqName())) && (veBaseSemester.getXqksrqName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseSemester.getXqksrqName()));
            veBaseSemester.setXqksrq(Integer.valueOf(times.intValue()));
        }
        else
        {
            veBaseSemester.setXqksrq(Integer.valueOf(0));
        }
        if ((!"".equals(veBaseSemester.getXqjsrqName())) && (veBaseSemester.getXqjsrqName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseSemester.getXqjsrqName()));
            veBaseSemester.setXqjsrq(Integer.valueOf(times.intValue()));
        }
        else
        {
            veBaseSemester.setXqjsrq(Integer.valueOf(0));
        }
        this.veBaseSemesterService.save(veBaseSemester);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("xqm");

        List<VeBaseSemester> list = this.veBaseSemesterService.list(queryWrapper);
        this.redisUtils.set("querySemesterList", list);
        return Result.ok("添加成功!");
    }

    @AutoLog("添加班级信息")
    @ApiOperation(value="添加班级信息", notes="添加班级信息")
    @PostMapping({"/addBanJi"})
    @Transactional
    public Result<?> addBanJi(@RequestBody VeBaseBanJi veBaseBanJi)
    {
        if (("".equals(veBaseBanJi.getXzbdm())) || (veBaseBanJi.getXzbdm() == null)) {
            return Result.error("行政班代码不能为空!");
        }
        if (("".equals(veBaseBanJi.getXzbmc())) || (veBaseBanJi.getXzbmc() == null)) {
            return Result.error("行政班名称不能为空!");
        }
        VeBaseBanJi model1 = this.veBaseBanJiService.getBanJiByName(null, veBaseBanJi.getXzbmc());
        if (model1 != null) {
            return Result.error("行政班名称已存在!");
        }
        VeBaseBanJi model2 = this.veBaseBanJiService.getBanJiByCode(null, veBaseBanJi.getXzbdm());
        if (model2 != null) {
            return Result.error("行政班代码已存在!");
        }
        if (veBaseBanJi.getGradeId() == null) {
            return Result.error("年级不能为空!");
        }
        if (("".equals(veBaseBanJi.getJbny())) || (veBaseBanJi.getJbny() == null)) {
            return Result.error("建班年月不能为空!");
        }
        VeBaseGrade veBaseGrade = (VeBaseGrade)this.veBaseGradeService.getById(veBaseBanJi.getGradeId());
        if (veBaseGrade == null) {
            return Result.error("该年级不存在!");
        }
        veBaseBanJi.setRxnf(veBaseGrade.getRxnf().toString());
        veBaseBanJi.setNjdm(veBaseGrade.getNjdm());
        veBaseBanJi.setNjmc(veBaseGrade.getNjmc());
        if (veBaseBanJi.getSpecId() == null) {
            return Result.error("专业不能为空!");
        }
        VeBaseSpecialty veBaseSpecialty = (VeBaseSpecialty)this.veBaseSpecialtyService.getById(veBaseBanJi.getSpecId());
        if (veBaseSpecialty == null) {
            return Result.error("该专业不存在!");
        }
        veBaseBanJi.setZydm(veBaseSpecialty.getZydm());
        if ((!"".equals(veBaseBanJi.getBysjName())) && (veBaseBanJi.getBysjName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseBanJi.getBysjName()));
            veBaseBanJi.setBysj(Integer.valueOf(times.intValue()));
            if (times.longValue() <= System.currentTimeMillis() / 1000L) {
                veBaseBanJi.setBystatus(Integer.valueOf(2));
            }
        }
        this.veBaseBanJiService.save(veBaseBanJi);

        VeBaseBanJiBzr veBaseBanJiBzr = new VeBaseBanJiBzr();
        veBaseBanJiBzr.setBjId(veBaseBanJi.getId());
        veBaseBanJiBzr.setBzrUserId(veBaseBanJi.getBzrUserId());
        veBaseBanJiBzr.setBzrUserName(veBaseBanJi.getBzrUserName());
        Long i = Long.valueOf(System.currentTimeMillis() / 1000L);
        veBaseBanJiBzr.setCreateTime(Integer.valueOf(Integer.parseInt(i.toString())));
        veBaseBanJiBzr.setIsCurrent(Integer.valueOf(0));
        LoginUser user = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        veBaseBanJiBzr.setCreateUserId(user.getUsername());
        veBaseBanJiBzr.setStartDate(Integer.valueOf(Integer.parseInt(i.toString())));
        this.veBaseBanJiBzrService.save(veBaseBanJiBzr);


        List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        return Result.ok("添加成功!");
    }

    @AutoLog("添加学制信息")
    @ApiOperation(value="添加学制信息", notes="添加学制信息")
    @PostMapping({"/addXueZhi"})
    @Transactional
    public Result<?> addXueZhi(@RequestBody VeBaseXueZhi veBaseXueZhi)
    {
        if (("".equals(veBaseXueZhi.getXzmc())) || (veBaseXueZhi.getXzmc() == null)) {
            return Result.error("学制名称不能为空!");
        }
        VeBaseXueZhi model = this.veBaseXueZhiService.getXueZhiByName(null, veBaseXueZhi.getXzmc());
        if (model != null) {
            return Result.error("学制名称已存在!");
        }
        this.veBaseXueZhiService.save(veBaseXueZhi);

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByAsc("years");
        List<VeBaseXueZhi> list = this.veBaseXueZhiService.list(queryWrapper);
        this.redisUtils.set("queryXueZhiList", list);
        return Result.ok("添加成功!");
    }

    @AutoLog("添加节假日信息")
    @ApiOperation(value="添加节假日信息", notes="添加节假日信息")
    @PostMapping({"/addFestival"})
    public Result<?> addFestival(@RequestBody VeBaseFestival veBaseFestival)
    {
        if (veBaseFestival.getSemId() == null) {
            return Result.error("学期不能为空!");
        }
        if (("".equals(veBaseFestival.getTitle())) || (veBaseFestival.getTitle() == null)) {
            return Result.error("节假日名称不能为空!");
        }
        if (("".equals(veBaseFestival.getBeginDate())) || (veBaseFestival.getBeginDate() == null)) {
            return Result.error("开始日期不能为空!");
        }
        if (("".equals(veBaseFestival.getEndDate())) || (veBaseFestival.getEndDate() == null)) {
            return Result.error("结束日期不能为空!");
        }
        if (("".equals(veBaseFestival.getDescription())) || (veBaseFestival.getDescription() == null)) {
            return Result.error("节假日描述不能为空!");
        }
        this.veBaseFestivalService.save(veBaseFestival);
        return Result.ok("添加成功!");
    }

    @AutoLog("系统配置修改")
    @ApiOperation(value="系统配置修改", notes="系统配置修改")
    @PostMapping({"/updateSysConfig"})
    public Result<?> updateSysConfig(@RequestBody List<VeBaseSysConfig> list)
    {
        if (list.size() > 0) {
            for (VeBaseSysConfig veBaseSysConfig : list) {
                this.veBaseSysConfigService.updateById(veBaseSysConfig);
            }
        }
        return Result.ok("修改成功! ");
    }

    @AutoLog("班级毕业定时器")
    @ApiOperation(value="班级毕业定时器", notes="班级毕业定时器")
    @Scheduled(cron="1 0 0 * * ?")
    public Result<?> updateBanJiSchedule()
    {
        this.veBaseBanJiService.updateBanJiSchedule();
        return Result.ok();
    }

    @AutoLog("编辑学校信息")
    @ApiOperation(value="编辑学校信息", notes="编辑学校信息")
    @PostMapping({"/editSchool"})
    public Result<?> editSchool(@RequestBody VeBaseSchool veBaseSchool)
    {
        if (("".equals(veBaseSchool.getXxdm())) || (veBaseSchool.getXxdm() == null)) {
            return Result.error("学校代码不能为空!");
        }
        if (("".equals(veBaseSchool.getXxmc())) || (veBaseSchool.getXxmc() == null)) {
            return Result.error("学校名称不能为空!");
        }
        if (("".equals(veBaseSchool.getXxdz())) || (veBaseSchool.getXxdz() == null)) {
            return Result.error("学校地址不能为空!");
        }
        if (("".equals(veBaseSchool.getXxbxlxm())) || (veBaseSchool.getXxbxlxm() == null)) {
            return Result.error("学校办学类不能为空!");
        }
        VeBaseSchool model = this.veBaseSchoolService.getSchoolByName(veBaseSchool.getId(), veBaseSchool.getXxmc());
        if (model != null) {
            return Result.error("学校名称已存在!");
        }
        if ((!"".equals(veBaseSchool.getDzxx())) && (veBaseSchool.getDzxx() != null) && (!EmailUtil.isEmail(veBaseSchool.getDzxx()))) {
            return Result.error("电子信箱错误!");
        }
        if ((!"".equals(veBaseSchool.getJxnyName())) && (veBaseSchool.getJxnyName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseSchool.getJxnyName()));
            veBaseSchool.setJxny(Integer.valueOf(times.intValue()));
        }
        else
        {
            veBaseSchool.setJxny(Integer.valueOf(0));
        }
        if (veBaseSchool.getXzUserId() != null)
        {
            VeBaseTeacher veBaseTeacher = this.veBaseTeacherService.getByUserId(veBaseSchool.getXzUserId());
            if (veBaseTeacher != null) {
                veBaseSchool.setXzxm(veBaseTeacher.getXm());
            }
        }
        if (veBaseSchool.getDwfzrUserId() != null)
        {
            VeBaseTeacher veBaseTeacher = this.veBaseTeacherService.getByUserId(veBaseSchool.getDwfzrUserId());
            if (veBaseTeacher != null) {
                veBaseSchool.setDwfzrxm(veBaseTeacher.getXm());
            }
        }
        this.veBaseSchoolService.updateById(veBaseSchool);
        return Result.ok("修改成功!");
    }

    @AutoLog("编辑校区信息")
    @ApiOperation(value="编辑校区信息", notes="编辑校区信息")
    @PostMapping({"/editCampus"})
    public Result<?> editCampus(@RequestBody VeBaseCampus veBaseCampus)
    {
        if (("".equals(veBaseCampus.getXqdm())) || (veBaseCampus.getXqdm() == null)) {
            return Result.error("校区代码不能为空!");
        }
        if (("".equals(veBaseCampus.getXqmc())) || (veBaseCampus.getXqmc() == null)) {
            return Result.error("校区名称不能为空!");
        }
        VeBaseCampus model1 = this.veBaseCampusService.getCampusByName(veBaseCampus.getId(), veBaseCampus.getXqmc());
        if (model1 != null) {
            return Result.error("校区名称已存在!");
        }
        VeBaseCampus model2 = this.veBaseCampusService.getCampusByCode(veBaseCampus.getId(), veBaseCampus.getXqdm());
        if (model2 != null) {
            return Result.error("校区代码已存在!");
        }
        if ((!"".equals(veBaseCampus.getXqlxdh())) && (veBaseCampus.getXqlxdh() != null) && (!PhoneUtil.isMobile(veBaseCampus.getXqlxdh()))) {
            return Result.error("联系电话错误!");
        }
        if ((!"".equals(veBaseCampus.getDzyj())) && (veBaseCampus.getDzyj() != null) && (!EmailUtil.isEmail(veBaseCampus.getDzyj()))) {
            return Result.error("电子邮件错误!");
        }
        if (("".equals(veBaseCampus.getXqmj())) || (veBaseCampus.getXqmj() == null)) {
            veBaseCampus.setXqmj(Double.valueOf(0.0D));
        }
        if (("".equals(veBaseCampus.getXqjzmj())) || (veBaseCampus.getXqjzmj() == null)) {
            veBaseCampus.setXqjzmj(Double.valueOf(0.0D));
        }
        if (("".equals(veBaseCampus.getXqjxkysbzz())) || (veBaseCampus.getXqjxkysbzz() == null)) {
            veBaseCampus.setXqjxkysbzz(Double.valueOf(0.0D));
        }
        if (("".equals(veBaseCampus.getXqgdzczz())) || (veBaseCampus.getXqgdzczz() == null)) {
            veBaseCampus.setXqgdzczz(Double.valueOf(0.0D));
        }
        this.veBaseCampusService.updateById(veBaseCampus);

        List<VeBaseCampus> list = this.veBaseCampusService.list();
        this.redisUtils.set("queryCampusList", list);
        return Result.ok("修改成功!");
    }

    @AutoLog("编辑部门信息")
    @ApiOperation(value="编辑部门信息", notes="编辑部门信息")
    @PostMapping({"/editDepartment"})
    public Result<?> editDepartment(@RequestBody VeBaseDepartment veBaseDepartment)
            throws ParseException
    {
        if (("".equals(veBaseDepartment.getJgh())) || (veBaseDepartment.getJgh() == null)) {
            return Result.error("部门号不能为空!");
        }
        if (("".equals(veBaseDepartment.getJgmc())) || (veBaseDepartment.getJgmc() == null)) {
            return Result.error("部门名称不能为空!");
        }
        VeBaseDepartment model1 = this.veBaseDepartmentService.getDepartmentByName(veBaseDepartment.getId(), veBaseDepartment.getJgmc());
        if (model1 != null) {
            return Result.error("部门名称已存在!");
        }
        VeBaseDepartment model2 = this.veBaseDepartmentService.getDepartmentByCode(veBaseDepartment.getId(), veBaseDepartment.getJgh());
        if (model2 != null) {
            return Result.error("部门号已存在!");
        }
        if ((!"".equals(veBaseDepartment.getTelephone())) && (veBaseDepartment.getTelephone() != null) && (!PhoneUtil.isMobile(veBaseDepartment.getTelephone()))) {
            return Result.error("电话号码错误!");
        }
        if ((!"".equals(veBaseDepartment.getJlnyName())) && (veBaseDepartment.getJlnyName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseDepartment.getJlnyName()));
            veBaseDepartment.setJlny(Integer.valueOf(times.intValue()));
        }
        else
        {
            veBaseDepartment.setJlny(Integer.valueOf(0));
        }
        this.veBaseDepartmentService.updateById(veBaseDepartment);

        List<VeBaseDepartment> list = this.veBaseDepartmentService.getTreeList();
        this.redisUtils.set("queryDepartmentList", list);

        List<VeBaseDepartment> listTwo = this.veBaseDepartmentService.getDepartmentAndTeacherList();
        this.redisUtils.set("queryDepartmentAndTeacherList", listTwo);

        List<Map<String, Object>> listTree = this.veBaseAppUserService.queryOrganUserTreeList(null);
        this.redisUtils.set("queryOrganUserTreeList", listTree);
        return Result.ok("修改成功!");
    }

    @AutoLog("编辑教研组(专业组)信息")
    @ApiOperation(value="编辑教研组(专业组)信息", notes="编辑教研组(专业组)信息")
    @PostMapping({"/editJYZ"})
    @Transactional
    public Result<?> editJYZ(@RequestBody VeBaseJYZ veBaseJYZ)
    {
        if (("".equals(veBaseJYZ.getJyzbh())) || (veBaseJYZ.getJyzbh() == null)) {
            return Result.error("教研组编号不能为空!");
        }
        if (("".equals(veBaseJYZ.getJyzmc())) || (veBaseJYZ.getJyzmc() == null)) {
            return Result.error("教研组名称不能为空!");
        }
        VeBaseJYZ model1 = this.veBaseJYZService.getJYZByName(veBaseJYZ.getId(), veBaseJYZ.getJyzmc());
        if (model1 != null) {
            return Result.error("教研组名称已存在!");
        }
        VeBaseJYZ model2 = this.veBaseJYZService.getJYZByCode(veBaseJYZ.getId(), veBaseJYZ.getJyzbh());
        if (model2 != null) {
            return Result.error("教研组编号已存在!");
        }
        if ((!"".equals(veBaseJYZ.getTelephone())) && (veBaseJYZ.getTelephone() != null) && (!PhoneUtil.isMobile(veBaseJYZ.getTelephone()))) {
            return Result.error("电话号码错误!");
        }
        if ((!"".equals(veBaseJYZ.getJlnyName())) && (veBaseJYZ.getJlnyName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseJYZ.getJlnyName()));
            veBaseJYZ.setJlny(Integer.valueOf(times.intValue()));
        }
        else
        {
            veBaseJYZ.setJlny(Integer.valueOf(0));
        }
        this.veBaseJYZService.updateById(veBaseJYZ);

        List<Map<String, Object>> list = this.veBaseJYZService.getJYZTreeList();
        this.redisUtils.set("queryJYZTreeList", list);
        return Result.ok("修改成功!");
    }

    @AutoLog("编辑院系(专业部)信息")
    @ApiOperation(value="编辑院系(专业部)信息", notes="编辑院系(专业部)信息")
    @PostMapping({"/editFaculty"})
    public Result<?> editFaculty(@RequestBody VeBaseFaculty veBaseFaculty)
    {
        if (("".equals(veBaseFaculty.getYxdm())) || (veBaseFaculty.getYxdm() == null)) {
            return Result.error("院系代码不能为空!");
        }
        if (("".equals(veBaseFaculty.getYxmc())) || (veBaseFaculty.getYxmc() == null)) {
            return Result.error("院系名称不能为空!");
        }
        VeBaseFaculty model1 = this.veBaseFacultyService.getFacultyByName(veBaseFaculty.getId(), veBaseFaculty.getYxmc());
        if (model1 != null) {
            return Result.error("院系名称已存在!");
        }
        VeBaseFaculty model2 = this.veBaseFacultyService.getFacultyByCode(veBaseFaculty.getId(), veBaseFaculty.getYxdm());
        if (model2 != null) {
            return Result.error("院系代码已存在!");
        }
        this.veBaseFacultyService.updateById(veBaseFaculty);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("status", Integer.valueOf(1));
        queryWrapper.orderByAsc("yxdm");

        List<VeBaseFaculty> list = this.veBaseFacultyService.list(queryWrapper);
        this.redisUtils.set("queryFacultyList", list);
        List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        return Result.ok("修改成功!");
    }

    @AutoLog("编辑专业信息")
    @ApiOperation(value="编辑专业信息", notes="编辑专业信息")
    @PostMapping({"/editSpecialty"})
    @Transactional
    public Result<?> editSpecialty(@RequestBody VeBaseSpecialty veBaseSpecialty)
    {
        if (("".equals(veBaseSpecialty.getZybh())) || (veBaseSpecialty.getZybh() == null)) {
            return Result.error("专业编号不能为空!");
        }
        if (("".equals(veBaseSpecialty.getZydm())) || (veBaseSpecialty.getZydm() == null)) {
            return Result.error("专业代码不能为空!");
        }
        if (("".equals(veBaseSpecialty.getZymc())) || (veBaseSpecialty.getZymc() == null)) {
            return Result.error("专业名称不能为空!");
        }
        if (("".equals(veBaseSpecialty.getXz())) || (veBaseSpecialty.getXz() == null)) {
            return Result.error("学制不能为空!");
        }
        VeBaseSpecialty model1 = this.veBaseSpecialtyService.getSpecialtyByName(veBaseSpecialty.getId(), veBaseSpecialty.getZymc());
        if (model1 != null) {
            return Result.error("专业名称已存在!");
        }
        VeBaseSpecialty model2 = this.veBaseSpecialtyService.getSpecialtyByBH(veBaseSpecialty.getId(), veBaseSpecialty.getZybh());
        if (model2 != null) {
            return Result.error("专业编号已存在!");
        }
        if (veBaseSpecialty.getPid() == null)
        {
            if (veBaseSpecialty.getFalid() == null) {
                return Result.error("请选择所属专业部! ");
            }
            this.veBaseSpecialtyService.updateSpecialtyFalIdById(veBaseSpecialty.getId(), veBaseSpecialty.getFalid());
            veBaseSpecialty.setPid(Integer.valueOf(0));
        }
        else
        {
            VeBaseSpecialty model3 = (VeBaseSpecialty)this.veBaseSpecialtyService.getById(veBaseSpecialty.getId());
            if (model3.getPid() != veBaseSpecialty.getPid())
            {
                VeBaseSpecialty model4 = (VeBaseSpecialty)this.veBaseSpecialtyService.getById(veBaseSpecialty.getPid());

                this.veBaseSpecialtyService.updateSpecialtyFalIdById(veBaseSpecialty.getId(), model4.getFalid());
            }
        }
        if (veBaseSpecialty.getZyjss() == null) {
            veBaseSpecialty.setZyjss(Integer.valueOf(0));
        }
        if (veBaseSpecialty.getZxf() == null) {
            veBaseSpecialty.setZxf(Integer.valueOf(0));
        }
        if ((!"".equals(veBaseSpecialty.getJlnyName())) && (veBaseSpecialty.getJlnyName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseSpecialty.getJlnyName()));
            veBaseSpecialty.setJlny(Integer.valueOf(times.intValue()));
        }
        else
        {
            veBaseSpecialty.setJlny(Integer.valueOf(0));
        }
        this.veBaseSpecialtyService.updateById(veBaseSpecialty);

        List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        return Result.ok("修改成功!");
    }

    @AutoLog("编辑年级信息")
    @ApiOperation(value="编辑年级信息", notes="编辑年级信息")
    @PostMapping({"/editGrade"})
    public Result<?> editGrade(@RequestBody VeBaseGrade veBaseGrade)
    {
        if (("".equals(veBaseGrade.getNjdm())) || (veBaseGrade.getNjdm() == null)) {
            return Result.error("年级代码不能为空!");
        }
        if (veBaseGrade.getNjdm().length() > 4) {
            return Result.error("年级代码过长!");
        }
        if (("".equals(veBaseGrade.getNjmc())) || (veBaseGrade.getNjmc() == null)) {
            return Result.error("年级名称不能为空!");
        }
        if (veBaseGrade.getRxnf() == null) {
            return Result.error("入学年份不能为空!");
        }
        VeBaseGrade model1 = this.veBaseGradeService.getGradeByName(veBaseGrade.getId(), veBaseGrade.getNjmc());
        if (model1 != null) {
            return Result.error("年级名称已存在!");
        }
        VeBaseGrade model2 = this.veBaseGradeService.getGradeByCode(veBaseGrade.getId(), veBaseGrade.getNjdm());
        if (model2 != null) {
            return Result.error("年级代码已存在!");
        }
        VeBaseGrade model3 = this.veBaseGradeService.getGradeByRxnf(veBaseGrade.getId(), veBaseGrade.getRxnf());
        if (model3 != null) {
            return Result.error("入学年份已存在!");
        }
        if ((!"".equals(veBaseGrade.getBmjzrqName())) && (veBaseGrade.getBmjzrqName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseGrade.getBmjzrqName()));
            veBaseGrade.setBmjzrq(Integer.valueOf(times.intValue()));
        }
        this.veBaseGradeService.updateById(veBaseGrade);
        QueryWrapper queryWrapper = new QueryWrapper();

        queryWrapper.eq("njzt", Integer.valueOf(1));
        queryWrapper.orderByDesc("rxnf");

        List<VeBaseGrade> list = this.veBaseGradeService.list(queryWrapper);
        this.redisUtils.set("queryGradeList", list);
        return Result.ok("修改成功!");
    }

    @AutoLog("编辑学期信息")
    @ApiOperation(value="编辑学期信息", notes="编辑学期信息")
    @PostMapping({"/editSemester"})
    public Result<?> editSemester(@RequestBody VeBaseSemester veBaseSemester)
    {
        if (("".equals(veBaseSemester.getXqm())) || (veBaseSemester.getXqm() == null)) {
            return Result.error("学期码不能为空!");
        }
        if (("".equals(veBaseSemester.getXqmc())) || (veBaseSemester.getXqmc() == null)) {
            return Result.error("学期名称不能为空!");
        }
        VeBaseSemester model1 = this.veBaseSemesterService.getSemesterByName(veBaseSemester.getId(), veBaseSemester.getXqmc());
        if (model1 != null) {
            return Result.error("学期名称已存在!");
        }
        VeBaseSemester model2 = this.veBaseSemesterService.getSemesterByCode(veBaseSemester.getId(), veBaseSemester.getXqm());
        if (model2 != null) {
            return Result.error("学期码已存在!");
        }
        if (veBaseSemester.getIscurrent().intValue() == 0) {
            try
            {
                VeBaseSemester model3 = this.veBaseSemesterService.getSemesterByIsCurrent(veBaseSemester.getId());
                if (model3 != null) {
                    return Result.error("当前学期已存在!");
                }
            }
            catch (Exception ex)
            {
                return Result.error("当前学期已存在!");
            }
        }
        if ((!"".equals(veBaseSemester.getXqksrqName())) && (veBaseSemester.getXqksrqName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseSemester.getXqksrqName()));
            veBaseSemester.setXqksrq(Integer.valueOf(times.intValue()));
        }
        if ((!"".equals(veBaseSemester.getXqjsrqName())) && (veBaseSemester.getXqjsrqName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseSemester.getXqjsrqName()));
            veBaseSemester.setXqjsrq(Integer.valueOf(times.intValue()));
        }
        this.veBaseSemesterService.updateById(veBaseSemester);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("xqm");

        List<VeBaseSemester> list = this.veBaseSemesterService.list(queryWrapper);
        this.redisUtils.set("querySemesterList", list);
        return Result.ok("修改成功!");
    }

    @AutoLog("编辑班级信息")
    @ApiOperation(value="编辑班级信息", notes="编辑班级信息")
    @PostMapping({"/editBanJi"})
    @Transactional
    public Result<?> editBanJi(@RequestBody VeBaseBanJi veBaseBanJi)
    {
        if (("".equals(veBaseBanJi.getXzbdm())) || (veBaseBanJi.getXzbdm() == null)) {
            return Result.error("行政班代码不能为空!");
        }
        if (("".equals(veBaseBanJi.getXzbmc())) || (veBaseBanJi.getXzbmc() == null)) {
            return Result.error("行政班名称不能为空!");
        }
        VeBaseBanJi model1 = this.veBaseBanJiService.getBanJiByName(veBaseBanJi.getId(), veBaseBanJi.getXzbmc());
        if (model1 != null) {
            return Result.error("行政班名称已存在!");
        }
        VeBaseBanJi model2 = this.veBaseBanJiService.getBanJiByCode(veBaseBanJi.getId(), veBaseBanJi.getXzbdm());
        if (model2 != null) {
            return Result.error("行政班代码已存在!");
        }
        if (veBaseBanJi.getGradeId() == null) {
            return Result.error("年级不能为空!");
        }
        if (("".equals(veBaseBanJi.getJbny())) || (veBaseBanJi.getJbny() == null)) {
            return Result.error("建班年月不能为空!");
        }
        VeBaseGrade veBaseGrade = (VeBaseGrade)this.veBaseGradeService.getById(veBaseBanJi.getGradeId());
        if (veBaseGrade == null) {
            return Result.error("该年级不存在!");
        }
        veBaseBanJi.setNjdm(veBaseGrade.getNjdm());
        veBaseBanJi.setNjmc(veBaseGrade.getNjmc());
        veBaseBanJi.setRxnf(veBaseGrade.getRxnf().toString());
        if (veBaseBanJi.getSpecId() == null) {
            return Result.error("专业不能为空!");
        }
        VeBaseSpecialty veBaseSpecialty = (VeBaseSpecialty)this.veBaseSpecialtyService.getById(veBaseBanJi.getSpecId());
        if (veBaseSpecialty == null) {
            return Result.error("该专业不存在!");
        }
        veBaseBanJi.setZydm(veBaseSpecialty.getZydm());
        if ((!"".equals(veBaseBanJi.getBysjName())) && (veBaseBanJi.getBysjName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseBanJi.getBysjName()));
            veBaseBanJi.setBysj(Integer.valueOf(times.intValue()));
            if (times.longValue() <= System.currentTimeMillis() / 1000L) {
                veBaseBanJi.setBystatus(Integer.valueOf(2));
            } else {
                veBaseBanJi.setBystatus(Integer.valueOf(0));
            }
        }
        VeBaseBanJi veBaseBanJi1 = (VeBaseBanJi)this.veBaseBanJiService.getById(veBaseBanJi.getId());
        this.veBaseBanJiService.updateById(veBaseBanJi);
        if (!veBaseBanJi1.getBzrUserId().equals(veBaseBanJi.getBzrUserId()))
        {
            Long i = Long.valueOf(System.currentTimeMillis() / 1000L);

            VeBaseBanJiBzr veBaseBanJiBzr = this.veBaseBanJiBzrService.getByBjIdAndBzrUserId(veBaseBanJi.getId(), veBaseBanJi1.getBzrUserId());
            veBaseBanJiBzr.setIsCurrent(Integer.valueOf(1));
            veBaseBanJiBzr.setEndDate(Integer.valueOf(Integer.parseInt(i.toString())));


            VeBaseBanJiBzr veBaseBanJiBzr1 = new VeBaseBanJiBzr();
            veBaseBanJiBzr1.setBjId(veBaseBanJi.getId());
            veBaseBanJiBzr1.setBzrUserId(veBaseBanJi.getBzrUserId());
            veBaseBanJiBzr1.setBzrUserName(veBaseBanJi.getBzrUserName());
            veBaseBanJiBzr1.setCreateTime(Integer.valueOf(Integer.parseInt(i.toString())));
            veBaseBanJiBzr1.setIsCurrent(Integer.valueOf(0));
            LoginUser user = (LoginUser)SecurityUtils.getSubject().getPrincipal();
            veBaseBanJiBzr1.setCreateUserId(user.getUsername());
            veBaseBanJiBzr1.setStartDate(Integer.valueOf(Integer.parseInt(i.toString())));
            this.veBaseBanJiBzrService.save(veBaseBanJiBzr1);
        }
        List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        return Result.ok("修改成功!");
    }

    @AutoLog("编辑学制信息")
    @ApiOperation(value="编辑学制信息", notes="编辑学制信息")
    @PostMapping({"/editXueZhi"})
    public Result<?> editXueZhi(@RequestBody VeBaseXueZhi veBaseXueZhi)
    {
        if (("".equals(veBaseXueZhi.getXzmc())) || (veBaseXueZhi.getXzmc() == null)) {
            return Result.error("学制名称不能为空!");
        }
        VeBaseXueZhi model = this.veBaseXueZhiService.getXueZhiByName(veBaseXueZhi.getId(), veBaseXueZhi.getXzmc());
        if (model != null) {
            return Result.error("学制名称已存在!");
        }
        this.veBaseXueZhiService.updateById(veBaseXueZhi);

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByAsc("years");
        List<VeBaseXueZhi> list = this.veBaseXueZhiService.list(queryWrapper);
        this.redisUtils.set("queryXueZhiList", list);
        return Result.ok("修改成功!");
    }

    @AutoLog("编辑学生信息")
    @ApiOperation(value="编辑学生信息", notes="编辑学生信息")
    @PostMapping({"/editStudent"})
    @Transactional
    public Result<?> editStudent(@RequestBody VoStudentAndStudentInfo voStudentAndStudentInfo)
    {
        if (("".equals(voStudentAndStudentInfo.getXh())) || (voStudentAndStudentInfo.getXh() == null)) {
            return Result.error("学号不能为空! ");
        }
        if (("".equals(voStudentAndStudentInfo.getSfzh())) || (voStudentAndStudentInfo.getSfzh() == null)) {
            return Result.error("身份证号不能为空! ");
        }
        if (("".equals(voStudentAndStudentInfo.getXm())) || (voStudentAndStudentInfo.getXm() == null)) {
            return Result.error("姓名不能为空! ");
        }
        if (("".equals(voStudentAndStudentInfo.getXbm())) || (voStudentAndStudentInfo.getXbm() == null)) {
            return Result.error("性别不能为空! ");
        }
        if (!"".equals(IdCardUtil.IdentityCardVerification(voStudentAndStudentInfo.getSfzh()))) {
            return Result.error("身份证号错误! ");
        }
        VeBaseStudent model = this.veBaseStudentService.getStudentByXH(voStudentAndStudentInfo.getId(), voStudentAndStudentInfo.getXh());
        if (model != null) {
            return Result.error("学号已存在! ");
        }
        VeBaseStudent model1 = this.veBaseStudentService.getStudentByXH(voStudentAndStudentInfo.getId(), voStudentAndStudentInfo.getSfzh());
        if (model1 != null) {
            return Result.error("身份证号已存在! ");
        }
        if ((!"".equals(voStudentAndStudentInfo.getXslxdh())) && (voStudentAndStudentInfo.getXslxdh() != null) && (!PhoneUtil.isMobile(voStudentAndStudentInfo.getXslxdh()))) {
            return Result.error("学生联系电话错误! ");
        }
        if ((!"".equals(voStudentAndStudentInfo.getJtlxdh())) && (voStudentAndStudentInfo.getJtlxdh() != null) && (!PhoneUtil.isMobile(voStudentAndStudentInfo.getJtlxdh()))) {
            return Result.error("家庭联系电话错误! ");
        }
        if ((!"".equals(voStudentAndStudentInfo.getDzxx())) && (voStudentAndStudentInfo.getDzxx() != null) && (!EmailUtil.isEmail(voStudentAndStudentInfo.getDzxx()))) {
            return Result.error("电子信箱错误! ");
        }
        VeBaseStudent veBaseStudent = new VeBaseStudent();
        veBaseStudent.setId(voStudentAndStudentInfo.getId());
        veBaseStudent.setSfzh(voStudentAndStudentInfo.getSfzh());
        veBaseStudent.setXm(voStudentAndStudentInfo.getXm());
        veBaseStudent.setXbm(voStudentAndStudentInfo.getXbm());
        veBaseStudent.setXh(voStudentAndStudentInfo.getXh());
        veBaseStudent.setMzm(voStudentAndStudentInfo.getMzm());
        veBaseStudent.setJdfs(voStudentAndStudentInfo.getJdfs());
        veBaseStudent.setSfkns(voStudentAndStudentInfo.getSfkns());
        veBaseStudent.setFalId(voStudentAndStudentInfo.getFalId());
        veBaseStudent.setSpecId(voStudentAndStudentInfo.getSpecId());
        veBaseStudent.setBjId(voStudentAndStudentInfo.getBjId());
        veBaseStudent.setXz(voStudentAndStudentInfo.getXz());
        veBaseStudent.setBmh(voStudentAndStudentInfo.getBmh());
        if ((!"".equals(voStudentAndStudentInfo.getBjId())) && (voStudentAndStudentInfo.getBjId() != null))
        {
            VeBaseBanJi veBaseBanJi = (VeBaseBanJi)this.veBaseBanJiService.getById(voStudentAndStudentInfo.getBjId());
            if (veBaseBanJi != null) {
                veBaseStudent.setGradeId(veBaseBanJi.getGradeId());
            } else {
                veBaseStudent.setGradeId(null);
            }
            VeBaseStudent md = this.veBaseStudentService.getModelById(voStudentAndStudentInfo.getId());

            VeBaseBanJi veBaseBanJi1 = (VeBaseBanJi)this.veBaseBanJiService.getById(md.getBjId());
            if (veBaseBanJi1 != null)
            {
                if (md.getXbm().equals("1")) {
                    veBaseBanJi1.setNansrs(Integer.valueOf(veBaseBanJi1.getNansrs().intValue() - 1));
                } else {
                    veBaseBanJi1.setNvsrs(Integer.valueOf(veBaseBanJi1.getNvsrs().intValue() - 1));
                }
                this.veBaseBanJiService.updateById(veBaseBanJi1);
            }
            if (voStudentAndStudentInfo.getXbm().equals("1")) {
                veBaseBanJi.setNansrs(Integer.valueOf(veBaseBanJi.getNansrs().intValue() + 1));
            } else {
                veBaseBanJi.setNvsrs(Integer.valueOf(veBaseBanJi.getNvsrs().intValue() + 1));
            }
            this.veBaseBanJiService.updateById(veBaseBanJi);
        }
        if ((!"".equals(voStudentAndStudentInfo.getRxnyName())) && (voStudentAndStudentInfo.getRxnyName() != null))
        {
            Long td = Long.valueOf(DateTimeUtil.dateToTimestamp(voStudentAndStudentInfo.getRxnyName()));
            veBaseStudent.setRxny(td);
        }
        else
        {
            veBaseStudent.setRxny(Long.valueOf(0L));
        }
        if ((!"".equals(voStudentAndStudentInfo.getProvinceId())) && (voStudentAndStudentInfo.getProvinceId() != null))
        {
            veBaseStudent.setProvinceId(voStudentAndStudentInfo.getProvinceId());
            VeBaseDictArea veBaseDictArea = (VeBaseDictArea)this.veBaseDictAreaService.getById(voStudentAndStudentInfo.getProvinceId());
            if (veBaseDictArea != null) {
                veBaseStudent.setProvince(veBaseDictArea.getName());
            }
        }
        if ((!"".equals(voStudentAndStudentInfo.getCityId())) && (voStudentAndStudentInfo.getCityId() != null))
        {
            veBaseStudent.setCityId(voStudentAndStudentInfo.getCityId());
            VeBaseDictArea veBaseDictArea = (VeBaseDictArea)this.veBaseDictAreaService.getById(voStudentAndStudentInfo.getCityId());
            if (veBaseDictArea != null) {
                veBaseStudent.setCity(veBaseDictArea.getName());
            }
        }
        if ((!"".equals(voStudentAndStudentInfo.getCountyId())) && (voStudentAndStudentInfo.getCountyId() != null))
        {
            veBaseStudent.setCountyId(voStudentAndStudentInfo.getCountyId());
            VeBaseDictArea veBaseDictArea = (VeBaseDictArea)this.veBaseDictAreaService.getById(voStudentAndStudentInfo.getCountyId());
            if (veBaseDictArea != null) {
                veBaseStudent.setCounty(veBaseDictArea.getName());
            }
        }
        Long times = Long.valueOf(new Date().getTime() / 1000L);

        veBaseStudent.setUpdateTime(Integer.valueOf(times.intValue()));
        this.veBaseStudentService.updateById(veBaseStudent);

        VeBaseStudentInfo veBaseStudentInfo = new VeBaseStudentInfo();
        veBaseStudentInfo.setStuId(veBaseStudent.getId());
        veBaseStudentInfo.setCym(voStudentAndStudentInfo.getCym());
        if ((!"".equals(voStudentAndStudentInfo.getCsrqName())) && (voStudentAndStudentInfo.getCsrqName() != null))
        {
            Long td = Long.valueOf(DateTimeUtil.dateToTimestamp(voStudentAndStudentInfo.getCsrqName()));
            veBaseStudentInfo.setCsrq(Integer.valueOf(td.intValue()));
        }
        else
        {
            veBaseStudentInfo.setCsrq(Integer.valueOf(0));
        }
        veBaseStudentInfo.setJg(voStudentAndStudentInfo.getJg());
        veBaseStudentInfo.setZzmmm(voStudentAndStudentInfo.getZzmmm());
        veBaseStudentInfo.setZp(voStudentAndStudentInfo.getZp());
        veBaseStudentInfo.setBmfsm(voStudentAndStudentInfo.getBmfsm());
        veBaseStudentInfo.setByxx(voStudentAndStudentInfo.getByxx());
        veBaseStudentInfo.setRxcj(voStudentAndStudentInfo.getRxcj());
        veBaseStudentInfo.setXslxdh(voStudentAndStudentInfo.getXslxdh());
        veBaseStudentInfo.setJtlxdh(voStudentAndStudentInfo.getJtlxdh());
        veBaseStudentInfo.setDzxx(voStudentAndStudentInfo.getDzxx());
        veBaseStudentInfo.setJtdz(voStudentAndStudentInfo.getJtdz());
        veBaseStudentInfo.setJkzkm(voStudentAndStudentInfo.getJkzkm());
        veBaseStudentInfo.setHklbm(voStudentAndStudentInfo.getHklbm());
        veBaseStudentInfo.setTc(voStudentAndStudentInfo.getTc());
        veBaseStudentInfo.setSfsldrk(voStudentAndStudentInfo.getSfsldrk());
        veBaseStudentInfo.setSfdb(voStudentAndStudentInfo.getSfdb());
        veBaseStudentInfo.setUpdateTime(Integer.valueOf(times.intValue()));
        this.veBaseStudentInfoService.updateById(veBaseStudentInfo);

        return Result.ok("修改成功!");
    }

    @AutoLog("编辑教师信息")
    @ApiOperation(value="编辑教师信息", notes="编辑教师信息")
    @PostMapping({"/editTeacher"})
    public Result<?> editTeacher(@RequestBody VeBaseTeacher veBaseTeacher)
    {
        if (("".equals(veBaseTeacher.getGh())) || (veBaseTeacher.getGh() == null)) {
            return Result.error("工号不能为空! ");
        }
        if (("".equals(veBaseTeacher.getXm())) || (veBaseTeacher.getXm() == null)) {
            return Result.error("姓名不能为空! ");
        }
        if (("".equals(veBaseTeacher.getSfzjh())) || (veBaseTeacher.getSfzjh() == null)) {
            return Result.error("身份证号不能为空! ");
        }
        if (!"".equals(IdCardUtil.IdentityCardVerification(veBaseTeacher.getSfzjh()))) {
            return Result.error("身份证号错误! ");
        }
        VeBaseTeacher model = this.veBaseTeacherService.getByGH(veBaseTeacher.getId(), veBaseTeacher.getGh());
        if (model != null) {
            return Result.error("该工号已存在! ");
        }
        if ((!"".equals(veBaseTeacher.getLxdh())) && (veBaseTeacher.getLxdh() != null) && (!PhoneUtil.isMobile(veBaseTeacher.getLxdh()))) {
            return Result.error("联系电话错误! ");
        }
        if ((!"".equals(veBaseTeacher.getDzxx())) && (veBaseTeacher.getDzxx() != null) && (!EmailUtil.isEmail(veBaseTeacher.getDzxx()))) {
            return Result.error("电子信箱错误! ");
        }
        if ((!"".equals(veBaseTeacher.getCsrqName())) && (veBaseTeacher.getCsrqName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseTeacher.getCsrqName()));
            veBaseTeacher.setCsrq(Integer.valueOf(times.intValue()));
        }
        if ((!"".equals(veBaseTeacher.getCjgznyName())) && (veBaseTeacher.getCjgznyName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseTeacher.getCjgznyName()));
            veBaseTeacher.setCjgzny(Integer.valueOf(times.intValue()));
        }
        if ((!"".equals(veBaseTeacher.getCjnyName())) && (veBaseTeacher.getCjnyName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseTeacher.getCjnyName()));
            veBaseTeacher.setCjny(Integer.valueOf(times.intValue()));
        }
        if ((!"".equals(veBaseTeacher.getLxnyName())) && (veBaseTeacher.getLxnyName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseTeacher.getLxnyName()));
            veBaseTeacher.setLxny(Integer.valueOf(times.intValue()));
        }
        if ((!"".equals(veBaseTeacher.getRdName())) && (veBaseTeacher.getRdName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseTeacher.getRdName()));
            veBaseTeacher.setRdDate(Integer.valueOf(times.intValue()));
        }
        veBaseTeacher.setUserId(veBaseTeacher.getGh());
        this.veBaseTeacherService.updateById(veBaseTeacher);

        this.redisUtils.set("queryDepartmentAndTeacherList", this.veBaseDepartmentService.getDepartmentAndTeacherList());
        return Result.ok("修改成功!");
    }

    @AutoLog("校区信息批量删除")
    @ApiOperation(value="校区信息批量删除", notes="校区信息批量删除")
    @PostMapping({"/deleteCampusBatch"})
    public Result<?> deleteCampusBatch(@RequestParam(name="ids", required=true) String ids)
    {
        this.veBaseCampusService.removeByIds(Arrays.asList(ids.split(",")));

        List<VeBaseCampus> list = this.veBaseCampusService.list();
        this.redisUtils.set("queryCampusList", list);
        return Result.ok("删除成功!");
    }

    @AutoLog("院系(专业部)信息批量删除")
    @ApiOperation(value="院系(专业部)信息批量删除", notes="院系(专业部)信息批量删除")
    @PostMapping({"/deleteFacultyBatch"})
    public Result<?> deleteFacultyBatch(@RequestParam(name="ids", required=true) String ids)
    {
        this.veBaseFacultyService.removeByIds(Arrays.asList(ids.split(",")));
        String[] idList = ids.split(",");
        if (idList.length > 0)
        {
            this.veBaseSpecialtyService.deleteSpecialtyByFalId(idList);

            List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
            this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("status", Integer.valueOf(1));
        queryWrapper.orderByAsc("yxdm");

        List<VeBaseFaculty> list = this.veBaseFacultyService.list(queryWrapper);
        this.redisUtils.set("queryFacultyList", list);
        List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        return Result.ok("删除成功!");
    }

    @AutoLog("专业信息批量删除")
    @ApiOperation(value="专业信息批量删除", notes="专业信息批量删除")
    @PostMapping({"/deleteSpecialtyBatch"})
    public Result<?> deleteSpecialtyBatch(@RequestParam(name="ids", required=true) String ids)
    {
        String[] idList = ids.split(",");
        if (idList.length > 0)
        {
            this.veBaseSpecialtyService.deleteSpecialtyBatch(idList);

            List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
            this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        }
        return Result.ok("删除成功!");
    }

    @AutoLog("年级信息批量删除")
    @ApiOperation(value="年级信息批量删除", notes="年级信息批量删除")
    @PostMapping({"/deleteGradeBatch"})
    public Result<?> deleteGradeBatch(@RequestParam(name="ids", required=true) String ids)
    {
        this.veBaseGradeService.removeByIds(Arrays.asList(ids.split(",")));
        QueryWrapper queryWrapper = new QueryWrapper();

        queryWrapper.eq("njzt", Integer.valueOf(1));
        queryWrapper.orderByDesc("rxnf");

        List<VeBaseGrade> list = this.veBaseGradeService.list(queryWrapper);
        this.redisUtils.set("queryGradeList", list);
        return Result.ok("删除成功!");
    }

    @AutoLog("学期信息批量删除")
    @ApiOperation(value="学期信息批量删除", notes="学期信息批量删除")
    @PostMapping({"/deleteSemesterBatch"})
    public Result<?> deleteSemesterBatch(@RequestParam(name="ids", required=true) String ids)
    {
        this.veBaseSemesterService.removeByIds(Arrays.asList(ids.split(",")));
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("xqm");

        List<VeBaseSemester> list = this.veBaseSemesterService.list(queryWrapper);
        this.redisUtils.set("querySemesterList", list);
        return Result.ok("删除成功!");
    }

    @AutoLog("班级信息批量删除")
    @ApiOperation(value="班级信息批量删除", notes="班级信息批量删除")
    @PostMapping({"/deleteBanJiBatch"})
    @Transactional
    public Result<?> deleteBanJiBatch(@RequestParam(name="ids", required=true) String ids)
    {
        this.veBaseBanJiService.removeByIds(Arrays.asList(ids.split(",")));

        List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        return Result.ok("删除成功!");
    }

    @AutoLog("院系(专业部)信息批量启用")
    @ApiOperation(value="院系(专业部)信息批量启用", notes="院系(专业部)信息批量启用")
    @PostMapping({"/startFacultyBatch"})
    public Result<?> startFacultyBatch(@RequestParam(name="ids", required=true) String ids)
    {
        String[] arr = ids.split(",");
        if (arr.length > 0) {
            for (int i = 0; i < arr.length; i++)
            {
                VeBaseFaculty veBaseFaculty = (VeBaseFaculty)this.veBaseFacultyService.getById(arr[i]);
                if (veBaseFaculty != null)
                {
                    veBaseFaculty.setStatus(Integer.valueOf(1));
                    this.veBaseFacultyService.updateById(veBaseFaculty);
                }
            }
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("status", Integer.valueOf(1));
        queryWrapper.orderByAsc("yxdm");

        List<VeBaseFaculty> list = this.veBaseFacultyService.list(queryWrapper);
        this.redisUtils.set("queryFacultyList", list);
        List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        return Result.ok("启用成功!");
    }

    @AutoLog("院系(专业部)信息批量禁用")
    @ApiOperation(value="院系(专业部)信息批量禁用", notes="院系(专业部)信息批量禁用")
    @PostMapping({"/stopFacultyBatch"})
    public Result<?> stopFacultyBatch(@RequestParam(name="ids", required=true) String ids)
    {
        String[] arr = ids.split(",");
        if (arr.length > 0)
        {
            for (int i = 0; i < arr.length; i++)
            {
                VeBaseFaculty veBaseFaculty = (VeBaseFaculty)this.veBaseFacultyService.getById(arr[i]);
                if (veBaseFaculty != null)
                {
                    veBaseFaculty.setStatus(Integer.valueOf(0));
                    this.veBaseFacultyService.updateById(veBaseFaculty);
                }
            }
            this.veBaseSpecialtyService.stopSpecialtyByFalId(arr);

            List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
            this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("status", Integer.valueOf(1));
        queryWrapper.orderByAsc("yxdm");

        List<VeBaseFaculty> list = this.veBaseFacultyService.list(queryWrapper);
        this.redisUtils.set("queryFacultyList", list);
        List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        return Result.ok("禁用成功!");
    }

    @AutoLog("专业批量启用")
    @ApiOperation(value="专业批量启用", notes="专业批量启用")
    @PostMapping({"/startSpecialtyBatch"})
    public Result<?> startSpecialtyBatch(@RequestParam(name="ids", required=true) String ids)
    {
        String[] arr = ids.split(",");
        if (arr.length > 0)
        {
            for (int i = 0; i < arr.length; i++)
            {
                VeBaseSpecialty veBaseSpecialty = (VeBaseSpecialty)this.veBaseSpecialtyService.getById(arr[i]);
                if (veBaseSpecialty != null)
                {
                    veBaseSpecialty.setStatus(Integer.valueOf(1));
                    this.veBaseSpecialtyService.updateById(veBaseSpecialty);
                }
            }
            List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
            this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        }
        return Result.ok("启用成功!");
    }

    @AutoLog("专业批量禁用")
    @ApiOperation(value="专业批量禁用", notes="专业批量禁用")
    @PostMapping({"/stopSpecialtyBatch"})
    public Result<?> stopSpecialtyBatch(@RequestParam(name="ids", required=true) String ids)
    {
        String[] arr = ids.split(",");
        if (arr.length > 0)
        {
            this.veBaseSpecialtyService.stopSpecialtyBatch(arr);

            List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
            this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        }
        return Result.ok("禁用成功!");
    }

    @AutoLog("年级信息批量设为过期")
    @ApiOperation(value="年级信息批量设为过期", notes="年级信息批量设为过期")
    @PostMapping({"/pastGradeBatch"})
    public Result<?> pastGradeBatch(@RequestParam(name="ids", required=true) String ids)
    {
        String[] arr = ids.split(",");
        if (arr.length > 0) {
            for (int i = 0; i < arr.length; i++)
            {
                VeBaseGrade veBaseGrade = (VeBaseGrade)this.veBaseGradeService.getById(arr[i]);
                if (veBaseGrade != null)
                {
                    veBaseGrade.setNjzt(Integer.valueOf(0));
                    this.veBaseGradeService.updateById(veBaseGrade);
                }
            }
        }
        QueryWrapper queryWrapper = new QueryWrapper();

        queryWrapper.eq("njzt", Integer.valueOf(1));
        queryWrapper.orderByDesc("rxnf");

        List<VeBaseGrade> list = this.veBaseGradeService.list(queryWrapper);
        this.redisUtils.set("queryGradeList", list);
        return Result.ok("设置过期成功!");
    }

    @AutoLog("年级信息批量设为未过期")
    @ApiOperation(value="年级信息批量设为未过期", notes="年级信息批量设为未过期")
    @PostMapping({"/notPastGradeBatch"})
    public Result<?> notPastGradeBatch(@RequestParam(name="ids", required=true) String ids)
    {
        String[] arr = ids.split(",");
        if (arr.length > 0) {
            for (int i = 0; i < arr.length; i++)
            {
                VeBaseGrade veBaseGrade = (VeBaseGrade)this.veBaseGradeService.getById(arr[i]);
                if (veBaseGrade != null)
                {
                    veBaseGrade.setNjzt(Integer.valueOf(1));
                    this.veBaseGradeService.updateById(veBaseGrade);
                }
            }
        }
        QueryWrapper queryWrapper = new QueryWrapper();

        queryWrapper.eq("njzt", Integer.valueOf(1));
        queryWrapper.orderByDesc("rxnf");

        List<VeBaseGrade> list = this.veBaseGradeService.list(queryWrapper);
        this.redisUtils.set("queryGradeList", list);
        return Result.ok("设置未过期成功!");
    }

    @AutoLog("学期信息批量设为当前学期")
    @ApiOperation(value="学期信息批量设为当前学期", notes="学期信息批量设为当前学期")
    @PostMapping({"/presentSemesterBatch"})
    public Result<?> presentSemesterBatch(@RequestParam(name="ids", required=true) String ids)
    {
        if (ids.length() == 0) {
            return Result.error("请先勾选数据! ");
        }
        String[] arr = ids.split(",");
        if (arr.length > 1) {
            return Result.error("最多只能勾选一条数据! ");
        }
        VeBaseSemester model = this.veBaseSemesterService.getSemesterByIsCurrent(null);
        if (model != null) {
            return Result.error("当前学期已存在!");
        }
        if (arr.length > 0) {
            for (int i = 0; i < arr.length; i++)
            {
                VeBaseSemester veBaseSemester = (VeBaseSemester)this.veBaseSemesterService.getById(arr[i]);
                if (veBaseSemester != null)
                {
                    veBaseSemester.setIscurrent(Integer.valueOf(0));
                    this.veBaseSemesterService.updateById(veBaseSemester);
                }
            }
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByDesc("xqm");

        List<VeBaseSemester> list = this.veBaseSemesterService.list(queryWrapper);
        this.redisUtils.set("querySemesterList", list);
        return Result.ok("设置成功!");
    }

    @AutoLog("专业信息启用")
    @ApiOperation(value="专业信息启用", notes="专业信息启用")
    @PostMapping({"/startSpecialty"})
    @Transactional
    public Result<?> startSpecialty(@RequestParam(name="ids", required=true) String id)
    {
        VeBaseSpecialty veBaseSpecialty = (VeBaseSpecialty)this.veBaseSpecialtyService.getById(id);
        if (veBaseSpecialty != null)
        {
            veBaseSpecialty.setStatus(Integer.valueOf(1));
            this.veBaseSpecialtyService.updateById(veBaseSpecialty);
        }
        List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        return Result.ok("启用成功!");
    }

    @AutoLog("部门信息-单个删除")
    @ApiOperation(value="部门信息-单个删除", notes="部门信息-单个删除")
    @PostMapping({"/deleteDepartment"})
    public Result<?> deleteDepartment(@RequestParam(name="id", required=true) String id)
    {
        this.veBaseDepartmentService.removeById(id);

        List<VeBaseDepartment> list = this.veBaseDepartmentService.getTreeList();
        this.redisUtils.set("queryDepartmentList", list);

        List<VeBaseDepartment> listTwo = this.veBaseDepartmentService.getDepartmentAndTeacherList();
        this.redisUtils.set("queryDepartmentAndTeacherList", listTwo);

        List<Map<String, Object>> listTree = this.veBaseAppUserService.queryOrganUserTreeList(null);
        this.redisUtils.set("queryOrganUserTreeList", listTree);
        return Result.ok("删除成功!");
    }

    @AutoLog("教研组(专业组)信息-单个删除")
    @ApiOperation(value="教研组(专业组)信息-单个删除", notes="教研组(专业组)信息-单个删除")
    @PostMapping({"/deleteJYZ"})
    @Transactional
    public Result<?> deleteJYZ(@RequestParam(name="id", required=true) String id)
    {
        this.veBaseJYZService.removeById(id);

        List<Map<String, Object>> list = this.veBaseJYZService.getJYZTreeList();
        this.redisUtils.set("queryJYZTreeList", list);
        return Result.ok("删除成功!");
    }

    @AutoLog("院系(专业部)信息-单个删除")
    @ApiOperation(value="院系(专业部)信息-单个删除", notes="院系(专业部)信息-单个删除")
    @PostMapping({"/deleteFaculty"})
    public Result<?> deleteFaculty(@RequestParam(name="id", required=true) String id)
    {
        this.veBaseFacultyService.removeById(id);
        String[] idList = id.split(",");
        if (idList.length > 0)
        {
            this.veBaseSpecialtyService.deleteSpecialtyByFalId(idList);

            List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
            this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("status", Integer.valueOf(1));
        queryWrapper.orderByAsc("yxdm");

        List<VeBaseFaculty> list = this.veBaseFacultyService.list(queryWrapper);
        this.redisUtils.set("queryFacultyList", list);
        List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
        return Result.ok("删除成功!");
    }

    @AutoLog("学制信息-单个删除")
    @ApiOperation(value="学制信息-单个删除", notes="学制信息-单个删除")
    @PostMapping({"/deleteXueZhi"})
    public Result<?> deleteXueZhi(@RequestParam(name="id", required=true) String id)
    {
        this.veBaseXueZhiService.removeById(id);

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByAsc("years");
        List<VeBaseXueZhi> list = this.veBaseXueZhiService.list(queryWrapper);
        this.redisUtils.set("queryXueZhiList", list);
        return Result.ok("删除成功!");
    }

    @AutoLog("教师信息批量删除")
    @ApiOperation(value="教师信息批量删除", notes="教师信息批量删除")
    @PostMapping({"/deleteTeacherBatch"})
    @Transactional
    public Result<?> deleteTeacherBatch(@RequestParam(name="ids", required=true) String ids)
    {
        String[] strings = ids.split(",");

        List<VeBaseTeacher> list = new ArrayList();

        List<VeBaseAppUser> detailList = new ArrayList();
        for (String s : strings)
        {
            VeBaseTeacher veBaseTeacher = (VeBaseTeacher)this.veBaseTeacherService.getById(s);
            veBaseTeacher.setIsDeleted(Integer.valueOf(-1));
            veBaseTeacher.setStatus(Integer.valueOf(2));
            list.add(veBaseTeacher);
            VeBaseAppUser veBaseAppUser = this.veBaseAppUserService.getAppUserByUserId(veBaseTeacher.getUserId());
            if (veBaseAppUser != null)
            {
                veBaseAppUser.setStatus("1");
                veBaseAppUser.setIsDel("1");
                detailList.add(veBaseAppUser);
            }
        }
        this.veBaseTeacherService.updateBatchById(list);
        if (detailList.size() > 0) {
            this.veBaseAppUserService.updateBatchById(detailList);
        }
        this.redisUtils.set("queryDepartmentAndTeacherList", this.veBaseDepartmentService.getDepartmentAndTeacherList());

        return Result.ok("删除成功!");
    }

    @AutoLog("学生信息批量删除")
    @ApiOperation(value="学生信息批量删除", notes="学生信息批量删除")
    @PostMapping({"/deleteStudentBatch"})
    public Result<?> deleteStudentBatch(@RequestParam(name="ids", required=true) String ids)
    {
        this.veBaseStudentService.removeByIds(Arrays.asList(ids.split(",")));
        this.veBaseStudentInfoService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("删除成功!");
    }

    @AutoLog("节假日信息批量删除")
    @ApiOperation(value="节假日信息批量删除", notes="节假日信息批量删除")
    @PostMapping({"/deleteFestivalBatch"})
    public Result<?> deleteFestivalBatch(@RequestParam(name="ids", required=true) String ids)
    {
        this.veBaseFestivalService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("删除成功!");
    }

    @AutoLog("根据学期年份生成校历信息")
    @ApiOperation(value="根据学期年份生成校历信息", notes="根据学期年份生成校历信息")
    @PostMapping({"/addCalendar"})
    public Result<?> addCalendar(@RequestParam(name="semId", required=true) Integer semId, @RequestParam(name="times", required=true) String times)
            throws ParseException
    {
        List<VeBaseCalendar> list = new ArrayList();

        Integer year = Integer.valueOf(Integer.parseInt(times));

        List<VeBaseCalendar> modelList = this.veBaseCalendarService.getCalendarListBySemId(semId, year, Integer.valueOf(1));
        if (modelList.size() > 0) {
            return Result.error("当前校历已存在! ");
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdf = new SimpleDateFormat("E");
        for (int i = 1; i < 13; i++)
        {
            int num = 0;
            if (i == 2) {
                num = 28;
            } else if ((i == 4) || (i == 6) || (i == 9) || (i == 11)) {
                num = 30;
            } else {
                num = 31;
            }
            for (int j = 1; j <= num; j++)
            {
                VeBaseCalendar veBaseCalendar = new VeBaseCalendar();
                veBaseCalendar.setSemId(semId);
                veBaseCalendar.setYear(year);
                veBaseCalendar.setMonth(Integer.valueOf(i));
                String t = times + "-" + i + "-" + j;
                Date dtDay = simpleDateFormat.parse(t);

                int whatDay = 0;
                String week = sdf.format(dtDay);
                switch (week)
                {
                    case "星期一":
                        whatDay = 1;
                        break;
                    case "星期二":
                        whatDay = 2;
                        break;
                    case "星期三":
                        whatDay = 3;
                        break;
                    case "星期四":
                        whatDay = 4;
                        break;
                    case "星期五":
                        whatDay = 5;
                        break;
                    case "星期六":
                        whatDay = 6;
                        break;
                    case "星期日":
                        whatDay = 7;
                        break;
                    default:
                        whatDay = 0;
                }
                veBaseCalendar.setDates(t);
                veBaseCalendar.setDayOfWeek(Integer.valueOf(whatDay));

                list.add(veBaseCalendar);
            }
        }
        this.veBaseCalendarService.saveBatch(list);
        return Result.ok("生成成功! ");
    }

    @AutoLog("根据学期id获取校历信息")
    @ApiOperation(value="根据学期id获取校历信息", notes="根据学期id获取校历信息")
    @GetMapping({"/queryCalendarListBySemId"})
    public Result<?> queryCalendarListBySemId(@RequestParam(name="semId", required=true) Integer semId, @RequestParam(name="times", required=true) String times)
            throws ParseException
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM");
        Date dt = simpleDateFormat.parse(times);

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dt);

        int year = calendar.get(1);

        int month = calendar.get(2) + 1;

        List<VeBaseCalendar> list = this.veBaseCalendarService.getCalendarListBySemId(semId, Integer.valueOf(year), Integer.valueOf(month));
        return Result.OK(list);
    }

    @ApiOperation(value="excel导入-部门数据", notes="excel导入-部门数据")
    @RequestMapping(value={"/importExcelDepartment"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
    public Result<?> importExcelDepartment(HttpServletRequest request, HttpServletResponse response)
    {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        Iterator localIterator = fileMap.entrySet().iterator();
        if (localIterator.hasNext())
        {
            Map.Entry<String, MultipartFile> entity = (Map.Entry)localIterator.next();
            MultipartFile file = (MultipartFile)entity.getValue();
            ImportParams params = new ImportParams();
            params.setTitleRows(1);
            params.setHeadRows(1);
            params.setNeedSave(true);
            try
            {
                int i = 0;

                StringBuilder sb = new StringBuilder();
                List<VeBaseDepartment> veBaseDepartmentList = ExcelImportUtil.importExcel(file.getInputStream(), VeBaseDepartment.class, params);
                Object localObject1;
                if (veBaseDepartmentList.size() > 0) {
                    for (localObject1 = veBaseDepartmentList.iterator(); ((Iterator)localObject1).hasNext();)
                    {
                        VeBaseDepartment veBaseDepartment = (VeBaseDepartment)((Iterator)localObject1).next();
                        if ((!"".equals(veBaseDepartment.getJgh())) && (veBaseDepartment.getJgh() != null) && (!"".equals(veBaseDepartment.getJgmc())) && (veBaseDepartment.getJgmc() != null) && (!"".equals(veBaseDepartment.getFzrUserId())) && (veBaseDepartment.getFzrUserId() != null))
                        {
                            VeBaseDepartment v1 = this.veBaseDepartmentService.getDepartmentByCode(null, veBaseDepartment.getJgh());

                            VeBaseDepartment v2 = this.veBaseDepartmentService.getDepartmentByName(null, veBaseDepartment.getJgmc());
                            if ((v1 == null) && (v2 == null))
                            {
                                if ((!"".equals(veBaseDepartment.getDssjjgh())) && (veBaseDepartment.getDssjjgh() != null))
                                {
                                    VeBaseDepartment model = this.veBaseDepartmentService.getDepartmentByName(null, veBaseDepartment.getDssjjgh());
                                    if (model != null)
                                    {
                                        veBaseDepartment.setDssjjgh(model.getDssjjgh());
                                        veBaseDepartment.setPid(model.getId());
                                    }
                                    else
                                    {
                                        veBaseDepartment.setDssjjgh("");
                                    }
                                }
                                if ((!"".equals(veBaseDepartment.getLsxqh())) && (veBaseDepartment.getLsxqh() != null))
                                {
                                    VeBaseCampus model = this.veBaseCampusService.getCampusByName(null, veBaseDepartment.getLsxqh());
                                    if (model != null)
                                    {
                                        veBaseDepartment.setLsxqh(model.getXqdm());
                                        veBaseDepartment.setCampusId(model.getId());
                                    }
                                    else
                                    {
                                        veBaseDepartment.setLsxqh("");
                                    }
                                }
                                if ((!"".equals(veBaseDepartment.getJlnyName())) && (veBaseDepartment.getJlnyName() != null))
                                {
                                    Date date = HSSFDateUtil.getJavaDate(Double.parseDouble(veBaseDepartment.getJlnyName()));
                                    Long l = Long.valueOf(date.getTime() / 1000L);
                                    veBaseDepartment.setJlny(Integer.valueOf(l.intValue()));
                                }
                                this.veBaseDepartmentService.save(veBaseDepartment);
                                i++;
                            }
                            else
                            {
                                sb.append(" 部门号:" + veBaseDepartment.getJgh() + "或者部门名称:" + veBaseDepartment.getJgmc() + "已存在;");
                            }
                        }
                    }
                }
                VeBaseDepartment v2;
                if (i == 0)
                {
                    if (sb.length() > 0) {
                        sb.replace(sb.length() - 1, sb.length(), "!");
                    }
                    return Result.error("文件导入失败, 请检查导入数据是否正确! " + sb);
                }
                Object list = this.veBaseDepartmentService.getTreeList();
                this.redisUtils.set("queryDepartmentList", list);

                List<VeBaseDepartment> listTwo = this.veBaseDepartmentService.getDepartmentAndTeacherList();
                this.redisUtils.set("queryDepartmentAndTeacherList", listTwo);

                List<Map<String, Object>> listTree = this.veBaseAppUserService.queryOrganUserTreeList(null);
                this.redisUtils.set("queryOrganUserTreeList", listTree);
                return Result.ok("文件导入成功！数据行数：" + i);
            }
            catch (Exception e)
            {
                StringBuilder sb;
                log.error(e.getMessage(), e);
                return Result.error("文件导入失败:" + e.getMessage());
            }
            finally
            {
                try
                {
                    file.getInputStream().close();
                }
                catch (IOException e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return Result.error("文件导入失败！");
    }

    @ApiOperation(value="excel导入-教研组数据", notes="excel导入-教研组数据")
    @RequestMapping(value={"/importExcelJYZ"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
    public Result<?> importExcelJYZ(HttpServletRequest request, HttpServletResponse response)
    {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        Iterator localIterator = fileMap.entrySet().iterator();
        if (localIterator.hasNext())
        {
            Map.Entry<String, MultipartFile> entity = (Map.Entry)localIterator.next();
            MultipartFile file = (MultipartFile)entity.getValue();
            ImportParams params = new ImportParams();
            params.setTitleRows(1);
            params.setHeadRows(1);
            params.setNeedSave(true);
            try
            {
                int i = 0;

                StringBuilder sb = new StringBuilder();
                List<VeBaseJYZ> veBaseJYZList = ExcelImportUtil.importExcel(file.getInputStream(), VeBaseJYZ.class, params);
                Object localObject1;
                if (veBaseJYZList.size() > 0) {
                    for (localObject1 = veBaseJYZList.iterator(); ((Iterator)localObject1).hasNext();)
                    {
                        VeBaseJYZ veBaseJYZ = (VeBaseJYZ)((Iterator)localObject1).next();
                        if ((!"".equals(veBaseJYZ.getJyzbh())) && (veBaseJYZ.getJyzbh() != null) && (!"".equals(veBaseJYZ.getJyzmc())) && (veBaseJYZ.getJyzmc() != null))
                        {
                            VeBaseJYZ v1 = this.veBaseJYZService.getJYZByCode(null, veBaseJYZ.getJyzbh());

                            VeBaseJYZ v2 = this.veBaseJYZService.getJYZByName(null, veBaseJYZ.getJyzmc());
                            if ((v1 == null) && (v2 == null))
                            {
                                if ((!"".equals(veBaseJYZ.getPidName())) && (veBaseJYZ.getPidName() != null))
                                {
                                    VeBaseJYZ model = this.veBaseJYZService.getJYZByName(null, veBaseJYZ.getPidName());
                                    if (model != null) {
                                        veBaseJYZ.setPid(model.getId());
                                    } else {
                                        veBaseJYZ.setPid(Integer.valueOf(0));
                                    }
                                }
                                if ((!"".equals(veBaseJYZ.getCampusName())) && (veBaseJYZ.getCampusName() != null))
                                {
                                    VeBaseCampus model = this.veBaseCampusService.getCampusByName(null, veBaseJYZ.getCampusName());
                                    if (model != null) {
                                        veBaseJYZ.setCampusId(model.getId());
                                    } else {
                                        veBaseJYZ.setCampusId(null);
                                    }
                                }
                                if ((!"".equals(veBaseJYZ.getJlnyName())) && (veBaseJYZ.getJlnyName() != null))
                                {
                                    Date date = HSSFDateUtil.getJavaDate(Double.parseDouble(veBaseJYZ.getJlnyName()));
                                    Long l = Long.valueOf(date.getTime() / 1000L);
                                    veBaseJYZ.setJlny(Integer.valueOf(l.intValue()));
                                }
                                this.veBaseJYZService.save(veBaseJYZ);
                                i++;
                            }
                            else
                            {
                                sb.append(" 教研组编号:" + veBaseJYZ.getJyzbh() + "或者教研组名称:" + veBaseJYZ.getJyzmc() + "已存在;");
                            }
                        }
                    }
                }
                if (i == 0)
                {
                    if (sb.length() > 0) {
                        sb.replace(sb.length() - 1, sb.length(), "!");
                    }
                    return Result.error("文件导入失败, 请检查导入数据是否正确! " + sb);
                }
                Object list = this.veBaseJYZService.getJYZTreeList();
                this.redisUtils.set("queryJYZTreeList", list);
                return Result.ok("文件导入成功！数据行数：" + i);
            }
            catch (Exception e)
            {
                StringBuilder sb;
                log.error(e.getMessage(), e);
                return Result.error("文件导入失败:" + e.getMessage());
            }
            finally
            {
                try
                {
                    file.getInputStream().close();
                }
                catch (IOException e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return Result.error("文件导入失败！");
    }

    @ApiOperation(value="excel导入-专业部(院系)数据", notes="excel导入-专业部(院系)数据")
    @RequestMapping(value={"/importExcelFaculty"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
    public Result<?> importExcelFaculty(HttpServletRequest request, HttpServletResponse response)
    {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        Iterator localIterator = fileMap.entrySet().iterator();
        if (localIterator.hasNext())
        {
            Map.Entry<String, MultipartFile> entity = (Map.Entry)localIterator.next();
            MultipartFile file = (MultipartFile)entity.getValue();
            ImportParams params = new ImportParams();
            params.setTitleRows(1);
            params.setHeadRows(1);
            params.setNeedSave(true);
            try
            {
                int i = 0;

                StringBuilder sb = new StringBuilder();
                List<VeBaseFaculty> veBaseFacultyList = ExcelImportUtil.importExcel(file.getInputStream(), VeBaseFaculty.class, params);
                Object localObject1;
                if (veBaseFacultyList.size() > 0) {
                    for (localObject1 = veBaseFacultyList.iterator(); ((Iterator)localObject1).hasNext();)
                    {
                        VeBaseFaculty veBaseFaculty = (VeBaseFaculty)((Iterator)localObject1).next();
                        if ((!"".equals(veBaseFaculty.getYxdm())) && (veBaseFaculty.getYxdm() != null) && (!"".equals(veBaseFaculty.getYxmc())) && (veBaseFaculty.getYxmc() != null))
                        {
                            VeBaseFaculty v1 = this.veBaseFacultyService.getFacultyByCode(null, veBaseFaculty.getYxdm());

                            VeBaseFaculty v2 = this.veBaseFacultyService.getFacultyByName(null, veBaseFaculty.getYxmc());
                            if ((v1 == null) && (v2 == null))
                            {
                                this.veBaseFacultyService.save(veBaseFaculty);
                                i++;
                            }
                            else
                            {
                                sb.append(" 院系代码:" + veBaseFaculty.getYxdm() + "或者院系名称:" + veBaseFaculty.getYxmc() + "已存在;");
                            }
                        }
                    }
                }
                VeBaseFaculty v2;
                if (i == 0)
                {
                    if (sb.length() > 0) {
                        sb.replace(sb.length() - 1, sb.length(), "!");
                    }
                    return Result.error("文件导入失败, 请检查导入数据是否正确! " + sb);
                }
                QueryWrapper queryWrapper = new QueryWrapper();
                queryWrapper.eq("status", Integer.valueOf(1));
                queryWrapper.orderByAsc("yxdm");

                List<VeBaseFaculty> list = this.veBaseFacultyService.list(queryWrapper);
                this.redisUtils.set("queryFacultyList", list);
                List<Map<String, Object>> mapList = this.veBaseFacultyService.getTreeList();
                this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
                return Result.ok("文件导入成功！数据行数：" + i);
            }
            catch (Exception e)
            {
                StringBuilder sb;
                log.error(e.getMessage(), e);
                return Result.error("文件导入失败:" + e.getMessage());
            }
            finally
            {
                try
                {
                    file.getInputStream().close();
                }
                catch (IOException e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return Result.error("文件导入失败！");
    }

    @ApiOperation(value="excel导入-专业数据", notes="excel导入-专业数据")
    @RequestMapping(value={"/importExcelSpecialty"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
    public Result<?> importExcelSpecialty(HttpServletRequest request, HttpServletResponse response)
    {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        Iterator localIterator = fileMap.entrySet().iterator();
        if (localIterator.hasNext())
        {
            Map.Entry<String, MultipartFile> entity = (Map.Entry)localIterator.next();
            MultipartFile file = (MultipartFile)entity.getValue();
            ImportParams params = new ImportParams();
            params.setTitleRows(1);
            params.setHeadRows(1);
            params.setNeedSave(true);
            try
            {
                int i = 0;

                StringBuilder sb = new StringBuilder();
                List<VeBaseSpecialty> veBaseSpecialtyList = ExcelImportUtil.importExcel(file.getInputStream(), VeBaseSpecialty.class, params);
                Object localObject1;
                if (veBaseSpecialtyList.size() > 0) {
                    for (localObject1 = veBaseSpecialtyList.iterator(); ((Iterator)localObject1).hasNext();)
                    {
                        VeBaseSpecialty veBaseSpecialty = (VeBaseSpecialty)((Iterator)localObject1).next();
                        if ((!"".equals(veBaseSpecialty.getFacultyName())) && (veBaseSpecialty.getFacultyName() != null) && (!"".equals(veBaseSpecialty.getZybh())) && (veBaseSpecialty.getZybh() != null) && (!"".equals(veBaseSpecialty.getZymc())) && (veBaseSpecialty.getZymc() != null) && (!"".equals(veBaseSpecialty.getXuezhiName())) && (veBaseSpecialty.getXuezhiName() != null))
                        {
                            VeBaseSpecialty v1 = this.veBaseSpecialtyService.getSpecialtyByBH(null, veBaseSpecialty.getZybh());

                            VeBaseSpecialty v2 = this.veBaseSpecialtyService.getSpecialtyByName(null, veBaseSpecialty.getZymc());
                            if ((v1 == null) && (v2 == null))
                            {
                                VeBaseFaculty faculty = this.veBaseFacultyService.getFacultyByName(null, veBaseSpecialty.getFacultyName());
                                if (faculty != null)
                                {
                                    veBaseSpecialty.setFalid(faculty.getId());
                                    veBaseSpecialty.setYxdm(faculty.getYxdm());
                                }
                                else
                                {
                                    veBaseSpecialty.setFalid(null);
                                    veBaseSpecialty.setYxdm("");
                                }
                                if ((!"".equals(veBaseSpecialty.getPidName())) && (veBaseSpecialty.getPidName() != null))
                                {
                                    VeBaseSpecialty specialty = this.veBaseSpecialtyService.getSpecialtyByName(null, veBaseSpecialty.getPidName());
                                    if (specialty != null) {
                                        veBaseSpecialty.setPid(specialty.getId());
                                    } else {
                                        veBaseSpecialty.setPid(Integer.valueOf(0));
                                    }
                                }
                                VeBaseXueZhi xueZhi = this.veBaseXueZhiService.getXueZhiByName(null, veBaseSpecialty.getXuezhiName());
                                if (xueZhi != null) {
                                    veBaseSpecialty.setXz(xueZhi.getId().toString());
                                } else {
                                    veBaseSpecialty.setXz(null);
                                }
                                if ((!"".equals(veBaseSpecialty.getJlnyName())) && (veBaseSpecialty.getJlnyName() != null))
                                {
                                    Date date = HSSFDateUtil.getJavaDate(Double.parseDouble(veBaseSpecialty.getJlnyName()));
                                    Long l = Long.valueOf(date.getTime() / 1000L);
                                    veBaseSpecialty.setJlny(Integer.valueOf(l.intValue()));
                                }
                                this.veBaseSpecialtyService.save(veBaseSpecialty);
                                i++;
                            }
                            else
                            {
                                sb.append(" 专业编号:" + veBaseSpecialty.getZybh() + "或者专业名称:" + veBaseSpecialty.getZymc() + "已存在;");
                            }
                        }
                    }
                }
                if (i == 0)
                {
                    if (sb.length() > 0) {
                        sb.replace(sb.length() - 1, sb.length(), "!");
                    }
                    return Result.error("文件导入失败, 请检查导入数据是否正确! " + sb);
                }
                Object mapList = this.veBaseFacultyService.getTreeList();
                this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
                return Result.ok("文件导入成功！数据行数：" + i);
            }
            catch (Exception e)
            {
                StringBuilder sb;
                log.error(e.getMessage(), e);
                return Result.error("文件导入失败:" + e.getMessage());
            }
            finally
            {
                try
                {
                    file.getInputStream().close();
                }
                catch (IOException e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return Result.error("文件导入失败！");
    }

    @ApiOperation(value="excel导入-班级数据", notes="excel导入-班级数据")
    @RequestMapping(value={"/importExcelBanJi"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
    public Result<?> importExcelBanJi(HttpServletRequest request, HttpServletResponse response)
    {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        Iterator localIterator = fileMap.entrySet().iterator();
        if (localIterator.hasNext())
        {
            Map.Entry<String, MultipartFile> entity = (Map.Entry)localIterator.next();
            MultipartFile file = (MultipartFile)entity.getValue();
            ImportParams params = new ImportParams();
            params.setTitleRows(1);
            params.setHeadRows(1);
            params.setNeedSave(true);
            try
            {
                int i = 0;

                StringBuilder sb = new StringBuilder();
                List<VeBaseBanJi> veBaseBanJiList = ExcelImportUtil.importExcel(file.getInputStream(), VeBaseBanJi.class, params);
                Object localObject1;
                if (veBaseBanJiList.size() > 0) {
                    for (localObject1 = veBaseBanJiList.iterator(); ((Iterator)localObject1).hasNext();)
                    {
                        VeBaseBanJi veBaseBanJi = (VeBaseBanJi)((Iterator)localObject1).next();
                        if ((!"".equals(veBaseBanJi.getXzbdm())) && (veBaseBanJi.getXzbdm() != null) && (!"".equals(veBaseBanJi.getXzbmc())) && (veBaseBanJi.getXzbmc() != null) && (!"".equals(veBaseBanJi.getNjdm())) && (veBaseBanJi.getNjdm() != null) && (!"".equals(veBaseBanJi.getSpecName())) && (veBaseBanJi.getSpecName() != null) && (!"".equals(veBaseBanJi.getBzrUserId())) && (veBaseBanJi.getBzrUserId() != null) && (!"".equals(veBaseBanJi.getCampusName())) && (veBaseBanJi.getCampusName() != null) && (!"".equals(veBaseBanJi.getJxlName())) && (veBaseBanJi.getJxlName() != null) && (!"".equals(veBaseBanJi.getRoomName())) && (veBaseBanJi.getRoomName() != null))
                        {
                            VeBaseBanJi v1 = this.veBaseBanJiService.getBanJiByCode(null, veBaseBanJi.getXzbdm());

                            VeBaseBanJi v2 = this.veBaseBanJiService.getBanJiByName(null, veBaseBanJi.getXzbmc());
                            if ((v1 == null) && (v2 == null))
                            {
                                String sname = veBaseBanJi.getNjdm() + "级";
                                veBaseBanJi.setNjmc(sname);
                                VeBaseGrade grade = this.veBaseGradeService.getGradeByName(null, sname);
                                if (grade != null)
                                {
                                    veBaseBanJi.setGradeId(grade.getId());
                                    veBaseBanJi.setNjdm(grade.getNjdm());
                                    veBaseBanJi.setRxnf(grade.getRxnf().toString());
                                }
                                else
                                {
                                    veBaseBanJi.setGradeId(Integer.valueOf(0));
                                    veBaseBanJi.setNjdm("");
                                    veBaseBanJi.setRxnf("");
                                }
                                VeBaseSpecialty specialty = this.veBaseSpecialtyService.getSpecialtyByName(null, veBaseBanJi.getSpecName());
                                if (specialty != null)
                                {
                                    veBaseBanJi.setSpecId(specialty.getId());
                                    veBaseBanJi.setZydm(specialty.getZydm());
                                }
                                else
                                {
                                    veBaseBanJi.setSpecId(Integer.valueOf(0));
                                    veBaseBanJi.setZydm("");
                                }
                                VeBaseCampus campus = this.veBaseCampusService.getCampusByName(null, veBaseBanJi.getCampusName());
                                if (campus != null) {
                                    veBaseBanJi.setCampusId(campus.getId());
                                }
                                VeBaseBuild build = this.veBaseBuildService.getBuildByName(null, veBaseBanJi.getJxlName());
                                if (build != null) {
                                    veBaseBanJi.setJzid(build.getBuildId());
                                }
                                VeBaseBuildRoom room = this.veBaseBuildRoomService.getBuildRoomByName(null, veBaseBanJi.getRoomName());
                                if (room != null) {
                                    veBaseBanJi.setJsid(room.getId());
                                }
                                if ((!"".equals(veBaseBanJi.getBysjName())) && (veBaseBanJi.getBysjName() != null))
                                {
                                    Date date = HSSFDateUtil.getJavaDate(Double.parseDouble(veBaseBanJi.getBysjName()));
                                    Long l = Long.valueOf(date.getTime() / 1000L);
                                    veBaseBanJi.setBysj(Integer.valueOf(l.intValue()));
                                }
                                this.veBaseBanJiService.save(veBaseBanJi);
                                i++;
                            }
                            else
                            {
                                sb.append(" 行政班代码:" + veBaseBanJi.getXzbdm() + "或者行政班名称:" + veBaseBanJi.getXzbmc() + "已存在;");
                            }
                        }
                    }
                }
                if (i == 0)
                {
                    if (sb.length() > 0) {
                        sb.replace(sb.length() - 1, sb.length(), "!");
                    }
                    return Result.error("文件导入失败, 请检查导入数据是否正确! " + sb);
                }
                Object mapList = this.veBaseFacultyService.getTreeList();
                this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", mapList);
                return Result.ok("文件导入成功！数据行数：" + i);
            }
            catch (Exception e)
            {
                StringBuilder sb;
                log.error(e.getMessage(), e);
                return Result.error("文件导入失败:" + e.getMessage());
            }
            finally
            {
                try
                {
                    file.getInputStream().close();
                }
                catch (IOException e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return Result.error("文件导入失败！");
    }

    @RequestMapping({"/exportXlsFaculty"})
    public ModelAndView exportXlsFaculty(VeBaseFaculty veBaseFaculty, HttpServletRequest request)
    {
        List<Map<String, Object>> list = this.veBaseFacultyService.getFacultyPageList(veBaseFaculty);
        if (list.size() > 0) {
            for (Map map : list)
            {
                if ((!"".equals(map.get("createTime").toString())) && (map.get("createTime").toString() != null)) {
                    map.put("createTimeName", map.get("createTime").toString());
                } else {
                    map.put("createTimeName", "");
                }
                if ((!"".equals(map.get("updateTime").toString())) && (map.get("updateTime").toString() != null)) {
                    map.put("updateTimeName", map.get("updateTime").toString());
                } else {
                    map.put("updateTimeName", "");
                }
                if (Integer.parseInt(map.get("status").toString()) == 1) {
                    map.put("status", "可用");
                } else {
                    map.put("status", "不可用");
                }
            }
        }
        ModelAndView modelAndView = new ModelAndView(new JeecgEntityExcelView());

        modelAndView.addObject("fileName", "专业部(院系)信息");
        modelAndView.addObject("entity", VeBaseFaculty.class);
        LoginUser user = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        modelAndView.addObject("params", new ExportParams("专业部(院系)信息列表", "导出人:" + user.getRealname(), "导出信息"));
        modelAndView.addObject("data", list);
        return modelAndView;
    }
}

