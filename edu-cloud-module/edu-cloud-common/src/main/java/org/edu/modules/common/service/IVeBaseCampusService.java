package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseCampus;

public abstract interface IVeBaseCampusService
        extends IService<VeBaseCampus>
{
    public abstract List<Map<String, Object>> getCampusPageList(VeBaseCampus paramVeBaseCampus);

    public abstract VeBaseCampus getCampusByName(Integer paramInteger, String paramString);

    public abstract VeBaseCampus getCampusByCode(Integer paramInteger, String paramString);
}
