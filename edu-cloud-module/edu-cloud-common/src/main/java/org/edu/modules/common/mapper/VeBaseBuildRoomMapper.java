package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseBuildRoom;

public abstract interface VeBaseBuildRoomMapper
        extends BaseMapper<VeBaseBuildRoom>
{
    public abstract List<Map<String, Object>> getBuildRoomPageList(VeBaseBuildRoom paramVeBaseBuildRoom);

    public abstract VeBaseBuildRoom getBuildRoomByName(Integer paramInteger, String paramString);

    public abstract List<VeBaseBuildRoom> getRoomListByBuildId(Integer paramInteger);
}
