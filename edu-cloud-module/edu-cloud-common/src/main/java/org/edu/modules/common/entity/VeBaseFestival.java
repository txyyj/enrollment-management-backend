package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_festival")
@ApiModel(value="ve_base_festival对象", description="校历信息表")
public class VeBaseFestival
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="学期Id", width=15.0D)
    @ApiModelProperty("学期Id")
    private Integer semId;
    @Excel(name="节假日名称", width=15.0D)
    @ApiModelProperty("节假日名称")
    private String title;
    @Excel(name="节假日开始时间2015-10-01", width=15.0D)
    @ApiModelProperty("节假日开始时间2015-10-01")
    private String beginDate;
    @Excel(name="节假日结束时间", width=15.0D)
    @ApiModelProperty("节假日结束时间")
    private String endDate;
    @Excel(name="节假日描述", width=15.0D)
    @ApiModelProperty("节假日描述")
    private String description;
    @Excel(name="终端id", width=15.0D)
    @ApiModelProperty("终端id")
    private Integer terminalId;

    public VeBaseFestival setTitle(String title)
    {
        this.title = title;return this;
    }

    public VeBaseFestival setSemId(Integer semId)
    {
        this.semId = semId;return this;
    }

    public VeBaseFestival setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseFestival(id=" + getId() + ", semId=" + getSemId() + ", title=" + getTitle() + ", beginDate=" + getBeginDate() + ", endDate=" + getEndDate() + ", description=" + getDescription() + ", terminalId=" + getTerminalId() + ")";
    }

    public VeBaseFestival setTerminalId(Integer terminalId)
    {
        this.terminalId = terminalId;return this;
    }

    public VeBaseFestival setDescription(String description)
    {
        this.description = description;return this;
    }

    public VeBaseFestival setEndDate(String endDate)
    {
        this.endDate = endDate;return this;
    }

    public VeBaseFestival setBeginDate(String beginDate)
    {
        this.beginDate = beginDate;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $semId = getSemId();result = result * 59 + ($semId == null ? 43 : $semId.hashCode());Object $terminalId = getTerminalId();result = result * 59 + ($terminalId == null ? 43 : $terminalId.hashCode());Object $title = getTitle();result = result * 59 + ($title == null ? 43 : $title.hashCode());Object $beginDate = getBeginDate();result = result * 59 + ($beginDate == null ? 43 : $beginDate.hashCode());Object $endDate = getEndDate();result = result * 59 + ($endDate == null ? 43 : $endDate.hashCode());Object $description = getDescription();result = result * 59 + ($description == null ? 43 : $description.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseFestival;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseFestival)) {
            return false;
        }
        VeBaseFestival other = (VeBaseFestival)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$semId = getSemId();Object other$semId = other.getSemId();
        if (this$semId == null ? other$semId != null : !this$semId.equals(other$semId)) {
            return false;
        }
        Object this$terminalId = getTerminalId();Object other$terminalId = other.getTerminalId();
        if (this$terminalId == null ? other$terminalId != null : !this$terminalId.equals(other$terminalId)) {
            return false;
        }
        Object this$title = getTitle();Object other$title = other.getTitle();
        if (this$title == null ? other$title != null : !this$title.equals(other$title)) {
            return false;
        }
        Object this$beginDate = getBeginDate();Object other$beginDate = other.getBeginDate();
        if (this$beginDate == null ? other$beginDate != null : !this$beginDate.equals(other$beginDate)) {
            return false;
        }
        Object this$endDate = getEndDate();Object other$endDate = other.getEndDate();
        if (this$endDate == null ? other$endDate != null : !this$endDate.equals(other$endDate)) {
            return false;
        }
        Object this$description = getDescription();Object other$description = other.getDescription();return this$description == null ? other$description == null : this$description.equals(other$description);
    }

    public Integer getId()
    {
        return this.id;
    }

    public Integer getSemId()
    {
        return this.semId;
    }

    public String getTitle()
    {
        return this.title;
    }

    public String getBeginDate()
    {
        return this.beginDate;
    }

    public String getEndDate()
    {
        return this.endDate;
    }

    public String getDescription()
    {
        return this.description;
    }

    public Integer getTerminalId()
    {
        return this.terminalId;
    }
}
