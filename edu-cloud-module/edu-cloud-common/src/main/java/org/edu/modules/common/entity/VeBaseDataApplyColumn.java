package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.Date;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_data_apply_column")
@ApiModel(value="ve_base_data_apply_column对象", description="数据申请字段表-数据流程管理")
public class VeBaseDataApplyColumn
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="数据申请id", width=15.0D)
    @ApiModelProperty("数据申请id")
    private String applyId;
    @Excel(name="字段名称", width=15.0D)
    @ApiModelProperty("字段名称")
    private String columnName;
    @Excel(name="表名称", width=15.0D)
    @ApiModelProperty("表名称")
    private String tableName;
    @Excel(name="创建时间", width=15.0D)
    @ApiModelProperty("创建时间")
    private Date createTime;

    public VeBaseDataApplyColumn setColumnName(String columnName)
    {
        this.columnName = columnName;return this;
    }

    public VeBaseDataApplyColumn setApplyId(String applyId)
    {
        this.applyId = applyId;return this;
    }

    public VeBaseDataApplyColumn setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseDataApplyColumn(id=" + getId() + ", applyId=" + getApplyId() + ", columnName=" + getColumnName() + ", tableName=" + getTableName() + ", createTime=" + getCreateTime() + ")";
    }

    public VeBaseDataApplyColumn setCreateTime(Date createTime)
    {
        this.createTime = createTime;return this;
    }

    public VeBaseDataApplyColumn setTableName(String tableName)
    {
        this.tableName = tableName;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $applyId = getApplyId();result = result * 59 + ($applyId == null ? 43 : $applyId.hashCode());Object $columnName = getColumnName();result = result * 59 + ($columnName == null ? 43 : $columnName.hashCode());Object $tableName = getTableName();result = result * 59 + ($tableName == null ? 43 : $tableName.hashCode());Object $createTime = getCreateTime();result = result * 59 + ($createTime == null ? 43 : $createTime.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseDataApplyColumn;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseDataApplyColumn)) {
            return false;
        }
        VeBaseDataApplyColumn other = (VeBaseDataApplyColumn)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$applyId = getApplyId();Object other$applyId = other.getApplyId();
        if (this$applyId == null ? other$applyId != null : !this$applyId.equals(other$applyId)) {
            return false;
        }
        Object this$columnName = getColumnName();Object other$columnName = other.getColumnName();
        if (this$columnName == null ? other$columnName != null : !this$columnName.equals(other$columnName)) {
            return false;
        }
        Object this$tableName = getTableName();Object other$tableName = other.getTableName();
        if (this$tableName == null ? other$tableName != null : !this$tableName.equals(other$tableName)) {
            return false;
        }
        Object this$createTime = getCreateTime();Object other$createTime = other.getCreateTime();return this$createTime == null ? other$createTime == null : this$createTime.equals(other$createTime);
    }

    public String getId()
    {
        return this.id;
    }

    public String getApplyId()
    {
        return this.applyId;
    }

    public String getColumnName()
    {
        return this.columnName;
    }

    public String getTableName()
    {
        return this.tableName;
    }

    public Date getCreateTime()
    {
        return this.createTime;
    }
}
