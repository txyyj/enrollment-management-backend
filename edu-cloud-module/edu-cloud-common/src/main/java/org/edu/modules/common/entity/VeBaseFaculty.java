package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_faculty")
@ApiModel(value="ve_base_faculty对象", description="院系信息表—专业部信息表")
public class VeBaseFaculty
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="专业部代码", width=15.0D)
    @ApiModelProperty("院系代码")
    private String yxdm;
    @Excel(name="专业部名称", width=15.0D)
    @ApiModelProperty("院系名称")
    private String yxmc;
    @Excel(name="专业部英文名称", width=15.0D)
    @ApiModelProperty("院系英文名称")
    private String yxywmc;
    @ApiModelProperty("创建时间")
    private Integer createTime;
    @ApiModelProperty("更新时间")
    private Integer updateTime;
    @Excel(name="状态", width=15.0D)
    @ApiModelProperty("状态：1=可用，0=禁用")
    private Integer status;
    @Excel(name="终端ID", width=15.0D)
    @ApiModelProperty("终端ID")
    private Integer terminalid;
    @Excel(name="创建时间", width=15.0D)
    @TableField(exist=false)
    private String createTimeName;
    @Excel(name="更新时间", width=15.0D)
    @TableField(exist=false)
    private String updateTimeName;

    public VeBaseFaculty setYxmc(String yxmc)
    {
        this.yxmc = yxmc;return this;
    }

    public VeBaseFaculty setYxdm(String yxdm)
    {
        this.yxdm = yxdm;return this;
    }

    public VeBaseFaculty setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseFaculty(id=" + getId() + ", yxdm=" + getYxdm() + ", yxmc=" + getYxmc() + ", yxywmc=" + getYxywmc() + ", createTime=" + getCreateTime() + ", updateTime=" + getUpdateTime() + ", status=" + getStatus() + ", terminalid=" + getTerminalid() + ", createTimeName=" + getCreateTimeName() + ", updateTimeName=" + getUpdateTimeName() + ")";
    }

    public VeBaseFaculty setUpdateTimeName(String updateTimeName)
    {
        this.updateTimeName = updateTimeName;return this;
    }

    public VeBaseFaculty setCreateTimeName(String createTimeName)
    {
        this.createTimeName = createTimeName;return this;
    }

    public VeBaseFaculty setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeBaseFaculty setStatus(Integer status)
    {
        this.status = status;return this;
    }

    public VeBaseFaculty setUpdateTime(Integer updateTime)
    {
        this.updateTime = updateTime;return this;
    }

    public VeBaseFaculty setCreateTime(Integer createTime)
    {
        this.createTime = createTime;return this;
    }

    public VeBaseFaculty setYxywmc(String yxywmc)
    {
        this.yxywmc = yxywmc;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $createTime = getCreateTime();result = result * 59 + ($createTime == null ? 43 : $createTime.hashCode());Object $updateTime = getUpdateTime();result = result * 59 + ($updateTime == null ? 43 : $updateTime.hashCode());Object $status = getStatus();result = result * 59 + ($status == null ? 43 : $status.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $yxdm = getYxdm();result = result * 59 + ($yxdm == null ? 43 : $yxdm.hashCode());Object $yxmc = getYxmc();result = result * 59 + ($yxmc == null ? 43 : $yxmc.hashCode());Object $yxywmc = getYxywmc();result = result * 59 + ($yxywmc == null ? 43 : $yxywmc.hashCode());Object $createTimeName = getCreateTimeName();result = result * 59 + ($createTimeName == null ? 43 : $createTimeName.hashCode());Object $updateTimeName = getUpdateTimeName();result = result * 59 + ($updateTimeName == null ? 43 : $updateTimeName.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseFaculty;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseFaculty)) {
            return false;
        }
        VeBaseFaculty other = (VeBaseFaculty)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$createTime = getCreateTime();Object other$createTime = other.getCreateTime();
        if (this$createTime == null ? other$createTime != null : !this$createTime.equals(other$createTime)) {
            return false;
        }
        Object this$updateTime = getUpdateTime();Object other$updateTime = other.getUpdateTime();
        if (this$updateTime == null ? other$updateTime != null : !this$updateTime.equals(other$updateTime)) {
            return false;
        }
        Object this$status = getStatus();Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$yxdm = getYxdm();Object other$yxdm = other.getYxdm();
        if (this$yxdm == null ? other$yxdm != null : !this$yxdm.equals(other$yxdm)) {
            return false;
        }
        Object this$yxmc = getYxmc();Object other$yxmc = other.getYxmc();
        if (this$yxmc == null ? other$yxmc != null : !this$yxmc.equals(other$yxmc)) {
            return false;
        }
        Object this$yxywmc = getYxywmc();Object other$yxywmc = other.getYxywmc();
        if (this$yxywmc == null ? other$yxywmc != null : !this$yxywmc.equals(other$yxywmc)) {
            return false;
        }
        Object this$createTimeName = getCreateTimeName();Object other$createTimeName = other.getCreateTimeName();
        if (this$createTimeName == null ? other$createTimeName != null : !this$createTimeName.equals(other$createTimeName)) {
            return false;
        }
        Object this$updateTimeName = getUpdateTimeName();Object other$updateTimeName = other.getUpdateTimeName();return this$updateTimeName == null ? other$updateTimeName == null : this$updateTimeName.equals(other$updateTimeName);
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getYxdm()
    {
        return this.yxdm;
    }

    public String getYxmc()
    {
        return this.yxmc;
    }

    public String getYxywmc()
    {
        return this.yxywmc;
    }

    public Integer getCreateTime()
    {
        return this.createTime;
    }

    public Integer getUpdateTime()
    {
        return this.updateTime;
    }

    public Integer getStatus()
    {
        return this.status;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public String getCreateTimeName()
    {
        return this.createTimeName;
    }

    public String getUpdateTimeName()
    {
        return this.updateTimeName;
    }
}
