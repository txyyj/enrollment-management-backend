package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_calendar")
@ApiModel(value="ve_base_calendar对象", description="校历信息表")
public class VeBaseCalendar
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="学期Id", width=15.0D)
    @ApiModelProperty("学期Id")
    private Integer semId;
    @Excel(name="月份", width=15.0D)
    @ApiModelProperty("月份")
    private Integer month;
    @Excel(name="日期2015-05-06", width=15.0D)
    @ApiModelProperty("日期2015-05-06")
    private String dates;
    @Excel(name="第几周", width=15.0D)
    @ApiModelProperty("第几周")
    private Integer week;
    @Excel(name="年份", width=15.0D)
    @ApiModelProperty("年份")
    private Integer year;
    @Excel(name="周几", width=15.0D)
    @ApiModelProperty("周几")
    private Integer dayOfWeek;
    @Excel(name="终端id", width=15.0D)
    @ApiModelProperty("终端id")
    private Integer terminalId;
    @TableField(exist=false)
    private String xqmc;

    public VeBaseCalendar setMonth(Integer month)
    {
        this.month = month;return this;
    }

    public VeBaseCalendar setSemId(Integer semId)
    {
        this.semId = semId;return this;
    }

    public VeBaseCalendar setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseCalendar(id=" + getId() + ", semId=" + getSemId() + ", month=" + getMonth() + ", dates=" + getDates() + ", week=" + getWeek() + ", year=" + getYear() + ", dayOfWeek=" + getDayOfWeek() + ", terminalId=" + getTerminalId() + ", xqmc=" + getXqmc() + ")";
    }

    public VeBaseCalendar setXqmc(String xqmc)
    {
        this.xqmc = xqmc;return this;
    }

    public VeBaseCalendar setTerminalId(Integer terminalId)
    {
        this.terminalId = terminalId;return this;
    }

    public VeBaseCalendar setDayOfWeek(Integer dayOfWeek)
    {
        this.dayOfWeek = dayOfWeek;return this;
    }

    public VeBaseCalendar setYear(Integer year)
    {
        this.year = year;return this;
    }

    public VeBaseCalendar setWeek(Integer week)
    {
        this.week = week;return this;
    }

    public VeBaseCalendar setDates(String dates)
    {
        this.dates = dates;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $semId = getSemId();result = result * 59 + ($semId == null ? 43 : $semId.hashCode());Object $month = getMonth();result = result * 59 + ($month == null ? 43 : $month.hashCode());Object $week = getWeek();result = result * 59 + ($week == null ? 43 : $week.hashCode());Object $year = getYear();result = result * 59 + ($year == null ? 43 : $year.hashCode());Object $dayOfWeek = getDayOfWeek();result = result * 59 + ($dayOfWeek == null ? 43 : $dayOfWeek.hashCode());Object $terminalId = getTerminalId();result = result * 59 + ($terminalId == null ? 43 : $terminalId.hashCode());Object $dates = getDates();result = result * 59 + ($dates == null ? 43 : $dates.hashCode());Object $xqmc = getXqmc();result = result * 59 + ($xqmc == null ? 43 : $xqmc.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseCalendar;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseCalendar)) {
            return false;
        }
        VeBaseCalendar other = (VeBaseCalendar)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$semId = getSemId();Object other$semId = other.getSemId();
        if (this$semId == null ? other$semId != null : !this$semId.equals(other$semId)) {
            return false;
        }
        Object this$month = getMonth();Object other$month = other.getMonth();
        if (this$month == null ? other$month != null : !this$month.equals(other$month)) {
            return false;
        }
        Object this$week = getWeek();Object other$week = other.getWeek();
        if (this$week == null ? other$week != null : !this$week.equals(other$week)) {
            return false;
        }
        Object this$year = getYear();Object other$year = other.getYear();
        if (this$year == null ? other$year != null : !this$year.equals(other$year)) {
            return false;
        }
        Object this$dayOfWeek = getDayOfWeek();Object other$dayOfWeek = other.getDayOfWeek();
        if (this$dayOfWeek == null ? other$dayOfWeek != null : !this$dayOfWeek.equals(other$dayOfWeek)) {
            return false;
        }
        Object this$terminalId = getTerminalId();Object other$terminalId = other.getTerminalId();
        if (this$terminalId == null ? other$terminalId != null : !this$terminalId.equals(other$terminalId)) {
            return false;
        }
        Object this$dates = getDates();Object other$dates = other.getDates();
        if (this$dates == null ? other$dates != null : !this$dates.equals(other$dates)) {
            return false;
        }
        Object this$xqmc = getXqmc();Object other$xqmc = other.getXqmc();return this$xqmc == null ? other$xqmc == null : this$xqmc.equals(other$xqmc);
    }

    public Integer getId()
    {
        return this.id;
    }

    public Integer getSemId()
    {
        return this.semId;
    }

    public Integer getMonth()
    {
        return this.month;
    }

    public String getDates()
    {
        return this.dates;
    }

    public Integer getWeek()
    {
        return this.week;
    }

    public Integer getYear()
    {
        return this.year;
    }

    public Integer getDayOfWeek()
    {
        return this.dayOfWeek;
    }

    public Integer getTerminalId()
    {
        return this.terminalId;
    }

    public String getXqmc()
    {
        return this.xqmc;
    }
}
