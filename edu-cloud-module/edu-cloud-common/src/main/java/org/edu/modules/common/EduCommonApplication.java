package org.edu.modules.common;

import org.edu.common.util.oConvertUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication(scanBasePackages={"org.edu"})
@EnableFeignClients(basePackages={"org.edu"})
public class EduCommonApplication
{
    private static final Logger log = LoggerFactory.getLogger(EduCommonApplication.class);

    public static void main(String[] args)
    {
        ConfigurableApplicationContext application = SpringApplication.run(EduCommonApplication.class, args);

        Environment env = application.getEnvironment();
        String port = env.getProperty("server.port");
        String path = oConvertUtils.getString(env.getProperty("server.servlet.context-path"));
        log.info("\n----------------------------------------------------------\n\tApplication edu-Boot is running! Access URLs:\n\tLocal: \t\thttp://localhost:" + port + path + "/doc.html\n----------------------------------------------------------");
    }
}
