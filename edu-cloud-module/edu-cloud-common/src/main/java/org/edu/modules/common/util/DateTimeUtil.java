package org.edu.modules.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeUtil
{
    public static String timestampToTime(long time)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long timeLong = Long.valueOf(time).longValue();
        String dateTime = simpleDateFormat.format(new Date(timeLong * 1000L));
        return dateTime;
    }

    public static long timeToTimestamp(String time)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try
        {
            Date date = simpleDateFormat.parse(time);
            return date.getTime() / 1000L;
        }
        catch (ParseException e) {}
        return 0L;
    }

    public static String timestampToDate(long time)
    {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        long timeLong = Long.valueOf(time).longValue();
        String dateTime = simpleDateFormat.format(new Date(timeLong * 1000L));
        return dateTime;
    }

    public static long dateToTimestamp(String time)
    {
        if ((!"".equals(time)) && (time.equals("1970-01-01"))) {
            return 0L;
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            Date date = simpleDateFormat.parse(time);
            return date.getTime() / 1000L;
        }
        catch (ParseException e) {}
        return 0L;
    }
}
