package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseInterfaceUser;

public abstract interface IVeBaseInterfaceUserService
        extends IService<VeBaseInterfaceUser>
{
    public abstract List<Map<String, Object>> getInterfaceUserPageList(VeBaseInterfaceUser paramVeBaseInterfaceUser);

    public abstract List<Map<String, Object>> getInterfaceUserByNameAndPwd(String paramString1, String paramString2);

    public abstract List<Map<String, Object>> getListByName(String paramString);

    public abstract List<Map<String, Object>> getInterfaceUserByIdAndInterfaceName(String paramString1, String paramString2);

    public abstract int deleteInterfaceUserRelById(String paramString);

    public abstract int addInterfaceUserRelBatch(String paramString1, String[] paramArrayOfString, String paramString2);
}
