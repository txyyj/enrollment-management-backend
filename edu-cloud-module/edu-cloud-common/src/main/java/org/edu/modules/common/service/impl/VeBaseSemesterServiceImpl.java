package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseSemester;
import org.edu.modules.common.mapper.VeBaseSemesterMapper;
import org.edu.modules.common.service.IVeBaseSemesterService;
import org.edu.modules.common.util.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseSemesterServiceImpl
        extends ServiceImpl<VeBaseSemesterMapper, VeBaseSemester>
        implements IVeBaseSemesterService
{
    @Autowired
    VeBaseSemesterMapper veBaseSemesterMapper;

    public List<Map<String, Object>> getSemesterPageList(VeBaseSemester veBaseSemester)
    {
        List<Map<String, Object>> list = this.veBaseSemesterMapper.getSemesterPageList(veBaseSemester);
        list = setRQName(list);
        return list;
    }

    public VeBaseSemester getSemesterByName(Integer id, String name)
    {
        VeBaseSemester veBaseSemester = this.veBaseSemesterMapper.getSemesterByName(id, name);
        if (veBaseSemester != null)
        {
            if (veBaseSemester.getXqksrq() != null) {
                veBaseSemester.setXqksrqName(DateTimeUtil.timestampToDate(veBaseSemester.getXqksrq().intValue()));
            } else {
                veBaseSemester.setXqksrqName("");
            }
            if (veBaseSemester.getXqjsrq() != null) {
                veBaseSemester.setXqjsrqName(DateTimeUtil.timestampToDate(veBaseSemester.getXqjsrq().intValue()));
            } else {
                veBaseSemester.setXqjsrqName("");
            }
        }
        return veBaseSemester;
    }

    public VeBaseSemester getSemesterByCode(Integer id, String code)
    {
        VeBaseSemester veBaseSemester = this.veBaseSemesterMapper.getSemesterByCode(id, code);
        if (veBaseSemester != null)
        {
            if (veBaseSemester.getXqksrq() != null) {
                veBaseSemester.setXqksrqName(DateTimeUtil.timestampToDate(veBaseSemester.getXqksrq().intValue()));
            } else {
                veBaseSemester.setXqksrqName("");
            }
            if (veBaseSemester.getXqjsrq() != null) {
                veBaseSemester.setXqjsrqName(DateTimeUtil.timestampToDate(veBaseSemester.getXqjsrq().intValue()));
            } else {
                veBaseSemester.setXqjsrqName("");
            }
        }
        return veBaseSemester;
    }

    public VeBaseSemester getSemesterByIsCurrent(Integer id)
    {
        VeBaseSemester veBaseSemester = this.veBaseSemesterMapper.getSemesterByIsCurrent(id);
        if (veBaseSemester != null)
        {
            if (veBaseSemester.getXqksrq() != null) {
                veBaseSemester.setXqksrqName(DateTimeUtil.timestampToDate(veBaseSemester.getXqksrq().intValue()));
            } else {
                veBaseSemester.setXqksrqName("");
            }
            if (veBaseSemester.getXqjsrq() != null) {
                veBaseSemester.setXqjsrqName(DateTimeUtil.timestampToDate(veBaseSemester.getXqjsrq().intValue()));
            } else {
                veBaseSemester.setXqjsrqName("");
            }
        }
        return veBaseSemester;
    }

    public List<VeBaseSemester> getByCodeName(String code, String name)
    {
        List<VeBaseSemester> list = this.veBaseSemesterMapper.getByCodeName(code, name);
        list = setVeBaseSemesterList(list);
        return list;
    }

    public VeBaseSemester getSemesterNew()
    {
        VeBaseSemester veBaseSemester = this.veBaseSemesterMapper.getSemesterNew();
        if (veBaseSemester != null)
        {
            if (veBaseSemester.getXqksrq() != null) {
                veBaseSemester.setXqksrqName(DateTimeUtil.timestampToDate(veBaseSemester.getXqksrq().intValue()));
            } else {
                veBaseSemester.setXqksrqName("");
            }
            if (veBaseSemester.getXqjsrq() != null) {
                veBaseSemester.setXqjsrqName(DateTimeUtil.timestampToDate(veBaseSemester.getXqjsrq().intValue()));
            } else {
                veBaseSemester.setXqjsrqName("");
            }
        }
        return veBaseSemester;
    }

    private List<Map<String, Object>> setRQName(List<Map<String, Object>> list)
    {
        if (list.size() > 0) {
            for (Map map : list)
            {
                if ((!"".equals(map.get("xqksrq"))) && (map.get("xqksrq") != null))
                {
                    Long xqksrq = Long.valueOf(Long.parseLong(map.get("xqksrq").toString()));
                    map.put("xqksrqName", DateTimeUtil.timestampToDate(xqksrq.longValue()));
                }
                else
                {
                    map.put("xqksrqName", "");
                }
                if ((!"".equals(map.get("xqjsrq"))) && (map.get("xqjsrq") != null))
                {
                    Long xqjsrq = Long.valueOf(Long.parseLong(map.get("xqjsrq").toString()));
                    map.put("xqjsrqName", DateTimeUtil.timestampToDate(xqjsrq.longValue()));
                }
                else
                {
                    map.put("xqjsrqName", "");
                }
            }
        }
        return list;
    }

    private List<VeBaseSemester> setVeBaseSemesterList(List<VeBaseSemester> list)
    {
        if (list.size() > 0) {
            for (VeBaseSemester veBaseSemester : list)
            {
                if (veBaseSemester.getXqksrq() != null)
                {
                    Long xqksrq = Long.valueOf(Long.parseLong(veBaseSemester.getXqksrq().toString()));
                    veBaseSemester.setXqksrqName(DateTimeUtil.timestampToDate(xqksrq.longValue()));
                }
                else
                {
                    veBaseSemester.setXqksrqName("");
                }
                if (veBaseSemester.getXqjsrq() != null)
                {
                    Long xqjsrq = Long.valueOf(Long.parseLong(veBaseSemester.getXqjsrq().toString()));
                    veBaseSemester.setXqjsrqName(DateTimeUtil.timestampToDate(xqjsrq.longValue()));
                }
                else
                {
                    veBaseSemester.setXqjsrqName("");
                }
            }
        }
        return list;
    }
}

