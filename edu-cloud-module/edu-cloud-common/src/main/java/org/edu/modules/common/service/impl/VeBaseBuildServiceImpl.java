package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseBuild;
import org.edu.modules.common.mapper.VeBaseBuildMapper;
import org.edu.modules.common.service.IVeBaseBuildService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseBuildServiceImpl
        extends ServiceImpl<VeBaseBuildMapper, VeBaseBuild>
        implements IVeBaseBuildService
{
    @Autowired
    private VeBaseBuildMapper veBaseBuildMapper;

    public VeBaseBuild getBuildByName(Integer id, String name)
    {
        return this.veBaseBuildMapper.getBuildByName(id, name);
    }

    public List<VeBaseBuild> queryBuildListByCampusId(Integer campusId)
    {
        return this.veBaseBuildMapper.queryBuildListByCampusId(campusId);
    }

    public List<Map<String, Object>> queryJianzhuByCampusId(Integer campusId)
    {
        return this.veBaseBuildMapper.queryJianzhuByCampusId(campusId);
    }

    public List<Map<String, Object>> queryRoomListByBuildId(Integer buildId)
    {
        return this.veBaseBuildMapper.queryRoomListByBuildId(buildId);
    }
}
