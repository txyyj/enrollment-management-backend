package org.edu.modules.common.vo;

import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

public class VoAppUserBySearch
        implements Serializable
{
    @ApiModelProperty("用户名")
    private String realName;
    @ApiModelProperty("专业id")
    private Integer specId;
    @ApiModelProperty("角色id")
    private Integer roleId;
    @ApiModelProperty("状态(1教师, 2学生)")
    private String userType;
    @ApiModelProperty("部门id")
    private Integer deptId;
    @ApiModelProperty("接口权限id")
    private String interfaceUserId;

    public VoAppUserBySearch setRoleId(Integer roleId)
    {
        this.roleId = roleId;return this;
    }

    public VoAppUserBySearch setSpecId(Integer specId)
    {
        this.specId = specId;return this;
    }

    public VoAppUserBySearch setRealName(String realName)
    {
        this.realName = realName;return this;
    }

    public String toString()
    {
        return "VoAppUserBySearch(realName=" + getRealName() + ", specId=" + getSpecId() + ", roleId=" + getRoleId() + ", userType=" + getUserType() + ", deptId=" + getDeptId() + ", interfaceUserId=" + getInterfaceUserId() + ")";
    }

    public VoAppUserBySearch setInterfaceUserId(String interfaceUserId)
    {
        this.interfaceUserId = interfaceUserId;return this;
    }

    public VoAppUserBySearch setDeptId(Integer deptId)
    {
        this.deptId = deptId;return this;
    }

    public VoAppUserBySearch setUserType(String userType)
    {
        this.userType = userType;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $specId = getSpecId();result = result * 59 + ($specId == null ? 43 : $specId.hashCode());Object $roleId = getRoleId();result = result * 59 + ($roleId == null ? 43 : $roleId.hashCode());Object $deptId = getDeptId();result = result * 59 + ($deptId == null ? 43 : $deptId.hashCode());Object $realName = getRealName();result = result * 59 + ($realName == null ? 43 : $realName.hashCode());Object $userType = getUserType();result = result * 59 + ($userType == null ? 43 : $userType.hashCode());Object $interfaceUserId = getInterfaceUserId();result = result * 59 + ($interfaceUserId == null ? 43 : $interfaceUserId.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VoAppUserBySearch;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VoAppUserBySearch)) {
            return false;
        }
        VoAppUserBySearch other = (VoAppUserBySearch)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$specId = getSpecId();Object other$specId = other.getSpecId();
        if (this$specId == null ? other$specId != null : !this$specId.equals(other$specId)) {
            return false;
        }
        Object this$roleId = getRoleId();Object other$roleId = other.getRoleId();
        if (this$roleId == null ? other$roleId != null : !this$roleId.equals(other$roleId)) {
            return false;
        }
        Object this$deptId = getDeptId();Object other$deptId = other.getDeptId();
        if (this$deptId == null ? other$deptId != null : !this$deptId.equals(other$deptId)) {
            return false;
        }
        Object this$realName = getRealName();Object other$realName = other.getRealName();
        if (this$realName == null ? other$realName != null : !this$realName.equals(other$realName)) {
            return false;
        }
        Object this$userType = getUserType();Object other$userType = other.getUserType();
        if (this$userType == null ? other$userType != null : !this$userType.equals(other$userType)) {
            return false;
        }
        Object this$interfaceUserId = getInterfaceUserId();Object other$interfaceUserId = other.getInterfaceUserId();return this$interfaceUserId == null ? other$interfaceUserId == null : this$interfaceUserId.equals(other$interfaceUserId);
    }

    public String getRealName()
    {
        return this.realName;
    }

    public Integer getSpecId()
    {
        return this.specId;
    }

    public Integer getRoleId()
    {
        return this.roleId;
    }

    public String getUserType()
    {
        return this.userType;
    }

    public Integer getDeptId()
    {
        return this.deptId;
    }

    public String getInterfaceUserId()
    {
        return this.interfaceUserId;
    }
}
