package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseSchool;
import org.edu.modules.common.mapper.VeBaseSchoolMapper;
import org.edu.modules.common.service.IVeBaseSchoolService;
import org.edu.modules.common.util.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseSchoolServiceImpl
        extends ServiceImpl<VeBaseSchoolMapper, VeBaseSchool>
        implements IVeBaseSchoolService
{
    @Autowired
    private VeBaseSchoolMapper veBaseSchoolMapper;

    public List<Map<String, Object>> getSchoolPageList(VeBaseSchool veBaseSchool)
    {
        List<Map<String, Object>> list = this.veBaseSchoolMapper.getSchoolPageList(veBaseSchool);
        list = setJxnyName(list);
        return list;
    }

    public VeBaseSchool getSchoolByName(String id, String name)
    {
        VeBaseSchool veBaseSchool = this.veBaseSchoolMapper.getSchoolByName(id, name);
        if (veBaseSchool != null) {
            if (veBaseSchool.getJxny() != null) {
                veBaseSchool.setJxnyName(DateTimeUtil.timestampToDate(veBaseSchool.getJxny().intValue()));
            } else {
                veBaseSchool.setJxnyName("");
            }
        }
        return veBaseSchool;
    }

    private List<Map<String, Object>> setJxnyName(List<Map<String, Object>> list)
    {
        if (list.size() > 0) {
            for (Map map : list) {
                if ((!"".equals(map.get("jxny"))) && (map.get("jxny") != null))
                {
                    Long times = Long.valueOf(Long.parseLong(map.get("jxny").toString()));
                    map.put("jxnyName", DateTimeUtil.timestampToDate(times.longValue()));
                }
                else
                {
                    map.put("jxnyName", "");
                }
            }
        }
        return list;
    }
}

