package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseStudent;
import org.edu.modules.common.mapper.VeBaseStudentMapper;
import org.edu.modules.common.service.IVeBaseStudentService;
import org.edu.modules.common.util.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseStudentServiceImpl
        extends ServiceImpl<VeBaseStudentMapper, VeBaseStudent>
        implements IVeBaseStudentService
{
    @Autowired
    private VeBaseStudentMapper veBaseStudentMapper;

    public VeBaseStudent getModelById(Integer id)
    {
        VeBaseStudent veBaseStudent = this.veBaseStudentMapper.getModelById(id);
        if ((veBaseStudent != null) &&
                (veBaseStudent.getRxny() != null)) {
            veBaseStudent.setRxnyName(DateTimeUtil.timestampToDate(veBaseStudent.getRxny().longValue()));
        }
        return veBaseStudent;
    }

    public List<Map<String, Object>> getStudentPageList(VeBaseStudent veBaseStudent)
    {
        List<Map<String, Object>> list = this.veBaseStudentMapper.getStudentPageList(veBaseStudent);
        if (list.size() > 0) {
            for (Map map : list)
            {
                if (map.get("rxny") != null) {
                    map.put("rxnyName", DateTimeUtil.timestampToDate(Long.parseLong(map.get("rxny").toString())));
                } else {
                    map.put("rxnyName", "");
                }
                if (map.get("csrq") != null) {
                    map.put("csrqName", DateTimeUtil.timestampToDate(Long.parseLong(map.get("csrq").toString())));
                } else {
                    map.put("csrqName", "");
                }
                if (map.get("createTime") != null) {
                    map.put("createTimeName", DateTimeUtil.timestampToTime(Long.parseLong(map.get("createTime").toString())));
                } else {
                    map.put("createTimeName", "");
                }
                if (map.get("updateTime") != null) {
                    map.put("updateTimeName", DateTimeUtil.timestampToTime(Long.parseLong(map.get("updateTime").toString())));
                } else {
                    map.put("updateTimeName", "");
                }
            }
        }
        return list;
    }

    public Map getStudentStatusStatistics()
    {
        Map map = this.veBaseStudentMapper.getStudentStatusStatistics();

        Long xs = Long.valueOf(map.get("xs").toString());

        Long zx = Long.valueOf(map.get("zx").toString());

        Long xx = Long.valueOf(map.get("xx").toString());

        Long tx = Long.valueOf(map.get("tx").toString());

        Long kc = Long.valueOf(map.get("kc").toString());

        Long byl = Long.valueOf(map.get("byl").toString());

        Long yy = Long.valueOf(map.get("yy").toString());

        Long zxx = Long.valueOf(map.get("zxx").toString());

        Long jy = Long.valueOf(map.get("jy").toString());
        DecimalFormat decimalFormat = new DecimalFormat("0.00");
        Long total = Long.valueOf(xs.longValue() + zx.longValue() + xx.longValue() + tx.longValue() + kc.longValue() + byl.longValue() + yy.longValue() + zxx.longValue() + jy.longValue());

        Double xsRate = Double.valueOf(xs.longValue() * 1.0D / total.longValue());
        map.put("xsRate", decimalFormat.format(xsRate.doubleValue() * 100.0D) + "%");

        Double zxRate = Double.valueOf(zx.longValue() * 1.0D / total.longValue());
        map.put("zxRate", decimalFormat.format(zxRate.doubleValue() * 100.0D) + "%");

        Double xxRate = Double.valueOf(xx.longValue() * 1.0D / total.longValue());
        map.put("xxRate", decimalFormat.format(xxRate.doubleValue() * 100.0D) + "%");

        Double txRate = Double.valueOf(tx.longValue() * 1.0D / total.longValue());
        map.put("txRate", decimalFormat.format(txRate.doubleValue() * 100.0D) + "%");

        Double kcRate = Double.valueOf(kc.longValue() * 1.0D / total.longValue());
        map.put("kcRate", decimalFormat.format(kcRate.doubleValue() * 100.0D) + "%");

        Double bylRate = Double.valueOf(byl.longValue() * 1.0D / total.longValue());
        map.put("bylRate", decimalFormat.format(bylRate.doubleValue() * 100.0D) + "%");

        Double yyRate = Double.valueOf(yy.longValue() * 1.0D / total.longValue());
        map.put("yyRate", decimalFormat.format(yyRate.doubleValue() * 100.0D) + "%");

        Double zxxRate = Double.valueOf(zxx.longValue() * 1.0D / total.longValue());
        map.put("zxxRate", decimalFormat.format(zxxRate.doubleValue() * 100.0D) + "%");

        Double jyRate = Double.valueOf(jy.longValue() * 1.0D / total.longValue());
        map.put("jyRate", decimalFormat.format(jyRate.doubleValue() * 100.0D) + "%");
        return map;
    }

    public VeBaseStudent getModelByUserId(String userId)
    {
        VeBaseStudent veBaseStudent = this.veBaseStudentMapper.getModelByUserId(userId);
        if ((veBaseStudent != null) &&
                (veBaseStudent.getRxny() != null)) {
            veBaseStudent.setRxnyName(DateTimeUtil.timestampToDate(veBaseStudent.getRxny().longValue()));
        }
        return veBaseStudent;
    }

    public List<Map<String, Object>> getModelByName(String name)
    {
        List<Map<String, Object>> veBaseStudent = this.veBaseStudentMapper.getModelByName(name);
        return veBaseStudent;
    }

    public int addStudent(VeBaseStudent veBaseStudent)
    {
        return this.veBaseStudentMapper.addStudent(veBaseStudent);
    }

    public VeBaseStudent getStudentBySFZH(Integer id, String sfzh)
    {
        VeBaseStudent veBaseStudent = this.veBaseStudentMapper.getStudentBySFZH(id, sfzh);
        if ((veBaseStudent != null) &&
                (veBaseStudent.getRxny() != null)) {
            veBaseStudent.setRxnyName(DateTimeUtil.timestampToDate(veBaseStudent.getRxny().longValue()));
        }
        return veBaseStudent;
    }

    public VeBaseStudent getStudentByXH(Integer id, String xh)
    {
        VeBaseStudent veBaseStudent = this.veBaseStudentMapper.getStudentByXH(id, xh);
        if ((veBaseStudent != null) &&
                (veBaseStudent.getRxny() != null)) {
            veBaseStudent.setRxnyName(DateTimeUtil.timestampToDate(veBaseStudent.getRxny().longValue()));
        }
        return veBaseStudent;
    }

    public Map getStudentAndInfoById(Integer id)
    {
        Map map = this.veBaseStudentMapper.getStudentAndInfoById(id);
        if (map != null)
        {
            if (map.get("rxny") != null) {
                map.put("rxnyName", DateTimeUtil.timestampToDate(Long.parseLong(map.get("rxny").toString())));
            } else {
                map.put("rxnyName", "");
            }
            if (map.get("csrq") != null) {
                map.put("csrqName", DateTimeUtil.timestampToDate(Long.parseLong(map.get("csrq").toString())));
            } else {
                map.put("csrqName", "");
            }
        }
        return map;
    }

    public List<Map<String, Object>> getStudentStatisticsByYear(String year)
    {
        return this.veBaseStudentMapper.getStudentStatisticsByYear(year);
    }
}

