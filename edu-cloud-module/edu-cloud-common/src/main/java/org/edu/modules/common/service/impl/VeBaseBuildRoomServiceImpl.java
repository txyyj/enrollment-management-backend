package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseBuildRoom;
import org.edu.modules.common.mapper.VeBaseBuildRoomMapper;
import org.edu.modules.common.service.IVeBaseBuildRoomService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseBuildRoomServiceImpl
        extends ServiceImpl<VeBaseBuildRoomMapper, VeBaseBuildRoom>
        implements IVeBaseBuildRoomService
{
    @Autowired
    private VeBaseBuildRoomMapper veBaseBuildRoomMapper;

    public List<Map<String, Object>> getBuildRoomPageList(VeBaseBuildRoom veBaseBuildRoom)
    {
        return this.veBaseBuildRoomMapper.getBuildRoomPageList(veBaseBuildRoom);
    }

    public VeBaseBuildRoom getBuildRoomByName(Integer id, String name)
    {
        return this.veBaseBuildRoomMapper.getBuildRoomByName(id, name);
    }

    public List<VeBaseBuildRoom> getRoomListByBuildId(Integer buildId)
    {
        return this.veBaseBuildRoomMapper.getRoomListByBuildId(buildId);
    }
}
