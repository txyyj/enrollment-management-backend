package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseCampus;
import org.edu.modules.common.mapper.VeBaseCampusMapper;
import org.edu.modules.common.service.IVeBaseCampusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseCampusServiceImpl
        extends ServiceImpl<VeBaseCampusMapper, VeBaseCampus>
        implements IVeBaseCampusService
{
    @Autowired
    private VeBaseCampusMapper veBaseCampusMapper;

    public List<Map<String, Object>> getCampusPageList(VeBaseCampus veBaseCampus)
    {
        return this.veBaseCampusMapper.getCampusPageList(veBaseCampus);
    }

    public VeBaseCampus getCampusByName(Integer id, String name)
    {
        return this.veBaseCampusMapper.getCampusByName(id, name);
    }

    public VeBaseCampus getCampusByCode(Integer id, String code)
    {
        return this.veBaseCampusMapper.getCampusByCode(id, code);
    }
}
