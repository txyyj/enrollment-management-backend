package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseBanJi;

public abstract interface IVeBaseBanJiService
        extends IService<VeBaseBanJi>
{
    public abstract List<VeBaseBanJi> getBanJiListBySpecId(Integer paramInteger1, Integer paramInteger2);

    public abstract List<VeBaseBanJi> queryBanJiListBySpecAndGradeId(Integer paramInteger1, Integer paramInteger2);

    public abstract List<VeBaseBanJi> getBanJiListByGradeId(Integer paramInteger);

    public abstract List<Map<String, Object>> getBanJiPageList(VeBaseBanJi paramVeBaseBanJi);

    public abstract List<Map<String, Object>> queryBanJiPageListBySearch(String paramString1, String paramString2);

    public abstract VeBaseBanJi getBanJiByName(Integer paramInteger, String paramString);

    public abstract VeBaseBanJi getBanJiByCode(Integer paramInteger, String paramString);

    public abstract int updateBanJiSchedule();
}
