package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_dict_area")
@ApiModel(value="ve_base_dict_area对象", description="地区(省市县)信息表")
public class VeBaseDictArea
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="地区名字", width=15.0D)
    @ApiModelProperty("地区名字")
    private String name;
    @Excel(name="上级路径ID", width=15.0D)
    @ApiModelProperty("上级路径ID")
    private Integer pid;
    @Excel(name="顺序", width=15.0D)
    @ApiModelProperty("顺序")
    private Integer listSort;
    @TableField(exist=false)
    private List<VeBaseDictArea> children;
    @TableField(exist=false)
    private String interfaceUserId;

    public VeBaseDictArea setPid(Integer pid)
    {
        this.pid = pid;return this;
    }

    public VeBaseDictArea setName(String name)
    {
        this.name = name;return this;
    }

    public VeBaseDictArea setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseDictArea(id=" + getId() + ", name=" + getName() + ", pid=" + getPid() + ", listSort=" + getListSort() + ", children=" + getChildren() + ", interfaceUserId=" + getInterfaceUserId() + ")";
    }

    public VeBaseDictArea setInterfaceUserId(String interfaceUserId)
    {
        this.interfaceUserId = interfaceUserId;return this;
    }

    public VeBaseDictArea setChildren(List<VeBaseDictArea> children)
    {
        this.children = children;return this;
    }

    public VeBaseDictArea setListSort(Integer listSort)
    {
        this.listSort = listSort;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $pid = getPid();result = result * 59 + ($pid == null ? 43 : $pid.hashCode());Object $listSort = getListSort();result = result * 59 + ($listSort == null ? 43 : $listSort.hashCode());Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());Object $children = getChildren();result = result * 59 + ($children == null ? 43 : $children.hashCode());Object $interfaceUserId = getInterfaceUserId();result = result * 59 + ($interfaceUserId == null ? 43 : $interfaceUserId.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseDictArea;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseDictArea)) {
            return false;
        }
        VeBaseDictArea other = (VeBaseDictArea)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$pid = getPid();Object other$pid = other.getPid();
        if (this$pid == null ? other$pid != null : !this$pid.equals(other$pid)) {
            return false;
        }
        Object this$listSort = getListSort();Object other$listSort = other.getListSort();
        if (this$listSort == null ? other$listSort != null : !this$listSort.equals(other$listSort)) {
            return false;
        }
        Object this$name = getName();Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) {
            return false;
        }
        Object this$children = getChildren();Object other$children = other.getChildren();
        if (this$children == null ? other$children != null : !this$children.equals(other$children)) {
            return false;
        }
        Object this$interfaceUserId = getInterfaceUserId();Object other$interfaceUserId = other.getInterfaceUserId();return this$interfaceUserId == null ? other$interfaceUserId == null : this$interfaceUserId.equals(other$interfaceUserId);
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public Integer getPid()
    {
        return this.pid;
    }

    public Integer getListSort()
    {
        return this.listSort;
    }

    public List<VeBaseDictArea> getChildren()
    {
        return this.children;
    }

    public String getInterfaceUserId()
    {
        return this.interfaceUserId;
    }
}
