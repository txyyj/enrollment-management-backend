package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseXJYD;

public abstract interface VeBaseXJYDMapper
        extends BaseMapper<VeBaseXJYD>
{
    public abstract List<Map<String, Object>> getXJYDPageList(VeBaseXJYD paramVeBaseXJYD);
}
