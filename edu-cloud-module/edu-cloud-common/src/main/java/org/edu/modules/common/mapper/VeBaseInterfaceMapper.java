package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseInterface;

public abstract interface VeBaseInterfaceMapper
        extends BaseMapper<VeBaseInterface>
{
    public abstract List<Map<String, Object>> getInterfacePageList(VeBaseInterface paramVeBaseInterface);

    public abstract List<Map<String, Object>> getInterfaceList();
}

