package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.common.entity.VeBaseDepartment;

public abstract interface IVeBaseDepartmentService
        extends IService<VeBaseDepartment>
{
    public abstract List<VeBaseDepartment> getTreeList();

    public abstract List<VeBaseDepartment> getDepartmentAndTeacherList();

    public abstract VeBaseDepartment getDepartmentByName(Integer paramInteger, String paramString);

    public abstract VeBaseDepartment getDepartmentByCode(Integer paramInteger, String paramString);
}
