package org.edu.modules.common.we;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class ConstantConfig
{
    @Value("${aliyun.file.endpoint}")
    private String endpoint;
    @Value("${aliyun.file.accessKeyId}")
    private String accessKeyId;
    @Value("${aliyun.file.accessKeySecret}")
    private String accessKeySecret;
    @Value("${aliyun.file.folder}")
    private String folder;
    @Value("${aliyun.file.bucketName}")
    private String bucketName;
    @Value("${aliyun.file.webUrl}")
    private String webUrl;

    public void setAccessKeySecret(String accessKeySecret)
    {
        this.accessKeySecret = accessKeySecret;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $endpoint = getEndpoint();result = result * 59 + ($endpoint == null ? 43 : $endpoint.hashCode());Object $accessKeyId = getAccessKeyId();result = result * 59 + ($accessKeyId == null ? 43 : $accessKeyId.hashCode());Object $accessKeySecret = getAccessKeySecret();result = result * 59 + ($accessKeySecret == null ? 43 : $accessKeySecret.hashCode());Object $folder = getFolder();result = result * 59 + ($folder == null ? 43 : $folder.hashCode());Object $bucketName = getBucketName();result = result * 59 + ($bucketName == null ? 43 : $bucketName.hashCode());Object $webUrl = getWebUrl();result = result * 59 + ($webUrl == null ? 43 : $webUrl.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof ConstantConfig;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof ConstantConfig)) {
            return false;
        }
        ConstantConfig other = (ConstantConfig)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$endpoint = getEndpoint();Object other$endpoint = other.getEndpoint();
        if (this$endpoint == null ? other$endpoint != null : !this$endpoint.equals(other$endpoint)) {
            return false;
        }
        Object this$accessKeyId = getAccessKeyId();Object other$accessKeyId = other.getAccessKeyId();
        if (this$accessKeyId == null ? other$accessKeyId != null : !this$accessKeyId.equals(other$accessKeyId)) {
            return false;
        }
        Object this$accessKeySecret = getAccessKeySecret();Object other$accessKeySecret = other.getAccessKeySecret();
        if (this$accessKeySecret == null ? other$accessKeySecret != null : !this$accessKeySecret.equals(other$accessKeySecret)) {
            return false;
        }
        Object this$folder = getFolder();Object other$folder = other.getFolder();
        if (this$folder == null ? other$folder != null : !this$folder.equals(other$folder)) {
            return false;
        }
        Object this$bucketName = getBucketName();Object other$bucketName = other.getBucketName();
        if (this$bucketName == null ? other$bucketName != null : !this$bucketName.equals(other$bucketName)) {
            return false;
        }
        Object this$webUrl = getWebUrl();Object other$webUrl = other.getWebUrl();return this$webUrl == null ? other$webUrl == null : this$webUrl.equals(other$webUrl);
    }

    public void setWebUrl(String webUrl)
    {
        this.webUrl = webUrl;
    }

    public void setBucketName(String bucketName)
    {
        this.bucketName = bucketName;
    }

    public void setEndpoint(String endpoint)
    {
        this.endpoint = endpoint;
    }

    public void setAccessKeyId(String accessKeyId)
    {
        this.accessKeyId = accessKeyId;
    }

    public String toString()
    {
        return "ConstantConfig(endpoint=" + getEndpoint() + ", accessKeyId=" + getAccessKeyId() + ", accessKeySecret=" + getAccessKeySecret() + ", folder=" + getFolder() + ", bucketName=" + getBucketName() + ", webUrl=" + getWebUrl() + ")";
    }

    public void setFolder(String folder)
    {
        this.folder = folder;
    }

    public String getEndpoint()
    {
        return this.endpoint;
    }

    public String getAccessKeyId()
    {
        return this.accessKeyId;
    }

    public String getAccessKeySecret()
    {
        return this.accessKeySecret;
    }

    public String getFolder()
    {
        return this.folder;
    }

    public String getBucketName()
    {
        return this.bucketName;
    }

    public String getWebUrl()
    {
        return this.webUrl;
    }
}

