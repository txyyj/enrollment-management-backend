package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_student")
@ApiModel(value="ve_base_student对象", description="学生基本信息表")
public class VeBaseStudent
        implements Serializable
{
    @ApiModelProperty("id")
    @TableId(type=IdType.AUTO)
    private Integer id;
    @Excel(name="身份证号", width=15.0D)
    @ApiModelProperty("身份证号，唯一")
    private String sfzh;
    @Excel(name="学号", width=15.0D)
    @ApiModelProperty("学号")
    private String xh;
    @Excel(name="姓名", width=15.0D)
    @ApiModelProperty("姓名")
    private String xm;
    @Excel(name="性别", width=15.0D)
    @ApiModelProperty("性别码1男生2女生")
    private String xbm;
    @ApiModelProperty("用户ID")
    private String userId;
    @Excel(name="民族", width=15.0D)
    @ApiModelProperty("民族码")
    private String mzm;
    @Excel(name="报名号", width=15.0D)
    @ApiModelProperty("报名号")
    private String bmh;
    @Excel(name="就读方式", width=15.0D)
    @ApiModelProperty("就读方式：1住校，2走读")
    private Integer jdfs;
    @Excel(name="当前状态码", width=15.0D)
    @ApiModelProperty("当前状态码'XS'=>'新生', 'ZX' => '在校', 'XX' => '休学', 'TX' => '退学', 'KC' => '开除', 'BY' => '毕业', 'YY' => '肄业', 'ZXX' => '转学', 'JY' => '结业'")
    private String xsdqztm;
    @ApiModelProperty("入学年月")
    private Long rxny;
    @Excel(name="学制", width=15.0D)
    @ApiModelProperty("学制;与学制表关联")
    private Integer xz;
    @Excel(name="院系", width=15.0D)
    @ApiModelProperty("院系ID")
    private Integer falId;
    @Excel(name="专业", width=15.0D)
    @ApiModelProperty("专业ID")
    private Integer specId;
    @Excel(name="班级", width=15.0D)
    @ApiModelProperty("班级ID")
    private Integer bjId;
    @Excel(name="年级", width=15.0D)
    @ApiModelProperty("年级ID")
    private Integer gradeId;
    @ApiModelProperty("创建时间")
    private Integer createTime;
    @ApiModelProperty("更新时间")
    private Integer updateTime;
    @Excel(name="户口所在省份", width=15.0D)
    @ApiModelProperty("户口所在省份")
    private String province;
    @ApiModelProperty("户口所在省份ID")
    private Integer provinceId;
    @Excel(name="户口所在市", width=15.0D)
    @ApiModelProperty("户口所在市")
    private String city;
    @ApiModelProperty("户口所在市Id")
    private Integer cityId;
    @Excel(name="户口所在区", width=15.0D)
    @ApiModelProperty("户口所在区")
    private String county;
    @ApiModelProperty("户口所在区ID")
    private Integer countyId;
    @ApiModelProperty("生源地省id")
    private Integer shengId;
    @ApiModelProperty("生源地市id")
    private Integer shiId;
    @ApiModelProperty("生源地区id")
    private Integer quId;
    @Excel(name="是否是困难生", width=15.0D)
    @ApiModelProperty("是否是困难生 0=否  1=是")
    private Integer sfkns;
    @Excel(name="终端ID", width=15.0D)
    @ApiModelProperty("终端ID")
    private Integer terminalId;
    @Excel(name="准考证号", width=15.0D)
    @ApiModelProperty("准考证号")
    private String zkzh;
    @Excel(name="考生号", width=15.0D)
    @ApiModelProperty("考生号")
    private String ksh;
    @ApiModelProperty("更新状态（0：未更新; 1：已更新）")
    private Integer updateStatus;
    @Excel(name="入学年月", width=15.0D)
    @TableField(exist=false)
    private String rxnyName;
    @TableField(exist=false)
    private String[] studentIds;
    @ApiModelProperty("0(not in studentIds), 1(in studentIds)")
    @TableField(exist=false)
    private Integer num;
    @TableField(exist=false)
    private String interfaceUserId;
    @Excel(name="出生日期", width=15.0D)
    @TableField(exist=false)
    private String csrqName;
    @Excel(name="创建时间", width=15.0D)
    @TableField(exist=false)
    private String createTimeName;
    @Excel(name="更新时间", width=15.0D)
    @TableField(exist=false)
    private String updateTimeName;
    @TableField(exist=false)
    private List<Integer> bjIds;

    public VeBaseStudent setXh(String xh)
    {
        this.xh = xh;return this;
    }

    public VeBaseStudent setSfzh(String sfzh)
    {
        this.sfzh = sfzh;return this;
    }

    public VeBaseStudent setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseStudent(id=" + getId() + ", sfzh=" + getSfzh() + ", xh=" + getXh() + ", xm=" + getXm() + ", xbm=" + getXbm() + ", userId=" + getUserId() + ", mzm=" + getMzm() + ", bmh=" + getBmh() + ", jdfs=" + getJdfs() + ", xsdqztm=" + getXsdqztm() + ", rxny=" + getRxny() + ", xz=" + getXz() + ", falId=" + getFalId() + ", specId=" + getSpecId() + ", bjId=" + getBjId() + ", gradeId=" + getGradeId() + ", createTime=" + getCreateTime() + ", updateTime=" + getUpdateTime() + ", province=" + getProvince() + ", provinceId=" + getProvinceId() + ", city=" + getCity() + ", cityId=" + getCityId() + ", county=" + getCounty() + ", countyId=" + getCountyId() + ", shengId=" + getShengId() + ", shiId=" + getShiId() + ", quId=" + getQuId() + ", sfkns=" + getSfkns() + ", terminalId=" + getTerminalId() + ", zkzh=" + getZkzh() + ", ksh=" + getKsh() + ", updateStatus=" + getUpdateStatus() + ", rxnyName=" + getRxnyName() + ", studentIds=" + Arrays.deepToString(getStudentIds()) + ", num=" + getNum() + ", interfaceUserId=" + getInterfaceUserId() + ", csrqName=" + getCsrqName() + ", createTimeName=" + getCreateTimeName() + ", updateTimeName=" + getUpdateTimeName() + ", bjIds=" + getBjIds() + ")";
    }

    public VeBaseStudent setBjIds(List<Integer> bjIds)
    {
        this.bjIds = bjIds;return this;
    }

    public VeBaseStudent setUpdateTimeName(String updateTimeName)
    {
        this.updateTimeName = updateTimeName;return this;
    }

    public VeBaseStudent setCreateTimeName(String createTimeName)
    {
        this.createTimeName = createTimeName;return this;
    }

    public VeBaseStudent setCsrqName(String csrqName)
    {
        this.csrqName = csrqName;return this;
    }

    public VeBaseStudent setInterfaceUserId(String interfaceUserId)
    {
        this.interfaceUserId = interfaceUserId;return this;
    }

    public VeBaseStudent setNum(Integer num)
    {
        this.num = num;return this;
    }

    public VeBaseStudent setStudentIds(String[] studentIds)
    {
        this.studentIds = studentIds;return this;
    }

    public VeBaseStudent setRxnyName(String rxnyName)
    {
        this.rxnyName = rxnyName;return this;
    }

    public VeBaseStudent setUpdateStatus(Integer updateStatus)
    {
        this.updateStatus = updateStatus;return this;
    }

    public VeBaseStudent setKsh(String ksh)
    {
        this.ksh = ksh;return this;
    }

    public VeBaseStudent setZkzh(String zkzh)
    {
        this.zkzh = zkzh;return this;
    }

    public VeBaseStudent setTerminalId(Integer terminalId)
    {
        this.terminalId = terminalId;return this;
    }

    public VeBaseStudent setSfkns(Integer sfkns)
    {
        this.sfkns = sfkns;return this;
    }

    public VeBaseStudent setQuId(Integer quId)
    {
        this.quId = quId;return this;
    }

    public VeBaseStudent setShiId(Integer shiId)
    {
        this.shiId = shiId;return this;
    }

    public VeBaseStudent setShengId(Integer shengId)
    {
        this.shengId = shengId;return this;
    }

    public VeBaseStudent setCountyId(Integer countyId)
    {
        this.countyId = countyId;return this;
    }

    public VeBaseStudent setCounty(String county)
    {
        this.county = county;return this;
    }

    public VeBaseStudent setCityId(Integer cityId)
    {
        this.cityId = cityId;return this;
    }

    public VeBaseStudent setCity(String city)
    {
        this.city = city;return this;
    }

    public VeBaseStudent setProvinceId(Integer provinceId)
    {
        this.provinceId = provinceId;return this;
    }

    public VeBaseStudent setProvince(String province)
    {
        this.province = province;return this;
    }

    public VeBaseStudent setUpdateTime(Integer updateTime)
    {
        this.updateTime = updateTime;return this;
    }

    public VeBaseStudent setCreateTime(Integer createTime)
    {
        this.createTime = createTime;return this;
    }

    public VeBaseStudent setGradeId(Integer gradeId)
    {
        this.gradeId = gradeId;return this;
    }

    public VeBaseStudent setBjId(Integer bjId)
    {
        this.bjId = bjId;return this;
    }

    public VeBaseStudent setSpecId(Integer specId)
    {
        this.specId = specId;return this;
    }

    public VeBaseStudent setFalId(Integer falId)
    {
        this.falId = falId;return this;
    }

    public VeBaseStudent setXz(Integer xz)
    {
        this.xz = xz;return this;
    }

    public VeBaseStudent setRxny(Long rxny)
    {
        this.rxny = rxny;return this;
    }

    public VeBaseStudent setXsdqztm(String xsdqztm)
    {
        this.xsdqztm = xsdqztm;return this;
    }

    public VeBaseStudent setJdfs(Integer jdfs)
    {
        this.jdfs = jdfs;return this;
    }

    public VeBaseStudent setBmh(String bmh)
    {
        this.bmh = bmh;return this;
    }

    public VeBaseStudent setMzm(String mzm)
    {
        this.mzm = mzm;return this;
    }

    public VeBaseStudent setUserId(String userId)
    {
        this.userId = userId;return this;
    }

    public VeBaseStudent setXbm(String xbm)
    {
        this.xbm = xbm;return this;
    }

    public VeBaseStudent setXm(String xm)
    {
        this.xm = xm;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $jdfs = getJdfs();result = result * 59 + ($jdfs == null ? 43 : $jdfs.hashCode());Object $rxny = getRxny();result = result * 59 + ($rxny == null ? 43 : $rxny.hashCode());Object $xz = getXz();result = result * 59 + ($xz == null ? 43 : $xz.hashCode());Object $falId = getFalId();result = result * 59 + ($falId == null ? 43 : $falId.hashCode());Object $specId = getSpecId();result = result * 59 + ($specId == null ? 43 : $specId.hashCode());Object $bjId = getBjId();result = result * 59 + ($bjId == null ? 43 : $bjId.hashCode());Object $gradeId = getGradeId();result = result * 59 + ($gradeId == null ? 43 : $gradeId.hashCode());Object $createTime = getCreateTime();result = result * 59 + ($createTime == null ? 43 : $createTime.hashCode());Object $updateTime = getUpdateTime();result = result * 59 + ($updateTime == null ? 43 : $updateTime.hashCode());Object $provinceId = getProvinceId();result = result * 59 + ($provinceId == null ? 43 : $provinceId.hashCode());Object $cityId = getCityId();result = result * 59 + ($cityId == null ? 43 : $cityId.hashCode());Object $countyId = getCountyId();result = result * 59 + ($countyId == null ? 43 : $countyId.hashCode());Object $shengId = getShengId();result = result * 59 + ($shengId == null ? 43 : $shengId.hashCode());Object $shiId = getShiId();result = result * 59 + ($shiId == null ? 43 : $shiId.hashCode());Object $quId = getQuId();result = result * 59 + ($quId == null ? 43 : $quId.hashCode());Object $sfkns = getSfkns();result = result * 59 + ($sfkns == null ? 43 : $sfkns.hashCode());Object $terminalId = getTerminalId();result = result * 59 + ($terminalId == null ? 43 : $terminalId.hashCode());Object $updateStatus = getUpdateStatus();result = result * 59 + ($updateStatus == null ? 43 : $updateStatus.hashCode());Object $num = getNum();result = result * 59 + ($num == null ? 43 : $num.hashCode());Object $sfzh = getSfzh();result = result * 59 + ($sfzh == null ? 43 : $sfzh.hashCode());Object $xh = getXh();result = result * 59 + ($xh == null ? 43 : $xh.hashCode());Object $xm = getXm();result = result * 59 + ($xm == null ? 43 : $xm.hashCode());Object $xbm = getXbm();result = result * 59 + ($xbm == null ? 43 : $xbm.hashCode());Object $userId = getUserId();result = result * 59 + ($userId == null ? 43 : $userId.hashCode());Object $mzm = getMzm();result = result * 59 + ($mzm == null ? 43 : $mzm.hashCode());Object $bmh = getBmh();result = result * 59 + ($bmh == null ? 43 : $bmh.hashCode());Object $xsdqztm = getXsdqztm();result = result * 59 + ($xsdqztm == null ? 43 : $xsdqztm.hashCode());Object $province = getProvince();result = result * 59 + ($province == null ? 43 : $province.hashCode());Object $city = getCity();result = result * 59 + ($city == null ? 43 : $city.hashCode());Object $county = getCounty();result = result * 59 + ($county == null ? 43 : $county.hashCode());Object $zkzh = getZkzh();result = result * 59 + ($zkzh == null ? 43 : $zkzh.hashCode());Object $ksh = getKsh();result = result * 59 + ($ksh == null ? 43 : $ksh.hashCode());Object $rxnyName = getRxnyName();result = result * 59 + ($rxnyName == null ? 43 : $rxnyName.hashCode());result = result * 59 + Arrays.deepHashCode(getStudentIds());Object $interfaceUserId = getInterfaceUserId();result = result * 59 + ($interfaceUserId == null ? 43 : $interfaceUserId.hashCode());Object $csrqName = getCsrqName();result = result * 59 + ($csrqName == null ? 43 : $csrqName.hashCode());Object $createTimeName = getCreateTimeName();result = result * 59 + ($createTimeName == null ? 43 : $createTimeName.hashCode());Object $updateTimeName = getUpdateTimeName();result = result * 59 + ($updateTimeName == null ? 43 : $updateTimeName.hashCode());Object $bjIds = getBjIds();result = result * 59 + ($bjIds == null ? 43 : $bjIds.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseStudent;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseStudent)) {
            return false;
        }
        VeBaseStudent other = (VeBaseStudent)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$jdfs = getJdfs();Object other$jdfs = other.getJdfs();
        if (this$jdfs == null ? other$jdfs != null : !this$jdfs.equals(other$jdfs)) {
            return false;
        }
        Object this$rxny = getRxny();Object other$rxny = other.getRxny();
        if (this$rxny == null ? other$rxny != null : !this$rxny.equals(other$rxny)) {
            return false;
        }
        Object this$xz = getXz();Object other$xz = other.getXz();
        if (this$xz == null ? other$xz != null : !this$xz.equals(other$xz)) {
            return false;
        }
        Object this$falId = getFalId();Object other$falId = other.getFalId();
        if (this$falId == null ? other$falId != null : !this$falId.equals(other$falId)) {
            return false;
        }
        Object this$specId = getSpecId();Object other$specId = other.getSpecId();
        if (this$specId == null ? other$specId != null : !this$specId.equals(other$specId)) {
            return false;
        }
        Object this$bjId = getBjId();Object other$bjId = other.getBjId();
        if (this$bjId == null ? other$bjId != null : !this$bjId.equals(other$bjId)) {
            return false;
        }
        Object this$gradeId = getGradeId();Object other$gradeId = other.getGradeId();
        if (this$gradeId == null ? other$gradeId != null : !this$gradeId.equals(other$gradeId)) {
            return false;
        }
        Object this$createTime = getCreateTime();Object other$createTime = other.getCreateTime();
        if (this$createTime == null ? other$createTime != null : !this$createTime.equals(other$createTime)) {
            return false;
        }
        Object this$updateTime = getUpdateTime();Object other$updateTime = other.getUpdateTime();
        if (this$updateTime == null ? other$updateTime != null : !this$updateTime.equals(other$updateTime)) {
            return false;
        }
        Object this$provinceId = getProvinceId();Object other$provinceId = other.getProvinceId();
        if (this$provinceId == null ? other$provinceId != null : !this$provinceId.equals(other$provinceId)) {
            return false;
        }
        Object this$cityId = getCityId();Object other$cityId = other.getCityId();
        if (this$cityId == null ? other$cityId != null : !this$cityId.equals(other$cityId)) {
            return false;
        }
        Object this$countyId = getCountyId();Object other$countyId = other.getCountyId();
        if (this$countyId == null ? other$countyId != null : !this$countyId.equals(other$countyId)) {
            return false;
        }
        Object this$shengId = getShengId();Object other$shengId = other.getShengId();
        if (this$shengId == null ? other$shengId != null : !this$shengId.equals(other$shengId)) {
            return false;
        }
        Object this$shiId = getShiId();Object other$shiId = other.getShiId();
        if (this$shiId == null ? other$shiId != null : !this$shiId.equals(other$shiId)) {
            return false;
        }
        Object this$quId = getQuId();Object other$quId = other.getQuId();
        if (this$quId == null ? other$quId != null : !this$quId.equals(other$quId)) {
            return false;
        }
        Object this$sfkns = getSfkns();Object other$sfkns = other.getSfkns();
        if (this$sfkns == null ? other$sfkns != null : !this$sfkns.equals(other$sfkns)) {
            return false;
        }
        Object this$terminalId = getTerminalId();Object other$terminalId = other.getTerminalId();
        if (this$terminalId == null ? other$terminalId != null : !this$terminalId.equals(other$terminalId)) {
            return false;
        }
        Object this$updateStatus = getUpdateStatus();Object other$updateStatus = other.getUpdateStatus();
        if (this$updateStatus == null ? other$updateStatus != null : !this$updateStatus.equals(other$updateStatus)) {
            return false;
        }
        Object this$num = getNum();Object other$num = other.getNum();
        if (this$num == null ? other$num != null : !this$num.equals(other$num)) {
            return false;
        }
        Object this$sfzh = getSfzh();Object other$sfzh = other.getSfzh();
        if (this$sfzh == null ? other$sfzh != null : !this$sfzh.equals(other$sfzh)) {
            return false;
        }
        Object this$xh = getXh();Object other$xh = other.getXh();
        if (this$xh == null ? other$xh != null : !this$xh.equals(other$xh)) {
            return false;
        }
        Object this$xm = getXm();Object other$xm = other.getXm();
        if (this$xm == null ? other$xm != null : !this$xm.equals(other$xm)) {
            return false;
        }
        Object this$xbm = getXbm();Object other$xbm = other.getXbm();
        if (this$xbm == null ? other$xbm != null : !this$xbm.equals(other$xbm)) {
            return false;
        }
        Object this$userId = getUserId();Object other$userId = other.getUserId();
        if (this$userId == null ? other$userId != null : !this$userId.equals(other$userId)) {
            return false;
        }
        Object this$mzm = getMzm();Object other$mzm = other.getMzm();
        if (this$mzm == null ? other$mzm != null : !this$mzm.equals(other$mzm)) {
            return false;
        }
        Object this$bmh = getBmh();Object other$bmh = other.getBmh();
        if (this$bmh == null ? other$bmh != null : !this$bmh.equals(other$bmh)) {
            return false;
        }
        Object this$xsdqztm = getXsdqztm();Object other$xsdqztm = other.getXsdqztm();
        if (this$xsdqztm == null ? other$xsdqztm != null : !this$xsdqztm.equals(other$xsdqztm)) {
            return false;
        }
        Object this$province = getProvince();Object other$province = other.getProvince();
        if (this$province == null ? other$province != null : !this$province.equals(other$province)) {
            return false;
        }
        Object this$city = getCity();Object other$city = other.getCity();
        if (this$city == null ? other$city != null : !this$city.equals(other$city)) {
            return false;
        }
        Object this$county = getCounty();Object other$county = other.getCounty();
        if (this$county == null ? other$county != null : !this$county.equals(other$county)) {
            return false;
        }
        Object this$zkzh = getZkzh();Object other$zkzh = other.getZkzh();
        if (this$zkzh == null ? other$zkzh != null : !this$zkzh.equals(other$zkzh)) {
            return false;
        }
        Object this$ksh = getKsh();Object other$ksh = other.getKsh();
        if (this$ksh == null ? other$ksh != null : !this$ksh.equals(other$ksh)) {
            return false;
        }
        Object this$rxnyName = getRxnyName();Object other$rxnyName = other.getRxnyName();
        if (this$rxnyName == null ? other$rxnyName != null : !this$rxnyName.equals(other$rxnyName)) {
            return false;
        }
        if (!Arrays.deepEquals(getStudentIds(), other.getStudentIds())) {
            return false;
        }
        Object this$interfaceUserId = getInterfaceUserId();Object other$interfaceUserId = other.getInterfaceUserId();
        if (this$interfaceUserId == null ? other$interfaceUserId != null : !this$interfaceUserId.equals(other$interfaceUserId)) {
            return false;
        }
        Object this$csrqName = getCsrqName();Object other$csrqName = other.getCsrqName();
        if (this$csrqName == null ? other$csrqName != null : !this$csrqName.equals(other$csrqName)) {
            return false;
        }
        Object this$createTimeName = getCreateTimeName();Object other$createTimeName = other.getCreateTimeName();
        if (this$createTimeName == null ? other$createTimeName != null : !this$createTimeName.equals(other$createTimeName)) {
            return false;
        }
        Object this$updateTimeName = getUpdateTimeName();Object other$updateTimeName = other.getUpdateTimeName();
        if (this$updateTimeName == null ? other$updateTimeName != null : !this$updateTimeName.equals(other$updateTimeName)) {
            return false;
        }
        Object this$bjIds = getBjIds();Object other$bjIds = other.getBjIds();return this$bjIds == null ? other$bjIds == null : this$bjIds.equals(other$bjIds);
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getSfzh()
    {
        return this.sfzh;
    }

    public String getXh()
    {
        return this.xh;
    }

    public String getXm()
    {
        return this.xm;
    }

    public String getXbm()
    {
        return this.xbm;
    }

    public String getUserId()
    {
        return this.userId;
    }

    public String getMzm()
    {
        return this.mzm;
    }

    public String getBmh()
    {
        return this.bmh;
    }

    public Integer getJdfs()
    {
        return this.jdfs;
    }

    public String getXsdqztm()
    {
        return this.xsdqztm;
    }

    public Long getRxny()
    {
        return this.rxny;
    }

    public Integer getXz()
    {
        return this.xz;
    }

    public Integer getFalId()
    {
        return this.falId;
    }

    public Integer getSpecId()
    {
        return this.specId;
    }

    public Integer getBjId()
    {
        return this.bjId;
    }

    public Integer getGradeId()
    {
        return this.gradeId;
    }

    public Integer getCreateTime()
    {
        return this.createTime;
    }

    public Integer getUpdateTime()
    {
        return this.updateTime;
    }

    public String getProvince()
    {
        return this.province;
    }

    public Integer getProvinceId()
    {
        return this.provinceId;
    }

    public String getCity()
    {
        return this.city;
    }

    public Integer getCityId()
    {
        return this.cityId;
    }

    public String getCounty()
    {
        return this.county;
    }

    public Integer getCountyId()
    {
        return this.countyId;
    }

    public Integer getShengId()
    {
        return this.shengId;
    }

    public Integer getShiId()
    {
        return this.shiId;
    }

    public Integer getQuId()
    {
        return this.quId;
    }

    public Integer getSfkns()
    {
        return this.sfkns;
    }

    public Integer getTerminalId()
    {
        return this.terminalId;
    }

    public String getZkzh()
    {
        return this.zkzh;
    }

    public String getKsh()
    {
        return this.ksh;
    }

    public Integer getUpdateStatus()
    {
        return this.updateStatus;
    }

    public String getRxnyName()
    {
        return this.rxnyName;
    }

    public String[] getStudentIds()
    {
        return this.studentIds;
    }

    public Integer getNum()
    {
        return this.num;
    }

    public String getInterfaceUserId()
    {
        return this.interfaceUserId;
    }

    public String getCsrqName()
    {
        return this.csrqName;
    }

    public String getCreateTimeName()
    {
        return this.createTimeName;
    }

    public String getUpdateTimeName()
    {
        return this.updateTimeName;
    }

    public List<Integer> getBjIds()
    {
        return this.bjIds;
    }
}
