package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseGrade;
import org.edu.modules.common.mapper.VeBaseGradeMapper;
import org.edu.modules.common.service.IVeBaseGradeService;
import org.edu.modules.common.util.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseGradeServiceImpl
        extends ServiceImpl<VeBaseGradeMapper, VeBaseGrade>
        implements IVeBaseGradeService
{
    @Autowired
    private VeBaseGradeMapper veBaseGradeMapper;

    public List<Map<String, Object>> getGradePageList(VeBaseGrade veBaseGrade)
    {
        List<Map<String, Object>> list = this.veBaseGradeMapper.getGradePageList(veBaseGrade);
        list = setRxnfName(list);
        return list;
    }

    public VeBaseGrade getGradeByName(Integer id, String name)
    {
        VeBaseGrade veBaseGrade = this.veBaseGradeMapper.getGradeByName(id, name);
        if (veBaseGrade != null) {
            if (veBaseGrade.getBmjzrq() != null) {
                veBaseGrade.setBmjzrqName(DateTimeUtil.timestampToDate(veBaseGrade.getBmjzrq().intValue()));
            } else {
                veBaseGrade.setBmjzrqName("");
            }
        }
        return veBaseGrade;
    }

    public VeBaseGrade getGradeByCode(Integer id, String code)
    {
        VeBaseGrade veBaseGrade = this.veBaseGradeMapper.getGradeByCode(id, code);
        if (veBaseGrade != null) {
            if (veBaseGrade.getBmjzrq() != null) {
                veBaseGrade.setBmjzrqName(DateTimeUtil.timestampToDate(veBaseGrade.getBmjzrq().intValue()));
            } else {
                veBaseGrade.setBmjzrqName("");
            }
        }
        return veBaseGrade;
    }

    public VeBaseGrade getGradeByRxnf(Integer id, Integer rxnf)
    {
        VeBaseGrade veBaseGrade = this.veBaseGradeMapper.getGradeByRxnf(id, rxnf);
        if (veBaseGrade != null) {
            if (veBaseGrade.getBmjzrq() != null) {
                veBaseGrade.setBmjzrqName(DateTimeUtil.timestampToDate(veBaseGrade.getBmjzrq().intValue()));
            } else {
                veBaseGrade.setBmjzrqName("");
            }
        }
        return veBaseGrade;
    }

    private List<Map<String, Object>> setRxnfName(List<Map<String, Object>> list)
    {
        if (list.size() > 0) {
            for (Map map : list) {
                if ((!"".equals(map.get("bmjzrq"))) && (map.get("bmjzrq") != null))
                {
                    Long bmjzrq = Long.valueOf(Long.parseLong(map.get("bmjzrq").toString()));
                    map.put("bmjzrqName", DateTimeUtil.timestampToDate(bmjzrq.longValue()));
                }
                else
                {
                    map.put("bmjzrqName", "");
                }
            }
        }
        return list;
    }
}

