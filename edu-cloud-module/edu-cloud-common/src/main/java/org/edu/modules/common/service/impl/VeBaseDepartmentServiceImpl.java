package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.common.entity.VeBaseDepartment;
import org.edu.modules.common.mapper.VeBaseDepartmentMapper;
import org.edu.modules.common.service.IVeBaseDepartmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-12-13 16:24
 */
@Service
public class VeBaseDepartmentServiceImpl extends ServiceImpl<VeBaseDepartmentMapper, VeBaseDepartment> implements IVeBaseDepartmentService {

    @Autowired
    private VeBaseDepartmentMapper veBaseDepartmentMapper;

    @Override
    public List<VeBaseDepartment> getTreeList() {
        return null;
    }

    @Override
    public List<VeBaseDepartment> getDepartmentAndTeacherList() {
        return null;
    }

    @Override
    public VeBaseDepartment getDepartmentByName(Integer paramInteger, String paramString) {
        return veBaseDepartmentMapper.getDepartmentByName(paramInteger, paramString);
    }

    @Override
    public VeBaseDepartment getDepartmentByCode(Integer paramInteger, String paramString) {
        return veBaseDepartmentMapper.getDepartmentByCode(paramInteger, paramString);
    }
}
