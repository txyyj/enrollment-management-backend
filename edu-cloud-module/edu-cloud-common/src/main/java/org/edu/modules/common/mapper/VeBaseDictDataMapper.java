package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.edu.modules.common.entity.VeBaseDictData;

public abstract interface VeBaseDictDataMapper
        extends BaseMapper<VeBaseDictData>
{
    public abstract List<VeBaseDictData> getDictDataByModelCode(String paramString1, String paramString2);

    public abstract List<VeBaseDictData> getDictDataByModelCodeTitle(String paramString1, String paramString2);
}
