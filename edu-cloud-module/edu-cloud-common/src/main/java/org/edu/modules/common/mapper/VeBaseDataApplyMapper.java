package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import org.edu.modules.common.entity.VeBaseDataApply;

@Mapper
public abstract interface VeBaseDataApplyMapper
        extends BaseMapper<VeBaseDataApply>
{
    public abstract List<Map<String, Object>> queryDataApplyPageList(VeBaseDataApply paramVeBaseDataApply);

    public abstract List<Map<String, Object>> queryNotCheckDataApplyPageList(VeBaseDataApply paramVeBaseDataApply);

    public abstract List<Map<String, Object>> queryDataApplyProcessedPageList(VeBaseDataApply paramVeBaseDataApply);

    public abstract List<Map<String, Object>> queryDataApplyUserId(String paramString);
}
