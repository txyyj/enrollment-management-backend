package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseDictionary;

public abstract interface VeBaseDictionaryMapper
        extends BaseMapper<VeBaseDictionary>
{
    public abstract List<VeBaseDictionary> getDictionaryListByCode(String paramString);

    public abstract List<Map<String, Object>> getDictDataListByCode(String paramString);

    public abstract List<VeBaseDictionary> getRootList();

    public abstract List<VeBaseDictionary> getBodyList();
}
