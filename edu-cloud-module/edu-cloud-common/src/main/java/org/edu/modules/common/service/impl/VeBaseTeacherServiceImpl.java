package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.text.DecimalFormat;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseTeacher;
import org.edu.modules.common.mapper.VeBaseTeacherMapper;
import org.edu.modules.common.service.IVeBaseTeacherService;
import org.edu.modules.common.util.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseTeacherServiceImpl
        extends ServiceImpl<VeBaseTeacherMapper, VeBaseTeacher>
        implements IVeBaseTeacherService
{
    @Autowired
    private VeBaseTeacherMapper veBaseTeacherMapper;

    public List<Map<String, Object>> getTeacherPageList(VeBaseTeacher veBaseTeacher)
    {
        List<Map<String, Object>> list = this.veBaseTeacherMapper.getTeacherPageList(veBaseTeacher);
        if (list.size() > 0) {
            for (Map map : list)
            {
                if ((!"".equals(map.get("csrq"))) && (map.get("csrq") != null)) {
                    map.put("csrqName", DateTimeUtil.timestampToDate(Long.parseLong(map.get("csrq").toString())));
                } else {
                    map.put("csrqName", "");
                }
                if ((!"".equals(map.get("cjgzny"))) && (map.get("cjgzny") != null)) {
                    map.put("cjgznyName", DateTimeUtil.timestampToDate(Long.parseLong(map.get("cjgzny").toString())));
                } else {
                    map.put("cjgznyName", "");
                }
                if ((!"".equals(map.get("cjny"))) && (map.get("cjny") != null)) {
                    map.put("cjnyName", DateTimeUtil.timestampToDate(Long.parseLong(map.get("cjny").toString())));
                } else {
                    map.put("cjnyName", "");
                }
                if ((!"".equals(map.get("lxny"))) && (map.get("lxny") != null)) {
                    map.put("lxnyName", DateTimeUtil.timestampToDate(Long.parseLong(map.get("lxny").toString())));
                } else {
                    map.put("lxnyName", "");
                }
                if ((!"".equals(map.get("rdDate"))) && (map.get("rdDate") != null)) {
                    map.put("rdName", DateTimeUtil.timestampToDate(Long.parseLong(map.get("rdDate").toString())));
                } else {
                    map.put("rdName", "");
                }
            }
        }
        return list;
    }

    public IPage<Map<String, Object>> getTeacherPageListByIPage(Page page, VeBaseTeacher veBaseTeacher)
    {
        IPage<Map<String, Object>> list = this.veBaseTeacherMapper.getTeacherPageListByIPage(page, veBaseTeacher);
        if (list.getRecords().size() > 0) {
            for (Map map : list.getRecords())
            {
                if ((!"".equals(map.get("csrq"))) && (map.get("csrq") != null)) {
                    map.put("csrqName", DateTimeUtil.timestampToDate(Long.parseLong(map.get("csrq").toString())));
                } else {
                    map.put("csrqName", "");
                }
                if ((!"".equals(map.get("cjgzny"))) && (map.get("cjgzny") != null)) {
                    map.put("cjgznyName", DateTimeUtil.timestampToDate(Long.parseLong(map.get("cjgzny").toString())));
                } else {
                    map.put("cjgznyName", "");
                }
                if ((!"".equals(map.get("cjny"))) && (map.get("cjny") != null)) {
                    map.put("cjnyName", DateTimeUtil.timestampToDate(Long.parseLong(map.get("cjny").toString())));
                } else {
                    map.put("cjnyName", "");
                }
                if ((!"".equals(map.get("lxny"))) && (map.get("lxny") != null)) {
                    map.put("lxnyName", DateTimeUtil.timestampToDate(Long.parseLong(map.get("lxny").toString())));
                } else {
                    map.put("lxnyName", "");
                }
                if ((!"".equals(map.get("rdDate"))) && (map.get("rdDate") != null)) {
                    map.put("rdName", DateTimeUtil.timestampToDate(Long.parseLong(map.get("rdDate").toString())));
                } else {
                    map.put("rdName", "");
                }
            }
        }
        return list;
    }

    public Map getTeacherSexStatistics()
    {
        Map map = this.veBaseTeacherMapper.getTeacherSexStatistics();

        Long manNumber = Long.valueOf(map.get("manNumber").toString());

        Long womanNumber = Long.valueOf(map.get("womanNumber").toString());

        Long otherNumber = Long.valueOf(map.get("otherNumber").toString());
        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        Double manRate = Double.valueOf(manNumber.longValue() * 1.0D / (manNumber.longValue() + womanNumber.longValue() + otherNumber.longValue()));
        map.put("manRate", decimalFormat.format(manRate.doubleValue() * 100.0D) + "%");

        Double womanRate = Double.valueOf(womanNumber.longValue() * 1.0D / (manNumber.longValue() + womanNumber.longValue() + otherNumber.longValue()));
        map.put("womanRate", decimalFormat.format(womanRate.doubleValue() * 100.0D) + "%");

        Double otherRate = Double.valueOf(otherNumber.longValue() * 1.0D / (manNumber.longValue() + womanNumber.longValue() + otherNumber.longValue()));
        map.put("otherRate", decimalFormat.format(otherRate.doubleValue() * 100.0D) + "%");
        return map;
    }

    public Map getTeacherAgeStatistics()
    {
        Map map = this.veBaseTeacherMapper.getTeacherAgeStatistics();

        Long oneIndex = Long.valueOf(map.get("oneIndex").toString());

        Long twoIndex = Long.valueOf(map.get("twoIndex").toString());

        Long threeIndex = Long.valueOf(map.get("threeIndex").toString());

        Long fourIndex = Long.valueOf(map.get("fourIndex").toString());
        DecimalFormat decimalFormat = new DecimalFormat("0.00");

        Double oneRate = Double.valueOf(oneIndex.longValue() * 1.0D / (oneIndex.longValue() + twoIndex.longValue() + threeIndex.longValue() + fourIndex.longValue()));
        map.put("oneRate", decimalFormat.format(oneRate.doubleValue() * 100.0D) + "%");

        Double twoRate = Double.valueOf(twoIndex.longValue() * 1.0D / (oneIndex.longValue() + twoIndex.longValue() + threeIndex.longValue() + fourIndex.longValue()));
        map.put("twoRate", decimalFormat.format(twoRate.doubleValue() * 100.0D) + "%");

        Double threeRate = Double.valueOf(threeIndex.longValue() * 1.0D / (oneIndex.longValue() + twoIndex.longValue() + threeIndex.longValue() + fourIndex.longValue()));
        map.put("threeRate", decimalFormat.format(threeRate.doubleValue() * 100.0D) + "%");

        Double fourRate = Double.valueOf(fourIndex.longValue() * 1.0D / (oneIndex.longValue() + twoIndex.longValue() + threeIndex.longValue() + fourIndex.longValue()));
        map.put("fourRate", decimalFormat.format(fourRate.doubleValue() * 100.0D) + "%");
        return map;
    }

    public VeBaseTeacher getByGH(Integer id, String gh)
    {
        return this.veBaseTeacherMapper.getByGH(id, gh);
    }

    public VeBaseTeacher getByUserId(String userId)
    {
        return this.veBaseTeacherMapper.getByUserId(userId);
    }

    public List<VeBaseTeacher> getTeacherListBySearch(VeBaseTeacher veBaseTeacher)
    {
        return this.veBaseTeacherMapper.getTeacherListBySearch(veBaseTeacher);
    }

    public int stopTeacherByUserId(String userId)
    {
        return this.veBaseTeacherMapper.stopTeacherByUserId(userId);
    }

    public VeBaseTeacher queryBzrByBanjiId(Integer banjiId)
    {
        return this.veBaseTeacherMapper.queryBzrByBanjiId(banjiId);
    }
}

