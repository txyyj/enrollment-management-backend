package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.common.entity.VeBaseAppUser;
import org.edu.modules.common.mapper.VeBaseAppUserMapper;
import org.edu.modules.common.service.IVeBaseAppUserService;
import org.edu.modules.common.vo.VoAppUserBySearch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Auther 李少君
 * @Date 2021-12-13 16:23
 */
@Service
public class VeBaseAppUserServiceImpl extends ServiceImpl<VeBaseAppUserMapper, VeBaseAppUser> implements IVeBaseAppUserService {

    @Autowired
    private VeBaseAppUserMapper veBaseAppUserMapper;

    @Override
    public int updateAppUserPasswordById(String paramString1, String paramString2, String paramString3) {

        return veBaseAppUserMapper.updateAppUserPasswordById(paramString1, paramString2, paramString3);
    }

    @Override
    public int addSysUser(VeBaseAppUser paramVeBaseAppUser) {

        return veBaseAppUserMapper.addSysUser(paramVeBaseAppUser);
    }

    @Override
    public VeBaseAppUser getPwdAndSalt(VeBaseAppUser paramVeBaseAppUser) {
        return null;
    }

    @Override
    public String pwdVerify(String paramString) {
        return null;
    }

    @Override
    public VeBaseAppUser getAppUserByUserId(String paramString) {

        return veBaseAppUserMapper.getAppUserByUserId(paramString);
    }

    @Override
    public Map getSysRoleUser(String paramString, Integer paramInteger) {

        return veBaseAppUserMapper.getSysRoleUser(paramString, paramInteger);
    }

    @Override
    public int addSysRoleUser(String paramString, Integer paramInteger) {

        return veBaseAppUserMapper.addSysRoleUser(paramString, paramInteger);
    }

    @Override
    public List<Map<String, Object>> queryOrganUserTreeList(String paramString) {
        return null;
    }

    @Override
    public List<Map<String, Object>> getAppUserBySearch(VoAppUserBySearch paramVoAppUserBySearch) {
        return veBaseAppUserMapper.getAppUserBySearch(paramVoAppUserBySearch);
    }

    @Override
    public int updateAppUserByUserId(String paramString) {
        return veBaseAppUserMapper.updateAppUserByUserId(paramString);
    }

    @Override
    public int updateSysUserDel(String paramString) {
        return veBaseAppUserMapper.updateSysUserDel(paramString);
    }

    @Override
    public int deleteOrgan(String paramString) {
        return veBaseAppUserMapper.deleteOrgan(paramString);
    }

    @Override
    public int deleteRole(String paramString) {
        return veBaseAppUserMapper.deleteRole(paramString);
    }

    @Override
    public List<Map<String, Object>> queryResourcePageList() {
        return veBaseAppUserMapper.queryResourcePageList();
    }

    @Override
    public IPage<Map<String, Object>> getDataListByTableName(Page paramPage, String paramString) {
        return veBaseAppUserMapper.getDataListByTableName(paramPage, paramString);
    }

    @Override
    public List<Map<String, Object>> queryColumnNameListByTableName(String paramString) {
        return veBaseAppUserMapper.queryColumnNameListByTableName(paramString);
    }

    @Override
    public List<Map<String, Object>> querySysRoleList() {
        return veBaseAppUserMapper.querySysRoleList();
    }

    @Override
    public List<Map<String, Object>> queryAppUserTeacherList() {
        return veBaseAppUserMapper.queryAppUserTeacherList();
    }

    @Override
    public int stopAppUserByUserId(String paramString) {
        return veBaseAppUserMapper.stopAppUserByUserId(paramString);
    }

    @Override
    public List<Map<String, Object>> queryAppManageList() {
        return veBaseAppUserMapper.queryAppManageList();
    }

    @Override
    public List<Map<String, Object>> getUnreadMessageCountUserId() {
        return veBaseAppUserMapper.getUnreadMessageCountUserId();
    }

    @Override
    public List<Map<String, Object>> queryAppUserListByRoleId(Integer paramInteger) {
        return veBaseAppUserMapper.queryAppUserListByRoleId(paramInteger);
    }
}
