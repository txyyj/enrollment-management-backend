package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Arrays;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_teacher")
@ApiModel(value="ve_base_teacher对象", description="教师档案表")
public class VeBaseTeacher
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @ApiModelProperty("教师用户id")
    private String userId;
    @Excel(name="工号", width=15.0D)
    @ApiModelProperty("工号")
    private String gh;
    @Excel(name="姓名", width=15.0D)
    @ApiModelProperty("姓名")
    private String xm;
    @Excel(name="曾用名", width=15.0D)
    @ApiModelProperty("曾用名")
    private String cym;
    @Excel(name="身份证件类型码", width=15.0D)
    @ApiModelProperty("身份证件类型码 （数据字典）")
    private String sfzjlxm;
    @Excel(name="身份证号", width=15.0D)
    @ApiModelProperty("身份证件号")
    private String sfzjh;
    @ApiModelProperty("出生日期")
    private Integer csrq;
    @ApiModelProperty("部门id")
    private Integer depId;
    @ApiModelProperty("教研组ID")
    private Integer jyzId;
    @Excel(name="性别", width=15.0D)
    @ApiModelProperty("性别码")
    private String xbm;
    @Excel(name="民族", width=15.0D)
    @ApiModelProperty("民族码")
    private String mzm;
    @Excel(name="健康状况码", width=15.0D)
    @ApiModelProperty("健康状况码")
    private String jkzkm;
    @Excel(name="政治面貌", width=15.0D)
    @ApiModelProperty("政治面貌码")
    private String zzmmm;
    @Excel(name="港澳台侨外码", width=15.0D)
    @ApiModelProperty("港澳台侨外码")
    private String gatqwm;
    @Excel(name="籍贯", width=15.0D)
    @ApiModelProperty("籍贯")
    private String jg;
    @Excel(name="是否是流动人口", width=15.0D)
    @ApiModelProperty("是否是流动人口")
    private String sfsldrk;
    @Excel(name="户口类别码", width=15.0D)
    @ApiModelProperty("户口类别码")
    private String hklbm;
    @Excel(name="当前住址", width=15.0D)
    @ApiModelProperty("当前住址")
    private String dqzz;
    @Excel(name="当前住址邮政编码 ", width=15.0D)
    @ApiModelProperty("当前住址邮政编码 ")
    private String dqzzyzbm;
    @ApiModelProperty("参加工作年月")
    private Integer cjgzny;
    @ApiModelProperty("从教年月")
    private Integer cjny;
    @ApiModelProperty("来校年月")
    private Integer lxny;
    @Excel(name="编制属性", width=15.0D)
    @ApiModelProperty("编制类别码")
    private String bzlbm;
    @Excel(name="职务码", width=15.0D)
    @ApiModelProperty("职务码")
    private String zwm;
    @Excel(name="教职工类别", width=15.0D)
    @ApiModelProperty("教职工类别码")
    private String jzglbm;
    @Excel(name="岗位类别码", width=15.0D)
    @ApiModelProperty("岗位类别码")
    private String gwlbm;
    @Excel(name="是否兼职教师", width=15.0D)
    @ApiModelProperty("是否兼职教师")
    private Integer sfjzjs;
    @Excel(name="是否双师型教师", width=15.0D)
    @ApiModelProperty("是否双师型教师")
    private Integer sfssxjs;
    @ApiModelProperty("照片")
    private String zp;
    @Excel(name="联系电话", width=15.0D)
    @ApiModelProperty("联系电话")
    private String lxdh;
    @Excel(name="电子信箱", width=15.0D)
    @ApiModelProperty("联系邮箱")
    private String dzxx;
    @Excel(name="邮政编码", width=15.0D)
    @ApiModelProperty("邮政编码")
    private String yzbm;
    @Excel(name="员工状态", width=15.0D)
    @ApiModelProperty("当前状态码 11默认在职")
    private String dqztm;
    @ApiModelProperty("状态;1可用2不可用")
    private Integer status;
    @Excel(name="系统ID", width=15.0D)
    @ApiModelProperty("系统ID")
    private Integer terminalid;
    @Excel(name="省市级专业带头人", width=15.0D)
    @ApiModelProperty("省市级专业带头人(0否1是)")
    private Integer ssjzydtrStatus;
    @Excel(name="骨干教师", width=15.0D)
    @ApiModelProperty("骨干教师(0否1是)")
    private Integer ggjsStatus;
    @Excel(name="高级专业技术职务教师", width=15.0D)
    @ApiModelProperty("高级专业技术职务教师(0否1是)")
    private Integer gjzyjsjsStatus;
    @Excel(name="国籍", width=15.0D)
    @ApiModelProperty("国籍名称")
    private String nationality;
    @Excel(name="全日制学历", width=15.0D)
    @ApiModelProperty("全日制学历")
    private String educationType;
    @Excel(name="最高学历", width=15.0D)
    @ApiModelProperty("最高学历")
    private String zgxl;
    @ApiModelProperty("入党时间")
    private Integer rdDate;
    @ApiModelProperty("双肩挑(0否1是)")
    private Integer sjtStatus;
    @Excel(name="职务状态", width=15.0D)
    @ApiModelProperty("职务状态(字典表)")
    private String zwztType;
    @Excel(name="职称类别", width=15.0D)
    @ApiModelProperty("职称类别")
    private String zclbType;
    @Excel(name="在编类型", width=15.0D)
    @ApiModelProperty("在编类型")
    private String zblxType;
    @Excel(name="序号", width=15.0D)
    @ApiModelProperty("序号")
    private Integer serialNumber;
    @Excel(name="是否删除", width=15.0D)
    @ApiModelProperty("是否删除(0正常, -1已删除)")
    private Integer isDeleted;
    @Excel(name="出生日期", width=15.0D)
    @TableField(exist=false)
    private String csrqName;
    @Excel(name="参加工作年月", width=15.0D)
    @TableField(exist=false)
    private String cjgznyName;
    @Excel(name="从教年月", width=15.0D)
    @TableField(exist=false)
    private String cjnyName;
    @Excel(name="来校年月", width=15.0D)
    @TableField(exist=false)
    private String lxnyName;
    @Excel(name="入党时间", width=15.0D)
    @TableField(exist=false)
    private String rdName;
    @Excel(name="所属部门", width=15.0D)
    @TableField(exist=false)
    private String depName;
    @Excel(name="所属专业组", width=15.0D)
    @TableField(exist=false)
    private String jyzName;
    @Excel(name="双肩挑", width=15.0D)
    @TableField(exist=false)
    private String sjtStatusName;
    @TableField(exist=false)
    private String interfaceUserId;
    @TableField(exist=false)
    private String[] teacherIds;
    @TableField(exist=false)
    private Integer roleId;

    public VeBaseTeacher setGh(String gh)
    {
        this.gh = gh;return this;
    }

    public VeBaseTeacher setUserId(String userId)
    {
        this.userId = userId;return this;
    }

    public VeBaseTeacher setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseTeacher(id=" + getId() + ", userId=" + getUserId() + ", gh=" + getGh() + ", xm=" + getXm() + ", cym=" + getCym() + ", sfzjlxm=" + getSfzjlxm() + ", sfzjh=" + getSfzjh() + ", csrq=" + getCsrq() + ", depId=" + getDepId() + ", jyzId=" + getJyzId() + ", xbm=" + getXbm() + ", mzm=" + getMzm() + ", jkzkm=" + getJkzkm() + ", zzmmm=" + getZzmmm() + ", gatqwm=" + getGatqwm() + ", jg=" + getJg() + ", sfsldrk=" + getSfsldrk() + ", hklbm=" + getHklbm() + ", dqzz=" + getDqzz() + ", dqzzyzbm=" + getDqzzyzbm() + ", cjgzny=" + getCjgzny() + ", cjny=" + getCjny() + ", lxny=" + getLxny() + ", bzlbm=" + getBzlbm() + ", zwm=" + getZwm() + ", jzglbm=" + getJzglbm() + ", gwlbm=" + getGwlbm() + ", sfjzjs=" + getSfjzjs() + ", sfssxjs=" + getSfssxjs() + ", zp=" + getZp() + ", lxdh=" + getLxdh() + ", dzxx=" + getDzxx() + ", yzbm=" + getYzbm() + ", dqztm=" + getDqztm() + ", status=" + getStatus() + ", terminalid=" + getTerminalid() + ", ssjzydtrStatus=" + getSsjzydtrStatus() + ", ggjsStatus=" + getGgjsStatus() + ", gjzyjsjsStatus=" + getGjzyjsjsStatus() + ", nationality=" + getNationality() + ", educationType=" + getEducationType() + ", zgxl=" + getZgxl() + ", rdDate=" + getRdDate() + ", sjtStatus=" + getSjtStatus() + ", zwztType=" + getZwztType() + ", zclbType=" + getZclbType() + ", zblxType=" + getZblxType() + ", serialNumber=" + getSerialNumber() + ", isDeleted=" + getIsDeleted() + ", csrqName=" + getCsrqName() + ", cjgznyName=" + getCjgznyName() + ", cjnyName=" + getCjnyName() + ", lxnyName=" + getLxnyName() + ", rdName=" + getRdName() + ", depName=" + getDepName() + ", jyzName=" + getJyzName() + ", sjtStatusName=" + getSjtStatusName() + ", interfaceUserId=" + getInterfaceUserId() + ", teacherIds=" + Arrays.deepToString(getTeacherIds()) + ", roleId=" + getRoleId() + ")";
    }

    public VeBaseTeacher setRoleId(Integer roleId)
    {
        this.roleId = roleId;return this;
    }

    public VeBaseTeacher setTeacherIds(String[] teacherIds)
    {
        this.teacherIds = teacherIds;return this;
    }

    public VeBaseTeacher setInterfaceUserId(String interfaceUserId)
    {
        this.interfaceUserId = interfaceUserId;return this;
    }

    public VeBaseTeacher setSjtStatusName(String sjtStatusName)
    {
        this.sjtStatusName = sjtStatusName;return this;
    }

    public VeBaseTeacher setJyzName(String jyzName)
    {
        this.jyzName = jyzName;return this;
    }

    public VeBaseTeacher setDepName(String depName)
    {
        this.depName = depName;return this;
    }

    public VeBaseTeacher setRdName(String rdName)
    {
        this.rdName = rdName;return this;
    }

    public VeBaseTeacher setLxnyName(String lxnyName)
    {
        this.lxnyName = lxnyName;return this;
    }

    public VeBaseTeacher setCjnyName(String cjnyName)
    {
        this.cjnyName = cjnyName;return this;
    }

    public VeBaseTeacher setCjgznyName(String cjgznyName)
    {
        this.cjgznyName = cjgznyName;return this;
    }

    public VeBaseTeacher setCsrqName(String csrqName)
    {
        this.csrqName = csrqName;return this;
    }

    public VeBaseTeacher setIsDeleted(Integer isDeleted)
    {
        this.isDeleted = isDeleted;return this;
    }

    public VeBaseTeacher setSerialNumber(Integer serialNumber)
    {
        this.serialNumber = serialNumber;return this;
    }

    public VeBaseTeacher setZblxType(String zblxType)
    {
        this.zblxType = zblxType;return this;
    }

    public VeBaseTeacher setZclbType(String zclbType)
    {
        this.zclbType = zclbType;return this;
    }

    public VeBaseTeacher setZwztType(String zwztType)
    {
        this.zwztType = zwztType;return this;
    }

    public VeBaseTeacher setSjtStatus(Integer sjtStatus)
    {
        this.sjtStatus = sjtStatus;return this;
    }

    public VeBaseTeacher setRdDate(Integer rdDate)
    {
        this.rdDate = rdDate;return this;
    }

    public VeBaseTeacher setZgxl(String zgxl)
    {
        this.zgxl = zgxl;return this;
    }

    public VeBaseTeacher setEducationType(String educationType)
    {
        this.educationType = educationType;return this;
    }

    public VeBaseTeacher setNationality(String nationality)
    {
        this.nationality = nationality;return this;
    }

    public VeBaseTeacher setGjzyjsjsStatus(Integer gjzyjsjsStatus)
    {
        this.gjzyjsjsStatus = gjzyjsjsStatus;return this;
    }

    public VeBaseTeacher setGgjsStatus(Integer ggjsStatus)
    {
        this.ggjsStatus = ggjsStatus;return this;
    }

    public VeBaseTeacher setSsjzydtrStatus(Integer ssjzydtrStatus)
    {
        this.ssjzydtrStatus = ssjzydtrStatus;return this;
    }

    public VeBaseTeacher setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeBaseTeacher setStatus(Integer status)
    {
        this.status = status;return this;
    }

    public VeBaseTeacher setDqztm(String dqztm)
    {
        this.dqztm = dqztm;return this;
    }

    public VeBaseTeacher setYzbm(String yzbm)
    {
        this.yzbm = yzbm;return this;
    }

    public VeBaseTeacher setDzxx(String dzxx)
    {
        this.dzxx = dzxx;return this;
    }

    public VeBaseTeacher setLxdh(String lxdh)
    {
        this.lxdh = lxdh;return this;
    }

    public VeBaseTeacher setZp(String zp)
    {
        this.zp = zp;return this;
    }

    public VeBaseTeacher setSfssxjs(Integer sfssxjs)
    {
        this.sfssxjs = sfssxjs;return this;
    }

    public VeBaseTeacher setSfjzjs(Integer sfjzjs)
    {
        this.sfjzjs = sfjzjs;return this;
    }

    public VeBaseTeacher setGwlbm(String gwlbm)
    {
        this.gwlbm = gwlbm;return this;
    }

    public VeBaseTeacher setJzglbm(String jzglbm)
    {
        this.jzglbm = jzglbm;return this;
    }

    public VeBaseTeacher setZwm(String zwm)
    {
        this.zwm = zwm;return this;
    }

    public VeBaseTeacher setBzlbm(String bzlbm)
    {
        this.bzlbm = bzlbm;return this;
    }

    public VeBaseTeacher setLxny(Integer lxny)
    {
        this.lxny = lxny;return this;
    }

    public VeBaseTeacher setCjny(Integer cjny)
    {
        this.cjny = cjny;return this;
    }

    public VeBaseTeacher setCjgzny(Integer cjgzny)
    {
        this.cjgzny = cjgzny;return this;
    }

    public VeBaseTeacher setDqzzyzbm(String dqzzyzbm)
    {
        this.dqzzyzbm = dqzzyzbm;return this;
    }

    public VeBaseTeacher setDqzz(String dqzz)
    {
        this.dqzz = dqzz;return this;
    }

    public VeBaseTeacher setHklbm(String hklbm)
    {
        this.hklbm = hklbm;return this;
    }

    public VeBaseTeacher setSfsldrk(String sfsldrk)
    {
        this.sfsldrk = sfsldrk;return this;
    }

    public VeBaseTeacher setJg(String jg)
    {
        this.jg = jg;return this;
    }

    public VeBaseTeacher setGatqwm(String gatqwm)
    {
        this.gatqwm = gatqwm;return this;
    }

    public VeBaseTeacher setZzmmm(String zzmmm)
    {
        this.zzmmm = zzmmm;return this;
    }

    public VeBaseTeacher setJkzkm(String jkzkm)
    {
        this.jkzkm = jkzkm;return this;
    }

    public VeBaseTeacher setMzm(String mzm)
    {
        this.mzm = mzm;return this;
    }

    public VeBaseTeacher setXbm(String xbm)
    {
        this.xbm = xbm;return this;
    }

    public VeBaseTeacher setJyzId(Integer jyzId)
    {
        this.jyzId = jyzId;return this;
    }

    public VeBaseTeacher setDepId(Integer depId)
    {
        this.depId = depId;return this;
    }

    public VeBaseTeacher setCsrq(Integer csrq)
    {
        this.csrq = csrq;return this;
    }

    public VeBaseTeacher setSfzjh(String sfzjh)
    {
        this.sfzjh = sfzjh;return this;
    }

    public VeBaseTeacher setSfzjlxm(String sfzjlxm)
    {
        this.sfzjlxm = sfzjlxm;return this;
    }

    public VeBaseTeacher setCym(String cym)
    {
        this.cym = cym;return this;
    }

    public VeBaseTeacher setXm(String xm)
    {
        this.xm = xm;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $csrq = getCsrq();result = result * 59 + ($csrq == null ? 43 : $csrq.hashCode());Object $depId = getDepId();result = result * 59 + ($depId == null ? 43 : $depId.hashCode());Object $jyzId = getJyzId();result = result * 59 + ($jyzId == null ? 43 : $jyzId.hashCode());Object $cjgzny = getCjgzny();result = result * 59 + ($cjgzny == null ? 43 : $cjgzny.hashCode());Object $cjny = getCjny();result = result * 59 + ($cjny == null ? 43 : $cjny.hashCode());Object $lxny = getLxny();result = result * 59 + ($lxny == null ? 43 : $lxny.hashCode());Object $sfjzjs = getSfjzjs();result = result * 59 + ($sfjzjs == null ? 43 : $sfjzjs.hashCode());Object $sfssxjs = getSfssxjs();result = result * 59 + ($sfssxjs == null ? 43 : $sfssxjs.hashCode());Object $status = getStatus();result = result * 59 + ($status == null ? 43 : $status.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $ssjzydtrStatus = getSsjzydtrStatus();result = result * 59 + ($ssjzydtrStatus == null ? 43 : $ssjzydtrStatus.hashCode());Object $ggjsStatus = getGgjsStatus();result = result * 59 + ($ggjsStatus == null ? 43 : $ggjsStatus.hashCode());Object $gjzyjsjsStatus = getGjzyjsjsStatus();result = result * 59 + ($gjzyjsjsStatus == null ? 43 : $gjzyjsjsStatus.hashCode());Object $rdDate = getRdDate();result = result * 59 + ($rdDate == null ? 43 : $rdDate.hashCode());Object $sjtStatus = getSjtStatus();result = result * 59 + ($sjtStatus == null ? 43 : $sjtStatus.hashCode());Object $serialNumber = getSerialNumber();result = result * 59 + ($serialNumber == null ? 43 : $serialNumber.hashCode());Object $isDeleted = getIsDeleted();result = result * 59 + ($isDeleted == null ? 43 : $isDeleted.hashCode());Object $roleId = getRoleId();result = result * 59 + ($roleId == null ? 43 : $roleId.hashCode());Object $userId = getUserId();result = result * 59 + ($userId == null ? 43 : $userId.hashCode());Object $gh = getGh();result = result * 59 + ($gh == null ? 43 : $gh.hashCode());Object $xm = getXm();result = result * 59 + ($xm == null ? 43 : $xm.hashCode());Object $cym = getCym();result = result * 59 + ($cym == null ? 43 : $cym.hashCode());Object $sfzjlxm = getSfzjlxm();result = result * 59 + ($sfzjlxm == null ? 43 : $sfzjlxm.hashCode());Object $sfzjh = getSfzjh();result = result * 59 + ($sfzjh == null ? 43 : $sfzjh.hashCode());Object $xbm = getXbm();result = result * 59 + ($xbm == null ? 43 : $xbm.hashCode());Object $mzm = getMzm();result = result * 59 + ($mzm == null ? 43 : $mzm.hashCode());Object $jkzkm = getJkzkm();result = result * 59 + ($jkzkm == null ? 43 : $jkzkm.hashCode());Object $zzmmm = getZzmmm();result = result * 59 + ($zzmmm == null ? 43 : $zzmmm.hashCode());Object $gatqwm = getGatqwm();result = result * 59 + ($gatqwm == null ? 43 : $gatqwm.hashCode());Object $jg = getJg();result = result * 59 + ($jg == null ? 43 : $jg.hashCode());Object $sfsldrk = getSfsldrk();result = result * 59 + ($sfsldrk == null ? 43 : $sfsldrk.hashCode());Object $hklbm = getHklbm();result = result * 59 + ($hklbm == null ? 43 : $hklbm.hashCode());Object $dqzz = getDqzz();result = result * 59 + ($dqzz == null ? 43 : $dqzz.hashCode());Object $dqzzyzbm = getDqzzyzbm();result = result * 59 + ($dqzzyzbm == null ? 43 : $dqzzyzbm.hashCode());Object $bzlbm = getBzlbm();result = result * 59 + ($bzlbm == null ? 43 : $bzlbm.hashCode());Object $zwm = getZwm();result = result * 59 + ($zwm == null ? 43 : $zwm.hashCode());Object $jzglbm = getJzglbm();result = result * 59 + ($jzglbm == null ? 43 : $jzglbm.hashCode());Object $gwlbm = getGwlbm();result = result * 59 + ($gwlbm == null ? 43 : $gwlbm.hashCode());Object $zp = getZp();result = result * 59 + ($zp == null ? 43 : $zp.hashCode());Object $lxdh = getLxdh();result = result * 59 + ($lxdh == null ? 43 : $lxdh.hashCode());Object $dzxx = getDzxx();result = result * 59 + ($dzxx == null ? 43 : $dzxx.hashCode());Object $yzbm = getYzbm();result = result * 59 + ($yzbm == null ? 43 : $yzbm.hashCode());Object $dqztm = getDqztm();result = result * 59 + ($dqztm == null ? 43 : $dqztm.hashCode());Object $nationality = getNationality();result = result * 59 + ($nationality == null ? 43 : $nationality.hashCode());Object $educationType = getEducationType();result = result * 59 + ($educationType == null ? 43 : $educationType.hashCode());Object $zgxl = getZgxl();result = result * 59 + ($zgxl == null ? 43 : $zgxl.hashCode());Object $zwztType = getZwztType();result = result * 59 + ($zwztType == null ? 43 : $zwztType.hashCode());Object $zclbType = getZclbType();result = result * 59 + ($zclbType == null ? 43 : $zclbType.hashCode());Object $zblxType = getZblxType();result = result * 59 + ($zblxType == null ? 43 : $zblxType.hashCode());Object $csrqName = getCsrqName();result = result * 59 + ($csrqName == null ? 43 : $csrqName.hashCode());Object $cjgznyName = getCjgznyName();result = result * 59 + ($cjgznyName == null ? 43 : $cjgznyName.hashCode());Object $cjnyName = getCjnyName();result = result * 59 + ($cjnyName == null ? 43 : $cjnyName.hashCode());Object $lxnyName = getLxnyName();result = result * 59 + ($lxnyName == null ? 43 : $lxnyName.hashCode());Object $rdName = getRdName();result = result * 59 + ($rdName == null ? 43 : $rdName.hashCode());Object $depName = getDepName();result = result * 59 + ($depName == null ? 43 : $depName.hashCode());Object $jyzName = getJyzName();result = result * 59 + ($jyzName == null ? 43 : $jyzName.hashCode());Object $sjtStatusName = getSjtStatusName();result = result * 59 + ($sjtStatusName == null ? 43 : $sjtStatusName.hashCode());Object $interfaceUserId = getInterfaceUserId();result = result * 59 + ($interfaceUserId == null ? 43 : $interfaceUserId.hashCode());result = result * 59 + Arrays.deepHashCode(getTeacherIds());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseTeacher;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseTeacher)) {
            return false;
        }
        VeBaseTeacher other = (VeBaseTeacher)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$csrq = getCsrq();Object other$csrq = other.getCsrq();
        if (this$csrq == null ? other$csrq != null : !this$csrq.equals(other$csrq)) {
            return false;
        }
        Object this$depId = getDepId();Object other$depId = other.getDepId();
        if (this$depId == null ? other$depId != null : !this$depId.equals(other$depId)) {
            return false;
        }
        Object this$jyzId = getJyzId();Object other$jyzId = other.getJyzId();
        if (this$jyzId == null ? other$jyzId != null : !this$jyzId.equals(other$jyzId)) {
            return false;
        }
        Object this$cjgzny = getCjgzny();Object other$cjgzny = other.getCjgzny();
        if (this$cjgzny == null ? other$cjgzny != null : !this$cjgzny.equals(other$cjgzny)) {
            return false;
        }
        Object this$cjny = getCjny();Object other$cjny = other.getCjny();
        if (this$cjny == null ? other$cjny != null : !this$cjny.equals(other$cjny)) {
            return false;
        }
        Object this$lxny = getLxny();Object other$lxny = other.getLxny();
        if (this$lxny == null ? other$lxny != null : !this$lxny.equals(other$lxny)) {
            return false;
        }
        Object this$sfjzjs = getSfjzjs();Object other$sfjzjs = other.getSfjzjs();
        if (this$sfjzjs == null ? other$sfjzjs != null : !this$sfjzjs.equals(other$sfjzjs)) {
            return false;
        }
        Object this$sfssxjs = getSfssxjs();Object other$sfssxjs = other.getSfssxjs();
        if (this$sfssxjs == null ? other$sfssxjs != null : !this$sfssxjs.equals(other$sfssxjs)) {
            return false;
        }
        Object this$status = getStatus();Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$ssjzydtrStatus = getSsjzydtrStatus();Object other$ssjzydtrStatus = other.getSsjzydtrStatus();
        if (this$ssjzydtrStatus == null ? other$ssjzydtrStatus != null : !this$ssjzydtrStatus.equals(other$ssjzydtrStatus)) {
            return false;
        }
        Object this$ggjsStatus = getGgjsStatus();Object other$ggjsStatus = other.getGgjsStatus();
        if (this$ggjsStatus == null ? other$ggjsStatus != null : !this$ggjsStatus.equals(other$ggjsStatus)) {
            return false;
        }
        Object this$gjzyjsjsStatus = getGjzyjsjsStatus();Object other$gjzyjsjsStatus = other.getGjzyjsjsStatus();
        if (this$gjzyjsjsStatus == null ? other$gjzyjsjsStatus != null : !this$gjzyjsjsStatus.equals(other$gjzyjsjsStatus)) {
            return false;
        }
        Object this$rdDate = getRdDate();Object other$rdDate = other.getRdDate();
        if (this$rdDate == null ? other$rdDate != null : !this$rdDate.equals(other$rdDate)) {
            return false;
        }
        Object this$sjtStatus = getSjtStatus();Object other$sjtStatus = other.getSjtStatus();
        if (this$sjtStatus == null ? other$sjtStatus != null : !this$sjtStatus.equals(other$sjtStatus)) {
            return false;
        }
        Object this$serialNumber = getSerialNumber();Object other$serialNumber = other.getSerialNumber();
        if (this$serialNumber == null ? other$serialNumber != null : !this$serialNumber.equals(other$serialNumber)) {
            return false;
        }
        Object this$isDeleted = getIsDeleted();Object other$isDeleted = other.getIsDeleted();
        if (this$isDeleted == null ? other$isDeleted != null : !this$isDeleted.equals(other$isDeleted)) {
            return false;
        }
        Object this$roleId = getRoleId();Object other$roleId = other.getRoleId();
        if (this$roleId == null ? other$roleId != null : !this$roleId.equals(other$roleId)) {
            return false;
        }
        Object this$userId = getUserId();Object other$userId = other.getUserId();
        if (this$userId == null ? other$userId != null : !this$userId.equals(other$userId)) {
            return false;
        }
        Object this$gh = getGh();Object other$gh = other.getGh();
        if (this$gh == null ? other$gh != null : !this$gh.equals(other$gh)) {
            return false;
        }
        Object this$xm = getXm();Object other$xm = other.getXm();
        if (this$xm == null ? other$xm != null : !this$xm.equals(other$xm)) {
            return false;
        }
        Object this$cym = getCym();Object other$cym = other.getCym();
        if (this$cym == null ? other$cym != null : !this$cym.equals(other$cym)) {
            return false;
        }
        Object this$sfzjlxm = getSfzjlxm();Object other$sfzjlxm = other.getSfzjlxm();
        if (this$sfzjlxm == null ? other$sfzjlxm != null : !this$sfzjlxm.equals(other$sfzjlxm)) {
            return false;
        }
        Object this$sfzjh = getSfzjh();Object other$sfzjh = other.getSfzjh();
        if (this$sfzjh == null ? other$sfzjh != null : !this$sfzjh.equals(other$sfzjh)) {
            return false;
        }
        Object this$xbm = getXbm();Object other$xbm = other.getXbm();
        if (this$xbm == null ? other$xbm != null : !this$xbm.equals(other$xbm)) {
            return false;
        }
        Object this$mzm = getMzm();Object other$mzm = other.getMzm();
        if (this$mzm == null ? other$mzm != null : !this$mzm.equals(other$mzm)) {
            return false;
        }
        Object this$jkzkm = getJkzkm();Object other$jkzkm = other.getJkzkm();
        if (this$jkzkm == null ? other$jkzkm != null : !this$jkzkm.equals(other$jkzkm)) {
            return false;
        }
        Object this$zzmmm = getZzmmm();Object other$zzmmm = other.getZzmmm();
        if (this$zzmmm == null ? other$zzmmm != null : !this$zzmmm.equals(other$zzmmm)) {
            return false;
        }
        Object this$gatqwm = getGatqwm();Object other$gatqwm = other.getGatqwm();
        if (this$gatqwm == null ? other$gatqwm != null : !this$gatqwm.equals(other$gatqwm)) {
            return false;
        }
        Object this$jg = getJg();Object other$jg = other.getJg();
        if (this$jg == null ? other$jg != null : !this$jg.equals(other$jg)) {
            return false;
        }
        Object this$sfsldrk = getSfsldrk();Object other$sfsldrk = other.getSfsldrk();
        if (this$sfsldrk == null ? other$sfsldrk != null : !this$sfsldrk.equals(other$sfsldrk)) {
            return false;
        }
        Object this$hklbm = getHklbm();Object other$hklbm = other.getHklbm();
        if (this$hklbm == null ? other$hklbm != null : !this$hklbm.equals(other$hklbm)) {
            return false;
        }
        Object this$dqzz = getDqzz();Object other$dqzz = other.getDqzz();
        if (this$dqzz == null ? other$dqzz != null : !this$dqzz.equals(other$dqzz)) {
            return false;
        }
        Object this$dqzzyzbm = getDqzzyzbm();Object other$dqzzyzbm = other.getDqzzyzbm();
        if (this$dqzzyzbm == null ? other$dqzzyzbm != null : !this$dqzzyzbm.equals(other$dqzzyzbm)) {
            return false;
        }
        Object this$bzlbm = getBzlbm();Object other$bzlbm = other.getBzlbm();
        if (this$bzlbm == null ? other$bzlbm != null : !this$bzlbm.equals(other$bzlbm)) {
            return false;
        }
        Object this$zwm = getZwm();Object other$zwm = other.getZwm();
        if (this$zwm == null ? other$zwm != null : !this$zwm.equals(other$zwm)) {
            return false;
        }
        Object this$jzglbm = getJzglbm();Object other$jzglbm = other.getJzglbm();
        if (this$jzglbm == null ? other$jzglbm != null : !this$jzglbm.equals(other$jzglbm)) {
            return false;
        }
        Object this$gwlbm = getGwlbm();Object other$gwlbm = other.getGwlbm();
        if (this$gwlbm == null ? other$gwlbm != null : !this$gwlbm.equals(other$gwlbm)) {
            return false;
        }
        Object this$zp = getZp();Object other$zp = other.getZp();
        if (this$zp == null ? other$zp != null : !this$zp.equals(other$zp)) {
            return false;
        }
        Object this$lxdh = getLxdh();Object other$lxdh = other.getLxdh();
        if (this$lxdh == null ? other$lxdh != null : !this$lxdh.equals(other$lxdh)) {
            return false;
        }
        Object this$dzxx = getDzxx();Object other$dzxx = other.getDzxx();
        if (this$dzxx == null ? other$dzxx != null : !this$dzxx.equals(other$dzxx)) {
            return false;
        }
        Object this$yzbm = getYzbm();Object other$yzbm = other.getYzbm();
        if (this$yzbm == null ? other$yzbm != null : !this$yzbm.equals(other$yzbm)) {
            return false;
        }
        Object this$dqztm = getDqztm();Object other$dqztm = other.getDqztm();
        if (this$dqztm == null ? other$dqztm != null : !this$dqztm.equals(other$dqztm)) {
            return false;
        }
        Object this$nationality = getNationality();Object other$nationality = other.getNationality();
        if (this$nationality == null ? other$nationality != null : !this$nationality.equals(other$nationality)) {
            return false;
        }
        Object this$educationType = getEducationType();Object other$educationType = other.getEducationType();
        if (this$educationType == null ? other$educationType != null : !this$educationType.equals(other$educationType)) {
            return false;
        }
        Object this$zgxl = getZgxl();Object other$zgxl = other.getZgxl();
        if (this$zgxl == null ? other$zgxl != null : !this$zgxl.equals(other$zgxl)) {
            return false;
        }
        Object this$zwztType = getZwztType();Object other$zwztType = other.getZwztType();
        if (this$zwztType == null ? other$zwztType != null : !this$zwztType.equals(other$zwztType)) {
            return false;
        }
        Object this$zclbType = getZclbType();Object other$zclbType = other.getZclbType();
        if (this$zclbType == null ? other$zclbType != null : !this$zclbType.equals(other$zclbType)) {
            return false;
        }
        Object this$zblxType = getZblxType();Object other$zblxType = other.getZblxType();
        if (this$zblxType == null ? other$zblxType != null : !this$zblxType.equals(other$zblxType)) {
            return false;
        }
        Object this$csrqName = getCsrqName();Object other$csrqName = other.getCsrqName();
        if (this$csrqName == null ? other$csrqName != null : !this$csrqName.equals(other$csrqName)) {
            return false;
        }
        Object this$cjgznyName = getCjgznyName();Object other$cjgznyName = other.getCjgznyName();
        if (this$cjgznyName == null ? other$cjgznyName != null : !this$cjgznyName.equals(other$cjgznyName)) {
            return false;
        }
        Object this$cjnyName = getCjnyName();Object other$cjnyName = other.getCjnyName();
        if (this$cjnyName == null ? other$cjnyName != null : !this$cjnyName.equals(other$cjnyName)) {
            return false;
        }
        Object this$lxnyName = getLxnyName();Object other$lxnyName = other.getLxnyName();
        if (this$lxnyName == null ? other$lxnyName != null : !this$lxnyName.equals(other$lxnyName)) {
            return false;
        }
        Object this$rdName = getRdName();Object other$rdName = other.getRdName();
        if (this$rdName == null ? other$rdName != null : !this$rdName.equals(other$rdName)) {
            return false;
        }
        Object this$depName = getDepName();Object other$depName = other.getDepName();
        if (this$depName == null ? other$depName != null : !this$depName.equals(other$depName)) {
            return false;
        }
        Object this$jyzName = getJyzName();Object other$jyzName = other.getJyzName();
        if (this$jyzName == null ? other$jyzName != null : !this$jyzName.equals(other$jyzName)) {
            return false;
        }
        Object this$sjtStatusName = getSjtStatusName();Object other$sjtStatusName = other.getSjtStatusName();
        if (this$sjtStatusName == null ? other$sjtStatusName != null : !this$sjtStatusName.equals(other$sjtStatusName)) {
            return false;
        }
        Object this$interfaceUserId = getInterfaceUserId();Object other$interfaceUserId = other.getInterfaceUserId();
        if (this$interfaceUserId == null ? other$interfaceUserId != null : !this$interfaceUserId.equals(other$interfaceUserId)) {
            return false;
        }
        return Arrays.deepEquals(getTeacherIds(), other.getTeacherIds());
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getUserId()
    {
        return this.userId;
    }

    public String getGh()
    {
        return this.gh;
    }

    public String getXm()
    {
        return this.xm;
    }

    public String getCym()
    {
        return this.cym;
    }

    public String getSfzjlxm()
    {
        return this.sfzjlxm;
    }

    public String getSfzjh()
    {
        return this.sfzjh;
    }

    public Integer getCsrq()
    {
        return this.csrq;
    }

    public Integer getDepId()
    {
        return this.depId;
    }

    public Integer getJyzId()
    {
        return this.jyzId;
    }

    public String getXbm()
    {
        return this.xbm;
    }

    public String getMzm()
    {
        return this.mzm;
    }

    public String getJkzkm()
    {
        return this.jkzkm;
    }

    public String getZzmmm()
    {
        return this.zzmmm;
    }

    public String getGatqwm()
    {
        return this.gatqwm;
    }

    public String getJg()
    {
        return this.jg;
    }

    public String getSfsldrk()
    {
        return this.sfsldrk;
    }

    public String getHklbm()
    {
        return this.hklbm;
    }

    public String getDqzz()
    {
        return this.dqzz;
    }

    public String getDqzzyzbm()
    {
        return this.dqzzyzbm;
    }

    public Integer getCjgzny()
    {
        return this.cjgzny;
    }

    public Integer getCjny()
    {
        return this.cjny;
    }

    public Integer getLxny()
    {
        return this.lxny;
    }

    public String getBzlbm()
    {
        return this.bzlbm;
    }

    public String getZwm()
    {
        return this.zwm;
    }

    public String getJzglbm()
    {
        return this.jzglbm;
    }

    public String getGwlbm()
    {
        return this.gwlbm;
    }

    public Integer getSfjzjs()
    {
        return this.sfjzjs;
    }

    public Integer getSfssxjs()
    {
        return this.sfssxjs;
    }

    public String getZp()
    {
        return this.zp;
    }

    public String getLxdh()
    {
        return this.lxdh;
    }

    public String getDzxx()
    {
        return this.dzxx;
    }

    public String getYzbm()
    {
        return this.yzbm;
    }

    public String getDqztm()
    {
        return this.dqztm;
    }

    public Integer getStatus()
    {
        return this.status;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public Integer getSsjzydtrStatus()
    {
        return this.ssjzydtrStatus;
    }

    public Integer getGgjsStatus()
    {
        return this.ggjsStatus;
    }

    public Integer getGjzyjsjsStatus()
    {
        return this.gjzyjsjsStatus;
    }

    public String getNationality()
    {
        return this.nationality;
    }

    public String getEducationType()
    {
        return this.educationType;
    }

    public String getZgxl()
    {
        return this.zgxl;
    }

    public Integer getRdDate()
    {
        return this.rdDate;
    }

    public Integer getSjtStatus()
    {
        return this.sjtStatus;
    }

    public String getZwztType()
    {
        return this.zwztType;
    }

    public String getZclbType()
    {
        return this.zclbType;
    }

    public String getZblxType()
    {
        return this.zblxType;
    }

    public Integer getSerialNumber()
    {
        return this.serialNumber;
    }

    public Integer getIsDeleted()
    {
        return this.isDeleted;
    }

    public String getCsrqName()
    {
        return this.csrqName;
    }

    public String getCjgznyName()
    {
        return this.cjgznyName;
    }

    public String getCjnyName()
    {
        return this.cjnyName;
    }

    public String getLxnyName()
    {
        return this.lxnyName;
    }

    public String getRdName()
    {
        return this.rdName;
    }

    public String getDepName()
    {
        return this.depName;
    }

    public String getJyzName()
    {
        return this.jyzName;
    }

    public String getSjtStatusName()
    {
        return this.sjtStatusName;
    }

    public String getInterfaceUserId()
    {
        return this.interfaceUserId;
    }

    public String[] getTeacherIds()
    {
        return this.teacherIds;
    }

    public Integer getRoleId()
    {
        return this.roleId;
    }
}
