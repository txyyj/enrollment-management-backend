package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseXueZhi;

public abstract interface VeBaseXueZhiMapper
        extends BaseMapper<VeBaseXueZhi>
{
    public abstract List<Map<String, Object>> getXueZhiPageList(VeBaseXueZhi paramVeBaseXueZhi);

    public abstract VeBaseXueZhi getXueZhiByName(Integer paramInteger, String paramString);
}
