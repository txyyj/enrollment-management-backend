package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Options;
import org.edu.modules.common.entity.VeBaseStudent;

public abstract interface VeBaseStudentMapper
        extends BaseMapper<VeBaseStudent>
{
    public abstract VeBaseStudent getModelById(Integer paramInteger);

    public abstract List<Map<String, Object>> getStudentPageList(VeBaseStudent paramVeBaseStudent);

    public abstract Map getStudentStatusStatistics();

    public abstract VeBaseStudent getModelByUserId(String paramString);

    public abstract List<Map<String, Object>> getModelByName(String paramString);

    @Options(useGeneratedKeys=true, keyProperty="id", keyColumn="id")
    public abstract int addStudent(VeBaseStudent paramVeBaseStudent);

    public abstract VeBaseStudent getStudentBySFZH(Integer paramInteger, String paramString);

    public abstract VeBaseStudent getStudentByXH(Integer paramInteger, String paramString);

    public abstract Map getStudentAndInfoById(Integer paramInteger);

    public abstract List<Map<String, Object>> getStudentStatisticsByYear(String paramString);
}
