package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_interface_user")
@ApiModel(value="ve_base_interface_user对象", description="接口用户表")
public class VeBaseInterfaceUser
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="用户名", width=15.0D)
    @ApiModelProperty("用户名")
    private String username;
    @Excel(name="密码", width=15.0D)
    @ApiModelProperty("密码")
    private String password;
    @Excel(name="令牌", width=15.0D)
    @ApiModelProperty("令牌")
    private String token;
    @Excel(name="创建时间", width=15.0D)
    @ApiModelProperty("创建时间")
    private Date createtime;
    @Excel(name="标识", width=15.0D)
    @ApiModelProperty("标识")
    private String flag;
    @Excel(name="备注", width=15.0D)
    @ApiModelProperty("备注")
    private String remark;
    @TableField(exist=false)
    @ApiModelProperty("接口id,分割字符串")
    private String interfaceIds;

    public VeBaseInterfaceUser setPassword(String password)
    {
        this.password = password;return this;
    }

    public VeBaseInterfaceUser setUsername(String username)
    {
        this.username = username;return this;
    }

    public VeBaseInterfaceUser setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseInterfaceUser(id=" + getId() + ", username=" + getUsername() + ", password=" + getPassword() + ", token=" + getToken() + ", createtime=" + getCreatetime() + ", flag=" + getFlag() + ", remark=" + getRemark() + ", interfaceIds=" + getInterfaceIds() + ")";
    }

    public VeBaseInterfaceUser setInterfaceIds(String interfaceIds)
    {
        this.interfaceIds = interfaceIds;return this;
    }

    public VeBaseInterfaceUser setRemark(String remark)
    {
        this.remark = remark;return this;
    }

    public VeBaseInterfaceUser setFlag(String flag)
    {
        this.flag = flag;return this;
    }

    public VeBaseInterfaceUser setCreatetime(Date createtime)
    {
        this.createtime = createtime;return this;
    }

    public VeBaseInterfaceUser setToken(String token)
    {
        this.token = token;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $username = getUsername();result = result * 59 + ($username == null ? 43 : $username.hashCode());Object $password = getPassword();result = result * 59 + ($password == null ? 43 : $password.hashCode());Object $token = getToken();result = result * 59 + ($token == null ? 43 : $token.hashCode());Object $createtime = getCreatetime();result = result * 59 + ($createtime == null ? 43 : $createtime.hashCode());Object $flag = getFlag();result = result * 59 + ($flag == null ? 43 : $flag.hashCode());Object $remark = getRemark();result = result * 59 + ($remark == null ? 43 : $remark.hashCode());Object $interfaceIds = getInterfaceIds();result = result * 59 + ($interfaceIds == null ? 43 : $interfaceIds.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseInterfaceUser;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseInterfaceUser)) {
            return false;
        }
        VeBaseInterfaceUser other = (VeBaseInterfaceUser)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$username = getUsername();Object other$username = other.getUsername();
        if (this$username == null ? other$username != null : !this$username.equals(other$username)) {
            return false;
        }
        Object this$password = getPassword();Object other$password = other.getPassword();
        if (this$password == null ? other$password != null : !this$password.equals(other$password)) {
            return false;
        }
        Object this$token = getToken();Object other$token = other.getToken();
        if (this$token == null ? other$token != null : !this$token.equals(other$token)) {
            return false;
        }
        Object this$createtime = getCreatetime();Object other$createtime = other.getCreatetime();
        if (this$createtime == null ? other$createtime != null : !this$createtime.equals(other$createtime)) {
            return false;
        }
        Object this$flag = getFlag();Object other$flag = other.getFlag();
        if (this$flag == null ? other$flag != null : !this$flag.equals(other$flag)) {
            return false;
        }
        Object this$remark = getRemark();Object other$remark = other.getRemark();
        if (this$remark == null ? other$remark != null : !this$remark.equals(other$remark)) {
            return false;
        }
        Object this$interfaceIds = getInterfaceIds();Object other$interfaceIds = other.getInterfaceIds();return this$interfaceIds == null ? other$interfaceIds == null : this$interfaceIds.equals(other$interfaceIds);
    }

    public String getId()
    {
        return this.id;
    }

    public String getUsername()
    {
        return this.username;
    }

    public String getPassword()
    {
        return this.password;
    }

    public String getToken()
    {
        return this.token;
    }

    public Date getCreatetime()
    {
        return this.createtime;
    }

    public String getFlag()
    {
        return this.flag;
    }

    public String getRemark()
    {
        return this.remark;
    }

    public String getInterfaceIds()
    {
        return this.interfaceIds;
    }
}
