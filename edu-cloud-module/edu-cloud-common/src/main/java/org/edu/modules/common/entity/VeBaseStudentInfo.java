package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_student_info")
@ApiModel(value="ve_base_student_info对象", description="学生基本信息表")
public class VeBaseStudentInfo
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("stuId")
    private Integer stuId;
    @Excel(name="曾用名", width=15.0D)
    @ApiModelProperty("曾用名")
    private String cym;
    @Excel(name="出生日期", width=15.0D)
    @ApiModelProperty("出生日期")
    private Integer csrq;
    @Excel(name="籍贯", width=15.0D)
    @ApiModelProperty("籍贯")
    private String jg;
    @Excel(name="健康状况码", width=15.0D)
    @ApiModelProperty("健康状况码")
    private String jkzkm;
    @Excel(name="政治面貌码", width=15.0D)
    @ApiModelProperty("政治面貌码")
    private String zzmmm;
    @Excel(name="户口类别码", width=15.0D)
    @ApiModelProperty("户口类别码")
    private String hklbm;
    @Excel(name="是否是流动", width=15.0D)
    @ApiModelProperty("是否是流动")
    private String sfsldrk;
    @Excel(name="家庭地址", width=15.0D)
    @ApiModelProperty("家庭地址")
    private String jtdz;
    @Excel(name="家庭联系电话", width=15.0D)
    @ApiModelProperty("家庭联系电话")
    private String jtlxdh;
    @Excel(name="特长", width=15.0D)
    @ApiModelProperty("特长")
    private String tc;
    @Excel(name="学生联系电话", width=15.0D)
    @ApiModelProperty("学生联系电话")
    private String xslxdh;
    @Excel(name="电子信箱", width=15.0D)
    @ApiModelProperty("电子信箱")
    private String dzxx;
    @Excel(name="照片", width=15.0D)
    @ApiModelProperty("照片")
    private String zp;
    @Excel(name="毕业照片", width=15.0D)
    @ApiModelProperty("毕业照片")
    private String byzp;
    @Excel(name="即时通讯号", width=15.0D)
    @ApiModelProperty("即时通讯号")
    private String jstxh;
    @Excel(name="是否低保", width=15.0D)
    @ApiModelProperty("是否低保0否1是")
    private Integer sfdb;
    @Excel(name="终端系统ID", width=15.0D)
    @ApiModelProperty("终端系统ID")
    private Integer terminalid;
    @Excel(name="毕业学校", width=15.0D)
    @ApiModelProperty("毕业学校")
    private String byxx;
    @Excel(name="报名方式", width=15.0D)
    @ApiModelProperty("报名方式")
    private String bmfsm;
    @Excel(name="入学成绩", width=15.0D)
    @ApiModelProperty("入学成绩")
    private Double rxcj;
    @Excel(name="家庭地址是否公开", width=15.0D)
    @ApiModelProperty("家庭地址是否公开1公开2不公开")
    private Integer jtdzstatus;
    @Excel(name="联系电话是否公开", width=15.0D)
    @ApiModelProperty("联系电话是否公开 1公开2不公开")
    private Integer xslxdhstatus;
    @Excel(name="更新时间", width=15.0D)
    @ApiModelProperty("更新时间")
    private Integer updateTime;

    public VeBaseStudentInfo setCsrq(Integer csrq)
    {
        this.csrq = csrq;return this;
    }

    public VeBaseStudentInfo setCym(String cym)
    {
        this.cym = cym;return this;
    }

    public VeBaseStudentInfo setStuId(Integer stuId)
    {
        this.stuId = stuId;return this;
    }

    public String toString()
    {
        return "VeBaseStudentInfo(stuId=" + getStuId() + ", cym=" + getCym() + ", csrq=" + getCsrq() + ", jg=" + getJg() + ", jkzkm=" + getJkzkm() + ", zzmmm=" + getZzmmm() + ", hklbm=" + getHklbm() + ", sfsldrk=" + getSfsldrk() + ", jtdz=" + getJtdz() + ", jtlxdh=" + getJtlxdh() + ", tc=" + getTc() + ", xslxdh=" + getXslxdh() + ", dzxx=" + getDzxx() + ", zp=" + getZp() + ", byzp=" + getByzp() + ", jstxh=" + getJstxh() + ", sfdb=" + getSfdb() + ", terminalid=" + getTerminalid() + ", byxx=" + getByxx() + ", bmfsm=" + getBmfsm() + ", rxcj=" + getRxcj() + ", jtdzstatus=" + getJtdzstatus() + ", xslxdhstatus=" + getXslxdhstatus() + ", updateTime=" + getUpdateTime() + ")";
    }

    public VeBaseStudentInfo setUpdateTime(Integer updateTime)
    {
        this.updateTime = updateTime;return this;
    }

    public VeBaseStudentInfo setXslxdhstatus(Integer xslxdhstatus)
    {
        this.xslxdhstatus = xslxdhstatus;return this;
    }

    public VeBaseStudentInfo setJtdzstatus(Integer jtdzstatus)
    {
        this.jtdzstatus = jtdzstatus;return this;
    }

    public VeBaseStudentInfo setRxcj(Double rxcj)
    {
        this.rxcj = rxcj;return this;
    }

    public VeBaseStudentInfo setBmfsm(String bmfsm)
    {
        this.bmfsm = bmfsm;return this;
    }

    public VeBaseStudentInfo setByxx(String byxx)
    {
        this.byxx = byxx;return this;
    }

    public VeBaseStudentInfo setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeBaseStudentInfo setSfdb(Integer sfdb)
    {
        this.sfdb = sfdb;return this;
    }

    public VeBaseStudentInfo setJstxh(String jstxh)
    {
        this.jstxh = jstxh;return this;
    }

    public VeBaseStudentInfo setByzp(String byzp)
    {
        this.byzp = byzp;return this;
    }

    public VeBaseStudentInfo setZp(String zp)
    {
        this.zp = zp;return this;
    }

    public VeBaseStudentInfo setDzxx(String dzxx)
    {
        this.dzxx = dzxx;return this;
    }

    public VeBaseStudentInfo setXslxdh(String xslxdh)
    {
        this.xslxdh = xslxdh;return this;
    }

    public VeBaseStudentInfo setTc(String tc)
    {
        this.tc = tc;return this;
    }

    public VeBaseStudentInfo setJtlxdh(String jtlxdh)
    {
        this.jtlxdh = jtlxdh;return this;
    }

    public VeBaseStudentInfo setJtdz(String jtdz)
    {
        this.jtdz = jtdz;return this;
    }

    public VeBaseStudentInfo setSfsldrk(String sfsldrk)
    {
        this.sfsldrk = sfsldrk;return this;
    }

    public VeBaseStudentInfo setHklbm(String hklbm)
    {
        this.hklbm = hklbm;return this;
    }

    public VeBaseStudentInfo setZzmmm(String zzmmm)
    {
        this.zzmmm = zzmmm;return this;
    }

    public VeBaseStudentInfo setJkzkm(String jkzkm)
    {
        this.jkzkm = jkzkm;return this;
    }

    public VeBaseStudentInfo setJg(String jg)
    {
        this.jg = jg;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $stuId = getStuId();result = result * 59 + ($stuId == null ? 43 : $stuId.hashCode());Object $csrq = getCsrq();result = result * 59 + ($csrq == null ? 43 : $csrq.hashCode());Object $sfdb = getSfdb();result = result * 59 + ($sfdb == null ? 43 : $sfdb.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $rxcj = getRxcj();result = result * 59 + ($rxcj == null ? 43 : $rxcj.hashCode());Object $jtdzstatus = getJtdzstatus();result = result * 59 + ($jtdzstatus == null ? 43 : $jtdzstatus.hashCode());Object $xslxdhstatus = getXslxdhstatus();result = result * 59 + ($xslxdhstatus == null ? 43 : $xslxdhstatus.hashCode());Object $updateTime = getUpdateTime();result = result * 59 + ($updateTime == null ? 43 : $updateTime.hashCode());Object $cym = getCym();result = result * 59 + ($cym == null ? 43 : $cym.hashCode());Object $jg = getJg();result = result * 59 + ($jg == null ? 43 : $jg.hashCode());Object $jkzkm = getJkzkm();result = result * 59 + ($jkzkm == null ? 43 : $jkzkm.hashCode());Object $zzmmm = getZzmmm();result = result * 59 + ($zzmmm == null ? 43 : $zzmmm.hashCode());Object $hklbm = getHklbm();result = result * 59 + ($hklbm == null ? 43 : $hklbm.hashCode());Object $sfsldrk = getSfsldrk();result = result * 59 + ($sfsldrk == null ? 43 : $sfsldrk.hashCode());Object $jtdz = getJtdz();result = result * 59 + ($jtdz == null ? 43 : $jtdz.hashCode());Object $jtlxdh = getJtlxdh();result = result * 59 + ($jtlxdh == null ? 43 : $jtlxdh.hashCode());Object $tc = getTc();result = result * 59 + ($tc == null ? 43 : $tc.hashCode());Object $xslxdh = getXslxdh();result = result * 59 + ($xslxdh == null ? 43 : $xslxdh.hashCode());Object $dzxx = getDzxx();result = result * 59 + ($dzxx == null ? 43 : $dzxx.hashCode());Object $zp = getZp();result = result * 59 + ($zp == null ? 43 : $zp.hashCode());Object $byzp = getByzp();result = result * 59 + ($byzp == null ? 43 : $byzp.hashCode());Object $jstxh = getJstxh();result = result * 59 + ($jstxh == null ? 43 : $jstxh.hashCode());Object $byxx = getByxx();result = result * 59 + ($byxx == null ? 43 : $byxx.hashCode());Object $bmfsm = getBmfsm();result = result * 59 + ($bmfsm == null ? 43 : $bmfsm.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseStudentInfo;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseStudentInfo)) {
            return false;
        }
        VeBaseStudentInfo other = (VeBaseStudentInfo)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$stuId = getStuId();Object other$stuId = other.getStuId();
        if (this$stuId == null ? other$stuId != null : !this$stuId.equals(other$stuId)) {
            return false;
        }
        Object this$csrq = getCsrq();Object other$csrq = other.getCsrq();
        if (this$csrq == null ? other$csrq != null : !this$csrq.equals(other$csrq)) {
            return false;
        }
        Object this$sfdb = getSfdb();Object other$sfdb = other.getSfdb();
        if (this$sfdb == null ? other$sfdb != null : !this$sfdb.equals(other$sfdb)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$rxcj = getRxcj();Object other$rxcj = other.getRxcj();
        if (this$rxcj == null ? other$rxcj != null : !this$rxcj.equals(other$rxcj)) {
            return false;
        }
        Object this$jtdzstatus = getJtdzstatus();Object other$jtdzstatus = other.getJtdzstatus();
        if (this$jtdzstatus == null ? other$jtdzstatus != null : !this$jtdzstatus.equals(other$jtdzstatus)) {
            return false;
        }
        Object this$xslxdhstatus = getXslxdhstatus();Object other$xslxdhstatus = other.getXslxdhstatus();
        if (this$xslxdhstatus == null ? other$xslxdhstatus != null : !this$xslxdhstatus.equals(other$xslxdhstatus)) {
            return false;
        }
        Object this$updateTime = getUpdateTime();Object other$updateTime = other.getUpdateTime();
        if (this$updateTime == null ? other$updateTime != null : !this$updateTime.equals(other$updateTime)) {
            return false;
        }
        Object this$cym = getCym();Object other$cym = other.getCym();
        if (this$cym == null ? other$cym != null : !this$cym.equals(other$cym)) {
            return false;
        }
        Object this$jg = getJg();Object other$jg = other.getJg();
        if (this$jg == null ? other$jg != null : !this$jg.equals(other$jg)) {
            return false;
        }
        Object this$jkzkm = getJkzkm();Object other$jkzkm = other.getJkzkm();
        if (this$jkzkm == null ? other$jkzkm != null : !this$jkzkm.equals(other$jkzkm)) {
            return false;
        }
        Object this$zzmmm = getZzmmm();Object other$zzmmm = other.getZzmmm();
        if (this$zzmmm == null ? other$zzmmm != null : !this$zzmmm.equals(other$zzmmm)) {
            return false;
        }
        Object this$hklbm = getHklbm();Object other$hklbm = other.getHklbm();
        if (this$hklbm == null ? other$hklbm != null : !this$hklbm.equals(other$hklbm)) {
            return false;
        }
        Object this$sfsldrk = getSfsldrk();Object other$sfsldrk = other.getSfsldrk();
        if (this$sfsldrk == null ? other$sfsldrk != null : !this$sfsldrk.equals(other$sfsldrk)) {
            return false;
        }
        Object this$jtdz = getJtdz();Object other$jtdz = other.getJtdz();
        if (this$jtdz == null ? other$jtdz != null : !this$jtdz.equals(other$jtdz)) {
            return false;
        }
        Object this$jtlxdh = getJtlxdh();Object other$jtlxdh = other.getJtlxdh();
        if (this$jtlxdh == null ? other$jtlxdh != null : !this$jtlxdh.equals(other$jtlxdh)) {
            return false;
        }
        Object this$tc = getTc();Object other$tc = other.getTc();
        if (this$tc == null ? other$tc != null : !this$tc.equals(other$tc)) {
            return false;
        }
        Object this$xslxdh = getXslxdh();Object other$xslxdh = other.getXslxdh();
        if (this$xslxdh == null ? other$xslxdh != null : !this$xslxdh.equals(other$xslxdh)) {
            return false;
        }
        Object this$dzxx = getDzxx();Object other$dzxx = other.getDzxx();
        if (this$dzxx == null ? other$dzxx != null : !this$dzxx.equals(other$dzxx)) {
            return false;
        }
        Object this$zp = getZp();Object other$zp = other.getZp();
        if (this$zp == null ? other$zp != null : !this$zp.equals(other$zp)) {
            return false;
        }
        Object this$byzp = getByzp();Object other$byzp = other.getByzp();
        if (this$byzp == null ? other$byzp != null : !this$byzp.equals(other$byzp)) {
            return false;
        }
        Object this$jstxh = getJstxh();Object other$jstxh = other.getJstxh();
        if (this$jstxh == null ? other$jstxh != null : !this$jstxh.equals(other$jstxh)) {
            return false;
        }
        Object this$byxx = getByxx();Object other$byxx = other.getByxx();
        if (this$byxx == null ? other$byxx != null : !this$byxx.equals(other$byxx)) {
            return false;
        }
        Object this$bmfsm = getBmfsm();Object other$bmfsm = other.getBmfsm();return this$bmfsm == null ? other$bmfsm == null : this$bmfsm.equals(other$bmfsm);
    }

    public Integer getStuId()
    {
        return this.stuId;
    }

    public String getCym()
    {
        return this.cym;
    }

    public Integer getCsrq()
    {
        return this.csrq;
    }

    public String getJg()
    {
        return this.jg;
    }

    public String getJkzkm()
    {
        return this.jkzkm;
    }

    public String getZzmmm()
    {
        return this.zzmmm;
    }

    public String getHklbm()
    {
        return this.hklbm;
    }

    public String getSfsldrk()
    {
        return this.sfsldrk;
    }

    public String getJtdz()
    {
        return this.jtdz;
    }

    public String getJtlxdh()
    {
        return this.jtlxdh;
    }

    public String getTc()
    {
        return this.tc;
    }

    public String getXslxdh()
    {
        return this.xslxdh;
    }

    public String getDzxx()
    {
        return this.dzxx;
    }

    public String getZp()
    {
        return this.zp;
    }

    public String getByzp()
    {
        return this.byzp;
    }

    public String getJstxh()
    {
        return this.jstxh;
    }

    public Integer getSfdb()
    {
        return this.sfdb;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public String getByxx()
    {
        return this.byxx;
    }

    public String getBmfsm()
    {
        return this.bmfsm;
    }

    public Double getRxcj()
    {
        return this.rxcj;
    }

    public Integer getJtdzstatus()
    {
        return this.jtdzstatus;
    }

    public Integer getXslxdhstatus()
    {
        return this.xslxdhstatus;
    }

    public Integer getUpdateTime()
    {
        return this.updateTime;
    }
}
