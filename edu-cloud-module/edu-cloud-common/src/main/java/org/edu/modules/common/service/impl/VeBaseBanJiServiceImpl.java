package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseBanJi;
import org.edu.modules.common.mapper.VeBaseBanJiMapper;
import org.edu.modules.common.mapper.VeBaseSpecialtyMapper;
import org.edu.modules.common.service.IVeBaseBanJiService;
import org.edu.modules.common.util.DateTimeUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseBanJiServiceImpl
        extends ServiceImpl<VeBaseBanJiMapper, VeBaseBanJi>
        implements IVeBaseBanJiService
{
    @Autowired
    private VeBaseBanJiMapper veBaseBanJiMapper;
    @Autowired
    private VeBaseSpecialtyMapper veBaseSpecialtyMapper;

    public List<VeBaseBanJi> getBanJiListBySpecId(Integer id, Integer bystatus)
    {
        List<VeBaseBanJi> list = this.veBaseBanJiMapper.getBanJiListBySpecId(id, bystatus);
        if (list.size() > 0) {
            for (VeBaseBanJi veBaseBanJi : list) {
                if (veBaseBanJi.getBysj() != null) {
                    veBaseBanJi.setBysjName(DateTimeUtil.timestampToDate(veBaseBanJi.getBysj().intValue()));
                } else {
                    veBaseBanJi.setBysjName("");
                }
            }
        }
        return list;
    }

    public List<VeBaseBanJi> queryBanJiListBySpecAndGradeId(Integer specId, Integer gradeId)
    {
        List<VeBaseBanJi> list = this.veBaseBanJiMapper.queryBanJiListBySpecAndGradeId(specId, gradeId);
        if (list.size() > 0) {
            for (VeBaseBanJi veBaseBanJi : list) {
                if (veBaseBanJi.getBysj() != null) {
                    veBaseBanJi.setBysjName(DateTimeUtil.timestampToDate(veBaseBanJi.getBysj().intValue()));
                } else {
                    veBaseBanJi.setBysjName("");
                }
            }
        }
        return list;
    }

    public List<VeBaseBanJi> getBanJiListByGradeId(Integer gradeId)
    {
        List<VeBaseBanJi> list = this.veBaseBanJiMapper.getBanJiListByGradeId(gradeId);
        if (list.size() > 0) {
            for (VeBaseBanJi veBaseBanJi : list) {
                if (veBaseBanJi.getBysj() != null) {
                    veBaseBanJi.setBysjName(DateTimeUtil.timestampToDate(veBaseBanJi.getBysj().intValue()));
                } else {
                    veBaseBanJi.setBysjName("");
                }
            }
        }
        return list;
    }

    public List<Map<String, Object>> getBanJiPageList(VeBaseBanJi veBaseBanJi)
    {
        List<Map<String, Object>> list = this.veBaseBanJiMapper.getBanJiPageList(veBaseBanJi);

        list = setRQName(list);
        return list;
    }

    public List<Map<String, Object>> queryBanJiPageListBySearch(String code, String bjmc)
    {
        List<Map<String, Object>> list = this.veBaseBanJiMapper.queryBanJiPageListBySearch(code, bjmc);

        list = setRQName(list);
        return list;
    }

    public VeBaseBanJi getBanJiByName(Integer id, String name)
    {
        VeBaseBanJi veBaseBanJi = this.veBaseBanJiMapper.getBanJiByName(id, name);
        if (veBaseBanJi != null) {
            if (veBaseBanJi.getBysj() != null) {
                veBaseBanJi.setBysjName(DateTimeUtil.timestampToDate(veBaseBanJi.getBysj().intValue()));
            } else {
                veBaseBanJi.setBysjName("");
            }
        }
        return veBaseBanJi;
    }

    public VeBaseBanJi getBanJiByCode(Integer id, String code)
    {
        VeBaseBanJi veBaseBanJi = this.veBaseBanJiMapper.getBanJiByCode(id, code);
        if (veBaseBanJi != null) {
            if (veBaseBanJi.getBysj() != null) {
                veBaseBanJi.setBysjName(DateTimeUtil.timestampToDate(veBaseBanJi.getBysj().intValue()));
            } else {
                veBaseBanJi.setBysjName("");
            }
        }
        return veBaseBanJi;
    }

    public int updateBanJiSchedule()
    {
        Long times = Long.valueOf(System.currentTimeMillis() / 1000L);
        return this.veBaseBanJiMapper.updateBanJiSchedule(times.intValue());
    }

    private List<Map<String, Object>> setRQName(List<Map<String, Object>> list)
    {
        if (list.size() > 0) {
            for (Map map : list) {
                if ((!"".equals(map.get("bysj"))) && (map.get("bysj") != null))
                {
                    Long bysj = Long.valueOf(Long.parseLong(map.get("bysj").toString()));
                    map.put("bysjName", DateTimeUtil.timestampToDate(bysj.longValue()));
                }
                else
                {
                    map.put("bysjName", "");
                }
            }
        }
        return list;
    }
}

