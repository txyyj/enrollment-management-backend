package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_data_apply")
@ApiModel(value="ve_base_data_apply对象", description="数据申请表-数据流程管理")
public class VeBaseDataApply
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="申请理由", width=15.0D)
    @ApiModelProperty("申请理由")
    private String reason;
    @Excel(name="公司名称", width=15.0D)
    @ApiModelProperty("公司名称")
    private String companyName;
    @Excel(name="字段内容", width=15.0D)
    @ApiModelProperty("字段内容")
    private String content;
    @Excel(name="申请时间", width=15.0D)
    @ApiModelProperty("申请时间")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date createTime;
    @Excel(name="状态", width=15.0D)
    @ApiModelProperty("状态")
    private String status;
    @Excel(name="申请人-账号名称", width=15.0D)
    @ApiModelProperty("申请人-账号名称")
    private String user;
    @Excel(name="部门名称", width=15.0D)
    @ApiModelProperty("部门名称")
    private String departmentName;
    @Excel(name="系统名称", width=15.0D)
    @ApiModelProperty("系统名称")
    private String systemName;
    @Excel(name="用户id", width=15.0D)
    @ApiModelProperty("用户id")
    private String appUserId;
    @Excel(name="用户名称", width=15.0D)
    @ApiModelProperty("用户名称")
    private String userName;
    @Excel(name="表名称", width=15.0D)
    @ApiModelProperty("表名称")
    private String tableName;
    @Excel(name="接口用户id", width=15.0D)
    @ApiModelProperty("接口用户id")
    private String interfaceUserId;
    @TableField(exist=false)
    @ApiModelProperty("开始时间")
    private String beginDate;
    @TableField(exist=false)
    @ApiModelProperty("结束时间")
    private String endDate;
    @TableField(exist=false)
    @ApiModelProperty("表字段")
    private List<Map<String, Object>> columnName;
    @TableField(exist=false)
    @ApiModelProperty("userId")
    private String userId;

    public VeBaseDataApply setCompanyName(String companyName)
    {
        this.companyName = companyName;return this;
    }

    public VeBaseDataApply setReason(String reason)
    {
        this.reason = reason;return this;
    }

    public VeBaseDataApply setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseDataApply(id=" + getId() + ", reason=" + getReason() + ", companyName=" + getCompanyName() + ", content=" + getContent() + ", createTime=" + getCreateTime() + ", status=" + getStatus() + ", user=" + getUser() + ", departmentName=" + getDepartmentName() + ", systemName=" + getSystemName() + ", appUserId=" + getAppUserId() + ", userName=" + getUserName() + ", tableName=" + getTableName() + ", interfaceUserId=" + getInterfaceUserId() + ", beginDate=" + getBeginDate() + ", endDate=" + getEndDate() + ", columnName=" + getColumnName() + ", userId=" + getUserId() + ")";
    }

    public VeBaseDataApply setUserId(String userId)
    {
        this.userId = userId;return this;
    }

    public VeBaseDataApply setColumnName(List<Map<String, Object>> columnName)
    {
        this.columnName = columnName;return this;
    }

    public VeBaseDataApply setEndDate(String endDate)
    {
        this.endDate = endDate;return this;
    }

    public VeBaseDataApply setBeginDate(String beginDate)
    {
        this.beginDate = beginDate;return this;
    }

    public VeBaseDataApply setInterfaceUserId(String interfaceUserId)
    {
        this.interfaceUserId = interfaceUserId;return this;
    }

    public VeBaseDataApply setTableName(String tableName)
    {
        this.tableName = tableName;return this;
    }

    public VeBaseDataApply setUserName(String userName)
    {
        this.userName = userName;return this;
    }

    public VeBaseDataApply setAppUserId(String appUserId)
    {
        this.appUserId = appUserId;return this;
    }

    public VeBaseDataApply setSystemName(String systemName)
    {
        this.systemName = systemName;return this;
    }

    public VeBaseDataApply setDepartmentName(String departmentName)
    {
        this.departmentName = departmentName;return this;
    }

    public VeBaseDataApply setUser(String user)
    {
        this.user = user;return this;
    }

    public VeBaseDataApply setStatus(String status)
    {
        this.status = status;return this;
    }

    @JsonFormat(pattern="yyyy-MM-dd")
    public VeBaseDataApply setCreateTime(Date createTime)
    {
        this.createTime = createTime;return this;
    }

    public VeBaseDataApply setContent(String content)
    {
        this.content = content;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $reason = getReason();result = result * 59 + ($reason == null ? 43 : $reason.hashCode());Object $companyName = getCompanyName();result = result * 59 + ($companyName == null ? 43 : $companyName.hashCode());Object $content = getContent();result = result * 59 + ($content == null ? 43 : $content.hashCode());Object $createTime = getCreateTime();result = result * 59 + ($createTime == null ? 43 : $createTime.hashCode());Object $status = getStatus();result = result * 59 + ($status == null ? 43 : $status.hashCode());Object $user = getUser();result = result * 59 + ($user == null ? 43 : $user.hashCode());Object $departmentName = getDepartmentName();result = result * 59 + ($departmentName == null ? 43 : $departmentName.hashCode());Object $systemName = getSystemName();result = result * 59 + ($systemName == null ? 43 : $systemName.hashCode());Object $appUserId = getAppUserId();result = result * 59 + ($appUserId == null ? 43 : $appUserId.hashCode());Object $userName = getUserName();result = result * 59 + ($userName == null ? 43 : $userName.hashCode());Object $tableName = getTableName();result = result * 59 + ($tableName == null ? 43 : $tableName.hashCode());Object $interfaceUserId = getInterfaceUserId();result = result * 59 + ($interfaceUserId == null ? 43 : $interfaceUserId.hashCode());Object $beginDate = getBeginDate();result = result * 59 + ($beginDate == null ? 43 : $beginDate.hashCode());Object $endDate = getEndDate();result = result * 59 + ($endDate == null ? 43 : $endDate.hashCode());Object $columnName = getColumnName();result = result * 59 + ($columnName == null ? 43 : $columnName.hashCode());Object $userId = getUserId();result = result * 59 + ($userId == null ? 43 : $userId.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseDataApply;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseDataApply)) {
            return false;
        }
        VeBaseDataApply other = (VeBaseDataApply)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$reason = getReason();Object other$reason = other.getReason();
        if (this$reason == null ? other$reason != null : !this$reason.equals(other$reason)) {
            return false;
        }
        Object this$companyName = getCompanyName();Object other$companyName = other.getCompanyName();
        if (this$companyName == null ? other$companyName != null : !this$companyName.equals(other$companyName)) {
            return false;
        }
        Object this$content = getContent();Object other$content = other.getContent();
        if (this$content == null ? other$content != null : !this$content.equals(other$content)) {
            return false;
        }
        Object this$createTime = getCreateTime();Object other$createTime = other.getCreateTime();
        if (this$createTime == null ? other$createTime != null : !this$createTime.equals(other$createTime)) {
            return false;
        }
        Object this$status = getStatus();Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) {
            return false;
        }
        Object this$user = getUser();Object other$user = other.getUser();
        if (this$user == null ? other$user != null : !this$user.equals(other$user)) {
            return false;
        }
        Object this$departmentName = getDepartmentName();Object other$departmentName = other.getDepartmentName();
        if (this$departmentName == null ? other$departmentName != null : !this$departmentName.equals(other$departmentName)) {
            return false;
        }
        Object this$systemName = getSystemName();Object other$systemName = other.getSystemName();
        if (this$systemName == null ? other$systemName != null : !this$systemName.equals(other$systemName)) {
            return false;
        }
        Object this$appUserId = getAppUserId();Object other$appUserId = other.getAppUserId();
        if (this$appUserId == null ? other$appUserId != null : !this$appUserId.equals(other$appUserId)) {
            return false;
        }
        Object this$userName = getUserName();Object other$userName = other.getUserName();
        if (this$userName == null ? other$userName != null : !this$userName.equals(other$userName)) {
            return false;
        }
        Object this$tableName = getTableName();Object other$tableName = other.getTableName();
        if (this$tableName == null ? other$tableName != null : !this$tableName.equals(other$tableName)) {
            return false;
        }
        Object this$interfaceUserId = getInterfaceUserId();Object other$interfaceUserId = other.getInterfaceUserId();
        if (this$interfaceUserId == null ? other$interfaceUserId != null : !this$interfaceUserId.equals(other$interfaceUserId)) {
            return false;
        }
        Object this$beginDate = getBeginDate();Object other$beginDate = other.getBeginDate();
        if (this$beginDate == null ? other$beginDate != null : !this$beginDate.equals(other$beginDate)) {
            return false;
        }
        Object this$endDate = getEndDate();Object other$endDate = other.getEndDate();
        if (this$endDate == null ? other$endDate != null : !this$endDate.equals(other$endDate)) {
            return false;
        }
        Object this$columnName = getColumnName();Object other$columnName = other.getColumnName();
        if (this$columnName == null ? other$columnName != null : !this$columnName.equals(other$columnName)) {
            return false;
        }
        Object this$userId = getUserId();Object other$userId = other.getUserId();return this$userId == null ? other$userId == null : this$userId.equals(other$userId);
    }

    public String getId()
    {
        return this.id;
    }

    public String getReason()
    {
        return this.reason;
    }

    public String getCompanyName()
    {
        return this.companyName;
    }

    public String getContent()
    {
        return this.content;
    }

    public Date getCreateTime()
    {
        return this.createTime;
    }

    public String getStatus()
    {
        return this.status;
    }

    public String getUser()
    {
        return this.user;
    }

    public String getDepartmentName()
    {
        return this.departmentName;
    }

    public String getSystemName()
    {
        return this.systemName;
    }

    public String getAppUserId()
    {
        return this.appUserId;
    }

    public String getUserName()
    {
        return this.userName;
    }

    public String getTableName()
    {
        return this.tableName;
    }

    public String getInterfaceUserId()
    {
        return this.interfaceUserId;
    }

    public String getBeginDate()
    {
        return this.beginDate;
    }

    public String getEndDate()
    {
        return this.endDate;
    }

    public List<Map<String, Object>> getColumnName()
    {
        return this.columnName;
    }

    public String getUserId()
    {
        return this.userId;
    }
}
