package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.common.entity.VeBaseSysConfig;

public abstract interface IVeBaseSysConfigService
        extends IService<VeBaseSysConfig>
{
    public abstract List<VeBaseSysConfig> querySysConfigPageList(VeBaseSysConfig paramVeBaseSysConfig);
}
