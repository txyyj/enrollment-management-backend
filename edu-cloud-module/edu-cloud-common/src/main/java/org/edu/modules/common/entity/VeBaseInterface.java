package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_interface")
@ApiModel(value="ve_base_interface对象", description="接口信息表")
public class VeBaseInterface
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="接口名称", width=15.0D)
    @ApiModelProperty("接口名称")
    private String interfaceName;
    @Excel(name="创建时间", width=15.0D)
    @ApiModelProperty("创建时间")
    private Date createTime;
    @Excel(name="标识", width=15.0D)
    @ApiModelProperty("标识")
    private String flag;
    @Excel(name="备注字段", width=15.0D)
    @ApiModelProperty("备注字段")
    private String remark;
    @Excel(name="创建人id", width=15.0D)
    @ApiModelProperty("创建人id")
    private String createId;
    @Excel(name="表名称", width=15.0D)
    @ApiModelProperty("表名称")
    private String tableName;

    public VeBaseInterface setCreateTime(Date createTime)
    {
        this.createTime = createTime;return this;
    }

    public VeBaseInterface setInterfaceName(String interfaceName)
    {
        this.interfaceName = interfaceName;return this;
    }

    public VeBaseInterface setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseInterface(id=" + getId() + ", interfaceName=" + getInterfaceName() + ", createTime=" + getCreateTime() + ", flag=" + getFlag() + ", remark=" + getRemark() + ", createId=" + getCreateId() + ", tableName=" + getTableName() + ")";
    }

    public VeBaseInterface setTableName(String tableName)
    {
        this.tableName = tableName;return this;
    }

    public VeBaseInterface setCreateId(String createId)
    {
        this.createId = createId;return this;
    }

    public VeBaseInterface setRemark(String remark)
    {
        this.remark = remark;return this;
    }

    public VeBaseInterface setFlag(String flag)
    {
        this.flag = flag;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $interfaceName = getInterfaceName();result = result * 59 + ($interfaceName == null ? 43 : $interfaceName.hashCode());Object $createTime = getCreateTime();result = result * 59 + ($createTime == null ? 43 : $createTime.hashCode());Object $flag = getFlag();result = result * 59 + ($flag == null ? 43 : $flag.hashCode());Object $remark = getRemark();result = result * 59 + ($remark == null ? 43 : $remark.hashCode());Object $createId = getCreateId();result = result * 59 + ($createId == null ? 43 : $createId.hashCode());Object $tableName = getTableName();result = result * 59 + ($tableName == null ? 43 : $tableName.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseInterface;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseInterface)) {
            return false;
        }
        VeBaseInterface other = (VeBaseInterface)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$interfaceName = getInterfaceName();Object other$interfaceName = other.getInterfaceName();
        if (this$interfaceName == null ? other$interfaceName != null : !this$interfaceName.equals(other$interfaceName)) {
            return false;
        }
        Object this$createTime = getCreateTime();Object other$createTime = other.getCreateTime();
        if (this$createTime == null ? other$createTime != null : !this$createTime.equals(other$createTime)) {
            return false;
        }
        Object this$flag = getFlag();Object other$flag = other.getFlag();
        if (this$flag == null ? other$flag != null : !this$flag.equals(other$flag)) {
            return false;
        }
        Object this$remark = getRemark();Object other$remark = other.getRemark();
        if (this$remark == null ? other$remark != null : !this$remark.equals(other$remark)) {
            return false;
        }
        Object this$createId = getCreateId();Object other$createId = other.getCreateId();
        if (this$createId == null ? other$createId != null : !this$createId.equals(other$createId)) {
            return false;
        }
        Object this$tableName = getTableName();Object other$tableName = other.getTableName();return this$tableName == null ? other$tableName == null : this$tableName.equals(other$tableName);
    }

    public String getId()
    {
        return this.id;
    }

    public String getInterfaceName()
    {
        return this.interfaceName;
    }

    public Date getCreateTime()
    {
        return this.createTime;
    }

    public String getFlag()
    {
        return this.flag;
    }

    public String getRemark()
    {
        return this.remark;
    }

    public String getCreateId()
    {
        return this.createId;
    }

    public String getTableName()
    {
        return this.tableName;
    }
}
