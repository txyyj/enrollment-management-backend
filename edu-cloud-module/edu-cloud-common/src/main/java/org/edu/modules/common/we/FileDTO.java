package org.edu.modules.common.we;

public class FileDTO
{
    private Long fileSize;
    private String fileApUrl;
    private String webUrl;
    private String fileSuffix;
    private String fileBucket;
    private String oldFileName;
    private String folder;
    private String message;

    public void setFileApUrl(String fileApUrl)
    {
        this.fileApUrl = fileApUrl;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $fileSize = getFileSize();result = result * 59 + ($fileSize == null ? 43 : $fileSize.hashCode());Object $fileApUrl = getFileApUrl();result = result * 59 + ($fileApUrl == null ? 43 : $fileApUrl.hashCode());Object $webUrl = getWebUrl();result = result * 59 + ($webUrl == null ? 43 : $webUrl.hashCode());Object $fileSuffix = getFileSuffix();result = result * 59 + ($fileSuffix == null ? 43 : $fileSuffix.hashCode());Object $fileBucket = getFileBucket();result = result * 59 + ($fileBucket == null ? 43 : $fileBucket.hashCode());Object $oldFileName = getOldFileName();result = result * 59 + ($oldFileName == null ? 43 : $oldFileName.hashCode());Object $folder = getFolder();result = result * 59 + ($folder == null ? 43 : $folder.hashCode());Object $message = getMessage();result = result * 59 + ($message == null ? 43 : $message.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof FileDTO;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof FileDTO)) {
            return false;
        }
        FileDTO other = (FileDTO)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$fileSize = getFileSize();Object other$fileSize = other.getFileSize();
        if (this$fileSize == null ? other$fileSize != null : !this$fileSize.equals(other$fileSize)) {
            return false;
        }
        Object this$fileApUrl = getFileApUrl();Object other$fileApUrl = other.getFileApUrl();
        if (this$fileApUrl == null ? other$fileApUrl != null : !this$fileApUrl.equals(other$fileApUrl)) {
            return false;
        }
        Object this$webUrl = getWebUrl();Object other$webUrl = other.getWebUrl();
        if (this$webUrl == null ? other$webUrl != null : !this$webUrl.equals(other$webUrl)) {
            return false;
        }
        Object this$fileSuffix = getFileSuffix();Object other$fileSuffix = other.getFileSuffix();
        if (this$fileSuffix == null ? other$fileSuffix != null : !this$fileSuffix.equals(other$fileSuffix)) {
            return false;
        }
        Object this$fileBucket = getFileBucket();Object other$fileBucket = other.getFileBucket();
        if (this$fileBucket == null ? other$fileBucket != null : !this$fileBucket.equals(other$fileBucket)) {
            return false;
        }
        Object this$oldFileName = getOldFileName();Object other$oldFileName = other.getOldFileName();
        if (this$oldFileName == null ? other$oldFileName != null : !this$oldFileName.equals(other$oldFileName)) {
            return false;
        }
        Object this$folder = getFolder();Object other$folder = other.getFolder();
        if (this$folder == null ? other$folder != null : !this$folder.equals(other$folder)) {
            return false;
        }
        Object this$message = getMessage();Object other$message = other.getMessage();return this$message == null ? other$message == null : this$message.equals(other$message);
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public void setFolder(String folder)
    {
        this.folder = folder;
    }

    public void setOldFileName(String oldFileName)
    {
        this.oldFileName = oldFileName;
    }

    public void setFileBucket(String fileBucket)
    {
        this.fileBucket = fileBucket;
    }

    public void setFileSuffix(String fileSuffix)
    {
        this.fileSuffix = fileSuffix;
    }

    public void setWebUrl(String webUrl)
    {
        this.webUrl = webUrl;
    }

    public void setFileSize(Long fileSize)
    {
        this.fileSize = fileSize;
    }

    public String toString()
    {
        return "FileDTO(fileSize=" + getFileSize() + ", fileApUrl=" + getFileApUrl() + ", webUrl=" + getWebUrl() + ", fileSuffix=" + getFileSuffix() + ", fileBucket=" + getFileBucket() + ", oldFileName=" + getOldFileName() + ", folder=" + getFolder() + ", message=" + getMessage() + ")";
    }

    public Long getFileSize()
    {
        return this.fileSize;
    }

    public String getFileApUrl()
    {
        return this.fileApUrl;
    }

    public String getWebUrl()
    {
        return this.webUrl;
    }

    public String getFileSuffix()
    {
        return this.fileSuffix;
    }

    public String getFileBucket()
    {
        return this.fileBucket;
    }

    public String getOldFileName()
    {
        return this.oldFileName;
    }

    public String getFolder()
    {
        return this.folder;
    }

    public String getMessage()
    {
        return this.message;
    }

    public FileDTO(Long fileSize, String fileApUrl, String webUrl, String fileSuffix, String fileBucket, String oldFileName, String folder, String message)
    {
        this.fileSize = fileSize;
        this.fileApUrl = fileApUrl;
        this.webUrl = webUrl;
        this.fileSuffix = fileSuffix;
        this.fileBucket = fileBucket;
        this.oldFileName = oldFileName;
        this.folder = folder;
        this.message = message;
    }

    public FileDTO() {}
}

