package org.edu.modules.common.redis;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.edu.modules.common.entity.VeBaseStudent;
import org.edu.modules.common.service.IVeBaseAppUserService;
import org.edu.modules.common.service.IVeBaseCampusService;
import org.edu.modules.common.service.IVeBaseDepartmentService;
import org.edu.modules.common.service.IVeBaseDictAreaService;
import org.edu.modules.common.service.IVeBaseDictionaryService;
import org.edu.modules.common.service.IVeBaseFacultyService;
import org.edu.modules.common.service.IVeBaseGradeService;
import org.edu.modules.common.service.IVeBaseJYZService;
import org.edu.modules.common.service.IVeBaseSemesterService;
import org.edu.modules.common.service.IVeBaseStudentService;
import org.edu.modules.common.service.IVeBaseXueZhiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class ApplicationRunnerRedis
        implements ApplicationRunner
{
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private IVeBaseAppUserService veBaseAppUserService;
    @Autowired
    private IVeBaseXueZhiService veBaseXueZhiService;
    @Autowired
    private IVeBaseGradeService veBaseGradeService;
    @Autowired
    private IVeBaseFacultyService veBaseFacultyService;
    @Autowired
    private IVeBaseDepartmentService veBaseDepartmentService;
    @Autowired
    private IVeBaseStudentService veBaseStudentService;
    @Autowired
    private IVeBaseCampusService veBaseCampusService;
    @Autowired
    private IVeBaseSemesterService veBaseSemesterService;
    @Autowired
    private IVeBaseDictAreaService veBaseDictAreaService;
    @Autowired
    private IVeBaseJYZService veBaseJYZService;
    @Autowired
    private IVeBaseDictionaryService veBaseDictionaryService;

    public void run(ApplicationArguments args)
            throws Exception
    {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.orderByAsc("years");
        this.redisUtils.set("queryXueZhiList", this.veBaseXueZhiService.list(queryWrapper));

        QueryWrapper queryWrapper2 = new QueryWrapper();
        queryWrapper2.eq("njzt", Integer.valueOf(1));
        queryWrapper2.orderByDesc("rxnf");
        this.redisUtils.set("queryGradeList", this.veBaseGradeService.list(queryWrapper2));

        QueryWrapper queryWrapper3 = new QueryWrapper();
        queryWrapper3.eq("status", Integer.valueOf(1));
        queryWrapper3.orderByAsc("yxdm");
        this.redisUtils.set("queryFacultyList", this.veBaseFacultyService.list(queryWrapper3));
        this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", this.veBaseFacultyService.getTreeList());

        this.redisUtils.set("queryDepartmentList", this.veBaseDepartmentService.getTreeList());
        this.redisUtils.set("queryDepartmentAndTeacherList", this.veBaseDepartmentService.getDepartmentAndTeacherList());
        this.redisUtils.set("queryOrganUserTreeList", this.veBaseAppUserService.queryOrganUserTreeList(null));

        this.redisUtils.set("queryCampusList", this.veBaseCampusService.list());

        QueryWrapper queryWrapper4 = new QueryWrapper();
        queryWrapper4.orderByDesc("xqm");
        this.redisUtils.set("querySemesterList", this.veBaseSemesterService.list(queryWrapper4));

        this.redisUtils.set("queryProvinceList", this.veBaseDictAreaService.getProvinceList());

        this.redisUtils.set("queryJYZTreeList", this.veBaseJYZService.getJYZTreeList());

        this.redisUtils.set("queryDictionaryTreeList", this.veBaseDictionaryService.getTreeList());

        this.redisUtils.set("queryStudentList", this.veBaseStudentService.getStudentPageList(new VeBaseStudent()));
    }
}
