package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_app_user")
@ApiModel(value="ve_base_app_user对象", description="用户信息")
public class VeBaseAppUser
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="工号（或学号）", width=15.0D)
    @ApiModelProperty("用户id")
    private String userId;
    @Excel(name="密码", width=15.0D)
    @ApiModelProperty("用户密码")
    private String userPassword;
    @Excel(name="md5密码盐", width=15.0D)
    @ApiModelProperty("md5密码盐")
    private String salt;
    @Excel(name="用户工号", width=15.0D)
    @ApiModelProperty("用户工号")
    private String userName;
    @Excel(name="用户名称（姓名）", width=15.0D)
    @ApiModelProperty("用户姓名")
    private String realName;
    @Excel(name="返回状态", width=15.0D)
    @ApiModelProperty("返回状态")
    private String status;
    @Excel(name="用户类型（js(教师)、xs（学生））", width=15.0D)
    @ApiModelProperty("用户类别(1老师 2学生)")
    private String userType;
    @Excel(name="用户登录名(手机号)", width=15.0D)
    @ApiModelProperty("用户手机号")
    private String userTel;
    @Excel(name="状态（学生为空，17：招聘；在职：10；退休：01；其他：18）", width=15.0D)
    @ApiModelProperty("状态（学生为空，0：招聘；1在职：2退休：3其他）")
    private String statusVal;
    @ApiModelProperty("是否删除")
    private String isDel;
    @Excel(name="numbers", width=15.0D)
    @ApiModelProperty("numbers")
    private String numbers;
    @Excel(name="userApp", width=15.0D)
    @ApiModelProperty("userApp")
    private String userApp;
    @Excel(name="创建时间", width=20.0D)
    @ApiModelProperty("创建时间")
    private String createTime;
    @Excel(name="修改时间", width=20.0D)
    @ApiModelProperty("修改时间")
    private String updateTime;
    @Excel(name="头像路径", width=20.0D)
    @ApiModelProperty("头像路径")
    private String avatarUrl;
    @Excel(name="密码强度", width=20.0D)
    @ApiModelProperty("密码强度")
    private String strength;
    @Excel(name="邮箱", width=20.0D)
    @ApiModelProperty("邮箱")
    private String email;
    @Excel(name="生日", width=20.0D)
    @ApiModelProperty("生日")
    private Date birthday;
    @Excel(name="学历", width=20.0D)
    @ApiModelProperty("学历")
    private String education;
    @Excel(name="学位", width=20.0D)
    @ApiModelProperty("学位")
    private String degree;
    @Excel(name="联系地址", width=20.0D)
    @ApiModelProperty("联系地址")
    private String address;
    @Excel(name="个性签名", width=20.0D)
    @ApiModelProperty("个性签名")
    private String signature;
    @Excel(name="性别", width=20.0D)
    @ApiModelProperty("性别(0保密, 1男, 2女)")
    private String sex;
    @TableField(exist=false)
    private String oldPwd;
    @TableField(exist=false)
    private String newPwd;
    @TableField(exist=false)
    private String affirmPwd;
    @TableField(exist=false)
    private String roleId;
    @TableField(exist=false)
    private String roleName;
    @TableField(exist=false)
    private String deptId;
    @TableField(exist=false)
    private String deptName;
    @TableField(exist=false)
    private String birthdayName;
    @ApiModelProperty("接口用户id")
    @TableField(exist=false)
    private String interfaceUserId;
    @ApiModelProperty("创建企业用户的用户角色id")
    @TableField(exist=false)
    private Integer userRoleId;

    public VeBaseAppUser setUserPassword(String userPassword)
    {
        this.userPassword = userPassword;return this;
    }

    public VeBaseAppUser setUserId(String userId)
    {
        this.userId = userId;return this;
    }

    public VeBaseAppUser setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseAppUser(id=" + getId() + ", userId=" + getUserId() + ", userPassword=" + getUserPassword() + ", salt=" + getSalt() + ", userName=" + getUserName() + ", realName=" + getRealName() + ", status=" + getStatus() + ", userType=" + getUserType() + ", userTel=" + getUserTel() + ", statusVal=" + getStatusVal() + ", isDel=" + getIsDel() + ", numbers=" + getNumbers() + ", userApp=" + getUserApp() + ", createTime=" + getCreateTime() + ", updateTime=" + getUpdateTime() + ", avatarUrl=" + getAvatarUrl() + ", strength=" + getStrength() + ", email=" + getEmail() + ", birthday=" + getBirthday() + ", education=" + getEducation() + ", degree=" + getDegree() + ", address=" + getAddress() + ", signature=" + getSignature() + ", sex=" + getSex() + ", oldPwd=" + getOldPwd() + ", newPwd=" + getNewPwd() + ", affirmPwd=" + getAffirmPwd() + ", roleId=" + getRoleId() + ", roleName=" + getRoleName() + ", deptId=" + getDeptId() + ", deptName=" + getDeptName() + ", birthdayName=" + getBirthdayName() + ", interfaceUserId=" + getInterfaceUserId() + ", userRoleId=" + getUserRoleId() + ")";
    }

    public VeBaseAppUser setUserRoleId(Integer userRoleId)
    {
        this.userRoleId = userRoleId;return this;
    }

    public VeBaseAppUser setInterfaceUserId(String interfaceUserId)
    {
        this.interfaceUserId = interfaceUserId;return this;
    }

    public VeBaseAppUser setBirthdayName(String birthdayName)
    {
        this.birthdayName = birthdayName;return this;
    }

    public VeBaseAppUser setDeptName(String deptName)
    {
        this.deptName = deptName;return this;
    }

    public VeBaseAppUser setDeptId(String deptId)
    {
        this.deptId = deptId;return this;
    }

    public VeBaseAppUser setRoleName(String roleName)
    {
        this.roleName = roleName;return this;
    }

    public VeBaseAppUser setRoleId(String roleId)
    {
        this.roleId = roleId;return this;
    }

    public VeBaseAppUser setAffirmPwd(String affirmPwd)
    {
        this.affirmPwd = affirmPwd;return this;
    }

    public VeBaseAppUser setNewPwd(String newPwd)
    {
        this.newPwd = newPwd;return this;
    }

    public VeBaseAppUser setOldPwd(String oldPwd)
    {
        this.oldPwd = oldPwd;return this;
    }

    public VeBaseAppUser setSex(String sex)
    {
        this.sex = sex;return this;
    }

    public VeBaseAppUser setSignature(String signature)
    {
        this.signature = signature;return this;
    }

    public VeBaseAppUser setAddress(String address)
    {
        this.address = address;return this;
    }

    public VeBaseAppUser setDegree(String degree)
    {
        this.degree = degree;return this;
    }

    public VeBaseAppUser setEducation(String education)
    {
        this.education = education;return this;
    }

    public VeBaseAppUser setBirthday(Date birthday)
    {
        this.birthday = birthday;return this;
    }

    public VeBaseAppUser setEmail(String email)
    {
        this.email = email;return this;
    }

    public VeBaseAppUser setStrength(String strength)
    {
        this.strength = strength;return this;
    }

    public VeBaseAppUser setAvatarUrl(String avatarUrl)
    {
        this.avatarUrl = avatarUrl;return this;
    }

    public VeBaseAppUser setUpdateTime(String updateTime)
    {
        this.updateTime = updateTime;return this;
    }

    public VeBaseAppUser setCreateTime(String createTime)
    {
        this.createTime = createTime;return this;
    }

    public VeBaseAppUser setUserApp(String userApp)
    {
        this.userApp = userApp;return this;
    }

    public VeBaseAppUser setNumbers(String numbers)
    {
        this.numbers = numbers;return this;
    }

    public VeBaseAppUser setIsDel(String isDel)
    {
        this.isDel = isDel;return this;
    }

    public VeBaseAppUser setStatusVal(String statusVal)
    {
        this.statusVal = statusVal;return this;
    }

    public VeBaseAppUser setUserTel(String userTel)
    {
        this.userTel = userTel;return this;
    }

    public VeBaseAppUser setUserType(String userType)
    {
        this.userType = userType;return this;
    }

    public VeBaseAppUser setStatus(String status)
    {
        this.status = status;return this;
    }

    public VeBaseAppUser setRealName(String realName)
    {
        this.realName = realName;return this;
    }

    public VeBaseAppUser setUserName(String userName)
    {
        this.userName = userName;return this;
    }

    public VeBaseAppUser setSalt(String salt)
    {
        this.salt = salt;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $userRoleId = getUserRoleId();result = result * 59 + ($userRoleId == null ? 43 : $userRoleId.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $userId = getUserId();result = result * 59 + ($userId == null ? 43 : $userId.hashCode());Object $userPassword = getUserPassword();result = result * 59 + ($userPassword == null ? 43 : $userPassword.hashCode());Object $salt = getSalt();result = result * 59 + ($salt == null ? 43 : $salt.hashCode());Object $userName = getUserName();result = result * 59 + ($userName == null ? 43 : $userName.hashCode());Object $realName = getRealName();result = result * 59 + ($realName == null ? 43 : $realName.hashCode());Object $status = getStatus();result = result * 59 + ($status == null ? 43 : $status.hashCode());Object $userType = getUserType();result = result * 59 + ($userType == null ? 43 : $userType.hashCode());Object $userTel = getUserTel();result = result * 59 + ($userTel == null ? 43 : $userTel.hashCode());Object $statusVal = getStatusVal();result = result * 59 + ($statusVal == null ? 43 : $statusVal.hashCode());Object $isDel = getIsDel();result = result * 59 + ($isDel == null ? 43 : $isDel.hashCode());Object $numbers = getNumbers();result = result * 59 + ($numbers == null ? 43 : $numbers.hashCode());Object $userApp = getUserApp();result = result * 59 + ($userApp == null ? 43 : $userApp.hashCode());Object $createTime = getCreateTime();result = result * 59 + ($createTime == null ? 43 : $createTime.hashCode());Object $updateTime = getUpdateTime();result = result * 59 + ($updateTime == null ? 43 : $updateTime.hashCode());Object $avatarUrl = getAvatarUrl();result = result * 59 + ($avatarUrl == null ? 43 : $avatarUrl.hashCode());Object $strength = getStrength();result = result * 59 + ($strength == null ? 43 : $strength.hashCode());Object $email = getEmail();result = result * 59 + ($email == null ? 43 : $email.hashCode());Object $birthday = getBirthday();result = result * 59 + ($birthday == null ? 43 : $birthday.hashCode());Object $education = getEducation();result = result * 59 + ($education == null ? 43 : $education.hashCode());Object $degree = getDegree();result = result * 59 + ($degree == null ? 43 : $degree.hashCode());Object $address = getAddress();result = result * 59 + ($address == null ? 43 : $address.hashCode());Object $signature = getSignature();result = result * 59 + ($signature == null ? 43 : $signature.hashCode());Object $sex = getSex();result = result * 59 + ($sex == null ? 43 : $sex.hashCode());Object $oldPwd = getOldPwd();result = result * 59 + ($oldPwd == null ? 43 : $oldPwd.hashCode());Object $newPwd = getNewPwd();result = result * 59 + ($newPwd == null ? 43 : $newPwd.hashCode());Object $affirmPwd = getAffirmPwd();result = result * 59 + ($affirmPwd == null ? 43 : $affirmPwd.hashCode());Object $roleId = getRoleId();result = result * 59 + ($roleId == null ? 43 : $roleId.hashCode());Object $roleName = getRoleName();result = result * 59 + ($roleName == null ? 43 : $roleName.hashCode());Object $deptId = getDeptId();result = result * 59 + ($deptId == null ? 43 : $deptId.hashCode());Object $deptName = getDeptName();result = result * 59 + ($deptName == null ? 43 : $deptName.hashCode());Object $birthdayName = getBirthdayName();result = result * 59 + ($birthdayName == null ? 43 : $birthdayName.hashCode());Object $interfaceUserId = getInterfaceUserId();result = result * 59 + ($interfaceUserId == null ? 43 : $interfaceUserId.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseAppUser;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseAppUser)) {
            return false;
        }
        VeBaseAppUser other = (VeBaseAppUser)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$userRoleId = getUserRoleId();Object other$userRoleId = other.getUserRoleId();
        if (this$userRoleId == null ? other$userRoleId != null : !this$userRoleId.equals(other$userRoleId)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$userId = getUserId();Object other$userId = other.getUserId();
        if (this$userId == null ? other$userId != null : !this$userId.equals(other$userId)) {
            return false;
        }
        Object this$userPassword = getUserPassword();Object other$userPassword = other.getUserPassword();
        if (this$userPassword == null ? other$userPassword != null : !this$userPassword.equals(other$userPassword)) {
            return false;
        }
        Object this$salt = getSalt();Object other$salt = other.getSalt();
        if (this$salt == null ? other$salt != null : !this$salt.equals(other$salt)) {
            return false;
        }
        Object this$userName = getUserName();Object other$userName = other.getUserName();
        if (this$userName == null ? other$userName != null : !this$userName.equals(other$userName)) {
            return false;
        }
        Object this$realName = getRealName();Object other$realName = other.getRealName();
        if (this$realName == null ? other$realName != null : !this$realName.equals(other$realName)) {
            return false;
        }
        Object this$status = getStatus();Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) {
            return false;
        }
        Object this$userType = getUserType();Object other$userType = other.getUserType();
        if (this$userType == null ? other$userType != null : !this$userType.equals(other$userType)) {
            return false;
        }
        Object this$userTel = getUserTel();Object other$userTel = other.getUserTel();
        if (this$userTel == null ? other$userTel != null : !this$userTel.equals(other$userTel)) {
            return false;
        }
        Object this$statusVal = getStatusVal();Object other$statusVal = other.getStatusVal();
        if (this$statusVal == null ? other$statusVal != null : !this$statusVal.equals(other$statusVal)) {
            return false;
        }
        Object this$isDel = getIsDel();Object other$isDel = other.getIsDel();
        if (this$isDel == null ? other$isDel != null : !this$isDel.equals(other$isDel)) {
            return false;
        }
        Object this$numbers = getNumbers();Object other$numbers = other.getNumbers();
        if (this$numbers == null ? other$numbers != null : !this$numbers.equals(other$numbers)) {
            return false;
        }
        Object this$userApp = getUserApp();Object other$userApp = other.getUserApp();
        if (this$userApp == null ? other$userApp != null : !this$userApp.equals(other$userApp)) {
            return false;
        }
        Object this$createTime = getCreateTime();Object other$createTime = other.getCreateTime();
        if (this$createTime == null ? other$createTime != null : !this$createTime.equals(other$createTime)) {
            return false;
        }
        Object this$updateTime = getUpdateTime();Object other$updateTime = other.getUpdateTime();
        if (this$updateTime == null ? other$updateTime != null : !this$updateTime.equals(other$updateTime)) {
            return false;
        }
        Object this$avatarUrl = getAvatarUrl();Object other$avatarUrl = other.getAvatarUrl();
        if (this$avatarUrl == null ? other$avatarUrl != null : !this$avatarUrl.equals(other$avatarUrl)) {
            return false;
        }
        Object this$strength = getStrength();Object other$strength = other.getStrength();
        if (this$strength == null ? other$strength != null : !this$strength.equals(other$strength)) {
            return false;
        }
        Object this$email = getEmail();Object other$email = other.getEmail();
        if (this$email == null ? other$email != null : !this$email.equals(other$email)) {
            return false;
        }
        Object this$birthday = getBirthday();Object other$birthday = other.getBirthday();
        if (this$birthday == null ? other$birthday != null : !this$birthday.equals(other$birthday)) {
            return false;
        }
        Object this$education = getEducation();Object other$education = other.getEducation();
        if (this$education == null ? other$education != null : !this$education.equals(other$education)) {
            return false;
        }
        Object this$degree = getDegree();Object other$degree = other.getDegree();
        if (this$degree == null ? other$degree != null : !this$degree.equals(other$degree)) {
            return false;
        }
        Object this$address = getAddress();Object other$address = other.getAddress();
        if (this$address == null ? other$address != null : !this$address.equals(other$address)) {
            return false;
        }
        Object this$signature = getSignature();Object other$signature = other.getSignature();
        if (this$signature == null ? other$signature != null : !this$signature.equals(other$signature)) {
            return false;
        }
        Object this$sex = getSex();Object other$sex = other.getSex();
        if (this$sex == null ? other$sex != null : !this$sex.equals(other$sex)) {
            return false;
        }
        Object this$oldPwd = getOldPwd();Object other$oldPwd = other.getOldPwd();
        if (this$oldPwd == null ? other$oldPwd != null : !this$oldPwd.equals(other$oldPwd)) {
            return false;
        }
        Object this$newPwd = getNewPwd();Object other$newPwd = other.getNewPwd();
        if (this$newPwd == null ? other$newPwd != null : !this$newPwd.equals(other$newPwd)) {
            return false;
        }
        Object this$affirmPwd = getAffirmPwd();Object other$affirmPwd = other.getAffirmPwd();
        if (this$affirmPwd == null ? other$affirmPwd != null : !this$affirmPwd.equals(other$affirmPwd)) {
            return false;
        }
        Object this$roleId = getRoleId();Object other$roleId = other.getRoleId();
        if (this$roleId == null ? other$roleId != null : !this$roleId.equals(other$roleId)) {
            return false;
        }
        Object this$roleName = getRoleName();Object other$roleName = other.getRoleName();
        if (this$roleName == null ? other$roleName != null : !this$roleName.equals(other$roleName)) {
            return false;
        }
        Object this$deptId = getDeptId();Object other$deptId = other.getDeptId();
        if (this$deptId == null ? other$deptId != null : !this$deptId.equals(other$deptId)) {
            return false;
        }
        Object this$deptName = getDeptName();Object other$deptName = other.getDeptName();
        if (this$deptName == null ? other$deptName != null : !this$deptName.equals(other$deptName)) {
            return false;
        }
        Object this$birthdayName = getBirthdayName();Object other$birthdayName = other.getBirthdayName();
        if (this$birthdayName == null ? other$birthdayName != null : !this$birthdayName.equals(other$birthdayName)) {
            return false;
        }
        Object this$interfaceUserId = getInterfaceUserId();Object other$interfaceUserId = other.getInterfaceUserId();return this$interfaceUserId == null ? other$interfaceUserId == null : this$interfaceUserId.equals(other$interfaceUserId);
    }

    public String getId()
    {
        return this.id;
    }

    public String getUserId()
    {
        return this.userId;
    }

    public String getUserPassword()
    {
        return this.userPassword;
    }

    public String getSalt()
    {
        return this.salt;
    }

    public String getUserName()
    {
        return this.userName;
    }

    public String getRealName()
    {
        return this.realName;
    }

    public String getStatus()
    {
        return this.status;
    }

    public String getUserType()
    {
        return this.userType;
    }

    public String getUserTel()
    {
        return this.userTel;
    }

    public String getStatusVal()
    {
        return this.statusVal;
    }

    public String getIsDel()
    {
        return this.isDel;
    }

    public String getNumbers()
    {
        return this.numbers;
    }

    public String getUserApp()
    {
        return this.userApp;
    }

    public String getCreateTime()
    {
        return this.createTime;
    }

    public String getUpdateTime()
    {
        return this.updateTime;
    }

    public String getAvatarUrl()
    {
        return this.avatarUrl;
    }

    public String getStrength()
    {
        return this.strength;
    }

    public String getEmail()
    {
        return this.email;
    }

    public Date getBirthday()
    {
        return this.birthday;
    }

    public String getEducation()
    {
        return this.education;
    }

    public String getDegree()
    {
        return this.degree;
    }

    public String getAddress()
    {
        return this.address;
    }

    public String getSignature()
    {
        return this.signature;
    }

    public String getSex()
    {
        return this.sex;
    }

    public String getOldPwd()
    {
        return this.oldPwd;
    }

    public String getNewPwd()
    {
        return this.newPwd;
    }

    public String getAffirmPwd()
    {
        return this.affirmPwd;
    }

    public String getRoleId()
    {
        return this.roleId;
    }

    public String getRoleName()
    {
        return this.roleName;
    }

    public String getDeptId()
    {
        return this.deptId;
    }

    public String getDeptName()
    {
        return this.deptName;
    }

    public String getBirthdayName()
    {
        return this.birthdayName;
    }

    public String getInterfaceUserId()
    {
        return this.interfaceUserId;
    }

    public Integer getUserRoleId()
    {
        return this.userRoleId;
    }
}
