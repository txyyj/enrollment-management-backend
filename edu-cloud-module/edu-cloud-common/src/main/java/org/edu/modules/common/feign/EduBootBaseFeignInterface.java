package org.edu.modules.common.feign;

import org.edu.common.api.vo.Result;
import org.edu.modules.common.entity.VeBaseUserLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("edu-base")
@Component
public abstract interface EduBootBaseFeignInterface
{
    @GetMapping({"/base/veBase/userLogPageList"})
    public abstract Result<?> userLogPageList(@RequestParam(name="userLog") VeBaseUserLog paramVeBaseUserLog, @RequestParam(name="pageNo", defaultValue="1") Integer paramInteger1, @RequestParam(name="pageSize", defaultValue="10") Integer paramInteger2);

    @GetMapping({"/base/veBase/roleMenuList"})
    public abstract Result<?> roleMenuList();

    @GetMapping({"/base/veBase/userLogById"})
    public abstract Result<?> userLogById(@RequestParam(name="id", required=true) String paramString);
}
