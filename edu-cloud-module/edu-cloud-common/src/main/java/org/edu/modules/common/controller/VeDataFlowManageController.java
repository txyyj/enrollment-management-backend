package org.edu.modules.common.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.modules.common.entity.VeBaseDataApply;
import org.edu.modules.common.service.IVeBaseAppUserService;
import org.edu.modules.common.service.IVeBaseDataApplyColumnService;
import org.edu.modules.common.service.IVeBaseDataApplyService;
import org.edu.modules.common.vo.VeBaseDataApplyVo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags={"数据流程管理"})
@RestController
@RequestMapping({"/common/veFlowManage"})
@ApiSort(60)
public class VeDataFlowManageController
{
    private static final Logger log = LoggerFactory.getLogger(VeDataFlowManageController.class);
    @Autowired
    private IVeBaseDataApplyService veBaseDataApplyService;
    @Autowired
    private IVeBaseAppUserService veBaseAppUserService;
    @Autowired
    private IVeBaseDataApplyColumnService veBaseDataApplyColumnService;

    @AutoLog("数据申请-分页列表查询")
    @ApiOperation(value="数据申请-分页列表查询", notes="数据申请-分页列表查询")
    @GetMapping({"/queryDataApplyPageList"})
    public Result<?> queryDataApplyPageList(VeBaseDataApply veBaseDataApply, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseDataApplyService.queryDataApplyPageList(veBaseDataApply);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.ok(pageInfo);
    }

    @AutoLog("数据待审批-分页列表查询")
    @ApiOperation(value="数据待审批-分页列表查询", notes="数据待审批-分页列表查询")
    @GetMapping({"/queryNotCheckDataApplyPageList"})
    public Result<?> queryNotCheckDataApplyPageList(VeBaseDataApply veBaseDataApply, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseDataApplyService.queryNotCheckDataApplyPageList(veBaseDataApply);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.ok(pageInfo);
    }

    @AutoLog("已审批数据-分页列表查询")
    @ApiOperation(value="已审批数据-分页列表查询", notes="已审批数据-分页列表查询")
    @GetMapping({"/queryDataApplyProcessedPageList"})
    public Result<?> queryDataApplyProcessedPageList(VeBaseDataApply veBaseDataApply, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseDataApplyService.queryDataApplyProcessedPageList(veBaseDataApply);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.ok(pageInfo);
    }

    @AutoLog("获取所有表信息-列表")
    @ApiOperation(value="获取所有表信息-列表", notes="获取所有表信息-列表")
    @GetMapping({"/queryTableNameList"})
    public Result<?> queryTableNameList()
    {
        List<Map<String, Object>> list = this.veBaseAppUserService.queryResourcePageList();
        return Result.ok(list);
    }

    @AutoLog("根据表名获取字段信息列表")
    @ApiOperation(value="根据表名获取字段信息列表", notes="根据表名获取字段信息列表")
    @GetMapping({"/queryColumnNameListByTableName"})
    public Result<?> queryColumnNameListByTableName(@RequestParam(name="tableName", required=true) String tableName)
    {
        List<Map<String, Object>> list = this.veBaseAppUserService.queryColumnNameListByTableName(tableName);
        return Result.ok(list);
    }

    @AutoLog("添加数据申请表")
    @ApiOperation(value="添加数据申请表", notes="添加数据申请表")
    @PostMapping({"/addDataApply"})
    @Transactional
    public Result<?> addDataApply(@RequestBody VeBaseDataApply veBaseDataApply)
    {
        if (veBaseDataApply == null) {
            return Result.error("添加失败! ");
        }
        veBaseDataApply.setStatus("0");

        this.veBaseDataApplyService.save(veBaseDataApply);
        if ((veBaseDataApply.getColumnName() != null) && (veBaseDataApply.getColumnName().size() > 0)) {
            this.veBaseDataApplyColumnService.addDataApplyColumn(veBaseDataApply.getId(), veBaseDataApply.getTableName(), veBaseDataApply.getColumnName());
        }
        return Result.ok("添加成功! ");
    }

    @AutoLog("编辑回显数据申请表")
    @ApiOperation(value="编辑回显数据申请表", notes="编辑回显数据申请表")
    @GetMapping({"/updateEchoDataApply"})
    public Result<?> updateEchoDataApply(int id)
    {
        if (id != 0)
        {
            VeBaseDataApplyVo veBaseDataApplyVo = this.veBaseDataApplyService.editEchoDataApply(id);
            return Result.OK(veBaseDataApplyVo);
        }
        return Result.error("编辑失败");
    }

    @AutoLog("编辑数据申请表")
    @ApiOperation(value="编辑数据申请表", notes="编辑数据申请表")
    @PostMapping({"/editDataApply"})
    @Transactional
    public Result<?> editDataApply(@RequestBody VeBaseDataApply veBaseDataApply)
    {
        if (veBaseDataApply == null) {
            return Result.error("编辑失败! ");
        }
        this.veBaseDataApplyService.updateById(veBaseDataApply);

        this.veBaseDataApplyColumnService.deleteDataApplyColumnByApplyId(veBaseDataApply.getId());
        if (veBaseDataApply.getColumnName().size() > 0) {
            this.veBaseDataApplyColumnService.addDataApplyColumn(veBaseDataApply.getId(), veBaseDataApply.getTableName(), veBaseDataApply.getColumnName());
        }
        return Result.ok("编辑成功!");
    }

    @AutoLog("数据申请表-通过id删除")
    @ApiOperation(value="数据申请表-通过id删除", notes="数据申请表-通过id删除")
    @PostMapping({"/deleteDataApply"})
    public Result<?> deleteDataApply(@RequestParam(name="id", required=true) String id)
    {
        this.veBaseDataApplyService.removeById(id);
        this.veBaseDataApplyColumnService.deleteDataApplyColumnByApplyId(id);
        return Result.OK("删除成功!");
    }

    @AutoLog("数据申请审批-通过")
    @ApiOperation(value="数据申请审批-通过", notes="数据申请审批-通过")
    @PostMapping({"/approveDataApply"})
    public Result<?> approveDataApply(@RequestParam(name="ids", required=true) String ids)
    {
        List<String> idList = Arrays.asList(ids.split(","));
        if (idList.size() > 0) {
            for (String id : idList)
            {
                VeBaseDataApply veBaseDataApply = (VeBaseDataApply)this.veBaseDataApplyService.getById(id);
                if (veBaseDataApply == null) {
                    return Result.error("未找到数据申请！");
                }
                veBaseDataApply.setStatus("1");
                this.veBaseDataApplyService.updateById(veBaseDataApply);
            }
        }
        return Result.OK("审批成功！");
    }

    @AutoLog("数据申请审批-未通过")
    @ApiOperation(value="数据申请审批-未通过", notes="数据申请审批-未通过")
    @PostMapping({"/noPassDataApply"})
    public Result<?> noPassDataApply(@RequestParam(name="ids", required=true) String ids)
    {
        List<String> idList = Arrays.asList(ids.split(","));
        if (idList.size() > 0) {
            for (String id : idList)
            {
                VeBaseDataApply veBaseDataApply = (VeBaseDataApply)this.veBaseDataApplyService.getById(id);
                if (veBaseDataApply == null) {
                    return Result.error("未找到数据申请！");
                }
                veBaseDataApply.setStatus("2");
                this.veBaseDataApplyService.updateById(veBaseDataApply);
            }
        }
        return Result.OK("审批成功！");
    }
}
