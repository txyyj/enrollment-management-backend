package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_dictionary")
@ApiModel(value="ve_base_dictionary对象", description="数据字典管理")
public class VeBaseDictionary
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private int id;
    @Excel(name="字典名称", width=15.0D)
    @ApiModelProperty("字典名称")
    private String title;
    @Excel(name="字典编码", width=15.0D)
    @ApiModelProperty("字典编码")
    private String code;
    @Excel(name="排序", width=15.0D)
    @ApiModelProperty("排序")
    private int listSort;
    @Excel(name="父id", width=15.0D)
    @ApiModelProperty("父id")
    private int pid;
    @Excel(name="终端Id", width=15.0D)
    @ApiModelProperty("终端Id")
    private int terminalId;
    @TableField(exist=false)
    List<VeBaseDictionary> veBaseDictionaryList;
    @TableField(exist=false)
    private String interfaceUserId;

    public VeBaseDictionary setCode(String code)
    {
        this.code = code;return this;
    }

    public VeBaseDictionary setTitle(String title)
    {
        this.title = title;return this;
    }

    public VeBaseDictionary setId(int id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseDictionary(id=" + getId() + ", title=" + getTitle() + ", code=" + getCode() + ", listSort=" + getListSort() + ", pid=" + getPid() + ", terminalId=" + getTerminalId() + ", veBaseDictionaryList=" + getVeBaseDictionaryList() + ", interfaceUserId=" + getInterfaceUserId() + ")";
    }

    public VeBaseDictionary setInterfaceUserId(String interfaceUserId)
    {
        this.interfaceUserId = interfaceUserId;return this;
    }

    public VeBaseDictionary setVeBaseDictionaryList(List<VeBaseDictionary> veBaseDictionaryList)
    {
        this.veBaseDictionaryList = veBaseDictionaryList;return this;
    }

    public VeBaseDictionary setTerminalId(int terminalId)
    {
        this.terminalId = terminalId;return this;
    }

    public VeBaseDictionary setPid(int pid)
    {
        this.pid = pid;return this;
    }

    public VeBaseDictionary setListSort(int listSort)
    {
        this.listSort = listSort;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;result = result * 59 + getId();result = result * 59 + getListSort();result = result * 59 + getPid();result = result * 59 + getTerminalId();Object $title = getTitle();result = result * 59 + ($title == null ? 43 : $title.hashCode());Object $code = getCode();result = result * 59 + ($code == null ? 43 : $code.hashCode());Object $veBaseDictionaryList = getVeBaseDictionaryList();result = result * 59 + ($veBaseDictionaryList == null ? 43 : $veBaseDictionaryList.hashCode());Object $interfaceUserId = getInterfaceUserId();result = result * 59 + ($interfaceUserId == null ? 43 : $interfaceUserId.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseDictionary;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseDictionary)) {
            return false;
        }
        VeBaseDictionary other = (VeBaseDictionary)o;
        if (!other.canEqual(this)) {
            return false;
        }
        if (getId() != other.getId()) {
            return false;
        }
        if (getListSort() != other.getListSort()) {
            return false;
        }
        if (getPid() != other.getPid()) {
            return false;
        }
        if (getTerminalId() != other.getTerminalId()) {
            return false;
        }
        Object this$title = getTitle();Object other$title = other.getTitle();
        if (this$title == null ? other$title != null : !this$title.equals(other$title)) {
            return false;
        }
        Object this$code = getCode();Object other$code = other.getCode();
        if (this$code == null ? other$code != null : !this$code.equals(other$code)) {
            return false;
        }
        Object this$veBaseDictionaryList = getVeBaseDictionaryList();Object other$veBaseDictionaryList = other.getVeBaseDictionaryList();
        if (this$veBaseDictionaryList == null ? other$veBaseDictionaryList != null : !this$veBaseDictionaryList.equals(other$veBaseDictionaryList)) {
            return false;
        }
        Object this$interfaceUserId = getInterfaceUserId();Object other$interfaceUserId = other.getInterfaceUserId();return this$interfaceUserId == null ? other$interfaceUserId == null : this$interfaceUserId.equals(other$interfaceUserId);
    }

    public int getId()
    {
        return this.id;
    }

    public String getTitle()
    {
        return this.title;
    }

    public String getCode()
    {
        return this.code;
    }

    public int getListSort()
    {
        return this.listSort;
    }

    public int getPid()
    {
        return this.pid;
    }

    public int getTerminalId()
    {
        return this.terminalId;
    }

    public List<VeBaseDictionary> getVeBaseDictionaryList()
    {
        return this.veBaseDictionaryList;
    }

    public String getInterfaceUserId()
    {
        return this.interfaceUserId;
    }
}
