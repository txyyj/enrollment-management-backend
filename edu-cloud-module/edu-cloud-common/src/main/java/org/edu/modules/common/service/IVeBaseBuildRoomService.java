package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseBuildRoom;

public abstract interface IVeBaseBuildRoomService
        extends IService<VeBaseBuildRoom>
{
    public abstract List<Map<String, Object>> getBuildRoomPageList(VeBaseBuildRoom paramVeBaseBuildRoom);

    public abstract VeBaseBuildRoom getBuildRoomByName(Integer paramInteger, String paramString);

    public abstract List<VeBaseBuildRoom> getRoomListByBuildId(Integer paramInteger);
}

