package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseBuild;

public abstract interface IVeBaseBuildService
        extends IService<VeBaseBuild>
{
    public abstract VeBaseBuild getBuildByName(Integer paramInteger, String paramString);

    public abstract List<VeBaseBuild> queryBuildListByCampusId(Integer paramInteger);

    public abstract List<Map<String, Object>> queryJianzhuByCampusId(Integer paramInteger);

    public abstract List<Map<String, Object>> queryRoomListByBuildId(Integer paramInteger);
}
