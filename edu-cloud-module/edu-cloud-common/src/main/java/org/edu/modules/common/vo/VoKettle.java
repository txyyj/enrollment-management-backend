package org.edu.modules.common.vo;

import java.util.Arrays;

public class VoKettle
{
    private String host1;
    private String db1;
    private String host2;
    private String db2;
    private String sql;
    private String tableName;
    private String id1;
    private String id2;
    private String[] columns1;
    private String[] columns2;
    private String interfaceUserId;

    public void setHost2(String host2)
    {
        this.host2 = host2;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $host1 = getHost1();result = result * 59 + ($host1 == null ? 43 : $host1.hashCode());Object $db1 = getDb1();result = result * 59 + ($db1 == null ? 43 : $db1.hashCode());Object $host2 = getHost2();result = result * 59 + ($host2 == null ? 43 : $host2.hashCode());Object $db2 = getDb2();result = result * 59 + ($db2 == null ? 43 : $db2.hashCode());Object $sql = getSql();result = result * 59 + ($sql == null ? 43 : $sql.hashCode());Object $tableName = getTableName();result = result * 59 + ($tableName == null ? 43 : $tableName.hashCode());Object $id1 = getId1();result = result * 59 + ($id1 == null ? 43 : $id1.hashCode());Object $id2 = getId2();result = result * 59 + ($id2 == null ? 43 : $id2.hashCode());result = result * 59 + Arrays.deepHashCode(getColumns1());result = result * 59 + Arrays.deepHashCode(getColumns2());Object $interfaceUserId = getInterfaceUserId();result = result * 59 + ($interfaceUserId == null ? 43 : $interfaceUserId.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VoKettle;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VoKettle)) {
            return false;
        }
        VoKettle other = (VoKettle)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$host1 = getHost1();Object other$host1 = other.getHost1();
        if (this$host1 == null ? other$host1 != null : !this$host1.equals(other$host1)) {
            return false;
        }
        Object this$db1 = getDb1();Object other$db1 = other.getDb1();
        if (this$db1 == null ? other$db1 != null : !this$db1.equals(other$db1)) {
            return false;
        }
        Object this$host2 = getHost2();Object other$host2 = other.getHost2();
        if (this$host2 == null ? other$host2 != null : !this$host2.equals(other$host2)) {
            return false;
        }
        Object this$db2 = getDb2();Object other$db2 = other.getDb2();
        if (this$db2 == null ? other$db2 != null : !this$db2.equals(other$db2)) {
            return false;
        }
        Object this$sql = getSql();Object other$sql = other.getSql();
        if (this$sql == null ? other$sql != null : !this$sql.equals(other$sql)) {
            return false;
        }
        Object this$tableName = getTableName();Object other$tableName = other.getTableName();
        if (this$tableName == null ? other$tableName != null : !this$tableName.equals(other$tableName)) {
            return false;
        }
        Object this$id1 = getId1();Object other$id1 = other.getId1();
        if (this$id1 == null ? other$id1 != null : !this$id1.equals(other$id1)) {
            return false;
        }
        Object this$id2 = getId2();Object other$id2 = other.getId2();
        if (this$id2 == null ? other$id2 != null : !this$id2.equals(other$id2)) {
            return false;
        }
        if (!Arrays.deepEquals(getColumns1(), other.getColumns1())) {
            return false;
        }
        if (!Arrays.deepEquals(getColumns2(), other.getColumns2())) {
            return false;
        }
        Object this$interfaceUserId = getInterfaceUserId();Object other$interfaceUserId = other.getInterfaceUserId();return this$interfaceUserId == null ? other$interfaceUserId == null : this$interfaceUserId.equals(other$interfaceUserId);
    }

    public void setInterfaceUserId(String interfaceUserId)
    {
        this.interfaceUserId = interfaceUserId;
    }

    public void setColumns2(String[] columns2)
    {
        this.columns2 = columns2;
    }

    public void setColumns1(String[] columns1)
    {
        this.columns1 = columns1;
    }

    public void setId2(String id2)
    {
        this.id2 = id2;
    }

    public void setId1(String id1)
    {
        this.id1 = id1;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public void setSql(String sql)
    {
        this.sql = sql;
    }

    public void setHost1(String host1)
    {
        this.host1 = host1;
    }

    public void setDb1(String db1)
    {
        this.db1 = db1;
    }

    public String toString()
    {
        return "VoKettle(host1=" + getHost1() + ", db1=" + getDb1() + ", host2=" + getHost2() + ", db2=" + getDb2() + ", sql=" + getSql() + ", tableName=" + getTableName() + ", id1=" + getId1() + ", id2=" + getId2() + ", columns1=" + Arrays.deepToString(getColumns1()) + ", columns2=" + Arrays.deepToString(getColumns2()) + ", interfaceUserId=" + getInterfaceUserId() + ")";
    }

    public void setDb2(String db2)
    {
        this.db2 = db2;
    }

    public String getHost1()
    {
        return this.host1;
    }

    public String getDb1()
    {
        return this.db1;
    }

    public String getHost2()
    {
        return this.host2;
    }

    public String getDb2()
    {
        return this.db2;
    }

    public String getSql()
    {
        return this.sql;
    }

    public String getTableName()
    {
        return this.tableName;
    }

    public String getId1()
    {
        return this.id1;
    }

    public String getId2()
    {
        return this.id2;
    }

    public String[] getColumns1()
    {
        return this.columns1;
    }

    public String[] getColumns2()
    {
        return this.columns2;
    }

    public String getInterfaceUserId()
    {
        return this.interfaceUserId;
    }
}

