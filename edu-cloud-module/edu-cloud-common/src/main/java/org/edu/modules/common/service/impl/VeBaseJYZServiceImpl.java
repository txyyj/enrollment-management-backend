package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.common.entity.VeBaseJYZ;
import org.edu.modules.common.mapper.VeBaseJYZMapper;
import org.edu.modules.common.service.IVeBaseJYZService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Auther 李少君
 * @Date 2021-12-13 16:26
 */
@Service
public class VeBaseJYZServiceImpl extends ServiceImpl<VeBaseJYZMapper, VeBaseJYZ> implements IVeBaseJYZService {

    @Autowired
    private VeBaseJYZMapper veBaseJYZMapper;

    @Override
    public List<Map<String, Object>> getJYZTreeList() {
        return null;
    }

    @Override
    public VeBaseJYZ getJYZByName(Integer paramInteger, String paramString) {
        return veBaseJYZMapper.getJYZByName(paramInteger, paramString);
    }

    @Override
    public VeBaseJYZ getJYZByCode(Integer paramInteger, String paramString) {
        return veBaseJYZMapper.getJYZByCode(paramInteger, paramString);
    }

    @Override
    public Map getJYZById(Integer paramInteger) {
        return veBaseJYZMapper.getJYZById(paramInteger);
    }
}
