package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseGrade;

public abstract interface VeBaseGradeMapper
        extends BaseMapper<VeBaseGrade>
{
    public abstract List<Map<String, Object>> getGradePageList(VeBaseGrade paramVeBaseGrade);

    public abstract VeBaseGrade getGradeByName(Integer paramInteger, String paramString);

    public abstract VeBaseGrade getGradeByCode(Integer paramInteger, String paramString);

    public abstract VeBaseGrade getGradeByRxnf(Integer paramInteger1, Integer paramInteger2);
}
