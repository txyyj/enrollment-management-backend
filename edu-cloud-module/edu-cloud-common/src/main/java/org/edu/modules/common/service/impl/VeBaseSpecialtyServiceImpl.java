package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.common.entity.VeBaseSpecialty;
import org.edu.modules.common.mapper.VeBaseSpecialtyMapper;
import org.edu.modules.common.service.IVeBaseSpecialtyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @Auther 李少君
 * @Date 2021-12-13 16:27
 */
@Service
public class VeBaseSpecialtyServiceImpl extends ServiceImpl<VeBaseSpecialtyMapper, VeBaseSpecialty> implements IVeBaseSpecialtyService {

    @Autowired
    private VeBaseSpecialtyMapper veBaseSpecialtyMapper;

    @Override
    public List<VeBaseSpecialty> specialtyListByFalid(Integer paramInteger) {
        return null;
    }

    @Override
    public List<Map<String, Object>> getSpecialtyPageList(VeBaseSpecialty paramVeBaseSpecialty) {
        return veBaseSpecialtyMapper.getSpecialtyPageList(paramVeBaseSpecialty);
    }

    @Override
    public VeBaseSpecialty getSpecialtyByName(Integer paramInteger, String paramString) {
        return veBaseSpecialtyMapper.getSpecialtyByName(paramInteger, paramString);
    }

    @Override
    public VeBaseSpecialty getSpecialtyByBH(Integer paramInteger, String paramString) {
        return veBaseSpecialtyMapper.getSpecialtyByBH(paramInteger, paramString);
    }

    @Override
    public List<Map<String, Object>> getSpecialtyStudentStatistics() {
        return veBaseSpecialtyMapper.getSpecialtyStudentStatistics();
    }

    @Override
    public List<Map<String, Object>> getSpecialtyTreeList() {
        return null;
    }

    @Override
    public List<Map<String, Object>> getSpecialtyByZYMC(String paramString) {
        return veBaseSpecialtyMapper.getSpecialtyByZYMC(paramString);
    }

    @Override
    public int stopSpecialtyBatch(String[] paramArrayOfString) {
        List<String> list = new ArrayList<>();
        for (String s : paramArrayOfString){
            list.add(s);
        }
        return veBaseSpecialtyMapper.stopSpecialtyBatch(list);
    }

    @Override
    public int deleteSpecialtyBatch(String[] paramArrayOfString) {
        List<String> list = new ArrayList<>();
        for (String s : paramArrayOfString){
            list.add(s);
        }
        return veBaseSpecialtyMapper.deleteSpecialtyBatch(list);
    }

    @Override
    public int deleteSpecialtyByFalId(String[] paramArrayOfString) {
        return veBaseSpecialtyMapper.deleteSpecialtyByFalId(paramArrayOfString);
    }

    @Override
    public int stopSpecialtyByFalId(String[] paramArrayOfString) {
        return veBaseSpecialtyMapper.stopSpecialtyByFalId(paramArrayOfString);
    }

    @Override
    public int updateSpecialtyFalIdById(Integer paramInteger1, Integer paramInteger2) {
        List<Integer> list = new ArrayList<>();
        list.add(paramInteger1);
        return veBaseSpecialtyMapper.updateSpecialtyFalIdById(list,paramInteger2);
    }

    @Override
    public List<Map<String, Object>> getGraduatePeopleCount(String paramString) {
        return veBaseSpecialtyMapper.getGraduatePeopleCount(paramString);
    }
}
