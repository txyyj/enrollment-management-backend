package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_transformation")
@ApiModel(value="ve_base_transformation对象", description="数据采集任务表")
public class VeBaseTransformation
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private Double idTransformation;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Integer idDirectory;
    @Excel(name="名称", width=15.0D)
    @ApiModelProperty("名称")
    private String name;
    @Excel(name="描述", width=15.0D)
    @ApiModelProperty("描述")
    private String description;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private String extendedDescription;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private String transVersion;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Integer transStatus;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Integer idStepRead;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Integer idStepWrite;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Integer idStepInput;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Integer idStepOutput;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Integer idStepUpdate;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Integer idDatabaseLog;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private String tableNameLog;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Integer userBatchid;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Integer useLogfield;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Integer idDatabaseMaxdate;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private String tableNameMaxdate;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private String fieldNameMaxdate;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Double offsetMaxdate;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Double diffMaxdate;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private String createdUser;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Date createdDate;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private String modifiedUser;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Date modifiedDate;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private Integer sizeRowset;

    public VeBaseTransformation setName(String name)
    {
        this.name = name;return this;
    }

    public VeBaseTransformation setIdDirectory(Integer idDirectory)
    {
        this.idDirectory = idDirectory;return this;
    }

    public VeBaseTransformation setIdTransformation(Double idTransformation)
    {
        this.idTransformation = idTransformation;return this;
    }

    public String toString()
    {
        return "VeBaseTransformation(idTransformation=" + getIdTransformation() + ", idDirectory=" + getIdDirectory() + ", name=" + getName() + ", description=" + getDescription() + ", extendedDescription=" + getExtendedDescription() + ", transVersion=" + getTransVersion() + ", transStatus=" + getTransStatus() + ", idStepRead=" + getIdStepRead() + ", idStepWrite=" + getIdStepWrite() + ", idStepInput=" + getIdStepInput() + ", idStepOutput=" + getIdStepOutput() + ", idStepUpdate=" + getIdStepUpdate() + ", idDatabaseLog=" + getIdDatabaseLog() + ", tableNameLog=" + getTableNameLog() + ", userBatchid=" + getUserBatchid() + ", useLogfield=" + getUseLogfield() + ", idDatabaseMaxdate=" + getIdDatabaseMaxdate() + ", tableNameMaxdate=" + getTableNameMaxdate() + ", fieldNameMaxdate=" + getFieldNameMaxdate() + ", offsetMaxdate=" + getOffsetMaxdate() + ", diffMaxdate=" + getDiffMaxdate() + ", createdUser=" + getCreatedUser() + ", createdDate=" + getCreatedDate() + ", modifiedUser=" + getModifiedUser() + ", modifiedDate=" + getModifiedDate() + ", sizeRowset=" + getSizeRowset() + ")";
    }

    public VeBaseTransformation setSizeRowset(Integer sizeRowset)
    {
        this.sizeRowset = sizeRowset;return this;
    }

    public VeBaseTransformation setModifiedDate(Date modifiedDate)
    {
        this.modifiedDate = modifiedDate;return this;
    }

    public VeBaseTransformation setModifiedUser(String modifiedUser)
    {
        this.modifiedUser = modifiedUser;return this;
    }

    public VeBaseTransformation setCreatedDate(Date createdDate)
    {
        this.createdDate = createdDate;return this;
    }

    public VeBaseTransformation setCreatedUser(String createdUser)
    {
        this.createdUser = createdUser;return this;
    }

    public VeBaseTransformation setDiffMaxdate(Double diffMaxdate)
    {
        this.diffMaxdate = diffMaxdate;return this;
    }

    public VeBaseTransformation setOffsetMaxdate(Double offsetMaxdate)
    {
        this.offsetMaxdate = offsetMaxdate;return this;
    }

    public VeBaseTransformation setFieldNameMaxdate(String fieldNameMaxdate)
    {
        this.fieldNameMaxdate = fieldNameMaxdate;return this;
    }

    public VeBaseTransformation setTableNameMaxdate(String tableNameMaxdate)
    {
        this.tableNameMaxdate = tableNameMaxdate;return this;
    }

    public VeBaseTransformation setIdDatabaseMaxdate(Integer idDatabaseMaxdate)
    {
        this.idDatabaseMaxdate = idDatabaseMaxdate;return this;
    }

    public VeBaseTransformation setUseLogfield(Integer useLogfield)
    {
        this.useLogfield = useLogfield;return this;
    }

    public VeBaseTransformation setUserBatchid(Integer userBatchid)
    {
        this.userBatchid = userBatchid;return this;
    }

    public VeBaseTransformation setTableNameLog(String tableNameLog)
    {
        this.tableNameLog = tableNameLog;return this;
    }

    public VeBaseTransformation setIdDatabaseLog(Integer idDatabaseLog)
    {
        this.idDatabaseLog = idDatabaseLog;return this;
    }

    public VeBaseTransformation setIdStepUpdate(Integer idStepUpdate)
    {
        this.idStepUpdate = idStepUpdate;return this;
    }

    public VeBaseTransformation setIdStepOutput(Integer idStepOutput)
    {
        this.idStepOutput = idStepOutput;return this;
    }

    public VeBaseTransformation setIdStepInput(Integer idStepInput)
    {
        this.idStepInput = idStepInput;return this;
    }

    public VeBaseTransformation setIdStepWrite(Integer idStepWrite)
    {
        this.idStepWrite = idStepWrite;return this;
    }

    public VeBaseTransformation setIdStepRead(Integer idStepRead)
    {
        this.idStepRead = idStepRead;return this;
    }

    public VeBaseTransformation setTransStatus(Integer transStatus)
    {
        this.transStatus = transStatus;return this;
    }

    public VeBaseTransformation setTransVersion(String transVersion)
    {
        this.transVersion = transVersion;return this;
    }

    public VeBaseTransformation setExtendedDescription(String extendedDescription)
    {
        this.extendedDescription = extendedDescription;return this;
    }

    public VeBaseTransformation setDescription(String description)
    {
        this.description = description;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $idTransformation = getIdTransformation();result = result * 59 + ($idTransformation == null ? 43 : $idTransformation.hashCode());Object $idDirectory = getIdDirectory();result = result * 59 + ($idDirectory == null ? 43 : $idDirectory.hashCode());Object $transStatus = getTransStatus();result = result * 59 + ($transStatus == null ? 43 : $transStatus.hashCode());Object $idStepRead = getIdStepRead();result = result * 59 + ($idStepRead == null ? 43 : $idStepRead.hashCode());Object $idStepWrite = getIdStepWrite();result = result * 59 + ($idStepWrite == null ? 43 : $idStepWrite.hashCode());Object $idStepInput = getIdStepInput();result = result * 59 + ($idStepInput == null ? 43 : $idStepInput.hashCode());Object $idStepOutput = getIdStepOutput();result = result * 59 + ($idStepOutput == null ? 43 : $idStepOutput.hashCode());Object $idStepUpdate = getIdStepUpdate();result = result * 59 + ($idStepUpdate == null ? 43 : $idStepUpdate.hashCode());Object $idDatabaseLog = getIdDatabaseLog();result = result * 59 + ($idDatabaseLog == null ? 43 : $idDatabaseLog.hashCode());Object $userBatchid = getUserBatchid();result = result * 59 + ($userBatchid == null ? 43 : $userBatchid.hashCode());Object $useLogfield = getUseLogfield();result = result * 59 + ($useLogfield == null ? 43 : $useLogfield.hashCode());Object $idDatabaseMaxdate = getIdDatabaseMaxdate();result = result * 59 + ($idDatabaseMaxdate == null ? 43 : $idDatabaseMaxdate.hashCode());Object $offsetMaxdate = getOffsetMaxdate();result = result * 59 + ($offsetMaxdate == null ? 43 : $offsetMaxdate.hashCode());Object $diffMaxdate = getDiffMaxdate();result = result * 59 + ($diffMaxdate == null ? 43 : $diffMaxdate.hashCode());Object $sizeRowset = getSizeRowset();result = result * 59 + ($sizeRowset == null ? 43 : $sizeRowset.hashCode());Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());Object $description = getDescription();result = result * 59 + ($description == null ? 43 : $description.hashCode());Object $extendedDescription = getExtendedDescription();result = result * 59 + ($extendedDescription == null ? 43 : $extendedDescription.hashCode());Object $transVersion = getTransVersion();result = result * 59 + ($transVersion == null ? 43 : $transVersion.hashCode());Object $tableNameLog = getTableNameLog();result = result * 59 + ($tableNameLog == null ? 43 : $tableNameLog.hashCode());Object $tableNameMaxdate = getTableNameMaxdate();result = result * 59 + ($tableNameMaxdate == null ? 43 : $tableNameMaxdate.hashCode());Object $fieldNameMaxdate = getFieldNameMaxdate();result = result * 59 + ($fieldNameMaxdate == null ? 43 : $fieldNameMaxdate.hashCode());Object $createdUser = getCreatedUser();result = result * 59 + ($createdUser == null ? 43 : $createdUser.hashCode());Object $createdDate = getCreatedDate();result = result * 59 + ($createdDate == null ? 43 : $createdDate.hashCode());Object $modifiedUser = getModifiedUser();result = result * 59 + ($modifiedUser == null ? 43 : $modifiedUser.hashCode());Object $modifiedDate = getModifiedDate();result = result * 59 + ($modifiedDate == null ? 43 : $modifiedDate.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseTransformation;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseTransformation)) {
            return false;
        }
        VeBaseTransformation other = (VeBaseTransformation)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$idTransformation = getIdTransformation();Object other$idTransformation = other.getIdTransformation();
        if (this$idTransformation == null ? other$idTransformation != null : !this$idTransformation.equals(other$idTransformation)) {
            return false;
        }
        Object this$idDirectory = getIdDirectory();Object other$idDirectory = other.getIdDirectory();
        if (this$idDirectory == null ? other$idDirectory != null : !this$idDirectory.equals(other$idDirectory)) {
            return false;
        }
        Object this$transStatus = getTransStatus();Object other$transStatus = other.getTransStatus();
        if (this$transStatus == null ? other$transStatus != null : !this$transStatus.equals(other$transStatus)) {
            return false;
        }
        Object this$idStepRead = getIdStepRead();Object other$idStepRead = other.getIdStepRead();
        if (this$idStepRead == null ? other$idStepRead != null : !this$idStepRead.equals(other$idStepRead)) {
            return false;
        }
        Object this$idStepWrite = getIdStepWrite();Object other$idStepWrite = other.getIdStepWrite();
        if (this$idStepWrite == null ? other$idStepWrite != null : !this$idStepWrite.equals(other$idStepWrite)) {
            return false;
        }
        Object this$idStepInput = getIdStepInput();Object other$idStepInput = other.getIdStepInput();
        if (this$idStepInput == null ? other$idStepInput != null : !this$idStepInput.equals(other$idStepInput)) {
            return false;
        }
        Object this$idStepOutput = getIdStepOutput();Object other$idStepOutput = other.getIdStepOutput();
        if (this$idStepOutput == null ? other$idStepOutput != null : !this$idStepOutput.equals(other$idStepOutput)) {
            return false;
        }
        Object this$idStepUpdate = getIdStepUpdate();Object other$idStepUpdate = other.getIdStepUpdate();
        if (this$idStepUpdate == null ? other$idStepUpdate != null : !this$idStepUpdate.equals(other$idStepUpdate)) {
            return false;
        }
        Object this$idDatabaseLog = getIdDatabaseLog();Object other$idDatabaseLog = other.getIdDatabaseLog();
        if (this$idDatabaseLog == null ? other$idDatabaseLog != null : !this$idDatabaseLog.equals(other$idDatabaseLog)) {
            return false;
        }
        Object this$userBatchid = getUserBatchid();Object other$userBatchid = other.getUserBatchid();
        if (this$userBatchid == null ? other$userBatchid != null : !this$userBatchid.equals(other$userBatchid)) {
            return false;
        }
        Object this$useLogfield = getUseLogfield();Object other$useLogfield = other.getUseLogfield();
        if (this$useLogfield == null ? other$useLogfield != null : !this$useLogfield.equals(other$useLogfield)) {
            return false;
        }
        Object this$idDatabaseMaxdate = getIdDatabaseMaxdate();Object other$idDatabaseMaxdate = other.getIdDatabaseMaxdate();
        if (this$idDatabaseMaxdate == null ? other$idDatabaseMaxdate != null : !this$idDatabaseMaxdate.equals(other$idDatabaseMaxdate)) {
            return false;
        }
        Object this$offsetMaxdate = getOffsetMaxdate();Object other$offsetMaxdate = other.getOffsetMaxdate();
        if (this$offsetMaxdate == null ? other$offsetMaxdate != null : !this$offsetMaxdate.equals(other$offsetMaxdate)) {
            return false;
        }
        Object this$diffMaxdate = getDiffMaxdate();Object other$diffMaxdate = other.getDiffMaxdate();
        if (this$diffMaxdate == null ? other$diffMaxdate != null : !this$diffMaxdate.equals(other$diffMaxdate)) {
            return false;
        }
        Object this$sizeRowset = getSizeRowset();Object other$sizeRowset = other.getSizeRowset();
        if (this$sizeRowset == null ? other$sizeRowset != null : !this$sizeRowset.equals(other$sizeRowset)) {
            return false;
        }
        Object this$name = getName();Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) {
            return false;
        }
        Object this$description = getDescription();Object other$description = other.getDescription();
        if (this$description == null ? other$description != null : !this$description.equals(other$description)) {
            return false;
        }
        Object this$extendedDescription = getExtendedDescription();Object other$extendedDescription = other.getExtendedDescription();
        if (this$extendedDescription == null ? other$extendedDescription != null : !this$extendedDescription.equals(other$extendedDescription)) {
            return false;
        }
        Object this$transVersion = getTransVersion();Object other$transVersion = other.getTransVersion();
        if (this$transVersion == null ? other$transVersion != null : !this$transVersion.equals(other$transVersion)) {
            return false;
        }
        Object this$tableNameLog = getTableNameLog();Object other$tableNameLog = other.getTableNameLog();
        if (this$tableNameLog == null ? other$tableNameLog != null : !this$tableNameLog.equals(other$tableNameLog)) {
            return false;
        }
        Object this$tableNameMaxdate = getTableNameMaxdate();Object other$tableNameMaxdate = other.getTableNameMaxdate();
        if (this$tableNameMaxdate == null ? other$tableNameMaxdate != null : !this$tableNameMaxdate.equals(other$tableNameMaxdate)) {
            return false;
        }
        Object this$fieldNameMaxdate = getFieldNameMaxdate();Object other$fieldNameMaxdate = other.getFieldNameMaxdate();
        if (this$fieldNameMaxdate == null ? other$fieldNameMaxdate != null : !this$fieldNameMaxdate.equals(other$fieldNameMaxdate)) {
            return false;
        }
        Object this$createdUser = getCreatedUser();Object other$createdUser = other.getCreatedUser();
        if (this$createdUser == null ? other$createdUser != null : !this$createdUser.equals(other$createdUser)) {
            return false;
        }
        Object this$createdDate = getCreatedDate();Object other$createdDate = other.getCreatedDate();
        if (this$createdDate == null ? other$createdDate != null : !this$createdDate.equals(other$createdDate)) {
            return false;
        }
        Object this$modifiedUser = getModifiedUser();Object other$modifiedUser = other.getModifiedUser();
        if (this$modifiedUser == null ? other$modifiedUser != null : !this$modifiedUser.equals(other$modifiedUser)) {
            return false;
        }
        Object this$modifiedDate = getModifiedDate();Object other$modifiedDate = other.getModifiedDate();return this$modifiedDate == null ? other$modifiedDate == null : this$modifiedDate.equals(other$modifiedDate);
    }

    public Double getIdTransformation()
    {
        return this.idTransformation;
    }

    public Integer getIdDirectory()
    {
        return this.idDirectory;
    }

    public String getName()
    {
        return this.name;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getExtendedDescription()
    {
        return this.extendedDescription;
    }

    public String getTransVersion()
    {
        return this.transVersion;
    }

    public Integer getTransStatus()
    {
        return this.transStatus;
    }

    public Integer getIdStepRead()
    {
        return this.idStepRead;
    }

    public Integer getIdStepWrite()
    {
        return this.idStepWrite;
    }

    public Integer getIdStepInput()
    {
        return this.idStepInput;
    }

    public Integer getIdStepOutput()
    {
        return this.idStepOutput;
    }

    public Integer getIdStepUpdate()
    {
        return this.idStepUpdate;
    }

    public Integer getIdDatabaseLog()
    {
        return this.idDatabaseLog;
    }

    public String getTableNameLog()
    {
        return this.tableNameLog;
    }

    public Integer getUserBatchid()
    {
        return this.userBatchid;
    }

    public Integer getUseLogfield()
    {
        return this.useLogfield;
    }

    public Integer getIdDatabaseMaxdate()
    {
        return this.idDatabaseMaxdate;
    }

    public String getTableNameMaxdate()
    {
        return this.tableNameMaxdate;
    }

    public String getFieldNameMaxdate()
    {
        return this.fieldNameMaxdate;
    }

    public Double getOffsetMaxdate()
    {
        return this.offsetMaxdate;
    }

    public Double getDiffMaxdate()
    {
        return this.diffMaxdate;
    }

    public String getCreatedUser()
    {
        return this.createdUser;
    }

    public Date getCreatedDate()
    {
        return this.createdDate;
    }

    public String getModifiedUser()
    {
        return this.modifiedUser;
    }

    public Date getModifiedDate()
    {
        return this.modifiedDate;
    }

    public Integer getSizeRowset()
    {
        return this.sizeRowset;
    }
}
