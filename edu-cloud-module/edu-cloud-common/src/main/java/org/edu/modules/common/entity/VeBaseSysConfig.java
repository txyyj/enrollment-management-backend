package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

@TableName("ve_base_sys_config")
@ApiModel(value="ve_base_sys_config对象", description="系统配置信息表")
public class VeBaseSysConfig
        implements Serializable
{
    @ApiModelProperty("配置ID")
    @TableId(type=IdType.AUTO)
    private Integer id;
    @ApiModelProperty("配置名称")
    private String name;
    @ApiModelProperty("配置类型")
    private Integer type;
    @ApiModelProperty("配置说明")
    private String title;
    @ApiModelProperty("配置分组")
    private Integer groups;
    @ApiModelProperty("配置值")
    private String extra;
    @ApiModelProperty("配置说明")
    private String remark;
    @ApiModelProperty("创建时间")
    private Integer createTime;
    @ApiModelProperty("更新时间")
    private Integer updateTime;
    @ApiModelProperty("状态")
    private Integer status;
    @ApiModelProperty("排序")
    private Integer listSort;
    @TableField(exist=false)
    private String interfaceUserId;

    public VeBaseSysConfig setType(Integer type)
    {
        this.type = type;return this;
    }

    public VeBaseSysConfig setName(String name)
    {
        this.name = name;return this;
    }

    public VeBaseSysConfig setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseSysConfig(id=" + getId() + ", name=" + getName() + ", type=" + getType() + ", title=" + getTitle() + ", groups=" + getGroups() + ", extra=" + getExtra() + ", remark=" + getRemark() + ", createTime=" + getCreateTime() + ", updateTime=" + getUpdateTime() + ", status=" + getStatus() + ", listSort=" + getListSort() + ", interfaceUserId=" + getInterfaceUserId() + ")";
    }

    public VeBaseSysConfig setInterfaceUserId(String interfaceUserId)
    {
        this.interfaceUserId = interfaceUserId;return this;
    }

    public VeBaseSysConfig setListSort(Integer listSort)
    {
        this.listSort = listSort;return this;
    }

    public VeBaseSysConfig setStatus(Integer status)
    {
        this.status = status;return this;
    }

    public VeBaseSysConfig setUpdateTime(Integer updateTime)
    {
        this.updateTime = updateTime;return this;
    }

    public VeBaseSysConfig setCreateTime(Integer createTime)
    {
        this.createTime = createTime;return this;
    }

    public VeBaseSysConfig setRemark(String remark)
    {
        this.remark = remark;return this;
    }

    public VeBaseSysConfig setExtra(String extra)
    {
        this.extra = extra;return this;
    }

    public VeBaseSysConfig setGroups(Integer groups)
    {
        this.groups = groups;return this;
    }

    public VeBaseSysConfig setTitle(String title)
    {
        this.title = title;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $type = getType();result = result * 59 + ($type == null ? 43 : $type.hashCode());Object $groups = getGroups();result = result * 59 + ($groups == null ? 43 : $groups.hashCode());Object $createTime = getCreateTime();result = result * 59 + ($createTime == null ? 43 : $createTime.hashCode());Object $updateTime = getUpdateTime();result = result * 59 + ($updateTime == null ? 43 : $updateTime.hashCode());Object $status = getStatus();result = result * 59 + ($status == null ? 43 : $status.hashCode());Object $listSort = getListSort();result = result * 59 + ($listSort == null ? 43 : $listSort.hashCode());Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());Object $title = getTitle();result = result * 59 + ($title == null ? 43 : $title.hashCode());Object $extra = getExtra();result = result * 59 + ($extra == null ? 43 : $extra.hashCode());Object $remark = getRemark();result = result * 59 + ($remark == null ? 43 : $remark.hashCode());Object $interfaceUserId = getInterfaceUserId();result = result * 59 + ($interfaceUserId == null ? 43 : $interfaceUserId.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseSysConfig;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseSysConfig)) {
            return false;
        }
        VeBaseSysConfig other = (VeBaseSysConfig)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$type = getType();Object other$type = other.getType();
        if (this$type == null ? other$type != null : !this$type.equals(other$type)) {
            return false;
        }
        Object this$groups = getGroups();Object other$groups = other.getGroups();
        if (this$groups == null ? other$groups != null : !this$groups.equals(other$groups)) {
            return false;
        }
        Object this$createTime = getCreateTime();Object other$createTime = other.getCreateTime();
        if (this$createTime == null ? other$createTime != null : !this$createTime.equals(other$createTime)) {
            return false;
        }
        Object this$updateTime = getUpdateTime();Object other$updateTime = other.getUpdateTime();
        if (this$updateTime == null ? other$updateTime != null : !this$updateTime.equals(other$updateTime)) {
            return false;
        }
        Object this$status = getStatus();Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) {
            return false;
        }
        Object this$listSort = getListSort();Object other$listSort = other.getListSort();
        if (this$listSort == null ? other$listSort != null : !this$listSort.equals(other$listSort)) {
            return false;
        }
        Object this$name = getName();Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) {
            return false;
        }
        Object this$title = getTitle();Object other$title = other.getTitle();
        if (this$title == null ? other$title != null : !this$title.equals(other$title)) {
            return false;
        }
        Object this$extra = getExtra();Object other$extra = other.getExtra();
        if (this$extra == null ? other$extra != null : !this$extra.equals(other$extra)) {
            return false;
        }
        Object this$remark = getRemark();Object other$remark = other.getRemark();
        if (this$remark == null ? other$remark != null : !this$remark.equals(other$remark)) {
            return false;
        }
        Object this$interfaceUserId = getInterfaceUserId();Object other$interfaceUserId = other.getInterfaceUserId();return this$interfaceUserId == null ? other$interfaceUserId == null : this$interfaceUserId.equals(other$interfaceUserId);
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public Integer getType()
    {
        return this.type;
    }

    public String getTitle()
    {
        return this.title;
    }

    public Integer getGroups()
    {
        return this.groups;
    }

    public String getExtra()
    {
        return this.extra;
    }

    public String getRemark()
    {
        return this.remark;
    }

    public Integer getCreateTime()
    {
        return this.createTime;
    }

    public Integer getUpdateTime()
    {
        return this.updateTime;
    }

    public Integer getStatus()
    {
        return this.status;
    }

    public Integer getListSort()
    {
        return this.listSort;
    }

    public String getInterfaceUserId()
    {
        return this.interfaceUserId;
    }
}
