package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseDataApplyColumn;
import org.edu.modules.common.mapper.VeBaseDataApplyColumnMapper;
import org.edu.modules.common.service.IVeBaseDataApplyColumnService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseDataApplyColumnServiceImpl
        extends ServiceImpl<VeBaseDataApplyColumnMapper, VeBaseDataApplyColumn>
        implements IVeBaseDataApplyColumnService
{
    @Autowired
    private VeBaseDataApplyColumnMapper veBaseDataApplyColumnMapper;

    public int addDataApplyColumn(String applyId, String tableName, List<Map<String, Object>> columnName)
    {
        return this.veBaseDataApplyColumnMapper.addDataApplyColumn(applyId, tableName, columnName);
    }

    public int deleteDataApplyColumnByApplyId(String applyId)
    {
        return this.veBaseDataApplyColumnMapper.deleteDataApplyColumnByApplyId(applyId);
    }
}
