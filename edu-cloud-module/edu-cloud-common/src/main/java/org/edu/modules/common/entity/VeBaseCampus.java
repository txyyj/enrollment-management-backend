package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_campus")
@ApiModel(value="ve_base_campus对象", description="校区信息表")
public class VeBaseCampus
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="校区代码", width=15.0D)
    @ApiModelProperty("校区代码")
    private String xqdm;
    @Excel(name="校区名称", width=15.0D)
    @ApiModelProperty("校区名称")
    private String xqmc;
    @Excel(name="校区地址", width=15.0D)
    @ApiModelProperty("校区地址")
    private String xqdz;
    @Excel(name="校区联系电话", width=15.0D)
    @ApiModelProperty("校区联系电话")
    private String xqlxdh;
    @Excel(name="校区传真电话", width=15.0D)
    @ApiModelProperty("校区传真电话")
    private String xqczdh;
    @Excel(name="校区负责人用户ID", width=15.0D)
    @ApiModelProperty("校区负责人用户ID")
    private String fzrUserId;
    @Excel(name="电子邮件", width=15.0D)
    @ApiModelProperty("电子邮件")
    private String dzyj;
    @Excel(name="校区邮政编码", width=15.0D)
    @ApiModelProperty("校区邮政编码")
    private String xqyzbm;
    @Excel(name="校区所在地行政区划码", width=15.0D)
    @ApiModelProperty("校区所在地行政区划码")
    private String xqszdxzqhm;
    @Excel(name="校区面积", width=15.0D)
    @ApiModelProperty("校区面积")
    private Double xqmj;
    @Excel(name="校区建筑面积", width=15.0D)
    @ApiModelProperty("校区建筑面积")
    private Double xqjzmj;
    @Excel(name="校区教学科研仪器设备总值", width=15.0D)
    @ApiModelProperty("校区教学科研仪器设备总值")
    private Double xqjxkysbzz;
    @Excel(name="校区固定资产总值", width=15.0D)
    @ApiModelProperty("校区固定资产总值")
    private Double xqgdzczz;
    @Excel(name="系统终端ID", width=15.0D)
    @ApiModelProperty("系统终端ID")
    private Integer terminalid;
    @Excel(name="fileid", width=15.0D)
    @ApiModelProperty("fileid")
    private Integer fileid;

    public VeBaseCampus setXqmc(String xqmc)
    {
        this.xqmc = xqmc;return this;
    }

    public VeBaseCampus setXqdm(String xqdm)
    {
        this.xqdm = xqdm;return this;
    }

    public VeBaseCampus setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseCampus(id=" + getId() + ", xqdm=" + getXqdm() + ", xqmc=" + getXqmc() + ", xqdz=" + getXqdz() + ", xqlxdh=" + getXqlxdh() + ", xqczdh=" + getXqczdh() + ", fzrUserId=" + getFzrUserId() + ", dzyj=" + getDzyj() + ", xqyzbm=" + getXqyzbm() + ", xqszdxzqhm=" + getXqszdxzqhm() + ", xqmj=" + getXqmj() + ", xqjzmj=" + getXqjzmj() + ", xqjxkysbzz=" + getXqjxkysbzz() + ", xqgdzczz=" + getXqgdzczz() + ", terminalid=" + getTerminalid() + ", fileid=" + getFileid() + ")";
    }

    public VeBaseCampus setFileid(Integer fileid)
    {
        this.fileid = fileid;return this;
    }

    public VeBaseCampus setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeBaseCampus setXqgdzczz(Double xqgdzczz)
    {
        this.xqgdzczz = xqgdzczz;return this;
    }

    public VeBaseCampus setXqjxkysbzz(Double xqjxkysbzz)
    {
        this.xqjxkysbzz = xqjxkysbzz;return this;
    }

    public VeBaseCampus setXqjzmj(Double xqjzmj)
    {
        this.xqjzmj = xqjzmj;return this;
    }

    public VeBaseCampus setXqmj(Double xqmj)
    {
        this.xqmj = xqmj;return this;
    }

    public VeBaseCampus setXqszdxzqhm(String xqszdxzqhm)
    {
        this.xqszdxzqhm = xqszdxzqhm;return this;
    }

    public VeBaseCampus setXqyzbm(String xqyzbm)
    {
        this.xqyzbm = xqyzbm;return this;
    }

    public VeBaseCampus setDzyj(String dzyj)
    {
        this.dzyj = dzyj;return this;
    }

    public VeBaseCampus setFzrUserId(String fzrUserId)
    {
        this.fzrUserId = fzrUserId;return this;
    }

    public VeBaseCampus setXqczdh(String xqczdh)
    {
        this.xqczdh = xqczdh;return this;
    }

    public VeBaseCampus setXqlxdh(String xqlxdh)
    {
        this.xqlxdh = xqlxdh;return this;
    }

    public VeBaseCampus setXqdz(String xqdz)
    {
        this.xqdz = xqdz;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $xqmj = getXqmj();result = result * 59 + ($xqmj == null ? 43 : $xqmj.hashCode());Object $xqjzmj = getXqjzmj();result = result * 59 + ($xqjzmj == null ? 43 : $xqjzmj.hashCode());Object $xqjxkysbzz = getXqjxkysbzz();result = result * 59 + ($xqjxkysbzz == null ? 43 : $xqjxkysbzz.hashCode());Object $xqgdzczz = getXqgdzczz();result = result * 59 + ($xqgdzczz == null ? 43 : $xqgdzczz.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $fileid = getFileid();result = result * 59 + ($fileid == null ? 43 : $fileid.hashCode());Object $xqdm = getXqdm();result = result * 59 + ($xqdm == null ? 43 : $xqdm.hashCode());Object $xqmc = getXqmc();result = result * 59 + ($xqmc == null ? 43 : $xqmc.hashCode());Object $xqdz = getXqdz();result = result * 59 + ($xqdz == null ? 43 : $xqdz.hashCode());Object $xqlxdh = getXqlxdh();result = result * 59 + ($xqlxdh == null ? 43 : $xqlxdh.hashCode());Object $xqczdh = getXqczdh();result = result * 59 + ($xqczdh == null ? 43 : $xqczdh.hashCode());Object $fzrUserId = getFzrUserId();result = result * 59 + ($fzrUserId == null ? 43 : $fzrUserId.hashCode());Object $dzyj = getDzyj();result = result * 59 + ($dzyj == null ? 43 : $dzyj.hashCode());Object $xqyzbm = getXqyzbm();result = result * 59 + ($xqyzbm == null ? 43 : $xqyzbm.hashCode());Object $xqszdxzqhm = getXqszdxzqhm();result = result * 59 + ($xqszdxzqhm == null ? 43 : $xqszdxzqhm.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseCampus;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseCampus)) {
            return false;
        }
        VeBaseCampus other = (VeBaseCampus)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$xqmj = getXqmj();Object other$xqmj = other.getXqmj();
        if (this$xqmj == null ? other$xqmj != null : !this$xqmj.equals(other$xqmj)) {
            return false;
        }
        Object this$xqjzmj = getXqjzmj();Object other$xqjzmj = other.getXqjzmj();
        if (this$xqjzmj == null ? other$xqjzmj != null : !this$xqjzmj.equals(other$xqjzmj)) {
            return false;
        }
        Object this$xqjxkysbzz = getXqjxkysbzz();Object other$xqjxkysbzz = other.getXqjxkysbzz();
        if (this$xqjxkysbzz == null ? other$xqjxkysbzz != null : !this$xqjxkysbzz.equals(other$xqjxkysbzz)) {
            return false;
        }
        Object this$xqgdzczz = getXqgdzczz();Object other$xqgdzczz = other.getXqgdzczz();
        if (this$xqgdzczz == null ? other$xqgdzczz != null : !this$xqgdzczz.equals(other$xqgdzczz)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$fileid = getFileid();Object other$fileid = other.getFileid();
        if (this$fileid == null ? other$fileid != null : !this$fileid.equals(other$fileid)) {
            return false;
        }
        Object this$xqdm = getXqdm();Object other$xqdm = other.getXqdm();
        if (this$xqdm == null ? other$xqdm != null : !this$xqdm.equals(other$xqdm)) {
            return false;
        }
        Object this$xqmc = getXqmc();Object other$xqmc = other.getXqmc();
        if (this$xqmc == null ? other$xqmc != null : !this$xqmc.equals(other$xqmc)) {
            return false;
        }
        Object this$xqdz = getXqdz();Object other$xqdz = other.getXqdz();
        if (this$xqdz == null ? other$xqdz != null : !this$xqdz.equals(other$xqdz)) {
            return false;
        }
        Object this$xqlxdh = getXqlxdh();Object other$xqlxdh = other.getXqlxdh();
        if (this$xqlxdh == null ? other$xqlxdh != null : !this$xqlxdh.equals(other$xqlxdh)) {
            return false;
        }
        Object this$xqczdh = getXqczdh();Object other$xqczdh = other.getXqczdh();
        if (this$xqczdh == null ? other$xqczdh != null : !this$xqczdh.equals(other$xqczdh)) {
            return false;
        }
        Object this$fzrUserId = getFzrUserId();Object other$fzrUserId = other.getFzrUserId();
        if (this$fzrUserId == null ? other$fzrUserId != null : !this$fzrUserId.equals(other$fzrUserId)) {
            return false;
        }
        Object this$dzyj = getDzyj();Object other$dzyj = other.getDzyj();
        if (this$dzyj == null ? other$dzyj != null : !this$dzyj.equals(other$dzyj)) {
            return false;
        }
        Object this$xqyzbm = getXqyzbm();Object other$xqyzbm = other.getXqyzbm();
        if (this$xqyzbm == null ? other$xqyzbm != null : !this$xqyzbm.equals(other$xqyzbm)) {
            return false;
        }
        Object this$xqszdxzqhm = getXqszdxzqhm();Object other$xqszdxzqhm = other.getXqszdxzqhm();return this$xqszdxzqhm == null ? other$xqszdxzqhm == null : this$xqszdxzqhm.equals(other$xqszdxzqhm);
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getXqdm()
    {
        return this.xqdm;
    }

    public String getXqmc()
    {
        return this.xqmc;
    }

    public String getXqdz()
    {
        return this.xqdz;
    }

    public String getXqlxdh()
    {
        return this.xqlxdh;
    }

    public String getXqczdh()
    {
        return this.xqczdh;
    }

    public String getFzrUserId()
    {
        return this.fzrUserId;
    }

    public String getDzyj()
    {
        return this.dzyj;
    }

    public String getXqyzbm()
    {
        return this.xqyzbm;
    }

    public String getXqszdxzqhm()
    {
        return this.xqszdxzqhm;
    }

    public Double getXqmj()
    {
        return this.xqmj;
    }

    public Double getXqjzmj()
    {
        return this.xqjzmj;
    }

    public Double getXqjxkysbzz()
    {
        return this.xqjxkysbzz;
    }

    public Double getXqgdzczz()
    {
        return this.xqgdzczz;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public Integer getFileid()
    {
        return this.fileid;
    }
}
