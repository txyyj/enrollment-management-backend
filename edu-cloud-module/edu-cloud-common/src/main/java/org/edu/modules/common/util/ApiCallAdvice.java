package org.edu.modules.common.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.edu.modules.common.entity.SysLog;
import org.edu.modules.common.service.ISysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.BoundHashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Aspect
@Component
public class ApiCallAdvice
{
    @Resource
    private RedisTemplate redisTemplate;
    @Resource
    private StringRedisTemplate stringRedisTemplate;
    @Autowired
    private ISysLogService sysLogService;
    private static final String FORMAT_PATTERN_DAY = "yyyy-MM-dd";
    private static final String FORMAT_PATTERN_MILLS = "yyyy-MM-dd HH:mm:ss:SSS";

    public void before()
    {
        ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();

        HttpServletRequest request = attributes.getRequest();

        String uri = request.getRequestURI();
        String date = dateFormat("yyyy-MM-dd");
        String ip = getRequestIp(request);
    }

    @AfterReturning("execution(* org.edu.modules.common.controller.VeCommonController.*(..))")
    public void afterReturning()
    {
        ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();

        HttpServletRequest request = attributes.getRequest();

        String uri = request.getRequestURI();
        String date = dateFormat("yyyy-MM-dd");
        String ip = request.getRemoteAddr();
        String interfaceUserId = request.getParameter("interfaceUserId");

        SysLog sysLog = new SysLog();
        sysLog.setLogType(Integer.valueOf(3));
        sysLog.setUserid(interfaceUserId);
        sysLog.setIp(ip);
        sysLog.setMethod(uri);
        sysLog.setCreateTime(new Date());
        this.sysLogService.save(sysLog);
    }

    @AfterThrowing(value="execution(* org.edu.modules.common.controller.VeCommonController.*(..))", throwing="ex")
    public void afterThrowing(Exception ex)
    {
        ServletRequestAttributes attributes = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();

        String uri = request.getRequestURI() + "_exception";
        String time = dateFormat("yyyy-MM-dd HH:mm:ss:SSS");
        String exception = ex.getMessage();

        this.redisTemplate.boundHashOps(uri).put(time, exception);
    }

    private String getRequestIp(HttpServletRequest request)
    {
        String ip = request.getHeader("x-forwarded-for");
        if ((ip == null) || (ip.length() == 0) || ("unknown".equalsIgnoreCase(ip)) || ("null".equals(ip))) {
            ip = "" + request.getHeader("Proxy-Client-IP");
        }
        if ((ip == null) || (ip.length() == 0) || ("unknown".equalsIgnoreCase(ip)) || ("null".equals(ip))) {
            ip = "" + request.getHeader("WL-Proxy-Client-IP");
        }
        if ((ip == null) || (ip.length() == 0) || ("unknown".equalsIgnoreCase(ip)) || ("null".equals(ip))) {
            ip = "" + request.getRemoteAddr();
        }
        return ip;
    }

    private String dateFormat(String pattern)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        return dateFormat.format(new Date());
    }
}

