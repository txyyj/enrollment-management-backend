package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.edu.modules.common.entity.VeBaseBanJiBzr;

public abstract interface VeBaseBanJiBzrMapper
        extends BaseMapper<VeBaseBanJiBzr>
{
    public abstract VeBaseBanJiBzr getByBjIdAndBzrUserId(Integer paramInteger, String paramString);

    public abstract IPage<VeBaseBanJiBzr> getBanJiBzrPageList(Page paramPage, VeBaseBanJiBzr paramVeBaseBanJiBzr);
}
