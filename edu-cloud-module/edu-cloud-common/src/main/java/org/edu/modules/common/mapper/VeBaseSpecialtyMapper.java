package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.common.entity.VeBaseSpecialty;

public abstract interface VeBaseSpecialtyMapper
        extends BaseMapper<VeBaseSpecialty>
{
    public abstract List<VeBaseSpecialty> querySpecialtyListByFalid(Integer paramInteger);

    public abstract List<Map<String, Object>> getSpecialtyPageList(VeBaseSpecialty paramVeBaseSpecialty);

    public abstract VeBaseSpecialty getSpecialtyByName(Integer paramInteger, String paramString);

    public abstract VeBaseSpecialty getSpecialtyByBH(Integer paramInteger, String paramString);

    public abstract List<Map<String, Object>> getSpecialtyStudentStatistics();

    public abstract List<Map<String, Object>> getSpecialtyListByPid(Long paramLong);

    public abstract List<Map<String, Object>> getSpecialtyList(Integer paramInteger1, Integer paramInteger2);

    public abstract List<Map<String, Object>> getSpecialtyListByFalId(Integer paramInteger);

    public abstract List<Map<String, Object>> getSpecialtyByZYMC(String paramString);

    public abstract List<Map<String, Object>> getSpecialtyAllListByFalIdList(List<Map<String, Object>> paramList);

    public abstract int stopSpecialtyBatch(@Param("ids") List<String> paramList);

    public abstract int deleteSpecialtyBatch(@Param("ids") List<String> paramList);

    public abstract int deleteSpecialtyByFalId(@Param("falIds") String[] paramArrayOfString);

    public abstract int stopSpecialtyByFalId(@Param("falIds") String[] paramArrayOfString);

    public abstract int updateSpecialtyFalIdById(@Param("ids") List<Integer> paramList, @Param("falId") Integer paramInteger);

    public abstract List<Map<String, Object>> getGraduatePeopleCount(@Param("specName") String paramString);
}
