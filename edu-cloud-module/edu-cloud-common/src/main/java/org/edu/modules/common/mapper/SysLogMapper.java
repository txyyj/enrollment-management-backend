package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.edu.modules.common.entity.SysLog;

public abstract interface SysLogMapper
        extends BaseMapper<SysLog>
{
    public abstract List<SysLog> getSysLogPageList(SysLog paramSysLog);
}
