package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.edu.modules.base.entity.VeBaseOperaterLog;

public abstract interface VeBaseOperaterLogMapper
        extends BaseMapper<VeBaseOperaterLog>
{
    public abstract List<VeBaseOperaterLog> operaterLogAllList(VeBaseOperaterLog paramVeBaseOperaterLog);

    public abstract List<VeBaseOperaterLog> operaterLogPageList(VeBaseOperaterLog paramVeBaseOperaterLog);
}
