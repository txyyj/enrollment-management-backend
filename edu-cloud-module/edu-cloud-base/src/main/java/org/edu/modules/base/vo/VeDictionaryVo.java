package org.edu.modules.base.vo;

import java.util.List;
import org.edu.modules.base.entity.VeDictionary;

public class VeDictionaryVo
{
    private int id;
    private String title;
    private String code;
    private int listSort;
    private int pid;
    private int terminalId;
    private List<VeDictionary> veDictionaryList;

    public void setCode(String code)
    {
        this.code = code;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;result = result * 59 + getId();result = result * 59 + getListSort();result = result * 59 + getPid();result = result * 59 + getTerminalId();Object $title = getTitle();result = result * 59 + ($title == null ? 43 : $title.hashCode());Object $code = getCode();result = result * 59 + ($code == null ? 43 : $code.hashCode());Object $veDictionaryList = getVeDictionaryList();result = result * 59 + ($veDictionaryList == null ? 43 : $veDictionaryList.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeDictionaryVo;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeDictionaryVo)) {
            return false;
        }
        VeDictionaryVo other = (VeDictionaryVo)o;
        if (!other.canEqual(this)) {
            return false;
        }
        if (getId() != other.getId()) {
            return false;
        }
        if (getListSort() != other.getListSort()) {
            return false;
        }
        if (getPid() != other.getPid()) {
            return false;
        }
        if (getTerminalId() != other.getTerminalId()) {
            return false;
        }
        Object this$title = getTitle();Object other$title = other.getTitle();
        if (this$title == null ? other$title != null : !this$title.equals(other$title)) {
            return false;
        }
        Object this$code = getCode();Object other$code = other.getCode();
        if (this$code == null ? other$code != null : !this$code.equals(other$code)) {
            return false;
        }
        Object this$veDictionaryList = getVeDictionaryList();Object other$veDictionaryList = other.getVeDictionaryList();return this$veDictionaryList == null ? other$veDictionaryList == null : this$veDictionaryList.equals(other$veDictionaryList);
    }

    public void setVeDictionaryList(List<VeDictionary> veDictionaryList)
    {
        this.veDictionaryList = veDictionaryList;
    }

    public void setTerminalId(int terminalId)
    {
        this.terminalId = terminalId;
    }

    public void setPid(int pid)
    {
        this.pid = pid;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public void setTitle(String title)
    {
        this.title = title;
    }

    public String toString()
    {
        return "VeDictionaryVo(id=" + getId() + ", title=" + getTitle() + ", code=" + getCode() + ", listSort=" + getListSort() + ", pid=" + getPid() + ", terminalId=" + getTerminalId() + ", veDictionaryList=" + getVeDictionaryList() + ")";
    }

    public void setListSort(int listSort)
    {
        this.listSort = listSort;
    }

    public int getId()
    {
        return this.id;
    }

    public String getTitle()
    {
        return this.title;
    }

    public String getCode()
    {
        return this.code;
    }

    public int getListSort()
    {
        return this.listSort;
    }

    public int getPid()
    {
        return this.pid;
    }

    public int getTerminalId()
    {
        return this.terminalId;
    }

    public List<VeDictionary> getVeDictionaryList()
    {
        return this.veDictionaryList;
    }
}
