package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseLoginSrategy;
import org.edu.modules.base.mapper.VeBaseLoginSrategyMapper;
import org.edu.modules.base.service.IVeBaseLoginSrategyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseLoginSrategyServiceImpl
        extends ServiceImpl<VeBaseLoginSrategyMapper, VeBaseLoginSrategy>
        implements IVeBaseLoginSrategyService
{
    @Autowired
    private VeBaseLoginSrategyMapper veBaseLoginSrategyMapper;

    public int getLoginSrategyAllList(VeBaseLoginSrategy veBaseLoginSrategy)
    {
        return this.veBaseLoginSrategyMapper.getLoginSrategyAllList(veBaseLoginSrategy);
    }

    public List<Map<String, Object>> getLoginSrategyPageList(VeBaseLoginSrategy veBaseLoginSrategy)
    {
        return this.veBaseLoginSrategyMapper.getLoginSrategyPageList(veBaseLoginSrategy);
    }
}
