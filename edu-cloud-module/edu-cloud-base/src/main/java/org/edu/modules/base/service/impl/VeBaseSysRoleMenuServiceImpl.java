package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseSysRoleMenu;
import org.edu.modules.base.mapper.VeBaseSysRoleMenuMapper;
import org.edu.modules.base.service.IVeBaseSysRoleMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseSysRoleMenuServiceImpl
        extends ServiceImpl<VeBaseSysRoleMenuMapper, VeBaseSysRoleMenu>
        implements IVeBaseSysRoleMenuService
{
    @Autowired
    private VeBaseSysRoleMenuMapper veBaseSysRoleMenuMapper;

    public int deleteByRoleId(Integer roleId)
    {
        return this.veBaseSysRoleMenuMapper.deleteByRoleId(roleId);
    }

    public int deleteModelByAppId(Integer roleId, String appId)
    {
        return this.veBaseSysRoleMenuMapper.deleteModelByAppId(roleId, appId);
    }

    public List<Map<String, Object>> getSysRoleMenuListByRoleIdAppId(Integer roleId, String appId)
    {
        return this.veBaseSysRoleMenuMapper.getSysRoleMenuListByRoleIdAppId(roleId, appId);
    }

    public int deleteRoleMenuByMenuId(String menuId)
    {
        return this.veBaseSysRoleMenuMapper.deleteRoleMenuByMenuId(menuId);
    }

    public int deleteRoleUserByRoleId(String roleId)
    {
        return this.veBaseSysRoleMenuMapper.deleteRoleUserByRoleId(roleId);
    }

    public int deleteRoleMenuByRoleId(String roleId)
    {
        return this.veBaseSysRoleMenuMapper.deleteRoleMenuByRoleId(roleId);
    }
}
