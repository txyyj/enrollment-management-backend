package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.base.entity.VeBaseAppUserOrgan;

public abstract interface IVeBaseAppUserOrganService
        extends IService<VeBaseAppUserOrgan>
{
    public abstract List<VeBaseAppUserOrgan> getTreeList();
}
