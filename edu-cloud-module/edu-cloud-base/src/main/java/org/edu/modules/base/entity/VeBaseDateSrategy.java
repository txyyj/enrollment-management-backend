package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableLogic;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Arrays;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_date_srategy")
@ApiModel(value="ve_base_date_srategy对象", description="时间策略管理")
public class VeBaseDateSrategy
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="开始时间", width=15.0D)
    @ApiModelProperty("开始时间")
    private String dateStart;
    @Excel(name="结束时间", width=15.0D)
    @ApiModelProperty("结束时间")
    private String dateEnd;
    @Excel(name="是否启用", width=15.0D)
    @ApiModelProperty("是否启用(0启用,1禁用)")
    private String isenable;
    @Excel(name="删除标识", width=15.0D)
    @ApiModelProperty("删除标识")
    @TableLogic(value="0", delval="-1")
    private String deleteFlag;
    @Excel(name="备用", width=15.0D)
    @ApiModelProperty("备用")
    private String fulldate;
    @TableField(exist=false)
    private String[] arrays;

    public VeBaseDateSrategy setDateEnd(String dateEnd)
    {
        this.dateEnd = dateEnd;return this;
    }

    public VeBaseDateSrategy setDateStart(String dateStart)
    {
        this.dateStart = dateStart;return this;
    }

    public VeBaseDateSrategy setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseDateSrategy(id=" + getId() + ", dateStart=" + getDateStart() + ", dateEnd=" + getDateEnd() + ", isenable=" + getIsenable() + ", deleteFlag=" + getDeleteFlag() + ", fulldate=" + getFulldate() + ", arrays=" + Arrays.deepToString(getArrays()) + ")";
    }

    public VeBaseDateSrategy setArrays(String[] arrays)
    {
        this.arrays = arrays;return this;
    }

    public VeBaseDateSrategy setFulldate(String fulldate)
    {
        this.fulldate = fulldate;return this;
    }

    public VeBaseDateSrategy setDeleteFlag(String deleteFlag)
    {
        this.deleteFlag = deleteFlag;return this;
    }

    public VeBaseDateSrategy setIsenable(String isenable)
    {
        this.isenable = isenable;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $dateStart = getDateStart();result = result * 59 + ($dateStart == null ? 43 : $dateStart.hashCode());Object $dateEnd = getDateEnd();result = result * 59 + ($dateEnd == null ? 43 : $dateEnd.hashCode());Object $isenable = getIsenable();result = result * 59 + ($isenable == null ? 43 : $isenable.hashCode());Object $deleteFlag = getDeleteFlag();result = result * 59 + ($deleteFlag == null ? 43 : $deleteFlag.hashCode());Object $fulldate = getFulldate();result = result * 59 + ($fulldate == null ? 43 : $fulldate.hashCode());result = result * 59 + Arrays.deepHashCode(getArrays());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseDateSrategy;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseDateSrategy)) {
            return false;
        }
        VeBaseDateSrategy other = (VeBaseDateSrategy)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$dateStart = getDateStart();Object other$dateStart = other.getDateStart();
        if (this$dateStart == null ? other$dateStart != null : !this$dateStart.equals(other$dateStart)) {
            return false;
        }
        Object this$dateEnd = getDateEnd();Object other$dateEnd = other.getDateEnd();
        if (this$dateEnd == null ? other$dateEnd != null : !this$dateEnd.equals(other$dateEnd)) {
            return false;
        }
        Object this$isenable = getIsenable();Object other$isenable = other.getIsenable();
        if (this$isenable == null ? other$isenable != null : !this$isenable.equals(other$isenable)) {
            return false;
        }
        Object this$deleteFlag = getDeleteFlag();Object other$deleteFlag = other.getDeleteFlag();
        if (this$deleteFlag == null ? other$deleteFlag != null : !this$deleteFlag.equals(other$deleteFlag)) {
            return false;
        }
        Object this$fulldate = getFulldate();Object other$fulldate = other.getFulldate();
        if (this$fulldate == null ? other$fulldate != null : !this$fulldate.equals(other$fulldate)) {
            return false;
        }
        return Arrays.deepEquals(getArrays(), other.getArrays());
    }

    public String getId()
    {
        return this.id;
    }

    public String getDateStart()
    {
        return this.dateStart;
    }

    public String getDateEnd()
    {
        return this.dateEnd;
    }

    public String getIsenable()
    {
        return this.isenable;
    }

    public String getDeleteFlag()
    {
        return this.deleteFlag;
    }

    public String getFulldate()
    {
        return this.fulldate;
    }

    public String[] getArrays()
    {
        return this.arrays;
    }
}
