package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_login_strategy")
@ApiModel(value="ve_base_login_strategy对象", description="登录策略表")
public class VeBaseLoginSrategy
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="策略名称", width=15.0D)
    @ApiModelProperty("策略名称")
    private String staName;
    @Excel(name="错误次数", width=15.0D)
    @ApiModelProperty("错误次数")
    private Double errorCount;
    @Excel(name="重置时间间隔", width=15.0D)
    @ApiModelProperty("重置时间间隔")
    private Double timeDiff;
    @Excel(name="处理类型", width=15.0D)
    @ApiModelProperty("处理类型")
    private String detailType;
    @Excel(name="冻结描述", width=15.0D)
    @ApiModelProperty("冻结描述")
    private String errorReson;
    @Excel(name="状态", width=15.0D)
    @ApiModelProperty("状态|0:启用,1:禁用")
    private String isEnable;
    @TableField(exist=false)
    private Integer startIndex;
    @TableField(exist=false)
    private Integer pageSize;

    public VeBaseLoginSrategy setErrorCount(Double errorCount)
    {
        this.errorCount = errorCount;return this;
    }

    public VeBaseLoginSrategy setStaName(String staName)
    {
        this.staName = staName;return this;
    }

    public VeBaseLoginSrategy setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseLoginSrategy(id=" + getId() + ", staName=" + getStaName() + ", errorCount=" + getErrorCount() + ", timeDiff=" + getTimeDiff() + ", detailType=" + getDetailType() + ", errorReson=" + getErrorReson() + ", isEnable=" + getIsEnable() + ", startIndex=" + getStartIndex() + ", pageSize=" + getPageSize() + ")";
    }

    public VeBaseLoginSrategy setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;return this;
    }

    public VeBaseLoginSrategy setStartIndex(Integer startIndex)
    {
        this.startIndex = startIndex;return this;
    }

    public VeBaseLoginSrategy setIsEnable(String isEnable)
    {
        this.isEnable = isEnable;return this;
    }

    public VeBaseLoginSrategy setErrorReson(String errorReson)
    {
        this.errorReson = errorReson;return this;
    }

    public VeBaseLoginSrategy setDetailType(String detailType)
    {
        this.detailType = detailType;return this;
    }

    public VeBaseLoginSrategy setTimeDiff(Double timeDiff)
    {
        this.timeDiff = timeDiff;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $errorCount = getErrorCount();result = result * 59 + ($errorCount == null ? 43 : $errorCount.hashCode());Object $timeDiff = getTimeDiff();result = result * 59 + ($timeDiff == null ? 43 : $timeDiff.hashCode());Object $startIndex = getStartIndex();result = result * 59 + ($startIndex == null ? 43 : $startIndex.hashCode());Object $pageSize = getPageSize();result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $staName = getStaName();result = result * 59 + ($staName == null ? 43 : $staName.hashCode());Object $detailType = getDetailType();result = result * 59 + ($detailType == null ? 43 : $detailType.hashCode());Object $errorReson = getErrorReson();result = result * 59 + ($errorReson == null ? 43 : $errorReson.hashCode());Object $isEnable = getIsEnable();result = result * 59 + ($isEnable == null ? 43 : $isEnable.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseLoginSrategy;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseLoginSrategy)) {
            return false;
        }
        VeBaseLoginSrategy other = (VeBaseLoginSrategy)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$errorCount = getErrorCount();Object other$errorCount = other.getErrorCount();
        if (this$errorCount == null ? other$errorCount != null : !this$errorCount.equals(other$errorCount)) {
            return false;
        }
        Object this$timeDiff = getTimeDiff();Object other$timeDiff = other.getTimeDiff();
        if (this$timeDiff == null ? other$timeDiff != null : !this$timeDiff.equals(other$timeDiff)) {
            return false;
        }
        Object this$startIndex = getStartIndex();Object other$startIndex = other.getStartIndex();
        if (this$startIndex == null ? other$startIndex != null : !this$startIndex.equals(other$startIndex)) {
            return false;
        }
        Object this$pageSize = getPageSize();Object other$pageSize = other.getPageSize();
        if (this$pageSize == null ? other$pageSize != null : !this$pageSize.equals(other$pageSize)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$staName = getStaName();Object other$staName = other.getStaName();
        if (this$staName == null ? other$staName != null : !this$staName.equals(other$staName)) {
            return false;
        }
        Object this$detailType = getDetailType();Object other$detailType = other.getDetailType();
        if (this$detailType == null ? other$detailType != null : !this$detailType.equals(other$detailType)) {
            return false;
        }
        Object this$errorReson = getErrorReson();Object other$errorReson = other.getErrorReson();
        if (this$errorReson == null ? other$errorReson != null : !this$errorReson.equals(other$errorReson)) {
            return false;
        }
        Object this$isEnable = getIsEnable();Object other$isEnable = other.getIsEnable();return this$isEnable == null ? other$isEnable == null : this$isEnable.equals(other$isEnable);
    }

    public String getId()
    {
        return this.id;
    }

    public String getStaName()
    {
        return this.staName;
    }

    public Double getErrorCount()
    {
        return this.errorCount;
    }

    public Double getTimeDiff()
    {
        return this.timeDiff;
    }

    public String getDetailType()
    {
        return this.detailType;
    }

    public String getErrorReson()
    {
        return this.errorReson;
    }

    public String getIsEnable()
    {
        return this.isEnable;
    }

    public Integer getStartIndex()
    {
        return this.startIndex;
    }

    public Integer getPageSize()
    {
        return this.pageSize;
    }
}
