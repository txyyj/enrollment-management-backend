package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.base.entity.VeBaseAppGroup;

public abstract interface IVeBaseAppGroupService
        extends IService<VeBaseAppGroup>
{
    public abstract int getAppGroupAllList(String paramString);

    public abstract List<VeBaseAppGroup> getAppGroupPageList(String paramString, Integer paramInteger1, Integer paramInteger2);

    public abstract VeBaseAppGroup getAppGroupByName(String paramString1, String paramString2);

    public abstract int addAppGroupManageBatch(String paramString, String[] paramArrayOfString);

    public abstract int deleteAppGroupManageById(String paramString);
}
