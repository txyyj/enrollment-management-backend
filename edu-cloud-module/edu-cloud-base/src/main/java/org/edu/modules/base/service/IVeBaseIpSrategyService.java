package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseIpSrategy;

public abstract interface IVeBaseIpSrategyService
        extends IService<VeBaseIpSrategy>
{
    public abstract int getIpSrategyAllList(VeBaseIpSrategy paramVeBaseIpSrategy);

    public abstract List<Map<String, Object>> getIpSrategyPageList(VeBaseIpSrategy paramVeBaseIpSrategy);

    public abstract int addSrategyServer(String paramString, String[] paramArrayOfString);

    public abstract int deleteSrategyServer(String paramString);
}
