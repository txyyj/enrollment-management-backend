package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.edu.common.api.vo.Result;
import org.edu.common.util.PasswordUtil;
import org.edu.common.util.oConvertUtils;
import org.edu.modules.base.entity.VeBaseAppUser;
import org.edu.modules.base.mapper.VeBaseAppUserMapper;
import org.edu.modules.base.service.IVeBaseAppUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.stereotype.Service;

@Service
public class VeBaseAppUserServiceImpl
        extends ServiceImpl<VeBaseAppUserMapper, VeBaseAppUser>
        implements IVeBaseAppUserService
{
    @Autowired
    private VeBaseAppUserMapper appUserMapper;

    public List<VeBaseAppUser> zombieUserAllList(VeBaseAppUser appUser)
    {
        List<VeBaseAppUser> appUserList = this.appUserMapper.getLoginUserAllList(appUser);

        List<VeBaseAppUser> loginUserList = this.appUserMapper.getLoginUserList(appUser);
        List<VeBaseAppUser> list = new ArrayList();
        int i;
        if (loginUserList.size() > 0)
        {
            i = 0;
            for (VeBaseAppUser a : appUserList)
            {
                for (VeBaseAppUser b : loginUserList)
                {
                    i = 0;
                    if (a.getUserId().equals(b.getUserId()))
                    {
                        i++;
                        break;
                    }
                }
                if (i == 0)
                {
                    a.setUserPassword(a.getStrength());
                    list.add(a);
                }
            }
        }
        else
        {
            for (VeBaseAppUser a : appUserList)
            {
                a.setUserPassword(a.getStrength());
                list.add(a);
            }
        }
        return list;
    }

    public int getLoginUserAllList(VeBaseAppUser appUser)
    {
        return this.appUserMapper.getLoginUserAllListNum(appUser);
    }

    public List<VeBaseAppUser> getLoginUserPageList(VeBaseAppUser appUser)
    {
        List<VeBaseAppUser> list = this.appUserMapper.getLoginUserPageList(appUser);
        if (list.size() > 0) {
            for (VeBaseAppUser veBaseAppUser : list) {
                veBaseAppUser.setUserPassword(veBaseAppUser.getStrength());
            }
        }
        return list;
    }

    public int getAppUserAllList(VeBaseAppUser appUser)
    {
        return this.appUserMapper.getAppUserList(appUser);
    }

    public List<Map<String, Object>> getAppUserPageList(VeBaseAppUser appUser)
    {
        List<Map<String, Object>> list = this.appUserMapper.getAppUserPageList(appUser);
        if (list.size() > 0) {
            for (Map map : list)
            {
                map.put("userPassword", map.get("strength").toString());

                List<Map<String, Object>> detailList = this.appUserMapper.getRoleNameByUserId(map.get("userId").toString());
                StringBuilder roleId = new StringBuilder();
                StringBuilder roleName = new StringBuilder();
                for (Map detailMap : detailList)
                {
                    roleId.append(detailMap.get("id").toString() + ",");
                    roleName.append(detailMap.get("text").toString() + ",");
                }
                if ((!"".equals(roleId.toString())) && (roleId.toString() != null))
                {
                    map.put("roleId", roleId.substring(0, roleId.length() - 1));
                    map.put("roleName", roleName.substring(0, roleName.length() - 1));
                }
                else
                {
                    map.put("roleId", "");
                    map.put("roleName", "");
                }
            }
        }
        return list;
    }

    @CacheEvict(value={"sys:cache:user"}, allEntries=true)
    public Result<?> updateAppUserPasswordById(String id, String username, String password, String strength)
    {
        String salt = oConvertUtils.randomGen(8);
        String passwordEncode = PasswordUtil.encrypt(username, password, salt);
        this.appUserMapper.updateAppUserPasswordById(id, passwordEncode, salt, strength);
        return Result.ok("密码修改成功!");
    }

    public VeBaseAppUser getPwdAndSalt(VeBaseAppUser veBaseAppUser)
    {
        String salt = oConvertUtils.randomGen(8);

        String passwordEncode = PasswordUtil.encrypt(veBaseAppUser.getUserName(), veBaseAppUser.getUserPassword(), salt);
        veBaseAppUser.setUserPassword(passwordEncode);
        veBaseAppUser.setSalt(salt);
        return veBaseAppUser;
    }

    public int serviceGroupAppUserAllList(VeBaseAppUser veBaseAppUser)
    {
        return this.appUserMapper.serviceGroupAppUserAllList(veBaseAppUser);
    }

    public List<Map<String, Object>> serviceGroupAppUserPageList(VeBaseAppUser veBaseAppUser)
    {
        return this.appUserMapper.serviceGroupAppUserPageList(veBaseAppUser);
    }

    public int deleteServiceGroupAppUser(String groupId)
    {
        return this.appUserMapper.deleteServiceGroupAppUser(groupId);
    }

    public int serviceGroupAppUserAdd(String groupId, String[] userId)
    {
        return this.appUserMapper.serviceGroupAppUserAdd(groupId, userId);
    }

    public int deleteGroupAppUser(String groupId, String[] userId)
    {
        return this.appUserMapper.deleteGroupAppUser(groupId, userId);
    }

    public List<Map<String, Object>> getAppOrganList()
    {
        return this.appUserMapper.getAppOrganList();
    }

    public List<Map<String, Object>> getSysRoleList()
    {
        return this.appUserMapper.getSysRoleList();
    }

    public int addOrgan(String userId, String organId)
    {
        return this.appUserMapper.addOrgan(userId, organId);
    }

    public int addRole(String userId, String roleId)
    {
        return this.appUserMapper.addRole(userId, roleId);
    }

    public int deleteOrgan(String userId)
    {
        return this.appUserMapper.deleteOrgan(userId);
    }

    public int deleteRole(String userId)
    {
        return this.appUserMapper.deleteRole(userId);
    }

    public List<Map<String, Object>> getAppUserByUserId(String userId, String id)
    {
        return this.appUserMapper.getAppUserByUserId(userId, id);
    }

    public List<Map<String, Object>> getAppUserAllListByUserId(String userId, String id)
    {
        return this.appUserMapper.getAppUserAllListByUserId(userId, id);
    }

    public List<Map<String, Object>> getAppUserByUserTel(String userTel, String id)
    {
        return this.appUserMapper.getAppUserByUserTel(userTel, id);
    }

    public int getAppUserAndStudentAndTeacherList(VeBaseAppUser veBaseAppUser)
    {
        return this.appUserMapper.getAppUserAndStudentAndTeacherList(veBaseAppUser);
    }

    public List<Map<String, Object>> getAppUserAndStudentAndTeacherPageList(VeBaseAppUser veBaseAppUser)
    {
        List<Map<String, Object>> list = this.appUserMapper.getAppUserAndStudentAndTeacherPageList(veBaseAppUser);
        if (list.size() > 0) {
            for (Map map : list)
            {
                List<Map<String, Object>> mapList = this.appUserMapper.getSysRoleListByUserId(map.get("userId").toString());
                map.put("roleList", mapList);
            }
        }
        return list;
    }

    public int addSysUser(VeBaseAppUser veBaseAppUser)
    {
        return this.appUserMapper.addSysUser(veBaseAppUser);
    }

    public int updateSysUserStatus(String userId, Integer status)
    {
        return this.appUserMapper.updateSysUserStatus(userId, status);
    }

    public int updateSysUserDel(String userId)
    {
        return this.appUserMapper.updateSysUserDel(userId);
    }

    public List<Map<String, Object>> getTeacherListByUserId(String userId)
    {
        return this.appUserMapper.getTeacherListByUserId(userId);
    }

    public List<Map<String, Object>> getStudentListByUserId(String userId)
    {
        return this.appUserMapper.getStudentListByUserId(userId);
    }

    public String pwdVerify(String pwd)
    {
        int flag = 0;
        if (pwd.matches(".*[0-9].*")) {
            flag++;
        }
        if (pwd.matches(".*[a-z].*")) {
            flag++;
        }
        if (pwd.matches(".*[A-Z].*")) {
            flag++;
        }
        if (pwd.matches(".*[!@*#$\\-_()+=&￥].*")) {
            flag++;
        }
        if (flag == 1) {
            return "弱";
        }
        if (flag == 2) {
            return "中";
        }
        return "强";
    }

    public int updatePwdAndSalt(String userId, String pwd, String salt)
    {
        return this.appUserMapper.updatePwdAndSalt(userId, pwd, salt);
    }
}
