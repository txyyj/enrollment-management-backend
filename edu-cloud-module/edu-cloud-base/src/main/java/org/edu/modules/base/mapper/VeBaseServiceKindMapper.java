package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.base.entity.VeBaseServiceKind;

public abstract interface VeBaseServiceKindMapper
        extends BaseMapper<VeBaseServiceKind>
{}
