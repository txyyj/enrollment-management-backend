package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

@TableName("ve_base_service")
@ApiModel(value="ve_base_service对象", description="服务信息")
public class VeBaseService
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="名称", width=15.0D)
    @ApiModelProperty("名称")
    private String name;
    @Excel(name="地址", width=15.0D)
    @ApiModelProperty("地址")
    private String url;
    @Excel(name="图标", width=15.0D)
    @ApiModelProperty("图标")
    private String icon;
    @Excel(name="类型", width=15.0D)
    @ApiModelProperty("类型")
    private String type;
    @Excel(name="宽", width=15.0D)
    @ApiModelProperty("宽")
    private Double width;
    @Excel(name="高", width=15.0D)
    @ApiModelProperty("高")
    private Double height;
    @Excel(name="默认最大化", width=15.0D)
    @ApiModelProperty("默认最大化")
    private Double isopenmax;
    @Excel(name="发布时间", width=20.0D, format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("发布时间")
    private Date dt;
    @Excel(name="流程说明", width=15.0D)
    @ApiModelProperty("流程说明")
    private String remark;
    @Excel(name="分类id", width=15.0D)
    @ApiModelProperty("分类id")
    private String kindid;
    @Excel(name="sysName", width=15.0D)
    @ApiModelProperty("sysName")
    private String sysName;
    @Excel(name="isHot", width=15.0D)
    @ApiModelProperty("isHot")
    private String ishot;
    @Excel(name="common", width=15.0D)
    @ApiModelProperty("common")
    private String common;
    @Excel(name="注意事项", width=15.0D)
    @ApiModelProperty("注意事项")
    private String needmatter;
    @TableField(exist=false)
    @ApiModelProperty("搜索开始时间")
    private String beginDate;
    @TableField(exist=false)
    @ApiModelProperty("搜索结束时间")
    private String endDate;
    @TableField(exist=false)
    private String groupId;
    @TableField(exist=false)
    private Integer startIndex;
    @TableField(exist=false)
    private Integer pageSize;

    public VeBaseService setUrl(String url)
    {
        this.url = url;return this;
    }

    public VeBaseService setName(String name)
    {
        this.name = name;return this;
    }

    public VeBaseService setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseService(id=" + getId() + ", name=" + getName() + ", url=" + getUrl() + ", icon=" + getIcon() + ", type=" + getType() + ", width=" + getWidth() + ", height=" + getHeight() + ", isopenmax=" + getIsopenmax() + ", dt=" + getDt() + ", remark=" + getRemark() + ", kindid=" + getKindid() + ", sysName=" + getSysName() + ", ishot=" + getIshot() + ", common=" + getCommon() + ", needmatter=" + getNeedmatter() + ", beginDate=" + getBeginDate() + ", endDate=" + getEndDate() + ", groupId=" + getGroupId() + ", startIndex=" + getStartIndex() + ", pageSize=" + getPageSize() + ")";
    }

    public VeBaseService setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;return this;
    }

    public VeBaseService setStartIndex(Integer startIndex)
    {
        this.startIndex = startIndex;return this;
    }

    public VeBaseService setGroupId(String groupId)
    {
        this.groupId = groupId;return this;
    }

    public VeBaseService setEndDate(String endDate)
    {
        this.endDate = endDate;return this;
    }

    public VeBaseService setBeginDate(String beginDate)
    {
        this.beginDate = beginDate;return this;
    }

    public VeBaseService setNeedmatter(String needmatter)
    {
        this.needmatter = needmatter;return this;
    }

    public VeBaseService setCommon(String common)
    {
        this.common = common;return this;
    }

    public VeBaseService setIshot(String ishot)
    {
        this.ishot = ishot;return this;
    }

    public VeBaseService setSysName(String sysName)
    {
        this.sysName = sysName;return this;
    }

    public VeBaseService setKindid(String kindid)
    {
        this.kindid = kindid;return this;
    }

    public VeBaseService setRemark(String remark)
    {
        this.remark = remark;return this;
    }

    @JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    public VeBaseService setDt(Date dt)
    {
        this.dt = dt;return this;
    }

    public VeBaseService setIsopenmax(Double isopenmax)
    {
        this.isopenmax = isopenmax;return this;
    }

    public VeBaseService setHeight(Double height)
    {
        this.height = height;return this;
    }

    public VeBaseService setWidth(Double width)
    {
        this.width = width;return this;
    }

    public VeBaseService setType(String type)
    {
        this.type = type;return this;
    }

    public VeBaseService setIcon(String icon)
    {
        this.icon = icon;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $width = getWidth();result = result * 59 + ($width == null ? 43 : $width.hashCode());Object $height = getHeight();result = result * 59 + ($height == null ? 43 : $height.hashCode());Object $isopenmax = getIsopenmax();result = result * 59 + ($isopenmax == null ? 43 : $isopenmax.hashCode());Object $startIndex = getStartIndex();result = result * 59 + ($startIndex == null ? 43 : $startIndex.hashCode());Object $pageSize = getPageSize();result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());Object $url = getUrl();result = result * 59 + ($url == null ? 43 : $url.hashCode());Object $icon = getIcon();result = result * 59 + ($icon == null ? 43 : $icon.hashCode());Object $type = getType();result = result * 59 + ($type == null ? 43 : $type.hashCode());Object $dt = getDt();result = result * 59 + ($dt == null ? 43 : $dt.hashCode());Object $remark = getRemark();result = result * 59 + ($remark == null ? 43 : $remark.hashCode());Object $kindid = getKindid();result = result * 59 + ($kindid == null ? 43 : $kindid.hashCode());Object $sysName = getSysName();result = result * 59 + ($sysName == null ? 43 : $sysName.hashCode());Object $ishot = getIshot();result = result * 59 + ($ishot == null ? 43 : $ishot.hashCode());Object $common = getCommon();result = result * 59 + ($common == null ? 43 : $common.hashCode());Object $needmatter = getNeedmatter();result = result * 59 + ($needmatter == null ? 43 : $needmatter.hashCode());Object $beginDate = getBeginDate();result = result * 59 + ($beginDate == null ? 43 : $beginDate.hashCode());Object $endDate = getEndDate();result = result * 59 + ($endDate == null ? 43 : $endDate.hashCode());Object $groupId = getGroupId();result = result * 59 + ($groupId == null ? 43 : $groupId.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseService;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseService)) {
            return false;
        }
        VeBaseService other = (VeBaseService)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$width = getWidth();Object other$width = other.getWidth();
        if (this$width == null ? other$width != null : !this$width.equals(other$width)) {
            return false;
        }
        Object this$height = getHeight();Object other$height = other.getHeight();
        if (this$height == null ? other$height != null : !this$height.equals(other$height)) {
            return false;
        }
        Object this$isopenmax = getIsopenmax();Object other$isopenmax = other.getIsopenmax();
        if (this$isopenmax == null ? other$isopenmax != null : !this$isopenmax.equals(other$isopenmax)) {
            return false;
        }
        Object this$startIndex = getStartIndex();Object other$startIndex = other.getStartIndex();
        if (this$startIndex == null ? other$startIndex != null : !this$startIndex.equals(other$startIndex)) {
            return false;
        }
        Object this$pageSize = getPageSize();Object other$pageSize = other.getPageSize();
        if (this$pageSize == null ? other$pageSize != null : !this$pageSize.equals(other$pageSize)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$name = getName();Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) {
            return false;
        }
        Object this$url = getUrl();Object other$url = other.getUrl();
        if (this$url == null ? other$url != null : !this$url.equals(other$url)) {
            return false;
        }
        Object this$icon = getIcon();Object other$icon = other.getIcon();
        if (this$icon == null ? other$icon != null : !this$icon.equals(other$icon)) {
            return false;
        }
        Object this$type = getType();Object other$type = other.getType();
        if (this$type == null ? other$type != null : !this$type.equals(other$type)) {
            return false;
        }
        Object this$dt = getDt();Object other$dt = other.getDt();
        if (this$dt == null ? other$dt != null : !this$dt.equals(other$dt)) {
            return false;
        }
        Object this$remark = getRemark();Object other$remark = other.getRemark();
        if (this$remark == null ? other$remark != null : !this$remark.equals(other$remark)) {
            return false;
        }
        Object this$kindid = getKindid();Object other$kindid = other.getKindid();
        if (this$kindid == null ? other$kindid != null : !this$kindid.equals(other$kindid)) {
            return false;
        }
        Object this$sysName = getSysName();Object other$sysName = other.getSysName();
        if (this$sysName == null ? other$sysName != null : !this$sysName.equals(other$sysName)) {
            return false;
        }
        Object this$ishot = getIshot();Object other$ishot = other.getIshot();
        if (this$ishot == null ? other$ishot != null : !this$ishot.equals(other$ishot)) {
            return false;
        }
        Object this$common = getCommon();Object other$common = other.getCommon();
        if (this$common == null ? other$common != null : !this$common.equals(other$common)) {
            return false;
        }
        Object this$needmatter = getNeedmatter();Object other$needmatter = other.getNeedmatter();
        if (this$needmatter == null ? other$needmatter != null : !this$needmatter.equals(other$needmatter)) {
            return false;
        }
        Object this$beginDate = getBeginDate();Object other$beginDate = other.getBeginDate();
        if (this$beginDate == null ? other$beginDate != null : !this$beginDate.equals(other$beginDate)) {
            return false;
        }
        Object this$endDate = getEndDate();Object other$endDate = other.getEndDate();
        if (this$endDate == null ? other$endDate != null : !this$endDate.equals(other$endDate)) {
            return false;
        }
        Object this$groupId = getGroupId();Object other$groupId = other.getGroupId();return this$groupId == null ? other$groupId == null : this$groupId.equals(other$groupId);
    }

    public String getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public String getUrl()
    {
        return this.url;
    }

    public String getIcon()
    {
        return this.icon;
    }

    public String getType()
    {
        return this.type;
    }

    public Double getWidth()
    {
        return this.width;
    }

    public Double getHeight()
    {
        return this.height;
    }

    public Double getIsopenmax()
    {
        return this.isopenmax;
    }

    public Date getDt()
    {
        return this.dt;
    }

    public String getRemark()
    {
        return this.remark;
    }

    public String getKindid()
    {
        return this.kindid;
    }

    public String getSysName()
    {
        return this.sysName;
    }

    public String getIshot()
    {
        return this.ishot;
    }

    public String getCommon()
    {
        return this.common;
    }

    public String getNeedmatter()
    {
        return this.needmatter;
    }

    public String getBeginDate()
    {
        return this.beginDate;
    }

    public String getEndDate()
    {
        return this.endDate;
    }

    public String getGroupId()
    {
        return this.groupId;
    }

    public Integer getStartIndex()
    {
        return this.startIndex;
    }

    public Integer getPageSize()
    {
        return this.pageSize;
    }
}
