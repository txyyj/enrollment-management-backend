package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import org.edu.modules.base.entity.VeBaseOperaterLog;
import org.edu.modules.base.mapper.VeBaseOperaterLogMapper;
import org.edu.modules.base.service.IVeBaseOperaterLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseOperaterLogServiceImpl
        extends ServiceImpl<VeBaseOperaterLogMapper, VeBaseOperaterLog>
        implements IVeBaseOperaterLogService
{
    @Autowired
    private VeBaseOperaterLogMapper veBaseOperaterLogMapper;

    public List<VeBaseOperaterLog> getOperaterLogAllList(VeBaseOperaterLog veBaseOperaterLog)
    {
        return this.veBaseOperaterLogMapper.operaterLogAllList(veBaseOperaterLog);
    }

    public List<VeBaseOperaterLog> getOperaterLogPageList(VeBaseOperaterLog veBaseOperaterLog)
    {
        return this.veBaseOperaterLogMapper.operaterLogPageList(veBaseOperaterLog);
    }
}
