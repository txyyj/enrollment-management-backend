package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseIpSrategy;
import org.edu.modules.base.mapper.VeBaseIpSrategyMapper;
import org.edu.modules.base.service.IVeBaseIpSrategyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseIpSrategyServiceImpl
        extends ServiceImpl<VeBaseIpSrategyMapper, VeBaseIpSrategy>
        implements IVeBaseIpSrategyService
{
    @Autowired
    private VeBaseIpSrategyMapper veBaseIpSrategyMapper;

    public int getIpSrategyAllList(VeBaseIpSrategy veBaseIpSrategy)
    {
        return this.veBaseIpSrategyMapper.getIpSrategyAllList(veBaseIpSrategy);
    }

    public List<Map<String, Object>> getIpSrategyPageList(VeBaseIpSrategy veBaseIpSrategy)
    {
        List<Map<String, Object>> list = this.veBaseIpSrategyMapper.getIpSrategyPageList(veBaseIpSrategy);
        if (list.size() > 0) {
            for (Map map : list)
            {
                List<Map<String, Object>> mapList = this.veBaseIpSrategyMapper.getSrategyServerList(map.get("id").toString());
                map.put("arrays", mapList);

                String id = "";
                String name = "";
                StringBuilder stringBuilder = new StringBuilder();
                StringBuilder stringBuilder1 = new StringBuilder();
                if (mapList.size() > 0)
                {
                    for (Map detail : mapList)
                    {
                        stringBuilder.append(detail.get("serid").toString()).append(",");
                        if ((!"".equals(detail.get("appName"))) && (detail.get("appName") != null)) {
                            stringBuilder1.append(detail.get("appName").toString()).append(",");
                        }
                    }
                    if ((!"".equals(stringBuilder.toString())) && (stringBuilder.toString() != null)) {
                        id = stringBuilder.substring(0, stringBuilder.lastIndexOf(","));
                    }
                    if ((!"".equals(stringBuilder1.toString())) && (stringBuilder1.toString() != null)) {
                        name = stringBuilder1.substring(0, stringBuilder1.lastIndexOf(","));
                    }
                }
                map.put("appManageGroupId", id);
                map.put("appManageGroupName", name);
            }
        }
        return list;
    }

    public int addSrategyServer(String id, String[] arrays)
    {
        return this.veBaseIpSrategyMapper.addSrategyServer(id, arrays);
    }

    public int deleteSrategyServer(String id)
    {
        return this.veBaseIpSrategyMapper.deleteSrategyServer(id);
    }
}
