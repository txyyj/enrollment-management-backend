package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseSysRoleMenu;

public abstract interface VeBaseSysRoleMenuMapper
        extends BaseMapper<VeBaseSysRoleMenu>
{
    public abstract int deleteByRoleId(Integer paramInteger);

    public abstract int deleteModelByAppId(Integer paramInteger, String paramString);

    public abstract List<Map<String, Object>> getSysRoleMenuListByRoleIdAppId(Integer paramInteger, String paramString);

    public abstract int deleteRoleMenuByMenuId(String paramString);

    public abstract int deleteRoleUserByRoleId(String paramString);

    public abstract int deleteRoleMenuByRoleId(String paramString);
}
