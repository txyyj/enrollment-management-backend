package org.edu.modules.base.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Map;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.modules.base.service.IVeBaseUserAccessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags={"首页"})
@RestController
@RequestMapping({"/base/veSiteSimple"})
@ApiSort(60)
public class VeSiteSimpleController
{
    private static final Logger log = LoggerFactory.getLogger(VeSiteSimpleController.class);
    @Autowired
    private IVeBaseUserAccessService veBaseUserAccessService;

    @AutoLog("在线用户柱状图")
    @ApiOperation(value="首页-在线用户柱状图", notes="首页-在线用户柱状图")
    @GetMapping({"/userAccessHourStatistics"})
    public Result<?> userAccessHourStatistics()
    {
        List<Map<String, Object>> list = this.veBaseUserAccessService.userAccessHourStatistics();
        return Result.ok(list);
    }

    @AutoLog("在线用户饼状图")
    @ApiOperation(value="首页-在线用户饼状图", notes="首页-在线用户饼状图")
    @GetMapping({"/userAccessDayStatistics"})
    public Result<?> userAccessDayStatistics()
    {
        Map map = this.veBaseUserAccessService.userAccessDayStatistics();
        return Result.ok(map);
    }

    @AutoLog("系统用户统计")
    @ApiOperation(value="首页-系统用户统计", notes="首页-系统用户统计")
    @GetMapping({"/userAppAccessStatistics"})
    public Result<?> userAppAccessStatistics()
    {
        Map map = this.veBaseUserAccessService.userAppAccessStatistics();
        return Result.ok(map);
    }
}
