package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Arrays;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_sys_role")
@ApiModel(value="ve_base_sys_role对象", description="权限管理-角色信息表")
public class VeBaseSysRole
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="角色编码", width=15.0D)
    @ApiModelProperty("角色编码")
    private String code;
    @Excel(name="角色名称", width=15.0D)
    @ApiModelProperty("角色名称")
    private String text;
    @Excel(name="角色备注", width=15.0D)
    @ApiModelProperty("角色备注")
    private String remark;
    @Excel(name="状态（1启用0禁用）", width=15.0D)
    @ApiModelProperty("状态（1启用0禁用）")
    private Integer status;
    @Excel(name="0自定义角色1默认角色", width=15.0D)
    @ApiModelProperty("0自定义角色1默认角色")
    private Integer isdefault;
    @Excel(name="终端系统ID", width=15.0D)
    @ApiModelProperty("终端系统ID")
    private Integer terminalid;
    @TableField(exist=false)
    private Integer startIndex;
    @TableField(exist=false)
    private Integer pageSize;
    @TableField(exist=false)
    private String appId;
    @TableField(exist=false)
    private String[] ids;

    public VeBaseSysRole setText(String text)
    {
        this.text = text;return this;
    }

    public VeBaseSysRole setCode(String code)
    {
        this.code = code;return this;
    }

    public VeBaseSysRole setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseSysRole(id=" + getId() + ", code=" + getCode() + ", text=" + getText() + ", remark=" + getRemark() + ", status=" + getStatus() + ", isdefault=" + getIsdefault() + ", terminalid=" + getTerminalid() + ", startIndex=" + getStartIndex() + ", pageSize=" + getPageSize() + ", appId=" + getAppId() + ", ids=" + Arrays.deepToString(getIds()) + ")";
    }

    public VeBaseSysRole setIds(String[] ids)
    {
        this.ids = ids;return this;
    }

    public VeBaseSysRole setAppId(String appId)
    {
        this.appId = appId;return this;
    }

    public VeBaseSysRole setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;return this;
    }

    public VeBaseSysRole setStartIndex(Integer startIndex)
    {
        this.startIndex = startIndex;return this;
    }

    public VeBaseSysRole setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeBaseSysRole setIsdefault(Integer isdefault)
    {
        this.isdefault = isdefault;return this;
    }

    public VeBaseSysRole setStatus(Integer status)
    {
        this.status = status;return this;
    }

    public VeBaseSysRole setRemark(String remark)
    {
        this.remark = remark;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $status = getStatus();result = result * 59 + ($status == null ? 43 : $status.hashCode());Object $isdefault = getIsdefault();result = result * 59 + ($isdefault == null ? 43 : $isdefault.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $startIndex = getStartIndex();result = result * 59 + ($startIndex == null ? 43 : $startIndex.hashCode());Object $pageSize = getPageSize();result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());Object $code = getCode();result = result * 59 + ($code == null ? 43 : $code.hashCode());Object $text = getText();result = result * 59 + ($text == null ? 43 : $text.hashCode());Object $remark = getRemark();result = result * 59 + ($remark == null ? 43 : $remark.hashCode());Object $appId = getAppId();result = result * 59 + ($appId == null ? 43 : $appId.hashCode());result = result * 59 + Arrays.deepHashCode(getIds());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseSysRole;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseSysRole)) {
            return false;
        }
        VeBaseSysRole other = (VeBaseSysRole)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$status = getStatus();Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) {
            return false;
        }
        Object this$isdefault = getIsdefault();Object other$isdefault = other.getIsdefault();
        if (this$isdefault == null ? other$isdefault != null : !this$isdefault.equals(other$isdefault)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$startIndex = getStartIndex();Object other$startIndex = other.getStartIndex();
        if (this$startIndex == null ? other$startIndex != null : !this$startIndex.equals(other$startIndex)) {
            return false;
        }
        Object this$pageSize = getPageSize();Object other$pageSize = other.getPageSize();
        if (this$pageSize == null ? other$pageSize != null : !this$pageSize.equals(other$pageSize)) {
            return false;
        }
        Object this$code = getCode();Object other$code = other.getCode();
        if (this$code == null ? other$code != null : !this$code.equals(other$code)) {
            return false;
        }
        Object this$text = getText();Object other$text = other.getText();
        if (this$text == null ? other$text != null : !this$text.equals(other$text)) {
            return false;
        }
        Object this$remark = getRemark();Object other$remark = other.getRemark();
        if (this$remark == null ? other$remark != null : !this$remark.equals(other$remark)) {
            return false;
        }
        Object this$appId = getAppId();Object other$appId = other.getAppId();
        if (this$appId == null ? other$appId != null : !this$appId.equals(other$appId)) {
            return false;
        }
        return Arrays.deepEquals(getIds(), other.getIds());
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getCode()
    {
        return this.code;
    }

    public String getText()
    {
        return this.text;
    }

    public String getRemark()
    {
        return this.remark;
    }

    public Integer getStatus()
    {
        return this.status;
    }

    public Integer getIsdefault()
    {
        return this.isdefault;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public Integer getStartIndex()
    {
        return this.startIndex;
    }

    public Integer getPageSize()
    {
        return this.pageSize;
    }

    public String getAppId()
    {
        return this.appId;
    }

    public String[] getIds()
    {
        return this.ids;
    }
}
