package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

@TableName("ve_base_service_group")
@ApiModel(value="ve_base_service_group对象", description="服务群组信息")
public class VeBaseServiceGroup
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="名称", width=15.0D)
    @ApiModelProperty("名称")
    private String groupName;
    @Excel(name="创建时间", width=20.0D, format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间")
    private Date createDate;
    @Excel(name="rls", width=15.0D)
    @ApiModelProperty("rls")
    private String rls;
    @Excel(name="sorts", width=15.0D)
    @ApiModelProperty("sorts")
    private Integer sorts;

    @JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    public VeBaseServiceGroup setCreateDate(Date createDate)
    {
        this.createDate = createDate;return this;
    }

    public VeBaseServiceGroup setGroupName(String groupName)
    {
        this.groupName = groupName;return this;
    }

    public VeBaseServiceGroup setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseServiceGroup(id=" + getId() + ", groupName=" + getGroupName() + ", createDate=" + getCreateDate() + ", rls=" + getRls() + ", sorts=" + getSorts() + ")";
    }

    public VeBaseServiceGroup setSorts(Integer sorts)
    {
        this.sorts = sorts;return this;
    }

    public VeBaseServiceGroup setRls(String rls)
    {
        this.rls = rls;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $sorts = getSorts();result = result * 59 + ($sorts == null ? 43 : $sorts.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $groupName = getGroupName();result = result * 59 + ($groupName == null ? 43 : $groupName.hashCode());Object $createDate = getCreateDate();result = result * 59 + ($createDate == null ? 43 : $createDate.hashCode());Object $rls = getRls();result = result * 59 + ($rls == null ? 43 : $rls.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseServiceGroup;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseServiceGroup)) {
            return false;
        }
        VeBaseServiceGroup other = (VeBaseServiceGroup)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$sorts = getSorts();Object other$sorts = other.getSorts();
        if (this$sorts == null ? other$sorts != null : !this$sorts.equals(other$sorts)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$groupName = getGroupName();Object other$groupName = other.getGroupName();
        if (this$groupName == null ? other$groupName != null : !this$groupName.equals(other$groupName)) {
            return false;
        }
        Object this$createDate = getCreateDate();Object other$createDate = other.getCreateDate();
        if (this$createDate == null ? other$createDate != null : !this$createDate.equals(other$createDate)) {
            return false;
        }
        Object this$rls = getRls();Object other$rls = other.getRls();return this$rls == null ? other$rls == null : this$rls.equals(other$rls);
    }

    public String getId()
    {
        return this.id;
    }

    public String getGroupName()
    {
        return this.groupName;
    }

    public Date getCreateDate()
    {
        return this.createDate;
    }

    public String getRls()
    {
        return this.rls;
    }

    public Integer getSorts()
    {
        return this.sorts;
    }
}
