package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_menu")
@ApiModel(value="ve_base_menu对象", description="菜单表")
public class VeBaseMenu
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="菜单名称", width=15.0D)
    @ApiModelProperty("菜单名称")
    private String menuname;
    @Excel(name="菜单路径", width=15.0D)
    @ApiModelProperty("菜单路径")
    private String url;
    @Excel(name="上级菜单ID", width=15.0D)
    @ApiModelProperty("上级菜单ID")
    private String pid;
    @Excel(name="排序号", width=15.0D)
    @ApiModelProperty("排序号")
    private Double orders;
    @Excel(name="是否执行待办", width=15.0D)
    @ApiModelProperty("是否执行待办（y是）")
    private String remind;
    @TableField(exist=false)
    private List<VeBaseMenu> children;

    public VeBaseMenu setUrl(String url)
    {
        this.url = url;return this;
    }

    public VeBaseMenu setMenuname(String menuname)
    {
        this.menuname = menuname;return this;
    }

    public VeBaseMenu setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseMenu(id=" + getId() + ", menuname=" + getMenuname() + ", url=" + getUrl() + ", pid=" + getPid() + ", orders=" + getOrders() + ", remind=" + getRemind() + ", children=" + getChildren() + ")";
    }

    public VeBaseMenu setChildren(List<VeBaseMenu> children)
    {
        this.children = children;return this;
    }

    public VeBaseMenu setRemind(String remind)
    {
        this.remind = remind;return this;
    }

    public VeBaseMenu setOrders(Double orders)
    {
        this.orders = orders;return this;
    }

    public VeBaseMenu setPid(String pid)
    {
        this.pid = pid;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $orders = getOrders();result = result * 59 + ($orders == null ? 43 : $orders.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $menuname = getMenuname();result = result * 59 + ($menuname == null ? 43 : $menuname.hashCode());Object $url = getUrl();result = result * 59 + ($url == null ? 43 : $url.hashCode());Object $pid = getPid();result = result * 59 + ($pid == null ? 43 : $pid.hashCode());Object $remind = getRemind();result = result * 59 + ($remind == null ? 43 : $remind.hashCode());Object $children = getChildren();result = result * 59 + ($children == null ? 43 : $children.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseMenu;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseMenu)) {
            return false;
        }
        VeBaseMenu other = (VeBaseMenu)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$orders = getOrders();Object other$orders = other.getOrders();
        if (this$orders == null ? other$orders != null : !this$orders.equals(other$orders)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$menuname = getMenuname();Object other$menuname = other.getMenuname();
        if (this$menuname == null ? other$menuname != null : !this$menuname.equals(other$menuname)) {
            return false;
        }
        Object this$url = getUrl();Object other$url = other.getUrl();
        if (this$url == null ? other$url != null : !this$url.equals(other$url)) {
            return false;
        }
        Object this$pid = getPid();Object other$pid = other.getPid();
        if (this$pid == null ? other$pid != null : !this$pid.equals(other$pid)) {
            return false;
        }
        Object this$remind = getRemind();Object other$remind = other.getRemind();
        if (this$remind == null ? other$remind != null : !this$remind.equals(other$remind)) {
            return false;
        }
        Object this$children = getChildren();Object other$children = other.getChildren();return this$children == null ? other$children == null : this$children.equals(other$children);
    }

    public String getId()
    {
        return this.id;
    }

    public String getMenuname()
    {
        return this.menuname;
    }

    public String getUrl()
    {
        return this.url;
    }

    public String getPid()
    {
        return this.pid;
    }

    public Double getOrders()
    {
        return this.orders;
    }

    public String getRemind()
    {
        return this.remind;
    }

    public List<VeBaseMenu> getChildren()
    {
        return this.children;
    }
}
