package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseAppManage;

public abstract interface IVeBaseAppManageService
        extends IService<VeBaseAppManage>
{
    public abstract int getAppManageAllListNum(String paramString1, String paramString2);

    public abstract List<VeBaseAppManage> getAppManageAllList(String paramString1, String paramString2);

    public abstract List<VeBaseAppManage> getAppManagePageList(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2);

    public abstract List<Map<String, Object>> serviceGroupAppManageAllList(VeBaseAppManage paramVeBaseAppManage);

    public abstract List<Map<String, Object>> serviceGroupAppManagePageList(VeBaseAppManage paramVeBaseAppManage);

    public abstract int deleteServiceGroupAppManage(String paramString);

    public abstract int serviceGroupAppManageAdd(String paramString, String[] paramArrayOfString);

    public abstract VeBaseAppManage getAppManageByName(String paramString1, String paramString2);
}
