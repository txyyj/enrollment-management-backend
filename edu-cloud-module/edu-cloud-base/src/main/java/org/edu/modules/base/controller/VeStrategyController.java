package org.edu.modules.base.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.modules.base.entity.PageBean;
import org.edu.modules.base.entity.VeBaseAppGroup;
import org.edu.modules.base.entity.VeBaseDateSrategy;
import org.edu.modules.base.entity.VeBaseIpSrategy;
import org.edu.modules.base.entity.VeBaseLoginSrategy;
import org.edu.modules.base.service.IVeBaseAppGroupService;
import org.edu.modules.base.service.IVeBaseDateSrategyService;
import org.edu.modules.base.service.IVeBaseIpSrategyService;
import org.edu.modules.base.service.IVeBaseLoginSrategyService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags={"策略管理"})
@RestController
@RequestMapping({"/base/veStrategy"})
@ApiSort(60)
public class VeStrategyController
{
    private static final Logger log = LoggerFactory.getLogger(VeStrategyController.class);
    @Autowired
    private IVeBaseDateSrategyService dateSrategyService;
    @Autowired
    private IVeBaseIpSrategyService ipSrategyService;
    @Autowired
    private IVeBaseLoginSrategyService loginSrategyService;
    @Autowired
    private IVeBaseAppGroupService veBaseAppGroupService;

    @AutoLog("时间策略列表-分页列表查询")
    @ApiOperation(value="时间策略列表-分页列表查询", notes="时间策略列表-分页列表查询")
    @GetMapping({"/DateSrategyPageList"})
    public Result<?> dateSrategyPageList(VeBaseDateSrategy dateSrategy, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        int num = this.dateSrategyService.getDateSrategyAllList(dateSrategy.getIsenable(), dateSrategy.getFulldate());
        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);
        pb.setList(this.dateSrategyService.getDateSrategyPageList(dateSrategy.getIsenable(), dateSrategy.getFulldate(), Integer.valueOf(pb.getStartIndex()), pageSize));
        return Result.ok(pb);
    }

    @AutoLog("ip策略列表-分页列表查询")
    @ApiOperation(value="ip策略列表-分页列表查询", notes="ip策略列表-分页列表查询")
    @GetMapping({"/ipSrategyPageList"})
    public Result<?> ipSrategyPageList(VeBaseIpSrategy ipSrategy, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        int num = this.ipSrategyService.getIpSrategyAllList(ipSrategy);
        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);
        ipSrategy.setStartIndex(Integer.valueOf(pb.getStartIndex()));
        ipSrategy.setPageSize(pageSize);
        pb.setList(this.ipSrategyService.getIpSrategyPageList(ipSrategy));
        return Result.ok(pb);
    }

    @AutoLog("登录策略列表-分页列表查询")
    @ApiOperation(value="登录策略列表-分页列表查询", notes="登录策略列表-分页列表查询")
    @GetMapping({"/loginSrategyPageList"})
    public Result<?> loginSrategyPageList(VeBaseLoginSrategy loginSrategy, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        int num = this.loginSrategyService.getLoginSrategyAllList(loginSrategy);
        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);
        loginSrategy.setStartIndex(Integer.valueOf(pb.getStartIndex()));
        loginSrategy.setPageSize(pageSize);
        pb.setList(this.loginSrategyService.getLoginSrategyPageList(loginSrategy));
        return Result.ok(pb);
    }

    @AutoLog("查询所有用户组信息")
    @ApiOperation(value="查询所有用户组信息", notes="查询所有用户组信息")
    @GetMapping({"/getAppGroupList"})
    public Result<?> getAppGroupList()
    {
        List<VeBaseAppGroup> list = this.veBaseAppGroupService.list();
        return Result.OK(list);
    }

    @AutoLog("时间策略-新增信息")
    @ApiOperation(value="新增时间策略", notes="时间策略-新增信息")
    @PostMapping({"/dateSrategyAdd"})
    @Transactional
    public Result<?> dateSrategyAdd(@RequestBody VeBaseDateSrategy dateSrategy)
    {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        dateSrategy.setId(uuid);
        dateSrategy.setDeleteFlag("0");
        this.dateSrategyService.save(dateSrategy);
        if (dateSrategy.getArrays().length > 0) {
            this.dateSrategyService.addSrategyServer(dateSrategy.getId(), dateSrategy.getArrays());
        }
        return Result.OK("添加成功！");
    }

    @AutoLog("ip策略-新增信息")
    @ApiOperation(value="新增ip策略", notes="ip策略-新增信息")
    @PostMapping({"/ipSrategyAdd"})
    @Transactional
    public Result<?> ipSrategyAdd(@RequestBody VeBaseIpSrategy ipSrategy)
    {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        ipSrategy.setId(uuid);
        this.ipSrategyService.save(ipSrategy);
        if (ipSrategy.getArrays().length > 0) {
            this.ipSrategyService.addSrategyServer(ipSrategy.getId(), ipSrategy.getArrays());
        }
        return Result.OK("添加成功！");
    }

    @AutoLog("新增登录策略-新增信息")
    @ApiOperation(value="新增登录策略", notes="新增登录策略-新增信息")
    @PostMapping({"/loginSrategyAdd"})
    public Result<?> loginSrategyAdd(@RequestBody VeBaseLoginSrategy loginSrategy)
    {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        loginSrategy.setId(uuid);
        this.loginSrategyService.save(loginSrategy);
        return Result.OK("添加成功！");
    }

    @AutoLog("时间策略-修改信息")
    @ApiOperation(value="修改时间策略", notes="时间策略-修改信息")
    @PostMapping({"/dateSrategyEdit"})
    @Transactional
    public Result<?> dateSrategyEdit(@RequestBody VeBaseDateSrategy dateSrategy)
    {
        dateSrategy.setDeleteFlag("0");
        this.dateSrategyService.updateById(dateSrategy);
        this.dateSrategyService.deleteSrategyServer(dateSrategy.getId());
        if (dateSrategy.getArrays().length > 0) {
            this.dateSrategyService.addSrategyServer(dateSrategy.getId(), dateSrategy.getArrays());
        }
        return Result.OK("修改成功!");
    }

    @AutoLog("ip策略-修改信息")
    @ApiOperation(value="修改ip策略", notes="ip策略-修改信息")
    @PostMapping({"/ipSrategyEdit"})
    @Transactional
    public Result<?> ipSrategyEdit(@RequestBody VeBaseIpSrategy ipSrategy)
    {
        this.ipSrategyService.updateById(ipSrategy);
        this.ipSrategyService.deleteSrategyServer(ipSrategy.getId());
        if (ipSrategy.getArrays().length > 0) {
            this.ipSrategyService.addSrategyServer(ipSrategy.getId(), ipSrategy.getArrays());
        }
        return Result.OK("修改成功!");
    }

    @AutoLog("登录策略-修改信息")
    @ApiOperation(value="修改登录策略", notes="登录策略-修改信息")
    @PostMapping({"/loginSrategyEdit"})
    public Result<?> loginSrategyEdit(@RequestBody VeBaseLoginSrategy loginSrategy)
    {
        this.loginSrategyService.updateById(loginSrategy);
        return Result.OK("编辑成功!");
    }

    @AutoLog("通过id启用时间策略")
    @ApiOperation(value="通过id启用时间策略", notes="通过id启用时间策略")
    @PostMapping({"/dateSrategyStart"})
    public Result<?> dateSrategyStart(@RequestParam(name="id", required=true) String id)
    {
        VeBaseDateSrategy dateSrategy = (VeBaseDateSrategy)this.dateSrategyService.getById(id);
        if (dateSrategy != null)
        {
            dateSrategy.setIsenable("0");
            this.dateSrategyService.updateById(dateSrategy);
        }
        return Result.OK("启用成功!");
    }

    @AutoLog("通过id启用ip策略")
    @ApiOperation(value="通过id启用ip策略", notes="通过id启用ip策略")
    @PostMapping({"/ipSrategyStart"})
    public Result<?> ipSrategyStart(@RequestParam(name="id", required=true) String id)
    {
        VeBaseIpSrategy ipSrategy = (VeBaseIpSrategy)this.ipSrategyService.getById(id);
        if (ipSrategy != null)
        {
            ipSrategy.setIsEnable("0");
            this.ipSrategyService.updateById(ipSrategy);
        }
        return Result.OK("启用成功!");
    }

    @AutoLog("通过id启用登录策略")
    @ApiOperation(value="通过id启用登录策略", notes="通过id启用登录策略")
    @PostMapping({"/loginSrategyStart"})
    public Result<?> loginSrategyStart(@RequestParam(name="id", required=true) String id)
    {
        VeBaseLoginSrategy loginSrategy = (VeBaseLoginSrategy)this.loginSrategyService.getById(id);
        if (loginSrategy != null)
        {
            loginSrategy.setIsEnable("0");
            this.loginSrategyService.updateById(loginSrategy);
        }
        return Result.OK("启用成功!");
    }

    @AutoLog("通过id禁用时间策略")
    @ApiOperation(value="通过id禁用时间策略", notes="通过id禁用时间策略")
    @PostMapping({"/dateSrategyForbidden"})
    public Result<?> dateSrategyForbidden(@RequestParam(name="id", required=true) String id)
    {
        VeBaseDateSrategy dateSrategy = (VeBaseDateSrategy)this.dateSrategyService.getById(id);
        if (dateSrategy != null)
        {
            dateSrategy.setIsenable("1");
            this.dateSrategyService.updateById(dateSrategy);
        }
        return Result.OK("禁用成功!");
    }

    @AutoLog("通过id禁用ip策略")
    @ApiOperation(value="通过id禁用ip策略", notes="通过id禁用ip策略")
    @PostMapping({"/ipSrategyForbidden"})
    public Result<?> ipSrategyForbidden(@RequestParam(name="id", required=true) String id)
    {
        VeBaseIpSrategy ipSrategy = (VeBaseIpSrategy)this.ipSrategyService.getById(id);
        if (ipSrategy != null)
        {
            ipSrategy.setIsEnable("1");
            this.ipSrategyService.updateById(ipSrategy);
        }
        return Result.OK("禁用成功!");
    }

    @AutoLog("通过id禁用登录策略")
    @ApiOperation(value="通过id禁用登录策略", notes="通过id禁用登录策略")
    @PostMapping({"/loginSrategyForbidden"})
    public Result<?> loginSrategyForbidden(@RequestParam(name="id", required=true) String id)
    {
        VeBaseLoginSrategy loginSrategy = (VeBaseLoginSrategy)this.loginSrategyService.getById(id);
        if (loginSrategy != null)
        {
            loginSrategy.setIsEnable("1");
            this.loginSrategyService.updateById(loginSrategy);
        }
        return Result.OK("禁用成功!");
    }

    @AutoLog("时间策略-通过id删除")
    @ApiOperation(value="时间策略-通过id删除", notes="时间策略-通过id删除")
    @PostMapping({"/dateSrategyDelete"})
    public Result<?> dateSrategyDelete(@RequestParam(name="id", required=true) String id)
    {
        this.dateSrategyService.removeDateSrategyById(id);
        return Result.OK("删除成功!");
    }

    @AutoLog("ip策略-通过id删除")
    @ApiOperation(value="ip策略-通过id删除", notes="ip策略-通过id删除")
    @PostMapping({"/ipSrategyDelete"})
    public Result<?> ipSrategyDelete(@RequestParam(name="id", required=true) String id)
    {
        this.ipSrategyService.removeById(id);
        return Result.OK("删除成功!");
    }
}
