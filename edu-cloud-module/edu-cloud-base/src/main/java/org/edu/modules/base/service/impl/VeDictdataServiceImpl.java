package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import org.edu.modules.base.entity.VeDictdata;
import org.edu.modules.base.entity.VeDictionary;
import org.edu.modules.base.mapper.VeDictdataMapper;
import org.edu.modules.base.service.IVeDictdataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeDictdataServiceImpl
        extends ServiceImpl<VeDictdataMapper, VeDictdata>
        implements IVeDictdataService
{
    @Autowired
    VeDictdataMapper veDictdataMapper;

    public List<VeDictdata> selectAll(VeDictionary veDictionary, Integer startLine, Integer pageSize)
    {
        return this.veDictdataMapper.selectAll(veDictionary, startLine, pageSize);
    }

    public int getSumPage(VeDictionary veDictionary)
    {
        return this.veDictdataMapper.getSumPage(veDictionary);
    }

    public List<String> getEnterNatures()
    {
        return this.veDictdataMapper.getEnterNatures();
    }

    public List<String> getEnterScale()
    {
        return this.veDictdataMapper.getEnterScale();
    }
}
