package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.base.entity.VeBaseSysMenu;
import org.edu.modules.base.entity.VeBaseSysRole;
import org.edu.modules.base.mapper.VeBaseSysMenuMapper;
import org.edu.modules.base.service.IVeBaseSysMenuService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/12/13 16:47
 */
@Service
public class VeBaseSysMenuServiceImpl extends ServiceImpl<VeBaseSysMenuMapper, VeBaseSysMenu> implements IVeBaseSysMenuService {
    @Override
    public List<Map<String, Object>> getSysMenuTreeList(String paramString) {
        return baseMapper.getBodyList(paramString);
    }

    @Override
    public int getSysRoleAllList(VeBaseSysRole paramVeBaseSysRole) {
        return baseMapper.getSysRoleAllList(paramVeBaseSysRole);
    }

    @Override
    public List<Map<String, Object>> getSysRolePageList(VeBaseSysRole paramVeBaseSysRole) {
        return baseMapper.getSysRolePageList(paramVeBaseSysRole);
    }

    @Override
    public Integer addSysMenu(VeBaseSysMenu paramVeBaseSysMenu) {
        return baseMapper.addSysMenu(paramVeBaseSysMenu);
    }

    @Override
    public VeBaseSysMenu getSysMenuByPid(Integer paramInteger) {
        return baseMapper.selectById(paramInteger);
    }

    @Override
    public int deleteByAppId(String paramString) {
        return baseMapper.deleteByAppId(paramString);
    }
}
