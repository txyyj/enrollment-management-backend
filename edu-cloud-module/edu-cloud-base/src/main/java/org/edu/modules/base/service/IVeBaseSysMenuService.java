package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseSysMenu;
import org.edu.modules.base.entity.VeBaseSysRole;

public abstract interface IVeBaseSysMenuService
        extends IService<VeBaseSysMenu>
{
    public abstract List<Map<String, Object>> getSysMenuTreeList(String paramString);

    public abstract int getSysRoleAllList(VeBaseSysRole paramVeBaseSysRole);

    public abstract List<Map<String, Object>> getSysRolePageList(VeBaseSysRole paramVeBaseSysRole);

    public abstract Integer addSysMenu(VeBaseSysMenu paramVeBaseSysMenu);

    public abstract VeBaseSysMenu getSysMenuByPid(Integer paramInteger);

    public abstract int deleteByAppId(String paramString);
}
