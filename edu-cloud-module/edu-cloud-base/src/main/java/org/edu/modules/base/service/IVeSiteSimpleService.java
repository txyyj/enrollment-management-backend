package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.base.entity.VeSiteSimple;

public abstract interface IVeSiteSimpleService
        extends IService<VeSiteSimple>
{}
