package org.edu.modules.base.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.query.QueryGenerator;
import org.edu.common.system.vo.LoginUser;
import org.edu.modules.base.entity.PageBean;
import org.edu.modules.base.entity.VeBaseAppUser;
import org.edu.modules.base.entity.VeBaseService;
import org.edu.modules.base.entity.VeBaseServiceGroup;
import org.edu.modules.base.entity.VeBaseServiceGroupApp;
import org.edu.modules.base.entity.VeBaseServiceGroupUser;
import org.edu.modules.base.entity.VeBaseServiceType;
import org.edu.modules.base.service.IVeBaseAppManageService;
import org.edu.modules.base.service.IVeBaseAppUserService;
import org.edu.modules.base.service.IVeBaseServiceGroupService;
import org.edu.modules.base.service.IVeBaseServiceKindService;
import org.edu.modules.base.service.IVeBaseServiceService;
import org.edu.modules.base.service.IVeBaseServiceTypeService;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Api(tags={"服务管理"})
@RestController
@RequestMapping({"/base/veService"})
@ApiSort(60)
public class VeServiceController
{
    private static final Logger log = LoggerFactory.getLogger(VeServiceController.class);
    @Autowired
    private IVeBaseServiceService serviceModelService;
    @Autowired
    private IVeBaseServiceKindService serviceKindService;
    @Autowired
    private IVeBaseServiceGroupService serviceGroupService;
    @Autowired
    private IVeBaseServiceTypeService serviceTypeService;
    @Autowired
    private IVeBaseAppUserService veBaseAppUserService;
    @Autowired
    private IVeBaseAppManageService veBaseAppManageService;

    @AutoLog("服务信息-分页列表查询")
    @ApiOperation(value="服务信息-分页列表查询", notes="服务信息-分页列表查询")
    @GetMapping({"/serviceModelPageList"})
    public Result<?> serviceModelPageList(VeBaseService serviceModel, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        int num = this.serviceModelService.serviceModelAllList(serviceModel);

        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);
        serviceModel.setStartIndex(Integer.valueOf(pb.getStartIndex()));
        serviceModel.setPageSize(Integer.valueOf(pb.getPageSize()));

        pb.setList(this.serviceModelService.serviceModelPageList(serviceModel));
        return Result.ok(pb);
    }

    @AutoLog("通过id查询服务信息")
    @ApiOperation(value="通过id查询服务信息", notes="通过id查询服务信息")
    @GetMapping({"/serviceModelById"})
    public Result<?> serviceModelById(@RequestParam(name="id", required=true) String id)
    {
        VeBaseService serviceModel = (VeBaseService)this.serviceModelService.getById(id);
        if (serviceModel == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(serviceModel);
    }

    @AutoLog("查询服务的服务分类信息")
    @ApiOperation(value="查询服务的服务分类信息", notes="查询服务的服务分类信息")
    @GetMapping({"/serviceKindList"})
    public Result<?> serviceKindList()
    {
        List<VeBaseServiceType> list = this.serviceTypeService.list();
        return Result.OK(list);
    }

    @AutoLog("服务信息-添加")
    @ApiOperation(value="服务信息-添加", notes="服务信息-添加")
    @PostMapping({"/serviceModelAdd"})
    public Result<?> serviceModelAdd(@RequestBody VeBaseService serviceModel)
    {
        String id = UUID.randomUUID().toString().replace("-", "");
        serviceModel.setId(id);

        VeBaseService model = this.serviceModelService.getServiceByName(null, serviceModel.getName());
        if (model != null) {
            return Result.error("服务名称已存在! ");
        }
        this.serviceModelService.save(serviceModel);
        return Result.ok("添加成功!");
    }

    @AutoLog("服务信息-修改")
    @ApiOperation(value="服务信息-修改", notes="服务信息-修改")
    @PostMapping({"/serviceModelEdit"})
    public Result<?> serviceModelEdit(@RequestBody VeBaseService serviceModel)
            throws ParseException
    {
        VeBaseService model = this.serviceModelService.getServiceByName(serviceModel.getId(), serviceModel.getName());
        if (model != null) {
            return Result.error("服务名称已存在! ");
        }
        Date date = new Date();
        SimpleDateFormat sdf = new SimpleDateFormat(" yyyy-MM-dd HH:mm:ss");
        String nowTime = sdf.format(date);
        Date time = sdf.parse(nowTime);
        serviceModel.setDt(time);
        this.serviceModelService.updateById(serviceModel);
        return Result.ok("修改成功!");
    }

    @AutoLog("服务信息-单个删除")
    @ApiOperation(value="服务信息-单个删除", notes="服务信息-单个删除")
    @PostMapping({"/serviceModelDelete"})
    @Transactional
    public Result<?> serviceModelDelete(@RequestParam(name="id", required=true) String id)
    {
        this.serviceModelService.removeById(id);

        this.serviceGroupService.deleteByAppId(id);
        return Result.ok("删除成功!");
    }

    @AutoLog("服务信息-批量删除")
    @ApiOperation(value="服务信息-批量删除", notes="服务信息-批量删除")
    @PostMapping({"/serviceModelDeleteBatch"})
    public Result<?> serviceModelDeleteBatch(@RequestParam(name="ids", required=true) String ids)
    {
        List<String> list = Arrays.asList(ids.split(","));
        this.serviceModelService.removeByIds(list);
        if (list.size() > 0) {
            for (String id : list) {
                this.serviceGroupService.deleteByAppId(id);
            }
        }
        return Result.ok("批量删除成功!");
    }

    @RequestMapping({"/serviceModelExportXls"})
    public ModelAndView serviceModelExportXls(VeBaseService serviceModel, HttpServletRequest request)
    {
        QueryWrapper<VeBaseService> queryWrapper = QueryGenerator.initQueryWrapper(serviceModel, request.getParameterMap());

        ModelAndView modelAndView = new ModelAndView(new JeecgEntityExcelView());
        List<VeBaseService> pageList = this.serviceModelService.list(queryWrapper);

        modelAndView.addObject("fileName", "服务信息");
        modelAndView.addObject("entity", VeBaseService.class);
        LoginUser user = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        modelAndView.addObject("params", new ExportParams("服务信息列表", "导出人:" + user.getRealname(), "导出信息"));
        modelAndView.addObject("data", pageList);
        return modelAndView;
    }

    @AutoLog("服务群组信息-分页列表查询")
    @ApiOperation(value="服务群组信息-分页列表查询", notes="服务群组信息-分页列表查询")
    @GetMapping({"/serviceGroupPageList"})
    public Result<?> serviceGroupPageList(VeBaseServiceGroup serviceGroup, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        int num = this.serviceGroupService.serviceGroupAllList(serviceGroup.getGroupName());

        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);

        pb.setList(this.serviceGroupService.serviceGroupPageList(serviceGroup.getGroupName(), Integer.valueOf(pb.getStartIndex()), pageSize));
        return Result.ok(pb);
    }

    @AutoLog("分配用户的信息列表-分页列表查询")
    @ApiOperation(value="分配用户的信息列表-分页列表查询", notes="分配用户的信息列表-分页列表查询")
    @GetMapping({"/serviceGroupAppUserPageList"})
    public Result<?> serviceGroupAppUserPageList(VeBaseAppUser veBaseAppUser, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        int num = this.veBaseAppUserService.serviceGroupAppUserAllList(veBaseAppUser);

        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);

        veBaseAppUser.setStartIndex(Integer.valueOf(pb.getStartIndex()));
        veBaseAppUser.setPageSize(pageSize);
        pb.setList(this.veBaseAppUserService.serviceGroupAppUserPageList(veBaseAppUser));
        return Result.ok(pb);
    }

    @AutoLog("分配服务的信息列表-分页列表查询")
    @ApiOperation(value="分配服务的信息列表-分页列表查询", notes="分配服务的信息列表-分页列表查询")
    @GetMapping({"/serviceGroupAppManagePageList"})
    public Result<?> serviceGroupAppManagePageList(VeBaseService veBaseService, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        int num = this.serviceModelService.serviceGroupAppManageAllList(veBaseService);

        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);

        veBaseService.setStartIndex(Integer.valueOf(pb.getStartIndex()));
        veBaseService.setPageSize(pageSize);
        pb.setList(this.serviceModelService.serviceGroupAppManagePageList(veBaseService));
        return Result.ok(pb);
    }

    @AutoLog("分配用户-添加")
    @ApiOperation(value="分配用户-添加", notes="分配用户-添加")
    @PostMapping({"/serviceGroupAppUserAdd"})
    @Transactional
    public Result<?> serviceGroupAppUserAdd(@RequestBody VeBaseServiceGroupUser veBaseServiceGroupUser)
    {
        if (("".equals(veBaseServiceGroupUser.getGroupId())) || (veBaseServiceGroupUser.getGroupId() == null)) {
            return Result.error("群组不能为空!");
        }
        String[] u_id = veBaseServiceGroupUser.getUserId().split(",");
        this.veBaseAppUserService.deleteServiceGroupAppUser(veBaseServiceGroupUser.getGroupId());
        if (u_id.length > 0) {
            this.veBaseAppUserService.serviceGroupAppUserAdd(veBaseServiceGroupUser.getGroupId(), u_id);
        }
        return Result.ok("分配成功!");
    }

    @AutoLog("分配服务-添加")
    @ApiOperation(value="分配服务-添加", notes="分配服务-添加")
    @PostMapping({"/serviceGroupAppManageAdd"})
    public Result<?> serviceGroupAppManageAdd(@RequestBody VeBaseServiceGroupApp veBaseServiceGroupApp)
    {
        if (("".equals(veBaseServiceGroupApp.getGroupId())) || (veBaseServiceGroupApp.getGroupId() == null)) {
            return Result.error("群组不能为空!");
        }
        String[] app_id = veBaseServiceGroupApp.getAppId().split(",");
        this.serviceGroupService.deleteGroupAppByGroupId(veBaseServiceGroupApp.getGroupId());
        if (app_id.length > 0) {
            this.serviceGroupService.addGroupApp(veBaseServiceGroupApp.getGroupId(), app_id);
        }
        return Result.ok("分配成功!");
    }

    @AutoLog("删除-分配用户")
    @ApiOperation(value="删除-分配用户", notes="删除-分配用户")
    @PostMapping({"/deleteServiceGroupAppUser"})
    public Result<?> deleteServiceGroupAppUser(@RequestParam(name="groupId", required=true) String groupId, @RequestParam(name="userIds", required=true) String userIds)
    {
        String[] arrays = userIds.split(",");
        this.veBaseAppUserService.deleteGroupAppUser(groupId, arrays);
        return Result.ok("取消分配成功!");
    }

    @AutoLog("删除-分配服务")
    @ApiOperation(value="删除-分配服务", notes="删除-分配服务")
    @PostMapping({"/deleteServiceGroupAppManage"})
    public Result<?> deleteServiceGroupAppManage(@RequestParam(name="groupId", required=true) String groupId, @RequestParam(name="appIds", required=true) String appIds)
    {
        String[] arrays = appIds.split(",");
        this.serviceGroupService.deleteServiceGroupAppManage(groupId, arrays);
        return Result.ok("取消分配成功!");
    }

    @AutoLog("通过id查询服务群组信息")
    @ApiOperation(value="通过id查询服务群组信息", notes="通过id查询服务群组信息")
    @GetMapping({"/serviceGroupById"})
    public Result<?> serviceGroupById(@RequestParam(name="id", required=true) String id)
    {
        VeBaseServiceGroup serviceGroup = (VeBaseServiceGroup)this.serviceGroupService.getById(id);
        if (serviceGroup == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(serviceGroup);
    }

    @AutoLog("服务群组信息-添加")
    @ApiOperation(value="服务群组信息-添加", notes="服务群组信息-添加")
    @PostMapping({"/serviceGroupAdd"})
    public Result<?> serviceGroupAdd(@RequestBody VeBaseServiceGroup serviceGroup)
    {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        serviceGroup.setId(uuid);

        VeBaseServiceGroup model = this.serviceGroupService.getServiceGroupByGroupName(null, serviceGroup.getGroupName());
        if (model != null) {
            return Result.error("服务群组名称已存在! ");
        }
        this.serviceGroupService.save(serviceGroup);
        return Result.ok("添加成功!");
    }

    @AutoLog("服务群组信息-修改")
    @ApiOperation(value="服务群组信息-修改", notes="服务群组信息-修改")
    @PostMapping({"/serviceGroupEdit"})
    public Result<?> serviceGroupEdit(@RequestBody VeBaseServiceGroup serviceGroup)
    {
        VeBaseServiceGroup model = this.serviceGroupService.getServiceGroupByGroupName(serviceGroup.getId(), serviceGroup.getGroupName());
        if (model != null) {
            return Result.error("服务群组名称已存在! ");
        }
        this.serviceGroupService.updateById(serviceGroup);
        return Result.ok("修改成功!");
    }

    @AutoLog("服务群组信息-单个删除")
    @ApiOperation(value="服务群组信息-单个删除", notes="服务群组信息-单个删除")
    @PostMapping({"/serviceGroupDelete"})
    public Result<?> serviceGroupDelete(@RequestParam(name="id", required=true) String id)
    {
        this.serviceGroupService.removeById(id);
        return Result.ok("删除成功!");
    }

    @AutoLog("服务群组信息-批量删除")
    @ApiOperation(value="服务群组信息-批量删除", notes="服务群组信息-批量删除")
    @PostMapping({"/serviceGroupDeleteBatch"})
    public Result<?> serviceGroupDeleteBatch(@RequestParam(name="ids", required=true) String ids)
    {
        this.serviceGroupService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    @RequestMapping({"/serviceGroupExportXls"})
    public ModelAndView serviceGroupExportXls(VeBaseServiceGroup serviceGroup, HttpServletRequest request)
    {
        QueryWrapper<VeBaseServiceGroup> queryWrapper = QueryGenerator.initQueryWrapper(serviceGroup, request.getParameterMap());

        ModelAndView modelAndView = new ModelAndView(new JeecgEntityExcelView());
        List<VeBaseServiceGroup> pageList = this.serviceGroupService.list(queryWrapper);

        modelAndView.addObject("fileName", "服务群组信息");
        modelAndView.addObject("entity", VeBaseServiceGroup.class);
        LoginUser user = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        modelAndView.addObject("params", new ExportParams("服务群组信息列表", "导出人:" + user.getRealname(), "导出信息"));
        modelAndView.addObject("data", pageList);
        return modelAndView;
    }

    @AutoLog("服务类型信息-分页列表查询")
    @ApiOperation(value="服务类型信息-分页列表查询", notes="服务类型信息-分页列表查询")
    @GetMapping({"/serviceTypePageList"})
    public Result<?> serviceTypePageList(VeBaseServiceType serviceType, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        int num = this.serviceTypeService.serviceTypeAllList(serviceType.getName());

        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);

        pb.setList(this.serviceTypeService.serviceTypePageList(serviceType.getName(), Integer.valueOf(pb.getStartIndex()), pageSize));
        return Result.ok(pb);
    }

    @AutoLog("通过id查询服务类型信息")
    @ApiOperation(value="通过id查询服务类型信息", notes="通过id查询服务类型信息")
    @GetMapping({"/serviceTypeById"})
    public Result<?> serviceTypeById(@RequestParam(name="id", required=true) String id)
    {
        VeBaseServiceType serviceType = (VeBaseServiceType)this.serviceTypeService.getById(id);
        if (serviceType == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(serviceType);
    }

    @AutoLog("服务类型信息-添加")
    @ApiOperation(value="服务类型信息-添加", notes="服务类型信息-添加")
    @PostMapping({"/serviceTypeAdd"})
    public Result<?> serviceTypeAdd(@RequestBody VeBaseServiceType serviceType)
    {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        serviceType.setId(uuid);

        VeBaseServiceType model = this.serviceTypeService.getServiceTypeByName(null, serviceType.getName());
        if (model != null) {
            return Result.error("服务类型名称已存在! ");
        }
        this.serviceTypeService.save(serviceType);
        return Result.ok("添加成功!");
    }

    @AutoLog("服务类型信息-修改")
    @ApiOperation(value="服务类型信息-修改", notes="服务类型信息-修改")
    @PostMapping({"/serviceTypeEdit"})
    public Result<?> serviceTypeEdit(@RequestBody VeBaseServiceType serviceType)
    {
        VeBaseServiceType model = this.serviceTypeService.getServiceTypeByName(serviceType.getId(), serviceType.getName());
        if (model != null) {
            return Result.error("服务类型名称已存在! ");
        }
        this.serviceTypeService.updateById(serviceType);
        return Result.ok("修改成功!");
    }

    @AutoLog("服务类型信息-单个删除")
    @ApiOperation(value="服务类型信息-单个删除", notes="服务类型信息-单个删除")
    @PostMapping({"/serviceTypeDelete"})
    public Result<?> serviceTypeDelete(@RequestParam(name="id", required=true) String id)
    {
        this.serviceTypeService.removeById(id);
        return Result.ok("删除成功!");
    }

    @AutoLog("服务类型信息-批量删除")
    @ApiOperation(value="服务类型信息-批量删除", notes="服务类型信息-批量删除")
    @PostMapping({"/serviceTypeDeleteBatch"})
    public Result<?> serviceTypeDeleteBatch(@RequestParam(name="ids", required=true) String ids)
    {
        this.serviceTypeService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    @RequestMapping({"/serviceTypeExportXls"})
    public ModelAndView serviceTypeExportXls(VeBaseServiceType serviceType, HttpServletRequest request)
    {
        QueryWrapper<VeBaseServiceType> queryWrapper = QueryGenerator.initQueryWrapper(serviceType, request.getParameterMap());

        ModelAndView modelAndView = new ModelAndView(new JeecgEntityExcelView());
        List<VeBaseServiceType> pageList = this.serviceTypeService.list(queryWrapper);

        modelAndView.addObject("fileName", "服务类型信息");
        modelAndView.addObject("entity", VeBaseServiceType.class);
        LoginUser user = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        modelAndView.addObject("params", new ExportParams("服务类型信息列表", "导出人:" + user.getRealname(), "导出信息"));
        modelAndView.addObject("data", pageList);
        return modelAndView;
    }
}
