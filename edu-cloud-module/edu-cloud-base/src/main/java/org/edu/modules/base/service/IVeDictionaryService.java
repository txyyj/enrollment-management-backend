package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.base.entity.VeDictionary;

public abstract interface IVeDictionaryService
        extends IService<VeDictionary>
{
    public abstract List<VeDictionary> selectAll(VeDictionary paramVeDictionary, Integer paramInteger1, Integer paramInteger2);

    public abstract List<VeDictionary> getTreeList();

    public abstract void saves(VeDictionary paramVeDictionary);

    public abstract int deleteDictionary(String paramString);

    public abstract int deleteDictData(String paramString);
}
