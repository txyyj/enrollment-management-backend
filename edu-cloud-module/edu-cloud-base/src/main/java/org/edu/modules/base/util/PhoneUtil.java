package org.edu.modules.base.util;

import java.util.regex.Pattern;

public class PhoneUtil
{
    public static boolean isMobile(String mobile)
    {
        String regex = "(\\+\\d+)?1[3458]\\d{9}$";
        return Pattern.matches(regex, mobile);
    }

    public static boolean isPostCode(String postCode)
    {
        String reg = "[1-9]\\d{5}";
        return Pattern.matches(reg, postCode);
    }
}
