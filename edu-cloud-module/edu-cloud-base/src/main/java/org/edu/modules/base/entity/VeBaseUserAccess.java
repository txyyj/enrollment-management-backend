package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

@TableName("ve_base_user_access")
@ApiModel(value="ve_base_user_access对象", description="用户访问记录")
public class VeBaseUserAccess
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="用户ID", width=15.0D)
    @ApiModelProperty("用户ID")
    private String userId;
    @Excel(name="用户密码", width=15.0D)
    @ApiModelProperty("用户密码")
    private String userPassword;
    @Excel(name="用户操作", width=15.0D)
    @ApiModelProperty("用户操作|0:登录,1:验证,2:登出")
    private String opr;
    @Excel(name="操作结果", width=15.0D)
    @ApiModelProperty("操作结果|0000:登录成功,2222:验票失败,9999:登录失败")
    private String status;
    @Excel(name="ticket", width=15.0D)
    @ApiModelProperty("ticket")
    private String ticket;
    @Excel(name="认证次数", width=15.0D)
    @ApiModelProperty("认证次数")
    private Double checkcount;
    @Excel(name="用户名", width=15.0D)
    @ApiModelProperty("用户名")
    private String userName;
    @Excel(name="登录时间", width=15.0D)
    @ApiModelProperty("登录时间")
    @JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @Excel(name="最新活动时间", width=15.0D)
    @ApiModelProperty("最新活动时间")
    private Date updateTime;
    @TableField(exist=false)
    private Integer startIndex;
    @TableField(exist=false)
    private Integer pageSize;

    public VeBaseUserAccess setUserPassword(String userPassword)
    {
        this.userPassword = userPassword;return this;
    }

    public VeBaseUserAccess setUserId(String userId)
    {
        this.userId = userId;return this;
    }

    public VeBaseUserAccess setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseUserAccess(id=" + getId() + ", userId=" + getUserId() + ", userPassword=" + getUserPassword() + ", opr=" + getOpr() + ", status=" + getStatus() + ", ticket=" + getTicket() + ", checkcount=" + getCheckcount() + ", userName=" + getUserName() + ", createTime=" + getCreateTime() + ", updateTime=" + getUpdateTime() + ", startIndex=" + getStartIndex() + ", pageSize=" + getPageSize() + ")";
    }

    public VeBaseUserAccess setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;return this;
    }

    public VeBaseUserAccess setStartIndex(Integer startIndex)
    {
        this.startIndex = startIndex;return this;
    }

    public VeBaseUserAccess setUpdateTime(Date updateTime)
    {
        this.updateTime = updateTime;return this;
    }

    @JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    public VeBaseUserAccess setCreateTime(Date createTime)
    {
        this.createTime = createTime;return this;
    }

    public VeBaseUserAccess setUserName(String userName)
    {
        this.userName = userName;return this;
    }

    public VeBaseUserAccess setCheckcount(Double checkcount)
    {
        this.checkcount = checkcount;return this;
    }

    public VeBaseUserAccess setTicket(String ticket)
    {
        this.ticket = ticket;return this;
    }

    public VeBaseUserAccess setStatus(String status)
    {
        this.status = status;return this;
    }

    public VeBaseUserAccess setOpr(String opr)
    {
        this.opr = opr;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $checkcount = getCheckcount();result = result * 59 + ($checkcount == null ? 43 : $checkcount.hashCode());Object $startIndex = getStartIndex();result = result * 59 + ($startIndex == null ? 43 : $startIndex.hashCode());Object $pageSize = getPageSize();result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $userId = getUserId();result = result * 59 + ($userId == null ? 43 : $userId.hashCode());Object $userPassword = getUserPassword();result = result * 59 + ($userPassword == null ? 43 : $userPassword.hashCode());Object $opr = getOpr();result = result * 59 + ($opr == null ? 43 : $opr.hashCode());Object $status = getStatus();result = result * 59 + ($status == null ? 43 : $status.hashCode());Object $ticket = getTicket();result = result * 59 + ($ticket == null ? 43 : $ticket.hashCode());Object $userName = getUserName();result = result * 59 + ($userName == null ? 43 : $userName.hashCode());Object $createTime = getCreateTime();result = result * 59 + ($createTime == null ? 43 : $createTime.hashCode());Object $updateTime = getUpdateTime();result = result * 59 + ($updateTime == null ? 43 : $updateTime.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseUserAccess;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseUserAccess)) {
            return false;
        }
        VeBaseUserAccess other = (VeBaseUserAccess)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$checkcount = getCheckcount();Object other$checkcount = other.getCheckcount();
        if (this$checkcount == null ? other$checkcount != null : !this$checkcount.equals(other$checkcount)) {
            return false;
        }
        Object this$startIndex = getStartIndex();Object other$startIndex = other.getStartIndex();
        if (this$startIndex == null ? other$startIndex != null : !this$startIndex.equals(other$startIndex)) {
            return false;
        }
        Object this$pageSize = getPageSize();Object other$pageSize = other.getPageSize();
        if (this$pageSize == null ? other$pageSize != null : !this$pageSize.equals(other$pageSize)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$userId = getUserId();Object other$userId = other.getUserId();
        if (this$userId == null ? other$userId != null : !this$userId.equals(other$userId)) {
            return false;
        }
        Object this$userPassword = getUserPassword();Object other$userPassword = other.getUserPassword();
        if (this$userPassword == null ? other$userPassword != null : !this$userPassword.equals(other$userPassword)) {
            return false;
        }
        Object this$opr = getOpr();Object other$opr = other.getOpr();
        if (this$opr == null ? other$opr != null : !this$opr.equals(other$opr)) {
            return false;
        }
        Object this$status = getStatus();Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) {
            return false;
        }
        Object this$ticket = getTicket();Object other$ticket = other.getTicket();
        if (this$ticket == null ? other$ticket != null : !this$ticket.equals(other$ticket)) {
            return false;
        }
        Object this$userName = getUserName();Object other$userName = other.getUserName();
        if (this$userName == null ? other$userName != null : !this$userName.equals(other$userName)) {
            return false;
        }
        Object this$createTime = getCreateTime();Object other$createTime = other.getCreateTime();
        if (this$createTime == null ? other$createTime != null : !this$createTime.equals(other$createTime)) {
            return false;
        }
        Object this$updateTime = getUpdateTime();Object other$updateTime = other.getUpdateTime();return this$updateTime == null ? other$updateTime == null : this$updateTime.equals(other$updateTime);
    }

    public String getId()
    {
        return this.id;
    }

    public String getUserId()
    {
        return this.userId;
    }

    public String getUserPassword()
    {
        return this.userPassword;
    }

    public String getOpr()
    {
        return this.opr;
    }

    public String getStatus()
    {
        return this.status;
    }

    public String getTicket()
    {
        return this.ticket;
    }

    public Double getCheckcount()
    {
        return this.checkcount;
    }

    public String getUserName()
    {
        return this.userName;
    }

    public Date getCreateTime()
    {
        return this.createTime;
    }

    public Date getUpdateTime()
    {
        return this.updateTime;
    }

    public Integer getStartIndex()
    {
        return this.startIndex;
    }

    public Integer getPageSize()
    {
        return this.pageSize;
    }
}
