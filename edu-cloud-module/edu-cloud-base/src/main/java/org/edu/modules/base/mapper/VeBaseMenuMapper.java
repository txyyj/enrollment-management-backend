package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.edu.modules.base.entity.VeBaseMenu;

public abstract interface VeBaseMenuMapper
        extends BaseMapper<VeBaseMenu>
{
    public abstract List<VeBaseMenu> getRootList();

    public abstract List<VeBaseMenu> getBodyList();
}
