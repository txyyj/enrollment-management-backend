package org.edu.modules.base.feign;

import org.edu.common.api.vo.Result;
import org.edu.modules.base.entity.SysLog;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient("edu-common")
@Component
public abstract interface EduBootCommonFeignInterface
{
    @GetMapping({"/common/veCommon/updateOrganUserRedis"})
    public abstract Result<?> updateOrganUserRedis();

    @GetMapping({"/common/veCommon/queryDictionaryTreeListRedis"})
    public abstract Result<?> queryDictionaryTreeListRedis(@RequestParam(name="interfaceUserId", required=true) String paramString);

    @GetMapping({"/common/veCommon/querySysLogPageList"})
    public abstract Result<?> querySysLogPageList(@SpringQueryMap SysLog paramSysLog, @RequestParam(name="pageNo", defaultValue="1") Integer paramInteger1, @RequestParam(name="pageSize", defaultValue="10") Integer paramInteger2);
}
