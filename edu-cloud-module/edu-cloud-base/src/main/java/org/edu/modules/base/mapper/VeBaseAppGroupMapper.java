package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseAppGroup;

public abstract interface VeBaseAppGroupMapper
        extends BaseMapper<VeBaseAppGroup>
{
    public abstract int getAppGroupAllList(String paramString);

    public abstract List<VeBaseAppGroup> getAppGroupPageList(String paramString, Integer paramInteger1, Integer paramInteger2);

    public abstract List<Map<String, Object>> getAppManageList(String paramString);

    public abstract VeBaseAppGroup getAppGroupByName(String paramString1, String paramString2);

    public abstract int addAppGroupManageBatch(String paramString, String[] paramArrayOfString);

    public abstract int deleteAppGroupManageById(String paramString);
}
