package org.edu.modules.base.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.modules.base.entity.PageBean;
import org.edu.modules.base.entity.VeBaseAppUser;
import org.edu.modules.base.service.IVeBaseAppUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags={"统计管理"})
@RestController
@RequestMapping({"/base/veStatistics"})
@ApiSort(60)
public class VeStatisticsController
{
    private static final Logger log = LoggerFactory.getLogger(VeStatisticsController.class);
    @Autowired
    private IVeBaseAppUserService appUserService;

    @AutoLog("僵尸用户列表-分页列表查询")
    @ApiOperation(value="僵尸用户列表-分页列表查询", notes="僵尸用户列表-分页列表查询")
    @GetMapping({"/zombieUserPageList"})
    public Result<?> zombieUserPageList(VeBaseAppUser appUser, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        List<VeBaseAppUser> list = this.appUserService.zombieUserAllList(appUser);

        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), list.size());
        List<VeBaseAppUser> pageList = new ArrayList();
        for (int i = 0; i < list.size(); i++) {
            if (pb.getStartIndex() == i) {
                if (list.size() - pb.getStartIndex() >= pageSize.intValue())
                {
                    int x = pb.getStartIndex();
                    for (int j = 0; j < pageSize.intValue(); j++)
                    {
                        VeBaseAppUser model = new VeBaseAppUser();
                        model.setId(((VeBaseAppUser)list.get(x)).getId());
                        model.setUserId(((VeBaseAppUser)list.get(x)).getUserId());
                        model.setUserPassword(((VeBaseAppUser)list.get(x)).getUserPassword());
                        model.setUserName(((VeBaseAppUser)list.get(x)).getUserName());
                        model.setRealName(((VeBaseAppUser)list.get(x)).getRealName());
                        model.setStatus(((VeBaseAppUser)list.get(x)).getStatus());
                        model.setUserType(((VeBaseAppUser)list.get(x)).getUserType());
                        model.setUserTel(((VeBaseAppUser)list.get(x)).getUserTel());
                        model.setStatusVal(((VeBaseAppUser)list.get(x)).getStatusVal());
                        model.setIsDel(((VeBaseAppUser)list.get(x)).getIsDel());
                        model.setNumbers(((VeBaseAppUser)list.get(x)).getNumbers());
                        model.setUserApp(((VeBaseAppUser)list.get(x)).getUserApp());
                        model.setCreateTime(((VeBaseAppUser)list.get(x)).getCreateTime());
                        model.setUpdateTime(((VeBaseAppUser)list.get(x)).getUpdateTime());
                        model.setRoleName(((VeBaseAppUser)list.get(x)).getRoleName());
                        pageList.add(model);
                        x++;
                    }
                }
                else
                {
                    int x = pb.getStartIndex();
                    for (int j = 0; j < list.size() - pb.getStartIndex(); j++)
                    {
                        VeBaseAppUser model = new VeBaseAppUser();
                        model.setId(((VeBaseAppUser)list.get(x)).getId());
                        model.setUserId(((VeBaseAppUser)list.get(x)).getUserId());
                        model.setUserPassword(((VeBaseAppUser)list.get(x)).getUserPassword());
                        model.setUserName(((VeBaseAppUser)list.get(x)).getUserName());
                        model.setRealName(((VeBaseAppUser)list.get(x)).getRealName());
                        model.setStatus(((VeBaseAppUser)list.get(x)).getStatus());
                        model.setUserType(((VeBaseAppUser)list.get(x)).getUserType());
                        model.setUserTel(((VeBaseAppUser)list.get(x)).getUserTel());
                        model.setStatusVal(((VeBaseAppUser)list.get(x)).getStatusVal());
                        model.setIsDel(((VeBaseAppUser)list.get(x)).getIsDel());
                        model.setNumbers(((VeBaseAppUser)list.get(x)).getNumbers());
                        model.setUserApp(((VeBaseAppUser)list.get(x)).getUserApp());
                        model.setCreateTime(((VeBaseAppUser)list.get(x)).getCreateTime());
                        model.setUpdateTime(((VeBaseAppUser)list.get(x)).getUpdateTime());
                        model.setRoleName(((VeBaseAppUser)list.get(x)).getRoleName());
                        pageList.add(model);
                        x++;
                    }
                }
            }
        }
        pb.setList(pageList);
        return Result.ok(pb);
    }

    @AutoLog("账号信息-分页列表查询")
    @ApiOperation(value="账号信息-分页列表查询", notes="账号信息-分页列表查询")
    @GetMapping({"/appUserPageList"})
    public Result<?> appUserPageList(VeBaseAppUser appUser, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        int num = this.appUserService.getLoginUserAllList(appUser);
        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);
        appUser.setStartIndex(Integer.valueOf(pb.getStartIndex()));
        appUser.setPageSize(pageSize);
        pb.setList(this.appUserService.getLoginUserPageList(appUser));
        return Result.ok(pb);
    }

    @AutoLog("通过id查询用户详情信息")
    @ApiOperation(value="通过id查询用户详情信息", notes="通过id查询用户详情信息")
    @GetMapping({"/appUserById"})
    public Result<?> appUserById(@RequestParam(name="id", required=true) String id)
    {
        VeBaseAppUser appUser = (VeBaseAppUser)this.appUserService.getById(id);
        if (appUser == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(appUser);
    }
}
