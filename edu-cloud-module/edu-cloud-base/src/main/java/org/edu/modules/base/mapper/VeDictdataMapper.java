package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.base.entity.VeDictdata;
import org.edu.modules.base.entity.VeDictionary;

@Mapper
public abstract interface VeDictdataMapper
        extends BaseMapper<VeDictdata>
{
    public abstract List<VeDictdata> selectAll(@Param("veDictionary") VeDictionary paramVeDictionary, @Param("startLine") Integer paramInteger1, @Param("pageSize") Integer paramInteger2);

    public abstract int getSumPage(@Param("veDictionary") VeDictionary paramVeDictionary);

    public abstract List<String> getEnterNatures();

    public abstract List<String> getEnterScale();
}
