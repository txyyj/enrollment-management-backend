package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseServiceType;

public abstract interface VeBaseServiceTypeMapper
        extends BaseMapper<VeBaseServiceType>
{
    public abstract int serviceTypeAllList(String paramString);

    public abstract List<Map<String, Object>> serviceTypePageList(String paramString, Integer paramInteger1, Integer paramInteger2);

    public abstract VeBaseServiceType getServiceTypeByName(String paramString1, String paramString2);
}
