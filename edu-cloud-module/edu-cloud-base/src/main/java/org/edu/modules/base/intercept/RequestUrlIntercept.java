package org.edu.modules.base.intercept;

import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

@Component
public class RequestUrlIntercept
        implements HandlerInterceptor
{
    private Logger logger = LoggerFactory.getLogger(RequestUrlIntercept.class);

    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception
    {
        this.logger.info("请求开始  --- Url : " + request.getRequestURI());


        Map<String, String[]> list = request.getParameterMap();
        return true;
    }

    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception
    {
        try
        {
            InetAddress ip4 = Inet4Address.getLocalHost();
            String str = ip4.getHostAddress();
        }
        catch (UnknownHostException e)
        {
            e.printStackTrace();
        }
        Map<String, String[]> list = request.getParameterMap();
        this.logger.info("请求结束");
        if (ex != null) {
            this.logger.error("报错信息 : ", ex);
        }
    }
}
