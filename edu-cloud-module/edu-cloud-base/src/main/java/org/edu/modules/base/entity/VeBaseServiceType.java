package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_service_type")
@ApiModel(value="ve_base_service_type对象", description="服务类型信息")
public class VeBaseServiceType
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="名称", width=15.0D)
    @ApiModelProperty("名称")
    private String name;
    @Excel(name="orders", width=15.0D)
    @ApiModelProperty("orders")
    private Double orders;
    @Excel(name="icon", width=15.0D)
    @ApiModelProperty("icon")
    private String icon;

    public VeBaseServiceType setIcon(String icon)
    {
        this.icon = icon;return this;
    }

    public VeBaseServiceType setName(String name)
    {
        this.name = name;return this;
    }

    public String toString()
    {
        return "VeBaseServiceType(id=" + getId() + ", name=" + getName() + ", orders=" + getOrders() + ", icon=" + getIcon() + ")";
    }

    public VeBaseServiceType setId(String id)
    {
        this.id = id;return this;
    }

    public VeBaseServiceType setOrders(Double orders)
    {
        this.orders = orders;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $orders = getOrders();result = result * 59 + ($orders == null ? 43 : $orders.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $name = getName();result = result * 59 + ($name == null ? 43 : $name.hashCode());Object $icon = getIcon();result = result * 59 + ($icon == null ? 43 : $icon.hashCode());return result;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseServiceType)) {
            return false;
        }
        VeBaseServiceType other = (VeBaseServiceType)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$orders = getOrders();Object other$orders = other.getOrders();
        if (this$orders == null ? other$orders != null : !this$orders.equals(other$orders)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$name = getName();Object other$name = other.getName();
        if (this$name == null ? other$name != null : !this$name.equals(other$name)) {
            return false;
        }
        Object this$icon = getIcon();Object other$icon = other.getIcon();return this$icon == null ? other$icon == null : this$icon.equals(other$icon);
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseServiceType;
    }

    public String getId()
    {
        return this.id;
    }

    public String getName()
    {
        return this.name;
    }

    public Double getOrders()
    {
        return this.orders;
    }

    public String getIcon()
    {
        return this.icon;
    }
}
