package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.Date;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseUserAccess;

public abstract interface VeBaseUserAccessMapper
        extends BaseMapper<VeBaseUserAccess>
{
    public abstract List<Map<String, Object>> userAccessStatistics(List<Date> paramList);

    public abstract int errorUserAccessAllList(VeBaseUserAccess paramVeBaseUserAccess);

    public abstract List<Map<String, Object>> errorUserAccessPageList(VeBaseUserAccess paramVeBaseUserAccess);

    public abstract int userAccessAllList(VeBaseUserAccess paramVeBaseUserAccess);

    public abstract List<Map<String, Object>> userAccessPageList(VeBaseUserAccess paramVeBaseUserAccess);

    public abstract List<Map<String, Object>> userAccessHourStatistics(List<String> paramList);

    public abstract Map userAccessDayStatistics(String paramString);

    public abstract int userAppALLStatistics();

    public abstract int userAppNormalStatistics();

    public abstract int userAppLockStatistics();

    public abstract int userAppAccessStatistics();
}
