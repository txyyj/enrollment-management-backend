package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseLoginSrategy;

public abstract interface IVeBaseLoginSrategyService
        extends IService<VeBaseLoginSrategy>
{
    public abstract int getLoginSrategyAllList(VeBaseLoginSrategy paramVeBaseLoginSrategy);

    public abstract List<Map<String, Object>> getLoginSrategyPageList(VeBaseLoginSrategy paramVeBaseLoginSrategy);
}
