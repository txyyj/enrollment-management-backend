package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_sys_menu")
@ApiModel(value="ve_base_sys_menu对象", description="权限管理-菜单信息表")
public class VeBaseSysMenu
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="上一级ID", width=15.0D)
    @ApiModelProperty("上一级ID")
    private Integer pid;
    @Excel(name="名称", width=15.0D)
    @ApiModelProperty("名称")
    private String text;
    @Excel(name="链接地址", width=15.0D)
    @ApiModelProperty("链接地址")
    private String url;
    @Excel(name="路径", width=15.0D)
    @ApiModelProperty("路径")
    private String path;
    @Excel(name="", width=15.0D)
    @ApiModelProperty("")
    private String iconcls;
    @Excel(name="菜单颜色", width=15.0D)
    @ApiModelProperty("菜单颜色")
    private String iconcolour;
    @Excel(name="是否为分类1是0否", width=15.0D)
    @ApiModelProperty("是否为分类1是0否")
    private Integer issort;
    @Excel(name="排序", width=15.0D)
    @ApiModelProperty("排序")
    private Integer sort;
    @Excel(name="菜单启用状态0否1是", width=15.0D)
    @ApiModelProperty("菜单启用状态0否1是")
    private Integer status;
    @Excel(name="是否仅开发者模式可见", width=15.0D)
    @ApiModelProperty("是否仅开发者模式可见")
    private Integer isdev;
    @Excel(name="终端系统ID", width=15.0D)
    @ApiModelProperty("终端系统ID")
    private Integer terminalid;
    @Excel(name="子系统ID", width=15.0D)
    @ApiModelProperty("子系统ID")
    private String appId;
    @Excel(name="说明", width=15.0D)
    @ApiModelProperty("说明")
    private String tip;

    public VeBaseSysMenu setText(String text)
    {
        this.text = text;return this;
    }

    public VeBaseSysMenu setPid(Integer pid)
    {
        this.pid = pid;return this;
    }

    public VeBaseSysMenu setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseSysMenu(id=" + getId() + ", pid=" + getPid() + ", text=" + getText() + ", url=" + getUrl() + ", path=" + getPath() + ", iconcls=" + getIconcls() + ", iconcolour=" + getIconcolour() + ", issort=" + getIssort() + ", sort=" + getSort() + ", status=" + getStatus() + ", isdev=" + getIsdev() + ", terminalid=" + getTerminalid() + ", appId=" + getAppId() + ", tip=" + getTip() + ")";
    }

    public VeBaseSysMenu setTip(String tip)
    {
        this.tip = tip;return this;
    }

    public VeBaseSysMenu setAppId(String appId)
    {
        this.appId = appId;return this;
    }

    public VeBaseSysMenu setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeBaseSysMenu setIsdev(Integer isdev)
    {
        this.isdev = isdev;return this;
    }

    public VeBaseSysMenu setStatus(Integer status)
    {
        this.status = status;return this;
    }

    public VeBaseSysMenu setSort(Integer sort)
    {
        this.sort = sort;return this;
    }

    public VeBaseSysMenu setIssort(Integer issort)
    {
        this.issort = issort;return this;
    }

    public VeBaseSysMenu setIconcolour(String iconcolour)
    {
        this.iconcolour = iconcolour;return this;
    }

    public VeBaseSysMenu setIconcls(String iconcls)
    {
        this.iconcls = iconcls;return this;
    }

    public VeBaseSysMenu setPath(String path)
    {
        this.path = path;return this;
    }

    public VeBaseSysMenu setUrl(String url)
    {
        this.url = url;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $pid = getPid();result = result * 59 + ($pid == null ? 43 : $pid.hashCode());Object $issort = getIssort();result = result * 59 + ($issort == null ? 43 : $issort.hashCode());Object $sort = getSort();result = result * 59 + ($sort == null ? 43 : $sort.hashCode());Object $status = getStatus();result = result * 59 + ($status == null ? 43 : $status.hashCode());Object $isdev = getIsdev();result = result * 59 + ($isdev == null ? 43 : $isdev.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $text = getText();result = result * 59 + ($text == null ? 43 : $text.hashCode());Object $url = getUrl();result = result * 59 + ($url == null ? 43 : $url.hashCode());Object $path = getPath();result = result * 59 + ($path == null ? 43 : $path.hashCode());Object $iconcls = getIconcls();result = result * 59 + ($iconcls == null ? 43 : $iconcls.hashCode());Object $iconcolour = getIconcolour();result = result * 59 + ($iconcolour == null ? 43 : $iconcolour.hashCode());Object $appId = getAppId();result = result * 59 + ($appId == null ? 43 : $appId.hashCode());Object $tip = getTip();result = result * 59 + ($tip == null ? 43 : $tip.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseSysMenu;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseSysMenu)) {
            return false;
        }
        VeBaseSysMenu other = (VeBaseSysMenu)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$pid = getPid();Object other$pid = other.getPid();
        if (this$pid == null ? other$pid != null : !this$pid.equals(other$pid)) {
            return false;
        }
        Object this$issort = getIssort();Object other$issort = other.getIssort();
        if (this$issort == null ? other$issort != null : !this$issort.equals(other$issort)) {
            return false;
        }
        Object this$sort = getSort();Object other$sort = other.getSort();
        if (this$sort == null ? other$sort != null : !this$sort.equals(other$sort)) {
            return false;
        }
        Object this$status = getStatus();Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) {
            return false;
        }
        Object this$isdev = getIsdev();Object other$isdev = other.getIsdev();
        if (this$isdev == null ? other$isdev != null : !this$isdev.equals(other$isdev)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$text = getText();Object other$text = other.getText();
        if (this$text == null ? other$text != null : !this$text.equals(other$text)) {
            return false;
        }
        Object this$url = getUrl();Object other$url = other.getUrl();
        if (this$url == null ? other$url != null : !this$url.equals(other$url)) {
            return false;
        }
        Object this$path = getPath();Object other$path = other.getPath();
        if (this$path == null ? other$path != null : !this$path.equals(other$path)) {
            return false;
        }
        Object this$iconcls = getIconcls();Object other$iconcls = other.getIconcls();
        if (this$iconcls == null ? other$iconcls != null : !this$iconcls.equals(other$iconcls)) {
            return false;
        }
        Object this$iconcolour = getIconcolour();Object other$iconcolour = other.getIconcolour();
        if (this$iconcolour == null ? other$iconcolour != null : !this$iconcolour.equals(other$iconcolour)) {
            return false;
        }
        Object this$appId = getAppId();Object other$appId = other.getAppId();
        if (this$appId == null ? other$appId != null : !this$appId.equals(other$appId)) {
            return false;
        }
        Object this$tip = getTip();Object other$tip = other.getTip();return this$tip == null ? other$tip == null : this$tip.equals(other$tip);
    }

    public Integer getId()
    {
        return this.id;
    }

    public Integer getPid()
    {
        return this.pid;
    }

    public String getText()
    {
        return this.text;
    }

    public String getUrl()
    {
        return this.url;
    }

    public String getPath()
    {
        return this.path;
    }

    public String getIconcls()
    {
        return this.iconcls;
    }

    public String getIconcolour()
    {
        return this.iconcolour;
    }

    public Integer getIssort()
    {
        return this.issort;
    }

    public Integer getSort()
    {
        return this.sort;
    }

    public Integer getStatus()
    {
        return this.status;
    }

    public Integer getIsdev()
    {
        return this.isdev;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public String getAppId()
    {
        return this.appId;
    }

    public String getTip()
    {
        return this.tip;
    }
}
