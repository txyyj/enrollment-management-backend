package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseDateSrategy;

public abstract interface IVeBaseDateSrategyService
        extends IService<VeBaseDateSrategy>
{
    public abstract int getDateSrategyAllList(String paramString1, String paramString2);

    public abstract List<Map<String, Object>> getDateSrategyPageList(String paramString1, String paramString2, Integer paramInteger1, Integer paramInteger2);

    public abstract int removeDateSrategyById(String paramString);

    public abstract int addSrategyServer(String paramString, String[] paramArrayOfString);

    public abstract int deleteSrategyServer(String paramString);
}
