package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.base.entity.VeSiteSimple;

public abstract interface VeSiteSimpleMapper
        extends BaseMapper<VeSiteSimple>
{}
