package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_operater_log")
@ApiModel(value="ve_base_operater_log对象", description="操作日志")
public class VeBaseOperaterLog
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="应用模块", width=15.0D)
    @ApiModelProperty("应用模块")
    private String useModule;
    @Excel(name="操作菜单", width=15.0D)
    @ApiModelProperty("操作菜单")
    private String useMenu;
    @Excel(name="操作", width=15.0D)
    @ApiModelProperty("操作")
    private String operater;
    @Excel(name="详情", width=15.0D)
    @ApiModelProperty("详情")
    private String contentInfo;
    @Excel(name="操作者ID", width=15.0D)
    @ApiModelProperty("操作者ID")
    private String operaterUserid;
    @Excel(name="操作人名字", width=15.0D)
    @ApiModelProperty("操作人名字")
    private String operaterUsername;
    @Excel(name="操作IP", width=15.0D)
    @ApiModelProperty("操作IP")
    private String ip;
    @Excel(name="结果", width=15.0D)
    @ApiModelProperty("结果")
    private String result;
    @Excel(name="增加时间", width=15.0D)
    @ApiModelProperty("增加时间")
    private Date createtime;
    @TableField(exist=false)
    private Integer startIndex;
    @TableField(exist=false)
    private Integer pageSize;

    public VeBaseOperaterLog setUseMenu(String useMenu)
    {
        this.useMenu = useMenu;return this;
    }

    public VeBaseOperaterLog setUseModule(String useModule)
    {
        this.useModule = useModule;return this;
    }

    public VeBaseOperaterLog setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseOperaterLog(id=" + getId() + ", useModule=" + getUseModule() + ", useMenu=" + getUseMenu() + ", operater=" + getOperater() + ", contentInfo=" + getContentInfo() + ", operaterUserid=" + getOperaterUserid() + ", operaterUsername=" + getOperaterUsername() + ", ip=" + getIp() + ", result=" + getResult() + ", createtime=" + getCreatetime() + ", startIndex=" + getStartIndex() + ", pageSize=" + getPageSize() + ")";
    }

    public VeBaseOperaterLog setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;return this;
    }

    public VeBaseOperaterLog setStartIndex(Integer startIndex)
    {
        this.startIndex = startIndex;return this;
    }

    public VeBaseOperaterLog setCreatetime(Date createtime)
    {
        this.createtime = createtime;return this;
    }

    public VeBaseOperaterLog setResult(String result)
    {
        this.result = result;return this;
    }

    public VeBaseOperaterLog setIp(String ip)
    {
        this.ip = ip;return this;
    }

    public VeBaseOperaterLog setOperaterUsername(String operaterUsername)
    {
        this.operaterUsername = operaterUsername;return this;
    }

    public VeBaseOperaterLog setOperaterUserid(String operaterUserid)
    {
        this.operaterUserid = operaterUserid;return this;
    }

    public VeBaseOperaterLog setContentInfo(String contentInfo)
    {
        this.contentInfo = contentInfo;return this;
    }

    public VeBaseOperaterLog setOperater(String operater)
    {
        this.operater = operater;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $startIndex = getStartIndex();result = result * 59 + ($startIndex == null ? 43 : $startIndex.hashCode());Object $pageSize = getPageSize();result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $useModule = getUseModule();result = result * 59 + ($useModule == null ? 43 : $useModule.hashCode());Object $useMenu = getUseMenu();result = result * 59 + ($useMenu == null ? 43 : $useMenu.hashCode());Object $operater = getOperater();result = result * 59 + ($operater == null ? 43 : $operater.hashCode());Object $contentInfo = getContentInfo();result = result * 59 + ($contentInfo == null ? 43 : $contentInfo.hashCode());Object $operaterUserid = getOperaterUserid();result = result * 59 + ($operaterUserid == null ? 43 : $operaterUserid.hashCode());Object $operaterUsername = getOperaterUsername();result = result * 59 + ($operaterUsername == null ? 43 : $operaterUsername.hashCode());Object $ip = getIp();result = result * 59 + ($ip == null ? 43 : $ip.hashCode());Object $result = getResult();result = result * 59 + ($result == null ? 43 : $result.hashCode());Object $createtime = getCreatetime();result = result * 59 + ($createtime == null ? 43 : $createtime.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseOperaterLog;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseOperaterLog)) {
            return false;
        }
        VeBaseOperaterLog other = (VeBaseOperaterLog)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$startIndex = getStartIndex();Object other$startIndex = other.getStartIndex();
        if (this$startIndex == null ? other$startIndex != null : !this$startIndex.equals(other$startIndex)) {
            return false;
        }
        Object this$pageSize = getPageSize();Object other$pageSize = other.getPageSize();
        if (this$pageSize == null ? other$pageSize != null : !this$pageSize.equals(other$pageSize)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$useModule = getUseModule();Object other$useModule = other.getUseModule();
        if (this$useModule == null ? other$useModule != null : !this$useModule.equals(other$useModule)) {
            return false;
        }
        Object this$useMenu = getUseMenu();Object other$useMenu = other.getUseMenu();
        if (this$useMenu == null ? other$useMenu != null : !this$useMenu.equals(other$useMenu)) {
            return false;
        }
        Object this$operater = getOperater();Object other$operater = other.getOperater();
        if (this$operater == null ? other$operater != null : !this$operater.equals(other$operater)) {
            return false;
        }
        Object this$contentInfo = getContentInfo();Object other$contentInfo = other.getContentInfo();
        if (this$contentInfo == null ? other$contentInfo != null : !this$contentInfo.equals(other$contentInfo)) {
            return false;
        }
        Object this$operaterUserid = getOperaterUserid();Object other$operaterUserid = other.getOperaterUserid();
        if (this$operaterUserid == null ? other$operaterUserid != null : !this$operaterUserid.equals(other$operaterUserid)) {
            return false;
        }
        Object this$operaterUsername = getOperaterUsername();Object other$operaterUsername = other.getOperaterUsername();
        if (this$operaterUsername == null ? other$operaterUsername != null : !this$operaterUsername.equals(other$operaterUsername)) {
            return false;
        }
        Object this$ip = getIp();Object other$ip = other.getIp();
        if (this$ip == null ? other$ip != null : !this$ip.equals(other$ip)) {
            return false;
        }
        Object this$result = getResult();Object other$result = other.getResult();
        if (this$result == null ? other$result != null : !this$result.equals(other$result)) {
            return false;
        }
        Object this$createtime = getCreatetime();Object other$createtime = other.getCreatetime();return this$createtime == null ? other$createtime == null : this$createtime.equals(other$createtime);
    }

    public String getId()
    {
        return this.id;
    }

    public String getUseModule()
    {
        return this.useModule;
    }

    public String getUseMenu()
    {
        return this.useMenu;
    }

    public String getOperater()
    {
        return this.operater;
    }

    public String getContentInfo()
    {
        return this.contentInfo;
    }

    public String getOperaterUserid()
    {
        return this.operaterUserid;
    }

    public String getOperaterUsername()
    {
        return this.operaterUsername;
    }

    public String getIp()
    {
        return this.ip;
    }

    public String getResult()
    {
        return this.result;
    }

    public Date getCreatetime()
    {
        return this.createtime;
    }

    public Integer getStartIndex()
    {
        return this.startIndex;
    }

    public Integer getPageSize()
    {
        return this.pageSize;
    }
}
