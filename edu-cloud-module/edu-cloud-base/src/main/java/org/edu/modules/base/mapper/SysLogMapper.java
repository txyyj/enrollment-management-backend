package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.edu.modules.base.entity.SysLog;

public abstract interface SysLogMapper
        extends BaseMapper<SysLog>
{
    public abstract int getSysLogAllList(SysLog paramSysLog);

    public abstract List<SysLog> getSysLogPageList(SysLog paramSysLog);
}
