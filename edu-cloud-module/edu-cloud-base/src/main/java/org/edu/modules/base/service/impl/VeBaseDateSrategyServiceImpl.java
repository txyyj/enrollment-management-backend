package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseDateSrategy;
import org.edu.modules.base.mapper.VeBaseDateSrategyMapper;
import org.edu.modules.base.service.IVeBaseDateSrategyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseDateSrategyServiceImpl
        extends ServiceImpl<VeBaseDateSrategyMapper, VeBaseDateSrategy>
        implements IVeBaseDateSrategyService
{
    @Autowired
    private VeBaseDateSrategyMapper veBaseDateSrategyMapper;

    public int getDateSrategyAllList(String isEnable, String fulldate)
    {
        return this.veBaseDateSrategyMapper.getDateSrategyAllList(isEnable, fulldate);
    }

    public List<Map<String, Object>> getDateSrategyPageList(String isEnable, String fulldate, Integer startIndex, Integer pageSize)
    {
        List<Map<String, Object>> list = this.veBaseDateSrategyMapper.getDateSrategyPageList(isEnable, fulldate, startIndex, pageSize);
        if (list.size() > 0) {
            for (Map map : list)
            {
                List<Map<String, Object>> mapList = this.veBaseDateSrategyMapper.getDateSrategyServer(map.get("id").toString());
                map.put("arrays", mapList);

                String id = "";
                String name = "";
                StringBuilder stringBuilder = new StringBuilder();
                StringBuilder stringBuilder1 = new StringBuilder();
                if (mapList.size() > 0)
                {
                    for (Map detail : mapList)
                    {
                        stringBuilder.append(detail.get("serid").toString()).append(",");
                        if ((!"".equals(detail.get("roleName"))) && (detail.get("roleName") != null)) {
                            stringBuilder1.append(detail.get("roleName").toString()).append(",");
                        }
                    }
                    if ((!"".equals(stringBuilder.toString())) && (stringBuilder.toString() != null)) {
                        id = stringBuilder.substring(0, stringBuilder.lastIndexOf(","));
                    }
                    if ((!"".equals(stringBuilder1.toString())) && (stringBuilder1.toString() != null)) {
                        name = stringBuilder1.substring(0, stringBuilder1.lastIndexOf(","));
                    }
                }
                map.put("appUserGroupId", id);
                map.put("appUserGroupName", name);
            }
        }
        return list;
    }

    public int removeDateSrategyById(String id)
    {
        return this.veBaseDateSrategyMapper.removeDateStrategyById(id);
    }

    public int addSrategyServer(String id, String[] groupId)
    {
        return this.veBaseDateSrategyMapper.addSrategyServer(id, groupId);
    }

    public int deleteSrategyServer(String id)
    {
        return this.veBaseDateSrategyMapper.deleteSrategyServer(id);
    }
}
