package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_dictionary")
@ApiModel(value="ve_base_dictionary对象", description="数据字典管理")
public class VeDictionary
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private int id;
    @Excel(name="字典名称", width=15.0D)
    @ApiModelProperty("字典名称")
    private String title;
    @Excel(name="字典编码", width=15.0D)
    @ApiModelProperty("字典编码")
    private String code;
    @Excel(name="排序", width=15.0D)
    @ApiModelProperty("排序")
    private int listSort;
    @Excel(name="父id", width=15.0D)
    @ApiModelProperty("父id")
    private int pid;
    @Excel(name="终端Id", width=15.0D)
    @ApiModelProperty("终端Id")
    private int terminalId;
    @TableField(exist=false)
    List<VeDictionary> veDictionaryList;
    @TableField(exist=false)
    private String interfaceUserId;

    public VeDictionary setCode(String code)
    {
        this.code = code;return this;
    }

    public VeDictionary setTitle(String title)
    {
        this.title = title;return this;
    }

    public VeDictionary setId(int id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeDictionary(id=" + getId() + ", title=" + getTitle() + ", code=" + getCode() + ", listSort=" + getListSort() + ", pid=" + getPid() + ", terminalId=" + getTerminalId() + ", veDictionaryList=" + getVeDictionaryList() + ", interfaceUserId=" + getInterfaceUserId() + ")";
    }

    public VeDictionary setInterfaceUserId(String interfaceUserId)
    {
        this.interfaceUserId = interfaceUserId;return this;
    }

    public VeDictionary setVeDictionaryList(List<VeDictionary> veDictionaryList)
    {
        this.veDictionaryList = veDictionaryList;return this;
    }

    public VeDictionary setTerminalId(int terminalId)
    {
        this.terminalId = terminalId;return this;
    }

    public VeDictionary setPid(int pid)
    {
        this.pid = pid;return this;
    }

    public VeDictionary setListSort(int listSort)
    {
        this.listSort = listSort;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;result = result * 59 + getId();result = result * 59 + getListSort();result = result * 59 + getPid();result = result * 59 + getTerminalId();Object $title = getTitle();result = result * 59 + ($title == null ? 43 : $title.hashCode());Object $code = getCode();result = result * 59 + ($code == null ? 43 : $code.hashCode());Object $veDictionaryList = getVeDictionaryList();result = result * 59 + ($veDictionaryList == null ? 43 : $veDictionaryList.hashCode());Object $interfaceUserId = getInterfaceUserId();result = result * 59 + ($interfaceUserId == null ? 43 : $interfaceUserId.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeDictionary;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeDictionary)) {
            return false;
        }
        VeDictionary other = (VeDictionary)o;
        if (!other.canEqual(this)) {
            return false;
        }
        if (getId() != other.getId()) {
            return false;
        }
        if (getListSort() != other.getListSort()) {
            return false;
        }
        if (getPid() != other.getPid()) {
            return false;
        }
        if (getTerminalId() != other.getTerminalId()) {
            return false;
        }
        Object this$title = getTitle();Object other$title = other.getTitle();
        if (this$title == null ? other$title != null : !this$title.equals(other$title)) {
            return false;
        }
        Object this$code = getCode();Object other$code = other.getCode();
        if (this$code == null ? other$code != null : !this$code.equals(other$code)) {
            return false;
        }
        Object this$veDictionaryList = getVeDictionaryList();Object other$veDictionaryList = other.getVeDictionaryList();
        if (this$veDictionaryList == null ? other$veDictionaryList != null : !this$veDictionaryList.equals(other$veDictionaryList)) {
            return false;
        }
        Object this$interfaceUserId = getInterfaceUserId();Object other$interfaceUserId = other.getInterfaceUserId();return this$interfaceUserId == null ? other$interfaceUserId == null : this$interfaceUserId.equals(other$interfaceUserId);
    }

    public int getId()
    {
        return this.id;
    }

    public String getTitle()
    {
        return this.title;
    }

    public String getCode()
    {
        return this.code;
    }

    public int getListSort()
    {
        return this.listSort;
    }

    public int getPid()
    {
        return this.pid;
    }

    public int getTerminalId()
    {
        return this.terminalId;
    }

    public List<VeDictionary> getVeDictionaryList()
    {
        return this.veDictionaryList;
    }

    public String getInterfaceUserId()
    {
        return this.interfaceUserId;
    }
}
