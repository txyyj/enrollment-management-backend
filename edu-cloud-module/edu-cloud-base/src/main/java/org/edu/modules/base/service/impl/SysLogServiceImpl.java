package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import org.edu.modules.base.entity.SysLog;
import org.edu.modules.base.mapper.SysLogMapper;
import org.edu.modules.base.service.ISysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SysLogServiceImpl
        extends ServiceImpl<SysLogMapper, SysLog>
        implements ISysLogService
{
    @Autowired
    private SysLogMapper sysLogMapper;

    public int getSysLogAllList(SysLog sysLog)
    {
        return this.sysLogMapper.getSysLogAllList(sysLog);
    }

    public List<SysLog> getSysLogPageList(SysLog sysLog)
    {
        return this.sysLogMapper.getSysLogPageList(sysLog);
    }
}
