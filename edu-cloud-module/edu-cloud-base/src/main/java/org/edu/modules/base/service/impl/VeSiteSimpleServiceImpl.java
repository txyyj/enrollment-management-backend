package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.base.entity.VeSiteSimple;
import org.edu.modules.base.mapper.VeSiteSimpleMapper;
import org.edu.modules.base.service.IVeSiteSimpleService;
import org.springframework.stereotype.Service;

@Service
public class VeSiteSimpleServiceImpl
        extends ServiceImpl<VeSiteSimpleMapper, VeSiteSimple>
        implements IVeSiteSimpleService
{}
