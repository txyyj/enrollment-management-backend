package org.edu.cloud.demo.test.service;

import org.edu.common.api.vo.Result;

public interface JeecgDemoService {
    Result<String> getMessage(String name);
}
