
package org.edu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@EnableFeignClients(basePackages = {"org.edu"})
@SpringBootApplication(scanBasePackages = "org.edu")
public class EduCloudDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(EduCloudDemoApplication.class, args);
    }
}
