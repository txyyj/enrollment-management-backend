package org.edu.config.oss;

import org.edu.common.util.oss.OssBootUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 云存储 配置
 */
@Configuration
public class OssConfiguration {

    @Value("${edu.oss.endpoint}")
    private String endpoint;
    @Value("${edu.oss.accessKey}")
    private String accessKeyId;
    @Value("${edu.oss.secretKey}")
    private String accessKeySecret;
    @Value("${edu.oss.bucketName}")
    private String bucketName;
    @Value("${edu.oss.staticDomain}")
    private String staticDomain;


    @Bean
    public void initOssBootConfiguration() {
        OssBootUtil.setEndPoint(endpoint);
        OssBootUtil.setAccessKeyId(accessKeyId);
        OssBootUtil.setAccessKeySecret(accessKeySecret);
        OssBootUtil.setBucketName(bucketName);
        OssBootUtil.setStaticDomain(staticDomain);
    }
}
