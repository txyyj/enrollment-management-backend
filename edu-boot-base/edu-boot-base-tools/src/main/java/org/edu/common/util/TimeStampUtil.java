package org.edu.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class TimeStampUtil {

    /**
     * 获取当前精确到秒的时间戳
     */
    public static int getNowSecondStamp(){
        Date date = new Date();
        String timestamp = String.valueOf(date.getTime()/1000);
        return Integer.valueOf(timestamp);
    }
    /*
     * 获取当前时间戳
     */
    public static long getNowStamp(){
        Date date = new Date();
        long ts = date.getTime();
        return ts;
    }
      /*
     * 将精确到秒的时间戳转换为时间
     */
    public static String secondStampToDate(int st) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long lt = st * 1000;
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }
    /*
     * 将时间戳转换为时间
     */
    public static String stampToDate(long lt) {
        String res;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(lt);
        res = simpleDateFormat.format(date);
        return res;
    }
}
