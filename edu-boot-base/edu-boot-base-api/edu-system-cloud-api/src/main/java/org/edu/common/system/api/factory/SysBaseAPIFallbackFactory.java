package org.edu.common.system.api.factory;

import feign.hystrix.FallbackFactory;
import org.edu.common.system.api.fallback.SysBaseAPIFallback;
import org.edu.common.system.api.ISysBaseAPI;
import org.springframework.stereotype.Component;

@Component
public class SysBaseAPIFallbackFactory implements FallbackFactory<ISysBaseAPI> {

    @Override
    public ISysBaseAPI create(Throwable throwable) {
        SysBaseAPIFallback fallback = new SysBaseAPIFallback();
        fallback.setCause(throwable);
        return fallback;
    }
}
