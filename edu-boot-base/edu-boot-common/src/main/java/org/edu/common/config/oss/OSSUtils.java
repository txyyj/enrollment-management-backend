package org.edu.common.config.oss;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.ObjectMetadata;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @Author:Jin_long
 * @Date:2021/1/27 14:53
 * @explain: oss 2.0
 */
@Slf4j
@Component
public class OSSUtils {
    //OSS 的地址
    private final static String OSS_END_POINT = "oss-cn-beijing.aliyuncs.com";
    //OSS 的key值
    private final static String OSS_ACCESS_KEY_ID = "LTAI4FyHP5mu6YS2yzSxSfyQ";
    //OSS 的secret值
    private final static String OSS_ACCESS_KEY_SECRET = "Orvkzsrufwnnugsb7EsqqhHDg3lKku";
    //OSS 的bucket名字
    private final static String OSS_BUCKET_NAME = "yuzhiquan";
    //设置URL过期时间为10年
    private final static Date OSS_URL_EXPIRATION = DateUtils.addDays(new Date(), 365 * 10);

    /**
     * 下载成文件
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String upload(MultipartFile file) throws IOException {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String endpoint = OSS_END_POINT;
        String accessKeyId = OSS_ACCESS_KEY_ID;
        String accessKeySecret = OSS_ACCESS_KEY_SECRET;
        String bucketName = OSS_BUCKET_NAME;
        long size = file.getSize();
        System.out.println("文件大小"+size);

        // 获取文件的后缀名
        String suffixName = getExtension(file);
        // 生成上传文件名
        String finalFileName = System.currentTimeMillis() + "" + new SecureRandom().nextInt(0x0400) + suffixName;
        String objectName = "file" + "/" + sdf.format(new Date()) + "/" + finalFileName;


        ObjectMetadata meta = new ObjectMetadata();
        meta.setContentDisposition("attachment;");
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        InputStream inputStream = file.getInputStream();
        ossClient.putObject(bucketName, objectName, inputStream, meta);
        // 生成以GET方法访问的签名URL，访客可以直接通过浏览器访问相关内容。
        URL url = ossClient.generatePresignedUrl(bucketName, objectName, OSS_URL_EXPIRATION);
        ossClient.shutdown();
        System.out.println(url.toString());
        return url.toString();
    }

    /**
     * 页面浏览
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String ossUrl(MultipartFile file) throws Exception {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
        String endpoint = OSS_END_POINT;
        String accessKeyId = OSS_ACCESS_KEY_ID;
        String accessKeySecret = OSS_ACCESS_KEY_SECRET;
        String bucketName = OSS_BUCKET_NAME;
        // 获取文件的后缀名
        String suffixName = getExtension(file);
        // 生成上传文件名
        String finalFileName = System.currentTimeMillis() + "" + new SecureRandom().nextInt(0x0400) + suffixName;
        String objectName = "file" + "/" + sdf.format(new Date()) + "/" + finalFileName;


        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        // MultipartFile 转换为File
        File file1 = MultipartFileToFile.multipartFileToFile(file);
        ossClient.putObject(bucketName, objectName, file1);
        // 上传完成后删除本地文件
        MultipartFileToFile.delteTempFile(file1);
        // 生成以GET方法访问的签名URL，访客可以直接通过浏览器访问相关内容。
        URL url = ossClient.generatePresignedUrl(bucketName, objectName, OSS_URL_EXPIRATION);
        ossClient.shutdown();
        System.out.println(url.toString());
        long size = file.getSize();
        System.out.println("文件大小"+size);
        return url.toString();
    }


    /**
     * 获取文件名的后缀
     *
     * @param file 表单文件
     * @return 后缀名
     */
    public static final String getExtension(MultipartFile file) {
        String extension = FilenameUtils.getExtension(file.getOriginalFilename());
        if (StringUtilsip.isEmpty(extension)) {
            extension = MimeTypeUtils.getExtension(file.getContentType());
        }
        return extension;
    }


}
