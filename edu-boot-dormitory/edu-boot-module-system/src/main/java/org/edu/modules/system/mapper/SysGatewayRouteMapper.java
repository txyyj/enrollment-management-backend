package org.edu.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.system.entity.SysGatewayRoute;

/**
 * @Description: gateway路由管理
 * @Author: jeecg-boot
 * @Date:   2020-05-26
 * @Version: V1.0
 */
public interface SysGatewayRouteMapper extends BaseMapper<SysGatewayRoute> {

}
