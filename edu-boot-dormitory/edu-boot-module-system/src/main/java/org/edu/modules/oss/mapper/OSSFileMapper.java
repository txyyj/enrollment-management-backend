package org.edu.modules.oss.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.oss.entity.OSSFile;

public interface OSSFileMapper extends BaseMapper<OSSFile> {

}
