package org.edu.modules.system.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.system.entity.SysTenant;

public interface SysTenantMapper extends BaseMapper<SysTenant> {

}
