package org.edu.modules.message.service.impl;

import org.edu.modules.message.entity.SysMessage;
import org.edu.modules.message.mapper.SysMessageMapper;
import org.edu.modules.message.service.ISysMessageService;
import org.edu.common.system.base.service.impl.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * @Description: 消息
 * @Author: jeecg-boot
 * @Date:  2019-04-09
 * @Version: V1.0
 */
@Service
public class SysMessageServiceImpl extends BaseServiceImpl<SysMessageMapper, SysMessage> implements ISysMessageService {

}
