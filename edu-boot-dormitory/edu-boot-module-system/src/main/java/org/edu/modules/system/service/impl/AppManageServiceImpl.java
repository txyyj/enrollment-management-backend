package org.edu.modules.system.service.impl;

import com.baomidou.mybatisplus.extension.service.IService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.edu.modules.system.entity.AppManage;
import org.edu.modules.system.mapper.AppManageMapper;
import org.edu.modules.system.service.IAppManageService;
import org.springframework.stereotype.Service;

/**
 * 应用信息表实现类
 * @Author lbh
 * @Date 2022/1/13 11:57
 * @Version 1.0
 */
@Slf4j
@Service
public class AppManageServiceImpl extends ServiceImpl<AppManageMapper, AppManage> implements IAppManageService {
}
