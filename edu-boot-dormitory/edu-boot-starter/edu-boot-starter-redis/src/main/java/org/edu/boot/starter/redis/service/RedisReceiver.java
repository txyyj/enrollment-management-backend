package org.edu.boot.starter.redis.service;


import cn.hutool.core.util.ObjectUtil;
import lombok.Data;
import org.edu.boot.starter.redis.listener.JeecgRedisListerer;
import org.edu.common.base.BaseMap;
import org.edu.common.constant.GlobalConstants;
import org.edu.common.util.SpringContextHolder;
import org.springframework.stereotype.Component;

@Component
@Data
public class RedisReceiver {


    /**
     * 接受消息并调用业务逻辑处理器
     *
     * @param params
     */
    public void onMessage(BaseMap params) {
        Object handlerName = params.get(GlobalConstants.HANDLER_NAME);
        JeecgRedisListerer messageListener = SpringContextHolder.getHandler(handlerName.toString(), JeecgRedisListerer.class);
        if (ObjectUtil.isNotEmpty(messageListener)) {
            messageListener.onMessage(params);
        }
    }

}
