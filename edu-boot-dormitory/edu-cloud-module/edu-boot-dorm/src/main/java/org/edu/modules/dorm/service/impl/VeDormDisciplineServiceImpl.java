package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormWeiJi;
import org.edu.modules.dorm.mapper.VeDormDisciplineMapper;
import org.edu.modules.dorm.service.VeDormDisciplineService;
import org.springframework.stereotype.Service;

@Service
public class VeDormDisciplineServiceImpl extends ServiceImpl<VeDormDisciplineMapper, VeDormWeiJi> implements VeDormDisciplineService {

}
