package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormWeiXiu;
import org.edu.modules.dorm.vo.VeDormWeiXiuVo;

import java.util.List;

public interface VeDormWeiXiuMapper extends BaseMapper<VeDormWeiXiu>{

     List<VeDormWeiXiuVo> weiXiuInfo(@Param("ssId") Integer ssId,@Param("title") String title);

     List<VeDormWeiXiuVo> weiXiuInfoTable(@Param("title") String title);

}
