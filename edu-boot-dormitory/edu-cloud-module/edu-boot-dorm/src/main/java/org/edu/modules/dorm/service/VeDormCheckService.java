package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormCheck;
import org.edu.modules.dorm.vo.VeDormCheckVo;

import java.util.List;

public interface VeDormCheckService extends IService<VeDormCheck> {

    /**
     * 1、考勤列表。（通过姓名、学号模糊查询，宿舍楼编号精准查询）
     * @param XM 姓名
     * @param XH 学号
     * @param SSLBM 宿舍楼编号
     * */
    List<VeDormCheckVo> selectList(String XM, String XH, Long SSLBM);

    /**
     * 2、通过id查询。（违纪管理-通用）
     * @param id 考勤id
     */
    VeDormCheckVo selectById(Long id);

}
