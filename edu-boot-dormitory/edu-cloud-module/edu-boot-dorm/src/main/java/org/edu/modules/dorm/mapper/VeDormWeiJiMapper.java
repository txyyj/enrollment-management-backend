package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormWeiJi;
import org.edu.modules.dorm.vo.VeDormWeiJiVo;

import java.util.List;

public interface VeDormWeiJiMapper extends BaseMapper<VeDormWeiJi> {

    Integer updateDiscipline(VeDormWeiJi veDormWeiJi);

    List<VeDormWeiJiVo> weiJiTable(@Param("name") String name, @Param("xh") String xh);

}
