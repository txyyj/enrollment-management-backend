package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormStudent;

public interface VeDormStudentService extends IService<VeDormStudent> {

}
