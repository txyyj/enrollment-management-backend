package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 查找学生
 * @Author:
 * @Date:   2021-03-02
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeFindStudentVo implements Serializable {

    /**Id*/
    @TableId(value = "id")
    @ApiModelProperty(value = "id")
    private Long id;

    /**name*/
    @ApiModelProperty(value = "name")
    private String name;

}
