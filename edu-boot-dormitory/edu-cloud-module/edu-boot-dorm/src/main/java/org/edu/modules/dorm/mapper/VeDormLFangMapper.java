package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormLaiFang;
import org.edu.modules.dorm.vo.VeDormLaiFangVo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VeDormLFangMapper extends BaseMapper<VeDormLaiFang> {

  List<VeDormLaiFangVo> findLaiFang(@Param("XM")String XM,
                                    @Param("SFZH")String SFZH);

}
