package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 违纪类型管理
 * @Author:
 * @Date:   2021-03-09
 * @Version: V1.0
 */

@Data
@TableName("ve_dorm_discipline_type")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormDisciplineType对象", description = "违纪类型管理")
public class VeDormDisciplineType implements Serializable {

    /** （1）自增id。 */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "自增id")
    private Long id;

    /** （2）违纪名称。 */
    @Excel(name = "违纪名称", width = 15)
    @ApiModelProperty(value = "违纪名称")
    @TableField(value = "name")
    private String name;

    /** （3）违纪状态。（1=启用，0=禁用） */
    @Excel(name = "记录人用户id", width = 15)
    @ApiModelProperty(value = "违纪状态（1=启用，0=禁用）")
    @TableField(value = "status")
    private Integer status;

    /** （4）终端id。 */
    @Excel(name = "终端id", width = 15)
    @ApiModelProperty(value = "终端id")
    @TableField(value = "terminalId")
    private Long terminalId;

}
