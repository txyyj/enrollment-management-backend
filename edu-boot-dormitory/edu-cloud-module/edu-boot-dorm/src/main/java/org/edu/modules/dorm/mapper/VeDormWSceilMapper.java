package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormWsceil;
import org.edu.modules.dorm.vo.VeDormWsceilVo;

import java.util.List;

public interface VeDormWSceilMapper extends BaseMapper<VeDormWsceil> {

    List<VeDormWsceilVo> queryWsceil(@Param("campusId") Long campusId,
                                     @Param("jzwId") Long jzwId,
                                     @Param("lcId") Long lcId,
                                     @Param("ssId") Long ssId,
                                     @Param("semId") Long semId,
                                     @Param("weekNum") Long weekNum);

    List<VeDormWsceilVo> allWsceil(@Param("ssId") Long ssId);

    Integer checkWsceil(@Param("semId")Long semId,@Param("weekNum")Integer weekNum,@Param("ssId")Long ssId);

    //查询导入数据是否有重复
    List<VeDormWsceil> Wsceil(@Param("semId") Long semId,@Param("campusId") Long campusId,@Param("jzwId") Long jzwId,
                              @Param("lcId") Long lcId,@Param("ssId") Long ssId,@Param("month") Long month,
                              @Param("weekNum") Long weekNum,@Param("week") Long week);

}
