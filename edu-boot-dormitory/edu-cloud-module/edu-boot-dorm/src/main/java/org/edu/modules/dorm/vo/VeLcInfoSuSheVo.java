package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeLcInfoSuSheVo implements Serializable {

    @ApiModelProperty(value = "楼层")
    private String lc;

    @ApiModelProperty(value = "总寝室数")
    private Integer allSushe;

    @ApiModelProperty(value = "总床位数")
    private Integer sleepNum;

    @ApiModelProperty(value = "已住人数")
    private Integer inNum;

    @ApiModelProperty(value = "剩余穿位数")
    private Integer noInNum;

    @ApiModelProperty(value = "宿舍信息")
    private List<VeDormssfpVo> suSheList ;

}
