package org.edu.modules.dorm.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.vo.LoginUser;
import org.edu.modules.dorm.entity.*;
import org.edu.modules.dorm.entity.bo.BasicResponseBO;
import org.edu.modules.dorm.service.VeBaseDormManageService;
import org.edu.modules.dorm.service.VeDormLodgingAuditService;
import org.edu.modules.dorm.vo.VeDormLodgingAuditExcelVo;
import org.edu.modules.dorm.vo.VeDormLodgingAuditVo;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@Api(tags="留宿审核")
@RestController
@RequestMapping("dorm/change")
@Slf4j
public class VeDormLodgingAuditController{
    @Autowired
    private VeDormLodgingAuditService veDormLodgingAuditService;
    @Autowired
    private VeBaseDormManageService veBaseDormManageService; //李少君

    @AutoLog(value = "留宿审核列表")
    @ApiOperation(value="留宿审核列表", notes="留宿审核列表-留宿审核")
    @PostMapping(value = "/lodgingAuditTable")
    public Result<?> lodgingAuditTable(
      @ApiParam("姓名") @RequestParam(value = "xm",required = false) String xm,
       @ApiParam(value = "类型")  @RequestParam(value = "type",required = false) Integer type){
//        BasicResponseBO<List> list1 = veBaseDormManageService.getFacultyMessage();
//        List<VeBaseFaculty> result1 = JSONObject.parseArray(JSON.toJSONString(list1.getResult()), VeBaseFaculty.class);//院系
//        BasicResponseBO<List> list2 = veBaseDormManageService.getBanjiMessage();
//        List<VeBaseBanji> result2 = JSONObject.parseArray(JSON.toJSONString(list2.getResult()), VeBaseBanji.class);//班级
//        BasicResponseBO<List> list3 = veBaseDormManageService.getStudentMessage();
//        List<VeDormStudent> result3 = JSONObject.parseArray(JSON.toJSONString(list3.getResult()), VeDormStudent.class);//学生
//
//        List<VeDormLodgingAuditVo> v = veDormLodgingAuditService.LodgingAuditTable(xm,type);
//        List<VeBaseSpecialty> zy = new ArrayList<>();
//        for(int i =0 ;i<v.size();i++){
//            for (int j =0 ;j<result3.size();j++){
//                if(v.get(i).getUserId()==(result3.get(j).getId().longValue())){
//                    v.get(i).setFalId(result3.get(j).getFalId());
//                    v.get(i).setSpecId(result3.get(i).getSpecId());
//                    v.get(i).setBanjiId(result3.get(i).getBjId());
//
//                    VeBaseSpecialty vs = new VeBaseSpecialty();
//                    BasicResponseBO<List> list4 = veBaseDormManageService.getSpecialtyMessageById(result3.get(i).getSpecId());
//                    List<VeBaseSpecialty> result4 = JSONObject.parseArray(JSON.toJSONString(list4.getResult()), VeBaseSpecialty.class);
//                    vs.setZymc(result4.get(i).getZymc());
//                    zy.add(vs);
//                }
//            }
//        }
//        for (int i =0;i<v.size();i++){
//            for (int j =0 ;j<result1.size();j++){
//                if(v.get(i).getFalId()==(result1.get(j).getId().longValue())){
//                    v.get(i).setYxmc(result1.get(i).getYxmc());
//                }
//            }
//        }
//        for (int i =0;i<v.size();i++){
//            for (int j =0 ;j<result2.size();j++){
//                if(v.get(i).getBanjiId()==(result2.get(j).getId().longValue())){
//                    v.get(i).setYxmc(result2.get(i).getXzbmc());
//                }
//            }
//        }

//        BasicResponseBO<List> list4 = veBaseDormManageService.getSpecialtyMessageById();
//        List<VeBaseSpecialty> result4 = JSONObject.parseArray(JSON.toJSONString(list4.getResult()), VeBaseSpecialty.class);





//        return Result.OK(v);
        return Result.OK(veDormLodgingAuditService.LodgingAuditTable(xm,type));
    }

    @AutoLog(value = "审核")
    @ApiOperation(value="审核", notes="审核-留宿审核")
    @PostMapping(value = "/audit")
    public Result<?> audit(
       @ApiParam("id")      @RequestParam("id") Integer id,
       @ApiParam("审核状态") @RequestParam("auditStatus") Integer status,
       @ApiParam("审核原因") @RequestParam(value = "auditRemark",required = false) String auditRemark
    ){

           if (auditRemark!=null){
               veDormLodgingAuditService.LodgingAudit(status,id,auditRemark);
           }

           else {
               veDormLodgingAuditService.check(status,id);
           }
      return Result.OK("审核成功");
    }
    @ApiOperation(value="根据id查询", notes="根据id查询-留宿审核")
    @PostMapping(value = "/selectById")
    public Result<?> selectById(
            @ApiParam("id") @RequestParam("id") Integer id){
        return Result.OK(veDormLodgingAuditService.selectById(id));
    }
    @AutoLog(value = "留宿审核导出excel表格")
    @ApiOperation(value = "留宿审核导出excel表格", notes = "留宿审核")
    @RequestMapping(value = "export")
    public ModelAndView excelExport( @ApiParam("姓名") @RequestParam(value = "xm",required = false) String xm,
                                     @ApiParam(value = "类型")  @RequestParam(value = "type",required = false) Integer type) {
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        List<VeDormLodgingAuditVo> list = veDormLodgingAuditService.LodgingAuditTable(xm, type);
        List<VeDormLodgingAuditExcelVo> excelVoList=new ArrayList<>();

        for (int i = 0; i < list.size(); i++) {
            VeDormLodgingAuditExcelVo excelVo=new VeDormLodgingAuditExcelVo();
            excelVo.setId(list.get(i).getId());
            excelVo.setXm(list.get(i).getXm());
            excelVo.setXqmc(list.get(i).getXqmc());
            excelVo.setJzwmc(list.get(i).getJzwmc());
            excelVo.setNjmc(list.get(i).getNjmc());
            excelVo.setYxmc(list.get(i).getZymc());
            excelVo.setZymc(list.get(i).getZymc());
            excelVo.setXzbmc(list.get(i).getXzbmc());
            Integer types =list.get(i).getType();

            if (types==0){
                excelVo.setType("暑假留宿");
            } else if (types==1){
                excelVo.setType("寒假留宿");
            }

            if (list.get(i).getAuditStatus()==0){
                excelVo.setAuditStatus("不通过");
            }
            else if (list.get(i).getAuditStatus()==1){
                excelVo.setAuditStatus("通过");
            }
            excelVo.setStartTime(list.get(i).getStartTime());
            excelVo.setEndTime(list.get(i).getEndTime());
            excelVo.setAudittime(list.get(i).getAudittime());
            excelVo.setFjbm(list.get(i).getFjbm());
            excelVoList.add(excelVo);
        }
        mv.addObject(NormalExcelConstants.FILE_NAME, "留宿审核列表");
        mv.addObject(NormalExcelConstants.CLASS, VeDormLodgingAuditExcelVo .class);
        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ExportParams exportParams = new ExportParams("留宿审核列表数据", "导出人:" + user.getRealname(), "导出信息");
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST,excelVoList);
        return mv;
    }


}
