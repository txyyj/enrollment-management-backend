package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 宿舍表
 * @Author:
 * @Date:   2021-03-05
 * @Version: V1.0
 */

@Data
@TableName("ve_dorm_sushe")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormSuShe implements Serializable {

      /**id*/
      @TableId(type = IdType.AUTO)
      @ApiModelProperty(value = "id")
      private Long id;

      /**校区号*/
      @Excel(name = "校区号", width = 15)
      @ApiModelProperty(value = "校区号")
      @TableField(value = "XQH")
      private Long XQH;

      /**宿舍楼编号*/
      @Excel(name = "宿舍楼编号", width = 15)
      @ApiModelProperty(value = "宿舍楼编号")
      @TableField(value = "JZWH")
      private Long JZWh;

      /**楼层号*/
      @Excel(name = "楼层号", width = 15)
      @ApiModelProperty(value = "楼层号")
      @TableField(value = "LCH")
      private Long LCH;

      /**房间编号*/
      @Excel(name = "房间编号", width = 15)
      @ApiModelProperty(value = "房间编号")
      @TableField(value = "FJBM")
      private String FJBM;

      /**入住性别*/
      @Excel(name = "入住性别", width = 15)
      @ApiModelProperty(value = "入住性别")
      @TableField(value = "RZXB")
      private Integer RZXB;

      /**是否可用*/
      @Excel(name = "是否可用", width = 15)
      @ApiModelProperty(value = "是否可用")
      @TableField(value = "SFKY")
      private Long SFKY;

      /**已住人数*/
      @Excel(name = "已住人数", width = 15)
      @ApiModelProperty(value = "已住人数")
      @TableField(value = "YZRS")
      private Long YZRS;

      /**可住人数*/
      @Excel(name = "可住人数", width = 15)
      @ApiModelProperty(value = "可住人数")
      @TableField(value = "KZRS")
      private Long KZRS;

      /**网络端口*/
      @Excel(name = "网络端口", width = 15)
      @ApiModelProperty(value = "网络端口")
      @TableField(value = "WLDK")
      private String WLDK;

      /**安装电视机*/
      @Excel(name = "安装电视机", width = 15)
      @ApiModelProperty(value = "安装电视机")
      @TableField(value = "AZDSJ")
      private String AZDSJ;

      /**电话端口*/
      @Excel(name = "电话端口", width = 15)
      @ApiModelProperty(value = "电话端口")
      @TableField(value = "DHDK")
      private String DHDK;

      /**电话号码*/
      @Excel(name = "电话号码", width = 15)
      @ApiModelProperty(value = "电话号码")
      @TableField(value = "DHHM")
      private String DHHM;

      /**是否有网络*/
      @Excel(name = "是否有网络", width = 15)
      @ApiModelProperty(value = "是否有网络")
      @TableField(value = "SFWL")
      private Integer SFWL;

      /**是否电视机*/
      @Excel(name = "是否有电视机", width = 15)
      @ApiModelProperty(value = "是否有电视机")
      @TableField(value = "SFDSJ")
      private Integer SFDSJ;

      /**水表底数*/
      @Excel(name = "水表底数", width = 15)
      @ApiModelProperty(value = "水表底数")
      @TableField(value = "SBDS")
      private Long SBDS;

      /**电表底数*/
      @Excel(name = "电表底数", width = 15)
      @ApiModelProperty(value = "电表底数")
      @TableField(value = "DBDS")
      private Long DBDS;

      /**宿舍备注*/
      @Excel(name = "宿舍备注", width = 15)
      @ApiModelProperty(value = "宿舍备注")
      @TableField(value = "SSBZ")
      private Long SSBZ;

      /**评优级别*/
      @Excel(name = "评优级别", width = 15)
      @ApiModelProperty(value = "评优级别")
      @TableField(value = "star")
      private Integer star;

      /**状态*/
      @Excel(name = "状态", width = 15)
      @ApiModelProperty(value = "状态")
      @TableField(value = "status")
      private Long status;

      /**系统Id*/
      @Excel(name = "系统Id", width = 15)
      @ApiModelProperty(value = "系统Id")
      @TableField(value = "terminalId")
      private Long terminalId;

}
