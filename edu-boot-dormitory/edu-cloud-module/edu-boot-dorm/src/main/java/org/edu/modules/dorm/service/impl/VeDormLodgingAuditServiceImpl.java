package org.edu.modules.dorm.service.impl;

import org.edu.modules.dorm.mapper.VeDormLodgingAuditMapper;
import org.edu.modules.dorm.service.VeDormLodgingAuditService;
import org.edu.modules.dorm.vo.VeDormLodgingAuditVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VeDormLodgingAuditServiceImpl implements VeDormLodgingAuditService {
    @Autowired
    private VeDormLodgingAuditMapper veDormLodgingAuditMapper;

    @Override
    public List<VeDormLodgingAuditVo> LodgingAuditTable(String xm, Integer type) {
        return veDormLodgingAuditMapper.LodgingAuditTable(xm,type);
    }

    @Override
    public Integer LodgingAudit(Integer status, Integer id,String auditRemark) {
        return veDormLodgingAuditMapper.LodgingAudit(status,id,auditRemark);
    }

    @Override
    public Integer check(Integer status, Integer id) {
        return veDormLodgingAuditMapper.check(status,id);
    }

    @Override
    public List<VeDormLodgingAuditVo> selectById(Integer id) {
        return veDormLodgingAuditMapper.selectById(id);
    }

}
