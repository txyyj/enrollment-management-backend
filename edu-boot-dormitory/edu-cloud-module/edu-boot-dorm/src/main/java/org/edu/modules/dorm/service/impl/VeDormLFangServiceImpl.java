package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormLaiFang;
import org.edu.modules.dorm.entity.VeDormStudent;
import org.edu.modules.dorm.mapper.VeDormDictionaryMapper;
import org.edu.modules.dorm.mapper.VeDormLFangMapper;
import org.edu.modules.dorm.mapper.VeDormStudentMapper;
import org.edu.modules.dorm.mapper.VeDormSuSheMapper;
import org.edu.modules.dorm.service.VeDormLFangService;
import org.edu.modules.dorm.vo.VeDormLaiFangVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VeDormLFangServiceImpl extends ServiceImpl<VeDormLFangMapper, VeDormLaiFang> implements VeDormLFangService {
   @Autowired
   private VeDormStudentMapper veDormStudentMapper;
   @Autowired
   private VeDormSuSheMapper veDormSuSheMapper;
   @Autowired
   private VeDormDictionaryMapper veDormDictionaryMapper;
   @Autowired
   private VeDormLFangMapper veDormLFangMapper;

    @Override
    public List<VeDormLaiFangVo> showLaiFang(String XM,String SFZH) {
      return veDormLFangMapper.findLaiFang(XM, SFZH);
    }

    @Override
    public VeDormLaiFangVo updateLaiFangMessage(Long id) {
        VeDormLaiFang veDormLaiFang = veDormLFangMapper.selectById(id);
        VeDormLaiFangVo veDormLaiFangVo = new VeDormLaiFangVo();
        VeDormStudent veDormStudent = veDormStudentMapper.selectById(veDormLaiFang.getUserId()) ;
        veDormLaiFangVo.setId(veDormLaiFang.getId());
        veDormLaiFangVo.setTFLX(veDormLaiFang.getTFLX());
        veDormLaiFangVo.setXQId(veDormStudent.getXQH());
        veDormLaiFangVo.setSSLId(veDormStudent.getSSLBM());
        veDormLaiFangVo.setSSId(veDormStudent.getFJBM());
        veDormLaiFangVo.setUserId(veDormLaiFang.getUserId());
        veDormLaiFangVo.setTFSY(veDormLaiFang.getTFSY());
        veDormLaiFangVo.setXM(veDormLaiFang.getXM());
        veDormLaiFangVo.setSFZH(veDormLaiFang.getSFZH());
        return veDormLaiFangVo;
    }

}
