package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormLouceng;
import org.edu.modules.dorm.entity.VeDormSSLou;
import org.edu.modules.dorm.mapper.VeDormLoucengMapper;
import org.edu.modules.dorm.service.VeDormLoucengService;
import org.edu.modules.dorm.service.VeDormSuSheLouService;
import org.edu.modules.dorm.service.VeFindTFStudentService;
import org.edu.modules.dorm.vo.VeDormLouCengVo;
import org.edu.modules.dorm.vo.VeDormLoucengExcelVO;
import org.edu.modules.dorm.vo.VeFindStudentVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class VeDormLoucengServiceImpl extends ServiceImpl<VeDormLoucengMapper, VeDormLouceng> implements VeDormLoucengService {
    @Autowired
    private VeDormLoucengMapper veDormLoucengMapper;

    @Override
    public Integer insertNew(VeDormLouceng veDormLouceng) {
        return veDormLoucengMapper.insert(veDormLouceng);
    }

    @Override
    public Integer updateLouCeng(VeDormLouceng veDormLouceng) {
        return veDormLoucengMapper.updateById(veDormLouceng);
    }

    @Override
    public Integer deleteLouceng(Integer id) {
        return veDormLoucengMapper.deleteById(id);
    }

    @Override
    public List<VeDormLouCengVo> selectByParam(Integer xqId, Integer sslId) {
        return veDormLoucengMapper.selectByParam(xqId,sslId);
    }

    @Autowired
    VeFindTFStudentService mVeFindTFStudentService ;
    @Autowired
    VeDormSuSheLouService mVeDormSuSheLouService ;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void excelImport(List<VeDormLoucengExcelVO> list) throws Exception {
        // 获取所有校区信息
        List<VeFindStudentVo> xiaoqu =  mVeFindTFStudentService.findXiaoQu() ;
        Map<String,Long> map = new HashMap<>() ;
        xiaoqu.forEach(l-> map.put(l.getName(),l.getId()));
        // 获取所有宿舍楼信息
        List<VeDormSSLou> sushelou = mVeDormSuSheLouService.list() ;
        Map<Long,List<VeDormSSLou>> groupXiaoqu = sushelou.stream().collect(Collectors.groupingBy(VeDormSSLou::getXqh)) ;
        Map<Long,Map<String,Integer>> sushelouMap = new HashMap<>() ;
        groupXiaoqu.forEach((k,v)->{
            Map<String,Integer> c = new HashMap<>() ;
            v.forEach(l-> c.put(l.getJzwmc(),l.getId()));
            sushelouMap.put(k,c) ;
        });
        // 字典
        Map<String,Integer> haveMap = new HashMap<>() ;
        haveMap.put("有",1) ;
        haveMap.put("无",0) ;
        int t = 4 ;
        try{
            for ( VeDormLoucengExcelVO ve : list) {
                VeDormLouceng lc = new VeDormLouceng() ;
                BeanUtils.copyProperties(ve,lc);
                // 获取校区信息
                Long xiaoquId = map.get(ve.getXqh()) ;
                // 宿舍楼
                Integer lou = sushelouMap.get(xiaoquId).get(ve.getJzwh()) ;
                lc.setXqh(Integer.parseInt(xiaoquId+"")).setJzwh(lou).setTerminalid(1);
                if(Objects.nonNull(ve.getSfsxt())){
                    lc.setSfsxt(haveMap.getOrDefault(ve.getSfsxt(),0)) ;
                }
                // 判断楼层是否已存在
                QueryWrapper<VeDormLouceng> wrapper=new QueryWrapper<>();
                wrapper.eq("jzwh",lc.getJzwh());
                wrapper.and(qw->qw.eq("lcname", lc.getLcname()).or().eq("lccode",lc.getLccode())) ;
                List<VeDormLouceng> loucengs =veDormLoucengMapper.selectList(wrapper);
                if (CollectionUtils.isEmpty(loucengs)){
                    veDormLoucengMapper.insert(lc) ;
                }
                t++ ;
            }
        }catch (Exception e){
            throw new Exception("第"+t+"行数据有误！") ;
        }
    }

    @Override
    public Long getLoucengId(Integer jzwh, String lcName) {
        return veDormLoucengMapper.getLoucengId(jzwh, lcName);
    }

}
