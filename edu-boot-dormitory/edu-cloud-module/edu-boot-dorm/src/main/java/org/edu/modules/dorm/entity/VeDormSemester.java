package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 学期表
 * @Author:
 * @Date:   2021-03-08
 * @Version: V1.0
 */

@Data
@TableName("ve_base_semester")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormSemester implements Serializable {

    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /**学期码*/
    @Excel(name = "学期码",width = 15)
    @ApiModelProperty(value = "学期码")
    @TableField(value = "XQM")
    private String XQM;

    /**学期名称*/
    @Excel(name = "学期名称",width = 15)
    @ApiModelProperty(value = "学期名称")
    @TableField(value = "XQMC")
    private String XQMC;

    /**学年*/
    @Excel(name = "学年",width = 15)
    @ApiModelProperty(value = "学年")
    @TableField(value = "XN")
    private String XN;

    /**学期开始日期*/
    @Excel(name = "学期开始日期",width = 15)
    @ApiModelProperty(value = "学期开始日期")
    @TableField(value = "XQKSRQ")
    private String XQKSRQ;

    /**学期结束日期*/
    @Excel(name = "学期结束日期",width = 15)
    @ApiModelProperty(value = "学期结束日期")
    @TableField(value = "XQJSRQ")
    private String XQJSRQ;

    /**是否当前学期*/
    @Excel(name = "是否当前学期",width = 15)
    @ApiModelProperty(value = "是否当前学期")
    @TableField(value = "isCurrent")
    private String isCurrent;

    /**终端id*/
    @Excel(name = "终端id",width = 15)
    @ApiModelProperty(value = "终端id")
    @TableField(value = "terminalId")
    private Long terminalId;

}
