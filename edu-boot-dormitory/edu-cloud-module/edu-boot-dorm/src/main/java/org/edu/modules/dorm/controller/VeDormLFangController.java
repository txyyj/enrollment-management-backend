package org.edu.modules.dorm.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.dorm.entity.VeDormDictionary;
import org.edu.modules.dorm.entity.VeDormLaiFang;
import org.edu.modules.dorm.entity.VeDormStudent;
import org.edu.modules.dorm.entity.bo.BasicResponseBO;
import org.edu.modules.dorm.entity.bo.VeCampusBO;
import org.edu.modules.dorm.service.*;
import org.edu.modules.dorm.vo.VeDormLaiFangVo;
import org.edu.modules.dorm.vo.VeDormOneQuery;
import org.edu.modules.dorm.vo.VeFindStudentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Api(tags="宿舍访客管理")
@RestController
@RequestMapping("dorm/laiFang")
@Slf4j
public class VeDormLFangController extends BaseController<VeDormLaiFang,VeDormLFangService> {
  @Autowired
  private VeDormLFangService veDormLaiFangService;
  @Autowired
  private VeFindTFStudentService veFindTFStudentService;
  @Autowired
  private VeDormStudentService veDormStudentService;
  @Autowired
  private VeDormDictionaryService veDormDictionaryService;
  @Autowired
  private VeBaseDormManageService veBaseDormManageService; //李少君

  /**
   * 来访申请
   * @param
   * @return
   */
  @AutoLog(value = "新增来访申请")
  @ApiOperation(value="新增来访申请", notes="站点选择-新增来访申请")
  @PostMapping(value = "/add")
  public Result<?> addLaiFang(@ApiParam(value ="姓名") @RequestParam("XM") String XM,
                              @ApiParam(value ="身份证号") @RequestParam(value = "SFZH",required = false) String SFZH,
                              @ApiParam(value ="用户id") @RequestParam("userId") Long userId,
                              @ApiParam(value ="探访事由") @RequestParam("TFSY") String TFSY,
                              @ApiParam(value ="探访类型") @RequestParam(value = "TFLX",required = false) Long TFLX,
                              @ApiParam(value ="房间编号") @RequestParam("FJBH") Long FJBH) {
    Long time = System.currentTimeMillis() / 1000;
    VeDormLaiFang veDormLaiFang = new VeDormLaiFang();
    veDormLaiFang.setXM(XM);
    veDormLaiFang.setSFZH(SFZH);
    veDormLaiFang.setUserId(userId);
    veDormLaiFang.setTFSY(TFSY);
    veDormLaiFang.setTFLX(TFLX);
    veDormLaiFang.setDJSJ(time);
    veDormLaiFang.setFJBH(FJBH);

    //默认为空
    veDormLaiFang.setDJR(0L);
    veDormLaiFang.setApplyTime(0L);
    veDormLaiFang.setLKSJ(0L);
    veDormLaiFang.setLKDJR(0L);

    veDormLaiFangService.save(veDormLaiFang);
    return Result.OK("新增来访成功！");
  }

  /**
   * 申请列表展示身份证或者姓名模糊查询
   * @param XM, SFZH
   */
  @ApiModelProperty(value = "申请列表展示（身份证，姓名模糊查询）", notes = "申请列表展示")
  @PostMapping(value = "/showLaiFang")
  public Result<?> showLaiFang(@ApiParam(value ="姓名") @RequestParam(value = "XM",required = false)String XM,
                               @ApiParam(value ="身份证号") @RequestParam(value = "SFZH",required = false) String SFZH) {

    List<VeDormLaiFangVo> listTwo = veDormLaiFangService.showLaiFang(XM, SFZH);
    return Result.OK(listTwo);
  }

  /**
   * 根据ID获取编辑内容
   */
  @ApiModelProperty(value = "根据Id获取编辑信息", notes = "站点选择-编辑来访申请")
  @PostMapping(value = "/updateMessage")
  public Result<?> updateMessageById(@ApiParam(value ="学生宿舍来访情况登记id") @RequestParam("id") Long id){
    VeDormLaiFangVo veDormLaiFangVo = veDormLaiFangService.updateLaiFangMessage(id);
    return  Result.OK(veDormLaiFangVo);
  }

  /**
   * 编辑申请
   */
  @AutoLog(value = "编辑来访申请")
  @ApiModelProperty(value = "编辑来访申请",notes = "站点选择-编辑来访申请")
  @PostMapping(value = "/update")
  public Result<?> updateLaiFang(@ApiParam(value ="姓名") @RequestParam("XM") String XM,
                                 @ApiParam(value ="学生宿舍来访情况登记id") @RequestParam("id") Long id,
                                 @ApiParam(value ="身份证号") @RequestParam("SFZH") String SFZH,
                                 @ApiParam(value ="用户id") @RequestParam("userId") Long userId,
                                 @ApiParam(value ="探访事由") @RequestParam("TFSY") String TFSY,
                                 @ApiParam(value ="探访类型") @RequestParam("TFLX") Long TFLX){
    VeDormLaiFang veDormLaiFang = new VeDormLaiFang();
    veDormLaiFang.setXM(XM);
    veDormLaiFang.setSFZH(SFZH);
    veDormLaiFang.setUserId(userId);
    veDormLaiFang.setTFSY(TFSY);
    veDormLaiFang.setTFLX(TFLX);
    veDormLaiFang.setId(id);

    veDormLaiFangService.updateById(veDormLaiFang);
    return Result.OK("编辑成功！");
  }

  /**
   * 删除申请
   * @param id
   */
  @AutoLog(value = "删除来访申请")
  @ApiModelProperty(value = "删除来访申请",notes = "站点选择-删除来访申请")
  @PostMapping(value = "/delete")
  public Result<?> deleteLaiFang(@ApiParam(value ="学生宿舍来访情况登记id") @RequestParam("id") Long id) {
    veDormLaiFangService.removeById(id);
    return Result.OK("删除成功！");
  }

  /**
   * 批量删除申请
   * @param ids
   */
  @AutoLog(value = "批量删除来访申请")
  @ApiModelProperty(value = "批量删除来访申请", notes = "站点选择-批量删除来访申请")
  @PostMapping(value = "/deleteBatch")
  public Result<?> deleteLaiFang(@ApiParam(value ="学生宿舍来访情况登记id") @RequestParam("ids") String ids) {
    veDormLaiFangService.removeByIds(Arrays.asList(ids.split(",")));
    return Result.OK("批量删除成功！");
  }

  /**
   * 离开
   * @param id
   */
  @AutoLog(value = "离开")
  @ApiModelProperty(value = "离开通过ID，离开登记人",notes = "站点选择-离开")
  @PostMapping(value = "/leave")
  public Result<?> leave(@ApiParam(value ="学生宿舍来访情况登记id") @RequestParam(name = "id") Long id,
                         @ApiParam(value ="离开登记人") @RequestParam(name = "LKDJR") Long LKDJR) {
    VeDormLaiFang veDormLaiFang = new VeDormLaiFang();
    veDormLaiFang.setId(id);
    veDormLaiFang.setLKSJ(new Date().getTime());
    veDormLaiFang.setLKDJR(LKDJR);

    veDormLaiFangService.updateById(veDormLaiFang);
    return Result.OK("离开登记成功！");
  }

  /**
   * 根据Id查询对应的消息
   * @param id
   */
  @ApiModelProperty(value = "通过Id，查看对应的消息进行编辑", notes = "站点选择-编辑")
  @PostMapping(value = "/queryById")
  public Result<?> leave(@ApiParam(value ="学生宿舍来访情况登记id") @RequestParam(name = "id") Long id) {
    VeDormLaiFang veDormLaiFang = veDormLaiFangService.getById(id);
    QueryWrapper<VeDormStudent> wrapper = new QueryWrapper<>();
    wrapper.eq("userId",veDormLaiFang.getUserId());

    VeDormStudent veDormStudent = veDormStudentService.getOne(wrapper);
    VeDormOneQuery veDormOneQuery = new VeDormOneQuery();
    veDormOneQuery.setId(id);
    veDormOneQuery.setXM(veDormLaiFang.getXM());
    veDormOneQuery.setSFZH(veDormLaiFang.getSFZH());
    veDormOneQuery.setTFSY(veDormLaiFang.getTFSY());
    veDormOneQuery.setTFLX(veDormLaiFang.getTFLX());
    veDormOneQuery.setXQBH(veDormStudent.getXQH());
    veDormOneQuery.setSSLBH(veDormStudent.getSSLBM());
    veDormOneQuery.setFJBM(veDormStudent.getFJBM());
    veDormOneQuery.setUserName(veDormStudent.getXM());
    return Result.OK(veDormOneQuery);
  }

  /**
   * 查找校区
   */
  @ApiOperation(value="查找校区", notes="")
  @PostMapping(value = "/xiaoQu")
  public Result<?> xiaoQu() {
    List<VeFindStudentVo> list = veFindTFStudentService.findXiaoQu();
//    BasicResponseBO<List> list = veBaseDormManageService.getCampusMessage(); //李少君
//    List<VeCampusBO> result = JSONObject.parseArray(JSON.toJSONString(list.getResult()),VeCampusBO.class);
//    List<VeFindStudentVo> list1 = new ArrayList<>();
//    for(int i =0 ;i<result.size();i++){
//      VeFindStudentVo c = new VeFindStudentVo();
//      c.setId(result.get(i).getId());
//      c.setName(result.get(i).getXqmc());
//      list1.add(c);
//    }

//    List<VeFindStudentVo> list= Constant.VUE_APP_API_BASE_URL+"";
    return Result.OK(list);
  }

  /**
   * 查找宿舍楼
   */
  @ApiOperation(value="查找宿舍楼", notes="校区ID")
  @PostMapping(value = "/suSheLou")
  public Result<?> suSheLou(@ApiParam(value ="校区编号") @RequestParam(name = "XQBH") Long XQBH) {
    List<VeFindStudentVo> list = veFindTFStudentService.findSuSheLou(XQBH);
    return Result.OK(list);
  }

  /**
   * 查找宿舍号
   */
  @ApiModelProperty(value = "查找宿舍", notes = "宿舍楼编号")
  @PostMapping(value = "/suShe")
  public Result<?> suShe(@ApiParam(value ="宿舍楼编号") @RequestParam(name = "SSLBH") Long SSLBH,
                         @ApiParam(value ="校区编号") @RequestParam(name = "XQBH") Long XQBH) {
    List<VeFindStudentVo> list = veFindTFStudentService.findSuSheHao(SSLBH, XQBH);
    return Result.OK(list);
  }

  /**
   * 查找学生
   */
  @ApiModelProperty(value = "查找学生", notes = "宿舍号")
  @PostMapping(value = "/student")
  public Result<?> student(@ApiParam(value ="宿舍编号") @RequestParam(name = "SSBH") Long SSBH,
                           @ApiParam(value ="宿舍楼编号") @RequestParam(name = "SSLBH") Long SSLBH,
                           @ApiParam(value ="校区编号") @RequestParam(name = "XQBH") Long XQBH) {
    List<VeFindStudentVo> list = veFindTFStudentService.fidStudent(SSBH,SSLBH,XQBH);
    return Result.OK(list);

  }

  /**
   * 查找类型
   */
  @ApiModelProperty(value = "查找类型", notes = "类型名称")
  @PostMapping(value = "/dictionaryType")
  public Result<?> student(@ApiParam(value ="标题") @RequestParam(name = "title") String title) {
    QueryWrapper<VeDormDictionary> wrapper = new QueryWrapper<>();
    wrapper.eq("title",title);
    wrapper.orderByDesc("id");

    List<VeDormDictionary> list = veDormDictionaryService.list(wrapper);
    return Result.OK(list);
  }

}
