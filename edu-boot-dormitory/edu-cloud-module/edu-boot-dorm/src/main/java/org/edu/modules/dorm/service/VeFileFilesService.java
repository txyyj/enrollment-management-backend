package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeFileFiles;

/**
 * @Auther 李少君
 * @Date 2021-08-12 17:17
 */
public interface VeFileFilesService extends IService<VeFileFiles> {
}
