package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormNotice;
import org.edu.modules.dorm.vo.VeDormNoticeVo;

import java.util.List;

public interface VeDormNoticeService extends IService<VeDormNotice> {

    List<VeDormNoticeVo> queryByName(String title);

}
