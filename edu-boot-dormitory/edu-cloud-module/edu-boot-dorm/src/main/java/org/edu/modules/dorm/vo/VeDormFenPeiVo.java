package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormFenPeiVo对象", description = "宿舍分配查询")
public class VeDormFenPeiVo implements Serializable {

    @ApiModelProperty(value = "宿舍楼Id")
    private Integer id;

    @ApiModelProperty(value = "建筑物名称")
    private String jzwmc;

    @ApiModelProperty(value = "建筑物层数")
    private Integer jzwcs;

    @ApiModelProperty(value = "房间编号")
    private String fjbh;

    @ApiModelProperty(value = "可住人数")
    private Integer kzrs;

    @ApiModelProperty(value = "已住人数")
    private Integer yzrs;

}
