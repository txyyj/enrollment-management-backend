package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormWeiJi;
import org.edu.modules.dorm.mapper.VeDormWeiJiMapper;
import org.edu.modules.dorm.service.VeDormWeiJiService;
import org.edu.modules.dorm.vo.VeDormWeiJiVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VeDormWeiJiServiceImpl extends ServiceImpl<VeDormWeiJiMapper, VeDormWeiJi> implements VeDormWeiJiService{
    @Autowired
    private VeDormWeiJiMapper veDormWeiJiMapper;

    @Override
    public Integer updateDiscipline(VeDormWeiJi veDormWeiJi) {
        return veDormWeiJiMapper.updateDiscipline(veDormWeiJi);
    }

    @Override
    public List<VeDormWeiJiVo> weiJiTable(String name, String xh) {
        return veDormWeiJiMapper.weiJiTable(name,xh);
    }

}
