package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeBaseCampus;
import org.edu.modules.dorm.mapper.VeBaseCampusMapper;
import org.edu.modules.dorm.service.VeBaseCampusService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VeBaseCampusImplServiceImpl extends ServiceImpl<VeBaseCampusMapper, VeBaseCampus> implements VeBaseCampusService {
    @Autowired
    private VeBaseCampusMapper veBaseCampusMapper;

    @Override
    public List<VeBaseCampus> list() {
        return veBaseCampusMapper.selectList(null);
    }

}
