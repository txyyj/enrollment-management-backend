package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
@TableName("ve_dorm_weiji")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ve_dorm_weiji对象", description = "宿舍违纪表")
public class VeDormWeiJi implements Serializable {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    @Excel(name = "学生id", width = 15)
    @ApiModelProperty(value = "学生id")
    private Long userid;

    @Excel(name = "学号", width = 15)
    @ApiModelProperty(value = "学号")
    private String xh;

    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private String xm;

    @Excel(name = "违纪类型", width = 15)
    @ApiModelProperty(value = "违纪类型")
    private Long wjlx;

    @Excel(name = "违纪时间", width = 15)
    @ApiModelProperty(value = "违纪时间")
    private Long wjsj;

    @Excel(name = "违纪说明", width = 15)
    @ApiModelProperty(value = "违纪说明")
    private String wjsm;

    @Excel(name = "登记人id", width = 15)
    @ApiModelProperty(value = "登记人id")
    private Long djr;

    @Excel(name = "登记时间", width = 15)
    @ApiModelProperty(value = "登记时间")
    private Long djsj;

    @Excel(name = "系统ID", width = 15)
    @ApiModelProperty(value = "系统ID")
    private Long terminalid;

}
