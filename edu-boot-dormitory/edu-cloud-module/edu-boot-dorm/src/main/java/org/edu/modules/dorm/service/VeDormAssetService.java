package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormAsset;
import org.edu.modules.dorm.vo.VeDormAssetVo;

import java.util.List;

public interface VeDormAssetService extends IService<VeDormAsset> {

    List<VeDormAssetVo> fuzzyQuery(String zcName, Long zcType, Integer status);

}
