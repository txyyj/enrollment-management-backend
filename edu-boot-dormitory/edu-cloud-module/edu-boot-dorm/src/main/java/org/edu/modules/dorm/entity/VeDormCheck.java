package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 宿舍考勤管理
 * @Author:
 * @Date:   2021-03-09
 * @Version: V1.0
 */

@Data
@TableName("ve_dorm_check")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormCheck对象", description = "宿舍考勤管理")
public class VeDormCheck implements Serializable {

    /** （1）自增id。 */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "自增id")
    private Long id;

    /** （2）考勤学生id。 */
    @Excel(name = "考勤学生id", width = 15)
    @ApiModelProperty(value = "考勤学生id")
    @TableField(value = "stuId")
    private Long stuId;

    /** （3）考勤规则编码。 */
    @Excel(name = "考勤规则编码", width = 15)
    @ApiModelProperty(value = "考勤规则编码")
    @TableField(value = "checkCode")
    private String checkCode;

    /** （4）考勤日期。 */
    @Excel(name = "考勤日期", width = 15)
    @ApiModelProperty(value = "考勤日期")
    @TableField(value = "checkTime")
    private String checkTime;

    /** （5）考勤记录时间。 */
    @Excel(name = "考勤记录时间", width = 15)
    @ApiModelProperty(value = "考勤记录时间")
    @TableField(value = "createTime")
    private Long createTime;

    /** （6）考勤记录人id。 */
    @Excel(name = "考勤记录人id", width = 15)
    @ApiModelProperty(value = "考勤记录人id")
    @TableField(value = "createUserId")
    private Long createUserId;

    /** （7）终端id。 */
    @Excel(name = "终端id", width = 15)
    @ApiModelProperty(value = "终端id")
    @TableField(value = "terminalId")
    private Long terminalId;

}
