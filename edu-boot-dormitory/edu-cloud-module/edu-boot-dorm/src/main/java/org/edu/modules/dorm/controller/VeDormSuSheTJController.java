package org.edu.modules.dorm.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.dorm.entity.VeDormSuShe;
import org.edu.modules.dorm.service.VeDormSuSheTJService;
import org.edu.modules.dorm.vo.VeDormCheckVo;
import org.edu.modules.dorm.vo.VeDormSuSheTJVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@Api(tags="宿舍统计")
@RestController
@RequestMapping("dorm/suSheTJ")
@Slf4j
public class VeDormSuSheTJController extends BaseController<VeDormSuShe, VeDormSuSheTJService> {
    @Autowired(required = false)
    private VeDormSuSheTJService veDormSuSheTJService;

    /**
     * 1、宿舍容量统计。
     */
    @ApiOperation(value="宿舍容量统计", notes="站点可选择模板-宿舍容量统计")
    @PostMapping(value = "/dormCount")
    public Result<?> dormCount() {
        ArrayList<VeDormSuSheTJVo> list = (ArrayList<VeDormSuSheTJVo>) veDormSuSheTJService.selectDormList();
        return Result.OK(list);
    }

    /**
     * 2、宿舍学生统计。
     */
    @ApiOperation(value="宿舍学生统计", notes="站点可选择模板-宿舍学生统计")
    @PostMapping(value = "/studentCount")
    public Result<?> studentCount() {
        ArrayList<VeDormSuSheTJVo> list = (ArrayList<VeDormSuSheTJVo>) veDormSuSheTJService.selectStudentList();
        return Result.OK(list);
    }

    /**
     * 3、考勤统计。（通过考勤类型id、宿舍楼id、院系id、专业id、班级id精确查询，房间编号模糊查询）
     * @param checkRuleId 考勤类型id
     * @param sslId 宿舍楼id
     * @param facultyId 院系id
     * @param specialtyId 专业id
     * @param banjiId 班级id
     * @param FJBM 房间编号
     */
    @ApiOperation(value="通过考勤类型id、宿舍楼id、院系id、专业id、班级id精确查询，房间编号模糊查询", notes = "站点可选择模板-通过考勤类型id、宿舍楼id、院系id、专业id、班级id精确查询，房间编号模糊查询")
    @PostMapping(value = "/checkCount")
    public Result<?> checkCount(@ApiParam(value = "考勤类型id") @RequestParam(name = "checkRuleId") Long checkRuleId,
                                @ApiParam(value = "宿舍楼id") @RequestParam(name = "sslId") Long sslId,
                                @ApiParam(value = "院系id") @RequestParam(name = "facultyId") Long facultyId,
                                @ApiParam(value = "专业id") @RequestParam(name = "specialtyId") Long specialtyId,
                                @ApiParam(value = "班级id") @RequestParam(name = "banjiId") Long banjiId,
                                @ApiParam(value = "房间编号") @RequestParam(name = "FJBM") String FJBM) {


        ArrayList<VeDormCheckVo> list = (ArrayList<VeDormCheckVo>) veDormSuSheTJService.selectCheckList(checkRuleId, sslId, facultyId, specialtyId, banjiId, FJBM);
        return Result.OK(list);
    }

    /**
     * 4、违纪统计。（通过违纪类型id、宿舍楼id、院系id、专业id、班级id精确查询，房间编号模糊查询）
     * @param disciplineTypeId 违纪类型id
     * @param sslId 宿舍楼id
     * @param facultyId 院系id
     * @param specialtyId 专业id
     * @param banjiId 班级id
     * @param FJBM 房间编号
     */
    @ApiOperation(value="通过违纪类型id、宿舍楼id、院系id、专 业id、班级id精确查询，房间编号模糊查询", notes = "站点可选择模板-通过违纪类型id、宿舍楼id、院系id、专业id、班级id精确查询，房间编号模糊查询")
    @PostMapping(value = "/disciplineCount")
    public Result<?> disciplineCount(@ApiParam(value = "违纪类型id") @RequestParam(name = "disciplineTypeId",required = false) Long disciplineTypeId,
                                     @ApiParam(value = "宿舍楼id") @RequestParam(name = "sslId",required = false) Long sslId,
                                     @ApiParam(value = "院系id") @RequestParam(name = "facultyId",required = false) Long facultyId,
                                     @ApiParam(value = "专业id") @RequestParam(name = "specialtyId",required = false) Long specialtyId,
                                     @ApiParam(value = "班级id") @RequestParam(name = "banjiId",required = false) Long banjiId,
                                     @ApiParam(value = "房间编号") @RequestParam(name = "FJBM",required = false) String FJBM) {
        ArrayList<VeDormCheckVo> list = (ArrayList<VeDormCheckVo>) veDormSuSheTJService.selectDisciplineList(disciplineTypeId, sslId, facultyId, specialtyId, banjiId, FJBM);
        return Result.OK(list);
    }

}
