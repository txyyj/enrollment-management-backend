package org.edu.modules.dorm.vo;

import lombok.Data;

/**
 * @author leidq
 * @create 2021-05-14 16:02
 */
@Data
public class VeReplaceBedVotmp {

    private String xQBH; //校区编号
    private String sSLBH; //宿舍楼编号
    private String sSBH;  //宿舍编号
    private String stuId; //学生id

    private String xQBH1; //校区编号
    private String sSLBH1; //宿舍楼编号
    private String sSBH1;  //宿舍编号
    private String stuId1; //学生id

}
