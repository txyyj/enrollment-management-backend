package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;


@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormWsceilExcelVO implements Serializable {

    @Excel(name = "学期",width = 15)
    @ApiModelProperty(value = "semId")
    private String semId;

    @Excel(name = "校区",width = 15)
    @ApiModelProperty(value = "校区Id")
    private String campusId;

    @Excel(name = "宿舍楼",width = 15)
    @ApiModelProperty(value = "建筑物Id")
    private String jzwId;

    @Excel(name = "楼层",width = 15)
    @ApiModelProperty(value = "楼层Id")
    private String lcId;

    @Excel(name = "宿舍",width = 15)
    @ApiModelProperty(value = "宿舍Id")
    private String ssId;

    @Excel(name = "得分",width = 15)
    @ApiModelProperty(value = "得分")
    private Long score;

    @Excel(name = "月份",width = 15)
    @ApiModelProperty(value = "月份")
    private Long mouth;

    @Excel(name = "周数",width = 15)
    @ApiModelProperty(value = "周数")
    private Long weekNum;

    /**周*/
    @Excel(name = "周",width = 15)
    @ApiModelProperty(value = "周")
    private Long week;


}
