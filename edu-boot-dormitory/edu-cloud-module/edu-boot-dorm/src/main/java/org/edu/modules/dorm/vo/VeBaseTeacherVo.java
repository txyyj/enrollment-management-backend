package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeBaseTeacherVo", description = "学生列表")
public class VeBaseTeacherVo implements Serializable {

    @TableId(value = "id")
    @ApiModelProperty(value = "id")
    private Integer id;

    @TableId(value = "XM")
    @ApiModelProperty(value = "姓名")
    private String XM;

}
