package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormManager;
import org.edu.modules.dorm.mapper.VeDormManagerMapper;
import org.edu.modules.dorm.service.VeDormManagerService;
import org.edu.modules.dorm.vo.VeDormManagerVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public  class VeDormManagerServiceImpl extends ServiceImpl<VeDormManagerMapper, VeDormManager> implements VeDormManagerService {
    @Autowired
    private VeDormManagerMapper veDormManagerMapper;

    @Override
    public Integer addMng(VeDormManager veDormManager) {
        return veDormManagerMapper.insert(veDormManager);
    }



}
