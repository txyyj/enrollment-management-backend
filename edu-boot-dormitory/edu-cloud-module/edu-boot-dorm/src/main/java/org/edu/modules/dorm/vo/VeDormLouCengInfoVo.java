package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.edu.modules.dorm.entity.VeDormSuShe;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;
import java.util.List;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormLouCengInfoVo implements Serializable {

//    1.宿舍名称、总楼层数、总寝室数、总床位数、已住人数
    //同一楼层宿舍
    @Excel(name = "宿舍名称", width = 15)
    @ApiModelProperty("宿舍名称")
    private List<VeDormSuShe> roomList;

    @Excel(name = "总楼层数", width = 15)
    @ApiModelProperty("总楼层数")
    private Integer totalFloors;

    @Excel(name = "总寝室数", width = 15)
    @ApiModelProperty("总寝室数")
    private Integer totalRooms;

    @Excel(name = "总床位数", width = 15)
    @ApiModelProperty("总床位数")
    private Long totalBeds;

    @Excel(name = "已住人数", width = 15)
    @ApiModelProperty("已住人数")
    private Long yzrs;

}
