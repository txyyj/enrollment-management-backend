package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

@Data
@TableName("ve_dorm_asset")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "HqDormAssetVO对象", description = "资产查询")
public class VeDormAssetExcelVo {

    /**资产ID*/
    @ApiModelProperty(value = "资产ID")
    private Long id;

    /**资产名称*/
    @Excel(name = "资产名称", width = 15)
    @ApiModelProperty(value = "资产名称")
    private String zcName;

    /**宿舍楼编号*/
    @Excel(name = "宿舍楼编号", width = 15)
    @ApiModelProperty(value = "宿舍楼编号")
    private String JZWH;

    /**房间编号*/
    @Excel(name = "房间编号", width = 15)
    @ApiModelProperty(value = "房间编号")
    private String FJBM;

    /**资产个数*/
    @Excel(name = "资产个数", width = 15)
    @ApiModelProperty(value = "资产个数")
    private Integer nums;

    /**资产类别*/
    @Excel(name = "资产类别", width = 15)
    @ApiModelProperty(value = "资产类别")
    private String typeName;

    /**资产状态*/
    @Excel(name = "资产状态", width = 15)
    @ApiModelProperty(value = "资产状态")
    private String  state;

}
