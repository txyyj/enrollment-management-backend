package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 宿舍制度类别
 * @Author:
 * @Date:   2021-03-08
 * @Version: V1.0
 */

@Data
@TableName("ve_dorm_notice")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormNotice implements Serializable {

    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /**通知类型*/
    @Excel(name = "通知类型",width = 15)
    @ApiModelProperty(value = "通知类型")
    @TableField(value = "type")
    private Long type;

    /**校区Id*/
    @Excel(name = "校区Id",width = 15)
    @ApiModelProperty(value = "校区ID")
    @TableField(value = "campusId")
    private Long campusId;

    /**通知目标*/
    @Excel(name = "通知目标",width = 15)
    @ApiModelProperty(value = "通知目标")
    @TableField(value = "rowId")
    private Long rowId;

    /**标题*/
    @Excel(name = "标题",width = 15)
    @ApiModelProperty(value = "标题")
    @TableField(value = "title")
    private String title;

    /**内容*/
    @Excel(name = "内容",width = 15)
    @ApiModelProperty(value = "内容")
    @TableField(value = "content")
    private String content;

    /**发布者*/
    @Excel(name = "发布者",width = 15)
    @ApiModelProperty(value = "发布者")
    @TableField(value = "createUserId")
    private Long createUserId;

    /**发布时间*/
    @Excel(name = "发布时间",width = 15)
    @ApiModelProperty(value = "发布时间")
    @TableField(value = "createTime")
    private Long createTime;

    /**终端id*/
    @Excel(name = "终端id",width = 15)
    @ApiModelProperty(value = "终端id")
    @TableField(value = "terminalId")
    private Long terminalId;

}
