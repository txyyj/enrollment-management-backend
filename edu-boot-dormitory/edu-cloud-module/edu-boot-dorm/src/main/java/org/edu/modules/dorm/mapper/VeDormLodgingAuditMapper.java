package org.edu.modules.dorm.mapper;

import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.vo.VeDormLodgingAuditVo;

import java.util.List;

public interface VeDormLodgingAuditMapper{

    List<VeDormLodgingAuditVo> LodgingAuditTable(@Param("xm") String xm,@Param("type") Integer type);

    Integer LodgingAudit(@Param("status") Integer status,@Param("id") Integer id,@Param("auditRemark") String auditRemark);

    Integer check(@Param("status") Integer status,@Param("id") Integer id);

    List<VeDormLodgingAuditVo> selectById(@Param("id") Integer id);

}
