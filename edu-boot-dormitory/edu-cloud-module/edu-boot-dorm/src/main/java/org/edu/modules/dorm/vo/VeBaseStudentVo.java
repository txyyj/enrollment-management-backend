package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@TableName("ve_base_student")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeBaseStudentVo", description = "学生列表")
public class VeBaseStudentVo implements Serializable {

    @TableId(value = "id")
    @ApiModelProperty(value = "id")
    private Integer id;

    @TableId(value = "XH")
    @ApiModelProperty(value = "学号")
    private String xh;

    @TableId(value = "SFZH")
    @ApiModelProperty(value = "身份证号")
    private String sfzh;

    @TableId(value = "XBM")
    @ApiModelProperty(value = "性别码")
     private String xbm;

    @TableId(value = "XM")
    @ApiModelProperty(value ="姓名")
    private String xm;

}
