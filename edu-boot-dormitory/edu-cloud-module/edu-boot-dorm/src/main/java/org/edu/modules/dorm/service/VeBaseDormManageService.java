package org.edu.modules.dorm.service;

import org.edu.modules.dorm.entity.bo.*;


import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-07-07 11:06
 */
public interface VeBaseDormManageService {
    //获取校区信息
    BasicResponseBO<List> getCampusMessage();

    //获取年级信息
    BasicResponseBO<List> getGradeMessage();
    //获取学期信息
    BasicResponseBO<List> getSemesterMessage();
    //根据专业id获取班级下拉框信息
    BasicResponseBO<List> getBanjiMessage(Integer zyId);
    //获取学院下拉框信息
    BasicResponseBO<List> getFacultyMessage();
    //根据学院id获取专业下拉框信息
    BasicResponseBO<List> getSpecialtyMessage(Integer xyId);

    //通过专业id获取专业信息
    BasicResponseBO<List> getSpecialtyMessageById(Integer zyId);
    //获取班级信息
    BasicResponseBO<List> getBanjiMessage();
    //获取学生信息
    BasicResponseBO<List> getStudentMessage();
    //获取教师信息
    BasicResponseBO<List> getTeacherMessage();


}
