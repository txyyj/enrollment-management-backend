package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormChange;
import org.edu.modules.dorm.entity.VeDormStudent;
import org.edu.modules.dorm.vo.VeDormChangeVo;

import java.util.List;

public interface VeDormChangeService extends IService<VeDormChange> {

    Boolean approveChange(VeDormChange veDormChange, VeDormStudent veDormStudent);

    List<VeDormChangeVo> applyMessage(String name);

    VeDormChangeVo approveMessage(Long id);

}
