package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormWeiJiVo implements Serializable {

    @Excel(name = "id", width = 15)
    @ApiModelProperty(value = "id")
    private Integer id;

    @Excel(name = "学生姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private String  xm;

    @Excel(name = "学号", width = 15)
    @ApiModelProperty(value = "学号")
    private String xh;

    @Excel(name = "违纪日期", width = 15)
    @ApiModelProperty(value = "违纪日期")
    private String wjsj;

    @Excel(name = "记录时间", width = 15)
    @ApiModelProperty(value = "记录时间")
    private Integer djsj;

    @Excel(name = "记录人用户id", width = 15)
    @ApiModelProperty(value = "记录人用户id")
    private String djr;

    @Excel(name = "违纪说明", width = 15)
    @ApiModelProperty(value = "违纪说明")
    private String WJSM;

    @Excel(name = "类型", width = 15)
    @ApiModelProperty(value = "类型")
    private String typename;

    @Excel(name = "类型Id", width = 15)
    @ApiModelProperty(value = "类型ID")
    private String typeId;

}
