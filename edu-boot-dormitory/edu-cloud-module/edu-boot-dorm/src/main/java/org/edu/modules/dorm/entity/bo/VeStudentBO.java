package org.edu.modules.dorm.entity.bo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Auther 李少君
 * @Date 2021-07-07 15:36
 */
@Data
@TableName("ve_dorm_student")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeStudentBO {

    private Long id;

    private String sfzh;

    private String xh;

    private String xm;

    private String xbm;

    private Long userId;

    private String mzm;

    private String bmh;

    private Integer jdfs;

    private String xsdqztm;

    private Long rxny;

    private Long xz;

    private Long falId;

    private Long specId;

    private Long bjId;

    private Long gradeId;

    private Long createTime;

    private Long updateTime;

    private String province;

    private Long provinceId;

    private String city;

    private Long cityId;

    private String county;

    private Long countyId;

    private Long shengId;

    private Long shild;

    private Long quId;

    private Integer sfkns;

    private String zkzh;

    private String ksh;

    private Integer updateStatus;


}
