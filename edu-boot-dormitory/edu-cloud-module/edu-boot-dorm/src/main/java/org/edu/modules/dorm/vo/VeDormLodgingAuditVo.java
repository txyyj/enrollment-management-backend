package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormLodgingAuditVo {

    @ApiModelProperty(value = "id")
    private Integer id;

    @Excel(name = "学号",width = 15)
    @ApiModelProperty(value = "学号")
    private String xh;

    private Integer userId;//李少君+
    private Integer falId;//李少君+
    private Integer specId;//李少君+
    private Integer banjiId;//李少君+

    @Excel(name = "姓名",width = 15)
    @ApiModelProperty(value = "姓名")
    private String xm;

    @Excel(name = "校区名称",width = 15)
    @ApiModelProperty(value = "校区名称")
    private  String xqmc;

    @Excel(name = "建筑物名称",width = 15)
    @ApiModelProperty(value = "建筑物名称")
    private String jzwmc;

    @Excel(name = "年纪名称",width = 15)
    @ApiModelProperty(value = "年级名称")
    private String njmc;

    @Excel(name = "院系名称",width = 15)
    @ApiModelProperty(value = "院系名称")
    private String yxmc;

    @Excel(name = "专业名称",width = 15)
    @ApiModelProperty(value = "专业名称")
    private String zymc;
    @Excel(name = "行政班名称",width = 15)
    @ApiModelProperty(value = "行政班名称")
    private String xzbmc;

    @Excel(name = "留宿类型",width = 15)
    @ApiModelProperty(value = "留宿类型")
    private Integer type;

    @Excel(name = "开始时间",width = 15)
    @ApiModelProperty(value = "开始时间")
    private int startTime;

    @Excel(name = "结束时间",width = 15)
    @ApiModelProperty(value = "结束时间")
    private int endTime;

    @Excel(name = "审核状态",width = 15)
    @ApiModelProperty(value = "审核状态")
    private Integer auditStatus;

    @Excel(name = "宿舍名称",width = 15)
    @ApiModelProperty(value = "宿舍名称")
    private String fjbm;

    @Excel(name = "申请时间",width = 15)
    @ApiModelProperty(value = "申请时间")
    private Long audittime;

    @Excel(name = "留宿原因",width = 15)
    @ApiModelProperty(value = "留宿原因")
    private String remark;

    @Excel(name = "审核原因",width = 15)
    @ApiModelProperty(value = "审核原因")
    private String auditremark;

}
