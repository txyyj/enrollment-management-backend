package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormAsset;
import org.edu.modules.dorm.vo.VeDormAssetVo;
import org.edu.modules.dorm.vo.VeSuSheAssetVo;

import java.util.List;

public interface VeDormSuSheAssetMapper  {
    List<VeSuSheAssetVo> suSheAsset();

    List<VeSuSheAssetVo> suSheQueryByName(
                                          @Param("zcName")String zcName,
                                          @Param("zcType")Integer zcType,
                                          @Param("status")Integer status);

    List<VeSuSheAssetVo> suSheQueryBySsId(
            @Param("ssId")Long ssId,
             @Param("zcName")String zcName,
            @Param("zcType")Integer zcType,
            @Param("status")Integer status);

  VeSuSheAssetVo updateMessage(@Param("id")Long id);
}
