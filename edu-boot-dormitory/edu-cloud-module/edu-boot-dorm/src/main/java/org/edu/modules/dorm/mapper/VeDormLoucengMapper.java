package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormLouceng;
import org.edu.modules.dorm.vo.VeDormLouCengVo;

import java.util.List;

public interface VeDormLoucengMapper extends BaseMapper<VeDormLouceng> {

    List<VeDormLouCengVo> selectByParam(@Param("xqId") Integer xqId,@Param("sslId")Integer sslId);
    //根据建筑物号和楼层号获取楼层的id
    Long getLoucengId(@Param("JZWH") Integer jzwh,@Param("lcName") String lcName);

}
