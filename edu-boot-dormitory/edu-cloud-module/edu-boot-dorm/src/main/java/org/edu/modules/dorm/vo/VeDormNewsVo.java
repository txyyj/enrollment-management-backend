package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 寝室制度
 * @Author:
 * @Date:   2021-03-08
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="寝室制度", description="寝室制度")
public class VeDormNewsVo {
    /**id*/
    @ApiModelProperty(value = "id")
    private Long id;
    /**标题*/
    @Excel(name = "标题",width = 15)
    @ApiModelProperty(value = "标题")
    private String title;
    /**用户姓名*/
    @Excel(name = "用户姓名",width = 15)
    @ApiModelProperty(value = "用户姓名")
    private String createUserName;
    /**创建时间*/
    @Excel(name = "创建时间",width = 15)
    @ApiModelProperty(value = "创建时间")
    private Long createTime;
    /**排序*/
    @Excel(name = "排序",width = 15)
    @ApiModelProperty(value = "排序")
    private Long listSort;
    /**类别*/
    @Excel(name = "类型",width = 15)
    @ApiModelProperty(value = "类别")
    private String typeName;
  /**类别ID*/
  @Excel(name = "类型ID",width = 15)
  @ApiModelProperty(value = "类别ID")
  private Long typeId;
  /**内容*/
  @Excel(name = "内容",width = 15)
  @ApiModelProperty(value = "内容")
  private String content;
  /**文件名称*/
  @Excel(name = "文件名称",width = 15)
  @ApiModelProperty("文件名称")
  private String  fileName;
  /**文件大小*/
  @Excel(name = "文件大小",width = 15)
  @ApiModelProperty("文件大小")
  private Long  fileSize;
  /**文件路径*/
  @Excel(name = "文件路径",width = 15)
  @ApiModelProperty("文件路径")
  private String  sourceFile;
}
