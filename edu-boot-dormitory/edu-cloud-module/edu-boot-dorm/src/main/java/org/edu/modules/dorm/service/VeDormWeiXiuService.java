package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormWeiXiu;
import org.edu.modules.dorm.vo.VeDormWeiXiuVo;

import java.util.List;

public interface VeDormWeiXiuService extends IService<VeDormWeiXiu> {

    List<VeDormWeiXiuVo> weiXiuInfo( Integer ssId,String title);

    List<VeDormWeiXiuVo> weiXiuInfoTable(String title);

}
