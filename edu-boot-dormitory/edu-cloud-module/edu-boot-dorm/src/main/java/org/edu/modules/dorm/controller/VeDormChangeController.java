package org.edu.modules.dorm.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.dorm.entity.VeDormChange;
import org.edu.modules.dorm.entity.VeDormStudent;
import org.edu.modules.dorm.entity.bo.BasicResponseBO;
import org.edu.modules.dorm.entity.bo.VeCampusBO;
import org.edu.modules.dorm.service.VeBaseDormManageService;
import org.edu.modules.dorm.service.VeDormChangeService;
import org.edu.modules.dorm.service.VeDormStudentService;
import org.edu.modules.dorm.service.VeDormSuSheFenPeiService;
import org.edu.modules.dorm.vo.VeDormChangeVo;
import org.edu.modules.dorm.vo.VeDormLouCengVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(tags="宿舍变更")
@RestController
@RequestMapping("dorm/change")
@Slf4j
public class VeDormChangeController extends BaseController<VeDormChange, VeDormChangeService> {
    @Autowired
    private VeDormChangeService veDormChangeService;
    @Autowired
    private VeDormStudentService veDormStudentService;
    @Autowired
    private VeDormSuSheFenPeiService veDormSuSheFenPeiService;
    @Autowired
    private VeBaseDormManageService veBaseDormManageService; //李少君

    /**
     * 申请变更
     * @param userId, XXQH, XSSLBH, SQYY
     */
    @AutoLog(value = "申请变更学生宿舍")
    @ApiOperation(value="新增申请信息", notes="站点可选择模板-申请变更宿舍")
    @PostMapping(value = "/add")
    public Result<?> addChange(@ApiParam(value = "用户id") @RequestParam("userId") Long userId,
                               @ApiParam(value = "新校区号") @RequestParam("XXQH") Long XXQH,
                               @ApiParam(value = "新宿舍楼编号") @RequestParam("XSSLBH") Long XSSLBH,
                               @ApiParam(value = "申请缘由") @RequestParam("SQYY") String SQYY,
                               @ApiParam(value = "原校区号") @RequestParam("XQH") Long XQH,
                               @ApiParam(value = "原宿舍楼号") @RequestParam("SSLH") Long SSLH,
                               @ApiParam(value = "房间编号") @RequestParam("FJBH") Long FJBH) {
        Long time = System.currentTimeMillis() / 1000;
        VeDormStudent veDormStudent = veDormStudentService.getById(userId);
        VeDormChange veDormChange = new VeDormChange();
        veDormChange.setSQSJ(time);
        veDormChange.setSQYY(SQYY);
        veDormChange.setXXQH(XXQH);
        veDormChange.setXSSLBM(XSSLBH);
        veDormChange.setFJBM(FJBH);
        veDormChange.setXQH(XQH);
        veDormChange.setSSLBM(SSLH);
        veDormChange.setUserId(userId);
        veDormChange.setSQR(userId);
        veDormChange.setXM(veDormStudent.getXM());
        veDormChange.setXH(veDormStudent.getXH());

        veDormChangeService.save(veDormChange);
        return Result.OK("申请成功！");
    }

    /**
     * 查看申请内容
     */
    @ApiOperation(value = "查看申请的信息")
    @PostMapping(value = "/queryByName")
    public Result<?> applyMessage(@ApiParam(value = "姓名") @RequestParam(value = "name") String name){
        if("".equals(name)){
            name = null;
        }
//        BasicResponseBO<List> list = veBaseDormManageService.getCampusMessage(); //李少君
//        List<VeCampusBO> result = JSONObject.parseArray(JSON.toJSONString(list.getResult()),VeCampusBO.class);
//        List<VeDormChangeVo> v = veDormChangeService.applyMessage(name);
//        for(int i =0 ;i<v.size();i++){
//            for (int j =0 ;j<result.size();j++){
//                if(v.get(i).getOldCampusId()==(result.get(j).getId().longValue())){
//                    v.get(i).setXQM(result.get(j).getXqmc());
//                }
//                if(v.get(i).getNewCampusId()==(result.get(j).getId().longValue())){
//                    v.get(i).setXXQM(result.get(j).getXqmc());
//                }
//            }
//        }
        List<VeDormChangeVo> list = veDormChangeService.applyMessage(name);
        return Result.OK(list);
    }

    /**
     * 获取编辑申请信息
     * @param id
     */
    @AutoLog("获取编辑申请变更宿舍")
    @ApiOperation(value = "获取编辑申请信息", notes = "")
    @PostMapping(value = "/updateMessage")
    public Result<?> updateMessage(@ApiParam(value = "学生宿舍变动id") @RequestParam("id") Long id){
        VeDormChange veDormChange = veDormChangeService.getById(id);
        return Result.OK(veDormChange);
    }

    /**
     * 编辑申请信息
     * @param userId, id, XM, XXQH, XSSLBH, SQYY
     */
    @AutoLog("编辑申请变更宿舍")
    @ApiOperation(value = "编辑申请信息", notes = "")
    @PostMapping(value = "/update")
    public Result<?> updateChange(@ApiParam(value = "学生宿舍变动id") @RequestParam("id") Long id,
                                  @ApiParam(value = "用户id") @RequestParam("userId") Long userId,
                                  @ApiParam(value = "新校区号") @RequestParam("XXQH") Long XXQH,
                                  @ApiParam(value = "新宿舍楼编号") @RequestParam("XSSLBH") Long XSSLBH,
                                  @ApiParam(value = "申请缘由") @RequestParam("SQYY") String SQYY,
                                  @ApiParam(value = "原校区号") @RequestParam("XQH") Long XQH,
                                  @ApiParam(value = "原宿舍楼号") @RequestParam("SSLH") Long SSLH,
                                  @ApiParam(value = "房间编号") @RequestParam("FJBH") Long FJBH){
        VeDormChange veDormChange = new VeDormChange();
        VeDormStudent veDormStudent = veDormStudentService.getById(userId);
        veDormChange.setId(id);
        veDormChange.setUserId(userId);
        veDormChange.setXXQH(XXQH);
        veDormChange.setXSSLBM(XSSLBH);
        veDormChange.setSQYY(SQYY);
        veDormChange.setSQR(userId);
        veDormChange.setFJBM(FJBH);
        veDormChange.setXQH(XQH);
        veDormChange.setSSLBM(SSLH);
        veDormChange.setXM(veDormStudent.getXM());
        veDormChange.setXH(veDormStudent.getXH());

        veDormChangeService.updateById(veDormChange);
        return Result.OK("编辑成功！");
    }

    /**
     * 删除申请信息
     * @param id
     */
    @AutoLog("删除变更宿舍申请")
    @ApiOperation(value = "删除宿舍变更申请", notes = "站点可选择模板-通过id删除")
    @PostMapping(value = "/delete")
    public Result<?> deleteChange(@ApiParam(value = "学生宿舍变动id") @RequestParam("id") Integer id){
        veDormChangeService.removeById(id);
        return Result.OK("删除成功！");
    }

    /**
     * 变更宿舍审批
     */
    @AutoLog("变更宿舍审批")
    @ApiOperation(value = "变更宿舍审批",notes = "站点可选择模板-审批")
    @PostMapping(value = "/approve")
    public Result<?> approveChange(@ApiParam(value = "学生宿舍变动id") @RequestParam("id") Long id,
                                   @ApiParam(value = "变动状态") @RequestParam("BDZT") Integer BDZT,   //变动状态（0申请 1审批通过 2 驳回 3已执行）
                                   @ApiParam(value = "审批说明") @RequestParam("SPSM") String SPSM,
                                   @ApiParam(value = "校区号") @RequestParam(value = "XXQH", required = false) Long XXQH,
                                   @ApiParam(value = "新宿舍楼编号") @RequestParam(value = "XSSLBH", required = false) Long XSSLBH,
                                   @ApiParam(value = "新宿舍号") @RequestParam(value = "XFJBH", required = false) Long XFJBH,
                                   @ApiParam(value = "用户ID") @RequestParam("userId") Long userId,
                                   @ApiParam(value = "新床位") @RequestParam(value = "CWH", required = false) String CWH) {
        //宿舍审批表
        Long time = System.currentTimeMillis()/1000;
        VeDormChange veDormChange = new VeDormChange();
        veDormChange.setId(id);
        veDormChange.setBDZT(BDZT);
        veDormChange.setSPSM(SPSM);
        veDormChange.setSPR(0L);
        veDormChange.setXFJBM(XFJBH);
        veDormChange.setSPSJ(time);

        //获取旧学生
        QueryWrapper<VeDormStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("id", userId);
        VeDormStudent oldStudent = veDormStudentService.getOne(wrapper);

        //学生表
        VeDormStudent veDormStudent = new VeDormStudent();
        veDormStudent.setUserId(userId);
        if (BDZT != 2){
            veDormStudent.setXQH(XXQH);
            veDormStudent.setFJBM(XFJBH);
            veDormStudent.setSSLBM(XSSLBH);
            veDormStudent.setCWH(CWH);

            veDormSuSheFenPeiService.updateYzrs(-1,oldStudent.getFJBM());
            veDormSuSheFenPeiService.updateYzrs(1,XFJBH);
        }
        Boolean flag = veDormChangeService.approveChange(veDormChange,veDormStudent);
        if(flag){
            return Result.OK("审批成功！");
        }else{
            return Result.error("审批失败！");
        }
    }

    /**
     * 根据ID获取审批信息
     * @param id
     */
    @AutoLog("根据ID获取审批信息")
    @ApiOperation(value = "根据ID获取审批信息", notes = "")
    @PostMapping(value = "/approveMessage")
    public Result<?> approveMessage(@ApiParam(value = "学生宿舍变动id") @RequestParam("id") Long id){
        VeDormChangeVo veDormChangeVo = veDormChangeService.approveMessage(id);
        return Result.OK(veDormChangeVo);
    }

}
