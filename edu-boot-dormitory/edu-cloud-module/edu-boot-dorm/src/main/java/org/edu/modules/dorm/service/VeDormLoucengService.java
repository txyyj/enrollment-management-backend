package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormLouceng;
import org.edu.modules.dorm.vo.VeDormLouCengVo;
import org.edu.modules.dorm.vo.VeDormLoucengExcelVO;

import java.util.List;

public interface VeDormLoucengService extends IService<VeDormLouceng> {

    Integer insertNew(VeDormLouceng veDormLouceng);

    Integer updateLouCeng(VeDormLouceng veDormLouceng);

    Integer deleteLouceng(Integer id);

    List<VeDormLouCengVo> selectByParam(Integer xqId, Integer sslId);

    void excelImport(List<VeDormLoucengExcelVO> list) throws Exception;
    //根据建筑物号和楼层号获取楼层的id
    Long getLoucengId(@Param("JZWH") Integer jzwh, @Param("lcName") String lcName);

}
