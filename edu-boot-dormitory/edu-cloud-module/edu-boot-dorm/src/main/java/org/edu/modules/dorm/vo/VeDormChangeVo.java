package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.TableField;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 变更申请
 * @Author:
 * @Date:   2021-01-22
 * @Version: V1.0
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormChangeVO对象", description = "申请变更")
public class VeDormChangeVo implements Serializable {
    
    /**id*/
    @ApiModelProperty(value = "id")
    private Long id;
    
    /**学号*/
    @Excel(name = "学号", width = 15)
    @ApiModelProperty(value = "学号")
    private String XH;
    
    /**姓名*/
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private String XM;

    /**原校区号-李少君+*/
//    @Excel(name = "原校区号", width = 15)
    @ApiModelProperty(value = "原校区号")
    private Long oldCampusId;
    
    /**原校区名*/
    @Excel(name = "原校区名", width = 15)
    @ApiModelProperty(name = "原校区名")
    private String XQM;
    
    /**原宿舍楼名*/
    @Excel(name = "原宿舍楼名", width = 15)
    @ApiModelProperty(value = "原宿舍楼名")
    private String SSLM;
    
    /**原楼层号*/
    @Excel(name = "原楼层号", width = 15)
    @ApiModelProperty(value = "原楼层号")
    private Integer LCH;
    
    /**原房间名*/
    @Excel(name = "原房间名", width = 15)
    @ApiModelProperty(value = "原房间名")
    private String SSM;
    
    /**新校区名*/
    @Excel(name = "新校区名", width = 15)
    @ApiModelProperty(value = "新校区名")
    private String  XXQM;
    
    /**新校区号*/
    @Excel(name = "新校区号", width = 15)
    @ApiModelProperty(value = "新校区号")
    private Long newCampusId;

    /**新宿舍楼名*/
    @Excel(name = "新宿舍楼名", width = 15)
    @ApiModelProperty(value = "新宿舍楼名")
    private String  XSSLM;
    
    /**新宿舍楼*/
    @Excel(name = "新宿舍楼", width = 15)
    @ApiModelProperty(value = "新宿舍楼名")
    private Long  XSSLH;
    
    /**新楼层号*/
    @Excel(name = "新楼层号", width = 15)
    @ApiModelProperty(value = "新楼层号")
    private Integer XLCH;
    
    /**新宿舍名*/
    @Excel(name = "新宿舍名", width = 15)
    @ApiModelProperty(value = "新宿舍名")
    private String XSSM;
    
    /**申请人*/
    @Excel(name = "申请人", width =15 )
    @ApiModelProperty(value = "申请人")
    private Long SQR;
    
    /**申请时间*/
    @Excel(name = "申请时间", width = 15)
    @ApiModelProperty(value = "申请时间")
    @TableField(value = "SQSJ")
    private Long SQSJ;
    
    /**审批人*/
    @Excel(name = "审批人", width = 15)
    @ApiModelProperty(value = "审批人")
    @TableField(value = "SPR")
    private String SPR;

    /**变动状态（0申请 1审批通过 2 驳回 3已执行）*/
    @Excel(name = "变动状态（0申请 1审批通过 2 驳回 3已执行）", width = 15)
    @ApiModelProperty(value = "变动状态（0申请 1审批通过 2 驳回 3已执行）")
    private Integer BDZT;
    
    /**审批时间*/
    @Excel(name = "审批时间", width = 15)
    @ApiModelProperty(value = "审批时间")
    private Long SPSJ;

    /**申请缘由*/
    @Excel(name = "申请缘由", width = 15)
    @ApiModelProperty(value = "申请缘由")
    private String SQYY;

}
