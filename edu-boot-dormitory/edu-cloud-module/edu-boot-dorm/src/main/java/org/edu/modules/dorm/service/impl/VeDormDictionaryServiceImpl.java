package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormDictionary;
import org.edu.modules.dorm.mapper.VeDormDictionaryMapper;
import org.edu.modules.dorm.service.VeDormDictionaryService;
import org.springframework.stereotype.Service;

@Service
public class VeDormDictionaryServiceImpl extends ServiceImpl<VeDormDictionaryMapper, VeDormDictionary> implements VeDormDictionaryService {

}
