package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.dorm.entity.VeDormCheckRule;

public interface VeDormCheckRuleMapper extends BaseMapper<VeDormCheckRule> {

}
