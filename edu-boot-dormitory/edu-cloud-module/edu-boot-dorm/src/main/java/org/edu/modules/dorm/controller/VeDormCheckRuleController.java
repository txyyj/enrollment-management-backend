package org.edu.modules.dorm.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.dorm.entity.VeDormCheckRule;
import org.edu.modules.dorm.service.VeDormCheckRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@Api(tags="宿舍考勤设置")
@RestController
@RequestMapping("dorm/checkRule")
@Slf4j
public class VeDormCheckRuleController extends BaseController<VeDormCheckRule, VeDormCheckRuleService> {
    @Autowired(required = false)
    private VeDormCheckRuleService veDormCheckRuleService;

    /**
     * 1、设置考勤时间。
     * @param wgID 晚归id
     * @param wgLimitTime 晚归限制时间
     * @param ybgsID 夜不归宿id
     * @param ybgsLimitTime 夜不归宿限制时间
     */
    @AutoLog(value = "站点可选择模板-设置考勤时间")
    @ApiOperation(value="设置考勤时间", notes="站点可选择模板-设置考勤时间")
    @PostMapping(value = "/setCheckTime")
    public Result<?> setCheckTime(@ApiParam(value = "晚归id") @RequestParam(value = "wgID") Long wgID,
                                  @ApiParam(value = "晚归限制时间") @RequestParam(value = "wgLimitTime") String wgLimitTime,
                                  @ApiParam(value = "夜不归宿id") @RequestParam(value = "ybgsID") Long ybgsID,
                                  @ApiParam(value = "夜不归宿限制时间") @RequestParam(value = "ybgsLimitTime") String ybgsLimitTime) {
        //（1）晚归实体类。
        VeDormCheckRule wgEntity = new VeDormCheckRule();
        wgEntity.setId(wgID);
        wgEntity.setLimitTime(wgLimitTime);
        //（2）夜不归宿实体类。
        VeDormCheckRule ybgsEntity = new VeDormCheckRule();
        ybgsEntity.setId(ybgsID);
        ybgsEntity.setLimitTime(ybgsLimitTime);

        Integer updateNum = veDormCheckRuleService.updateCheckTime(wgEntity, ybgsEntity);
        if (updateNum == 2) {
            return Result.OK("设置成功！");
        } else {
            return Result.error("设置失败！");
        }
    }

    /**
     * 2、查询所有考勤类型。
     */
    @ApiOperation(value="查询所有考勤类型", notes="站点可选择模板-查询所有考勤类型")
    @PostMapping(value = "/selectAllCheckType")
    public Result<?> selectAllCheckType() {
        QueryWrapper<VeDormCheckRule> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("id");
        ArrayList<VeDormCheckRule> list = (ArrayList<VeDormCheckRule>) veDormCheckRuleService.list(wrapper);
        return Result.OK(list);
    }

}
