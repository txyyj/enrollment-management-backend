package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormWsceil;
import org.edu.modules.dorm.vo.VeDormCalendarVo;
import org.edu.modules.dorm.vo.VeDormWsceilExcelVO;
import org.edu.modules.dorm.vo.VeDormWsceilVo;

import java.util.List;

public interface VeDormWsceilService extends IService<VeDormWsceil> {

    List<VeDormWsceilVo> queryWsceil(Long campusId, Long jzwId, Long lcId, Long ssId, Long semId, Long week);


    VeDormCalendarVo findNowDate();

    Boolean add(Long campusId, Long semId, Integer week, Long jzwId, Long lcId, Long ssId, List<Long> list,
                Long mouth);

    List<VeDormWsceil> updateMessage(Long id);

    Boolean update(List<Long> idList, List<Long> scoreList);

    Boolean delete(Long id);

    List<VeDormWsceilVo> allWsceil(Long ssId);

    Integer findWsceil(Long semId, Integer weekNum, Long ssId);

    int excelImport(List<VeDormWsceilExcelVO> list) throws Exception;

}
