package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormCheckRule;
import org.edu.modules.dorm.mapper.VeDormCheckRuleMapper;
import org.edu.modules.dorm.service.VeDormCheckRuleService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

@Service
public class VeDormCheckRuleServiceImpl extends ServiceImpl<VeDormCheckRuleMapper, VeDormCheckRule> implements VeDormCheckRuleService {
    @Resource
    private VeDormCheckRuleMapper veDormCheckRuleMapper;

    /**
     * 1、设置考勤时间。
     * @param wgEntity 晚归实体类
     * @param ybgsEntity 夜不归宿实体类
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer updateCheckTime(VeDormCheckRule wgEntity, VeDormCheckRule ybgsEntity) {
        Integer updateNum = veDormCheckRuleMapper.updateById(wgEntity);
        updateNum += veDormCheckRuleMapper.updateById(ybgsEntity);

        return updateNum;
    }

}
