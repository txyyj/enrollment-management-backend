package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 宿舍考勤管理
 * @Author:
 * @Date:   2021-03-08
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormCheckVO对象", description = "宿舍考勤管理")
public class VeDormCheckVo implements Serializable {

    /** （1）自增id。（学生宿舍关系表id） */
    @ApiModelProperty(value = "自增id（学生宿舍关系表id）")
    private Long id;

    /** （2）姓名。 */
    @ApiModelProperty(value = "姓名")
    @TableField(value = "XM")
    private String XM;

    /** （3）学号。 */
    @ApiModelProperty(value = "学号")
    @TableField(value = "XH")
    private String XH;

    /** (4)建筑物名称。（宿舍楼） */
    @ApiModelProperty(value = "建筑物名称（宿舍楼）")
    @TableField(value = "JZWMC")
    private String JZWMC;

    /** （5）房间编号。（房间号） */
    @ApiModelProperty(value = "房间编号（房间号）")
    @TableField(value = "FJBM")
    private String FJBM;

    /** （6）宿舍楼编号。 */
    @ApiModelProperty(value = "宿舍楼编号")
    @TableField(value = "SSLBM")
    private Long SSLBM;

    /** （6）院系名称。 */
    @ApiModelProperty(value = "院系名称")
    @TableField(value = "YXMC")
    private String YXMC;

    /** （7）专业名称。 */
    @ApiModelProperty(value = "专业名称")
    @TableField(value = "ZYMC")
    private String ZYMC;

    /** （8）年级名称。 */
    @ApiModelProperty(value = "年级名称")
    @TableField(value = "NJMC")
    private String NJMC;

    /** （9）行政班名称。 */
    @ApiModelProperty(value = "行政班名称")
    @TableField(value = "XZBMC")
    private String XZBMC;

    /** （10）名称。（考勤类型） */
    @ApiModelProperty(value = "名称（考勤类型）")
    @TableField(value = "checkName")
    private String checkName;

    /** （11）考勤日期。（考勤时间） */
    @ApiModelProperty(value = "考勤日期（考勤类型）")
    @TableField(value = "checkTime")
    private Long checkTime;//李少君将string该成Long

    /** （12）名称。（违纪类型） */
    @ApiModelProperty(value = "名称（违纪类型）")
    @TableField(value = "wjName")
    private String wjName;

    /** （13）违纪日期。（违纪时间） */
    @ApiModelProperty(value = "违纪日期（违纪时间）")
    @TableField(value = "WJSJ")
    private Long WJSJ;

    /** （14）宿舍楼id。 */
    @ApiModelProperty(value = "宿舍楼id")
    @TableField(value = "SSLID")
    private Long SSLID;

    /** （14）宿舍楼id。 */
    @ApiModelProperty(value = "学生id")
    private String stuId;

}
