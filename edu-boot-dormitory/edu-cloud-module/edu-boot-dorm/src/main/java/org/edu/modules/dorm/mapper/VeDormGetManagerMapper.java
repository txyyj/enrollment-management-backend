package org.edu.modules.dorm.mapper;

import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormManager;
import org.edu.modules.dorm.vo.VeDormManagerVo;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-07-14 11:08
 */
@Repository
public interface VeDormGetManagerMapper {
    VeDormManagerVo showMng(@Param("sslId")Integer sslId);
    int updateAdminId(@Param("sslId")Integer sslId, @Param("userId")Integer userId);
}
