package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeFileFiles;
import org.edu.modules.dorm.mapper.VeFileFilesMapper;
import org.edu.modules.dorm.service.VeFileFilesService;
import org.springframework.stereotype.Service;

/**
 * @Auther 李少君
 * @Date 2021-08-12 17:22
 */
@Service
public class VeFileFilesServiceImpl extends ServiceImpl<VeFileFilesMapper, VeFileFiles> implements VeFileFilesService {
}
