package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormChange;
import org.edu.modules.dorm.vo.VeDormChangeVo;

import java.util.List;

public interface VeDormChangeMapper extends BaseMapper<VeDormChange> {

    List<VeDormChangeVo> applyMessage(@Param("name")String name);

    VeDormChangeVo approveMessage(@Param("id")Long id);

}
