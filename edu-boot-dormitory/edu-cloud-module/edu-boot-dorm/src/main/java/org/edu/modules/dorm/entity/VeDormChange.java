package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 宿舍变更
 * @Author:
 * @Date:   2021-03-03
 * @Version: V1.0
 */

@Data
@TableName("ve_dorm_change")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormChange implements Serializable {
    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /**用户Id*/
    @Excel(name = "用户Id",width = 15)
    @ApiModelProperty(value = "用户Id")
    @TableField(value = "userId")
    private Long userId;

    /**工号*/
    @Excel(name = "工号",width = 15)
    @ApiModelProperty(value = "工号")
    @TableField(value = "XH")
    private String XH;

    /**姓名*/
    @Excel(name = "姓名",width = 15)
    @ApiModelProperty(value = "姓名")
    @TableField(value = "XM")
    private String XM;

    /**原校区号*/
    @Excel(name = "原校区号",width = 15)
    @ApiModelProperty(name = "原校区号")
    @TableField(value = "XQH")
    private Long XQH;

    /**原宿舍楼编号*/
    @Excel(name = "原宿舍楼编号",width = 15)
    @ApiModelProperty(value = "原宿舍楼编号")
    @TableField(value = "SSLBM")
    private Long SSLBM;

    /**原楼层号*/
    @Excel(name = "原楼层号",width = 15)
    @ApiModelProperty(value = "原楼层号")
    @TableField(value = "LCH")
    private Integer LCH;

    /**原房间编号*/
    @Excel(name = "原房间编号",width = 15)
    @ApiModelProperty(value = "原房间编号")
    @TableField(value = "FJBM")
    private Long FJBM;

    /**新校区号*/
    @Excel(name = "新校区号",width = 15)
    @ApiModelProperty(value = "新校区号")
    @TableField(value = "XXQH")
    private Long XXQH;

    /**新宿舍楼编号*/
    @Excel(name = "新宿舍楼编号",width = 15)
    @ApiModelProperty(value = "新宿舍楼编号")
    @TableField(value = "XSSLBM")
    private Long XSSLBM;

    /**新楼层号*/
    @Excel(name = "新楼层号",width = 15)
    @ApiModelProperty(value = "新楼层号")
    @TableField(value = "XLCH")
    private Integer XLCH;

    /**新房间编号*/
    @Excel(name = "新房间编号",width = 15)
    @ApiModelProperty(value = "新房间编号")
    @TableField(value = "XFJBM")
    private Long XFJBM;

    /**申请人*/
    @Excel(name = "申请人",width =15 )
    @ApiModelProperty(value = "申请人")
    @TableField(value = "SQR")
    private Long SQR;

    /**申请原因*/
    @Excel(name ="申请原因",width = 200)
    @ApiModelProperty(value = "申请原因")
    @TableField(value = "SQYY")
    private String SQYY;

    /**申请时间*/
    @Excel(name = "申请时间",width = 15)
    @ApiModelProperty(value = "申请时间")
    @TableField(value = "SQSJ")
    private Long SQSJ;

    /**审批人*/
    @Excel(name = "审批人",width = 15)
    @ApiModelProperty(value = "审批人")
    @TableField(value = "SPR")
    private Long SPR;

    /**审批说明*/
    @Excel(name = "审批说明",width = 15)
    @ApiModelProperty(value = "审批说明")
    @TableField(value = "SPSM")
    private String SPSM;

    /**变动状态（0申请 1审批通过 2 驳回 3已执行）*/
    @Excel(name = "变动状态（0申请 1审批通过 2 驳回 3已执行）",width = 15)
    @ApiModelProperty(value = "变动状态（0申请 1审批通过 2 驳回 3已执行）")
    @TableField(value = "BDZT")
    private Integer BDZT;

    /**审批时间*/
    @Excel(name = "审批时间",width = 15)
    @ApiModelProperty(value = "审批时间")
    @TableField(value = "SPSJ")
    private Long SPSJ;

}
