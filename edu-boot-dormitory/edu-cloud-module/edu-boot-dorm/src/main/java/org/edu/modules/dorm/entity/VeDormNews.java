package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 宿舍制度
 * @Author:
 * @Date:   2021-03-08
 * @Version: V1.0
 */

@Data
@TableName("ve_dorm_news")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormNews implements Serializable {

    /**id*/
    @TableId(type = IdType.AUTO )
    @ApiModelProperty(value = "id")
    private Long id;

    /**标题*/
    @Excel(name = "标题",width = 15)
    @ApiModelProperty(value = "标题")
    @TableField(value = "title")
    private String title;

    /**内容*/
    @Excel(name = "内容",width = 15)
    @ApiModelProperty(value = "内容")
    @TableField(value = "content")
    private String content;

    /**文件Id*/
    @Excel(name = "文件Id",width = 15)
    @ApiModelProperty(value = "文件Id")
    @TableField(value = "fileId")
    private Long fileId;

    /**用户Id*/
    @Excel(name = "用户Id",width = 15)
    @ApiModelProperty(value = "用户Id")
    @TableField(value = "craeteUserId")
    private Long craereUserId;

    /**用户姓名*/
    @Excel(name = "用户姓名",width = 15)
    @ApiModelProperty(value = "用户姓名")
    @TableField(value = "createUserName")
    private String createUserName;

    /**创建时间*/
    @Excel(name = "创建时间",width = 15)
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "createTime")
    private Long createTime;

    /**排序*/
    @Excel(name = "排序",width = 15)
    @ApiModelProperty(value = "排序")
    @TableField(value = "listSort")
    private Long listSort;

    /**类别*/
    @Excel(name = "类型",width = 15)
    @ApiModelProperty(value = "类别")
    @TableField(value = "typeId")
    private Long typeId;

}
