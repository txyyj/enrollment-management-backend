package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormManager;
import org.edu.modules.dorm.vo.VeDormManagerVo;

import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-07-14 10:58
 */
public interface VeDormGetManagerService {

    VeDormManagerVo showMng(Integer sslId);
    int updateAdminId(Integer sslId, Integer userId);
}
