package org.edu.modules.dorm.service.impl;

import org.edu.modules.dorm.mapper.VeBaseTeacherMapper;
import org.edu.modules.dorm.service.VeBaseTeacherService;
import org.edu.modules.dorm.vo.VeBaseTeacherVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VeBaseTeacherServiceImpl implements VeBaseTeacherService {
    @Autowired
    private VeBaseTeacherMapper veBaseTeacherMapper;

    @Override
    public List<VeBaseTeacherVo> teacherMessage() {
      return veBaseTeacherMapper.teacherMessage();
    }

}
