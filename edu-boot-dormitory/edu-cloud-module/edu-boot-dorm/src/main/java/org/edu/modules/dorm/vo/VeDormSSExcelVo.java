package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;



@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormSSExcelVo ", description = "宿舍信息导入模板")
public class VeDormSSExcelVo {



    @Excel(name = "宿舍名称", width = 15)
    @ApiModelProperty(value = "宿舍名称")
    private String fjbm;

    @Excel(name = "可住人数", width = 15)
    @ApiModelProperty(value = "已住人数")
    private Integer kzrs;

    @Excel(name = "已住人数", width = 15)
    @ApiModelProperty(value = "已住人数")
    private Integer yzrs;

    @Excel(name = "电话号码", width = 15)
    @ApiModelProperty(value = "电话号码")
    private String dhhm;

    @Excel(name = "水表截至码", width = 15)
    @ApiModelProperty(value = "水表截至码")
    private Integer sbds;

    @Excel(name = "电表截至码", width = 15)
    @ApiModelProperty(value = "电表截至码")
    private Integer dbds;

    @Excel(name = "状态", width = 15)
    @ApiModelProperty(value = "状态")
    private String state;








}
