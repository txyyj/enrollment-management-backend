package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormDisciplineType;

public interface VeDormDisciplineTypeService extends IService<VeDormDisciplineType> {

}
