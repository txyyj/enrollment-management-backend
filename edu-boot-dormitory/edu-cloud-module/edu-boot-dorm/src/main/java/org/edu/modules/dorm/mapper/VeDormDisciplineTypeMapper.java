package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.dorm.entity.VeDormDisciplineType;

public interface VeDormDisciplineTypeMapper extends BaseMapper<VeDormDisciplineType> {

}
