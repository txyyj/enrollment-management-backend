package org.edu.modules.dorm.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.dorm.entity.VeDormAsset;
import org.edu.modules.dorm.entity.VeDormSSLou;
import org.edu.modules.dorm.entity.VeFileFiles;
import org.edu.modules.dorm.mapper.VeFileFilesMapper;
import org.edu.modules.dorm.service.VeDormAssetService;
import org.edu.modules.dorm.service.VeDormSuSheAssetService;
import org.edu.modules.dorm.service.VeDormSuSheLouService;
import org.edu.modules.dorm.service.VeDormSuSheService;
import org.edu.modules.dorm.vo.VeDormSuSheLouVo;
import org.edu.modules.dorm.vo.VeDormSuSheVo;
import org.edu.modules.dorm.vo.VeSuSheAssetVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags="宿舍楼信息展示")
@RestController
@RequestMapping("dorm/suSheLou")
@Slf4j
public class VeDormSuSheLouController extends BaseController<VeDormSSLou,VeDormSuSheLouService> {
    @Autowired
    private VeDormSuSheLouService veDormSuSheLouService;
    @Autowired
    private VeDormSuSheService veDormSuSheService;
    @Autowired
    private VeDormSuSheAssetService veDormSuSheAssetService;
    @Autowired
    private VeDormAssetService veDormAssetService;
    @Autowired
    private VeFileFilesMapper veFileFilesMapper;

    /**
     * 宿舍楼信息展示
     * @return
     */
    @AutoLog(value = "宿舍楼信息展示")
    @ApiOperation(value="宿舍楼信息展示", notes = "宿舍楼信息展示")
    @PostMapping(value = "/suSheLouMessage")
    public Result<?> suSheLouMessage(){
        List<VeDormSuSheLouVo> list = veDormSuSheLouService.suSheLouMessage();
        return  Result.OK(list);
    }


    /**
     * 平铺展示各楼层信息
     * @return
     */
    @ApiOperation(value="平铺展示各楼层信息", notes="通过楼层ID查询到对应楼层的宿舍信息")
    @PostMapping(value = "/suSheMessage")
    public Result<?> suSheMessage( @ApiParam(value = "宿舍楼ID")@RequestParam(name = "sslId",required = true)Long sslId){
        List<VeDormSuSheVo> list = veDormSuSheService.suSHeMessage(sslId);
        return Result.OK(list);
    }

    /**
     * 宿舍资产展示
     * @return
     *

     */
    @ApiOperation(value="宿舍资产展示", notes = "通过宿舍ID查询到宿舍资产信息")
    @PostMapping(value = "/suSheAssetBySsId")
    public Result<?> suSheAssetBySsId(){
        List<VeSuSheAssetVo> list = veDormSuSheAssetService.suSheAsset();
        return Result.OK(list);

    }


    /**
     * 通过Id获取修改内容
     * @return
     *
     */
    @ApiOperation(value="通过Id获取修改内容", notes = "通过Id获取修改内容")
    @PostMapping(value = "/updateMessage")
    public Result<?> updateMessage( @ApiParam(value = "资产ID")@RequestParam("id")Long id){
        VeSuSheAssetVo veDormAsset = veDormSuSheAssetService.updateMessage(id);
        return  Result.OK(veDormAsset);
    }


    /**
     * 为寝室添加资产
     * @return
     */
    @AutoLog(value = "为寝室添加资产")
    @ApiOperation(value="添加资产", notes = "站点可选择模板-添加")
    @PostMapping(value = "/addAssert")
    public Result<?> addSuSheAsset( @ApiParam(value = "宿舍ID")@RequestParam("ssId") Long  ssId,
                                    @ApiParam(value = "资产名称")@RequestParam("zcName")String zcName,
                                    @ApiParam(value = "数量")@RequestParam(value = "nums",required = false)Integer nums,
                                    @ApiParam(value = "排序")@RequestParam(value ="listSort",required = false)Integer listSort,
                                    @ApiParam(value = "文件ID")@RequestParam(value ="fileId",required = false) Long fileId,
                                    @ApiParam(value = "资产类型")@RequestParam(value ="zcType",required = false)Integer zcType,
                                    @ApiParam(value = "状态")@RequestParam(value ="status",required = false)Integer status,
                                    @ApiParam(value = "备注")@RequestParam(value ="content",required = false)String content,
                                    @ApiParam(value = "图片Url")@RequestParam(value ="imageUrl",required = false)String imageUrl,
                                    @ApiParam(value = "图片大小")@RequestParam(value ="imageSize",required = false)Long imageSize,
                                    @ApiParam(value = "图片名称")@RequestParam(value ="imageName",required = false)String imageName){

      //判断是否存在
      QueryWrapper<VeDormAsset> wrapper = new QueryWrapper<>();
      wrapper.eq("ssId",ssId);
      wrapper.eq("zcName",zcName);
      List<VeDormAsset> list = veDormAssetService.list(wrapper);

      if(list.size() != 0){
        return Result.error("宿舍已经存在相同资产");
      }
      Long time = System.currentTimeMillis()/1000;
      VeFileFiles files = new VeFileFiles();
      files.setSourcefile(imageUrl);
      files.setSize(imageSize);
      files.setCreatetime(time);
      files.setName(imageName);
      veFileFilesMapper.insert(files);


        VeDormAsset veDormAsset = new VeDormAsset();
        veDormAsset.setSsId(ssId);
        veDormAsset.setZcName(zcName);
        veDormAsset.setNums(nums);
        veDormAsset.setListSort(listSort);
        veDormAsset.setZcType(zcType);
        veDormAsset.setStatus(status);
        veDormAsset.setFileId(fileId);
        veDormAsset.setContent(content);
        veDormAsset.setCreateTime(time);
        veDormAsset.setFileId(files.getId());

        //操作人
        veDormAsset.setCraeteUserId(0);
        veDormAsset.setCreateUser(0L);

        veDormAssetService.save(veDormAsset);
        return Result.OK("添加成功！");
    }

    /**
     * 资产模糊查询
     * @return
     */
    @AutoLog(value = "资产模糊查询")
    @ApiOperation(value="资产模糊查询", notes = "根据资产名称进行模糊搜索，根据资产类别、资产状态下拉框选择条件搜索")
    @PostMapping(value = "/suSheQueryByName")
    public Result<?> suSheAssetByName(@ApiParam(value = "资产名称") @RequestParam(name = "zcName") String zcName,
                                      @ApiParam(value = "资产类型") @RequestParam(name = "zcType") String zcType,
                                      @ApiParam(value = "状态") @RequestParam(name = "status") String  status) {
        //非空判断
        if("".equals(zcName)){
            zcName = null;
        }
        Integer Status = null;
//        if( !"".equals(status)){
//            Status = Integer.parseInt(status);
//        }
        if( !"".equals(status) && Integer.parseInt(status) !=0){
            Status = Integer.parseInt(status);
        }else{
            Status = null;
        }
        Integer zctype = null;
        if(!"".equals(zcType)){
            zctype = Integer.parseInt(zcType);
        }
        List<VeSuSheAssetVo> list =  veDormSuSheAssetService.suSheQueryByName( zcName, zctype, Status);
        return Result.OK(list);

    }

    /**
     * 宿舍资产模糊查询
     * @return
     */
    @AutoLog(value = "宿舍资产模糊查询")
    @ApiOperation(value="宿舍资产模糊查询", notes="根据不同宿舍，资产名称进行模糊搜索，根据资产类别、资产状态下拉框选择条件搜索")
    @PostMapping(value = "/suSheQueryBySsId")
    public Result<?> suSheAssetBySsId(
            @ApiParam(value = "宿舍ID") @RequestParam(name = "ssId")Long ssId,
            @ApiParam(value = "资产名称")@RequestParam(name = "zcName")String zcName,
            @ApiParam(value = "资产类型")@RequestParam(name = "zcType")String zcType,
            @ApiParam(value = "状态")@RequestParam(name = "status")String  status
    ){
        //非空判断
        if("".equals(zcName)){
            zcName = null;
        }
        Integer Status = null;
        if( !"".equals(status)){
            Status = Integer.parseInt(status);
        }
        Integer zctype = null;
        if(!"".equals(zcType)){
            zctype = Integer.parseInt(zcType);
        }
        List<VeSuSheAssetVo> list =  veDormSuSheAssetService.suSheQueryBySsId(ssId, zcName, zctype, Status);
        return Result.OK(list);

    }
    /**
     * 为寝室修改资产
     * @return
     */
    @AutoLog(value = "为寝室修改资产")
    @ApiOperation(value="修改资产", notes="站点可选择模板-修改")
    @PostMapping(value = "/updateAssert")
    public Result<?> updateSuSheAsset( @ApiParam(value = "资产ID")@RequestParam("id") Long id,
                                       @ApiParam(value = "资产名称")@RequestParam("zcName")String zcName,
                                       @ApiParam(value = "数量")@RequestParam("nums")Integer nums,
                                       @ApiParam(value = "排序")@RequestParam("listSort")Integer listSort,
                                       @ApiParam(value = "图片Url")@RequestParam("imageUrl")String imageUrl,
                                       @ApiParam(value = "图片大小")@RequestParam("imageSize")Long imageSize,
                                       @ApiParam(value = "图片名称")@RequestParam("imageName")String imageName,
                                       @ApiParam(value = "资产类型")@RequestParam("zcType")Integer zcType,
                                       @ApiParam(value = "状态")@RequestParam("status")Integer status,
                                       @ApiParam(value = "内容")@RequestParam("content")String  content){


      //判断是否存在
      QueryWrapper<VeDormAsset> wrapper = new QueryWrapper<>();
      wrapper.eq("ssId",veDormAssetService.getById(id).getSsId());
      wrapper.eq("zcName",zcName);
      List<VeDormAsset> list = veDormAssetService.list(wrapper);
      System.out.println(list.size());
      if(list.size()>1){
        return Result.error("宿舍已经存在相同资产");
      }
      Long time = System.currentTimeMillis()/1000;
      VeFileFiles files = new VeFileFiles();
      files.setSourcefile(imageUrl);
      files.setSize(imageSize);
      files.setCreatetime(time);
      files.setName(imageName);
      veFileFilesMapper.insert(files);
        VeDormAsset veDormAsset = new VeDormAsset();
        veDormAsset.setZcName(zcName);
        veDormAsset.setNums(nums);
        veDormAsset.setListSort(listSort);
        veDormAsset.setZcType(zcType);
        veDormAsset.setStatus(status);
        veDormAsset.setFileId(files.getId());
        veDormAsset.setId(id);
        veDormAsset.setContent(content);
        veDormAssetService.updateById(veDormAsset);
        return Result.OK("修改成功");

    }


    /**
     * 为寝室删除资产
     * @return
     */
    @AutoLog(value = "为寝室删除资产")
    @ApiOperation(value="删除资产", notes="站点可选择模板-删除")
    @PostMapping(value = "/deleteAssert")
    public Result<?> deleteSuSheAsset( @ApiParam(value = "资产ID")@RequestParam("id") Integer id){
        veDormAssetService.removeById(id);
        return Result.OK("删除成功");

    }
    /**资产类别
     * 在changgeController中
     */




}
