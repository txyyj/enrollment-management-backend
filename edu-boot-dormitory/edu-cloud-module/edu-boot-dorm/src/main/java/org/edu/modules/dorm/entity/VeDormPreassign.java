package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
@TableName("ve_dorm_student")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ve_dorm_student对象", description = "宿舍信息")
public class VeDormPreassign implements Serializable {
    private static final long serialVersionUID = 1L;

    //id
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Integer id;

    @Excel(name = "用户id", width = 15)
    @ApiModelProperty(value = "用户id")
    private Integer userid;

    @Excel(name = "学号", width = 15)
    @ApiModelProperty(value = "学号")
    private String xh;

    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value ="姓名")
    private String xm;

    @Excel(name = "校区号", width = 15)
    @ApiModelProperty(value ="校区号")
    private Integer xqh;

    @Excel(name = "宿舍楼编号", width = 15)
    @ApiModelProperty(value = "宿舍楼编号")
    private Integer sslbm;

    @Excel(name = "楼层号", width = 15)
    @ApiModelProperty(value = "楼层号")
    private Integer lch;

    @Excel(name = "房间编号", width = 15)
    @ApiModelProperty(value = "房间编号")
    private Integer fjbm;

    @Excel(name = "床位号", width = 15)
    @ApiModelProperty(value = "床位号")
    private String cwh;

    @Excel(name = "状态", width = 15)
    @ApiModelProperty(value = "状态")
    private Integer status;

    @Excel(name = "添加时间", width = 15)
    @ApiModelProperty(value = "添加时间")
    private long createtime;

    @Excel(name = "添加人员Id", width = 15)
    @ApiModelProperty(value = "添加人员Id")
    private Integer createuserid;

    @Excel(name = "添加人员", width = 15)
    @ApiModelProperty(value = "添加人员")
    private String  createusername;

    @Excel(name = "系统id", width = 15)
    @ApiModelProperty(value = "系统id")
    private Integer terminalid;

}
