package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormSuShe;
import org.edu.modules.dorm.mapper.VeDormSuSheMapper;
import org.edu.modules.dorm.service.VeDormSuSheService;
import org.edu.modules.dorm.vo.VeDormSuSheVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VeDormSuSheServiceImpl extends ServiceImpl<VeDormSuSheMapper, VeDormSuShe> implements VeDormSuSheService {
    @Resource
    private VeDormSuSheMapper veDormSuSheMapper;

    @Override
    public List<VeDormSuSheVo> suSHeMessage(Long SSLId) {
        return veDormSuSheMapper.suSheMessage(SSLId);
    }

}
