package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.dorm.entity.VeDormAssetType;

public interface VeDormAssetTypeMapper extends BaseMapper<VeDormAssetType> {

}
