package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(description = "周数据返回")
public class VeBaseWeekVo implements Serializable {

    @ApiModelProperty(value = "周数")
    private Integer week;

    @ApiModelProperty(value = "显示信息")
    private String name;

}
