package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.*;
import org.edu.modules.dorm.vo.VeBaseStudentVo;
import org.edu.modules.dorm.vo.VeDormFenPeiVo;
import org.edu.modules.dorm.vo.VeDormSuSheStudentVo;
import org.edu.modules.dorm.vo.VeDormssfpVo;

import java.util.List;

public interface VeSuSheFenPeiMapper extends BaseMapper<VeDormStudent> {

    List<VeDormFenPeiVo> selectSuSheLou(@Param("id") Integer id);

    Integer count(@Param("id") Integer id);

    //学生宿舍关系表
    List<VeDormSuSheStudentVo> selectStudent(@Param("id") Integer fjbh);

    List<VeBaseGrade> getGrade();

    List<VeBaseFaculty> getCollege();

    List<VeBaseSpecialty> getMajor(@Param("xyId") Integer id);

    List<VeBaseBanji> getBanJi(@Param("gradeId") Integer gradeId,@Param("zyId")Integer zyId,@Param("xyId") Integer xyId);

    List<VeBaseStudentVo> queryStudent(@Param("gradeId") Integer gradeId,
                                       @Param("xyId")Integer xyId,@Param("zyId") Integer zyId,@Param("bjId")Integer bjId,
                                       @Param("xh") Long xh, @Param("xm") String xm, @Param("sfzh")Long sfzh);

    //获取楼层
    List<VeDormLouceng> getLouCeng(@Param("id") Integer id);

    //获取楼层下面的宿舍
    List<VeDormSuShe> getSuShe(@Param("lcId") Integer lcId,@Param("ssId") Integer ssId);

    //楼层宿舍数量
    Integer lcConut(@Param("lcId") Integer lcId,@Param("sslId") Integer ssId);

    List<VeDormssfpVo> getSuSheInfo(@Param("id") Integer id);

    Integer emptySuShe(@Param("id") Integer id);

    VeBaseStudentVo getBaseStudent(@Param("id") Integer id);

    Integer updateYzrs(@Param("num")Integer num,@Param("id") Long id);

    Integer emptyYzrs(@Param("id") Long id);

    List<VeDormSuSheStudentVo> selectStudentMore(@Param("xqh") Long xqh,@Param("ssl") Long ssl,
                                             @Param("ss") Long ss, @Param("stu") Long stu) ;

}
