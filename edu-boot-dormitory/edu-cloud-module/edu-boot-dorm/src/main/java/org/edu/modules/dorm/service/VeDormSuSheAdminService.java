package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormSShe;
import org.edu.modules.dorm.vo.VeDormSSVo;
import org.edu.modules.dorm.vo.VeDormSSheVO;

import java.util.List;

public interface VeDormSuSheAdminService extends IService<VeDormSShe>{

    //添加宿舍
    Integer insertSuSHe(VeDormSShe veDormSShe);

    //删除宿舍
    Integer deleteSuShe(Integer id);

    //编辑宿舍
    Integer editSuShe(VeDormSShe veDormSShe);

    List<VeDormSSVo> getSuShe(Integer xqId, Integer sslId, Integer lcId);

    String excelImport(List<VeDormSSheVO> list) throws Exception;

}
