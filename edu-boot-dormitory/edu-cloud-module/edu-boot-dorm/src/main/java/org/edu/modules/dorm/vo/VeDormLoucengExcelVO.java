package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormLoucengExcelVO {

    private static final long serialVersionUID = 1L;

    @Excel(name = "校区", width = 15)
    @ApiModelProperty(value = "校区号")
    private String xqh;

    @Excel(name = "宿舍楼", width = 15)
    @ApiModelProperty(value = "宿舍楼id")
    private String jzwh;

    @Excel(name = "楼层名称", width = 15)
    @ApiModelProperty(value = "楼层名称")
    private String lcname;

    @Excel(name = "楼层代码", width = 15)
    @ApiModelProperty(value = "楼层代码")
    private String lccode;

    @Excel(name = "是否有摄像头", width = 15)
    @ApiModelProperty(value="是否有摄像头")
    private String sfsxt;


}
