package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.dorm.entity.VeBaseCampus;

public interface VeBaseCampusMapper extends BaseMapper<VeBaseCampus> {

}
