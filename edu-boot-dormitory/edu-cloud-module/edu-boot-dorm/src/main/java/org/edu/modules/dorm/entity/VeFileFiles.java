package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@TableName("ve_file_files")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeFileFiles implements Serializable {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "sha1")
    private String sha1;

    @ApiModelProperty(value = "name")
    private String name;

    @ApiModelProperty(value = "sourcefile")
    private String  sourcefile;

    @ApiModelProperty(value = "error")
    private String error;

    @ApiModelProperty(value = "ext")
    private String ext;

    @ApiModelProperty(value = "size")
    private Long size;

    @ApiModelProperty(value = "createtime")
    private Long createtime;

    @ApiModelProperty(value = "createuser")
    private Long createuser;

    @ApiModelProperty(value = "terminalid")
    private Long terminalid;

}
