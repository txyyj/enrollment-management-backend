package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormSSLou;
import org.edu.modules.dorm.vo.VeDormSuSheLouVo;

import java.util.List;

public interface VeDormSuSheLouService extends IService<VeDormSSLou> {

    List<VeDormSuSheLouVo> suSheLouMessage();

}
