package org.edu.modules.dorm.service;

import org.edu.modules.dorm.vo.VeFindStudentVo;

import java.util.List;

public interface VeFindTFStudentService {

    List<VeFindStudentVo> findXiaoQu();

    List<VeFindStudentVo> findSuSheLou(Long XQH);

    List<VeFindStudentVo> findSuSheHao(Long SSLBM, Long XQH);

    List<VeFindStudentVo> fidStudent(Long FJBM, Long SSLBM, Long XQH);

}
