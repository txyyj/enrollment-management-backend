package org.edu.modules.dorm.constant;

import com.aliyun.oss.OSSClient;

import java.util.Date;

/**李少君复制
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/7/6 14:26
 */
public class OssDownloadUtil {
    private static OssDownloadUtil ossDownloadUtil;
    private static String endpoint;
    private static String accessKeyId;
    private static String accessKeySecret;
    private static String bucketName;

    static {
        endpoint = "https://oss-cn-beijing.aliyuncs.com";
        accessKeyId = "LTAI5tPnLGtPS6r8BpncZMrm";
        accessKeySecret = "BAJbUlVLXD6or53Zcw1nC8uI87UpOs";
        bucketName = "exaplebucket-beijing";
    }

    public String downFileFromOSS(String fileName) throws Exception {
        OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        // 设置这个文件地址的有效时间
//        Date expiration = new Date(new Date().getTime() + 3600l * 1000 * 24 * 365 * 10);
        Date expiration = new Date(new Date().getTime() + 3600l * 1000 * 24);
        //生成下载文件的url地址
        String url = ossClient.generatePresignedUrl(bucketName, fileName, expiration).toString();
        ossClient.shutdown();
        return url;
    }
    public static OssDownloadUtil getOssDownloadUtil (){
        if (ossDownloadUtil==null){
            synchronized (OssDownloadUtil.class){
                if (ossDownloadUtil==null){
                    ossDownloadUtil=new OssDownloadUtil();
                }
            }
        }
        return ossDownloadUtil;
    }
}
