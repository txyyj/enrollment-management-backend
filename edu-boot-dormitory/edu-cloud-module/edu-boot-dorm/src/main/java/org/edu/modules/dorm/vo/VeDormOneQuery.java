package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 返回编辑消息
 * @Author:
 * @Date:   2021-03-05
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormOneQuery implements Serializable {
    
    /**id*/
    private Long id;
    
    /**姓名*/
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private String XM;

    /**身份证号*/
    @Excel(name = "身份证号", width = 15)
    @ApiModelProperty(value = "身份证号")
    private String SFZH;

    /**探访事由*/
    @Excel(name = "探访事由", width = 200)
    @ApiModelProperty(value = "探访事由")
    private String TFSY;

    /**探访类型（数据字典）*/
    @Excel(name = "探访类型", width = 15)
    @ApiModelProperty(value = "探访类型")
    private Long TFLX;

    /**宿舍楼编号*/
    @Excel(name = "宿舍楼编号", width = 15)
    @ApiModelProperty("宿舍楼编号")
    private Long SSLBH;

    /**房间编号*/
    @Excel(name = "房间编号", width = 15)
    @ApiModelProperty("房间编号")
    private Long FJBM;

    /**校区编号*/
    @Excel(name = "校区编号", width = 15)
    @ApiModelProperty("校区编号")
    private Long XQBH;

    /**被探访人名字*/
    @Excel(name = "被探访人", width = 15)
    @ApiModelProperty("被探访人")
    private String userName;

}
