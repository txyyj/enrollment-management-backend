package org.edu.modules.dorm.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormSSLou;
import org.edu.modules.dorm.entity.VeDormStudent;
import org.edu.modules.dorm.entity.VeDormSuShe;
import org.edu.modules.dorm.entity.VeFileFiles;
import org.edu.modules.dorm.entity.bo.BasicResponseBO;
import org.edu.modules.dorm.entity.bo.VeCampusBO;
import org.edu.modules.dorm.mapper.VeDormSSLouMapper;
import org.edu.modules.dorm.mapper.VeDormStudentMapper;
import org.edu.modules.dorm.mapper.VeDormSuSheMapper;
import org.edu.modules.dorm.mapper.VeFileFilesMapper;
import org.edu.modules.dorm.service.VeBaseDormManageService;
import org.edu.modules.dorm.service.VeDormSSLouService;
import org.edu.modules.dorm.service.VeFindTFStudentService;
import org.edu.modules.dorm.vo.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.*;

@Service

public class VeDormSSLouServiceImpl extends ServiceImpl<VeDormSSLouMapper, VeDormSSLou> implements VeDormSSLouService {
    @Resource
    private VeDormSSLouMapper veDormSSLouMapper;
    @Resource
    private VeDormSuSheMapper veDormSuSheMapper ;
    @Resource
    private VeFileFilesMapper veFileFilesMapper;
    @Autowired
    private VeDormStudentMapper veDormStudentMapper ;
    @Autowired
    private VeFindTFStudentService mVeFindTFStudentService ;
    @Resource
    private VeBaseDormManageService veBaseDormManageService;

    @Override
    public Integer addSuSheLou(VeDormSSLou veDormSuSheLou) {
        return veDormSSLouMapper.insert(veDormSuSheLou);
    }

    @Override
    public List<VeDormSSLou> getList(String name) {
        return null;
    }

    @Override
    public List<VeDormSSLouVo> selectByParam(String ssl) {
        return veDormSSLouMapper.selectByParam(ssl);
    }

    @Override
    public Integer deleteSSL(Integer id) {
        return veDormSSLouMapper.deleteById(id);
    }

    @Override
    public List<VeDormSSLou> getSuSheLou() {
        return veDormSSLouMapper.selectList(null);
    }

    @Override
    public List<VeDormSSLouVo> exportList(String ssl) {
        return null;
    }

    @Override
    public List<VeDormSSLou> getPageList(QueryWrapper<VeDormSSLouVo> queryWrapper) {
        return null;
    }

    @Override
    public VeDormWeiJiImageVo getEditInfo(Long id) {
        return veDormSSLouMapper.getEditInfo(id);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer addSSL(VeDormSSLou veDormSSLou, VeFileFiles veFileFiles) {
        veFileFilesMapper.insert(veFileFiles);
        veDormSSLou.setFileid(veFileFiles.getId());

        return veDormSSLouMapper.insert(veDormSSLou);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer editSSL(VeDormSSLou veDormSSLou, VeFileFiles veFileFiles) {
        veFileFilesMapper.insert(veFileFiles);
        veDormSSLou.setFileid(veFileFiles.getId());

        return veDormSSLouMapper.updateById(veDormSSLou);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String replaceBed(List<VeReplaceBedVo> veReplaceBedVos) {
        VeReplaceBedVo veReplaceBedVo1 = veReplaceBedVos.get(0);
        VeReplaceBedVo veReplaceBedVo2 = veReplaceBedVos.get(1);
        //1.先对比是否都是同性别宿舍
        VeDormSuShe shShe1 = veDormSuSheMapper.selectById(veReplaceBedVo1.getSSBH());
        VeDormSuShe shShe2 = veDormSuSheMapper.selectById(veReplaceBedVo2.getSSBH());
        if (shShe1.getRZXB().equals(shShe2.getRZXB())) {
            // 查询学生宿舍对应关系
            VeDormStudent stu1 = veDormStudentMapper.selectById(veReplaceBedVo1.getStuId());
            VeDormStudent stu2 = veDormStudentMapper.selectById(veReplaceBedVo2.getStuId());
            // 调换学生信息
            Long time = System.currentTimeMillis()/1000;
            VeDormStudent change = new VeDormStudent() ;
            change.setId(stu1.getId()) ;
            change.setRZSJ(Integer.parseInt(time+"")) ;
            change.setUserId(Long.parseLong(veReplaceBedVo2.getStuId())) ;
            change.setXM(stu2.getXM()) ;
            change.setXH(stu2.getXH()) ;
            veDormStudentMapper.updateById(change) ;
            change = new VeDormStudent() ;
            change.setId(stu2.getId()) ;
            change.setRZSJ(Integer.parseInt(time+"")) ;
            change.setUserId(Long.parseLong(veReplaceBedVo1.getStuId())) ;
            change.setXM(stu1.getXM()) ;
            change.setXH(stu1.getXH()) ;
            veDormStudentMapper.updateById(change) ;
            return "success";
        } else {
            return "非同性别宿舍，无法调换！";
        }
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String replaceSushe(List<VeReplaceBedVo> veReplaceBedVos) {
        VeReplaceBedVo veReplaceBedVo1 = veReplaceBedVos.get(0);
        VeReplaceBedVo veReplaceBedVo2 = veReplaceBedVos.get(1);
        VeDormSuShe shShe1 = veDormSuSheMapper.selectById(veReplaceBedVo1.getSSBH());
        VeDormSuShe shShe2 = veDormSuSheMapper.selectById(veReplaceBedVo2.getSSBH());
        Long id1 = shShe1.getId() ;
//        String fjbm = shShe1.getFJBM() ;
        if(Objects.nonNull(shShe2) && Objects.nonNull(shShe1)){
            if( shShe1.getRZXB().equals(shShe2.getRZXB())){
                shShe1.setId(shShe2.getId()) ;
//                shShe1.setFJBM(shShe2.getFJBM()) ;
                veDormSuSheMapper.updateById(shShe1) ;
                shShe2.setId(id1);
                veDormSuSheMapper.updateById(shShe2) ;
                return "success" ;
            }
            return "非同性别宿舍，无法调换！" ;
        }
        return "参数有误！" ;
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public void excelImport(List<VeDOrmSSLouExcelVo> list) throws Exception {
        // 获取所有校区信息
//        List<VeFindStudentVo> xiaoqu =  mVeFindTFStudentService.findXiaoQu() ;李少君注释
        BasicResponseBO<List> bo = veBaseDormManageService.getCampusMessage(); //李少君
        List<VeCampusBO> result = JSONObject.parseArray(JSON.toJSONString(bo.getResult()),VeCampusBO.class);
        List<VeFindStudentVo> xiaoqu = new ArrayList<>();
        for(int i =0 ;i<result.size();i++){
            VeFindStudentVo c = new VeFindStudentVo();
            c.setId(result.get(i).getId());
            c.setName(result.get(i).getXqmc());
            xiaoqu.add(c);
        }
        Map<String,Long> map = new HashMap<>() ;
        xiaoqu.forEach(l-> map.put(l.getName(),l.getId()));

        // 字典
        Map<String,Integer> dictMap1 = new HashMap<>() ;
        dictMap1.put("混住宿舍楼",0) ;
        dictMap1.put("男生宿舍楼",1) ;
        dictMap1.put("女生宿舍楼",2) ;
        Map<String,Integer> dictMap2 = new HashMap<>() ;
        dictMap2.put("正常使用",0) ;
        dictMap2.put("维修",1) ;
        dictMap2.put("清空",2) ;



        int t = 4 ;
        try{
            for ( VeDOrmSSLouExcelVo ve : list) {
                VeDormSSLou dorm = new VeDormSSLou() ;
                BeanUtils.copyProperties(ve,dorm);
                // 获取校区信息


                Long xiaoquId = map.get(ve.getXqmc()) ;
                // 查询某某校区下的建筑物是否已存在
                QueryWrapper<VeDormSSLou> wrapper = new QueryWrapper<>() ;
                wrapper.eq("xqh",xiaoquId) ;
                wrapper.and(qw->qw.eq("jzwh", ve.getJzwh()).or().eq("jzwmc",ve.getJzwmc())) ;
                List<VeDormSSLou> dorms =  veDormSSLouMapper.selectList(wrapper) ;
                // 数据已存在，跳过
                if( CollectionUtils.isEmpty(dorms)){
                    if( ve.getXb() != null ){
                        dorm.setXbm(dictMap1.getOrDefault(ve.getXb(),0)) ;
                    }
                    if( ve.getState() != null ){
                        dorm.setStatus(dictMap2.getOrDefault(ve.getState(),0)) ;
                    }
                    if( ve.getJzwdz() == null ){
                        dorm.setJzwdz("") ;
                    }
                    dorm.setXqh(xiaoquId).setTerminalid(1).setListsort(ve.getListSort()) ;
                    veDormSSLouMapper.insert(dorm) ;
                }
                t++ ;

            }
        }catch (Exception e){
            throw new Exception("第"+t+"行数据有误！") ;
        }
    }

}
