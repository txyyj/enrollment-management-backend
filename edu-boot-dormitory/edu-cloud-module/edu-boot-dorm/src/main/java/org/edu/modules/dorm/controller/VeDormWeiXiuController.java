package org.edu.modules.dorm.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.dorm.entity.VeDormWeiXiu;
import org.edu.modules.dorm.service.VeDormWeiXiuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "宿舍维修管理")
@RestController
@RequestMapping("dorm/weixiu")
@Slf4j
public class VeDormWeiXiuController extends BaseController<VeDormWeiXiu, VeDormWeiXiuService> {
    @Autowired
    private VeDormWeiXiuService veDormWeiXiuService;

    @AutoLog(value = "维修信息弹窗-宿舍维修管理")
    @ApiOperation(value = "维修信息弹窗", notes = "维修信息弹窗-宿舍维修管理")
    @PostMapping("/weiXiuInfo")
    public Result<?> weiInfo(@ApiParam("宿舍id") @RequestParam(value = "ssId") Integer ssId,
                             @ApiParam("维修信息") @RequestParam(value = "title",required = false) String title) {
        return Result.OK(veDormWeiXiuService.weiXiuInfo(ssId,title));
    }

    @AutoLog(value = "添加维修信息-宿舍维修管理")
    @ApiOperation(value = "添加维修信息", notes = "添加维修信息-宿舍维修管理")
    @PostMapping("/addWeiXiu")
    public Result<?> addWeiXiu(@ApiParam("宿舍id") @RequestParam(value = "ssId") Integer ssId,
                               @ApiParam("维修信息") @RequestParam(value = "title") String title,
                               @ApiParam("维修内容") @RequestParam(value = "content") String content,
                               @ApiParam("维修时间") @RequestParam(value = "addTime") Integer addTime,
                               @ApiParam("创建人") @RequestParam(value = "createUserId") Integer createUserId,
                               @ApiParam("创建时间") @RequestParam(value = "createTime") Long createTime) {
        VeDormWeiXiu weiXiu = new VeDormWeiXiu();
        weiXiu.setSsid(ssId);
        weiXiu.setTitle(title);
        weiXiu.setContent(content);
        weiXiu.setAddtime(addTime);
        weiXiu.setCreatetime(createTime);
        weiXiu.setCreateuserid(createUserId);
        veDormWeiXiuService.save(weiXiu);
        return Result.OK("添加成功！");
    }

    @AutoLog(value = "维修信息表格-宿舍维修管理")
    @ApiOperation(value = "维修信息表格", notes = "维修信息表格-宿舍维修管理")
    @PostMapping("/weiXiuTable")
    public Result<?> weiXiuTable(@ApiParam("维修信息") @RequestParam(value = "title") String title) {
        return Result.OK(veDormWeiXiuService.weiXiuInfoTable(title));
    }

    @AutoLog(value = "编辑-宿舍维修管理")
    @ApiOperation(value = "编辑", notes = "编辑-宿舍维修管理")
    @PostMapping("/editWeiXiu")
    public Result<?> editWeiXiu(@ApiParam("id") @RequestParam(value = "id") Integer id,
                                @ApiParam("宿舍id") @RequestParam(value = "ssId") Integer ssId,
                                @ApiParam("维修信息") @RequestParam(value = "title") String title,
                                @ApiParam("维修内容") @RequestParam(value = "content") String content,
                                @ApiParam("创建时间") @RequestParam(value = "addTime") Integer addTime,
                                @ApiParam("创建人") @RequestParam(value = "createUserId") Integer createUserId,
                                @ApiParam("创建时间") @RequestParam(value = "createTime") Long createTime) {
        VeDormWeiXiu weiXiu = new VeDormWeiXiu();
        weiXiu.setId(id);
        weiXiu.setSsid(ssId);
        weiXiu.setTitle(title);
        weiXiu.setContent(content);
        weiXiu.setAddtime(addTime);
        weiXiu.setCreatetime(createTime);
        weiXiu.setCreateuserid(createUserId);

        veDormWeiXiuService.updateById(weiXiu);
        return Result.OK("编辑成功！");
    }

    @AutoLog(value = "删除维修信息-宿舍维修管理")
    @ApiOperation(value = "删除维修信息", notes = "删除维修信息-宿舍维修管理")
    @PostMapping("/deleteWeiXiu")
    public Result<?> deleteWeiXiu(@ApiParam("id") @RequestParam(value = "id") Integer id) {
        veDormWeiXiuService.removeById(id);
        return Result.OK("删除成功！");
    }

    @ApiOperation(value = "获取便捷信息", notes = "获取便捷信息-宿舍维修管理")
    @PostMapping("/getEditInfo")
    public Result<?> getEditInfo(@ApiParam("id") @RequestParam(value = "id") Integer id) {
        return Result.OK(veDormWeiXiuService.getById(id));
    }

}
