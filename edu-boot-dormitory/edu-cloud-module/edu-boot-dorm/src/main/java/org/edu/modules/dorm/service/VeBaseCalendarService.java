package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeBaseCalendar;

import java.util.List;

public interface VeBaseCalendarService extends IService<VeBaseCalendar> {
    //查周数
    List<VeBaseCalendar> getCalendar(Long semester);
}
