package org.edu.modules.dorm.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.dorm.entity.VeDormStudent;
import org.edu.modules.dorm.entity.VeDormWeiJi;
import org.edu.modules.dorm.service.VeDormStudentService;
import org.edu.modules.dorm.service.VeDormWeiJiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;

@Api(tags="违纪学生情况管理")
@RestController
@RequestMapping("dorm/ruleMng")
@Slf4j
@ApiSort(value = 100)
public class VeDormWeiJiController extends BaseController<VeDormWeiJi, VeDormWeiJiService>{
    @Autowired
    private VeDormWeiJiService veDormWeiJiService;
    @Autowired
    private VeDormStudentService veDormStudentService;

    @AutoLog(value = "添加违纪-违纪管理")
    @ApiOperation(value="添加违纪", notes = "添加违纪-违纪管理")
    @PostMapping(value = "/addDiscipline")
    public Result<?>addDiscipline(@ApiParam(value = "用户id") @RequestParam(value = "userId") Long userId,
                                  @ApiParam(value = "违纪类型") @RequestParam(value = "wjlx") Long wjlx,
                                  @ApiParam(value = "违纪时间") @RequestParam(value = "wjsj") Long wjsj,
                                  @ApiParam(value = "违纪说明") @RequestParam(value = "wjsm") String wjsm) {
        QueryWrapper<VeDormStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("id", userId);
        VeDormStudent student = veDormStudentService.getOne(wrapper);

//        //获取登记人
//        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        VeDormWeiJi veDormWeiJi = new VeDormWeiJi();
        veDormWeiJi.setUserid(userId);
        veDormWeiJi.setXh(student.getXH());
        veDormWeiJi.setWjlx(wjlx);
        veDormWeiJi.setXm(student.getXM());
        veDormWeiJi.setWjsj(wjsj);
        veDormWeiJi.setWjsm(wjsm);
        veDormWeiJi.setDjr(0L);
        veDormWeiJi.setDjsj(System.currentTimeMillis() / 1000);

        veDormWeiJiService.save(veDormWeiJi);
        return Result.OK("添加成功！");
    }

    @AutoLog(value = "违纪情况编辑-违纪管理")
    @ApiOperation(value="违纪情况编辑", notes = "违纪情况编辑-违纪管理")
    @PostMapping(value = "/editDiscipline")
    public Result<?>editDiscipline(@ApiParam(value = "违纪id") @RequestParam(value = "id") Long id,
                                   @ApiParam(value = "违纪类型") @RequestParam(value = "wjlx") Long wjlx,
                                   @ApiParam(value = "违纪时间") @RequestParam(value = "wjsj") Long wjsj,
                                   @ApiParam(value = "违纪说明") @RequestParam(value = "wjsm") String  wjsm) {
//        //获取登记人
//        LoginUser sysUser = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        VeDormWeiJi veDormWeiJi = new VeDormWeiJi();
        veDormWeiJi.setId(id);
        veDormWeiJi.setWjlx(wjlx);
        veDormWeiJi.setWjsj(wjsj);
        veDormWeiJi.setWjsm(wjsm);
        veDormWeiJi.setDjr(0L);
        veDormWeiJi.setDjsj(System.currentTimeMillis() / 1000);

        veDormWeiJiService.updateById(veDormWeiJi);
        return Result.OK("修改成功！");
    }

    @AutoLog(value = "违纪删除-违纪管理")
    @ApiOperation(value="违纪删除", notes = "违纪删除-违纪管理")
    @PostMapping(value = "/deleteDiscipline")
    public Result<?>deleteDiscipline(@ApiParam(value = "违纪id") @RequestParam(value = "id") Integer id) {
        veDormWeiJiService.removeById(id);
        return Result.OK("删除成功！");
    }

    @AutoLog(value = "违纪列表-违纪管理")
    @ApiOperation(value="违纪列表", notes = "违纪列表-违纪管理")
    @PostMapping(value = "/disciplinedTable")
    public Result<?> getTable(@ApiParam(value = "姓名") @RequestParam(value ="name", required = false) String name,
                              @ApiParam(value = "学号") @RequestParam(value = "xh", required = false) String xh) {
        return Result.OK(veDormWeiJiService.weiJiTable(name, xh));
    }

    @AutoLog(value = "批量删除-违纪管理")
    @ApiOperation(value="批量删除", notes = "批量删除-违纪管理")
    @PostMapping(value = "/deleteMany")
    public Result<?> deleteMany(@ApiParam(value = "违纪ids") @RequestParam(value = "id") String ids) {
        veDormWeiJiService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功！");
    }

    @ApiOperation(value="获取便捷信息", notes = "获取编辑信息-违纪管理")
    @PostMapping(value = "/getEdit")
    public Result<?> getEdit(@ApiParam(value = "违纪id") @RequestParam(value ="id") Long id) {
       return Result.OK(veDormWeiJiService.getById(id));
    }

}
