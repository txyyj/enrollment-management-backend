package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeStudentInfoVo {

    @TableId(value = "id")
    @ApiModelProperty(value ="id")
    private Integer id;

    @ApiModelProperty(value ="xh")
    private Integer xh;

    @ApiModelProperty(value ="xm")
    private String xm;

}
