package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeBaseCalendar;

import java.util.List;

public interface VeBaseCalendarMapper extends BaseMapper<VeBaseCalendar> {
    //查周数
    List<VeBaseCalendar> getCalendar(@Param("semester") Long semester);
}
