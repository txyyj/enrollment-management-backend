package org.edu.modules.dorm.service;

import org.edu.modules.dorm.vo.VeBaseTeacherVo;

import java.util.List;

public interface VeBaseTeacherService {

  List<VeBaseTeacherVo> teacherMessage();

}
