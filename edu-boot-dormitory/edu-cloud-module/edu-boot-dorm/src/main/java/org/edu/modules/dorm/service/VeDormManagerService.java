package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormManager;
import org.edu.modules.dorm.vo.VeDormManagerVo;

import java.util.List;

public interface VeDormManagerService extends IService<VeDormManager> {

    Integer addMng(VeDormManager veDormManager);


}
