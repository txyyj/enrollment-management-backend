package org.edu.modules.dorm.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormAsset;
import org.edu.modules.dorm.vo.VeDormAssetVo;

import java.util.List;

public interface VeDormAssetMapper extends BaseMapper<VeDormAsset> {

    List<VeDormAssetVo> fuzzyQuery(@Param("zcName")String zcName,
                                   @Param("zcType")Long zcType,
                                   @Param("status")Integer status);


}
