package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.dorm.entity.VeDormStudent;
import org.springframework.stereotype.Repository;

@Repository
public interface VeDormStudentMapper extends BaseMapper<VeDormStudent> {

}
