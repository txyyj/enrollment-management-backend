package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
@TableName("ve_oa_car_baoxian")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeBaseBanji implements Serializable {

    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    //专业代码
    @Excel(name = "行政班代码", width = 15)
    @ApiModelProperty(value = "行政班代码")
    @TableField(value = "XZBDM")
    private String xzbdm;

    //专业名称
    @Excel(name = "行政班名称", width = 15)
    @ApiModelProperty(value = "行政班名称")
    @TableField(value="XZBMC")
    private String  xzbmc;

}
