package org.edu.modules.dorm.mapper;

import org.edu.modules.dorm.vo.VeBaseTeacherVo;

import java.util.List;

public interface VeBaseTeacherMapper {
  List<VeBaseTeacherVo> teacherMessage();
}
