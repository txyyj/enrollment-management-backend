package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormSuShe;
import org.edu.modules.dorm.vo.VeDormCheckVo;
import org.edu.modules.dorm.vo.VeDormSuSheTJVo;

import java.util.List;

public interface VeDormSuSheTJService extends IService<VeDormSuShe> {

    /**
     * 1、宿舍容量统计。
     */
    List<VeDormSuSheTJVo> selectDormList();

    /**
     * 2、宿舍学生统计。
     */
    List<VeDormSuSheTJVo> selectStudentList();

    /**
     * 3、考勤统计。（通过考勤类型id、宿舍楼id、院系id、专业id、班级id精确查询，房间编号模糊查询）
     * @param checkRuleId 考勤类型id
     * @param sslId 宿舍楼id
     * @param facultyId 院系id
     * @param specialtyId 专业id
     * @param banjiId 班级id
     * @param FJBM 房间编号
     */
    List<VeDormCheckVo> selectCheckList(Long checkRuleId, Long sslId, Long facultyId, Long specialtyId,
                                        Long banjiId, String FJBM);

    /**
     * 4、违纪统计。（通过违纪类型id、宿舍楼id、院系id、专业id、班级id精确查询，房间编号模糊查询）
     * @param disciplineTypeId 违纪类型id
     * @param sslId 宿舍楼id
     * @param facultyId 院系id
     * @param specialtyId 专业id
     * @param banjiId 班级id
     * @param FJBM 房间编号
     */
    List<VeDormCheckVo> selectDisciplineList(Long disciplineTypeId, Long sslId, Long facultyId, Long specialtyId,
                                             Long banjiId, String FJBM);

}
