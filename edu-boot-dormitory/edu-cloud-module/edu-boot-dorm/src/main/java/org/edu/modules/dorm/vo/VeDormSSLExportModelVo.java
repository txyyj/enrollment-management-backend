package org.edu.modules.dorm.vo;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormSSLExportModelVo {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Integer id;

    @Excel(name = "建筑物号", width = 15)
    @ApiModelProperty(value = "建筑物号")
    private String jzwh;

    @Excel(name = "建筑物名称", width = 15)
    @ApiModelProperty(value = "建筑物名称")
    private String jzwmc;

    @Excel(name = "校区号", width = 15)
    @ApiModelProperty(value ="校区号")
    private Long xqh;

    @ApiModelProperty(value ="性别码")
    private Integer xbm;

    @Excel(name = "建筑物层数", width = 15)
    @ApiModelProperty(value ="建筑物层数")
    private Integer jzwcs;


    @Excel(name ="总建筑面积", width = 15)
    @ApiModelProperty(value ="总建筑面积")
    private Integer zjzmj;

    @Excel(name ="总使用面积", width = 15)
    @ApiModelProperty(value ="总使用面积")
    private  Integer zsymj;

    @Excel(name ="建筑物地址", width = 15)
    @ApiModelProperty(value ="建筑物地址")
    private String jzwdz;

    @Excel(name ="建筑物状况码", width = 15)
    @ApiModelProperty(value ="建筑物状况码")

    private String  jzwzkm;

    @Excel(name ="建筑物图片", width = 15)
    @ApiModelProperty(value="建筑物图片")
    private Integer jzwtp;

    @Excel(name ="建筑物平面图", width = 15)
    @ApiModelProperty(value="建筑物平面图")
    private Integer jzwpmt;

    @Excel(name ="排序", width = 15)
    @ApiModelProperty(value ="排序")
    private Integer listsort;


    @ApiModelProperty(value ="状态")
    private Integer status;

    @ApiModelProperty(value ="系统Id")
    private Integer terminalid;

    @ApiModelProperty(value ="宿舍楼图片Id")
    private Long fileid;
















}
