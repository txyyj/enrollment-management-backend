package org.edu.modules.dorm.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.dorm.entity.VeDormWeiJi;
import org.edu.modules.dorm.service.VeDormDisciplineService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags="违纪管理")
@RestController
@RequestMapping("dorm/discipline")
@Slf4j
public class VeDormDisciplineController extends BaseController<VeDormWeiJi, VeDormDisciplineService> {
    @Autowired(required = false)
    private VeDormDisciplineService veDormDisciplineService;

    /**
     * 1、添加违纪。
     * @param stuId 学生id
     * @param XH 学号
     * @param XM 姓名
     * @param typeId 违纪类型id
     * @param wjTime 违纪日期
     * 未包含参数 createUserId 记录人用户id
     * 未包含参数 terminalId 终端id
     */
    @AutoLog(value = "站点可选择模板-添加违纪")
    @ApiOperation(value="添加违纪", notes="站点可选择模板-添加违纪")
    @PostMapping(value = "/insert")
    public Result<?> insert(@ApiParam(value = "学生id") @RequestParam(name = "stuId") Long stuId,
                            @ApiParam(value = "学号") @RequestParam(name = "XH") String XH,
                            @ApiParam(value = "姓名") @RequestParam(name = "XM") String XM,
                            @ApiParam(value = "违纪类型id") @RequestParam(name = "typeId") Long typeId,
                            @ApiParam(value = "违纪日期") @RequestParam(name = "wjTime") Long wjTime) {
        VeDormWeiJi veDormWeiJi = new VeDormWeiJi();
        veDormWeiJi.setUserid(stuId);
        veDormWeiJi.setXh(XH);
        veDormWeiJi.setXm(XM);
        veDormWeiJi.setWjlx(typeId);
        veDormWeiJi.setWjsj(wjTime);
        Long createTime = System.currentTimeMillis() / 1000;   //登记时间。
        veDormWeiJi.setDjsj(createTime);

        veDormWeiJi.setWjsm("无");   //临时违纪说明。
        veDormWeiJi.setDjr(0L);   //临时登记人id。

        boolean isInsert = veDormDisciplineService.save(veDormWeiJi);
        if(isInsert) {
            return Result.OK("添加成功！");
        } else {
            return Result.error("添加失败！");
        }
    }

}
