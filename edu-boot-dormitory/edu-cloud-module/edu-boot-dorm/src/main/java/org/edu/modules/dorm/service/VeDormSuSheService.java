package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormSuShe;
import org.edu.modules.dorm.vo.VeDormSuSheVo;

import java.util.List;

public interface VeDormSuSheService extends IService<VeDormSuShe> {

    List<VeDormSuSheVo> suSHeMessage(Long SSLId);

}
