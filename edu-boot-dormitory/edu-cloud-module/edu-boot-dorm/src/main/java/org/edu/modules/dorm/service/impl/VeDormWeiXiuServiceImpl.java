package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormWeiXiu;
import org.edu.modules.dorm.mapper.VeDormWeiXiuMapper;
import org.edu.modules.dorm.service.VeDormWeiXiuService;
import org.edu.modules.dorm.vo.VeDormWeiXiuVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VeDormWeiXiuServiceImpl extends ServiceImpl<VeDormWeiXiuMapper,VeDormWeiXiu> implements VeDormWeiXiuService {
    @Autowired
    private VeDormWeiXiuMapper veDormWeiXiuMapper;

    @Override
    public List<VeDormWeiXiuVo> weiXiuInfo(Integer ssId,String title) {
        return veDormWeiXiuMapper.weiXiuInfo(ssId,title);
    }

    @Override
    public List<VeDormWeiXiuVo> weiXiuInfoTable(String title) {
        return veDormWeiXiuMapper.weiXiuInfoTable(title);
    }

}
