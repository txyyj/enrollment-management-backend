package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormSSheVO implements Serializable {

    private static final long serialVersionUID = 1L;

    @Excel(name = "校区", width = 15)
    @ApiModelProperty(value = "校区号")
    private String xqh;

    @Excel(name = "宿舍楼", width = 15)
    @ApiModelProperty(value = "宿舍楼编号")
    private String jzwh;

    @Excel(name = "楼层", width = 15)
    @ApiModelProperty(value = "楼层号")
    private String lch;

    @Excel(name = "房间编号", width = 15)
    @ApiModelProperty(value = "房间编号")
    private String fjbm;

    @Excel(name = "入住性别", width = 15)
    @ApiModelProperty(value = "入住性别")
    private String rzxb;

    @Excel(name = "可住人数", width = 15)
    @ApiModelProperty(value = "已住人数")
    private Integer kzrs;

    @Excel(name = "已住人数", width = 15)
    @ApiModelProperty(value = "已住人数")
    private Integer yzrs;

    @Excel(name = "网络端口", width = 15)
    @ApiModelProperty(value = "网络端口")
    private String wldk;


    @Excel(name = "电话号码", width = 15)
    @ApiModelProperty(value = "电话号码")
    private String  dhhm;

    @Excel(name = "水表底数", width = 15)
    @ApiModelProperty(value = "水表底数")
    private Integer sbds;

    @Excel(name = "电表底数", width = 15)
    @ApiModelProperty(value = "电表底数")
    private Integer dbds;

    @Excel(name = "宿舍备注", width = 15)
    @ApiModelProperty(value = "宿舍备注")
    private String ssbz;

}
