package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 宿舍资产
 * @Author:
 * @Date:   2021-03-02
 * @Version: V1.0
 */

@Data
@TableName("ve_dorm_asset")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormAsset implements Serializable {
    
    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /**资产名称*/
    @Excel(name = "资产名称", width = 15)
    @ApiModelProperty(value = "资产名称")
    @TableField(value = "zcName")
    private String zcName;

    /**宿舍id*/
    @Excel(name = "宿舍id", width = 15)
    @ApiModelProperty(value = "宿舍id")
    @TableField(value = "ssId")
    private Long ssId;

    /**资产个数*/
    @Excel(name = "资产个数", width = 15)
    @ApiModelProperty(value = "资产个数")
    @TableField(value = "nums")
    private Integer nums;

    /**描述说明*/
    @Excel(name = "描述说明", width = 15)
    @ApiModelProperty(value = "描述说明")
    @TableField(value = "content")
    private String content;

    /**文件id*/
    @Excel(name = "文件id", width = 15)
    @ApiModelProperty(value = "文件id")
    @TableField(value = "fileId")
    private Long fileId;

    /**用户id*/
    @Excel(name = "用户id", width = 15)
    @ApiModelProperty(value = "用户id")
    @TableField(value = "craeteUserId")
    private Integer craeteUserId;

    /**用户姓名*/
    @Excel(name = "用户姓名", width = 15)
    @ApiModelProperty(value = "用户姓名")
    @TableField(value = "createUser")
    private Long createUser;

    /**创建时间*/
    @Excel(name = "创建时间", width = 15)
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "createTime")
    private Long createTime;

    /**排序*/
    @Excel(name = "排序", width = 15)
    @ApiModelProperty(value = "排序")
    @TableField(value = "listSort")
    private Integer listSort;

    /**资产类别*/
    @Excel(name = "资产类别", width = 15)
    @ApiModelProperty(value = "资产类别")
    @TableField(value = "zcType")
    private Integer zcType;

    /**资产状态*/
    @Excel(name = "资产状态", width = 15)
    @ApiModelProperty(value = "资产状态")
    @TableField(value = "status")
    private Integer status;

    /**终端id*/
    @Excel(name = "终端id", width = 15)
    @ApiModelProperty(value = "终端id")
    @TableField(value = "terminalId")
    private Integer terminalId;

}
