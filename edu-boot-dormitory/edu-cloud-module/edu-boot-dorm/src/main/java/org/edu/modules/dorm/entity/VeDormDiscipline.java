package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 违纪管理
 * @Author:
 * @Date:   2021-03-09
 * @Version: V1.0
 */

@Data
@TableName("ve_dorm_discipline")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormDiscipline对象", description = "违纪管理")
public class VeDormDiscipline implements Serializable {

    /** （1）自增id。 */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "自增id")
    private Long id;

    /** （2）学生id。 */
    @Excel(name = "学生id", width = 15)
    @ApiModelProperty(value = "学生id")
    @TableField(value = "stuId")
    private Long stuId;

    /** （3）宿舍楼id。 */
    @Excel(name = "宿舍楼id", width = 15)
    @ApiModelProperty(value = "宿舍楼id")
    @TableField(value = "sslId")
    private Long sslId;

    /** （4）违纪类型id。 */
    @Excel(name = "违纪类型id", width = 15)
    @ApiModelProperty(value = "违纪类型id")
    @TableField(value = "typeId")
    private Long typeId;

    /** （5）违纪日期。 */
    @Excel(name = "违纪日期", width = 15)
    @ApiModelProperty(value = "违纪日期")
    @TableField(value = "wjTime")
    private String wjTime;

    /** （6）记录时间。 */
    @Excel(name = "记录时间", width = 15)
    @ApiModelProperty(value = "记录时间")
    @TableField(value = "createTime")
    private Long createTime;

    /** （7）记录人用户id。 */
    @Excel(name = "记录人用户id", width = 15)
    @ApiModelProperty(value = "记录人用户id")
    @TableField(value = "createUserId")
    private Long createUserId;

    /** （8）终端id。 */
    @Excel(name = "终端id", width = 15)
    @ApiModelProperty(value = "终端id")
    @TableField(value = "terminalId")
    private Long terminalId;

}
