package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormManager;
import org.edu.modules.dorm.vo.VeDormManagerVo;

import java.util.List;

public interface VeDormManagerMapper extends BaseMapper<VeDormManager> {

}
