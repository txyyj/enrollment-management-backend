package org.edu.modules.dorm.service.impl;

import org.edu.modules.dorm.mapper.VeFindTFStudentMapper;
import org.edu.modules.dorm.service.VeFindTFStudentService;
import org.edu.modules.dorm.vo.VeFindStudentVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VeFindTFStudentServiceImpl implements VeFindTFStudentService {
    @Resource
    private VeFindTFStudentMapper veFindTFStudentMapper;

    @Override
    public List<VeFindStudentVo> findXiaoQu() {
        return veFindTFStudentMapper.findXiaoQu();
    }

    @Override
    public List<VeFindStudentVo> findSuSheLou(Long XQH) {
        return veFindTFStudentMapper.findSuSheLou(XQH);
    }

    @Override
    public List<VeFindStudentVo> findSuSheHao(Long SSLBM, Long XQH) {
        return veFindTFStudentMapper.findSuSheHao(SSLBM, XQH);
    }

    @Override
    public List<VeFindStudentVo> fidStudent(Long FJBM, Long SSLBM, Long XQH) {
        return veFindTFStudentMapper.fidStudent(FJBM, SSLBM, XQH);
    }

}
