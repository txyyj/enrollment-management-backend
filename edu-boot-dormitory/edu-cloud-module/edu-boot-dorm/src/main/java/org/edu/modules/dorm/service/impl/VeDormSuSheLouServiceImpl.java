package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormSSLou;
import org.edu.modules.dorm.mapper.VeDormSuSheLouMapper;
import org.edu.modules.dorm.service.VeDormSuSheLouService;
import org.edu.modules.dorm.vo.VeDormSuSheLouVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VeDormSuSheLouServiceImpl extends ServiceImpl<VeDormSuSheLouMapper, VeDormSSLou> implements VeDormSuSheLouService {
    @Resource
    private VeDormSuSheLouMapper hqDormSuSheLouMapper;

    @Override
    public List<VeDormSuSheLouVo> suSheLouMessage() {
        return hqDormSuSheLouMapper.suSheLouMessage();
    }

}
