package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormSuShe;
import org.edu.modules.dorm.vo.VeDormCheckVo;
import org.edu.modules.dorm.vo.VeDormSuSheTJVo;

import java.util.List;

public interface VeDormSuSheTJMapper extends BaseMapper<VeDormSuShe> {

    /**
     * 1、宿舍容量统计。
     */
    List<VeDormSuSheTJVo> selectDormList();

    /**
     * 2、宿舍学生统计。
     */
    List<VeDormSuSheTJVo> selectStudentList();

    /**
     * 3、考勤统计。（通过考勤类型id、宿舍楼id、院系id、专业id、班级id精确查询，房间编号模糊查询）
     * @param checkRuleId 考勤类型id
     * @param sslId 宿舍楼id
     * @param facultyId 院系id
     * @param specialtyId 专业id
     * @param banjiId 班级id
     * @param FJBM 房间编号
     */
    List<VeDormCheckVo> selectCheckList(@Param("checkRuleId") Long checkRuleId, @Param("sslId") Long sslId,
                                        @Param("facultyId") Long facultyId, @Param("specialtyId") Long specialtyId,
                                        @Param("banjiId") Long banjiId, @Param("FJBM") String FJBM);

    /**
     * 4、违纪统计。（通过违纪类型id、宿舍楼id、院系id、专业id、班级id精确查询，房间编号模糊查询）
     * @param disciplineTypeId 违纪类型id
     * @param sslId 宿舍楼id
     * @param facultyId 院系id
     * @param specialtyId 专业id
     * @param banjiId 班级id
     * @param FJBM 房间编号
     */
    List<VeDormCheckVo> selectDisciplineList(@Param("disciplineTypeId") Long disciplineTypeId, @Param("sslId") Long sslId,
                                             @Param("facultyId") Long facultyId, @Param("specialtyId") Long specialtyId,
                                             @Param("banjiId") Long banjiId, @Param("FJBM") String FJBM);

}
