package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormLouCengVo对象", description = "楼层条件查询")
public class VeDormLouCengVo implements Serializable {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "校区id")
    private Integer xqid;

    @ApiModelProperty(value = "建筑物id")
    private Integer sslid;

    @ApiModelProperty(value = "校区号")
    private Integer xqh;

    @ApiModelProperty(value = "建筑物号")
    private Integer jzwh;

    @ApiModelProperty(value = "校区名")
    private String xqmc;

    @ApiModelProperty(value = "楼层名称")
    private String lcname;

    @ApiModelProperty(value = "楼层代码")
    private String lccode;

    @ApiModelProperty(value="是否有摄像头")
    private Integer sfsxt;

    @ApiModelProperty(value = "终端id")
    private Integer terminalid;

    @ApiModelProperty(value = "建筑物名称")
    private String jzwmc;

}
