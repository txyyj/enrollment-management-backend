package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
@TableName("ve_base_specialty")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeBaseSpecialty implements Serializable {

    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    //专业代码
    @Excel(name = "专业代码", width = 15)
    @ApiModelProperty(value = "专业代码")
    @TableField(value = "ZYDM")
    private String zydm;

    //专业名称
    @Excel(name = "专业名称", width = 15)
    @ApiModelProperty(value = "专业代码")
    @TableField(value="ZYMC",exist=true)
    private String zymc;

}
