package org.edu.modules.dorm.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.modules.dorm.service.VeDormSuSheFenPeiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags="宿舍清空")
@RestController
@RequestMapping("dorm/empty")
@Slf4j
public class VeDormEmptyController {
    @Autowired
    private VeDormSuSheFenPeiService veDormSuSheFenPeiService;

    @AutoLog(value = "清空宿舍-宿舍清空管理")
    @ApiOperation(value="清空宿舍", notes="宿舍清空管理")
    @PostMapping(value = "/emptySuShe")
    public Result<?> emptySuShe(@ApiParam(value ="学生宿舍表id") @RequestParam(value = "id") Integer id) {
        veDormSuSheFenPeiService.emptySuShe(id);
        return Result.OK("清空宿舍成功！");
    }

}
