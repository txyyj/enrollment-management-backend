package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import io.swagger.models.auth.In;
import org.edu.modules.dorm.entity.VeDormDisciplineType;
import org.edu.modules.dorm.entity.VeDormManager;
import org.edu.modules.dorm.mapper.VeDormDisciplineTypeMapper;
import org.edu.modules.dorm.mapper.VeDormGetManagerMapper;
import org.edu.modules.dorm.service.VeDormGetManagerService;
import org.edu.modules.dorm.service.VeDormManagerService;
import org.edu.modules.dorm.vo.VeDormManagerVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-07-14 11:00
 */
@Service
public class VeDormGetManagerServiceImpl implements VeDormGetManagerService {
    @Resource
    private VeDormGetManagerMapper veDormGetManagerMapper;
    @Override
    public VeDormManagerVo showMng(Integer sslId) {
        return veDormGetManagerMapper.showMng(sslId);
    }

    @Override
    public int updateAdminId(Integer sslId, Integer userId) {
        return veDormGetManagerMapper.updateAdminId(sslId, userId);
    }
}
