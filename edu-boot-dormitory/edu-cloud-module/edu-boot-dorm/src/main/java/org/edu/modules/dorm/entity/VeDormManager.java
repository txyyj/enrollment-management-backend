package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
@TableName("ve_dorm_manager")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ve_dorm_manager对象", description = "管理员表")
public class VeDormManager implements Serializable {

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Integer id;

    @Excel(name = "用户id", width = 15)
    @ApiModelProperty(value = "用户id")
    @TableField(value = "userId")
    private Integer userId;

    @Excel(name = "宿舍id", width = 15)
    @ApiModelProperty(value = "宿舍id")
    @TableField(value = "sslId")
    private Integer sslId;

}
