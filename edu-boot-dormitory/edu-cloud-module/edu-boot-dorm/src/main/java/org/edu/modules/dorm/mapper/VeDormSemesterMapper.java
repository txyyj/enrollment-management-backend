package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.dorm.entity.VeDormSemester;

import java.util.List;

public interface VeDormSemesterMapper extends BaseMapper<VeDormSemester> {
    //获取学期
    List<VeDormSemester> getSemester();
}
