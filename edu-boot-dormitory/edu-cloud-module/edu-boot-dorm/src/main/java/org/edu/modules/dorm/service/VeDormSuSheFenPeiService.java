package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.*;
import org.edu.modules.dorm.vo.VeBaseStudentVo;
import org.edu.modules.dorm.vo.VeDormFenPeiVo;
import org.edu.modules.dorm.vo.VeDormSuSheStudentVo;
import org.edu.modules.dorm.vo.VeDormssfpVo;

import java.util.List;

public interface VeDormSuSheFenPeiService extends IService<VeDormStudent>{

    void addStudent(VeDormStudent veDormStudent);

    List<VeDormFenPeiVo> selectSuSheLou(Integer id);

    Integer count(Integer id);

    //查询学生
    List<VeDormSuSheStudentVo> selectStudent(Integer fjbh);

    //学生下拉框
    List<VeBaseGrade> getGrade();

    List<VeBaseFaculty> getCollege();

    List<VeBaseSpecialty> getMajor(Integer xyId);

    List<VeBaseBanji> getBanJi(Integer gradeId,Integer zyId,Integer xyId);

     //学生列表选择学生
     List<VeBaseStudentVo> queryStudent(Integer gradeId, Integer xyId, Integer zyId, Integer bjId, Long xh, String xm, Long sfzh);

     //1.宿舍名称、总楼层数、总寝室数、总床位数、已住人数
     //获取楼层
     List<VeDormLouceng> getLouCeng( Integer id);

     //获取楼层下面的宿舍
     List<VeDormSuShe> getSuShe( Integer lcId, Integer sslId);

     //楼层宿舍数量
     Integer lcConut(Integer lcId, Integer ssId);

     List<VeDormssfpVo> getSuSheInfo(Integer id);

    //清空宿舍
    Integer emptySuShe(Integer id);

    VeBaseStudentVo getBaseStudent(Integer id);

    //已住人数改变
    Integer updateYzrs(Integer num, Long id);

    List<VeDormSuSheStudentVo> selectStudentMore(@Param("xqh") Long xqh, @Param("ssl") Long ssl,
                                                 @Param("ss") Long ss, @Param("stu") Long stu) ;

}
