package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormCheck;
import org.edu.modules.dorm.mapper.VeDormCheckMapper;
import org.edu.modules.dorm.service.VeDormCheckService;
import org.edu.modules.dorm.vo.VeDormCheckVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VeDormCheckServiceImpl extends ServiceImpl<VeDormCheckMapper, VeDormCheck> implements VeDormCheckService {
    @Resource
    private VeDormCheckMapper veDormCheckMapper;

    /**
     * 1、考勤列表。（通过姓名、学号模糊查询，宿舍楼编号精准查询）
     * @param XM 姓名
     * @param XH 学号
     * @param SSLBM 宿舍楼编号
     * */
    @Override
    public List<VeDormCheckVo> selectList(String XM, String XH, Long SSLBM) {
        return veDormCheckMapper.selectList(XM, XH, SSLBM);
    }

    /**
     * 2、通过id查询。（违纪管理-通用）
     * @param id 考勤id
     */
    @Override
    public VeDormCheckVo selectById(Long id) {
        return veDormCheckMapper.selectById(id);
    }

}
