package org.edu.modules.dorm.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormAsset;
import org.edu.modules.dorm.mapper.VeDormAssetMapper;
import org.edu.modules.dorm.service.VeDormAssetService;
import org.edu.modules.dorm.vo.VeDormAssetVo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VeDormAssetServiceImpl extends ServiceImpl<VeDormAssetMapper, VeDormAsset> implements VeDormAssetService {

//    @Value("${common.host}")
//    private String dirHost ;

    @Resource
    private VeDormAssetMapper hqDormAssetMapper;



    @Override
    public List<VeDormAssetVo> fuzzyQuery(String zcName, Long zcType, Integer status) {
        return hqDormAssetMapper.fuzzyQuery(zcName,zcType,status);

    }



}
