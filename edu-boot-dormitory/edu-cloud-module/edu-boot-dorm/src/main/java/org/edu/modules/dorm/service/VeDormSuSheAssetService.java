package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormAsset;
import org.edu.modules.dorm.vo.VeDormAssetVo;
import org.edu.modules.dorm.vo.VeDormSuSheVo;
import org.edu.modules.dorm.vo.VeSuSheAssetVo;

import java.util.List;

public interface VeDormSuSheAssetService{

    List<VeSuSheAssetVo> suSheAsset();

    List<VeSuSheAssetVo> suSheQueryByName( String zcName,Integer zcType,Integer status);

    List<VeSuSheAssetVo> suSheQueryBySsId(Long ssIdm,String zcName,Integer zcType,Integer status);

    VeSuSheAssetVo updateMessage(Long id);

}
