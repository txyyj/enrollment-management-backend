package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.dorm.entity.VeDormSSLou;
import org.edu.modules.dorm.vo.VeDormSuSheLouVo;

import java.util.List;

public interface VeDormSuSheLouMapper extends BaseMapper<VeDormSSLou> {

    List<VeDormSuSheLouVo> suSheLouMessage();

}
