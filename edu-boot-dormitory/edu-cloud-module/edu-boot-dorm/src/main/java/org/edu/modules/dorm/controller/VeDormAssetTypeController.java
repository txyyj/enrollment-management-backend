package org.edu.modules.dorm.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.common.system.vo.LoginUser;
import org.edu.modules.dorm.entity.VeDormAssetType;
import org.edu.modules.dorm.service.VeDormAssetService;
import org.edu.modules.dorm.service.VeDormAssetTypeService;
import org.edu.modules.dorm.vo.VeDormAssetExcelVo;
import org.edu.modules.dorm.vo.VeDormAssetVo;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
@Api(tags="宿舍资产管理")
@RestController
@RequestMapping("dorm/assetType")
@Slf4j

public class VeDormAssetTypeController extends BaseController<VeDormAssetType, VeDormAssetTypeService> {
    @Autowired
    private VeDormAssetTypeService dormAssetService;
    @Autowired
    private VeDormAssetService veDormAssetService;
    /**
     * 新增信息
     * @param zcName zcType status,
     * @return
     */
    @AutoLog(value = "站点可选择模板-资产查询列表")
    @ApiOperation(value="资产查询列表", notes="站点可选择模板-资产查询列表")
    @PostMapping("/fuzzyQuery")
    public Result<?> fuzzyQuery(
                                @ApiParam(value = "资产名称")@RequestParam(name = "zcName")String zcName,
                                @ApiParam(value = "资产类型")@RequestParam(name = "zcType")Long zcType,
                                @ApiParam(value = "状态")@RequestParam(name = "status")Integer status){

        List<VeDormAssetVo> list = veDormAssetService.fuzzyQuery(zcName,zcType,status);
        return Result.OK(list);

    }

    /**
     * 新增信息
     * @param typeName
     * @return
     */
    @AutoLog(value = "站点可选择模板-通过模板进行添加")
    @ApiOperation(value="添加宿舍资产类型", notes="站点可选择模板-通过VeDormAssetType对象")
    @PostMapping("/add")
    public Result<?> addProperty(@ApiParam(value = "类型名称") @RequestParam("typeName") String typeName) {
        //判断是否已经存在
      QueryWrapper<VeDormAssetType> wrapper = new QueryWrapper<>();
      wrapper.eq("typeName",typeName);
      List<VeDormAssetType> list = dormAssetService.list(wrapper);
      if(list.size() != 0){
        return  Result.error("已经存在该类型");
      }
        VeDormAssetType veDormAssetType = new VeDormAssetType();
        veDormAssetType.setTypeName(typeName);
        dormAssetService.save(veDormAssetType);
        return Result.OK("添加成功!");

    }

    /**
     * 通过类别名称模糊查询
     * @param assetName
     * @return
     */
    @AutoLog(value = "站点可选择模板-通过类别名模糊查询")
    @ApiOperation(value="通过类别名模糊查询", notes="站点可选择模板-通过类别名模糊查询")
    @PostMapping(value = "/queryByName")

    public Result<?> queryByName( @ApiParam(value = "类别名称")@RequestParam(name = "assetName") String assetName){
        QueryWrapper<VeDormAssetType> wrapper = new QueryWrapper<>();
        wrapper.like("typeName",assetName);
        wrapper.orderByDesc("id");
        ArrayList<VeDormAssetType> list = (ArrayList<VeDormAssetType>) dormAssetService.list(wrapper);
        return Result.OK(list);

    }


    /**
     * 通过id查询
     * @param id
     * @return
     */
    @AutoLog(value = "站点可选择模板-编辑")
    @ApiOperation(value="通过id查询", notes="站点可选择模板-通过id查询，进行编辑")
    @PostMapping(value = "/queryById")
    public Result<?> queryById( @ApiParam(value = "类型Id")@RequestParam(name="id") String id) {
        VeDormAssetType hqDormAssetType = dormAssetService.getById(id);
        if(hqDormAssetType==null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(hqDormAssetType);
    }

    /**
     * 修改信息
     * @param typeName
     * @return
     */
    @AutoLog(value = "站点可选择模板-修改信息")
    @ApiOperation(value="修改信息", notes="站点可选择模板-修改信息")
    @PostMapping(value = "/edit")
    public Result<?> edit(@ApiParam(value = "资产类型") @RequestParam(value = "typeName") String typeName,
                          @ApiParam(value = "资产ID") @RequestParam(value = "id")Long id) {
      //判断是否已经存在
      QueryWrapper<VeDormAssetType> wrapper = new QueryWrapper<>();
      wrapper.eq("typeName",typeName);
      List<VeDormAssetType> list = dormAssetService.list(wrapper);
      if(list.size() > 1){
        return  Result.error("已经存在该类型");
      }
        VeDormAssetType veDormAssetType = new VeDormAssetType();
        veDormAssetType.setTypeName(typeName);
        veDormAssetType.setId(id);
        dormAssetService.updateById(veDormAssetType);
        return Result.OK("编辑成功!");
    }
    /**
     * 通过id删除
     * @param id
     * @return
     */
    @AutoLog(value = "站点可选择模板-通过id删除")
    @ApiOperation(value="通过id删除", notes="站点可选择模板-通过id删除")
    @PostMapping(value = "/delete")
    public Result<?> delete( @ApiParam(value = "资产Id")@RequestParam(name="id",required=true) String id) {
        dormAssetService.removeById(id);
        return Result.OK("删除成功!");
    }
    /**
     * 批量删除
     * @param ids
     * @return
     */
    @AutoLog(value = "站点可选择模板-批量删除")
    @ApiOperation(value="批量删除", notes="站点可选择模板-批量删除")
    @PostMapping(value = "/deleteBatch")
    public Result<?> deleteBatch( @ApiParam(value = "资产ID数组")@RequestParam(name="ids",required=true) String ids) {
        this.dormAssetService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }

    /**
     * 查询所有的资产类别
     * @param
     * @return
     */

    @ApiOperation(value="查询所有的资产类别", notes="查询所有的资产类别")
    @PostMapping(value = "/assetMessage")
    public Result<?> deleteBatch() {
      QueryWrapper<VeDormAssetType> wrapper = new QueryWrapper<>();
      wrapper.orderByDesc("id");
        List<VeDormAssetType> list = dormAssetService.list(wrapper);
        return Result.OK(list);
    }


    //导出
    @AutoLog(value = "宿舍资产导出excel表格")
    @ApiOperation(value = "宿舍资产导出excel表格",notes = "宿舍资产")
    @RequestMapping(value = "export")
    public ModelAndView excelExport(@ApiParam(value = "资产名称") @RequestParam(name = "zcName",required = false) String zcName,
                                    @ApiParam(value = "资产类型") @RequestParam(name = "zcType",required = false) String zcType,
                                    @ApiParam(value = "状态") @RequestParam(name = "status",required = false) String  status) {
        //非空判断
        if("".equals(zcName)){
            zcName = null;
        }
        Integer Status = null;
        if( !"".equals(status)){
            Status = Integer.parseInt(status);
        }
        Long zctype = null;
        if(!"".equals(zcType)){
            zctype = Long.parseLong(zcType);
        }
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        List<VeDormAssetVo> list = veDormAssetService.fuzzyQuery( zcName, zctype, Status);

        List<VeDormAssetExcelVo> excelVoList = new ArrayList<>();
        for (VeDormAssetVo veDormAssetVo : list) {
            VeDormAssetExcelVo excelVo = new VeDormAssetExcelVo();
            excelVo.setZcName(veDormAssetVo.getZcName());
            excelVo.setId(veDormAssetVo.getId());
            excelVo.setJZWH(veDormAssetVo.getJZWH());
            excelVo.setFJBM(veDormAssetVo.getFJBM());
            excelVo.setNums(veDormAssetVo.getNums());
            excelVo.setTypeName(veDormAssetVo.getTypeName());

            Long statu = veDormAssetVo.getStatus();
            String state = null;
            if (statu == 1) {
                state = "正常";
            } else if (statu == 2) {
                state = "维修";
            } else if (statu == 3) {
                state = "报废";
            }
            excelVo.setState(state);
            excelVoList.add(excelVo);

        }

        mv.addObject(NormalExcelConstants.FILE_NAME, "宿舍资产");
        mv.addObject(NormalExcelConstants.CLASS, VeDormAssetExcelVo.class);
        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ExportParams exportParams = new ExportParams("宿舍资产", "导出人:"+user.getRealname(), "导出信息");
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, excelVoList);
        return mv;

    }



}
