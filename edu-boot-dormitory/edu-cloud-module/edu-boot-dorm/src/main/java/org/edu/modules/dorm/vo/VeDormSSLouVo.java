package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormSuSVo对象", description = "宿舍楼列表")
public class VeDormSSLouVo implements Serializable {

    @ApiModelProperty(value = "id")
    private Integer id;

    @Excel(name = "建筑物号", width = 15)
    @ApiModelProperty(value = "建筑物号")
    private String jzwh;

    @Excel(name = "建筑物名称", width = 15)
    @ApiModelProperty(value = "建筑物名称")
    private String jzwmc;

    @Excel(name = "性别码", width = 15)
    @ApiModelProperty(value ="性别码")
    private Integer xbm;

    @Excel(name = "建筑物层数", width = 15)
    @ApiModelProperty(value ="建筑物层数")
    private Integer jzwcs;

    @Excel(name ="总建筑面积", width = 15)
    @ApiModelProperty(value ="总建筑面积")
    private Integer zjzmj;

    @Excel(name ="建筑物地址", width = 15)
    @ApiModelProperty(value ="建筑物地址")
    private String jzwdz;

    @Excel(name ="排序", width = 15)
    @ApiModelProperty(value ="排序")
    private Integer listSort;

    @Excel(name ="状态", width = 15)
    @ApiModelProperty(value ="状态")
    private Integer status;

    @Excel(name="校区名称", width =15)
    @ApiModelProperty(value = "校区名称")
    private String xqmc;

    @ApiModelProperty(value = "校区号")
    private Integer xqh;//李少君

}
