package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.edu.modules.dorm.entity.VeDormSuShe;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.util.List;
import java.util.Map;

/**
 * @Description: 宿舍分配信息展示
 * @Author:
 * @Date:   2021-03-06
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeSuSheFenPeiVo {

    @TableId(value = "id")
    @ApiModelProperty(value = "id")
    private Integer id;

    @Excel(name = "宿舍楼名称", width = 15)
    @ApiModelProperty("宿舍楼名称")
     private String jzwmc;

    @Excel(name = "总楼层数", width = 15)
    @ApiModelProperty("总楼层数")
    private Integer jzwcs;

    @Excel(name = "总寝室数", width = 15)
    @ApiModelProperty("总寝室数")
    private Integer totalRom;

    @Excel(name = "可住人数", width = 15)
    @ApiModelProperty("可住人数")
    private Integer kzrs;

    @Excel(name = "已住人数", width = 15)
    @ApiModelProperty("已住人数")
    private Integer yzrs;

    @Excel(name = "性别码", width = 15)
    @ApiModelProperty("性别码")
    private Integer xbm;

    @ApiModelProperty("楼层宿舍信息 key-楼层，value-宿舍")
    private Map<Long, List<VeDormSuShe>> map ;

    //李少君+
    @ApiModelProperty(value ="宿舍楼图片Id")
    private Long fileid;

    //李少君+
    @ApiModelProperty(value ="宿舍楼图片url")
    private String sourcefile;

}
