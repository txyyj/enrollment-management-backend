package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormSemester;
import org.edu.modules.dorm.mapper.VeDormSemesterMapper;
import org.edu.modules.dorm.service.VeDormSemesterService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VeDormSemesterServiceImpl extends ServiceImpl<VeDormSemesterMapper, VeDormSemester> implements VeDormSemesterService {

    @Resource
    private VeDormSemesterMapper veDormSemesterMapper;

    @Override
    public List<VeDormSemester> getSemester() {
        return veDormSemesterMapper.getSemester();
    }
}
