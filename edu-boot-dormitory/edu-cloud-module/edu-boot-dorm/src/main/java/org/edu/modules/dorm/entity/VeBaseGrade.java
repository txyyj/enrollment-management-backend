package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
@TableName("ve_base_grade")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeBaseGrade implements Serializable {

    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

     //年级代码
    @Excel(name = "年级代码", width = 15)
    @ApiModelProperty(value = "年级代码")
    @TableField(value = "NJDM")
    private String njdm;

    //年级名称
    @Excel(name = "年级名称", width = 15)
    @ApiModelProperty(value = "年级名称")
    @TableField(value = "NJMC")
    private String njmc;

}
