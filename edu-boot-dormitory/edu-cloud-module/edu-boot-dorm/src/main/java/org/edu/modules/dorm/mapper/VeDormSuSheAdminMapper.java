package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormSShe;
import org.edu.modules.dorm.vo.VeDormSSVo;

import java.util.List;

public interface VeDormSuSheAdminMapper extends BaseMapper<VeDormSShe>{

    List<VeDormSSVo> selectParms(@Param("xqId")Integer  xq,
                                 @Param("sslId")Integer sslId,
                                 @Param("lcId") Integer  lcId);

    //根据校区和宿舍楼获取宿舍楼id
    Long getSSLId(@Param("xqh") Integer xqh,@Param("jzwmc") String jzwmc);
    //根据校区id、宿舍楼id、楼层获取楼层id
    Long getloucengId(@Param("xqh") Integer xqh, @Param("jzwh") Integer jzwh,@Param("lcName") String lcName);
    //根据楼层号和房间编号查询是否已存在该宿舍
    Long getssId(@Param("lch") Integer lch,@Param("fjbh") String fjbh);

}
