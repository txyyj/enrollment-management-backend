package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
@TableName("ve_dorm_weixiu")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ve_dorm_weixiu对象", description = "宿舍维修表")
public class VeDormWeiXiu implements Serializable{

    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Integer id;

    @Excel(name = "宿舍id", width = 15)
    @ApiModelProperty(value = "宿舍id")
    @TableField(value = "ssId")
    private Integer ssid;

    @Excel(name = "维修信息", width = 15)
    @ApiModelProperty(value = "维修信息")
    private String title;

    @Excel(name = "维修内容", width = 15)
    @ApiModelProperty(value = "维修内容")
    private String content;

    @Excel(name = "创建时间", width = 15)
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "addTime")
    private int addtime;

    @Excel(name = "创建人", width = 15)
    @ApiModelProperty(value = "创建人")
    @TableField(value = "createUserId")
    private int  createuserid;

    @Excel(name = "创建时间", width = 15)
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "createTime")
    private Long createtime;

}
