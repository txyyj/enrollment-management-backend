package org.edu.modules.dorm.mapper;

import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.vo.VeFindStudentVo;

import java.util.List;

public interface VeFindTFStudentMapper {

    List<VeFindStudentVo> findXiaoQu();

    List<VeFindStudentVo> findSuSheLou(@Param("XQH")Long XQH);

    List<VeFindStudentVo> findSuSheHao(@Param("SSLBM")Long SSLBM,@Param("XQH")Long XQH);

    List<VeFindStudentVo> fidStudent(@Param("FJBM")Long FJBM,@Param("SSLBM")Long SSLBM,@Param("XQH")Long XQH);

}
