package org.edu.modules.dorm.service.impl;

import org.edu.modules.dorm.mapper.VeDormSuSheAssetMapper;
import org.edu.modules.dorm.service.VeDormSuSheAssetService;
import org.edu.modules.dorm.vo.VeDormAssetVo;
import org.edu.modules.dorm.vo.VeSuSheAssetVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;
@Service
public class VeDormSuSheAssetServiceImpl  implements VeDormSuSheAssetService {

    @Autowired
    private VeDormSuSheAssetMapper veDormSuSheAssetMapper;
    @Override
    //宿舍资产信息
    public List<VeSuSheAssetVo> suSheAsset( ) {
        return veDormSuSheAssetMapper.suSheAsset();
    }

    @Override
    public List<VeSuSheAssetVo> suSheQueryByName( String zcName, Integer zcType, Integer status) {

        return veDormSuSheAssetMapper.suSheQueryByName( zcName, zcType, status);
    }

    @Override
    public List<VeSuSheAssetVo> suSheQueryBySsId(Long ssId, String zcName, Integer zcType, Integer status) {
        return  veDormSuSheAssetMapper.suSheQueryBySsId(ssId, zcName, zcType, status);
    }

  @Override
  public VeSuSheAssetVo updateMessage(Long id) {
    return veDormSuSheAssetMapper.updateMessage(id);
  }
}
