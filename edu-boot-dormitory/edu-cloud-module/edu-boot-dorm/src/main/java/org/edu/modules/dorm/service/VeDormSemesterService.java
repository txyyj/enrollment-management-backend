package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormSemester;

import java.util.List;

public interface VeDormSemesterService extends IService<VeDormSemester> {
    //获取学期
    List<VeDormSemester> getSemester();
}
