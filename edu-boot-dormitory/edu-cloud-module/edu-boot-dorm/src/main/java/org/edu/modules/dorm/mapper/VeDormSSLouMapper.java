package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormSSLou;
import org.edu.modules.dorm.vo.VeDormSSLouVo;
import org.edu.modules.dorm.vo.VeDormWeiJiImageVo;

import java.util.List;

public interface VeDormSSLouMapper extends BaseMapper<VeDormSSLou> {

    List<VeDormSSLouVo> selectByParam(@Param("ssl") String ssl);

    List<VeDormSSLouVo> exportList(@Param("ssl") String ssl);

    VeDormWeiJiImageVo  getEditInfo(@Param("id") Long id);

}
