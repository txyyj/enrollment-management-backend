package org.edu.modules.dorm.service;

import org.edu.modules.dorm.vo.VeDormLodgingAuditVo;

import java.util.List;

public interface VeDormLodgingAuditService {

    List<VeDormLodgingAuditVo> LodgingAuditTable(String xm,Integer type);

    Integer LodgingAudit(Integer status,Integer id,String auditRemark);

    Integer check(Integer status, Integer id);

    List<VeDormLodgingAuditVo> selectById(Integer id);

}
