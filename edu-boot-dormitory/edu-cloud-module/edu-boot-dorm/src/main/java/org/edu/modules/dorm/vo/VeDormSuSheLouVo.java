package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 宿舍楼表
 * @Author:
 * @Date:   2021-01-22
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "SuSheLouVo", description = "宿舍楼信息展示")
public class VeDormSuSheLouVo implements Serializable {

    /**宿舍楼ID*/
    @TableId(value = "id")
    @ApiModelProperty(value = "宿舍楼ID")
    private String id;

    /**性别码  0待定 1男 2女*/
    @TableField(value = "XBM")
    @ApiModelProperty(value = "性别码  0待定 1男 2女")
    private String XBM;

    /**建筑物名称*/
    @TableField(value = "JZWMC")
    @ApiModelProperty(value = "建筑物名称")
    private String JZWMC;

    /**建筑物层数*/
    @TableField(value = "JZWCS")
    @ApiModelProperty(value = "建筑物层数")
    private Integer JZWCS;

    /**总寝室数*/
    @TableField(value = "susheCount")
    @ApiModelProperty(value = "总寝室数")
    private Long susheCount;

    /**总床位数*/
    @TableField(value = "cwSum")
    @ApiModelProperty(value = "总床位数")
    private Long cwSum;

    /**可住人数*/
    @Excel(name = "可住人数", width = 15)
    @ApiModelProperty(value = "可住人数")
    private Integer KZRS;

    /**已住人数*/
    @TableField(value = "studentSum")
    @ApiModelProperty(value = "已住人数")
    private Long studentSum;

}
