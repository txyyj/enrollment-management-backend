package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 通知
 * @Author:
 * @Date:   2021-03-10
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "通知", description = "通知")
public class VeDormNoticeVo implements Serializable {

    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /**通知类型*/
    @Excel(name = "通知类型", width = 15)
    @ApiModelProperty(value = "通知类型")
    private Long type;

    /**校区Id*/
    @Excel(name = "校区Id", width = 15)
    @ApiModelProperty(value = "校区ID")
    private Long campusId;

    /**通知目标*/
    @Excel(name = "通知目标", width = 15)
    @ApiModelProperty(value = "通知目标")
    private Long rowId;

    /**标题*/
    @Excel(name = "标题", width = 15)
    @ApiModelProperty(value = "标题")
    private String title;

    /**内容*/
    @Excel(name = "内容", width = 15)
    @ApiModelProperty(value = "内容")
    private String content;

    /**发布者*/
    @Excel(name = "发布者", width = 15)
    @ApiModelProperty(value = "发布者")
    private Long createUserId;

    /**发布时间*/
    @Excel(name = "发布时间", width = 15)
    @ApiModelProperty(value = "发布时间")
    private Long createTime;

    /**接收范围*/
    @Excel(name = "接收范围", width = 15)
    @ApiModelProperty(value = "接收范围")
    private String SSLName;

    /**发布人*/
    @Excel(name = "发布人", width = 15)
    @ApiModelProperty(value = "发布人")
    private Long userName;

}
