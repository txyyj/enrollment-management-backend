package org.edu.modules.dorm.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.dorm.entity.VeDormDisciplineType;
import org.edu.modules.dorm.service.VeDormDisciplineTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@Api(tags="违纪类型管理")
@RestController
@RequestMapping("dorm/disciplineType")
@Slf4j
public class VeDormDisciplineTypeController extends BaseController<VeDormDisciplineType, VeDormDisciplineTypeService> {
    @Autowired(required = false)
    private VeDormDisciplineTypeService veDormDisciplineTypeService;

    /**
     * 1、查询所有违纪类型。
     */
    @ApiOperation(value="查询所有违纪类型", notes="站点可选择模板-查询所有违纪类型")
    @PostMapping(value = "/selectAllDisciplineType")
    public Result<?> selectAllDisciplineType() {
        QueryWrapper<VeDormDisciplineType> wrapper = new QueryWrapper<>();
        wrapper.orderByDesc("id");
        ArrayList<VeDormDisciplineType> list = (ArrayList<VeDormDisciplineType>) veDormDisciplineTypeService.list(wrapper);
        return Result.OK(list);
    }

    /**
     * 2、添加违纪类型。
     * @param name 违纪名称
     * @param status 违纪状态（1=启用，0=禁用）
     * 未包含参数 terminalId 终端id
     */
    @AutoLog(value = "站点可选择模板-添加违纪类型")
    @ApiOperation(value="添加违纪类型", notes="站点可选择模板-添加违纪类型")
    @PostMapping(value = "/insert")
    @ApiImplicitParam(name = "status", value = "是否启用（1启用，0禁用）", required = true)
    public Result<?> insert(@ApiParam(value = "违纪名称") @RequestParam(name = "name") String name,
                            @ApiParam(value = "违纪状态") @RequestParam(name = "status") Integer status) {
      //判断是否重复
      QueryWrapper<VeDormDisciplineType> wrapper = new QueryWrapper<>();
      wrapper.eq("name",name);
      List<VeDormDisciplineType> list = veDormDisciplineTypeService.list(wrapper);
      if(list.size() != 0){
        return Result.error("已经存在该违纪");
      }
        VeDormDisciplineType veDormDisciplineType = new VeDormDisciplineType();
        veDormDisciplineType.setName(name);
        veDormDisciplineType.setStatus(status);

        boolean isInsert = veDormDisciplineTypeService.save(veDormDisciplineType);
        if(isInsert) {
            return Result.OK("添加成功！");
        } else {
            return Result.error("添加失败！");
        }
    }

    /**
     * 3、违纪类型列表。（通过名称模糊查询，状态精准查询）
     * @param name 违纪名称
     * @param status 违纪状态（1=启用，0=禁用）
     */
    @ApiOperation(value="通过名称模糊查询，状态精准查询", notes="站点可选择模板-通过名称模糊查询，状态精准查询")
    @PostMapping(value = "/selectList")
    public Result<?> selectList(@ApiParam(value = "违纪名称") @RequestParam(name = "name") String name,
                                @ApiParam(value = "违纪状态") @RequestParam(name = "status") String status) {
        QueryWrapper<VeDormDisciplineType> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("name", name);
        if (!status.equals("")) {
            Integer Status = Integer.parseInt(status);
            queryWrapper.eq("status", Status);
        }
        queryWrapper.orderByDesc("id");

        List<VeDormDisciplineType> list = veDormDisciplineTypeService.list(queryWrapper);
        return Result.OK(list);
    }

    /**
     * 4、批量启用或禁用。（1=启用，0=禁用）
     * @param ids 违纪类型id
     * @param result 启用或禁用的结果
     */
    @AutoLog(value = "站点可选择模板-批量启用或禁用")
    @ApiOperation(value="批量启用或禁用", notes="站点可选择模板-批量启用或禁用")
    @PostMapping(value = "/batchUpdateState")
    public Result<?> batchUpdateState(@ApiParam(value = "违纪类型id") @RequestParam(name = "ids") String ids,
                                      @ApiParam(value = "启用或禁用的结果") @RequestParam(name = "result") Integer result) {
        String[] strings = ids.split(",");
        ArrayList<VeDormDisciplineType> list = new ArrayList<>();
        for (String s : strings) {
            VeDormDisciplineType veDormDisciplineType = new VeDormDisciplineType();
            veDormDisciplineType.setId(Long.parseLong(s));
            veDormDisciplineType.setStatus(result);

            list.add(veDormDisciplineType);
        }
        veDormDisciplineTypeService.updateBatchById(list);

        if (result == 1) {
            return Result.OK("启用成功！");
        } else {
            return Result.OK("禁用成功！");
        }
    }

    /**
     * 5、通过id查询。
     * @param id 违纪类型id
     */
    @ApiOperation(value="通过id查询", notes="站点可选择模板-通过id查询")
    @PostMapping(value = "/selectById")
    public Result<?> selectById(@ApiParam(value = "违纪类型id") @RequestParam(name = "id") Long id) {
        return Result.OK(veDormDisciplineTypeService.getById(id));
    }

    /**
     * 6、编辑违纪类型。
     * @param id 违纪类型id
     * @param name 违纪名称
     * @param status 违纪状态
     */
    @AutoLog(value = "站点可选择模板-编辑违纪类型")
    @ApiOperation(value="编辑违纪类型", notes="站点可选择模板-编辑违纪类型")
    @PostMapping(value = "/update")
    public Result<?> update(@ApiParam(value = "违纪类型id") @RequestParam(name = "id") Long id,
                            @ApiParam(value = "违纪名称") @RequestParam(name = "name") String name,
                            @ApiParam(value = "违纪状态") @RequestParam(name = "status") Integer status) {
      //判断是否重复
      QueryWrapper<VeDormDisciplineType> wrapper = new QueryWrapper<>();
      wrapper.eq("name",name);
      List<VeDormDisciplineType> list = veDormDisciplineTypeService.list(wrapper);
      if(list.size() > 1){
        return Result.error("已经存在该违纪");
      }
        VeDormDisciplineType veDormDisciplineType = new VeDormDisciplineType();
        veDormDisciplineType.setId(id);
        veDormDisciplineType.setName(name);
        veDormDisciplineType.setStatus(status);

        boolean isUpdate = veDormDisciplineTypeService.updateById(veDormDisciplineType);
        if(isUpdate) {
            return Result.OK("编辑成功！");
        } else {
            return Result.error("编辑失败！");
        }
    }

    /**
     * 删除违纪类型
     */
    @ApiOperation(value="删除违纪类型")
    @PostMapping(value = "/deleteById")
    public Result<?> deleteById(@ApiParam(value = "违纪类型id") @RequestParam(name = "id") Long id) {
        veDormDisciplineTypeService.removeById(id) ;
        return Result.OK();
    }

    /**
     * 批量删除违纪类型
     */
    @ApiOperation(value="删除违纪类型")
    @PostMapping(value = "/deleteByIds")
    public Result<?> deleteById(@RequestParam("ids") List<Long> ids) {
        veDormDisciplineTypeService.removeByIds(ids) ;
        return Result.OK();
    }

}
