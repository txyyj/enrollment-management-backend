package org.edu.modules.dorm.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.common.system.vo.LoginUser;
import org.edu.modules.dorm.entity.VeDormSShe;
import org.edu.modules.dorm.entity.bo.BasicResponseBO;
import org.edu.modules.dorm.entity.bo.VeCampusBO;
import org.edu.modules.dorm.service.VeBaseDormManageService;
import org.edu.modules.dorm.service.VeDormSuSheAdminService;
import org.edu.modules.dorm.vo.VeDormLouCengVo;
import org.edu.modules.dorm.vo.VeDormSSVo;
import org.edu.modules.dorm.vo.VeDormSSheVO;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Api(tags = "宿舍管理")
@RestController
@RequestMapping("dorm/dormMng")
@Slf4j
@ApiSort(value = 100)
public class VeDormSuSheController extends BaseController<VeDormSShe, VeDormSuSheAdminService> {
    @Autowired
    private VeDormSuSheAdminService veDormSuSheAdminService;
    @Autowired
    private VeBaseDormManageService veBaseDormManageService; //李少君

    //添加宿舍
    @AutoLog(value = "宿舍管理-添加宿舍")
    @ApiOperation(value = "添加宿舍", notes = "宿舍管理-添加宿舍")
    @PostMapping(value = "/add")
    public Result<?> add(@ApiParam("校区号") @RequestParam(value = "xqh") Integer xqh,
                         @ApiParam("建筑物号") @RequestParam(value = "jzwh") Integer jzwh,
                         @ApiParam("网络端口") @RequestParam(value = "wldk",required = false) String wldk,
                         @ApiParam("房间编码") @RequestParam(value = "fjbm") String fjbm,
                         @ApiParam("楼层号") @RequestParam(value = "lch") Integer lch,
                         @ApiParam("可住人数") @RequestParam(value = "kzrs") Integer kzrs,
                         @ApiParam("电表读数") @RequestParam(value = "dbds",required = false) Integer dbds,
                         @ApiParam("水表读数") @RequestParam(value = "sbds",required = false) Integer sbds,
                         @ApiParam("是否有电视机") @RequestParam(value = "sfdsj",required = false) Integer sfdsj,
                         @ApiParam("电话号码") @RequestParam(value = "dhhm",required = false) String dhhm,
                         @ApiParam("是否网络") @RequestParam(value = "sfwl",required = false) Integer sfwl,
                         @ApiParam("是否洗手间") @RequestParam(value = "sfxsj",required = false) Integer sfxsj,
                         @ApiParam("入住性别") @RequestParam(value = "rzxb") Integer rzxb,
                         @ApiParam("宿舍状态") @RequestParam(value = "status") Integer status) {
        if(kzrs>12){
            return Result.error("所填可用床位数过多,请重新选择!");
        }
        QueryWrapper<VeDormSShe> wrapper=new QueryWrapper<>();
        wrapper.eq("fjbm",fjbm);
        VeDormSShe suShe=veDormSuSheAdminService.getOne(wrapper);
        if (suShe!=null){

            return Result.OK("添加失败，该宿舍已经存在");
        }
        VeDormSShe veDormSShe = new VeDormSShe();
        veDormSShe.setXqh(xqh);
        veDormSShe.setJzwh(jzwh);
        veDormSShe.setFjbm(fjbm);
        veDormSShe.setLch(lch);
        veDormSShe.setKzrs(kzrs);
        veDormSShe.setDbds(dbds);
        veDormSShe.setSbds(sbds);
        veDormSShe.setSfdsj(sfdsj);
        veDormSShe.setDhhm(dhhm);
        veDormSShe.setSfwl(sfwl);
        veDormSShe.setSfxsj(sfxsj);
        veDormSShe.setRzxb(rzxb);
        veDormSShe.setStatus(status);
        veDormSShe.setWldk(wldk);
        veDormSShe.setSsbz("1");

        veDormSuSheAdminService.insertSuSHe(veDormSShe);
        return Result.OK("添加成功！");
    }

    @AutoLog(value = "宿舍管理-宿舍列表")
    @ApiOperation(value = "宿舍列表", notes = "宿舍管理-宿舍列表")
    @PostMapping(value = "/select")
    public Result<?> querySuShe(@ApiParam("校区id") @RequestParam(value = "xqId", required = false) Integer xqId,
                                @ApiParam("建筑物id") @RequestParam(value = "jzwId", required = false) Integer jzwId,
                                @ApiParam("楼层id") @RequestParam(value = "lcId", required = false) Integer lcId) {

//        BasicResponseBO<List> list = veBaseDormManageService.getCampusMessage(); //李少君
//        List<VeCampusBO> result = JSONObject.parseArray(JSON.toJSONString(list.getResult()),VeCampusBO.class);
//        List<VeDormSSVo> v = veDormSuSheAdminService.getSuShe(xqId, jzwId, lcId);
//        for(int i =0 ;i<v.size();i++){
//            for (int j =0 ;j<result.size();j++){
//                if(v.get(i).getXqh()==(result.get(j).getId().longValue())){
//                    v.get(i).setXqmc(result.get(j).getXqmc());
//                }
//            }
//
//        }
        return Result.OK(veDormSuSheAdminService.getSuShe(xqId, jzwId, lcId));
    }

    @AutoLog(value = "宿舍管理-宿舍删除")
    @ApiOperation(value = "宿舍删除", notes = "宿舍管理-宿舍删除")
    @PostMapping(value = "/delete")
    public Result<?> delete(@ApiParam("宿舍id") @RequestParam(name = "id") Integer id) {
        veDormSuSheAdminService.removeById(id);
        return Result.OK("删除成功！");
    }

    @ApiOperation(value = "获取编辑信息", notes = "宿舍管理-获取编辑信息")
    @PostMapping(value = "/getEdit")
    public Result<?> getEdit(@ApiParam("宿舍id") @RequestParam(value = "id") Integer id) {
        return Result.OK(veDormSuSheAdminService.getById(id));
    }

    @AutoLog(value = "宿舍管理-宿舍编辑")
    @ApiOperation(value = "宿舍编辑", notes = "宿舍管理-宿舍编辑")
    @PostMapping(value = "/edit")
    public Result<?> edit(@ApiParam("id") @RequestParam(value = "id") Integer id,
                         @ApiParam("校区号") @RequestParam(value = "xqh") Integer xqh,
                         @ApiParam("建筑物号") @RequestParam(value = "jzwh") Integer jzwh,
                         @ApiParam("网络端口") @RequestParam(value = "wldk") String wldk,
                         @ApiParam("房间编码") @RequestParam(value = "fjbm") String fjbm,
                         @ApiParam("楼层号") @RequestParam(value = "lch") Integer lch,
                         @ApiParam("可住人数") @RequestParam(value = "kzrs") Integer kzrs,
                         @ApiParam("电表读数") @RequestParam(value = "dbds") Integer dbds,
                         @ApiParam("水表读数") @RequestParam(value = "sbds") Integer sbds,
                         @ApiParam("是否有电视机") @RequestParam(value = "sfdsj") Integer sfdsj,
                         @ApiParam("电话号码") @RequestParam(value = "dhhm") String dhhm,
                         @ApiParam("是否网络") @RequestParam(value = "sfwl") Integer sfwl,
                         @ApiParam("是否洗手间") @RequestParam(value = "sfxsj") Integer sfxsj,
                         @ApiParam("入住性别") @RequestParam(value = "rzxb") Integer rzxb,
                         @ApiParam("宿舍状态") @RequestParam(value = "status") Integer status) {
        VeDormSShe veDormSShe = new VeDormSShe();
        veDormSShe.setXqh(xqh);
        veDormSShe.setJzwh(jzwh);
        veDormSShe.setFjbm(fjbm);
        veDormSShe.setLch(lch);
        veDormSShe.setKzrs(kzrs);
        veDormSShe.setDbds(dbds);
        veDormSShe.setSbds(sbds);
        veDormSShe.setSfdsj(sfdsj);
        veDormSShe.setDhhm(dhhm);
        veDormSShe.setSfwl(sfwl);
        veDormSShe.setSfxsj(sfxsj);
        veDormSShe.setRzxb(rzxb);
        veDormSShe.setStatus(status);
        veDormSShe.setWldk(wldk);
        veDormSShe.setSsbz("1");
        veDormSShe.setId(id);

        veDormSuSheAdminService.editSuShe(veDormSShe);
        return Result.OK();
    }

    //导出excel模板
    @AutoLog(value = "宿舍信息导出excel模板")
    @ApiOperation(value = "宿舍信息导出excel模板", notes = "宿舍管理—宿舍信息导出excel模板")
    @RequestMapping(value = "exportModel")
    public ModelAndView exportModel() {
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, "宿舍列表");
        mv.addObject(NormalExcelConstants.CLASS, VeDormSSheVO.class);

        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ExportParams exportParams = new ExportParams("宿舍列表数据", "导出人:" +user.getId(), "导出信息");
        List<VeDormSSheVO> list = new ArrayList<>();
        VeDormSSheVO v = new VeDormSSheVO();
        v.setXqh("北新1111111");
        v.setJzwh("学生公寓2");
        v.setLch("2楼");
        v.setFjbm("201");
        v.setRzxb("男");
        v.setKzrs(4);
        v.setYzrs(3);
        v.setWldk("无");
        v.setSbds(16);
        v.setDbds(50);
        v.setSsbz("干净卫生");
        list.add(v);
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
//        mv.addObject(NormalExcelConstants.DATA_LIST, new ArrayList<VeDormSSheVO>());
        mv.addObject(NormalExcelConstants.DATA_LIST, list);
        return mv;
    }

    //导入
    @AutoLog(value = "宿舍信息导入excel表格")
    @ApiOperation(value = "宿舍信息导入excel表格", notes = "宿舍楼管理—宿舍楼信息导入excel表格")
    @RequestMapping(value = "import")
    public Result<?> excelImport(MultipartFile file) {
        ImportParams params = new ImportParams( );
        params.setTitleRows(2);
        params.setHeadRows(1);
        params.setNeedSave(true);
        try {
            List<VeDormSSheVO> list = ExcelImportUtil.importExcel(file.getInputStream(), VeDormSSheVO.class, params);
            String msg = veDormSuSheAdminService.excelImport(list);
            if("导入成功".equals(msg)){
                return Result.OK("文件导入成功！数据行数：" + list.size());
            }else {
                return Result.error(msg);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("文件导入失败:" + e.getMessage());
        } finally {
            try {
                file.getInputStream().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
