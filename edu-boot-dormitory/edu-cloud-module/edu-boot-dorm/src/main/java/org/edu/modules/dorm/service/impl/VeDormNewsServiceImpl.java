package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormNews;
import org.edu.modules.dorm.mapper.VeDormNewsMapper;
import org.edu.modules.dorm.mapper.VeDormNewsTypeMapper;
import org.edu.modules.dorm.service.VeDormNewsService;
import org.edu.modules.dorm.vo.VeDormNewsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class VeDormNewsServiceImpl extends ServiceImpl<VeDormNewsMapper, VeDormNews> implements VeDormNewsService {
   @Autowired
   private VeDormNewsMapper veDormNewsMapper;
   @Autowired
    private VeDormNewsTypeMapper veDormNewsTypeMapper;


    @Override
    public List<VeDormNewsVo> queryNewsByName(String title, Long typeId) {

        return veDormNewsMapper.findNews(title,typeId);
    }

  @Override
  public VeDormNewsVo updateMessage(Long id) {
    return veDormNewsMapper.updateMessage(id);
  }
}
