package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 学生宿舍来访情况登记表
 * @Author:
 * @Date:   2021-03-03
 * @Version: V1.0
 */
@Data
@TableName("ve_dorm_laifang")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormLaiFangVo implements Serializable {
    /**id*/
    @ApiModelProperty("id")
    private Long id;
    /**姓名*/
    @Excel(name = "姓名",width = 15)
    @ApiModelProperty(value = "姓名")
    private String XM;
    /**身份证号*/
    @Excel(name = "身份证号",width = 15)
    @ApiModelProperty(value = "身份证号")

    private String SFZH;
    /**被探访人员*/
    @Excel(name = "被探访人员",width = 15)
    @ApiModelProperty(value = "被探访人员")

    private Long userId;
    /**房间编号*/
    @Excel(name = "被探访人员",width = 15)
    @ApiModelProperty(value = "被探访人员")

    private Long FJBH;
    /**探访事由*/
    @Excel(name = "探访事由",width = 200)
    @ApiModelProperty(value = "探访事由")

    private String TFSY;
    /**探访类型（数据字典）*/
    @Excel(name = "探访类型",width = 15)
    @ApiModelProperty(value = "探访类型")
    private Long TFLX;
    /**登记时间*/
    @Excel(name = "登记时间",width = 15)
    @ApiModelProperty(value = "登记时间")

    private Long DJSJ;
    /**来访申请时间*/
    @Excel(name = "来访申请时间",width = 15)
    @ApiModelProperty(value = "来访申请时间")

    private Long applyTime;
    /**登记人*/
    @Excel(name = "登记人",width = 15)
    @ApiModelProperty(value = "登记人")
    private Long DJR;
    /**申请离开时间*/
    @Excel(name = "申请离开时间",width = 15)
    @ApiModelProperty(value = "申请离开时间")

    private Long leaveTime;
    /**离开时间*/
    @Excel(name = "离开时间",width = 15)
    @ApiModelProperty(value = "离开时间")

    private Long LKSJ;
    /**离开登记人*/
    @Excel(name = "离开登记人",width = 15)
    @ApiModelProperty(value = "离开登记人")

    private Long LKDJR;
    /**状态（0 已登记 1已离开 2已申请）*/
    @Excel(name = "状态",width = 15)
    @ApiModelProperty(value = "状态")
    private Integer status;
    @Excel(name = "被探访人",width = 15)
    @ApiModelProperty(value = "被探访人")
    private String userName;
    @Excel(name = "房间号",width = 15)
    @ApiModelProperty(value = "房间号")
    private String roomNum;
    @Excel(name = "探访类型",width = 15)
    @ApiModelProperty(value = "探访类型")
    private String TFType;
    @Excel(name = "校区Id",width = 15)
    @ApiModelProperty(value = "校区Id")
    private Long XQId;
    @Excel(name = "宿舍楼Id",width = 15)
    @ApiModelProperty(value = "宿舍楼Id")
    private Long SSLId;
    @Excel(name = "宿舍Id",width = 15)
    @ApiModelProperty(value = "宿舍Id")
    private Long SSId;
  /**类型*/
  @Excel(name = "类型",width = 15)
  @ApiModelProperty("类型")
  private String  zcType;


}
