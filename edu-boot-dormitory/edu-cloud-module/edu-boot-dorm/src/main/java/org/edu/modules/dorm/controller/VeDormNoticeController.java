package org.edu.modules.dorm.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.dorm.entity.VeDormNotice;
import org.edu.modules.dorm.service.VeDormNoticeService;
import org.edu.modules.dorm.vo.VeDormNoticeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags="宿舍通知模块")
@RestController
@RequestMapping("dorm/notice")
@Slf4j
public class VeDormNoticeController extends BaseController<VeDormNotice, VeDormNoticeService> {
    @Autowired
    private VeDormNoticeService veDormNoticeService;

    /**
     * 新增通知
     */
    @AutoLog(value = "新增通知")
    @ApiOperation(value = "新增通知",notes = "新增通知")
    @PostMapping("/add")
    public Result<?> addNotice(@ApiParam(value = "宿舍楼Id") @RequestParam(value = "rowId",required = false) Long rowId,
                               @ApiParam(value = "校区ID") @RequestParam("campusId") String campusId,
                               @ApiParam(value = "标题名称") @RequestParam("title") String title,
                               @ApiParam(value = "通知内容") @RequestParam("content") String content,
                               @ApiParam(value = "通知类型") @RequestParam("type") Long type) {
        Long campusid = Long.parseLong(campusId);
        VeDormNotice veDormNotice = new VeDormNotice();
        Long time = System.currentTimeMillis()/1000;
        veDormNotice.setCreateTime(time);
        veDormNotice.setContent(content);
        veDormNotice.setType(type);
        veDormNotice.setTitle(title);
        veDormNotice.setRowId(rowId);
        veDormNotice.setTerminalId(1L);
        veDormNotice.setCampusId(campusid);
        //操作用户
        veDormNotice.setCreateUserId(0L);
        veDormNoticeService.save(veDormNotice);
        return Result.OK("添加成功！");
    }

    /**
     * 根据标题进行模糊搜索
     */
    @ApiOperation(value = "根据标题进行模糊搜索",notes = "根据标题进行模糊搜索")
    @PostMapping("/queryByName")
    public Result<?> queryByName(@ApiParam(value = "通知标题") @RequestParam("title") String title) {
        List<VeDormNoticeVo> list = veDormNoticeService.queryByName(title);
        return Result.OK(list);
    }

    /**
     * 根据Id获取编辑信息
     */
    @ApiOperation(value = "根据Id获取编辑信息",notes = "根据Id获取编辑信息")
    @PostMapping("/updateMessage")
    public Result<?> updateMessage(@ApiParam(value = "通知ID") @RequestParam("id") Long id) {
        VeDormNotice veDormNotice= veDormNoticeService.getById(id);
        return Result.OK(veDormNotice);
    }

    /**
     * 编辑通知
     */
    @ApiOperation(value = "编辑通知",notes = "编辑通知")
    @PostMapping("/update")
    public Result<?> updateMessage(@ApiParam(value = "通知Id") @RequestParam("id") Long id,
                                   @ApiParam(value = "宿舍楼ID") @RequestParam("rowId") Long rowId,
                                   @ApiParam(value = "校区ID") @RequestParam("campusId") String campusId,
                                   @ApiParam(value = "通知标题") @RequestParam("title") String title,
                                   @ApiParam(value = "通知内容") @RequestParam("content") String content,
                                   @ApiParam(value = "通知类型") @RequestParam("type") Long type) {
        Long campusid = Long.parseLong(campusId);

        VeDormNotice veDormNotice = new VeDormNotice();
        veDormNotice.setContent(content);
        veDormNotice.setType(type);
        veDormNotice.setTitle(title);
        veDormNotice.setRowId(rowId);
        veDormNotice.setId(id);
        veDormNotice.setCampusId(campusid);

        veDormNoticeService.updateById(veDormNotice);
        return Result.OK("编辑成功！");
    }

    /**
     * 根据Id删除
     */
    @ApiOperation(value = "根据Id删除", notes = "根据Id删除")
    @PostMapping("/delete")
    public Result<?> delete(@ApiParam(value = "通知Id") @RequestParam("id") Long id){
        veDormNoticeService.removeById(id);
        return Result.OK("删除成功！");
    }

}
