package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.dorm.entity.VeDormNewsType;

public interface VeDormNewsTypeMapper extends BaseMapper<VeDormNewsType> {

}
