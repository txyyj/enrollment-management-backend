package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormChange;
import org.edu.modules.dorm.entity.VeDormStudent;
import org.edu.modules.dorm.mapper.VeDormChangeMapper;
import org.edu.modules.dorm.mapper.VeDormStudentMapper;
import org.edu.modules.dorm.service.VeDormChangeService;
import org.edu.modules.dorm.vo.VeDormChangeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VeDormChangeServiceImpl extends ServiceImpl<VeDormChangeMapper, VeDormChange> implements VeDormChangeService {
    @Autowired
    private VeDormStudentMapper veDormStudentMapper;
    @Autowired
    private VeDormChangeMapper veDormChangeMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public Boolean approveChange(VeDormChange veDormChange, VeDormStudent veDormStudent) {
        QueryWrapper<VeDormStudent> wrapper = new QueryWrapper<>();
        wrapper.eq("id", veDormStudent.getUserId());
        VeDormStudent veDormStudent1 = veDormStudentMapper.selectOne(wrapper);
        veDormStudent.setId(veDormStudent1.getId());
        veDormStudentMapper.updateById(veDormStudent);
        Integer i = veDormChangeMapper.updateById(veDormChange);
        return i != 0;
    }

    @Override
    public List<VeDormChangeVo> applyMessage(String name) {
        return veDormChangeMapper.applyMessage( name);
    }

    @Override
    public VeDormChangeVo approveMessage(Long id) {
        return veDormChangeMapper.approveMessage(id);
    }

}
