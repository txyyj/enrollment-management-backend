package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormNotice;
import org.edu.modules.dorm.mapper.VeDormNoticeMapper;
import org.edu.modules.dorm.mapper.VeDormSuSheLouMapper;
import org.edu.modules.dorm.service.VeDormNoticeService;
import org.edu.modules.dorm.vo.VeDormNoticeVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class VeDormNoticeServiceImpl extends ServiceImpl<VeDormNoticeMapper, VeDormNotice> implements VeDormNoticeService {
    @Autowired
    private VeDormSuSheLouMapper veDormSuSheLouMapper;
    @Autowired
    private VeDormNoticeMapper veDormNoticeMapper;

    @Override
    public List<VeDormNoticeVo> queryByName(String title) {
        return veDormNoticeMapper.queryByTitle(title);
    }

}
