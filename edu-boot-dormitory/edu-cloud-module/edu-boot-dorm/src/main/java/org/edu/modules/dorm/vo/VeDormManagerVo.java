package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Auther 李少君
 * @Date 2021-07-14 10:44
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormManagerVo对象", description = "获取添加管理员中已添加的管理员")
public class VeDormManagerVo implements Serializable {
    @ApiModelProperty(value = "宿舍楼id")
    private Integer id;

    @ApiModelProperty(value = "学生公寓名")
    private String jzwmc;

    @ApiModelProperty(value = "管理员id")
    private Integer adminId;

    @ApiModelProperty(value = "管理员")
    private String xm;
}
