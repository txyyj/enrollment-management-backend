package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormSSVo ", description = "宿舍信息展示")
public class VeDormSSVo {

    @Excel(name = "id", width = 15)
    @ApiModelProperty(value = "id")
    private Integer id;

    @Excel(name = "校区id", width = 15)
    @ApiModelProperty(value = "校区id")
    private Integer xqid;

    @ApiModelProperty(value = "校区名")
    private String xqmc;

    @Excel(name = "建筑物id", width = 15)
    @ApiModelProperty(value = "建筑物id")
    private Integer jzwid;

    @Excel(name = "楼层id", width = 15)
    @ApiModelProperty(value = "楼层")
    private Integer lcid;

    @Excel(name = "校区号", width = 15)
    @ApiModelProperty(value = "校区号")
    private Integer xqh;

    @Excel(name = "宿舍楼编号", width = 15)
    @ApiModelProperty(value = "宿舍楼编号")
    private Integer jzwh;

    @Excel(name = "楼层号", width = 15)
    @ApiModelProperty(value = "楼层号")
    private Integer lch;

    @Excel(name = "宿舍名称", width = 15)
    @ApiModelProperty(value = "宿舍名称")
    private String fjbm;

    @Excel(name = "可住人数", width = 15)
    @ApiModelProperty(value = "已住人数")
    private Integer kzrs;

    @Excel(name = "已住人数", width = 15)
    @ApiModelProperty(value = "已住人数")
    private Integer yzrs;

    @Excel(name = "电话号码", width = 15)
    @ApiModelProperty(value = "电话号码")
    private String dhhm;

    @Excel(name = "水表截至码", width = 15)
    @ApiModelProperty(value = "水表截至码")
    private Integer sbds;

    @Excel(name = "电表截至码", width = 15)
    @ApiModelProperty(value = "电表截至码")
    private Integer dbds;

    @Excel(name = "状态", width = 15)
    @ApiModelProperty(value = "状态")
    private Integer status;

}
