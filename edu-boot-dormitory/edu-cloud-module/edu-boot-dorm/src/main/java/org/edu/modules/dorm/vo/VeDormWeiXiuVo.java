package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
public class VeDormWeiXiuVo implements Serializable {

        @Excel(name="维修Id",width = 15)
        @ApiModelProperty(value = "维修id")
        private Integer id;

        @Excel(name = "宿舍id", width = 15)
        @ApiModelProperty(value = "宿舍id")
        private Integer ssid;

        @Excel(name = "维修信息", width = 15)
        @ApiModelProperty(value = "维修信息")
        private String title;

        @Excel(name = "维修内容", width = 15)
        @ApiModelProperty(value = "维修内容")
        private String content;

        @Excel(name = "创建时间", width = 15)
        @ApiModelProperty(value = "创建时间")
        private int addtime;

        @Excel(name = "创建人", width = 15)
        @ApiModelProperty(value = "创建人")
        private int  createuserid;

        @Excel(name = "创建时间", width = 15)
        @ApiModelProperty(value = "创建时间")
        private Long createtime;

        @Excel(name = "房间编码", width = 15)
        @ApiModelProperty(value = "房间编码")
        private String fjbm;

}
