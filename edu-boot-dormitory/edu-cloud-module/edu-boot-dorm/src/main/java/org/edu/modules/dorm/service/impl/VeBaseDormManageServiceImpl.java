package org.edu.modules.dorm.service.impl;

import com.alibaba.fastjson.JSON;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.edu.modules.dorm.config.HttpURLConnectionUtil;
import org.edu.modules.dorm.entity.bo.*;
import org.edu.modules.dorm.service.VeBaseDormManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.sound.midi.Soundbank;
import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-07-07 11:07 调用接口获取数据
 */
@Service
public class VeBaseDormManageServiceImpl implements VeBaseDormManageService {

    @Value("${common.host}")
    private String dirHost ;

    @Autowired
    ObjectMapper mapper;

    //获取校区信息
    @Override
    public BasicResponseBO<List> getCampusMessage(){
        String url = "common/veCommon/queryCampusList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
        BasicResponseBO<List> b = getUrl(url,List.class) ;
//        BasicResponseBO<List<VeCampusBO>> responseBO = new BasicResponseBO<>();
//        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
//                .setResult(b.getResult() == null ? null :
//                        JSON.parseArray(JSON.toJSONString(b.getResult()), VeCampusBO.class)) ;
        return b;
    }

    //获取学期信息
    @Override
    public BasicResponseBO<List> getSemesterMessage() {
        String url = "common/veCommon/querySemesterList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        return b;
    }

    //获取年级信息
    @Override
    public BasicResponseBO<List> getGradeMessage() {
        String url = "common/veCommon/queryGradeList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        return b;
    }


    //获取院系信息
    @Override
    public BasicResponseBO<List> getFacultyMessage() {
        String url = "common/veCommon/queryFacultyList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        return b;
    }

    //根据院系id获取专业信息
    @Override
    public BasicResponseBO<List> getSpecialtyMessage(Integer xyId) {
        String url = "common/veCommon/querySpecialtyListByFalId?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67"+"&falId"+"="+xyId;
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        return b;
    }

    //通过专业id获取专业信息
    @Override
    public BasicResponseBO<List> getSpecialtyMessageById(Integer zyId) {
        String url = "common/veCommon/querySpecialtyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67"+"&specialtyId"+"="+zyId;
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        return b;
    }

    //获取班级信息
    @Override
    public BasicResponseBO<List> getBanjiMessage() {
        String url = "common/veCommon/queryBanJiList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        return b;
    }

    //获取学生信息
    @Override
    public BasicResponseBO<List> getStudentMessage() {
        String url = "common/veCommon/getStudentList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        return b;
    }

    @Override
    public BasicResponseBO<List> getTeacherMessage() {
        String url = "common/veCommon/getTeacherList?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        return b;
    }

    //根据专业id获取班级信息
    @Override
    public BasicResponseBO<List> getBanjiMessage(Integer zyId) {
        String url = "common/veCommon/queryBanJiListBySpecId?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67"+"&specId"+"="+zyId;
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        return b;
    }


    /**
     * 调用公告数据接口
     */
    private <T> BasicResponseBO<T> getUrl(String url, Class<T> clazz) {
        try {
            url = dirHost+url ;
            String result = HttpURLConnectionUtil.doGet(url) ;
            BasicResponseBO<T> re = mapper.readValue(result, new TypeReference<BasicResponseBO<T>>() {});
            T body = mapper.readValue(mapper.writeValueAsString(re.getResult()), clazz);
            re.setResult(body);
            return re;
        } catch (Exception e) {
            return new BasicResponseBO<T>().setSuccess(false).setMessage("操作失败");
        }
    }


}
