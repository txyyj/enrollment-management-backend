package org.edu.modules.dorm.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.dorm.entity.VeDormCheck;
import org.edu.modules.dorm.service.VeDormCheckService;
import org.edu.modules.dorm.vo.VeDormCheckVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@Api(tags="宿舍考勤管理")
@RestController
@RequestMapping("dorm/check")
@Slf4j
public class VeDormCheckController extends BaseController<VeDormCheck, VeDormCheckService> {
    @Autowired(required = false)
    private VeDormCheckService veDormCheckService;

    /**
     * 1、考勤列表。（通过姓名、学号模糊查询，宿舍楼编号精准查询）
     * @param XM 姓名
     * @param XH 学号
     * @param SSLBM 宿舍楼编号
     * */
    @ApiOperation(value="通过姓名、学号模糊查询，宿舍楼编号精准查询", notes="站点可选择模板-通过姓名、学号模糊查询，宿舍楼编号精准查询")
    @PostMapping(value = "/selectList")
    public Result<?> selectList(@ApiParam(value = "姓名") @RequestParam(name = "XM") String XM,
                                @ApiParam(value = "学号") @RequestParam(name = "XH") String XH,
                                @ApiParam(value = "宿舍楼编号") @RequestParam(name = "SSLBM") Long SSLBM) {
        ArrayList<VeDormCheckVo> list = (ArrayList<VeDormCheckVo>) veDormCheckService.selectList(XM, XH, SSLBM);
        return Result.OK(list);
    }
    
    /**
     * 2、通过id查询。（违纪管理-通用）
     * @param id 考勤id
     */
    @ApiOperation(value="通过id查询", notes="站点可选择模板-通过id查询")
    @PostMapping(value = "/selectById")
    public Result<?> selectById(@ApiParam(value = "考勤id") @RequestParam(name = "id") Long id) {
        VeDormCheckVo veDormCheckVo = veDormCheckService.selectById(id);
        if(veDormCheckVo != null) {
            return Result.OK(veDormCheckVo);
        } else {
            return Result.error("未找到对应数据！");
        }
    }

    /**
     * 3、添加考勤。
     * @param stuId 考勤学生id
     * @param checkCode 考勤规则编码
     * 未包含参数 checkTime 考勤日期
     * //未包含参数 createUserId 考勤记录人id
     * 未包含参数 terminalId 终端id
     */
    @AutoLog(value = "站点可选择模板-添加考勤")
    @ApiOperation(value="添加考勤", notes="站点可选择模板-添加考勤")
    @PostMapping(value = "/insert")
    public Result<?> insert(@ApiParam(value = "考勤学生id") @RequestParam(name = "stuId") Long stuId,
                            @ApiParam(value = "考勤规则编码") @RequestParam(name = "checkCode") String checkCode) {
        VeDormCheck veDormCheck = new VeDormCheck();
        veDormCheck.setStuId(stuId);
        veDormCheck.setCheckCode(checkCode);
        Long createTime = System.currentTimeMillis() / 1000;
        veDormCheck.setCreateTime(createTime);
        veDormCheck.setCheckTime(createTime.toString()) ;
        veDormCheck.setTerminalId(1L) ;
//        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
//        if(Objects.nonNull(user)){
//            veDormCheck.setCreateUserId(Long.parseLong(user.getId())) ;
//        }
        boolean isInsert = veDormCheckService.save(veDormCheck);
        if(isInsert) {
            return Result.OK("添加成功！");
        } else {
            return Result.error("添加失败！");
        }
    }

}
