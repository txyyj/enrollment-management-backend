package org.edu.modules.dorm.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.common.system.vo.LoginUser;
import org.edu.modules.dorm.entity.VeDormLouceng;
import org.edu.modules.dorm.entity.bo.BasicResponseBO;
import org.edu.modules.dorm.entity.bo.VeCampusBO;
import org.edu.modules.dorm.service.VeBaseCampusService;
import org.edu.modules.dorm.service.VeBaseDormManageService;
import org.edu.modules.dorm.service.VeDormLoucengService;
import org.edu.modules.dorm.vo.VeDormLouCengVo;
import org.edu.modules.dorm.vo.VeDormLoucengExcelVO;
import org.edu.modules.dorm.vo.VeFindStudentVo;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Api(tags="楼层管理")
@RestController
@RequestMapping("dorm/floorMng")
@Slf4j
@ApiSort(value = 100)
public class VeDormLoucengController extends BaseController<VeDormLouceng, VeDormLoucengService> {
    @Autowired
    private VeDormLoucengService veDormLoucengService;
    @Autowired
    private VeBaseCampusService veBaseCampusService;
    @Autowired
    private VeBaseDormManageService veBaseDormManageService; //李少君

    //添加楼层
    @AutoLog(value = "楼层管理-添加楼层")
    @ApiOperation(value="添加楼层", notes="楼层管理-添加楼层")
    @PostMapping(value = "/addLouCeng")
    public Result<?> add(@ApiParam(value = "校区号") @RequestParam(value ="xqh" , required=false) Integer xqh,
                         @ApiParam(value = "建筑物号") @RequestParam(value = "jzwh" , required=false) Integer jzwh,
                         @ApiParam(value = "楼层名称") @RequestParam(value ="lcName" ) String lcName,
                         @ApiParam(value = "楼层代码") @RequestParam(value = "lcCode") String lcCode,
                         @ApiParam(value = "是否有摄像头") @RequestParam(value = "sfsxt", required=false) Integer sfsxt,
                         @ApiParam(value = "系统id") @RequestParam(value = "terminalId", required=false) Integer terminalId) {

        QueryWrapper<VeDormLouceng> wrapper=new QueryWrapper<>();
        wrapper.eq("lcName",lcName);
        wrapper.eq("jzwh",jzwh);
        VeDormLouceng suShe=veDormLoucengService.getOne(wrapper);
        if (suShe!=null){
            return Result.error("该宿舍楼已经存在该楼层，添加失败");

        }
        VeDormLouceng veDormLouceng = new VeDormLouceng();
        veDormLouceng.setXqh(xqh);
        veDormLouceng.setJzwh(jzwh);
        veDormLouceng.setLcname(lcName);
        veDormLouceng.setLccode(lcCode);
        veDormLouceng.setSfsxt(sfsxt);
        veDormLouceng.setTerminalid(terminalId);

        veDormLoucengService.insertNew(veDormLouceng);
        return Result.OK();
    }

   @AutoLog(value = "楼层管理-校区下拉框")
   @ApiOperation(value="校区下拉框", notes="楼层管理-校区下拉框")
   @PostMapping(value = "/selectXQ")
   public Result<?> queryXQ() {
        return Result.OK( veBaseCampusService.list());
   }

    //通过校区和宿舍楼id尽心查询
    @AutoLog(value = "楼层管理-宿舍楼层查询")
    @ApiOperation(value="宿舍楼层查询", notes="楼层管理-宿舍楼查询")
    @PostMapping(value = "/select")
    public Result<?> query(@ApiParam(value = "校区id") @RequestParam(value ="xqId", required = false) Integer xqId,
                           @ApiParam(value = "宿舍楼id") @RequestParam(value ="sslId", required = false) Integer sslId) {
//        BasicResponseBO<List> list = veBaseDormManageService.getCampusMessage(); //李少君
//        List<VeCampusBO> result = JSONObject.parseArray(JSON.toJSONString(list.getResult()),VeCampusBO.class);
//        List<VeDormLouCengVo> v = veDormLoucengService.selectByParam(xqId,sslId);
//        for(int i =0 ;i<v.size();i++){
//            for (int j =0 ;j<result.size();j++){
//                if(v.get(i).getXqh()==(result.get(j).getId().longValue())){
//                    v.get(i).setXqmc(result.get(j).getXqmc());
//                }
//            }
//
//        }
//
//
//        return Result.OK(v);
        return Result.OK( veDormLoucengService.selectByParam(xqId,sslId));
    }

    //宿舍楼层编辑
    @AutoLog(value = "楼层管理-编辑楼层")
    @ApiOperation(value="编辑楼层", notes="楼层管理-编辑楼层")
    @PostMapping(value = "/edit")
    public Result<?> edit(@ApiParam(value = "id") @RequestParam(value ="id") Integer id,
                          @ApiParam(value = "校区号") @RequestParam(value ="xqh", required=false ) Integer xqh,
                          @ApiParam(value = "建筑物号") @RequestParam(value = "jzwh", required=false ) Integer jzwh,
                          @ApiParam(value = "楼层名称") @RequestParam(value ="lcName") String lcName,
                          @ApiParam(value = "楼层代码") @RequestParam(value = "lcCode") String lcCode,
                          @ApiParam(value = "是否有摄像头") @RequestParam(value = "sfsxt", required = false) Integer sfsxt,
                          @ApiParam(value="系统id") @RequestParam(value = "terminalId", required = false) Integer terminalId) {
        VeDormLouceng veDormLouceng = new VeDormLouceng();
        veDormLouceng.setId(id);
        veDormLouceng.setXqh(xqh);
        veDormLouceng.setJzwh(jzwh);
        veDormLouceng.setLcname(lcName);
        veDormLouceng.setLccode(lcCode);
        veDormLouceng.setSfsxt(sfsxt);
        veDormLouceng.setTerminalid(terminalId);

        veDormLoucengService.updateById(veDormLouceng);
        return Result.OK();
    }


    @AutoLog(value = "楼层管理-楼层删除")
    @ApiOperation(value="楼层删除", notes="楼层管理-楼层删除")
    @PostMapping(value = "/delete")
    public Result<?> delete(@ApiParam(value = "楼层id") @RequestParam(value = "id") Integer id) {
        veDormLoucengService.deleteLouceng(id);
        return Result.OK();
    }

    @ApiOperation(value="获取编辑信息", notes="获取编辑信息-楼层删除")
    @PostMapping(value = "/getEdit")
    public Result<?> getEdit(@ApiParam(value = "楼层id") @RequestParam("id") String id) {
        return Result.OK( veDormLoucengService.getById(id));
    }

    @ApiOperation(value="", notes="楼层管理-批量删除")
    @PostMapping(value = "/deleteMany")
    public Result<?> deleteMany(@ApiParam("批量删除id") @RequestParam(value ="ids") String ids) {
        veDormLoucengService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功！");
    }

    @AutoLog(value = "导出excel模板")
    @ApiOperation(value = "导出excel模板", notes = "导出excel模板")
    @RequestMapping(value = "exportModel")
    public ModelAndView exportModel() {
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        mv.addObject(NormalExcelConstants.FILE_NAME, "楼层列表");
        mv.addObject(NormalExcelConstants.CLASS, VeDormLoucengExcelVO.class);

        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        ExportParams exportParams = new ExportParams("宿舍列表数据", "导出人:" +user.getId(), "导出信息");

        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, new ArrayList<VeDormLoucengExcelVO>());
        return mv;
    }

    @AutoLog(value = "导入excel表格")
    @ApiOperation(value = "导入excel表格", notes = "导入excel表格")
    @PostMapping(value = "import")
    public Result<?> excelImport(MultipartFile file) {
        ImportParams params = new ImportParams( );
        params.setTitleRows(2);
        params.setHeadRows(1);
        params.setNeedSave(true);
        try {
            List<VeDormLoucengExcelVO> list = ExcelImportUtil.importExcel(file.getInputStream(), VeDormLoucengExcelVO.class, params);
            veDormLoucengService.excelImport(list);
            return Result.OK("文件导入成功！数据行数：" + list.size());
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("文件导入失败:" + e.getMessage());
        } finally {
            try {
                file.getInputStream().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }


}
