package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

@Data
@TableName("ve_base_campus")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeBaseCampus implements Serializable {

    //校区表
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Integer id;

    @Excel(name = "校区代码", width = 15)
    @ApiModelProperty(value ="校区代码")
    private String xqdm;

    @Excel(name = "校区名称", width = 15)
    @ApiModelProperty(value ="校区名称")
    private String xqmc;

    @Excel(name = "校区地址", width = 15)
    @ApiModelProperty(value ="校区地址")
    private String xqdz;

    @Excel(name = "校区联系电话", width = 15)
    @ApiModelProperty(value ="校区联系电话")
    private String xqlxdh;

    @Excel(name = "校区传真电话", width = 15)
    @ApiModelProperty(value ="校区传真电话")
    private String xqczdh;

    @Excel(name = "社区负责人用户Id", width = 15)
    @ApiModelProperty(value ="社区负责人用户Id")
    private Integer fzruserid;

    @Excel(name = "电子邮件", width = 15)
    @ApiModelProperty(value ="电子邮件")
    private String dzyj;

    @Excel(name = "校区邮政编码", width = 15)
    @ApiModelProperty(value ="校区邮政编码")
    private String xqyzbm;

    @Excel(name = "校区所在地行政区划码", width = 15)
    @ApiModelProperty(value ="校区所在地行政区划码")
    private String xqszdxcqhm;

    @Excel(name = "校区面积", width = 15)
    @ApiModelProperty(value ="校区面积")
    private float xqmj;

    @Excel(name = "校区建筑面积", width = 15)
    @ApiModelProperty(value ="校区建筑面积")
    private float xqjzwmj;

    @Excel(name = "校区教学科研仪器设备总值", width = 15)
    @ApiModelProperty(value ="校区教学科研仪器设备总值")
    private float xqjxkysbzz;

    @Excel(name = "校区固定资产总值", width = 15)
    @ApiModelProperty(value ="校区固定资产总值")
    private float  xqgdzczz;

    @Excel(name = "系统终端Id", width = 15)
    @ApiModelProperty(value ="系统终端Id")
    private Integer terminalid;

}
