package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description: 卫生评比
 * @Author:
 * @Date:   2021-03-09
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "卫生评比", description = "卫生评比")
public class VeDormWsceilVo {

    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /**学期*/
//    @Excel(name = "学期ID", width = 15)李少君+
    @ApiModelProperty(value = "学期ID")
    private Long semId;

    /**学期*/
    @Excel(name = "学期", width = 15)
    @ApiModelProperty(value = "学期")
    private String semName;

    /**校区名*/
//    @Excel(name = "校区ID", width = 15)李少君+
    @ApiModelProperty(value = "校区ID")
    private Long campusId;

    /**校区名*/
    @Excel(name = "校区名", width = 15)
    @ApiModelProperty(value = "校区名")
    private String campusName;
    
    /**建筑物名*/
    @Excel(name = "建筑物名", width = 15)
    @ApiModelProperty(value = "建筑物名")
    private String jzwName;
    
    /**楼层名*/
    @Excel(name = "楼层名", width = 15)
    @ApiModelProperty(value = "楼层名")
    private String lcName;
    
    /**宿舍名*/
    @Excel(name = "宿舍名", width = 15)
    @ApiModelProperty(value = "宿舍名")
    private String ssName;
    
    /**周数*/
    @Excel(name = "周数", width = 15)
    @ApiModelProperty(value = "周数")
    private Long weekNum;
    
    /**周一分数*/
    @Excel(name = "周一分数", width = 15)
    @ApiModelProperty(value = "周一分数")
    private Integer one;

    /**周二分数*/
    @Excel(name = "周二分数", width = 15)
    @ApiModelProperty(value = "周二分数")
    private Integer two;
    
    /**周三分数*/
    @Excel(name = "周三分数", width = 15)
    @ApiModelProperty(value = "周三分数")
    private Integer three;
    
    /**周四分数*/
    @Excel(name = "周四分数", width = 15)
    @ApiModelProperty(value = "周四分数")
    private Integer four;
    
    /**周五分数*/
    @Excel(name = "周五分数", width = 15)
    @ApiModelProperty(value = "周五分数")
    private Integer five;
    
    /**周一Id*/
    @Excel(name = "周一Id", width = 15)
    @ApiModelProperty(value = "周一Id")
    private Integer oneId;

    /**周二Id*/
    @Excel(name = "周二Id", width = 15)
    @ApiModelProperty(value = "周二Id")
    private Integer twoId;
    
    /**周三Id*/
    @Excel(name = "周三Id", width = 15)
    @ApiModelProperty(value = "周三Id")
    private Integer threeId;
    
    /**周四Id*/
    @Excel(name = "周四Id", width = 15)
    @ApiModelProperty(value = "周四Id")
    private Integer fourId;
    
    /**周五Id*/
    @Excel(name = "周五Id", width = 15)
    @ApiModelProperty(value = "周五Id")
    private Integer fiveId;
    
    /**平均成绩*/
    @Excel(name = "平均成绩", width = 15)
    @ApiModelProperty(value = "平均成绩")
    private Integer averageScore;
    
}
