package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormLaiFang;
import org.edu.modules.dorm.vo.VeDormLaiFangVo;

import java.util.List;

public interface VeDormLFangService extends IService<VeDormLaiFang> {

    List<VeDormLaiFangVo> showLaiFang(String XM,String SFZH);

    VeDormLaiFangVo updateLaiFangMessage(Long id);

}
