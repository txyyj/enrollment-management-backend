package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.*;
import org.edu.modules.dorm.mapper.VeDormDisciplineMapper;
import org.edu.modules.dorm.mapper.VeSuSheFenPeiMapper;
import org.edu.modules.dorm.service.VeDormSuSheFenPeiService;
import org.edu.modules.dorm.vo.VeBaseStudentVo;
import org.edu.modules.dorm.vo.VeDormFenPeiVo;
import org.edu.modules.dorm.vo.VeDormSuSheStudentVo;
import org.edu.modules.dorm.vo.VeDormssfpVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VeDormSuSheFenPeiServiceImpl extends ServiceImpl<VeSuSheFenPeiMapper, VeDormStudent> implements VeDormSuSheFenPeiService {

    @Autowired
    private VeSuSheFenPeiMapper veSuSheFenPeiMapper;
    @Autowired
    private VeDormDisciplineMapper veDormDisciplineMapper;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void addStudent(VeDormStudent veDormStudent) {
        // 入住人数减一
        veSuSheFenPeiMapper.updateYzrs(1,veDormStudent.getFJBM());
        veSuSheFenPeiMapper.insert(veDormStudent) ;
    }

    @Override
    public List<VeDormFenPeiVo> selectSuSheLou(Integer id) {
       return veSuSheFenPeiMapper.selectSuSheLou(id);
    }

    @Override
    public Integer count(Integer id) {
        return veSuSheFenPeiMapper.count(id);
    }

    @Override
    public List<VeDormSuSheStudentVo> selectStudent(Integer fjbh) {
        return veSuSheFenPeiMapper.selectStudent(fjbh);
    }


    @Override
    public List<VeBaseGrade> getGrade() {
     return veSuSheFenPeiMapper.getGrade();
    }

    @Override
    public List<VeBaseFaculty> getCollege() {
      return  veSuSheFenPeiMapper.getCollege();
    }

    @Override
    public List<VeBaseSpecialty> getMajor(Integer xyId) {
        return veSuSheFenPeiMapper.getMajor(xyId);
    }

    @Override
    public List<VeBaseBanji> getBanJi(Integer gradeId,Integer zyId,Integer xyId) {

        return veSuSheFenPeiMapper.getBanJi(gradeId, zyId,xyId);
    }

    @Override
    public List<VeBaseStudentVo> queryStudent(Integer gradeId, Integer xyId, Integer zyId, Integer bjId, Long xh, String xm, Long sfzh) {
        return veSuSheFenPeiMapper.queryStudent(gradeId,xyId,zyId,bjId,xh,xm,sfzh);
    }

    @Override
    public List<VeDormLouceng> getLouCeng(Integer id) {
        return veSuSheFenPeiMapper.getLouCeng(id);
    }

    @Override
    public List<VeDormSuShe> getSuShe(Integer lcId, Integer sslId) {
       return veSuSheFenPeiMapper.getSuShe(lcId,sslId);
    }

    @Override
    public Integer lcConut(Integer lcId, Integer ssIld) {
       return veSuSheFenPeiMapper.lcConut(lcId,ssIld);
    }


    @Override
    public List<VeDormssfpVo> getSuSheInfo(Integer id) {
        return veSuSheFenPeiMapper.getSuSheInfo(id);
    }


    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer emptySuShe(Integer id) {
        veSuSheFenPeiMapper.emptyYzrs(id+0L);
      QueryWrapper<VeDormWeiJi> wrapper = new QueryWrapper<>();
      wrapper.eq("userId",id);
       List<VeDormWeiJi> list = veDormDisciplineMapper.selectList(wrapper);
      for (int i = 0; i <list.size() ; i++) {
        veDormDisciplineMapper.deleteById(list.get(i).getId());
      }
        return veSuSheFenPeiMapper.emptySuShe(id);
    }

    @Override
    public VeBaseStudentVo getBaseStudent(Integer id) {
        return veSuSheFenPeiMapper.getBaseStudent(id);
    }


    @Override
    public Integer updateYzrs(Integer num,Long id) {
        return veSuSheFenPeiMapper.updateYzrs(num,id);
    }

    @Override
    public List<VeDormSuSheStudentVo> selectStudentMore(Long xqh, Long ssl, Long ss, Long stu) {
        return veSuSheFenPeiMapper.selectStudentMore(xqh, ssl, ss, stu);
    }
}
