package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormCheckRule;

public interface VeDormCheckRuleService extends IService<VeDormCheckRule> {

    /**
     * 1、设置考勤时间。
     * @param wgEntity 晚归实体类
     * @param ybgsEntity 夜不归宿实体类
     */
    Integer updateCheckTime(VeDormCheckRule wgEntity, VeDormCheckRule ybgsEntity);

}
