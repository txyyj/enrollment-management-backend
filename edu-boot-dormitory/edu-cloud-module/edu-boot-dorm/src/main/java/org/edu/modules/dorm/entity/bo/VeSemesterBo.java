package org.edu.modules.dorm.entity.bo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Auther 李少君
 * @Date 2021-07-20 10:11
 */
@Data
@TableName("ve_base_semester")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeSemesterBo implements Serializable {

    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /**学期码*/
    @Excel(name = "学期码",width = 15)
    @ApiModelProperty(value = "学期码")
    @TableField(value = "Xqm")
    private String Xqm;

    /**学期名称*/
    @Excel(name = "学期名称",width = 15)
    @ApiModelProperty(value = "学期名称")
    @TableField(value = "Xqmc")
    private String Xqmc;

    /**学年*/
    @Excel(name = "学年",width = 15)
    @ApiModelProperty(value = "学年")
    @TableField(value = "Xn")
    private String Xn;

    /**学期开始日期*/
    @Excel(name = "学期开始日期",width = 15)
    @ApiModelProperty(value = "学期开始日期")
    @TableField(value = "Xqksrq")
    private String Xqksrq;

    /**学期结束日期*/
    @Excel(name = "学期结束日期",width = 15)
    @ApiModelProperty(value = "学期结束日期")
    @TableField(value = "XQjsrq")
    private String XQjsrq;

    /**是否当前学期*/
    @Excel(name = "是否当前学期",width = 15)
    @ApiModelProperty(value = "是否当前学期")
    @TableField(value = "iscurrent")
    private String iscurrent;

    /**终端id*/
    @Excel(name = "终端id",width = 15)
    @ApiModelProperty(value = "终端id")
    @TableField(value = "terminalid")
    private Long terminalid;

    private String xqksrqName;

    private String xqjsrqName;
}
