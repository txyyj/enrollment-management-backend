package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 宿舍考勤设置
 * @Author:
 * @Date:   2021-03-08
 * @Version: V1.0
 */

@Data
@TableName("ve_dorm_check_rule")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormCheckRule对象", description = "宿舍考勤设置")
public class VeDormCheckRule implements Serializable {

    /** （1）自增id。 */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "自增id")
    private Long id;

    /** （2）名称。 */
    @Excel(name = "名称", width = 15)
    @ApiModelProperty(value = "名称")
    @TableField(value = "name")
    private String name;

    /** （3）编码。 */
    @Excel(name = "编码", width = 15)
    @ApiModelProperty(value = "编码")
    @TableField(value = "code")
    private String code;

    /** （4）限制时间（起始时间）。 */
    @Excel(name = "限制时间", width = 15)
    @ApiModelProperty(value = "限制时间（起始时间）")
    @TableField(value = "limitTime")
    private String limitTime;

    /** （5）终端id。 */
    @Excel(name = "终端id", width = 15)
    @ApiModelProperty(value = "终端id")
    @TableField(value = "terminalId")
    private Long terminalId;

}
