package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 宿舍展示
 * @Author:
 * @Date:   2021-03-02
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormSuSheVo implements Serializable {

    /**id*/
    @TableId(value = "id")
    @ApiModelProperty(value = "id")
    private Integer id;
    
    /**宿舍名称*/
    @Excel(name = "宿舍名称", width = 15)
    @ApiModelProperty(value = "宿舍名称")
    private String FJBM;
    
    /**楼层号*/
    @Excel(name = "楼层号", width = 15)
    @ApiModelProperty(value = "楼层号")
    private Long LCH;

    /**入住性别*/
    @Excel(name = "入住性别", width = 15)
    @ApiModelProperty(value = "入住性别 0待定 1男 2女")
    private Integer RZXB;

    /**可住人数*/
    @Excel(name = "可住人数", width = 15)
    @ApiModelProperty(value = "可住人数")
    private Integer KZRS;

    /**已住人数*/
    @Excel(name = "已住人数", width = 15)
    @ApiModelProperty(value = "已住人数")
    private Integer YZRS;

}
