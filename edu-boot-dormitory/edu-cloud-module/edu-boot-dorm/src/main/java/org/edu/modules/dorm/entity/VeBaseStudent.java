package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Auther 李少君
 * @Date 2021-07-20 11:35
 */
@Data
@TableName("ve_base_student")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeBaseStudent implements Serializable {

    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    private String ksh;

    private String xbm;

    private Integer xz;

    private Integer shengId;

    private String city;

    private Integer falId;

    private String county;

    private Integer cityId;

    private Integer terminalId;

    private String province;

    private Integer countyId;

    private Integer specId;

    private String mzm;

    private Integer gradeId;

    private Long rxny;

    private String zkzh;

    private String updateTime;

    private Integer shiId;

    private Integer quId;

    private Long userId;

    private Integer bjId;

    private Integer provinceId;

    private String xh;

    private String sfzh;

    private String xsdqztm;

    private String xm;

    private String createTime;

    private Integer jdfs;

    private Integer updateStatus;

    private Integer sfkns;

    private String bmh;

}
