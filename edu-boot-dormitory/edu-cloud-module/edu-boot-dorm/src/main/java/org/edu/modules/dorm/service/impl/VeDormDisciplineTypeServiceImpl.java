package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormDisciplineType;
import org.edu.modules.dorm.mapper.VeDormDisciplineTypeMapper;
import org.edu.modules.dorm.service.VeDormDisciplineTypeService;
import org.springframework.stereotype.Service;

@Service
public class VeDormDisciplineTypeServiceImpl extends ServiceImpl<VeDormDisciplineTypeMapper, VeDormDisciplineType> implements VeDormDisciplineTypeService {

}
