package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormStudent;
import org.edu.modules.dorm.mapper.VeDormStudentMapper;
import org.edu.modules.dorm.service.VeDormStudentService;
import org.springframework.stereotype.Service;

@Service
public class VeDormStudentServiceImpl extends ServiceImpl<VeDormStudentMapper, VeDormStudent> implements VeDormStudentService {

}
