package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeBaseCalendar;
import org.edu.modules.dorm.mapper.VeBaseCalendarMapper;
import org.edu.modules.dorm.service.VeBaseCalendarService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VeBaseCalendarServiceImpl extends ServiceImpl<VeBaseCalendarMapper, VeBaseCalendar> implements VeBaseCalendarService {

    @Resource
    private VeBaseCalendarMapper veBaseCalendarMapper;

    @Override
    public List<VeBaseCalendar> getCalendar(Long semester) {
        return veBaseCalendarMapper.getCalendar(semester);
    }
}
