package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import javax.persistence.Table;
import java.io.Serializable;
/**
 * @Description: 宿舍资产展示
 * @Author:
 * @Date:   2021-03-02
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)

public class VeSuSheAssetVo implements Serializable {
    /**资产Id*/
    @TableId(value = "id")
    @ApiModelProperty(value = "id")
    private Integer id;
    /**资产名称*/
    @Excel(name = "资产名称",width = 15)
    @ApiModelProperty("资产名称")
    private String zcName;
    /**宿舍名称 B1 宿舍楼 101*/
    /**宿舍楼编号*/
    @Excel(name = "宿舍楼编号",width = 15)
    @ApiModelProperty("宿舍楼编号")
    private Long JZWH;

    /**房间编号*/
    @Excel(name = "房间编号",width = 15)
    @ApiModelProperty("房间编号")
    private String FJBM;

    /**资产个数*/
    @Excel(name = "资产个数",width = 15)
    @ApiModelProperty("资产个数")
    private Integer nums;
    /**排序*/
    @Excel(name = "排序",width = 15)
    @ApiModelProperty("排序")
    private Integer listSort;
    /**资产类别*/
    @Excel(name = "资产类别",width = 15)
    @ApiModelProperty("资产类别")
    private String typeName;
    /**资产状态*/
    @Excel(name = "资产状态",width = 15)
    @ApiModelProperty("资产状态 1正常 2维修 3报废")
    private Integer status;
    /**宿舍楼名称*/
    @Excel(name = "宿舍楼名称",width = 15)
    @ApiModelProperty("宿舍楼名称")
    private String JZWMC;
    /**校区名称*/
    @Excel(name = "校区名称",width = 15)
    @ApiModelProperty("校区名称")
    private String  campusName;
  /**文件名称*/
  @Excel(name = "文件名称",width = 15)
  @ApiModelProperty("文件名称")
  private String  fileName;
  /**文件大小*/
  @Excel(name = "文件大小",width = 15)
  @ApiModelProperty("文件大小")
  private String  fileSize;
  /**文件路径*/
  @Excel(name = "文件路径",width = 15)
  @ApiModelProperty("文件路径")
  private String  sourceFile;
  /**说明*/
  @Excel(name = "说明",width = 15)
  @ApiModelProperty("说明")
  private String  content;
  /**类型*/
  @Excel(name = "类型",width = 15)
  @ApiModelProperty("类型")
  private String  zcType;




}
