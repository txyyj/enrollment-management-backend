package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeBaseCampus;

import java.util.List;

public interface VeBaseCampusService extends IService<VeBaseCampus> {

    List<VeBaseCampus> list();

}
