package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormWeiJi;
import org.edu.modules.dorm.vo.VeDormWeiJiVo;

import java.util.List;

public interface VeDormWeiJiService extends IService<VeDormWeiJi> {

    Integer updateDiscipline(VeDormWeiJi veDormWeiJi);

    List<VeDormWeiJiVo> weiJiTable(String name, String xh);

}
