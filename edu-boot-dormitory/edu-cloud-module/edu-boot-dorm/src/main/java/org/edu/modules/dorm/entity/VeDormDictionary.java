package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 字典
 * @Author:
 * @Date:   2021-03-02
 * @Version: V1.0
 */

@Data
@TableName("ve_dorm_dictionary")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormDictionary implements Serializable {

      /**id*/
      @TableId(type = IdType.AUTO)
      @ApiModelProperty(value = "id")
      private Long id;

      /**字典名称*/
      @Excel(name = "字典名称", width = 15)
      @ApiModelProperty(value = "字典名称")
      @TableField(value = "title")
      private String title;

      /**字典编码*/
      @Excel(name = "字典编码", width = 15)
      @ApiModelProperty(value = "字典编码")
      @TableField(value = "code")
      private String code;

      /**序号*/
      @Excel(name = "序号", width = 15)
      @ApiModelProperty(value = "序号")
      @TableField(value = "listSort")
      private String listSort;

      /**父Id*/
      @Excel(name = "父Id", width = 15)
      @ApiModelProperty(value = "父Id")
      @TableField(value = "pid")
      private String pid;

}
