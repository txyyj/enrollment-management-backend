package org.edu.modules.dorm.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormLouceng;
import org.edu.modules.dorm.entity.VeDormSSLou;
import org.edu.modules.dorm.entity.VeDormSShe;
import org.edu.modules.dorm.entity.bo.BasicResponseBO;
import org.edu.modules.dorm.entity.bo.VeCampusBO;
import org.edu.modules.dorm.mapper.VeDormSuSheAdminMapper;
import org.edu.modules.dorm.service.*;
import org.edu.modules.dorm.vo.VeDormSSVo;
import org.edu.modules.dorm.vo.VeDormSSheVO;
import org.edu.modules.dorm.vo.VeFindStudentVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class VeDormSuSheAdminServiceImpl extends ServiceImpl<VeDormSuSheAdminMapper, VeDormSShe> implements VeDormSuSheAdminService {
    @Resource
    private VeDormSuSheAdminMapper veDormSuSheAdminMapper;

    //添加宿舍
    @Override
    public Integer insertSuSHe(VeDormSShe veDoremSuShe){
        return veDormSuSheAdminMapper.insert(veDoremSuShe);
    }

    //删除宿舍
    @Override
    public Integer deleteSuShe(Integer id) {
        return veDormSuSheAdminMapper.deleteById(id);
    }

   //边界宿舍
    @Override
    public Integer editSuShe(VeDormSShe veDormSShe) {
        return veDormSuSheAdminMapper.updateById(veDormSShe);
    }

    @Override
    public List<VeDormSSVo> getSuShe(Integer xqId, Integer sslId, Integer lcId) {
        return veDormSuSheAdminMapper.selectParms(xqId, sslId, lcId);
    }

    @Autowired
    VeFindTFStudentService mVeFindTFStudentService ;
    @Autowired
    VeDormSuSheLouService mVeDormSuSheLouService ;
    @Autowired
    VeDormLoucengService mVeDormLoucengService ;
    @Resource
    private VeBaseDormManageService veBaseDormManageService;

    @Override
    @Transactional(rollbackFor = Exception.class)
    public String excelImport(List<VeDormSSheVO> list){

        //获取所有校区信息
        BasicResponseBO<List> bo = veBaseDormManageService.getCampusMessage(); //李少君
        List<VeCampusBO> result = JSONObject.parseArray(JSON.toJSONString(bo.getResult()),VeCampusBO.class);
        List<VeFindStudentVo> bo1 = new ArrayList<>();
        for(int i =0 ;i<result.size();i++){
            VeFindStudentVo c = new VeFindStudentVo();
            c.setId(result.get(i).getId());
            c.setName(result.get(i).getXqmc());
            bo1.add(c);
        }
        // 获取所有宿舍楼信息
        List<VeDormSSLou> sushelou = mVeDormSuSheLouService.list() ;


        // 获取所有楼层
        List<VeDormLouceng> lc= mVeDormLoucengService.list() ;
        for (int i = 0; i < list.size(); i++) {
            VeDormSShe v = new VeDormSShe();
            for (int j = 0; j < bo1.size(); j++) {
                if(list.get(i).getXqh().equals(bo1.get(j).getName())){
                    v.setXqh(bo1.get(j).getId());
                }
            }
            if("".equals(v.getXqh())){
                return "查无该校区，请重新输入!";
            }
            //根据校区和宿舍楼获取宿舍楼id
            Long sslId = veDormSuSheAdminMapper.getSSLId((int) v.getXqh(), list.get(i).getJzwh());
            if(sslId==null){
                return "查无该宿舍楼，请重新输入!";
            }else {
                v.setJzwh(sslId);
            }
            //根据校区id、宿舍楼id、楼层获取楼层id
            Long lcId = veDormSuSheAdminMapper.getloucengId((int) v.getXqh(), (int) v.getJzwh(), list.get(i).getJzwh());
            if(lcId==null){
                return "查无该楼层，请重新输入!";
            }else {
                v.setLch(lcId.intValue());
            }
            //根据楼层号和房间编号查询是否已存在该宿舍
            Long ssId = veDormSuSheAdminMapper.getssId(v.getLch(), list.get(i).getFjbm());
            if(lcId==null){
                v.setFjbm(list.get(i).getFjbm());
            }else {
                return "已有该宿舍,请重新输入!";
            }
            if("男".equals(list.get(i).getRzxb())){
                v.setRzxb(1);
            }else if("女".equals(list.get(i).getRzxb())){
                v.setRzxb(2);
            }
            if(list.get(i).getKzrs()<13){
                v.setKzrs(list.get(i).getKzrs());
                if(list.get(i).getYzrs()<=list.get(i).getKzrs()){
                    v.setYzrs(list.get(i).getYzrs());
                }else {
                    return "已住人数不能多于可住人数";
                }
            }else {
                return "可住人数不能超过12人";
            }
            v.setWldk(list.get(i).getWldk()).setDhhm(list.get(i).getDhhm()).setSbds(list.get(i).getSbds()).setDbds(list.get(i).getDbds())
            .setSsbz(list.get(i).getSsbz());
            veDormSuSheAdminMapper.insert(v) ;



        }

        return "导入成功";
    }

//    @Override
//    @Transactional(rollbackFor = Exception.class)
//    public void excelImport(List<VeDormSSheVO> list) throws Exception {
//        // 获取所有校区信息
//        List<VeFindStudentVo> xiaoqu =  mVeFindTFStudentService.findXiaoQu() ;
//        Map<String,Long> map = new HashMap<>() ;
//        xiaoqu.forEach(l-> map.put(l.getName(),l.getId()));
//        // 获取所有宿舍楼信息
//        List<VeDormSSLou> sushelou = mVeDormSuSheLouService.list() ;
//        Map<Long,List<VeDormSSLou>> groupXiaoqu = sushelou.stream().collect(Collectors.groupingBy(VeDormSSLou::getXqh)) ;
//        Map<Long,Map<String,Integer>> sushelouMap = new HashMap<>() ;
//        groupXiaoqu.forEach((k,v)->{
//            Map<String,Integer> c = new HashMap<>() ;
//            v.forEach(l-> c.put(l.getJzwmc(),l.getId()));
//            sushelouMap.put(k,c) ;
//        });
//        // 获取所有楼层
//        List<VeDormLouceng> lc= mVeDormLoucengService.list() ;
//        Map<Integer,List<VeDormLouceng>> groupSushelou = lc.stream().collect(Collectors.groupingBy(VeDormLouceng::getJzwh)) ;
//        Map<Integer,Map<String,Integer>> lcMap = new HashMap<>() ;
//        groupSushelou.forEach((k,v)->{
//            Map<String,Integer> c = new HashMap<>() ;
//            v.forEach(l-> c.put(l.getLcname(),l.getId()));
//            lcMap.put(k,c) ;
//        });
//        // 入住性别字典
//        Map<String,Integer> sexMap = new HashMap<>() ;
//        sexMap.put("男",1) ;
//        sexMap.put("女",2) ;
//
//        int t = 4 ;
//        try{
//            for ( VeDormSSheVO ve : list) {
//                VeDormSShe sushe = new VeDormSShe() ;
//                BeanUtils.copyProperties(ve,sushe);
//                // 获取校区信息
//                Long xiaoquId = map.get(ve.getXqh()) ;
//                // 宿舍楼
//                Integer lou = sushelouMap.get(xiaoquId).get(ve.getJzwh()) ;
//                // 楼层
//                Integer lcId = lcMap.get(lou).get(ve.getLch()) ;
//
//                sushe.setXqh(xiaoquId).setJzwh(Long.parseLong(lou+""))
//                        .setLch(lcId);
//
//                // 判断宿舍是否已存在
//                QueryWrapper<VeDormSShe> wrapper=new QueryWrapper<>();
//                wrapper.eq("fjbm",sushe.getFjbm());
//                wrapper.eq("lch",sushe.getLch());
//                VeDormSShe ss=veDormSuSheAdminMapper.selectOne(wrapper);
//                if (ss!=null){
//                    if(Objects.nonNull(ve.getRzxb())){
//                        sushe.setRzxb(sexMap.getOrDefault(ve.getRzxb(),0)) ;
//                    }
//                    veDormSuSheAdminMapper.insert(sushe) ;
//                }
//                t++ ;
//            }
//        }catch (Exception e){
//            throw new Exception("第"+t+"行数据有误！") ;
//        }
//    }

}
