package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormAssetType;
import org.edu.modules.dorm.mapper.VeDormAssetTypeMapper;
import org.edu.modules.dorm.service.VeDormAssetTypeService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeDormAssetTypeServiceImpl extends ServiceImpl<VeDormAssetTypeMapper, VeDormAssetType> implements VeDormAssetTypeService {
    @Autowired
    private VeDormAssetTypeMapper dormAssetMapper;

}
