package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 卫生
 * @Author:
 * @Date:   2021-03-08
 * @Version: V1.0
 */

@Data
@TableName("ve_dorm_wsceil")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormWsceil implements Serializable {

    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /**semId*/
    @Excel(name = "学期",width = 15)
    @ApiModelProperty(value = "semId")
    @TableField(value = "semId")
    private Long semId;

    /**校区Id*/
    @Excel(name = "校区",width = 15)
    @ApiModelProperty(value = "校区Id")
    @TableField(value = "campusId")
    private Long campusId;

    /**建筑物Id*/
    @Excel(name = "宿舍楼",width = 15)
    @ApiModelProperty(value = "建筑物Id")
    @TableField(value = "jzwId")
    private Long jzwId;

    /**楼层Id*/
    @Excel(name = "楼层",width = 15)
    @ApiModelProperty(value = "楼层Id")
    @TableField(value = "lcId")
    private Long lcId;

    /**宿舍Id*/
    @Excel(name = "宿舍",width = 15)
    @ApiModelProperty(value = "宿舍Id")
    @TableField(value = "ssId")
    private Long ssId;

    /**添加时间*/
//    @Excel(name = "添加时间",width = 15)
    @ApiModelProperty(value = "添加时间")
    @TableField(value = "addTime")
    private Long addTime;

    /**得分*/
    @Excel(name = "得分",width = 15)
    @ApiModelProperty(value = "得分")
    @TableField(value = "score")
    private Long score;

    /**月份*/
    @Excel(name = "月份",width = 15)
    @ApiModelProperty(value = "月份")
    @TableField(value = "mouth")
    private Long mouth;

    /**周数*/
    @Excel(name = "周数",width = 15)
    @ApiModelProperty(value = "周数")
    @TableField(value = "weekNum")
    private Long weekNum;

    /**周*/
    @Excel(name = "周",width = 15)
    @ApiModelProperty(value = "周")
    @TableField(value = "week")
    private Long week;

    /**终端Id*/
//    @Excel(name = "终端Id",width = 15)
    @ApiModelProperty(value = "终端Id")
    @TableField(value = "terminalId")
    private Long terminalId;

}
