package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 学生宿舍关系
 * @Author:
 * @Date:   2021-03-03
 * @Version: V1.0
 */

@Data
@TableName("ve_dorm_student")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormStudent implements Serializable {

    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    @TableField(value = "id")
    private Long id;


    /**用户id*/
    @Excel(name = "用户id",width = 15)
    @ApiModelProperty(value = "用户id")
    @TableField(value = "userId")
    private Long userId;

    /**学号*/
    @Excel(name = "XH",width = 15)
    @ApiModelProperty(value = "学号")
    @TableField(value = "XH")
    private String XH;

    /**姓名*/
    @Excel(name = "姓名",width = 15)
    @ApiModelProperty(value = "姓名")
    @TableField(value = "XM")
    private String XM;

    /**宿舍楼编号*/
    @Excel(name = "宿舍楼编号",width = 15)
    @ApiModelProperty(value = "宿舍楼编号")
    @TableField(value = "SSLBM")
    private Long SSLBM;

    /**校区号*/
    @Excel(name = "校区号",width = 15)
    @ApiModelProperty(value = "校区号")
    @TableField(value = "XQH")
    private Long XQH;

    /**楼层号*/
    @Excel(name = "楼层号",width = 15)
    @ApiModelProperty(value = "楼层号")
    @TableField(value = "LCH")
    private Long LCH;

    /**房间编号*/
    @Excel(name = "房间编号",width = 15)
    @ApiModelProperty(value = "房间编号")
    @TableField(value = "FJBM")
    private Long FJBM;

    /**床位号*/
    @Excel(name = "床位号",width = 15)
    @ApiModelProperty(value = "床位号")
    @TableField(value = "CWh")
    private String CWH;

    /**入住时间*/
    @Excel(name = "入住时间",width = 15)
    @ApiModelProperty(value = "入住时间")
    @TableField(value = "RZSJ")
    private Integer RZSJ;

    /**添加时间*/
    @Excel(name = "添加时间",width = 15)
    @ApiModelProperty(value = "添加时间")
    @TableField(value = "createTime")
    private Integer createTime;

    /**添加人员名称*/
    @Excel(name = "添加人员名称",width = 15)
    @ApiModelProperty(value = "添加人员名称")
    @TableField(value = "createUserName")
    private String  createUserName;

    /**添加人员id*/
    @Excel(name = "添加人员id",width = 15)
    @ApiModelProperty(value = "添加人员id")
    @TableField(value = "createUserId")
    private Long createUserId;

    /**系统id*/
    @Excel(name = "系统id",width = 15)
    @ApiModelProperty(value = "系统id")
    @TableField(value = "terminalId")
    private Long terminalId;

}
