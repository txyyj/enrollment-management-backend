package org.edu.modules.dorm.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.dorm.entity.VeDormNews;
import org.edu.modules.dorm.entity.VeDormNewsType;
import org.edu.modules.dorm.entity.VeFileFiles;
import org.edu.modules.dorm.mapper.VeFileFilesMapper;
import org.edu.modules.dorm.service.VeDormNewsService;
import org.edu.modules.dorm.service.VeDormNewsTypeService;
import org.edu.modules.dorm.vo.VeDormNewsVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;

@Api(tags="宿舍制度模块")
@RestController
@RequestMapping("dorm/news")
@Slf4j
public class VeDormNewsController extends BaseController<VeDormNewsType,VeDormNewsTypeService> {
    @Autowired
    private VeDormNewsTypeService veDormNewsTypeService;
    @Autowired
    private VeDormNewsService veDormNewsService;
    @Autowired
    private VeFileFilesMapper veFileFilesMapper;
    /**
     * 新增制度类别
     * @param
     * @return
     */
    @AutoLog(value = "新增制度类别")
    @ApiOperation(value = "新增制度类别",notes = "输入制度名，新增制度")
    @PostMapping("/add")
    public Result<?> addNewsType(@ApiParam(value = "制度类型名称") @RequestParam("typeName")String typeName
                               ){
        VeDormNewsType veDormNewsType = new VeDormNewsType();
        veDormNewsType.setTypeName(typeName);
        veDormNewsTypeService.save(veDormNewsType);
        return Result.OK("新增成功");

    }


    /**
     * 模糊查询获取类型
     * @param name
     * @return
     */
    @ApiOperation(value = "模糊查询",notes = "")
    @PostMapping(value = "/queryByName")
    public Result<?> queryByName(@ApiParam(value = "制度类型名称") @RequestParam("name")String name){
        QueryWrapper<VeDormNewsType> wrapper = new QueryWrapper<>();
        wrapper.like("typeName",name);
        wrapper.orderByDesc("id");
        List<VeDormNewsType> list = veDormNewsTypeService.list(wrapper);
        return Result.OK(list);
    }

    /**
     * 根据Id获取编辑信息
     * @param id
     * @return
     */

    @ApiOperation(value = "根据Id获取编辑信息",notes = "")
    @PostMapping(value = "/updateMessage")
    public Result<?> updateMessage(@ApiParam(value = "制度类型ID") @RequestParam("id")String id){
        VeDormNewsType veDormNewsType = veDormNewsTypeService.getById(id);
        return Result.OK(veDormNewsType);
    }

    /**
     * 对类型进行编辑
     * @param typeName,id
     * @return
     */
    @AutoLog("对类型进行编辑")
    @ApiOperation(value = "对类型进行编辑",notes = "")
    @PostMapping(value = "/update")
    public Result<?> update(@ApiParam(value = "制度类型名称") @RequestParam("typeName")String typeName,
                            @ApiParam(value = "制度类型ID") @RequestParam("id")Long id){
        VeDormNewsType veDormNewsType = new VeDormNewsType();
        veDormNewsType.setId(id);
        veDormNewsType.setTypeName(typeName);
        veDormNewsTypeService.updateById(veDormNewsType);
        return Result.OK("编辑成功");
    }

    /**
     * 根据Id对类型进行删除
     * @param id
     */
    @AutoLog("根据Id对类型进行删除")
    @ApiOperation(value = "根据Id对类型进行删除",notes = "")
    @PostMapping(value = "/delete")
    public Result<?> delete(@ApiParam(value = "制度类型ID") @RequestParam("id")Long id){
        veDormNewsTypeService.removeById(id);
        return Result.OK("删除成功");
    }

    /**
     * 根据Ids对类型进行删除
     * @param ids
     */
    @AutoLog("根据Ids对类型进行批量删除")
    @ApiOperation(value = "根据Id对类型进行删除",notes = "")
    @PostMapping(value = "/deleteBatch")
    public Result<?> deleteBatch(@ApiParam(value = "制度类型IDS") @RequestParam("ids")String ids){
        veDormNewsTypeService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("删除成功");
    }

    /**
     * 添加宿舍制度
     * @param title,content,fileId,listSort,typeId
     * @return
     */
    @AutoLog("添加宿舍制度")
    @ApiOperation(value = "添加宿舍制度",notes = "")
    @PostMapping(value = "/addNews")
    public Result<?> deleteBatch(@ApiParam(value = "标题") @RequestParam("title")String title,
                                 @ApiParam(value = "内容") @RequestParam(value = "content",required = false)String content,
                                 @ApiParam(value = "文件Url")@RequestParam(value = "fileUrl",required = false)String fileUrl,
                                 @ApiParam(value = "文件大小")@RequestParam(value = "fileSize",required = false)Long fileSize,
                                 @ApiParam(value = "文件名称")@RequestParam(value = "fileName",required = false)String fileName,
                                 @ApiParam(value = "排序") @RequestParam(value = "listSort",required = false)Long listSort,
                                 @ApiParam(value = "类型ID") @RequestParam(value = "typeId",required = false)Long typeId){
        Long createTime = System.currentTimeMillis()/1000;

      VeFileFiles files = new VeFileFiles();
      files.setSourcefile(fileUrl);
      files.setSize(fileSize);
      files.setCreatetime(createTime);
      files.setName(fileName);
      veFileFilesMapper.insert(files);
        VeDormNews veDormNews = new VeDormNews();
        veDormNews.setCreateTime(createTime);
        veDormNews.setTitle(title);
        veDormNews.setContent(content);
        veDormNews.setFileId(files.getId());
        veDormNews.setListSort(listSort);
        veDormNews.setTypeId(typeId);
        //插入用户
        veDormNews.setCraereUserId(1L);
        veDormNews.setCreateUserName("管理员");
        veDormNewsService.save(veDormNews);
        return Result.OK("添加成功");


    }

    /**
     * 模糊查询制度管理
     * @param title
     * @return
     */
    @ApiOperation(value = "模糊查询制度管理",notes = "")
    @PostMapping(value = "/queryNewsByName")
    public Result<?> queryNewsByName(@ApiParam(value = "标题") @RequestParam("title")String title,
                                     @ApiParam(value = "类型ID") @RequestParam("typeId")String typeId){
        Long typeid = null;
        if(typeId !=""&&!"".equals(typeId)) {
            typeid = Long.parseLong(typeId);
        }
        List<VeDormNewsVo> list = veDormNewsService.queryNewsByName(title, typeid);
        return Result.OK(list);

    }


    /**
     *根据Id获取寝室制度编辑消息
     * @param id
     * @return
     */
    @ApiOperation(value = "根据Id获取寝室制度编辑消息",notes = "")
    @PostMapping(value = "/updateNewsMessage")
    public Result<?> updateNewsMessage(@ApiParam(value = "类型ID") @RequestParam("id")Long id){
      VeDormNewsVo veDormNews = veDormNewsService.updateMessage(id);
        return Result.OK(veDormNews);

    }

    /**
     * 编辑寝室制度
     * @param title,content,fileId,listSort,typeId
     * @return
     */
    @AutoLog("编辑寝室制度")
    @ApiOperation(value = "编辑寝室制度",notes = "")
    @PostMapping(value = "/updateNews")
    public Result<?> updateNews(@ApiParam(value = "标题") @RequestParam("title")String title,
                                @ApiParam(value = "内容") @RequestParam(value = "content",required = false)String content,
                                @ApiParam(value = "文件Url")@RequestParam(value = "fileUrl",required = false)String fileUrl,
                                @ApiParam(value = "文件大小")@RequestParam(value = "fileSize",required = false)Long fileSize,
                                @ApiParam(value = "文件名称")@RequestParam(value = "fileName",required = false)String fileName,
                                @ApiParam(value = "排序") @RequestParam(value = "listSort",required = false)Long listSort,
                                @ApiParam(value = "类型ID") @RequestParam(value = "typeId",required = false)Long typeId,
                                @ApiParam(value = "制度ID") @RequestParam("id")Long id){

        VeDormNews veDormNews = new VeDormNews();
        veDormNews.setId(id);
        veDormNews.setTitle(title);
        veDormNews.setContent(content);
        Long fileId = null ;
        if(Objects.nonNull(fileUrl) && !"".equals(fileUrl)){
            VeFileFiles files = new VeFileFiles();
            files.setSourcefile(fileUrl);
            files.setSize(fileSize);
            files.setName(fileName);
            veFileFilesMapper.insert(files);
            fileId = files.getId() ;
        }
        veDormNews.setListSort(listSort);
        veDormNews.setTypeId(typeId);
        veDormNews.setFileId(fileId);
        veDormNewsService.updateById(veDormNews);
        return Result.OK("编辑成功");


    }
    /**
     *根据Id删除寝室制度消息
     * @param id
     * @return
     */
    @ApiOperation(value = "根据Id删除寝室制度消息",notes = "")
    @PostMapping(value = "/deleteNews")
    public Result<?> deletdNews(@ApiParam(value = "制度ID") @RequestParam("id")Long id){
        veDormNewsService.removeById(id);
        return Result.OK("删除成功");

    }

    /**
     *根据Ids批量删除寝室制度消息
     * @param ids
     * @return
     */
    @ApiOperation(value = "根据Ids批量删除寝室制度消息",notes = "")
    @PostMapping(value = "/deleteBatchNews")
    public Result<?> deletdNews(@ApiParam(value = "制度IDS") @RequestParam("ids")String ids){
        veDormNewsService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功");
    }
}
