package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 学生宿舍来访情况登记表
 * @Author:
 * @Date:   2021-03-03
 * @Version: V1.0
 */

@Data
@TableName("ve_dorm_laifang")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormLaiFang implements Serializable {
    
    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty("id")
    private Long id;

    /**姓名*/
    @Excel(name = "姓名",width = 15)
    @ApiModelProperty(value = "姓名")
    @TableField(value = "XM")
    private String XM;

    /**身份证号*/
    @Excel(name = "身份证号",width = 15)
    @ApiModelProperty(value = "身份证号")
    @TableField(value = "SFZH")
    private String SFZH;

    /**被探访人员*/
    @Excel(name = "被探访人员",width = 15)
    @ApiModelProperty(value = "被探访人员")
    @TableField(value = "userId")
    private Long userId;

    /**房间编号*/
    @Excel(name = "被探访人员",width = 15)
    @ApiModelProperty(value = "被探访人员")
    @TableField(value = "FJBH")
    private Long FJBH;

    /**探访事由*/
    @Excel(name = "探访事由",width = 200)
    @ApiModelProperty(value = "探访事由")
    @TableField(value = "TFSY")
    private String TFSY;

    /**探访类型（数据字典）*/
    @Excel(name = "探访类型",width = 15)
    @ApiModelProperty(value = "探访类型")
    @TableField(value = "TFLX")
    private Long TFLX;

    /**登记时间*/
    @Excel(name = "登记时间",width = 15)
    @ApiModelProperty(value = "登记时间")
    @TableField(value = "DJSJ")
    private Long DJSJ;

    /**来访申请时间*/
    @Excel(name = "来访申请时间",width = 15)
    @ApiModelProperty(value = "来访申请时间")
    @TableField(value = "applyTime")
    private Long applyTime;

    /**登记人*/
    @Excel(name = "登记人",width = 15)
    @ApiModelProperty(value = "登记人")
    @TableField(value = "DJR")
    private Long DJR;

    /**申请离开时间*/
    @Excel(name = "申请离开时间",width = 15)
    @ApiModelProperty(value = "申请离开时间")
    @TableField(value = "leaveTime")
    private Long leaveTime;

    /**离开时间*/
    @Excel(name = "离开时间",width = 15)
    @ApiModelProperty(value = "离开时间")
    @TableField(value = "LKSJ")
    private Long LKSJ;

    /**离开登记人*/
    @Excel(name = "离开登记人",width = 15)
    @ApiModelProperty(value = "离开登记人")
    @TableField(value = "LKDJR")
    private Long LKDJR;

    /**状态（0 已登记 1已离开 2已申请）*/
    @Excel(name = "状态",width = 15)
    @ApiModelProperty(value = "状态")
    @TableField(value = "status")
    private Integer status;

}
