package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @Description: 宿舍统计
 * @Author:
 * @Date:   2021-03-11
 * @Version: V1.0
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormSuSheTJVO对象", description = "宿舍统计")
public class VeDormSuSheTJVo implements Serializable {

    /** （1）自增id。（宿舍楼表id） */
    @ApiModelProperty(value = "自增id（宿舍楼表id）")
    private Long id;

    /** (2)建筑物名称。（宿舍楼） */
    @ApiModelProperty(value = "建筑物名称（宿舍楼）")
    @TableField(value = "JZWMC")
    private String JZWMC;

    /** （3）宿舍总数。 */
    @ApiModelProperty(value = "宿舍总数")
    @TableField(value = "susheCount")
    private Long susheCount;

    /** （4）可住人数。 */
    @ApiModelProperty(value = "可住人数")
    @TableField(value = "KZRS")
    private Long KZRS;

    /** （5）已住人数。 */
    @ApiModelProperty(value = "已住人数")
    @TableField(value = "YZRS")
    private Long YZRS;

}
