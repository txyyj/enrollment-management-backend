package org.edu.modules.dorm.vo;

import lombok.Data;

/**
 * @author leidq
 * @create 2021-05-14 16:02
 */
@Data
public class VeReplaceBedVo {

    /**
     * 宿舍Id
     */
    private String SSBH;

    /**
     * 学生id
     */
    private String stuId;

}
