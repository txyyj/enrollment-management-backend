package org.edu.modules.dorm.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 校历表
 * @Author:
 * @Date:   2021-03-10
 * @Version: V1.0
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "校历表", description = "校历表")
public class VeDormCalendarVo implements Serializable {
    
    /**id*/
    @ApiModelProperty(value = "id")
    private Long id;
    
    /**学期ID*/
    @Excel(name = "学期Id", width = 15)
    @ApiModelProperty(value = "学期Id")
    private Long semId;
    
    /**学期名称*/
    @Excel(name = "学期名称", width = 15)
    @ApiModelProperty(value = "学期名称")
    private String XQMC;
    
    /**月份*/
    @Excel(name = "月份", width = 15)
    @ApiModelProperty(value = "月份")
    @TableField(value = "month")
    private Integer month;

    /**日期*/
    @Excel(name = "日期", width = 15)
    @ApiModelProperty(value = "日期")
    @TableField(value = "dates")
    private String dates;

    /**第几周*/
    @Excel(name = "第几周", width = 15)
    @ApiModelProperty(value = "第几周")
    @TableField(value = "week")
    private Integer week;

    /**年份*/
    @Excel(name = "年份", width = 15)
    @ApiModelProperty(value = "年份")
    @TableField(value = "year")
    private Long year;

    /**周几*/
    @Excel(name = "周几", width = 15)
    @ApiModelProperty(value = "周几")
    @TableField(value = "dayOfWeek")
    private Long dayOfWeek;

}
