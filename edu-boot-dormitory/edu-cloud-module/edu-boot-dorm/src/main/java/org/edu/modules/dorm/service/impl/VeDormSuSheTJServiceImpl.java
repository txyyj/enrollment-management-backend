package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormSuShe;
import org.edu.modules.dorm.mapper.VeDormSuSheTJMapper;
import org.edu.modules.dorm.service.VeDormSuSheTJService;
import org.edu.modules.dorm.vo.VeDormCheckVo;
import org.edu.modules.dorm.vo.VeDormSuSheTJVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class VeDormSuSheTJServiceImpl extends ServiceImpl<VeDormSuSheTJMapper, VeDormSuShe> implements VeDormSuSheTJService {
    @Resource
    private VeDormSuSheTJMapper veDormSuSheTJMapper;

    /**
     * 1、宿舍容量统计。
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<VeDormSuSheTJVo> selectDormList() {

        return veDormSuSheTJMapper.selectDormList();
    }

    /**
     * 2、宿舍学生统计。
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<VeDormSuSheTJVo> selectStudentList() {
        return veDormSuSheTJMapper.selectStudentList();
    }

    /**
     * 3、考勤统计。（通过考勤类型id、宿舍楼id、院系id、专业id、班级id精确查询，房间编号模糊查询）
     * @param checkRuleId 考勤类型id
     * @param sslId 宿舍楼id
     * @param facultyId 院系id
     * @param specialtyId 专业id
     * @param banjiId 班级id
     * @param FJBM 房间编号
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<VeDormCheckVo> selectCheckList(Long checkRuleId, Long sslId, Long facultyId, Long specialtyId,
                                               Long banjiId, String FJBM) {
        return veDormSuSheTJMapper.selectCheckList(checkRuleId, sslId, facultyId, specialtyId, banjiId, FJBM);
    }

    /**
     * 4、违纪统计。（通过违纪类型id、宿舍楼id、院系id、专业id、班级id精确查询，房间编号模糊查询）
     * @param disciplineTypeId 违纪类型id
     * @param sslId 宿舍楼id
     * @param facultyId 院系id
     * @param specialtyId 专业id
     * @param banjiId 班级id
     * @param FJBM 房间编号
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public List<VeDormCheckVo> selectDisciplineList(Long disciplineTypeId, Long sslId, Long facultyId, Long specialtyId,
                                                    Long banjiId, String FJBM) {
        return veDormSuSheTJMapper.selectDisciplineList(disciplineTypeId, sslId, facultyId, specialtyId, banjiId, FJBM);
    }

}
