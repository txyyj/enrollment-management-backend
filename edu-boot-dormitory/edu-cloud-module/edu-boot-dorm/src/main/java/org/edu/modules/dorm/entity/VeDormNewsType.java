package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Description: 宿舍制度类别
 * @Author:
 * @Date:   2021-03-08
 * @Version: V1.0
 */

@Data
@TableName("ve_dorm_newstype")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormNewsType implements Serializable {

    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /**类别名称*/
    @Excel(name = "类别名称",width = 15)
    @ApiModelProperty(value = "类别名称")
    @TableField(value = "typeName")
    private String typeName;

    /**终端id*/
    @Excel(name = "终端id",width = 15)
    @ApiModelProperty(value = "终端id")
    @TableField(value = "terminalId")
    private Long terminalId;

}
