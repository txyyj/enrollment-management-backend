package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormNews;
import org.edu.modules.dorm.vo.VeDormNewsVo;

import java.util.List;

public interface VeDormNewsMapper extends BaseMapper<VeDormNews> {

    List<VeDormNewsVo> findNews(@Param("title")String title,@Param("typeId")Long typeId);

    VeDormNewsVo updateMessage(@Param("id")Long id);
}
