package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormCheck;
import org.edu.modules.dorm.vo.VeDormCheckVo;

import java.util.List;

public interface VeDormCheckMapper extends BaseMapper<VeDormCheck> {

    /**
     * 1、考勤列表。（通过姓名、学号模糊查询，宿舍楼编号精准查询）
     * @param XM 姓名
     * @param XH 学号
     * @param SSLBM 宿舍楼编号
     * */
    List<VeDormCheckVo> selectList(@Param("XM") String XM, @Param("XH") String XH, @Param("SSLBM") Long SSLBM);

    /**
     * 2、通过id查询。（违纪管理-通用）
     * @param id 考勤id
     */
    VeDormCheckVo selectById(@Param("id") Long id);
    
}
