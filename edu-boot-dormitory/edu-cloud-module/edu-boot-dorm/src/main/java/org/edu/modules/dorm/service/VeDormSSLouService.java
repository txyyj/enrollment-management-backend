package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormSSLou;
import org.edu.modules.dorm.entity.VeFileFiles;
import org.edu.modules.dorm.vo.VeDOrmSSLouExcelVo;
import org.edu.modules.dorm.vo.VeDormSSLouVo;
import org.edu.modules.dorm.vo.VeDormWeiJiImageVo;
import org.edu.modules.dorm.vo.VeReplaceBedVo;

import java.util.List;

public interface VeDormSSLouService extends IService<VeDormSSLou> {

    Integer addSuSheLou(VeDormSSLou veDormSuSheLou);

    List<VeDormSSLou> getList(String name);

    List<VeDormSSLouVo> selectByParam(String ssl);

    Integer deleteSSL(Integer id);

    List<VeDormSSLou> getSuSheLou();

    List<VeDormSSLouVo> exportList(String ssl);

    List<VeDormSSLou> getPageList(QueryWrapper<VeDormSSLouVo> queryWrapper);

    VeDormWeiJiImageVo getEditInfo( Long id);

    Integer addSSL(VeDormSSLou veDormSSLou, VeFileFiles veFileFiles);

    Integer editSSL(VeDormSSLou veDormSSLou, VeFileFiles veFileFiles);

    String replaceBed(List<VeReplaceBedVo> veReplaceBedVos);

    String replaceSushe(List<VeReplaceBedVo> veReplaceBedVos);

    void excelImport(List<VeDOrmSSLouExcelVo> list) throws Exception;
}
