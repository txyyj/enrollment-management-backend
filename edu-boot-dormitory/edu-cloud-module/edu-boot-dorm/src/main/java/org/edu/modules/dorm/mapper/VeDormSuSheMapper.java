package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.dorm.entity.VeDormSuShe;
import org.edu.modules.dorm.vo.VeDormSuSheVo;

import java.util.List;

public interface VeDormSuSheMapper extends BaseMapper<VeDormSuShe> {

    List<VeDormSuSheVo> suSheMessage(@Param("SSLId")Long SSLId);

}
