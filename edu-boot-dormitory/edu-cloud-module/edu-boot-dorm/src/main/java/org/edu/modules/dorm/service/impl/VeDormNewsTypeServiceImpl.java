package org.edu.modules.dorm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.dorm.entity.VeDormNewsType;
import org.edu.modules.dorm.mapper.VeDormNewsTypeMapper;
import org.edu.modules.dorm.service.VeDormNewsTypeService;
import org.springframework.stereotype.Service;

@Service
public class VeDormNewsTypeServiceImpl extends ServiceImpl<VeDormNewsTypeMapper, VeDormNewsType> implements VeDormNewsTypeService {

}
