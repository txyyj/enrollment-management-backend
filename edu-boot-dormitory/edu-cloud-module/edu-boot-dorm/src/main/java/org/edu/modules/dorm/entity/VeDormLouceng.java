package org.edu.modules.dorm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

@Data
@TableName("ve_dorm_louceng")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ve_dorm_louceng对象", description = "楼层")
public class VeDormLouceng {

    //楼层表
    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Integer id;

    //校区号
    @Excel(name = "校区号", width = 15)
    @ApiModelProperty(value = "校区号")
    private Integer xqh;

    @Excel(name = "宿舍楼id", width = 15)
    @ApiModelProperty(value = "宿舍楼id")
    private Integer jzwh;

    @Excel(name = "楼层名称", width = 15)
    @ApiModelProperty(value = "楼层名称")
    private String lcname;

    @Excel(name = "楼层代码", width = 15)
    @ApiModelProperty(value = "楼层代码")
    private String lccode;

    @Excel(name = "是否有摄像头", width = 15)
    @ApiModelProperty(value="是否有摄像头")
    private Integer sfsxt;

    @Excel(name = "终端id", width = 15)
    @ApiModelProperty(value = "终端id")
    private Integer terminalid;

}
