package org.edu.modules.dorm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.dorm.entity.VeDormNews;
import org.edu.modules.dorm.vo.VeDormNewsVo;

import java.util.List;

public interface VeDormNewsService extends IService<VeDormNews> {

    List<VeDormNewsVo> queryNewsByName(String title,Long typeId);

    VeDormNewsVo updateMessage(Long id);
}
