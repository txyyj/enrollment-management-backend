package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormSuSVo对象", description = "宿舍楼列表")
public class VeDOrmSSLouExcelVo {

    @ApiModelProperty(value = "id")
    private Integer id;

    @Excel(name = "宿舍楼名称", width = 15)
    @ApiModelProperty(value = "宿舍楼名称")
    private String jzwmc;

    @Excel(name = "宿舍类型", width = 15)
    @ApiModelProperty(value ="性别")
    private String xb;

    @Excel(name = "宿舍楼代码", width = 15)
    @ApiModelProperty(value = "建筑物号")
    private String jzwh;

    @Excel(name="所属校区", width =15)
    @ApiModelProperty(value = "校区名称")
    private String xqmc;

    @Excel(name = "层数", width = 15)
    @ApiModelProperty(value ="建筑物层数")
    private Integer jzwcs;

    @Excel(name ="面积", width = 15)
    @ApiModelProperty(value ="总建筑面积")
    private Integer zjzmj;

    @Excel(name ="地址", width = 15)
    @ApiModelProperty(value ="建筑物地址")
    private String jzwdz;

    @Excel(name ="排序", width = 15)
    @ApiModelProperty(value ="排序")
    private Integer listSort;

    @Excel(name ="状态", width = 15)
    @ApiModelProperty(value ="状态")
    private String  state;



}
