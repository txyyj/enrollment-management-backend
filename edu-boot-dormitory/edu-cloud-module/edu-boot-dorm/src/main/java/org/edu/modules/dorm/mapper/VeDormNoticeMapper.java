package org.edu.modules.dorm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import feign.Param;
import org.edu.modules.dorm.entity.VeDormNotice;
import org.edu.modules.dorm.vo.VeDormNoticeVo;

import java.util.List;

public interface VeDormNoticeMapper extends BaseMapper<VeDormNotice> {

    List<VeDormNoticeVo> queryByTitle(@Param("title") String title);

}
