package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormSuSheTableVo{

    private Integer id;

    //校区号
    @Excel(name = "校区号", width = 15)
    @ApiModelProperty(value = "校区号")
    private Integer xqh;

    //宿舍楼编号
    @Excel(name = "宿舍楼编号", width = 15)
    @ApiModelProperty(value = "宿舍楼编号")
    private Integer jzwh;

    //楼层号
    @Excel(name = "楼层号", width = 15)
    @ApiModelProperty(value = "楼层号")
    private Integer lch;

    //房间编号
    @Excel(name = "房间编号", width = 15)
    @ApiModelProperty(value = "房间编号")
    private Integer fjbh;

    //入住性别
    @Excel(name = "入住性别", width = 15)
    @ApiModelProperty(value = "入住性别")
    private Integer rzxb;

    //是否可用
    @Excel(name = "是否可用", width = 15)
    @ApiModelProperty(value = "是否可用")
    private Integer sfky;

    //可住人数
    @Excel(name = "可住人数", width = 15)
    @ApiModelProperty(value = "已住人数")
    private Integer kzrs;

    //已住人数
    @Excel(name = "已住人数", width = 15)
    @ApiModelProperty(value = "已住人数")
    private Integer yzrs;

    //网络端口
    @Excel(name = "网络端口", width = 15)
    @ApiModelProperty(value = "网络端口")
    private String wldk;

    //安装电视机
    @Excel(name = "安装电视机", width = 15)
    @ApiModelProperty(value = "安装电视机")
    private String azdsj;

    //电话端口
    @Excel(name = "电话端口", width = 15)
    @ApiModelProperty(value = "电话端口")
    private String dhdk;

    //电话号码
    @Excel(name = "电话号码", width = 15)
    @ApiModelProperty(value = "电话号码")
    private Integer dhhm;

    //是否有网络
    @Excel(name = "是否有网络", width = 15)
    @ApiModelProperty(value = "是否有网络")
    private Integer sfwl;

    //是否电视机
    @Excel(name = "是否电视机", width = 15)
    @ApiModelProperty(value = "是否电视机")
    private Integer sfdsj;

    //水表底数
    @Excel(name = "水表底数", width = 15)
    @ApiModelProperty(value = "水表底数")
    private Integer sbds;

    //电表底数
    @Excel(name = "电表底数", width = 15)
    @ApiModelProperty(value = "电表底数")
    private Integer dbds;

    //宿舍备注
    @Excel(name = "宿舍备注", width = 15)
    @ApiModelProperty(value = "宿舍备注")
    private Integer ssbz;

    //评优级别
    @Excel(name = "评优级别", width = 15)
    @ApiModelProperty(value = "评优级别")
    private Integer star;

    //状态
    @Excel(name = "状态", width = 15)
    @ApiModelProperty(value = "状态")
    private Integer status;

    //系统id
    @Excel(name="电表底数",width =15)
    @ApiModelProperty(value = "电表底数")
    private Integer terminalId;

    //校区名
    @Excel(name="电表底数",width =15)
    @ApiModelProperty(value = "电表底数")
    private String xqmc;

    //宿舍楼名
    @Excel(name="电表底数",width =15)
    @ApiModelProperty(value = "电表底数")
    private String jzwmc;

    //楼层名
    @Excel(name="电表底数",width =15)
    @ApiModelProperty(value = "电表底数")
    private String lcname;

}
