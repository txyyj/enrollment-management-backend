package org.edu.modules.dorm.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "VeDormSuSheStudentVo", description = "学生宿舍关系查询")
public class VeDormSuSheStudentVo implements Serializable {

    @ApiModelProperty(value = "id")
    private Integer id;

    @ApiModelProperty(value = "姓名")
    private String xm;

    @ApiModelProperty(value = "学号")
    private String xh;

    @ApiModelProperty(value = "建筑物名称")
    private String jzwmc;

    @ApiModelProperty(value = "房间编码")
    private String fjbm;

    @ApiModelProperty(value = "床位号")
    private String  cwh;

    @ApiModelProperty(value = "楼层")
    private String  lcname;

}
