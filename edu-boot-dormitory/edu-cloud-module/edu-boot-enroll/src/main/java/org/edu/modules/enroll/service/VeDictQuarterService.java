package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeDictQuarter;
import org.edu.modules.enroll.vo.VeDictQuarterVo;

import java.util.Date;
import java.util.List;
//招生季接口

public interface VeDictQuarterService extends IService<VeDictQuarter> {


//    // 新增校验相同的招生季代码，招生季名称 修改特定的招生季为当前招生季
//    Integer AddupdateIsCurOne (String code,String name);
//// 新增校验相同的招生季代码，招生季名称
//List<VeDictQuarterVo>  CheckAddQuarter (String code,String name);


//    //校验相同的招生季代码，招生季名称
//    List<VeDictQuarterVo> CheckQuarter(String code,String name,Integer id);
    //将所有的招生季设为否
//    Integer updateAnyIsCur();
//
//    //将特定招生季改为非当前招生季
//    Integer  updateIsCur(String code,String name,Integer id);


    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    //查询所有 (pageNo-1)*pageSize, pageSize
    List<VeDictQuarterVo> selectById();
    //编辑
    Integer    updateQuarter (String code,
                              Integer id,
                              String name,
                              String year,
                              Integer isCur,
                              String rxny,
                              Date start_Time,
                              Date end_Time);
    //l逻辑删除
    Integer delQuarter(String code,Integer id);
    //设置当前招生季
    Integer  updateIsCurOne(Integer isCur, Integer id);

    //添加新的招生季
    Integer addQuarter( String code,String name,String year,Integer isCur,String rxny, Date start_Time,Date end_Time);
}
