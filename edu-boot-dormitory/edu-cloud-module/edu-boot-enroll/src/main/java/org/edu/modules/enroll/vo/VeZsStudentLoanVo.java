package org.edu.modules.enroll.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;
import java.util.Date;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/8/5 9:59
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsStudentLoanVo implements Serializable {
    @ApiModelProperty(value = "id")
    private Long id;

    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private String xm;

    @Excel(name = "身份证号", width = 15)
    @ApiModelProperty(value = "身份证号")
    private String sfzh;

    @Excel(name = "学号", width = 15)
    @ApiModelProperty(value = "学号")
    private String xh;

    @Excel(name = "院系", width = 15)
    @ApiModelProperty(value = "院系")
    private String falId;

    @Excel(name = "专业名称", width = 15)
    @ApiModelProperty(value = "专业名称")
    @TableField(value = "major")
    private String specId;

    @ApiModelProperty(value = "开户行")
    @TableField(value = "bank")
    private String bank;

    @ApiModelProperty(value = "高校账号")
    @TableField(value = "bank_account")
    private String bankAccount;

    @ApiModelProperty(value = "贷款金额")
    @TableField(value = "loan_amount")
    private String loanAmount;

    @ApiModelProperty(value = "审核状态 0未审核 1已审核")
    @TableField(value = "isCheck")
    private String isCheck;

    @ApiModelProperty(value = "银行回执单（存图片url）")
    @TableField(value = "bank_receipt")
    private String bankReceipt;

    @ApiModelProperty(value = "不通过理由")
    @TableField(value = "failure_reason")
    private String failureReason;

    @ApiModelProperty(value = "合同编号")
    @TableField(value = "contract_no")
    private String contractNo;

    @ApiModelProperty(value = "高校账号名称")
    @TableField(value = "bank_name")
    private String bankName;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "正常毕业时间")
    @TableField(value = "graduationTime")
    private Date graduationTime;
}
