package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeZsFenban;
import org.edu.modules.enroll.vo.VeDormSushe;
import org.edu.modules.enroll.vo.VeZsArrangeSusheVo;
import org.edu.modules.enroll.vo.VeZsPlanBedVo;

import java.util.HashMap;
import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-07-25 20:48
 */
//public interface VeFenSusheService {
public interface VeFenSusheService extends IService<VeZsFenban>{//林彬辉
    //显示安排床位班级的表格
    HashMap<String, Object> getPlanBedList(Integer gradeId, Integer falId, Integer banjiId, Integer currentPage, Integer pageSize);
    //显示选择安排入住的寝室表格
    HashMap<String, Object> getArrangeSusheList(Integer jzwmc, Integer lch, String fjbm,Integer sfwk,Integer xb, Integer currentPage, Integer pageSize);
    //获取指定班级的所有未分配宿舍的学生
    List<VeZsFenban> getStudent(Integer banjiId);
    //给这些班级的学生分配宿舍
    boolean fenpeiSushe(Long id, Integer susheId);
    //查询某个宿舍的信息（用于计算空床位数）
    VeDormSushe getSusheMessage( Integer susheId);
}
