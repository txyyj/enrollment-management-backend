package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.VeBaseCampus;

public interface VeBaseCampusMappper extends BaseMapper<VeBaseCampus> {
}
