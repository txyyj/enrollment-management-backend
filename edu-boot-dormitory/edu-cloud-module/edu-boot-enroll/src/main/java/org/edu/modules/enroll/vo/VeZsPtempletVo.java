package org.edu.modules.enroll.vo;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;
import java.util.Date;

/**
 * @Description: 通知书模板表vo
 * @Author:  wcj
 * @Date:  2021-04-23
 * @Version:  V1.0
 */

@Data
@TableName("ve_zs_ptemplet")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsPtempletVo implements Serializable {

    /** id */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private int id;

    /** 模板名称 */
    @Excel(name = "模板名称", width = 15)
    @ApiModelProperty(value = "模板名称")
    @TableField(value = "name")
    private String name;

    /** 宽度 */
    @Excel(name = "宽度", width = 15)
    @ApiModelProperty(value = "宽度")
    @TableField(value = "width")
    private Double width;

    /** 高度 */
    @Excel(name = "高度", width = 15)
    @ApiModelProperty(value = "高度")
    @TableField(value = "height")
    private Double height;

    /** 图片ID */
    @Excel(name = "图片ID", width = 15)
    @ApiModelProperty(value = "图片ID")
    @TableField(value = "fileId")
    private int fileId;

    /** 打印代码 */
    @Excel(name = "打印代码", width = 15)
    @ApiModelProperty(value = "打印代码")
    private String code;

    /** 启用状态0禁用1启用 */
    @Excel(name = "启用状态", width = 15)
    @ApiModelProperty(value = "启用状态0禁用1启用")
    private int status;

    /**打印方向类型*/
    @Excel(name = "打印方向类型", width = 15)
    @ApiModelProperty(value = "打印方向类型")
    private String printType;

    /**打印方向名称*/
    @Excel(name = "打印方向类型", width = 15)
    @ApiModelProperty(value = "打印方向类型")
    private String printTypeName;




}
