package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.enroll.entity.VeZsSingleEnrollExpansion;

import java.util.List;

/**
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.service
 * @date 2021/7/28 18:31
 */
public interface SingleAndExpansionService extends IService<VeZsSingleEnrollExpansion> {
    List<VeZsSingleEnrollExpansion> getSingleAndExpansion(Long specialtyId,String  stuName,
                                                          String sfzh,Integer currentPage,
                                                          Integer pageSize, Integer isCheck);
    Integer countSingleAndExpansion(Long specialtyId,String  stuName,
                                    String sfzh, Integer isCheck);
}
