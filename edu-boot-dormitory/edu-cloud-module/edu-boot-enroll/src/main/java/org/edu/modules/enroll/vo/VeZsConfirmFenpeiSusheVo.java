package org.edu.modules.enroll.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**确认班级分配床位信息
 * @Auther 李少君
 * @Date 2021-07-26 16:39
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsConfirmFenpeiSusheVo implements Serializable {

    /** id */
    @ApiModelProperty(value = "id")
    private Long id;

    /** 院系 */
    @ApiModelProperty(value = "院系")
    @TableField(value = "ycmc")
    private String yxmc;

    /** 所属年级 */
    @ApiModelProperty(value = "所属年级")
    @TableField(value = "njmc")
    private String njmc;

    /** 班级 */
    @ApiModelProperty(value = "班级")
    @TableField(value = "xzbmc")
    private String xzbmc;

    /** 男生人数 */
    @ApiModelProperty(value = "男生人数")
    @TableField(value = "nansrs")
    private Integer nansrs;

    /** 女生人数 */
    @ApiModelProperty(value = "女生人数")
    @TableField(value = "nvsrs")
    private Integer nvsrs;

    /** 总人数 */
    @ApiModelProperty(value = "总人数(人数上限)")
    @TableField(value = "zrs")
    private Integer zrs;

    /** 本次安排空床位数 */
    @ApiModelProperty(value = "总空床位数")
    @TableField(value = "zkcws")
    private Integer zkcws;

    /** 楼栋 */
    @ApiModelProperty(value = "楼栋")
    @TableField(value = "jzwmc")
    private String jzwmc;

    /** 宿舍号 */
    @ApiModelProperty(value = "宿舍号")
    @TableField(value = "fjbm")
    private String fjbm;

    /** 本次安排空床位数 */
    @ApiModelProperty(value = "本次安排空床位数")
    @TableField(value = "kcws")
    private Integer kcws;



}
