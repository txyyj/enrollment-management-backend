package org.edu.modules.enroll.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**分班列表
 * @Auther 李少君
 * @Date 2021-07-24 11:10
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsFenbanVo implements Serializable {

    /** id */
    @ApiModelProperty(value = "id")
    private Long id;

    /** 学生姓名 */
    @Excel(name = "学生姓名", width = 15)
    @ApiModelProperty(value = "学生姓名")
    @TableField(value = "xm")
    private String xm;

    /** 学生学号 */
    @Excel(name = "学生学号", width = 15)
    @ApiModelProperty(value = "学生学号")
    @TableField(value = "xh")
    private String xh;

    /** 入学总分 */
    @Excel(name = "入学总分", width = 15)
    @ApiModelProperty(value = "入学总分")
    @TableField(value = "mark")
    private Integer mark;

    /** 招生类型 */
    @Excel(name = "招生类型", width = 15)
    @ApiModelProperty(value = "招生类型")
    @TableField(value = "enrollType")
    private String enrollType;

    /** 录取批次 */
    @Excel(name = "录取批次", width = 15)
    @ApiModelProperty(value = "录取批次")
    @TableField(value = "luqu")
    private String luqu;

    /** 分配状态 */
    @Excel(name = "分配状态", width = 15)
    @ApiModelProperty(value = "分配状态")
    @TableField(value = "statuId")
    private Integer statuId;

    /** 校区id */
    @Excel(name = "校区id", width = 15)
    @ApiModelProperty(value = "校区id")
    @TableField(value = "campusId")
    private Integer campusId;

    /** 校区名称 */
    @Excel(name = "校区名称", width = 15)
    @ApiModelProperty(value = "校区名称")
    @TableField(value = "xqmc")
    private String xqmc;

    /** 专业id */
    @Excel(name = "专业id", width = 15)
    @ApiModelProperty(value = "专业id")
    @TableField(value = "specId")
    private Integer specId;

    /** 专业名称 */
    @Excel(name = "专业名称", width = 15)
    @ApiModelProperty(value = "专业名称")
    @TableField(value = "zymc")
    private String zymc;

    /** 年级id */
    @Excel(name = "年级id", width = 15)
    @ApiModelProperty(value = "年级id")
    @TableField(value = "gradeId")
    private Integer gradeId;

    /** 年级名称 */
    @Excel(name = "年级名称", width = 15)
    @ApiModelProperty(value = "年级名称")
    @TableField(value = "njmc")
    private String njmc;

    /** 入学年份 */
    @ApiModelProperty(value = "年级名称")
    @TableField(value = "njdm")
    private String njdm;

    /** 班级id */
    @Excel(name = "班级id", width = 15)
    @ApiModelProperty(value = "班级id")
    @TableField(value = "banjiId")
    private Integer banjiId;

    /** 行班级名称 */
    @Excel(name = "班级名称", width = 15)
    @ApiModelProperty(value = "班级名称")
    @TableField(value = "xzbmc")
    private String xzbmc;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 15)
    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time")
    private Date createTime;

    /** 创建人id */
    @Excel(name = "创建人id", width = 15)
    @ApiModelProperty(value = "创建人id")
    @TableField(value = "update_by")
    private String createBy;

    /** 更新时间 */
    @Excel(name = "更新时间", width = 15)
    @ApiModelProperty(value = "更新时间")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField(value = "update_time")
    private Date updateTime;

    /** 终端ID */
    @Excel(name = "终端ID", width = 15)
    @ApiModelProperty(value = "终端ID")
    @TableField(value = "terminalId")
    private Integer terminalId;

    /** 逻辑删除的标识 */
    @Excel(name = "逻辑删除", width = 15)
    @ApiModelProperty(value = "逻辑删除（0=否，1=是）")
    @TableField(value = "is_deleted")
    @TableLogic
    private Integer is_deleted;

    //班级总人数
    /** 终端ID */
    @Excel(name = "总人数", width = 15)
    @ApiModelProperty(value = "总人数")
    @TableField(value = "zrs")
    private Integer zrs;

}
