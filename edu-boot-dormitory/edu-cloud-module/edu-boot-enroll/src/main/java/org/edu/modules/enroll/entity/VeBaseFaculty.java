package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;
/**
 * @Description:  院系表对象类接口
 * @Author:  wcj
 * @Date:  2021-04-05
 * @Version:  V1.0
 */

@Data
@TableName(value = "ve_base_faculty",schema = "edu_dev")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeBaseFaculty implements Serializable {
    
    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /**院系代码*/
    @Excel(name = "院系代码", width = 15)
    @ApiModelProperty(value = "院系代码")
    @TableField(value = "YXDM")
    private String yxdm;

    /**院系名称*/
    @Excel(name = "院系名称", width = 15)
    @ApiModelProperty(value = "院系名称")
    @TableField(value = "YXMC")
    private String  yxmc;

}
