package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeBaseGrade;
import org.edu.modules.enroll.mapper.VeBaseGradeMapper;
import org.edu.modules.enroll.service.VeBaseGradeService;
import org.springframework.stereotype.Service;

@Service
public class VeBaseGradeServiceImpl extends ServiceImpl<VeBaseGradeMapper, VeBaseGrade> implements VeBaseGradeService {
}
