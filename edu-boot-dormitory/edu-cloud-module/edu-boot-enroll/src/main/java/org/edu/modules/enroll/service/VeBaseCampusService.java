package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.enroll.entity.VeBaseCampus;
import org.springframework.stereotype.Service;


public interface VeBaseCampusService extends IService<VeBaseCampus> {
}
