package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.enroll.entity.VeBaseSemester;
import org.springframework.stereotype.Service;


public interface VeBaseSemesterService extends IService<VeBaseSemester> {
}
