package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeZsPtemplet;
import org.edu.modules.enroll.mapper.VeZsPtempletMapper;
import org.edu.modules.enroll.service.VeZsPtempletService;
import org.springframework.stereotype.Service;

/**
 * 通知书模板表接口实现类
 * @author wcj
 */
@Service
public class VeZsPtempletServiceImpl extends ServiceImpl<VeZsPtempletMapper, VeZsPtemplet> implements VeZsPtempletService {
}
