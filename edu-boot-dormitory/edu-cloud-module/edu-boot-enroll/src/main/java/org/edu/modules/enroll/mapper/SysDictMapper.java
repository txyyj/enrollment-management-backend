package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.SysDict;

/**数据字典表接口
 * @author 86158
 */
public interface SysDictMapper  extends BaseMapper<SysDict> {
}
