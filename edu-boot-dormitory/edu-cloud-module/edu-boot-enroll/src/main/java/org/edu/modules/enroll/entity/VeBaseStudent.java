package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.util.Date;

/**
 * @Description:  学生表
 * @Author:  yhx
 * @Date:  2021-04-11
 * @Version:  V1.0
 */

@Data
@TableName(value = "ve_base_student",schema = "edu_dev")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeBaseStudent {

    /** id */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /** 身份证号 */
    @Excel(name = "身份证号", width = 15)
    @ApiModelProperty(value = "身份证号")
    @TableField(value = "SFZH")
    private String sfzh;

    /** 学号 */
    @Excel(name = "学号", width = 15)
    @ApiModelProperty(value = "学号")
    @TableField(value = "XH")
    private String xh;

    /** 姓名 */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    @TableField(value = "XM")
    private String xm;

    /** 性别码 */
    @Excel(name = "性别码", width = 15)
    @ApiModelProperty(value = "性别码")
    @TableField(value = "XBM")
    private String xbm;

    /** 用户ID */
    @Excel(name = "用户ID", width = 15)
    @ApiModelProperty(value = "用户ID")
    @TableField(value = "user_id")
    private String userId;

    /** 民族码 */
    @Excel(name = "民族码", width = 15)
    @ApiModelProperty(value = "民族码")
    @TableField(value = "MZM")
    private String mzm;

    /** 报名号 */
    @Excel(name = "报名号", width = 15)
    @ApiModelProperty(value = "报名号，唯一")
    @TableField(value = "BMH")
    private String bmh;

    /** 就读方式 */
    @Excel(name = "就读方式", width = 15)
    @ApiModelProperty(value = "就读方式：1住校，2走读")
    @TableField(value = "JDFS")
    private Integer jdfs;

    /** 当前状态码 */
    @Excel(name = "当前状态码", width = 15)
    @ApiModelProperty(value = "当前状态码'XS'=>'新生', 'ZX' => '在校', 'XX' => '休学', 'TX' => '退学', 'KC' => '开除', 'BY' => '毕业', 'YY' => '肄业', 'ZXX' => '转学', 'JY' => '结业'")
    @TableField(value = "XSDQZTM")
    private String xsdqztm;

    /** 入学年月 */
    @Excel(name = "入学年月", width = 15)
    @ApiModelProperty(value = "入学年月")
    @TableField(value = "RXNY")
    private Long rxny;

    /** 学制ID */
    @Excel(name = "学制ID", width = 15)
    @ApiModelProperty(value = "学制ID")
    @TableField(value = "XZ")
    private Long xz;

    /** 院系ID */
    @Excel(name = "院系ID", width = 15)
    @ApiModelProperty(value = "院系ID")
    @TableField(value = "fal_id")
    private Long falId;

    /** 专业ID */
    @Excel(name = "专业ID", width = 15)
    @ApiModelProperty(value = "专业ID")
    @TableField(value = "spec_id")
    private Long specId;

    /** 班级ID */
    @Excel(name = "班级ID", width = 15)
    @ApiModelProperty(value = "班级ID")
    @TableField(value = "bj_id")
    private Long bjId;

    /** 年级ID */
    @Excel(name = "年级ID", width = 15)
    @ApiModelProperty(value = "年级ID")
    @TableField(value = "grade_id")
    private Long gradeId;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 15)
    @ApiModelProperty(value = "创建时间")
    @TableField(value = "create_time")
    private Long createTime;

    /** 更新时间 */
    @Excel(name = "更新时间", width = 15)
    @ApiModelProperty(value = "更新时间")
    @TableField(value = "update_time")
    private Long updateTime;

    /** 户口所在省份 */
    @Excel(name = "户口所在省份", width = 15)
    @ApiModelProperty(value = "户口所在省份")
    @TableField(value = "province")
    private String province;

    /** 省份ID */
    @Excel(name = "省份ID", width = 15)
    @ApiModelProperty(value = "省份ID")
    @TableField(value = "province_id")
    private Long provinceId;

    /** 户口所在市 */
    @Excel(name = "户口所在市", width = 15)
    @ApiModelProperty(value = "户口所在市")
    @TableField(value = "city")
    private String city;

    /** 市ID */
    @Excel(name = "市ID", width = 15)
    @ApiModelProperty(value = "市ID")
    @TableField(value = "city_id")
    private Long cityId;

    /** 户口所在区 */
    @Excel(name = "户口所在区", width = 15)
    @ApiModelProperty(value = "户口所在区")
    @TableField(value = "county")
    private String county;

    /** 区ID */
    @Excel(name = "区ID", width = 15)
    @ApiModelProperty(value = "区ID")
    @TableField(value = "county_id")
    private Long countyId;

    /** 生源地省id */
    @Excel(name = "生源地省id", width = 15)
    @ApiModelProperty(value = "生源地省id")
    @TableField(value = "sheng_id")
    private Long shengId;

    /** 生源地市id */
    @Excel(name = "生源地市id", width = 15)
    @ApiModelProperty(value = "生源地市id")
    @TableField(value = "shi_id")
    private Long shiId;

    /** 生源地区id */
    @Excel(name = "生源地区id", width = 15)
    @ApiModelProperty(value = "生源地区id")
    @TableField(value = "qu_id")
    private Long quId;

    /** 是否是困难生 */
    @Excel(name = "是否是困难生", width = 15)
    @ApiModelProperty(value = "是否是困难生 0=否  1=是")
    @TableField(value = "SFKNS")
    private Integer sfkns;

    /** 终端ID */
    @Excel(name = "终端ID", width = 15)
    @ApiModelProperty(value = "终端ID")
    @TableField(value = "terminal_id")
    private Long terminalId;

    /** 准考证号 */
    @Excel(name = "准考证号", width = 15)
    @ApiModelProperty(value = "准考证号")
    @TableField(value = "ZKZH")
    private String zkzh;

    /** 考生号 */
    @Excel(name = "考生号", width = 15)
    @ApiModelProperty(value = "考生号")
    @TableField(value = "KSH")
    private String ksh;

    /** 更新状态 */
    @Excel(name = "更新状态", width = 15)
    @ApiModelProperty(value = "更新状态（0：未更新; 1：已更新）")
    @TableField(value = "update_status")
    private Integer updateStatus;

}
