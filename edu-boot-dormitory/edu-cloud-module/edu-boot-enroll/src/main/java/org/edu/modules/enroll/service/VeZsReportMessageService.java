package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeZsReportMsg;

import java.util.List;

/**
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.service
 * @date 2021/7/19 21:11
 */
public interface VeZsReportMessageService extends IService<VeZsReportMsg> {
    List<VeZsReportMsg> getReportMessageList(String endPlaceId, String staTime,
                                             String endTime, Integer isNeed, Integer currentPage,
                                             Integer pageSize);
    Integer countReportMessage(String endPlaceId, String staTime,
                               String endTime, Integer isNeed);
    //删除
    Integer deletedById(Integer id);
}
