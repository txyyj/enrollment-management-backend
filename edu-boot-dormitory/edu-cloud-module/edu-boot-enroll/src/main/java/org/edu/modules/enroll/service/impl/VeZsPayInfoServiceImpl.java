package org.edu.modules.enroll.service.impl;

import org.edu.modules.enroll.entity.VeZsPayInfo;
import org.edu.modules.enroll.mapper.VeZsPayInfoMapper;
import org.edu.modules.enroll.service.VeZsPayInfoService;
import org.edu.modules.enroll.vo.VeZsPayInfoVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-08-05 17:28
 */
@Service
public class VeZsPayInfoServiceImpl implements VeZsPayInfoService {

    @Resource
    private VeZsPayInfoMapper veZsPayInfoMapper;

    @Override
    public boolean addPayInfo(VeZsPayInfo veZsPayInfo) {
        return veZsPayInfoMapper.addPayInfo(veZsPayInfo);
    }

    @Override
    public boolean updatePayInfo(VeZsPayInfo veZsPayInfo) {
        return veZsPayInfoMapper.updatePayInfo(veZsPayInfo);
    }

    @Override
    public HashMap<String, Object> getPayInfoList(Integer falId, Integer specId, String sfzh, String xm, Integer state, Integer currentPage, Integer pageSize) {
        Integer start = pageSize * (currentPage - 1);
        List<VeZsPayInfoVo> list = veZsPayInfoMapper.getPayInfoList(falId, specId, sfzh, xm, state, start, pageSize);
        Integer count = veZsPayInfoMapper.payInfoCount(falId, specId, sfzh, xm, state);
        HashMap<String,Object> map = new HashMap<>();
        map.put("list",list);
        map.put("count",count);
        return map;
    }

    @Override
    public boolean auditPass(Integer id, Integer state) {
        return veZsPayInfoMapper.auditPass(id, state);
    }

    @Override
    public boolean auditNoPass(Integer id, Integer state, String failureReason) {
        return veZsPayInfoMapper.auditNoPass(id, state, failureReason);
    }

    @Override
    public VeZsPayInfoVo showPayInfo(Integer id) {
        return veZsPayInfoMapper.showPayInfo(id);
    }
}
