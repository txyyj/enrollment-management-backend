package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.VeBaseSemester;

public interface VeBaseSemesterMapper extends BaseMapper<VeBaseSemester> {
}
