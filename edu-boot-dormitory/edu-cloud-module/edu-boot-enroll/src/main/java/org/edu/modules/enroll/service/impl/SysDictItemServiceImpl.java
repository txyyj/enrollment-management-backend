package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.SysDictItem;
import org.edu.modules.enroll.mapper.SysDictItemMapper;
import org.edu.modules.enroll.service.SysDictItemService;
import org.springframework.stereotype.Service;

/**
 * @author 86158
 */

@Service
public class SysDictItemServiceImpl extends ServiceImpl<SysDictItemMapper, SysDictItem> implements SysDictItemService {


}
