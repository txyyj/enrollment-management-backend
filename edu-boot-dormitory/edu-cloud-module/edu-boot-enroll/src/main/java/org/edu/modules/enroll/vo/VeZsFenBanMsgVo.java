package org.edu.modules.enroll.vo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableLogic;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Auther 李少君
 * @Date 2021-08-13 17:19
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsFenBanMsgVo implements Serializable {
    /** id */
    @ApiModelProperty(value = "id")
    private Long id;

    /** 学生姓名 */
    @Excel(name = "学生姓名", width = 15)
    @ApiModelProperty(value = "学生姓名")
    @TableField(value = "xm")
    private String xm;

    /** 学生学号 */
    @Excel(name = "学生学号", width = 15)
    @ApiModelProperty(value = "学生学号")
    @TableField(value = "xh")
    private String xh;

    /** 入学总分 */
    @Excel(name = "入学总分", width = 15)
    @ApiModelProperty(value = "入学总分")
    @TableField(value = "mark")
    private Integer mark;

    /** 招生类型 */
    @Excel(name = "招生类型", width = 15)
    @ApiModelProperty(value = "招生类型")
    @TableField(value = "enrollType")
    private String enrollType;

    /** 录取批次 */
    @Excel(name = "录取批次", width = 15)
    @ApiModelProperty(value = "录取批次")
    @TableField(value = "luqu")
    private String luqu;

    /** 分配状态 */
    @Excel(name = "分配状态", width = 15)
    @ApiModelProperty(value = "分配状态")
    @TableField(value = "statuId")
    private Integer statuId;

    /** 校区id */
    @Excel(name = "校区id", width = 15)
    @ApiModelProperty(value = "校区id")
    @TableField(value = "campusId")
    private Integer campusId;

    /** 专业id */
    @Excel(name = "专业id", width = 15)
    @ApiModelProperty(value = "专业id")
    @TableField(value = "specId")
    private Integer specId;

    /** 年级id */
    @Excel(name = "年级id", width = 15)
    @ApiModelProperty(value = "年级id")
    @TableField(value = "gradeId")
    private Integer gradeId;

    /** 班级id */
    @Excel(name = "班级id", width = 15)
    @ApiModelProperty(value = "班级id")
    @TableField(value = "banjiId")
    private Integer banjiId;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 15)
    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time",fill = FieldFill.INSERT)
    private Date createTime;

    /** 正常毕业时间 */
    @Excel(name = "正常毕业时间", width = 15)
    @ApiModelProperty(value = "正常毕业时间")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField(value = "graduation_time",fill = FieldFill.INSERT)
    private Date graduationTime;

    /** 创建人id */
    @Excel(name = "创建人id", width = 15)
    @ApiModelProperty(value = "创建人id")
    @TableField(value = "update_time",update = "now()")
    private String createBy;

    /** 更新时间 */
    @Excel(name = "更新时间", width = 15)
    @ApiModelProperty(value = "更新时间")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField(value = "update_time")
    private Date updateTime;

    /** 更新人id */
    @Excel(name = "更新人id", width = 15)
    @ApiModelProperty(value = "更新人id")
    @TableField(value = "update_by")
    private String updateBy;

    /** 终端ID */
    @Excel(name = "终端ID", width = 15)
    @ApiModelProperty(value = "终端ID")
    @TableField(value = "terminalId")
    private Integer terminalId;

    /** 逻辑删除的标识 */
    @Excel(name = "逻辑删除", width = 15)
    @ApiModelProperty(value = "逻辑删除（0=否，1=是）")
    @TableField(value = "is_deleted")
    @TableLogic
    private Integer is_deleted;

    /** 性别 */
    @Excel(name = "性别", width = 15)
    @ApiModelProperty(value = "性别（0=男，1=女）")
    @TableField(value = "xb")
    private String xb;

    /** 院系id */
    @Excel(name = "院系", width = 15)
    @ApiModelProperty(value = "院系")
    @TableField(value = "falId")
    private Integer falId;

    /** 宿舍id */
    @Excel(name = "宿舍id", width = 15)
    @ApiModelProperty(value = "宿舍id")
    @TableField(value = "susheId")
    private Integer susheId;

    /** 身份证号 */
    @Excel(name = "身份证号", width = 15)
    @ApiModelProperty(value = "身份证号")
    @TableField(value = "SFZH")
    private String sfzh;

    /** 名族码 */
    @TableField(value = "MZM")
    private String mzm;

    /** 报名号 */
    @TableField(value = "BMH")
    private String bmh;

    /** 就读方式 */
    @TableField(value = "JDFS")
    private Integer jdfs;

    /** 当前状态码 */
    @TableField(value = "XSDQZTM")
    private String xsdqztm;

    /** 入学年月 */
    @TableField(value = "RXNY")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Long rxny;

    /** 学制 */
    @TableField(value = "XZ")
    private Integer xz;

    /** 户口所在省份 */
    @TableField(value = "province")
    private String province;

    /** 户口所在省份id */
    @TableField(value = "province_id")
    private Integer provinceId;

    /** 户口所在市 */
    @TableField(value = "city")
    private String city;

    /** 户口所在市id */
    @TableField(value = "city_id")
    private Integer cityId;

    /** 户口所在区 */
    @TableField(value = "county")
    private String county;

    /** 户口所在区id */
    @TableField(value = "county_id")
    private Integer countyId;

    /** 生源地省id */
    @TableField(value = "sheng_id")
    private Integer shengId;

    /** 生源地市id */
    @TableField(value = "shi_id")
    private Integer shiId;

    /** 生源地区id */
    @TableField(value = "qu_id")
    private Integer quId;

    /** 是否是困难生 */
    @TableField(value = "SFKNS")
    private Integer sfkns;

    /** 准考证号 */
    @TableField(value = "ZKZH")
    private String zkzh;

    /** 考生号 */
    @TableField(value = "KSH")
    private String ksh;

    /** 出生年月 */
    @TableField(value = "CSNY")
    private String csny;

    /** 中学名称 */
    @TableField(value = "ZXMC")
    private String zxmc;

    /** 邮政编码 */
    @TableField(value = "YZBM")
    private String yzbm;

    /** 邮寄地址 */
    @TableField(value = "YJDZ")
    private String yjdz;

    /** 邮编 */
    @TableField(value = "YB")
    private String yb;

    /** 联系电话1 */
    @TableField(value = "LXDH1")
    private String lxdh1;

    /** 联系电话2 */
    @TableField(value = "LXDH2")
    private String lxdh2;

    /** 投档成绩 */
    @TableField(value = "TDCJ")
    private String tdcj;

    /** 志愿 */
    @TableField(value = "ZY")
    private String zy;

    /** 录取专业 */
    @TableField(value = "LQZY")
    private String lqzy;

    /** 寄件人 */
    @TableField(value = "JJR")
    private String jjr;

    /** 寄件人电话 */
    @TableField(value = "JJRDH")
    private String jjrdh;

    /** 寄件人地址 */
    @TableField(value = "JJRDZ")
    private String jjrdz;

    /** 收件人 */
    @TableField(value = "SJR")
    private String sjr;

    /** 收件人家庭地址 */
    @TableField(value = "SJRJTDZ")
    private String sjrjtdz;

    /** 邮件号 */
    @TableField(value = "YJH")
    private String yjh;

    /** 重量 */
    @TableField(value = "ZL")
    private String zl;

    @TableField(value = "YXMC")
    private String yxmc;

    @TableField(value = "ZYMC")
    private String zymc;

    @TableField(value = "XZBMC")
    private String xzbmc;
}
