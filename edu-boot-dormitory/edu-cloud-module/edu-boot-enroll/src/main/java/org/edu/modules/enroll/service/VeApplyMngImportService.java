package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.enroll.entity.VeDictQuarter;
import org.edu.modules.enroll.entity.VeZsRegistration;
import org.edu.modules.enroll.vo.ApplyMngImportVo;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public interface VeApplyMngImportService extends IService<VeZsRegistration> {

    /**
     * 取报名信息管理集合（已审核）导入模板
     * @return 新生报到集合
     */
    List<ApplyMngImportVo> getApplyMsgMngList();

    /**
     * 校验表格信息必填项是否为空
     * @return HashMap<String, String> result 0失败 1成功  msg 结果信息
     * @param list 表格文件中的对象集合
     * */
    HashMap<String, String> checkList(List<ApplyMngImportVo>list);

    /**
     * 校验表格信息是否有效
     * @return HashMap<String, String> result 0失败 1成功  msg 结果信息
     * @param list 表格文件中的对象集合
     * @param veDictQuarter 招生季对象
     * */
    HashMap<String, String> checkUserful(List<ApplyMngImportVo>list, VeDictQuarter veDictQuarter);


}
