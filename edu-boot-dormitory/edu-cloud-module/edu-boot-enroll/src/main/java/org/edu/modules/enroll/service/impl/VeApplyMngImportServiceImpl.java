package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.common.api.vo.Result;
import org.edu.modules.enroll.entity.*;
import org.edu.modules.enroll.mapper.*;

import org.edu.modules.enroll.service.IVeBaseManageService;
import org.edu.modules.enroll.service.VeApplyMngImportService;

import org.edu.modules.enroll.service.VeBaseGradeService;
import org.edu.modules.enroll.service.VeZsRegistrationService;
import org.edu.modules.enroll.vo.ApplyMngImportVo;

import org.edu.modules.enroll.vo.VeZsRegistrationVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

/**报名管理页面 导出模板
 * @author wcj
 */

@Service
public class VeApplyMngImportServiceImpl extends ServiceImpl<VeApplyMngImportMapper, VeZsRegistration> implements VeApplyMngImportService {
    @Resource
    private VeApplyMngImportMapper veApplyMngExportMapper;

    @Resource
    private VeBaseFacultyMapper veBaseFacultyMapper;

    @Resource
    private VeBaseSpecialtyMapper veBaseSpecialtyMapper;

    @Resource
    private VeDictAreaMapper veDictAreaMapper;

    @Resource
    private VeDictXueziMapper veDictXueziMapper;

    @Resource
    private SysDictMapper sysDictMapper;

    @Resource
    private SysDictItemMapper sysDictItemMapper;

    @Resource
    private VeZsRegistrationMapper veZsRegistrationMapper;
//  ========  林彬辉
    @Resource
    private IVeBaseManageService iVeBaseManageService;
    @Resource
    private VeBaseGradeService veBaseGradeService;
//    ======
    @Resource
    private VeZsRegistrationService veZsRegistrationService;
    /**
     * 获取报名信息管理业务实现类
     */
    @Override
    public List<ApplyMngImportVo> getApplyMsgMngList() {
        List<ApplyMngImportVo> list = veApplyMngExportMapper.getApplyMngList();
        return list;
    }

    @Override
    public HashMap<String, String> checkList(List<ApplyMngImportVo> list) {
        HashMap<String, String> map = new HashMap<>();

        Boolean full=true;
        String result=null;
        int count=0;
        String msg=null;
        for (ApplyMngImportVo vo : list) {
            count++;
            //数据库对象
            //数据校验  12个必填信息不能为空
            //姓名不为空
            if ("".equals(vo.getXm()) || vo.getXm() == null) {
                full = false;
            }
            //性别不为空
            if ("".equals(vo.getXbm()) || vo.getXbm() == null) {
                full = false;
            }
            //身份证不为空
            if ("".equals(vo.getSfzh()) || vo.getSfzh() == null) {
                full = false;
            }
            //学制不为空
            if ("".equals(vo.getXz()) || vo.getXz() == null) {
                full = false;
            }
            //家庭联系电话不为空
            if ("".equals(vo.getJtlxdh()) || vo.getJtlxdh() == null) {
                full = false;
            }
            //家庭邮编
            if ("".equals(vo.getJtyb()) || vo.getJtyb() == null) {
                full = false;
            }
            //家庭地址
            if ("".equals(vo.getJtdz()) || vo.getJtdz() == null) {
                full = false;
            }
            //出生日期
            if ("".equals(vo.getCsrq()) || vo.getCsrq() == null) {
                full = false;
            }

            //专业信息不为空
            if ("".equals(vo.getYxmc()) || vo.getYxmc() == null) {
                full = false;
            }
            if ("".equals(vo.getZymc()) || vo.getZymc() == null) {
                full = false;
            }
            //毕业学校
            if ("".equals(vo.getByxx()) || vo.getByxx() == null) {
                full = false;
            }
            //层次码不为空 添加字典
//            if (vo.getCcm().equals("") || vo.getCcm() == null) {
//                full = false;
//            }
            //户籍信息不为空
            if ( vo.getProvince() == null||"".equals(vo.getProvince()) ) {
                full = false;
            }
            if ( vo.getCity() == null||"".equals(vo.getCity()) ) {
                full = false;
            }
            if ( vo.getCounty() == null ||"".equals(vo.getCounty()) ) {
                full = false;
            }

            if(!full){
               msg= "文件导入失败:第"+count+"行学生信息未完善" ;
                //o失败，1成功
               result="0";
               map.put("msg",msg);
               map.put("result",result);
               return map;

            }
        }

        //o失败，1成功
        result="1";
        map.put("msg",null);
        map.put("result",result);
        return map;
    }

    @Override
    public HashMap<String, String> checkUserful(List<ApplyMngImportVo> list,VeDictQuarter veDictQuarter) {

        //获取全部院校
        QueryWrapper<VeBaseFaculty>queryWrapper=new QueryWrapper();
        List<VeBaseFaculty> veBaseFacultylist = veBaseFacultyMapper.selectList(queryWrapper);//2021.9.2
//       ======= 林彬辉
//        BasicResponseBO<List<VeBaseFaculty>> facultyBasicResponseBO = iVeBaseManageService.getFacultyAll();
//        List<VeBaseFaculty> veBaseFacultylist = facultyBasicResponseBO.getResult();
//      ========
        //获取全部专业
        QueryWrapper<VeBaseSpecialty>queryWrapper1=new QueryWrapper();
        List<VeBaseSpecialty> veBaseSpecialtylist = veBaseSpecialtyMapper.selectList(queryWrapper1);//2021.9.2
        //===========林彬辉
//        List<VeBaseSpecialty> veBaseSpecialtylist = new ArrayList<>();
//        for (VeBaseFaculty v : veBaseFacultylist){
//            BasicResponseBO<List<VeBaseSpecialty>> specialtyBasicResponseBO = iVeBaseManageService.getSpecialtyByFalId(v.getId());
//            veBaseSpecialtylist.addAll(specialtyBasicResponseBO.getResult());
//        }
//        =============
        //获取全部学制
        QueryWrapper<VeDictXuezi>veDictXueziWrapper=new QueryWrapper();
        List<VeDictXuezi> veDictXuezilist = veDictXueziMapper.selectList(veDictXueziWrapper);


        //获取省
        QueryWrapper<VeDictArea>provinceWrapper=new QueryWrapper();
        provinceWrapper.eq("pid",0);
        List<VeDictArea> provincelist = veDictAreaMapper.selectList(provinceWrapper);


        //获取省市县
        QueryWrapper<VeDictArea>cityWrapper=new QueryWrapper();
        cityWrapper.gt("pid",0);
        List<VeDictArea> citylist = veDictAreaMapper.selectList(cityWrapper);

        HashMap<String, String> map = new HashMap<>();
       //0失败，1成功
        String result="0";
        int count=0;


        for (ApplyMngImportVo vo : list) {
            count++;
            String msg= "文件第"+count+"行导入失败:";
            VeZsRegistration veZsRegistration=new VeZsRegistration();


            String yxdm=null;
            //院校检验\获取院校ID
            for (VeBaseFaculty veBaseFaculty : veBaseFacultylist) {

                if( veBaseFaculty.getYxmc().equals(vo.getYxmc())){
                    veZsRegistration.setYxmc(vo.getYxmc());
                    veZsRegistration.setFalId(veBaseFaculty.getId());
                    yxdm=veBaseFaculty.getYxdm();
                }

            }
            //院校信息有误
            if(veZsRegistration.getYxmc()==null || ("").equals(veZsRegistration.getYxmc())){
                msg+= "专业信息有误" ;
                map.put("msg",msg);
                map.put("result",result);
                return map;
            }

            //专业检验\专业ID获取
            for (VeBaseSpecialty veBaseSpecialty : veBaseSpecialtylist) {

                if( veBaseSpecialty.getZymc().equals(vo.getZymc())){
                    veZsRegistration.setZymc(vo.getZymc());
                    veZsRegistration.setSpecId(veBaseSpecialty.getId());

                    //专业关系是否匹配
                    if(!yxdm.equals(veBaseSpecialty.getYxdm())){
                        msg+= "专业同院校不匹配" ;
                        map.put("msg",msg);
                        map.put("result",result);
                        return map;

                    }
                }

            }
            //院校信息有误
            if(veZsRegistration.getZymc()==null || ("").equals(veZsRegistration.getZymc())){
                msg+= "专业信息有误" ;
                map.put("msg",msg);
                map.put("result",result);
                return map;
            }

            //学制校验以及获取id
            for (VeDictXuezi veDictXuezi : veDictXuezilist) {

                if( veDictXuezi.getXzmc().equals(vo.getXz())){

                    veZsRegistration.setXz(Long.parseLong(veDictXuezi.getId()+""));

                }

            }
            //学制有误
            if(veZsRegistration.getXz()==null ){
                msg+= "学制信息有误" ;
                map.put("msg",msg);
                map.put("result",result);
                return map;
            }

            //性别
            if(("男").equals(vo.getXbm()) || ("女").equals(vo.getXbm())){
                String sex=vo.getXbm().equals("男")?"1":"2";
                veZsRegistration.setXbm(sex);
            }else{
                msg+= "性别信息有误" ;
                map.put("msg",msg);
                map.put("result",result);
                return map;

            }
            //其他必填项填入(7+4)
            veZsRegistration.setBmh(vo.getSfzh());
            veZsRegistration.setXm(vo.getXm());
            veZsRegistration.setCsrq(vo.getCsrq());
            veZsRegistration.setSfzh(vo.getSfzh());
            veZsRegistration.setByxx(vo.getByxx());
            veZsRegistration.setJtdz(vo.getJtdz());
            veZsRegistration.setJtyb(vo.getJtyb());
            veZsRegistration.setJtlxdh(vo.getJtlxdh());

            //非必填项信息
            veZsRegistration.setCym(vo.getCym());
            //电话号码校验
            veZsRegistration.setXslxdh(vo.getXslxdh());
            veZsRegistration.setJstxh(vo.getJstxh());
            //电子信箱校验
            veZsRegistration.setDzxx(vo.getDzxx());

            veZsRegistration.setTc(vo.getTc());
            veZsRegistration.setRxny(vo.getRxny());
            veZsRegistration.setKsh(vo.getKsh());
            veZsRegistration.setZkzh(vo.getZkzh());

            //招生类型数据处理
            Integer zslx=null;
            if (!vo.getZslx().equals("") && vo.getZslx() != null) {

                if( (vo.getZslx().equals("1")||vo.getZslx().equals("统一招生")) ){
                    zslx=1;
                }else if( (vo.getZslx().equals("2")||vo.getZslx().equals("自主招生"))){
                    zslx=2;
                }else{
                    //招生类型输入有误
                    msg+= "招生类型输入有误" ;
                    map.put("msg",msg);
                    map.put("result",result);
                    return map;
                }

            }
            veZsRegistration.setZslx(zslx);
            //就读方式数据处理
            Integer jdfs=null;
            if (!vo.getZslx().equals("") && vo.getZslx() != null) {

                if(("1").equals(vo.getJdfs())||("住校").equals(vo.getJdfs()) ){
                    jdfs=1;
                }else if( ("1").equals(vo.getJdfs())||("走读").equals(vo.getJdfs())){
                    jdfs=2;
                }else{
                    //就读方式输入有误
                    msg+= "就读方式输入有误" ;
                    map.put("msg",msg);
                    map.put("result",result);
                    return map;
                }
            }
            veZsRegistration.setJdfs(jdfs);

            //分数处理
            Double kszf=0.0;
            if(!("").equals(vo.getKszf()) && null!=vo.getKszf()) {
                kszf = Double.parseDouble(vo.getKszf());
            }
            veZsRegistration.setKszf(kszf);

                //省检验\获取省ID
                for (VeDictArea veDictArea : provincelist) {
                    if( veDictArea.getName().equals(vo.getProvince())){
                        veZsRegistration.setProvince(vo.getProvince());
                        veZsRegistration.setProvinceId(veDictArea.getId());
                    }

                }

                //省信息有误
                if(veZsRegistration.getProvinceId()==null ){
                    msg+= "省份信息有误" ;
                    map.put("msg",msg);
                    map.put("result",result);
                    return map;
                }

                //市县检验\获取市县ID
                for (VeDictArea veDictArea : citylist) {
                    if( veDictArea.getName().equals(vo.getCity())){
                        veZsRegistration.setCity(vo.getCity());
                        veZsRegistration.setCityId(veDictArea.getId());
                    }else if( veDictArea.getName().equals(vo.getCounty())){
                        veZsRegistration.setCounty(vo.getCounty());
                        veZsRegistration.setCountyId(veDictArea.getId());
                    }

                }


            //市县信息有误
                if(veZsRegistration.getCityId()==null|| veZsRegistration.getCountyId()==null){
                    msg+= "市县区信息有误" ;
                    map.put("msg",msg);
                    map.put("result",result);
                    return map;
                }

            veZsRegistration.setJg(vo.getJg());

            //校验字典信息
            ArrayList<String>dictCodedList=new ArrayList<>();
            dictCodedList.add("nation");
            dictCodedList.add("healthy");
            dictCodedList.add("politic");
            dictCodedList.add("residence");
            HashMap<String,List<SysDictItem>>dictMap=new HashMap<>();

            for (String s : dictCodedList) {
                QueryWrapper<SysDict> wrapper = new QueryWrapper<>();
                wrapper.eq("dict_code",s);
                SysDict sysDict = sysDictMapper.selectOne(wrapper);

                QueryWrapper<SysDictItem> wrapper1 = new QueryWrapper<>();
                wrapper1.eq("dict_id",sysDict.getId());
                List<SysDictItem> sysDictItems = sysDictItemMapper.selectList(wrapper1);
                dictMap.put(s,sysDictItems);
            }
            //民族数据处理
            if (vo.getMzm() != null) {

                List<SysDictItem> sysDictItems=dictMap.get("nation");
                for (SysDictItem sysDictItem : sysDictItems) {
                    if(sysDictItem.getItemText().equals(vo.getMzm())){
                        veZsRegistration.setMzm(sysDictItem.getId());
                    }
                }
                if(veZsRegistration.getMzm()==null){
                    msg+= "民族信息有误" ;
                    map.put("msg",msg);
                    map.put("result",result);
                    return map;
                }
            }
            //健康状况数据处理
            if (vo.getJkzkm() != null) {

                List<SysDictItem> sysDictItems=dictMap.get("healthy");
                for (SysDictItem sysDictItem : sysDictItems) {
                    if(sysDictItem.getItemText().equals(vo.getJkzkm())){
                        veZsRegistration.setJkzkm(sysDictItem.getId());
                    }
                }
                if(veZsRegistration.getJkzkm()==null){
                    msg+= "健康状况信息有误" ;
                    map.put("msg",msg);
                    map.put("result",result);
                    return map;
                }
            }
            //户口类型数据处理
            if (vo.getHklbm() != null) {

                List<SysDictItem> sysDictItems=dictMap.get("residence");
                for (SysDictItem sysDictItem : sysDictItems) {
                    if(sysDictItem.getItemText().equals(vo.getHklbm())){
                        veZsRegistration.setHklbm(sysDictItem.getId());
                    }
                }
                if(veZsRegistration.getHklbm()==null){
                    msg+= "户口类型有误" ;
                    map.put("msg",msg);
                    map.put("result",result);
                    return map;
                }
            }
            //政治面貌数据处理
            if ( vo.getZzmmm() != null) {

                List<SysDictItem> sysDictItems=dictMap.get("politic");
                for (SysDictItem sysDictItem : sysDictItems) {
                    if(sysDictItem.getItemText().equals(vo.getZzmmm())){
                        veZsRegistration.setZzmmm(sysDictItem.getId());
                    }
                }
                if(veZsRegistration.getZzmmm()==null){
                    msg+= "政治面貌信息有误" ;
                    map.put("msg",msg);
                    map.put("result",result);
                    return map;
                }
            }
            //默认项填写
            veZsRegistration.setIsAdmit(0);
            veZsRegistration.setIsReport(0);
            veZsRegistration.setEnrollNum(0L);
            veZsRegistration.setIsDeleted(0);
            veZsRegistration.setIsCheck(0);
            veZsRegistration.setApplyTime(null);
            veZsRegistration.setCreateTime(new Date());
            veZsRegistration.setUpdateTime(new Date());
            veZsRegistration.setTerminalId(1L);
            veZsRegistration.setZsj(Long.parseLong(veDictQuarter.getId()+""));
            veZsRegistration.setRxnf(veDictQuarter.getYear());

//=======================林彬辉身份证存在则更新，不存在则添加
            //填入年级id
//            BasicResponseBO<List<VeBaseGrade>> listBasicResponseBO = iVeBaseManageService.getGradeAll();
//            List<VeBaseGrade> gradeList = listBasicResponseBO.getResult();
            List<VeBaseGrade> gradeList = veBaseGradeService.list();//2021.9.2
            for (VeBaseGrade grade : gradeList){
                if (veDictQuarter.getYear().equals(grade.getNjdm())){
                    veZsRegistration.setGradeId(grade.getId());
                    break;
                }
            }
            //身份证已存在则更新
            QueryWrapper<VeZsRegistration> wrapper = new QueryWrapper<>();
            wrapper.eq("SFZH",vo.getSfzh());

            VeZsRegistration one = veZsRegistrationService.getOne(wrapper);
            int insert = 0;
            if(one!=null){
                boolean res = true;
                if (one.getIsReport()==0){
                    res = veZsRegistrationService.update(veZsRegistration,wrapper);
                }else {//已报到学生无法更新
                    msg= "导入失败！第"+count+"行学生状态为已报到，无法进行更新" ;
                    map.put("msg",msg);
                    map.put("result",result);
                    return map;
                }

                if (res){
                    insert = 1;
                }else {
                    insert = 0;
                }
            }else {
                insert = veZsRegistrationMapper.insert(veZsRegistration);
            }
//            =====================
            //数据插入数据库
//          int insert = veZsRegistrationMapper.insert(veZsRegistration);
            if(insert<=0){
                msg= "导入失败！第"+count+"行数据导入数据库失败" ;
                map.put("msg",msg);
                map.put("result",result);
                return map;
            }

        }

        map.put("msg","导入成功！共插入"+count+"条数据");
        map.put("result","1");
        return map;
    }
}
