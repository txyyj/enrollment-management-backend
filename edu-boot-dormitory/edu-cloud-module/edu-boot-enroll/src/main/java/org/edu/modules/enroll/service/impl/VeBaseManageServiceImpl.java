package org.edu.modules.enroll.service.impl;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.edu.modules.enroll.config.HttpURLConnectionUtil;
import org.edu.modules.enroll.entity.*;
import org.edu.modules.enroll.service.IVeBaseManageService;
import org.edu.modules.enroll.service.VeBaseBanjiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;

/**公共数据接口类
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.service.impl
 * @date 2021/7/6 16:49
 */
@Service
public class VeBaseManageServiceImpl implements IVeBaseManageService {
    @Value("${common.host}")
    private String dirHost ;

    private String interfaceUserId = "?" +
            "interfaceUserId=09d5e1e7f9b049008eee645c783a1d67";
    private String id = "?" +
            "id=09d5e1e7f9b049008eee645c783a1d67";
    @Autowired
    ObjectMapper mapper;
    //根据用户id查教师
    @Override
    public BasicResponseBO<VeBaseTeacher> getTeaByUserId(String userId) {
        String url = "/common/veCommon/queryTeacherByUserId" + interfaceUserId + "&userId=" + userId ;
        BasicResponseBO<VeBaseTeacher> b = getUrl(url,VeBaseTeacher.class) ;
        return b;
    }

    //根据id查教师信息
    @Override
    public BasicResponseBO<VeBaseTeacher> getTeaById(Long id) {
        String url = "/common/veCommon/getTeacherById" + interfaceUserId + "&teacherId=" + id ;
        BasicResponseBO<VeBaseTeacher> b = getUrl(url,VeBaseTeacher.class) ;
        return b;
    }


    // 根据专业id获取专业信息
    @Override
    public BasicResponseBO<VeBaseSpecialty> getSpecialtyById(Long specId) {
        String url = "/common/veCommon/querySpecialtyById" + interfaceUserId + "&specialtyId=" + specId ;
        BasicResponseBO<VeBaseSpecialty> b = getUrl(url,VeBaseSpecialty.class) ;
        return b;
    }

    //根据专业部id查专业信息
    @Override
    public BasicResponseBO<List<VeBaseSpecialty>> getSpecialtyByFalId(Long falId) {
        String url = "/common/veCommon/querySpecialtyListByFalId" + interfaceUserId + "&falId=" + falId ;
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        BasicResponseBO<List<VeBaseSpecialty>> responseBO = new BasicResponseBO<>() ;
        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
                .setResult(b.getResult() == null ? null :
                        JSON.parseArray(JSON.toJSONString(b .getResult()),VeBaseSpecialty.class)) ;
        return responseBO;

    }
    //根据专业部id获取专业部信息
    @Override
    public BasicResponseBO<VeBaseFaculty> getFacultyById(Long id) {
        String url = "/common/veCommon/queryFacultyById" + interfaceUserId + "&facultyId=" + id ;
        BasicResponseBO<VeBaseFaculty> b = getUrl(url,VeBaseFaculty.class) ;
        return b;
    }

    //查询所有专业部
    @Override
    public BasicResponseBO<List<VeBaseFaculty>> getFacultyAll() {
        String url = "/common/veCommon/queryFacultyList" + interfaceUserId;
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        BasicResponseBO<List<VeBaseFaculty>> responseBO = new BasicResponseBO<>() ;
        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
                .setResult(b.getResult() == null ? null :
                        JSON.parseArray(JSON.toJSONString(b .getResult()),VeBaseFaculty.class)) ;
        return responseBO;
    }
    //查询所有班级
    @Override
    public BasicResponseBO<List<VeBaseBanji>> getBanjiAll() {
        String url = "/common/veCommon/queryBanJiList" + interfaceUserId;
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        BasicResponseBO<List<VeBaseBanji>> responseBO = new BasicResponseBO<>() ;
        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
                .setResult(b.getResult() == null ? null :
                        JSON.parseArray(JSON.toJSONString(b .getResult()),VeBaseBanji.class)) ;
        return responseBO;
    }

    //根据专业查班级
    @Override
    public BasicResponseBO<List<VeBaseBanji>> getBanjiBySpecId(Long specId) {
        String url = "/common/veCommon/queryBanJiListBySpecId" + interfaceUserId + "&specId=" + specId ;
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        BasicResponseBO<List<VeBaseBanji>> responseBO = new BasicResponseBO<>() ;
        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
                .setResult(b.getResult() == null ? null :
                        JSON.parseArray(JSON.toJSONString(b .getResult()),VeBaseBanji.class)) ;
        return responseBO;
    }

    //根据年级查班级
    @Override
    public BasicResponseBO<List<VeBaseBanji>> getBanjiByGradeId(Long gradeId) {
        String url = "/common/veCommon/queryBanJiListByGradeId" + interfaceUserId + "&gradeId=" + gradeId ;
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        BasicResponseBO<List<VeBaseBanji>> responseBO = new BasicResponseBO<>() ;
        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
                .setResult(b.getResult() == null ? null :
                        JSON.parseArray(JSON.toJSONString(b .getResult()),VeBaseBanji.class)) ;
        return responseBO;
    }

    @Override
    public BasicResponseBO<List<VeBaseGrade>> getGradeAll() {
        String url = "/common/veCommon/queryGradeList" + interfaceUserId;
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        BasicResponseBO<List<VeBaseGrade>> responseBO = new BasicResponseBO<>() ;
        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
                .setResult(b.getResult() == null ? null :
                        JSON.parseArray(JSON.toJSONString(b .getResult()),VeBaseGrade.class)) ;
        return responseBO;
    }

    //根据年级id查年级信息
    @Override
    public BasicResponseBO<VeBaseGrade> getGradeById(Long id) {
        String url = "/common/veCommon/queryGradeById" + interfaceUserId + "&gradeId=" + id ;
        BasicResponseBO<VeBaseGrade> b = getUrl(url,VeBaseGrade.class) ;
        return b;
    }

    //    根据班级id查班级信息
    @Override
    public BasicResponseBO<VeBaseBanji> getBanjiById(Long id) {
        String url = "/common/veCommon/queryBanJiById" + interfaceUserId + "&banJiId=" + id ;
        BasicResponseBO<VeBaseBanji> b = getUrl(url,VeBaseBanji.class) ;
//        BasicResponseBO<List<VeBaseBanji>> responseBO = new BasicResponseBO<>() ;
//        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
//                .setResult(b.getResult() == null ? null :
//                        JSON.parseArray(JSON.toJSONString(b .getResult()),VeBaseBanji.class)) ;
        return b;
    }

    @Override
    public BasicResponseBO<VeBaseStudent> getStudentById(Long id) {
        String url = "/common/veCommon/getStudentById" + interfaceUserId + "&studentId=" + id ;
        BasicResponseBO<VeBaseStudent> b = getUrl(url,VeBaseStudent.class) ;
        return b;
    }
    //新增学生
    @Override
    public BasicResponseBO<Object> saveStudent(VoStudentAndStudentInfo veBaseStudent) {
        String url = "/common/veCommon/addStudent" ;
        veBaseStudent.setInterfaceUserId(interfaceUserId);
        BasicResponseBO<Object> b = postUrl(url,Object.class, JSONArray.toJSONString(veBaseStudent)) ;
        return b;
    }

    //查所有学生
    @Override
    public BasicResponseBO<List<VeBaseStudent>> getStudentAll() {
        String url = "/common/veCommon/getStudentList" + interfaceUserId;
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        BasicResponseBO<List<VeBaseStudent>> responseBO = new BasicResponseBO<>() ;
        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
                .setResult(b.getResult() == null ? null :
                        JSON.parseArray(JSON.toJSONString(b .getResult()),VeBaseStudent.class)) ;
        return responseBO;
    }

    //获取所有教师
    @Override
    public BasicResponseBO<List<VeBaseTeacher>> getTeacher() {  //李少君
        String url = "/common/veCommon/getTeacherList" + interfaceUserId;
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        BasicResponseBO<List<VeBaseTeacher>> responseBO = new BasicResponseBO<>() ;
        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
                .setResult(b.getResult() == null ? null :
                        JSON.parseArray(JSON.toJSONString(b .getResult()),VeBaseTeacher.class)) ;
        return responseBO;
    }

    //获取校区信息
    @Override
    public BasicResponseBO<List> getCampusMessage(){
        String url = "common/veCommon/queryCampusList" + interfaceUserId;
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        return b;
    }
    //获取所有学期
    @Override
    public BasicResponseBO<List<VeBaseSemester>> getSemesterList() {
        String url = "/common/veCommon/querySemesterList" + interfaceUserId;
        BasicResponseBO<List> b = getUrl(url,List.class) ;
        BasicResponseBO<List<VeBaseSemester>> responseBO = new BasicResponseBO<>() ;
        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
                .setResult(b.getResult() == null ? null :
                        JSON.parseArray(JSON.toJSONString(b .getResult()),VeBaseSemester.class)) ;
        return responseBO;
    }

//
//    @Override
//    public BasicResponseBO<GetStudentPageListBo> getStudentPageList(Long bjid, Integer pageNo, Integer pageSize) {
//        String url = "/common/veCommon/getStudentPageList" + "?bjid=" +bjid + "&pageNo=" + pageNo + "&pageSize=" + pageSize ;
//        BasicResponseBO<GetStudentPageListBo> b1 = getUrl(url,GetStudentPageListBo.class) ;
//        BasicResponseBO<List> b = getUrl(url,List.class) ;
//        BasicResponseBO<List<GetStudentPageListBo>> responseBO = new BasicResponseBO<>() ;
//        responseBO.setSuccess(b.getSuccess()).setMessage(b.getMessage())
//                .setResult(b.getResult() == null ? null :
//                        JSON.parseArray(JSON.toJSONString(b .getResult()),GetStudentPageListBo.class)) ;
////        return responseBO;
//        return b1;
//    }


    /**
     * 调用公告数据接口
     */
    private <T> BasicResponseBO<T> getUrl(String url,Class<T> clazz) {
        try {
            url = dirHost+url;
            String result = HttpURLConnectionUtil.doGet(url) ;
            BasicResponseBO<T> re = mapper.readValue(result, new TypeReference<BasicResponseBO<T>>() {});
            T body = mapper.readValue(mapper.writeValueAsString(re.getResult()), clazz);
            re.setResult(body);
            return re;
        } catch (Exception e) {
            return new BasicResponseBO<T>().setSuccess(false).setMessage("操作失败");
        }
    }
    private <T> BasicResponseBO<T> postUrl(String url,Class<T> clazz,String param) {
        try {
            url = dirHost+url;
            String result = HttpURLConnectionUtil.doPost(url,param) ;
            BasicResponseBO<T> re = mapper.readValue(result, new TypeReference<BasicResponseBO<T>>() {});
            T body = mapper.readValue(mapper.writeValueAsString(re.getResult()), clazz);
            re.setResult(body);
            return re;
        } catch (Exception e) {
            return new BasicResponseBO<T>().setSuccess(false).setMessage("操作失败");
        }
    }


}
