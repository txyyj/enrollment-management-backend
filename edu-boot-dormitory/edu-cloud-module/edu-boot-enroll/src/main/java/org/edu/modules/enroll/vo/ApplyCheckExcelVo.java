package org.edu.modules.enroll.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Auther 李少君
 * @Date 2021-07-25 19:40
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ApplyCheckExcelVo implements Serializable {

    /** id */
    @ApiModelProperty(value = "id")
    private Long id;

    /** 姓名 */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private String xm;

    /** 性别码 */
    @Excel(name = "性别码", width = 15)
    @ApiModelProperty(value = "性别码")
    private String xbm;

    /** 身份证号 */
    @Excel(name = "身份证号", width = 15)
    @ApiModelProperty(value = "身份证号")
    private String sfzh;

    /** 毕业学校 */
    @Excel(name = "毕业学校", width = 15)
    @ApiModelProperty(value = "毕业学校")
    private String byxx;


    /** 准考证号 */
    @ApiModelProperty(value = "准考证号")
    private String zkzh;

    /** 考生号 */
    @Excel(name = "考生号", width = 15)
    @ApiModelProperty(value = "考试号")
    private String ksh;

    /** 考试总分 */
    @Excel(name = "考试总分", width = 15)
    @ApiModelProperty(value = "考试总分")
    private Double kszf;

    /** 报名专业 */
    @Excel(name = "报名专业", width = 15)
    @ApiModelProperty(value = "报名专业")
    private String zymc;

    /** 审核的标识 */
    @Excel(name = "审核状态", width = 15)
    @ApiModelProperty(value = "审核状态")
    private String isCheck;



}
