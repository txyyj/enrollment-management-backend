package org.edu.modules.enroll.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description:  报到信息管理导出Vo
 * @Author:  wcj
 * @Date:  2021-04-13
 * @Version:  V1.0
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ExportApplyMngVo {

    /** 1id */
    @ApiModelProperty(value = "id")
    private Long id;

    /** 2姓名 */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private String xm;

    /** 4身份证号 */
    @Excel(name = "身份证号", width = 30)
    @ApiModelProperty(value = "身份证号")
    private String sfzh;

    /** 所属专业部 */
    @Excel(name = "所属专业部", width = 15)
    @ApiModelProperty(value = "所属专业部")
    private String yxmc;

    /** 所属专业 */
    @Excel(name = "所属专业", width = 15)
    @ApiModelProperty(value = "所属专业")
    private String zymc;
//=============林彬辉
    /** 考试总分 */
    @Excel(name = "考试总分", width = 15)
    @ApiModelProperty(value = "考试总分")
    private String kszf;
//===============
    /** 5录取状态 */
    @Excel(name = "是否录取", width = 15)
    @ApiModelProperty(value = "录取状态0未录取1已录取")
    private String isAdmit;

    /** 3性别码 */
    @Excel(name = "性别", width = 15)
    @ApiModelProperty(value = "性别码")
    private String xbm;

    /** 9入学年份 */
    @Excel(name = "入学年份", width = 15)
    @ApiModelProperty(value = "入学年份")
    private String rxnf;

    /** 10招生季 */
    @Excel(name = "招生季", width = 15)
    @ApiModelProperty(value = "招生季")
    private String zsj;

    /** 6是否报道 */
//    @Excel(name = "是否报道", width = 15)
    @ApiModelProperty(value = "是否报道0否1是")
    private String isReport;

    /** 11学制ID */
    @Excel(name = "学制", width = 15)
    @ApiModelProperty(value = "学制ID")
    private String xz;




}
