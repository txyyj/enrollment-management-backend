package org.edu.modules.enroll.controller;

import java.util.Date;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.modules.enroll.entity.VeZsPtemplet;
import org.edu.modules.enroll.entity.VeZsRegistration;
import org.edu.modules.enroll.service.VeZsPtempletService;
import org.edu.modules.enroll.vo.VeZsPtempletVo;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.*;

/**
 * @Description:  通知书模板管理业务控制
 * @Author:  wcj
 * @Date:  2021-04-20
 * @Version:  V2.0
 *
 */
@Api(tags="通知书模板管理")
@RestController
@RequestMapping("enroll/TemplateMng")
@Slf4j
public class TemplateMngController {


    @Resource
    private VeZsPtempletService veZsPtempletService;

    @ApiOperation(value = "获取通知模板")
    @PostMapping(value = "/tempList")
    public Result<?> getApplyMsgMngList(
                                        @ApiParam("模板名称") @RequestParam("name") String name,
                                        @ApiParam("当前页") @RequestParam("currentPage") Integer currentPage,
                                        @ApiParam("条数") @RequestParam("pageSize") Integer pageSize) {

        //构造条件构造器
        System.out.println("name  "+name);
        QueryWrapper<VeZsPtemplet>wrapper=new QueryWrapper<>();
        if(name!=null && !("").equals(name)){
            wrapper.like("name",name);
        }
        //按条件搜索集合
        Page<VeZsPtemplet> page = veZsPtempletService.page(new Page<>(currentPage, pageSize), wrapper);
        //类型转换
        List<VeZsPtemplet> list = page.getRecords();
        //获取搜索页码
        Integer count=veZsPtempletService.list(wrapper).size();

        //返回前端搜索结果

        HashMap<String, Object> map = new HashMap<>();
        map.put("list", list);
        map.put("count", count);

        return Result.OK(map);
    }


    @ApiOperation(value="删除")
    @PostMapping(value = "/delModel")
    @Transactional(rollbackFor = Exception.class)
    public Result<?> deleteModel(@ApiParam("删除对象id") @RequestParam(value = "id") String id){


        UpdateWrapper<VeZsPtemplet> updateWrapper =new UpdateWrapper<>();
        updateWrapper.eq("id",id);
        updateWrapper.set("is_deleted",1);
        veZsPtempletService.update(updateWrapper);
        return Result.OK("删除成功！");

    }

    @ApiOperation(value="启用")
    @PostMapping(value = "/batchStart")
    @Transactional(rollbackFor = Exception.class)
    public Result<?> StartModel(@ApiParam("id") @RequestParam(value = "id") String id){

        System.out.println(id);

        UpdateWrapper<VeZsPtemplet> updateWrapper=new UpdateWrapper<>();
        updateWrapper.set("status",0);
        veZsPtempletService.update(updateWrapper);

        //启用
        UpdateWrapper<VeZsPtemplet> up=new UpdateWrapper<>();
        up.eq("id",id);
        up.set("status",1);
        boolean update = veZsPtempletService.update(up);
        if(update){
            return Result.OK("启用成功！");
        }
            return Result.error("启用失败！");

    }

    @ApiOperation(value="添加模板")
    @PostMapping(value = "/addTempModel")
    @Transactional(rollbackFor = Exception.class)
    public Result<?> addModel(@RequestBody Map<String,VeZsPtempletVo>map){
        VeZsPtempletVo vo = map.get("entityObj");
        //数据处理
        String code =vo.getCode().replaceAll("\"", "&quot;");

        //数据校验
        System.out.println(vo);
        //创建添加对象
        VeZsPtemplet veZsPtemplet=new VeZsPtemplet();

        veZsPtemplet.setName(vo.getName());
        veZsPtemplet.setWidth(vo.getWidth());
        veZsPtemplet.setHeight(vo.getHeight());
        veZsPtemplet.setType("enroll");
        veZsPtemplet.setFileId(0);
        veZsPtemplet.setCode(vo.getCode());
        veZsPtemplet.setCreateBy("");
        veZsPtemplet.setCreateTime(new Date());
        veZsPtemplet.setUpdateBy("");
        veZsPtemplet.setUpdateTime(new Date());
        veZsPtemplet.setTerminalId(1);
        veZsPtemplet.setIsDeleted(0);

        //仅一个模板可用
        if(vo.getStatus()==1){
            //先把其他设为0
            UpdateWrapper<VeZsPtemplet> updateWrapper=new UpdateWrapper<>();
            updateWrapper.set("status",0);
            boolean update = veZsPtempletService.update(updateWrapper);

            if(!update){
                return Result.error("修改其他模板状态失败");
            }
            veZsPtemplet.setStatus(1);
        }else{
            veZsPtemplet.setStatus(0);
        }

        veZsPtemplet.setPrintType(vo.getPrintType());

        boolean save = veZsPtempletService.save(veZsPtemplet);

        if(save){
              return Result.OK("添加成功！");
        }
        return Result.error("添加失败");

    }


    @ApiOperation(value="编辑模板")
    @PostMapping(value = "/editTempModel")
    @Transactional(rollbackFor = Exception.class)
    public Result<?> editModel(@RequestBody Map<String,VeZsPtempletVo>map){

        VeZsPtempletVo vo = map.get("entityObj");
        //如果他把唯一模板设为禁用？？
        //数据处理
        String code =vo.getCode().replaceAll("\"", "&quot;");
        System.out.println(code);
        //如果是设为启用
        if(vo.getStatus()==1){
            StartModel(vo.getId()+"");
        }
        //数据校验
        System.out.println(vo);
        //创建修改对象
        UpdateWrapper<VeZsPtemplet>up=new UpdateWrapper<>();
        up.eq("id",vo.getId());
        up.set("name",vo.getName());
        up.set("width",vo.getWidth());
        up.set("height",vo.getHeight());
        up.set("code",vo.getCode());
        up.set("Status",vo.getStatus());
        up.set("PrintType",vo.getPrintType());

        boolean update = veZsPtempletService.update(up);

        if(update){
            return Result.OK("模板编辑成功！");
        }
        return Result.error("模板编辑失败！");
    }

    @ApiOperation(value="获取模板详情")
    @PostMapping(value = "/infoModel")
    @Transactional(rollbackFor = Exception.class)
    public Result<?> Model(@ApiParam("id") @RequestParam(value = "id") String id){

        VeZsPtemplet veZsPtemplet = veZsPtempletService.getById(id);

        if(veZsPtemplet!=null){
            return Result.OK(veZsPtemplet);

        }
        return Result.error("获取模板失败！");



    }

}
