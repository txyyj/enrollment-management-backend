package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.VeBaseXhrule;

/**
 * 学号规则表Mapper接口
 * @author
 */
public interface VeBaseXhruleMapper extends BaseMapper<VeBaseXhrule> {
}
