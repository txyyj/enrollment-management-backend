package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description:  学号规则表
 * @Author:  yhx
 * @Date:  2021-04-15
 * @Version:  V1.0
 */
@Data
@TableName("ve_base_xhrule")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeBaseXhrule {

    /** id */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /** 规则名称 */
    @Excel(name = "规则名称", width = 15)
    @ApiModelProperty(value = "规则名称")
    @TableField(value = "name")
    private String name;

    /** 规则编码 */
    @Excel(name = "规则编码", width = 15)
    @ApiModelProperty(value = "规则编码")
    @TableField(value = "code")
    private String code;

    /** 位数 */
    @Excel(name = "位数", width = 15)
    @ApiModelProperty(value = "位数")
    @TableField(value = "digit")
    private Integer digit;

    /** 规则编码序号 */
    @Excel(name = "规则编码序号", width = 15)
    @ApiModelProperty(value = "规则编码序号从小到大")
    @TableField(value = "listSort")
    private Integer listSort;

    /** 是否启用 */
    @Excel(name = "是否启用", width = 15)
    @ApiModelProperty(value = "是否启用1是0否")
    @TableField(value = "status")
    private Integer status;

    /** 终端系统ID */
    @Excel(name = "终端系统ID", width = 15)
    @ApiModelProperty(value = "终端系统ID")
    @TableField(value = "terminalId")
    private Integer terminalId;

}
