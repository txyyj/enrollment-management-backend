package org.edu.modules.enroll.entity;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description:  学制表对象类接口
 * @Author:  wcj
 * @Date:  2021-04-13
 * @Version:  V1.0
 */

@Data
@TableName("ve_dict_xuezi")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDictXuezi {

    /**id*/
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "地区id")
    private int id;

    /** 学制名称 */
    @Excel(name = "学制名称", width = 15)
    @ApiModelProperty(value = "学制名称")
    @TableField(value = "XZMC")
    private String xzmc;
}
