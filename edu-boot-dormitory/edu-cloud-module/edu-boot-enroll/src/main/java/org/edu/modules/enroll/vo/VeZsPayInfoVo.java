package org.edu.modules.enroll.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Auther 李少君
 * @Date 2021-08-06 9:33
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsPayInfoVo implements Serializable {

    /** id */
    @ApiModelProperty(value = "id")
    private Long id;

    /** 学生姓名 */
    @ApiModelProperty(value = "学生姓名")
    @TableField(value = "xm")
    private String xm;

    /*身份证号*/
    @ApiModelProperty(value = "身份证号")
    @TableField(value = "sfzh")
    private String sfzh;

    /*专业部*/
    @ApiModelProperty(value = "专业部")
    @TableField(value = "yxmc")
    private String yxmc;

    /** 专业名称 */
    @ApiModelProperty(value = "专业名称")
    @TableField(value = "zymc")
    private String zymc;

    /** 行班级名称 */
    @ApiModelProperty(value = "班级名称")
    @TableField(value = "xzbmc")
    private String xzbmc;

    /*应缴费用*/
    @ApiModelProperty(value = "应缴费用")
    @TableField(value = "fees_payable")
    private String feesPayable;

    /*未缴费用*/
    @ApiModelProperty(value = "已缴费用")
    @TableField(value = "unpaid_expenses")
    private String unpaidExpenses;

    /*已缴费用*/
    @ApiModelProperty(value = "已缴费用")
    @TableField(value = "paid_expenses")
    private String paidExpenses;

    /*缴费凭证（图片）*/
    @ApiModelProperty(value = "缴费凭证")
    @TableField(value = "payment_voucher")
    private String paymentVoucher;

    /*审核状态*/
    @ApiModelProperty(value = "审核状态-0未审核 1未通过 2已通过")
    @TableField(value = "isCheck")
    private Integer isCheck;

    /*不通过理由*/
    @ApiModelProperty(value = "不通过理由")
    @TableField(value = "failure_reason")
    private String failureReason;

}
