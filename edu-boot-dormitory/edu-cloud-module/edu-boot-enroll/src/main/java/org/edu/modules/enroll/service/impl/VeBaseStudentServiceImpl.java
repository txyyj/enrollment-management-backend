package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeBaseStudent;
import org.edu.modules.enroll.mapper.VeBaseStudentMapper;
import org.edu.modules.enroll.service.VeBaseStudentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class VeBaseStudentServiceImpl extends ServiceImpl<VeBaseStudentMapper, VeBaseStudent> implements VeBaseStudentService {
    @Resource
    private VeBaseStudentMapper mapper;
    @Override
    public Integer saveBaseStudent(VeBaseStudent veBaseStudent) {
        Integer res = mapper.saveBaseStudent(veBaseStudent);

        return res;
    }
}
