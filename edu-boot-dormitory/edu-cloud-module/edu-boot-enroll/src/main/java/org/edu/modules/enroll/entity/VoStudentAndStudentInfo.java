package org.edu.modules.enroll.entity;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

@Data
@Accessors(chain = true)
public class VoStudentAndStudentInfo {
    /** id */
    @ApiModelProperty(value = "id")
    private Long id;

    /** 身份证号 */
    @Excel(name = "身份证号", width = 15)
    @ApiModelProperty(value = "身份证号")
    private String sfzh;

    /** 学号 */
    @Excel(name = "学号", width = 15)
    @ApiModelProperty(value = "学号")
    private String xh;

    /** 姓名 */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private String xm;

    /** 性别码 */
    @Excel(name = "性别码", width = 15)
    @ApiModelProperty(value = "性别码")
    private String xbm;

    /** 用户ID */
    @Excel(name = "用户ID", width = 15)
    @ApiModelProperty(value = "用户ID")
    private String userId;

    /** 民族码 */
    @Excel(name = "民族码", width = 15)
    @ApiModelProperty(value = "民族码")
    private String mzm;

    /** 报名号 */
    @Excel(name = "报名号", width = 15)
    @ApiModelProperty(value = "报名号，唯一")
    private String bmh;

    /** 就读方式 */
    @Excel(name = "就读方式", width = 15)
    @ApiModelProperty(value = "就读方式：1住校，2走读")
    private Integer jdfs;

    /** 当前状态码 */
    @Excel(name = "当前状态码", width = 15)
    @ApiModelProperty(value = "当前状态码'XS'=>'新生', 'ZX' => '在校', 'XX' => '休学', 'TX' => '退学', 'KC' => '开除', 'BY' => '毕业', 'YY' => '肄业', 'ZXX' => '转学', 'JY' => '结业'")
    private String xsdqztm;

    /** 入学年月 */
    @Excel(name = "入学年月", width = 15)
    @ApiModelProperty(value = "入学年月(时间戳)")
    private Long rxny;

    /** 学制ID */
    @Excel(name = "学制ID", width = 15)
    @ApiModelProperty(value = "学制ID")
    private Long xz;

    /** 院系ID */
    @Excel(name = "院系ID", width = 15)
    @ApiModelProperty(value = "院系ID")
    private Long falId;

    /** 专业ID */
    @Excel(name = "专业ID", width = 15)
    @ApiModelProperty(value = "专业ID")
    private Long specId;

    /** 班级ID */
    @Excel(name = "班级ID", width = 15)
    @ApiModelProperty(value = "班级ID")
    private Long bjId;

    /** 年级ID */
    @Excel(name = "年级ID", width = 15)
    @ApiModelProperty(value = "年级ID")
    private Long gradeId;

    /** 创建时间 */
    @Excel(name = "创建时间", width = 15)
    @ApiModelProperty(value = "创建时间")
    private Long createTime;

    /** 更新时间 */
    @Excel(name = "更新时间", width = 15)
    @ApiModelProperty(value = "更新时间")
    private Long updateTime;

    /** 户口所在省份 */
    @Excel(name = "户口所在省份", width = 15)
    @ApiModelProperty(value = "户口所在省份")
    private String province;

    /** 省份ID */
    @Excel(name = "省份ID", width = 15)
    @ApiModelProperty(value = "省份ID")
    private Long provinceId;

    /** 户口所在市 */
    @Excel(name = "户口所在市", width = 15)
    @ApiModelProperty(value = "户口所在市")
    private String city;

    /** 市ID */
    @Excel(name = "市ID", width = 15)
    @ApiModelProperty(value = "市ID")
    private Long cityId;

    /** 户口所在区 */
    @Excel(name = "户口所在区", width = 15)
    @ApiModelProperty(value = "户口所在区")
    private String county;

    /** 区ID */
    @Excel(name = "区ID", width = 15)
    @ApiModelProperty(value = "区ID")
    private Long countyId;

    /** 生源地省id */
    @Excel(name = "生源地省id", width = 15)
    @ApiModelProperty(value = "生源地省id")
    private Long shengId;

    /** 生源地市id */
    @Excel(name = "生源地市id", width = 15)
    @ApiModelProperty(value = "生源地市id")
    private Long shiId;

    /** 生源地区id */
    @Excel(name = "生源地区id", width = 15)
    @ApiModelProperty(value = "生源地区id")
    private Long quId;

    /** 是否是困难生 */
    @Excel(name = "是否是困难生", width = 15)
    @ApiModelProperty(value = "是否是困难生 0=否  1=是")
    private Integer sfkns;

    /** 终端ID */
    @Excel(name = "终端ID", width = 15)
    @ApiModelProperty(value = "终端ID")
    private Long terminalId;

    /** 准考证号 */
    @Excel(name = "准考证号", width = 15)
    @ApiModelProperty(value = "准考证号")
    private String zkzh;

    /** 考生号 */
    @Excel(name = "考生号", width = 15)
    @ApiModelProperty(value = "考生号")
    private String ksh;

    /** 更新状态 */
    @Excel(name = "更新状态", width = 15)
    @ApiModelProperty(value = "更新状态（0：未更新; 1：已更新）")
    private Integer updateStatus;

    @ApiModelProperty(value ="报名方式")
    private String bmfsm;
    @ApiModelProperty(value ="毕业学校")
    private String byxx;
    @ApiModelProperty(value ="出生日期(时间戳)")
    private Integer csrq;
    @ApiModelProperty(value ="出生日期")
    private String csrqName;
    @ApiModelProperty(value ="曾用名")
    private String cym;
    @ApiModelProperty(value ="电子信箱")
    private String dzxx;
    @ApiModelProperty(value ="户口类别码")
    private String hklbm;
    @ApiModelProperty(value ="接口用户id")
    private String interfaceUserId;
    @ApiModelProperty(value ="籍贯")
    private String jg;
    @ApiModelProperty(value ="健康状况码")
    private String jkzkm;
    @ApiModelProperty(value ="家庭地址")
    private String jtdz;
    @ApiModelProperty(value ="家庭联系电话")
    private String jtlxdh;
    @ApiModelProperty(value ="入学成绩")
    private Double rxcj;
    @ApiModelProperty(value ="入学年月")
    private String rxnyName;
    @ApiModelProperty(value ="是否低保0否1是")
    private Integer sfdb;
    @ApiModelProperty(value ="是否是流动")
    private String sfsldrk;
    @ApiModelProperty(value ="stuId")
    private Integer stuId;
    @ApiModelProperty(value ="特长")
    private String tc;
    @ApiModelProperty(value ="学生联系电话")
    private String xslxdh;
    @ApiModelProperty(value ="照片")
    private String zp;
    @ApiModelProperty(value ="政治面貌码")
    private String zzmmm;








}
