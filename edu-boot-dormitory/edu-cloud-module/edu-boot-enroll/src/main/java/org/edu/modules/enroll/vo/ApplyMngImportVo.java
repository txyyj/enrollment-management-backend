package org.edu.modules.enroll.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description:  报到信息管理导入Vo
 * @Author:  wcj
 * @Date:  2021-04-12
 * @Version:  V1.0
 */

@Data
@TableName("ve_zs_registration")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ApplyMngImportVo {

    /** 1id */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Long id;

    /** 2姓名 */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    @TableField(value = "XM")
    private String xm;

    /** 3出生日期 */
    @Excel(name = "出生日期", width = 15)
    @ApiModelProperty(value = "出生日期")
    @TableField(value = "CSRQ")
    private String csrq;

    /** 4性别码 */
    @Excel(name = "性别", width = 15)
    @ApiModelProperty(value = "性别")
    @TableField(value = "XBM")
    private String xbm;

    /** 5身份证号 */
    @Excel(name = "身份证号", width = 30)
    @ApiModelProperty(value = "身份证号")
    @TableField(value = "SFZH")
    private String sfzh;

    /** 6毕业学校 */
    @Excel(name = "毕业学校", width = 15)
    @ApiModelProperty(value = "毕业学校")
    @TableField(value = "BYXX")
    private String byxx;

    /** 7家庭地址 */
    @Excel(name = "家庭地址", width = 15)
    @ApiModelProperty(value = "家庭地址")
    @TableField(value = "JTDZ")
    private String jtdz;

    /** 8家庭联系电话 */
    @Excel(name = "家庭联系电话", width = 15)
    @ApiModelProperty(value = "家庭联系电话")
    @TableField(value = "JTLXDH")
    private String jtlxdh;

    /** 9家庭邮编 */
    @Excel(name = "家庭邮编", width = 15)
    @ApiModelProperty(value = "家庭邮编")
    @TableField(value = "JTYB")
    private String jtyb;

    /** 10院系名称 */
    @Excel(name = "院系名称", width = 15)
    @ApiModelProperty(value = "院系名称")
    @TableField(value = "YXMC")
    private String yxmc;

    /** 11专业名称 */
    @Excel(name = "专业名称", width = 15)
    @ApiModelProperty(value = "专业名称")
    @TableField(value = "ZYMC")
    private String zymc;

    /** 12学制ID */
    @Excel(name = "学制", width = 15)
    @ApiModelProperty(value = "学制")
    @TableField(value = "XZ")
    private String xz;

    /** 13层次码 */
//    @Excel(name = "层次", width = 15)
//    @ApiModelProperty(value = "层次")
//    @TableField(value = "CCM")
//    private String ccm;


    /** 14就读方式 */
    @Excel(name = "就读方式", width = 15)
    @ApiModelProperty(value = "就读方式")
    @TableField(value = "JDFS")
    private String jdfs;

    /** 15入学年月 */
    @Excel(name = "入学年月", width = 15)
    @ApiModelProperty(value = "入学年月")
    @TableField(value = "RXNY")
    private String rxny;


    /** 16民族码 */
    @Excel(name = "民族", width = 15)
    @ApiModelProperty(value = "民族")
    @TableField(value = "MZM")
    private String mzm;

    /** 17户口类别码 */
    @Excel(name = "户口类别", width = 15)
    @ApiModelProperty(value = "户口类别")
    @TableField(value = "HKLBM")
    private String hklbm;


    /** 18户口所在省份 */
    @Excel(name = "户口所在省份", width = 15)
    @ApiModelProperty(value = "户口所在省份")
    @TableField(value = "province")
    private String province;



    /** 19户口所在市 */
    @Excel(name = "户口所在市", width = 15)
    @ApiModelProperty(value = "户口所在市")
    @TableField(value = "city")
    private String city;


    /** 20户口所在区 */
    @Excel(name = "户口所在区", width = 15)
    @ApiModelProperty(value = "户口所在区")
    @TableField(value = "county")
    private String county;

    /** 21招生类型 */
    @Excel(name = "招生类型", width = 15)
    @ApiModelProperty(value = "招生类型")
    @TableField(value = "ZSLX")
    private String zslx;

    /** 22准考证号 */
    @Excel(name = "准考证号", width = 15)
    @ApiModelProperty(value = "准考证号")
    @TableField(value = "ZKZH")
    private String zkzh;

    /** 23考生号 */
    @Excel(name = "考生号", width = 15)
    @ApiModelProperty(value = "考生号")
    @TableField(value = "KSH")
    private String ksh;

    /** 24考试总分 */
    @Excel(name = "考试总分", width = 15)
    @ApiModelProperty(value = "考试总分")
    @TableField(value = "KSZF")
    private String kszf;

    /** 25学生联系电话 */
    @Excel(name = "学生联系电话", width = 15)
    @ApiModelProperty(value = "学生联系电话")
    @TableField(value = "XSLXDH")
    private String xslxdh;

    /** 26即时通讯号 */
    @Excel(name = "即时通讯号", width = 15)
    @ApiModelProperty(value = "即时通讯号")
    @TableField(value = "JSTXH")
    private String jstxh;

    /** 27电子信箱 */
    @Excel(name = "电子信箱", width = 15)
    @ApiModelProperty(value = "电子信箱")
    @TableField(value = "DZXX")
    private String dzxx;

    /** 28曾用名 */
    @Excel(name = "曾用名", width = 15)
    @ApiModelProperty(value = "曾用名")
    @TableField(value = "CYM")
    private String cym;

    /** 29政治面貌码 */
    @Excel(name = "政治面貌", width = 15)
    @ApiModelProperty(value = "政治面貌")
    @TableField(value = "ZZMMM")
    private String zzmmm;

    /** 30籍贯 */
    @Excel(name = "籍贯", width = 15)
    @ApiModelProperty(value = "籍贯")
    @TableField(value = "JG")
    private String jg;

    /** 31健康状况码 */
    @Excel(name = "健康状况", width = 15)
    @ApiModelProperty(value = "健康状况")
    @TableField(value = "JKZKM")
    private String jkzkm;

    /** 32特长 */
    @Excel(name = "特长", width = 15)
    @ApiModelProperty(value = "特长")
    @TableField(value = "TC")
    private String tc;



}
