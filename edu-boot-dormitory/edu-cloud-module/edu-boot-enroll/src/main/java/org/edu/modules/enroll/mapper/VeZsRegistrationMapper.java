package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeZsAdmissionInformation;
import org.edu.modules.enroll.entity.VeZsRegistration;
import org.edu.modules.enroll.vo.ApplyCheckVo;
import org.edu.modules.enroll.vo.ExportStuInfoVo;
import org.edu.modules.enroll.vo.VeZsRegistrationVo;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.List;

public interface VeZsRegistrationMapper extends BaseMapper<VeZsRegistration> {

    /**
     *获取报到学生集合
     * @param quarterId 招生季ID
     * @param facultyId 专业部ID
     * @param specialtyId 专业ID
     * @param keyword 关键词
     * @param condition 条件
     * @param currentPage 当前页码
     * @param pageSize 条数
     * @return 报到学生集合
     */
    List<VeZsRegistrationVo> getReportStuList(@Param("quarterId") Integer quarterId
            , @Param("facultyId") Long facultyId, @Param("specialtyId") Long specialtyId
            , @Param("keyword") String keyword, @Param("condition") String condition
            , @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);


    /**
     * 获取已报到新生集合
     * @param quarterId 招生季ID
     * @param facultyId 专业部ID
     * @param specialtyId 专业ID
     * @param keyword 关键词
     * @param condition 条件
     * @param currentPage 当前页码
     * @param pageSize 条数
     * @return 已报到新生集合
     */
    List<VeZsRegistrationVo> getReportedStuList(@Param("quarterId") Integer quarterId
            , @Param("facultyId") Long facultyId, @Param("specialtyId") Long specialtyId
            , @Param("keyword") String keyword, @Param("condition") String condition
            , @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);


    /**
     * 获取未分班的新生集合
     * @param gradeId 年级ID
     * @param facultyId 专业部ID
     * @param specialtyId 专业ID
     * @param keyword 关键词
     * @param condition 条件
     * @param currentPage 当前页码
     * @param pageSize 条数
     * @return 未分班的新生集合
     */
    List<VeZsRegistrationVo> getStuOfNoDivideClass(@Param("gradeId") Long gradeId
            , @Param("facultyId") Long facultyId, @Param("specialtyId") Long specialtyId
            , @Param("keyword") String keyword, @Param("condition") String condition
            , @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);

    /**
     * 获取已分班的新生集合（根据班级ID）
     * @param clazzId 班级ID
     * @param currentPage 当前页码
     * @param pageSize 条数
     * @return 已分班的新生集合
     */
    List<VeZsRegistrationVo> getStuOfDivideByClazzId(@Param("clazzId") Long clazzId
            , @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);


    /**
     * 获取新生信息集合
     * @param curYear 专当前年份
     * @param facultyId 专业部ID
     * @param specialtyId 专业ID
     * @param clazzId 班级ID
     * @param keyword 关键词
     * @param condition 条件
     * @param currentPage 当前页码
     * @param pageSize 条数
     * @return 新生信息集合
     */
//    List<VeZsRegistrationVo> getStuInfoList(@Param("curYear") String curYear, @Param("facultyId") Long facultyId
//            , @Param("specialtyId") Long specialtyId, @Param("clazzId") Long clazzId
//            , @Param("keyword") String keyword, @Param("condition") String condition
//            , @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);
    //添加isNum参数
    List<VeZsRegistrationVo> getStuInfoList(@Param("curYear") String curYear, @Param("isNum") Integer isNum,@Param("facultyId") Long facultyId
            , @Param("specialtyId") Long specialtyId, @Param("clazzId") Long clazzId
            , @Param("keyword") String keyword, @Param("condition") String condition
            , @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize);

    /**
     * 导出新生信息集合
     * @return 当前年份新生信息集合
     */
    List<ExportStuInfoVo> exportStuInfo(@Param("isNum") Integer isNum,@Param("facultyId") Long facultyId
            , @Param("specialtyId") Long specialtyId, @Param("clazzId") Long clazzId
            , @Param("keyword") String keyword, @Param("condition") String condition);


    /**
     * 获取打印的学生信息
     * @param id 学生ID
     * @return  打印的学生信息
     */
    VeZsRegistrationVo getPrintStuInfo(@Param("id")Long id);

    /**
     * 获取打印录取通知书的学生信息
     * @param quarterId 招生季ID
     * @param facultyId 专业部ID
     * @param specialtyId 专业ID
     * @param keyword 关键词
     * @param condition 条件
     * @param currentPage 当前页码
     * @param pageSize 条数
     * @param isPrint  是否打印
     * @return 打印录取通知书的学生信息
     */
    List<VeZsRegistrationVo> getAdmissionPrint(@Param("quarterId") Integer quarterId,
                                               @Param("facultyId") Long facultyId,
                                               @Param("specialtyId") Long specialtyId,
                                               @Param("keyword") String keyword,
                                               @Param("condition") String condition,
                                               @Param("currentPage") Integer currentPage,
                                               @Param("pageSize") Integer pageSize,
                                               @Param("isPrint") Integer isPrint);


    /**
     *获取网上报名信息集合
     * @param quarterId 招生季ID
     * @param facultyId 专业部ID
     * @param specialtyId 专业ID
     * @param keyword 关键词
     * @param condition 条件
     * @param currentPage 当前页码
     * @param pageSize 条数
     * @param isCheck 是否审核
     * @return 报名信息集合（未审核）
     */
    List<ApplyCheckVo> getApplyList(@Param("quarterId") Integer quarterId
            , @Param("facultyId") Long facultyId, @Param("specialtyId") Long specialtyId
            , @Param("keyword") String keyword, @Param("condition") String condition
            , @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize
            , @Param("isCheck") Integer isCheck);


    /**
     *获取报名信息管理名单集合
     * @param quarterId 招生季ID
     * @param facultyId 专业部ID
     * @param specialtyId 专业ID
     * @param keyword 关键词
     * @param condition 条件
     * @param currentPage 当前页码
     * @param pageSize 条数
     * @param isAdmit 是否录取
     * @return 报名信息集合（未审核）
     */
    List<VeZsRegistrationVo> getApplyMngList(@Param("quarterId") Integer quarterId
            , @Param("facultyId") Long facultyId, @Param("specialtyId") Long specialtyId
            , @Param("keyword") String keyword, @Param("condition") String condition
            , @Param("currentPage") Integer currentPage, @Param("pageSize") Integer pageSize
            , @Param("isAdmit") Integer isAdmit);


    /**
     * 获取报到统计人数
     * @param quarterId 招生季id
     * @param facultyId 院系id
     * @param clazzId 班级id
     * @param isReport 是否报到，1为已报到，0为未报到
     * @param time 报到时间
     * @param specId 专业id
     * @return 报到统计人数
     */
    Integer getReportStatisticsNum(@Param("quarterId") Integer quarterId, @Param("facultyId") Long facultyId,
                         @Param("clazzId") Long clazzId,@Param("isReport")Integer isReport,@Param("time")String time,
                                   @Param("specId") Long specId);


    ApplyCheckVo selectByXmKshSfzh(@Param("XM") String XM, @Param("KSH") String KSH, @Param("SFZH") String SFZH);
}
