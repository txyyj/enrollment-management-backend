package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeZsFenban;
import org.edu.modules.enroll.vo.VeZsFenbanExcelVo;
import org.edu.modules.enroll.vo.VeZsFenbanVo;

import java.util.HashMap;
import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-07-24 10:36
 */
public interface VeZsFenbanService extends IService<VeZsFenban> {
    //显示预分班表格
    HashMap<String, Object> getFenbanList(Integer gradeId, String enrollType, Integer falId, Integer specId, Integer statuId, Integer currentPage, Integer pageSize);
    //导出预分班表格
    List<VeZsFenbanVo> getFenbanImport(Integer gradeId, String enrollType, Integer falId, Integer specId, Integer statuId);
    //为学生指定班级
    boolean giveBanji(Integer id,Integer banjiId);
    //学生退班
    boolean exitBanji(Integer id);
    //(自动分班)查询计划人数分班表格
    List<VeZsFenbanVo> getZidongFenbanList(Integer gradeId,Integer specId,Integer falId,String enrollType);
    //计算总计划人数
    Integer jihuaNum(Integer gradeId,Integer specId,Integer falId,String enrollType);
    //计算分班人数
    Integer fenbanNum(Integer specId,Integer falId,Integer gradeId,String enrollType);
    //查询要分班的学生信息
    List<VeZsFenban> fenbanStu(Integer specId,String enrollType,Integer falId,Integer gradeId);
    //给学生分配班级
    boolean fenpeiBanji(Integer banjiId,Integer id);
    //导入excel表格
    String excelImport(List<VeZsFenbanExcelVo> list) throws Exception;
    //获取班级信息
    List<VeZsFenbanVo> getBanjiMessage(Integer gradeId, Integer specId);
    //显示自动分班后的学生表格
    HashMap<String, Object> zidongFenbanList(Integer gradeId, String enrollType, Integer falId, Integer specId,Integer currentPage, Integer pageSize);
    //获取登录学生的个人信息
    List<VeZsFenban> getStuMessage(String xm,String xh,String sfzh);
}
