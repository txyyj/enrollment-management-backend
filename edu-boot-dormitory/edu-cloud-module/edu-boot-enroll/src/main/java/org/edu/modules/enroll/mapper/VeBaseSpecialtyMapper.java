package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.VeBaseSpecialty;


/**
 * 专业表接口
 * @author 86158
 */
public interface VeBaseSpecialtyMapper extends BaseMapper<VeBaseSpecialty>  {
}
