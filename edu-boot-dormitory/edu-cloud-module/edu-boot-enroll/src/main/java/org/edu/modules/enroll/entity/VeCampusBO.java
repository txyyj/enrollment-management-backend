package org.edu.modules.enroll.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Auther 李少君
 * @Date 2021-07-07 11:26
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeCampusBO {

    @ApiModelProperty(value = "id")
    private Long id;


    private String xqdm;

    private String xqmc;

    private String xqdz;

    private String xqlxdh;

    private String xqczdh;

    private String fzrUserId;

    private String dzyj;

    private String xqyzbm;

    private String xqszdxzqhm;

    private Double xqmj;

    private Double xqjzmj;

    private Double xqjxkysbzz;

    private Double xqgdzczz;

    private Long terminalId;

    private Long fileId;

}
