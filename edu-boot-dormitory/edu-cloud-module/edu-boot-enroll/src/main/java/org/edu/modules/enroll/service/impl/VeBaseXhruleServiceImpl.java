package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeBaseXhrule;
import org.edu.modules.enroll.mapper.VeBaseXhruleMapper;
import org.edu.modules.enroll.service.VeBaseXhruleService;
import org.springframework.stereotype.Service;

@Service
public class VeBaseXhruleServiceImpl extends ServiceImpl<VeBaseXhruleMapper, VeBaseXhrule> implements VeBaseXhruleService {
}
