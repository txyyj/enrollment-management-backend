package org.edu.modules.enroll.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

import java.io.Serializable;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/7/24 10:28
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class MockAdmissionVo implements Serializable {
    @Excel(name = "考生号" ,width = 30)
    @ApiModelProperty(value = "考生号")
    private String KSH;

    @Excel(name = "姓名" ,width = 30)
    @ApiModelProperty(value = "姓名")
    private String XM;

    @Excel(name = "学年" ,width = 30)
    @ApiModelProperty(value = "学年")
    private int XN;

    @Excel(name = "学期" ,width = 30)
    @ApiModelProperty(value = "学期")
    private String XQ;

    @Excel(name = "身份证号" ,width = 30)
    @ApiModelProperty(value = "身份证号")
    private String SFZH;

    @Excel(name = "中学名称" ,width = 30)
    @ApiModelProperty(value = "中学名称")
    private String ZXMC;

    @Excel(name = "拟录取专业" ,width = 30)
    @ApiModelProperty(value = "拟录取专业")
    private String major;

    @Excel(name = "总分" ,width = 30)
    @ApiModelProperty(value = "总分")
    private Long ZF;

    @Excel(name = "科目一" ,width = 30)
    @ApiModelProperty(value = "科目一")
    private Long KM1;

    @Excel(name = "科目二" ,width = 30)
    @ApiModelProperty(value = "科目二")
    private Long KM2;

    @Excel(name = "科目三" ,width = 30)
    @ApiModelProperty(value = "科目三")
    private Long KM3;




}
