package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @Auther 李少君
 * @Date 2021-08-05 17:15
 */
@Data
@TableName("ve_zs_pay_info")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsPayInfo implements Serializable {
    /** id */
    @TableId(type = IdType.AUTO)
    @ApiModelProperty(value = "id")
    private Integer id;

    /*预分班学生id*/
    @ApiModelProperty(value = "预分班学生id")
    @TableField(value = "assign_class_id")
    private Integer assignClassId;

    /*应缴费用*/
    @ApiModelProperty(value = "应缴费用")
    @TableField(value = "fees_payable")
    private String feesPayable;

    /*未缴费用*/
    @ApiModelProperty(value = "未缴费用")
    @TableField(value = "unpaid_expenses")
    private String unpaidExpenses;

    /*已缴费用*/
    @ApiModelProperty(value = "已缴费用")
    @TableField(value = "paid_expenses")
    private String paidExpenses;

    /*审核状态*/
    @ApiModelProperty(value = "审核状态-0未审核 1未通过 2已通过")
    @TableField(value = "isCheck")
    private Integer isCheck;

    /*不通过理由*/
    @ApiModelProperty(value = "不通过理由")
    @TableField(value = "failure_reason")
    private String failureReason;

    /*缴费凭证（图片）*/
    @ApiModelProperty(value = "缴费凭证")
    @TableField(value = "payment_voucher")
    private String paymentVoucher;

    /*创建时间*/
    @ApiModelProperty(value = "创建时间")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField(value = "create_time")
    private Date createTime;

    /*修改时间*/
    @ApiModelProperty(value = "更新时间")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @TableField(value = "update_time")
    private Date updateTime;

}
