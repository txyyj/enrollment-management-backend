package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import io.swagger.annotations.ApiParam;
import org.apache.ibatis.annotations.Param;
import org.edu.common.api.vo.Result;
import org.edu.modules.enroll.entity.VeZsReportMsg;
import org.edu.modules.enroll.entity.VeZsStudentLoanEntity;
import org.edu.modules.enroll.vo.VeZsStudentLoanVo;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.List;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/8/5 9:40
 */
public interface VeZsStudentLoanService {
    HashMap<String, Object> showList(Integer state, Integer faculty, Integer major, Integer currentPage, Integer pageSize);

    Boolean addLoan(Integer id,String bank,String bankName,String bankAccount,Long loanAmount,String contractNo,String url);

    Result<?> selectById(Integer id);

    Boolean auditPass(Long[]  ids);

    Boolean auditFail(Long[]  ids,String failureReason);

    Boolean updateLoan(Integer id,String bank,String bankName,String bankAccount,Long loanAmount,String bankReceipt,String contractNo);

}
