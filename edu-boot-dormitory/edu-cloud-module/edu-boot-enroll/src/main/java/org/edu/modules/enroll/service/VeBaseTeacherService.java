package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.enroll.entity.VeBaseTeacher;
import org.springframework.stereotype.Service;


public interface VeBaseTeacherService extends IService<VeBaseTeacher> {
}
