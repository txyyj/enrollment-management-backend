package org.edu.modules.enroll.service;

import org.edu.modules.enroll.entity.*;

import java.util.List;

/**
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.service
 * @date 2021/7/6 16:45
 */
public interface IVeBaseManageService {
    //根据教师用户id查教师信息
    BasicResponseBO<VeBaseTeacher> getTeaByUserId(String userId);
    //根据教师id查教师信息
    BasicResponseBO<VeBaseTeacher> getTeaById(Long id);
    //根据专业id获取专业信息
    BasicResponseBO<VeBaseSpecialty> getSpecialtyById(Long specId);
    //根据专业部id查专业
    BasicResponseBO<List<VeBaseSpecialty>> getSpecialtyByFalId(Long falId);
    //根据专业部id查专业部信息
    BasicResponseBO<VeBaseFaculty> getFacultyById(Long id);
    //查询所有专业部信息
    BasicResponseBO<List<VeBaseFaculty>> getFacultyAll();
    //查询所有班级信息
    BasicResponseBO<List<VeBaseBanji>> getBanjiAll();
    //按专业id查询班级信息
    BasicResponseBO<List<VeBaseBanji>> getBanjiBySpecId(Long specId);
    //按年级id查询班级信息
    BasicResponseBO<List<VeBaseBanji>> getBanjiByGradeId(Long gradeId);
    //查询所有年级
    BasicResponseBO<List<VeBaseGrade>> getGradeAll();
    //根据年级id查年级信息
    BasicResponseBO<VeBaseGrade> getGradeById(Long id);
    //根据班级id查班级信息
    BasicResponseBO<VeBaseBanji> getBanjiById(Long id);
    //根据学生id查学生信息
    BasicResponseBO<VeBaseStudent> getStudentById(Long id);
    //增加学生信息
    BasicResponseBO<Object> saveStudent(VoStudentAndStudentInfo voStudentAndStudentInfo);
    //获取所有学生信息
    BasicResponseBO<List<VeBaseStudent>> getStudentAll();
//    //根据班级id分页查询学生信息
//    BasicResponseBO<GetStudentPageListBo> getStudentPageList(Long id, Integer pageNo, Integer pageSize);
    //获取所有教师信息
    BasicResponseBO<List<VeBaseTeacher>> getTeacher();//李少君

    //获取所有校区
    BasicResponseBO<List> getCampusMessage();//李少君
    
    //获取所有学期
    BasicResponseBO<List<VeBaseSemester>> getSemesterList();
}
