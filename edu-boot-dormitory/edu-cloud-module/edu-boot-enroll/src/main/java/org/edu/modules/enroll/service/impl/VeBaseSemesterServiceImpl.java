package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeBaseSemester;
import org.edu.modules.enroll.mapper.VeBaseSemesterMapper;
import org.edu.modules.enroll.service.VeBaseSemesterService;
import org.springframework.stereotype.Service;

@Service
public class VeBaseSemesterServiceImpl extends ServiceImpl<VeBaseSemesterMapper,VeBaseSemester> implements VeBaseSemesterService  {
}
