package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.VeDictArea;

/**户籍地区表接口
 * @author 86158
 */
public interface VeDictAreaMapper extends BaseMapper<VeDictArea> {
}
