package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.*;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.entity
 * @date 2021/7/19 13:41
 */
@Data
@TableName(value = "ve_dorm_student",schema = "edu_dorm")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormStudent {
    @ApiModelProperty(value = "id")
    @TableId(type = IdType.AUTO)
    private Long id;
    @TableField(value = "userId")
    @ApiModelProperty(value = "用户id")
    private Long userId;
    @TableField(value = "XH")
    @ApiModelProperty(value = "学号")
    private String xh;
    @TableField(value = "XM")
    @ApiModelProperty(value = "姓名")
    private String xm;
    @TableField(value = "XQH")
    @ApiModelProperty(value = "校区号")
    private Long xqh;
    @TableField(value = "FJBM")
    @ApiModelProperty(value = "房间编码")
    private Long fjbm;
    @TableField(value = "CWH")
    @ApiModelProperty(value = "床位号")
    private String cwh;

    @TableField(value = "LCH")
    @ApiModelProperty(value = "楼层号")
    private Long lch;
    @TableField(value = "SSLBM")
    @ApiModelProperty(value = "宿舍楼编号")
    private Long sslbm;
    @TableField(value = "RZSJ",fill = FieldFill.INSERT)
    @ApiModelProperty(value = "入住时间")
    private Long rzsj;
    @TableField(value = "createTime" ,fill = FieldFill.INSERT)
    @ApiModelProperty(value = "添加时间")
    private Long createTime;
    @TableField(value = "createUserId")
    @ApiModelProperty(value = "添加人员id")
    private Long createUserId;
    @TableField(value = "createUserName")
    @ApiModelProperty(value = "添加人员姓名")
    private String createUserName;
    @TableField(value = "terminalId")
    @ApiModelProperty(value = "系统id")
    private Long terminalId;


}
