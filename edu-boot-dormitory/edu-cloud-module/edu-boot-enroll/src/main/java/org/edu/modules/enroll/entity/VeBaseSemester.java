package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.entity
 * @date 2021/7/26 17:17
 */
@Data
@TableName(value = "ve_base_semester",schema = "edu_dev")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeBaseSemester implements Serializable {

    /** id */
    @ApiModelProperty(value = "id")
    @TableId(type = IdType.AUTO)
    private Integer id;

    @TableField(value = "XQM")
    @ApiModelProperty(value = "学期码")
    private String xqm;
    @TableField(value = "XQMC")
    @ApiModelProperty(value = "学期名称")
    private String xqmc;
    @TableField(value = "XN")
    @ApiModelProperty(value = "学年（度）")
    private String xn;
    @TableField(value = "XQKSRQ")
    @ApiModelProperty(value = "学期开始日期")
    private Integer xqksrq;
    @TableField(value = "XQJSRQ")
    @ApiModelProperty(value = "学期结束日期")
    private Integer xqjsrq;
    @TableField(value = "iscurrent")
    @ApiModelProperty(value = "是否当前学期(0是, 1否)")
    private Integer iscurrent;
    @TableField(value = "terminalid")
    @ApiModelProperty(value = "终端系统id")
    private Integer terminalid;



}
