package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.auth.In;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.*;
import org.edu.modules.enroll.vo.VeZsFenbanVo;

import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-07-24 10:39
 */
public interface VeZsFenbanMapper extends BaseMapper<VeZsFenban> {
    //获取学生预分班级列表
    List<VeZsFenbanVo> getFenbanList(@Param("gradeId") Integer gradeId,@Param("enrollType") String enrollType,@Param("falId") Integer falId,@Param("specId") Integer specId,@Param("statuId") Integer statuId,@Param("start") Integer start,@Param("pageSize")Integer pageSize);
    //导出学生预分班级列表
    List<VeZsFenbanVo> getFenbanImport(@Param("gradeId") Integer gradeId,@Param("enrollType") String enrollType,@Param("falId") Integer falId,@Param("specId") Integer specId,@Param("statuId") Integer statuId);
    //获取学生预分班级的人的数量
    Long fenbanCount(@Param("gradeId") Integer gradeId,@Param("enrollType") String enrollType,@Param("falId") Integer falId,@Param("specId") Integer specId,@Param("statuId") Integer statuId);
    //为学生指定班级
    boolean giveBanji(@Param("id") Integer id, @Param("banjiId") Integer banjiId);
    //学生退班
    boolean exitBanji(@Param("id") Integer id);
    //(自动分班)查询计划人数分班表格
    List<VeZsFenbanVo> getZidongFenbanList(@Param("gradeId") Integer gradeId,@Param("specId")Integer specId,@Param("falId")Integer falId,@Param("enrollType")String enrollType);
    //计算总计划人数
    Integer jihuaNum(@Param("gradeId") Integer gradeId,@Param("specId")Integer specId,@Param("falId")Integer falId,@Param("enrollType")String enrollType);
    //计算要分班的人数
    Integer fenbanNum(@Param("specId")Integer specId,@Param("falId")Integer falId,@Param("gradeId")Integer gradeId,@Param("enrollType")String enrollType);
    //查询要分班的学生信息
    List<VeZsFenban> fenbanStu(@Param("specId")Integer specId,@Param("enrollType")String enrollType,@Param("falId")Integer falId,@Param("gradeId")Integer gradeId);
    //给学生分配班级
    boolean fenpeiBanji(@Param("banjiId")Integer banjiId,@Param("id")Integer id);
    //获取所有专业信息
    List<VeBaseSpecialty> getZy();
    //获取班级信息
    List<VeZsFenbanVo> getBanjiMessage(@Param("gradeId") Integer gradeId,@Param("specId") Integer specId);
    //导入学生信息
    Boolean importFenBan(VeZsFenban zsFenban);

    //显示自动分班后的学生表格
    List<VeZsFenbanVo> zidongFenbanList(@Param("specId") Integer specId, @Param("enrollType") String enrollType, @Param("falId") Integer falId, @Param("gradeId") Integer gradeId, @Param("start") Integer start, @Param("pageSize") Integer pageSize);
    //计算自动分班后的学生人数
    Integer zidongFenbanNum(@Param("specId") Integer specId, @Param("enrollType") String enrollType, @Param("falId") Integer falId, @Param("gradeId") Integer gradeId, @Param("start") Integer start, @Param("pageSize") Integer pageSize);
    //查询所有民族
    List<SysDictItem> getMz();
    //获取学制
    List<VeDictXuezi> getXuezi();
    //获取所有省市区
    List<VeDictArea> getSheng(@Param("pid") Integer pid);
    //获取所有分班信息
    List<VeZsFenban> getFenbanMessage();
    //获取登录学生的个人信息
    List<VeZsFenban> getStuMessage(@Param("xm") String xm,@Param("ksh") String ksh,@Param("sfzh") String sfzh);

}
