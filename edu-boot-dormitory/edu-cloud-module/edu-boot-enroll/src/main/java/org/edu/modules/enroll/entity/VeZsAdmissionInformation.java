package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/7/27 20:07
 */
@Data
@TableName("ve_zs_admission_information")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeZsAdmissionInformation {
    @ApiModelProperty(value = "主键id")
    private Integer id;
    @ApiModelProperty(value = "姓名")
    private String XM;
    @ApiModelProperty(value = "性别（0：男；1：女；2：未知）")
    private Integer XB;
    @ApiModelProperty(value = "身份证号")
    private String SFZH;
    @ApiModelProperty(value = "准考证号")
    private String ZKZH;
    @ApiModelProperty(value = "报考学校")
    private String BKXX;
    @ApiModelProperty(value = "考场")
    private String KC;
    @ApiModelProperty(value = "座位号")
    private String ZWH;
    @ApiModelProperty(value = "考试地点")
    private String KSDD;
    @ApiModelProperty(value = "考试时间")
    private String KSSJ;
    @ApiModelProperty(value = "录取信息查询")
    private String LQXXCX;
    @ApiModelProperty(value = "考试分数查询")
    private String KSFSCX;
    @ApiModelProperty(value = "备注")
    private String remark;
    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    @ApiModelProperty(value = "更新时间")
    private String createBy;
    @ApiModelProperty(value = "修改用户ID")
    private Date updateTime;
    @ApiModelProperty(value = "update_by")
    private String updateBy;
    @ApiModelProperty(value = "逻辑删除 1(true)已删除; 0(false)未删除")
    private Integer isDeleted;
}
