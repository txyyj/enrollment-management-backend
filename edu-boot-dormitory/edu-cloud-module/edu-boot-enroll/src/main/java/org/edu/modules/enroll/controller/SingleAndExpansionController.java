package org.edu.modules.enroll.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.modules.enroll.entity.BasicResponseBO;
import org.edu.modules.enroll.entity.VeBaseSpecialty;
import org.edu.modules.enroll.entity.VeZsSingleEnrollExpansion;
import org.edu.modules.enroll.service.IVeBaseManageService;
import org.edu.modules.enroll.service.SingleAndExpansionService;

import org.edu.modules.enroll.service.VeBaseSpecialtyService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 单，扩招
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.controller
 * @date 2021/7/28 17:56
 */
@Api(tags="单，扩招报名审核")
@RestController
@RequestMapping("enroll/singleAndExpansion")
@Slf4j
public class SingleAndExpansionController {

    @Resource
    private SingleAndExpansionService singleAndExpansionService;
    @Resource
    private IVeBaseManageService iVeBaseManageService;
    @Resource
    private VeBaseSpecialtyService veBaseSpecialtyService;
    @ApiOperation(value="获取单扩招学生信息集合")
    @PostMapping(value = "/getSingleAndExpansionList")
    public Result<?> getSingleAndExpansionList(
                                     @ApiParam("专业ID") @RequestParam("specialtyId") Long specialtyId,
                                     @ApiParam("学生名字") @RequestParam("stuName") String  stuName,
                                     @ApiParam("身份证号") @RequestParam("sfzh") String sfzh,
                                     @ApiParam("当前页") @RequestParam("currentPage") Integer currentPage,
                                     @ApiParam("条数") @RequestParam("pageSize") Integer pageSize,
                                     @ApiParam("是否审核") @RequestParam("isCheck") Integer isCheck) {

        if (specialtyId==0){
            specialtyId = null;
        }
        if (stuName.equals("")){
            stuName = null;
        }
        if (sfzh.equals("")){
            sfzh = null;
        }

        if (isCheck == 2){
            isCheck = null;
        }
//        Integer current = (currentPage-1)*pageSize;
        QueryWrapper<VeZsSingleEnrollExpansion> wrapper = new QueryWrapper<>();
        if (specialtyId != null){
            wrapper.eq("specIdOne",specialtyId);
        }
        if (sfzh != null){
            wrapper.like("SFZH",sfzh);
        }
        if (stuName != null){
            wrapper.like("XM",stuName);
        }
        if (isCheck != null){
            wrapper.eq("isCheck",isCheck);
        }
        IPage<VeZsSingleEnrollExpansion> page = new Page<>(currentPage,pageSize);
        IPage<VeZsSingleEnrollExpansion> list = singleAndExpansionService.page(page,wrapper);
        for (VeZsSingleEnrollExpansion v : list.getRecords()){//获取专业名称
            VeBaseSpecialty specialty = veBaseSpecialtyService.getById(Long.valueOf(v.getSpecIdOne()));//2021.9.1 lbh
//            BasicResponseBO<VeBaseSpecialty> specialtyBasicResponseBO = iVeBaseManageService.getSpecialtyById(Long.valueOf(v.getSpecIdOne()));
//            VeBaseSpecialty specialty = specialtyBasicResponseBO.getResult();
            if (specialty != null){
                v.setZymcOne(specialty.getZymc());
            }else {
                v.setZymcOne("未查询到专业名称");
            }
            VeBaseSpecialty specialty2 = veBaseSpecialtyService.getById(Long.valueOf(v.getSpecIdTwo()));
//            BasicResponseBO<VeBaseSpecialty> specialtyBasicResponseBO2 = iVeBaseManageService.getSpecialtyById(Long.valueOf(v.getSpecIdTwo()));
//            VeBaseSpecialty specialty2 = specialtyBasicResponseBO2.getResult();
            if (specialty2 != null){
                v.setZymcTwo(specialty2.getZymc());
            }else {
                v.setZymcTwo("未查询到专业名称");
            }
        }
        return Result.OK(list);

    }

    @ApiOperation(value="批量审核")
    @PostMapping(value = "/batchCheck")
    public Result<?> batchReport(@ApiParam("拼接的ID字符串") @RequestParam(value = "ids") String ids){

        //切割字符串
        String[] split = ids.split(",");
        List<VeZsSingleEnrollExpansion> list = new ArrayList<>();
        for(String sp : split){
            Long id = Long.parseLong(sp);
            VeZsSingleEnrollExpansion veZsSingleEnrollExpansion = new VeZsSingleEnrollExpansion();
            veZsSingleEnrollExpansion.setId(id);
            veZsSingleEnrollExpansion.setIsCheck(1);
            list.add(veZsSingleEnrollExpansion);

        }
        boolean res = singleAndExpansionService.updateBatchById(list);
        if (res==true){
            return Result.OK("批量审核成功！");
        }else {
            return Result.OK("批量审核失败！");
        }


    }
    @ApiOperation(value="批量修改通过状态")
    @PostMapping(value = "/batchUpdateAdopt")
    public Result<?> batchReport(@ApiParam("拼接的ID字符串") @RequestParam(value = "ids") String ids,@ApiParam("是否通过") @RequestParam(value = "isAdopt") Integer isAdopt){

        //切割字符串
        String[] split = ids.split(",");
        List<VeZsSingleEnrollExpansion> list = new ArrayList<>();
        for(String sp : split){
            Long id = Long.parseLong(sp);
            VeZsSingleEnrollExpansion veZsSingleEnrollExpansion = new VeZsSingleEnrollExpansion();
            veZsSingleEnrollExpansion.setId(id);
            veZsSingleEnrollExpansion.setIsAdopt(isAdopt);
            list.add(veZsSingleEnrollExpansion);

        }
        boolean res = singleAndExpansionService.updateBatchById(list);
        String result = "";
        if (res==true){
            if (isAdopt == 1){
                result = "批量通过成功";
            }else {
                result = "批量不通过成功";
            }
        }else {
            if (isAdopt == 1){
                result = "批量通过失败";
            }else {
                result = "批量不通过失败";
            }
        }


        return Result.OK(result);

    }

    @ApiOperation(value="根据id获取单扩招学生信息")
    @PostMapping(value = "getSingleAndExpansionById")
    public Result<?> getSingleAndExpansionById( @ApiParam("单扩招表id") @RequestParam("id")Long id){

        VeZsSingleEnrollExpansion veZsSingleEnrollExpansion = singleAndExpansionService.getById(id);
        if (veZsSingleEnrollExpansion != null){
            return Result.OK(veZsSingleEnrollExpansion);
        }else {
            return Result.error("查无此条数据");
        }
    }
    }

