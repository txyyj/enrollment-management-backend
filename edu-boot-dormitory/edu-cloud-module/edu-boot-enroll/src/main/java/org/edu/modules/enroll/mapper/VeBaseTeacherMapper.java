package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.VeBaseTeacher;

public interface VeBaseTeacherMapper extends BaseMapper<VeBaseTeacher> {
}
