package org.edu.modules.enroll.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.jeecgframework.poi.excel.annotation.Excel;

/**
 * @Description:  报到信息管理导入Vo
 * @Author:  wcj
 * @Date:  2021-04-12
 * @Version:  V1.0
 */

@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ReplaceVo {

    /** 1id */
    @ApiModelProperty(value = "id")
    private Long id;

    /** 2姓名 */
    @Excel(name = "姓名", width = 15)
    @ApiModelProperty(value = "姓名")
    private String xm;

    /** 3出生日期 */
    @Excel(name = "出生日期", width = 15)
    @ApiModelProperty(value = "出生日期")
    private String csrq;

    /** 4性别码 */
    @Excel(name = "性别", width = 15)
    @ApiModelProperty(value = "性别码")
    private String xbm;

    /** 5身份证号 */
    @Excel(name = "身份证号", width = 30)
    @ApiModelProperty(value = "身份证号")
    private String sfzh;

    /** 6毕业学校 */
    @Excel(name = "毕业学校", width = 15)
    @ApiModelProperty(value = "毕业学校")
    private String byxx;

    /** 7家庭地址 */
    @Excel(name = "家庭地址", width = 15)
    @ApiModelProperty(value = "家庭地址(详细通讯地址)")
    private String jtdz;

    /** 8家庭联系电话 */
    @Excel(name = "家庭联系电话", width = 15)
    @ApiModelProperty(value = "家庭联系电话")
    private String jtlxdh;

    /** 9家庭邮编 */
    @Excel(name = "家庭邮编", width = 15)
    @ApiModelProperty(value = "家庭邮编")
    private String jtyb;

    /** 10院系名称 */
    @Excel(name = "院系名称", width = 15)
    @ApiModelProperty(value = "院系名称")
    private String yxmc;

    /** 11专业名称 */
    @Excel(name = "专业名称", width = 15)
    @ApiModelProperty(value = "专业名称")
    private String zymc;

    /** 12学制ID */
    @Excel(name = "学制", width = 15)
    @ApiModelProperty(value = "学制ID")
    private String xz;

    /** 13层次码 */
    @Excel(name = "层次", width = 15)
    @ApiModelProperty(value = "层次码")
    private String ccm;


    /** 14就读方式 */
    @Excel(name = "就读方式", width = 15)
    @ApiModelProperty(value = "就读方式：1住校，2走读")
    private Integer jdfs;

    /** 15入学年月 */
    @Excel(name = "入学年月", width = 15)
    @ApiModelProperty(value = "入学年月")
    private String rxny;


    /** 16民族码 */
    @Excel(name = "民族", width = 15)
    @ApiModelProperty(value = "民族码")
    private String mzm;

    /** 17户口类别码 */
    @Excel(name = "户口类别码", width = 15)
    @ApiModelProperty(value = "户口类别码")
    private String hklbm;


    /** 18户口所在省份 */
    @Excel(name = "户口所在省份", width = 15)
    @ApiModelProperty(value = "户口所在省份")
    private String province;



    /** 19户口所在市 */
    @Excel(name = "户口所在城市", width = 15)
    @ApiModelProperty(value = "户口所在市")
    private String city;


    /** 20户口所在区 */
    @Excel(name = "户口所在县区", width = 15)
    private String county;

    /** 21招生类型 */
    @Excel(name = "招生类型", width = 15)
    @ApiModelProperty(value = "招生类型，1统一招生，2自主招生")
    private Integer zslx;

    /** 22准考证号 */
    @Excel(name = "准考证号", width = 15)
    @ApiModelProperty(value = "准考证号")
    private String zkzh;

    /** 23考生号 */
    @Excel(name = "考生号", width = 15)
    @ApiModelProperty(value = "考生号")
    private String ksh;

    /** 24考试总分 */
    @Excel(name = "考试总分", width = 15)
    @ApiModelProperty(value = "考试总分")
    private String kszf;

    /** 25学生联系电话 */
    @Excel(name = "学生联系电话", width = 15)
    @ApiModelProperty(value = "学生联系电话")
    private String xslxdh;

    /** 26即时通讯号 */
    @Excel(name = "即时通讯号", width = 15)
    @ApiModelProperty(value = "即时通讯号")
    private String jstxh;

    /** 27电子信箱 */
    @Excel(name = "电子信箱", width = 15)
    @ApiModelProperty(value = "电子信箱")
    private String dzxx;

    /** 28曾用名 */
    @Excel(name = "曾用名", width = 15)
    @ApiModelProperty(value = "曾用名")
    private String cym;

    /** 29政治面貌码 */
    @Excel(name = "政治面貌", width = 15)
    @ApiModelProperty(value = "政治面貌码")
    private String zzmmm;

    /** 30籍贯 */
    @Excel(name = "籍贯", width = 15)
    @ApiModelProperty(value = "籍贯")
    private String jg;

    /** 31健康状况码 */
    @Excel(name = "健康状况", width = 15)
    @ApiModelProperty(value = "健康状况码")
    private String jkzkm;

    /** 32特长 */
    @Excel(name = "特长", width = 15)
    @ApiModelProperty(value = "特长")
    private String tc;

    /** 33省份ID */
    @Excel(name = "省份ID", width = 15)
    @ApiModelProperty(value = "省份ID")
    private String provinceId;

    /** 34市id */
    @Excel(name = "市ID", width = 15)
    @ApiModelProperty(value = "市ID")
    private String cityId;

    /** 35区ID */
    @Excel(name = "区ID", width = 15)
    @ApiModelProperty(value = "区ID")
    private String countyId;

    /** 36院校id */
    @Excel(name = "院校id ", width = 15)
    @ApiModelProperty(value = "院校id ")
    private String falId;

    /** 37专业id */
    @Excel(name = "报名专业ID", width = 15)
    @ApiModelProperty(value = "报名专业ID")
    private String specId;

    /** 是否低保 */
    @Excel(name = "是否低保", width = 15)
    @ApiModelProperty(value = "是否低保0否1是")
    private Integer sfdb;



}
