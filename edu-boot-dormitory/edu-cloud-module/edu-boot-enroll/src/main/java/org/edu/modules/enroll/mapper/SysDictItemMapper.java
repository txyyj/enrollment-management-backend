package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.SysDict;
import org.edu.modules.enroll.entity.SysDictItem;

/**
 * 字典选项表接口
 * @author wcj
 */
public interface SysDictItemMapper  extends BaseMapper<SysDictItem> {

}
