package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeZsFenban;
import org.edu.modules.enroll.mapper.VeFenSusheMapper;
import org.edu.modules.enroll.service.VeFenSusheService;
import org.edu.modules.enroll.vo.VeDormSushe;
import org.edu.modules.enroll.vo.VeZsArrangeSusheVo;
import org.edu.modules.enroll.vo.VeZsFenbanVo;
import org.edu.modules.enroll.vo.VeZsPlanBedVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * @Auther 李少君
 * @Date 2021-07-25 20:48
 */
@Service
//public class VeFenSusheServiceImpl implements VeFenSusheService {
public class VeFenSusheServiceImpl extends ServiceImpl<VeFenSusheMapper,VeZsFenban> implements VeFenSusheService {//林彬辉
    @Resource
    private VeFenSusheMapper veFenSusheMapper;

    //显示安排床位班级的表格
    @Override
    public HashMap<String, Object> getPlanBedList(Integer gradeId, Integer falId, Integer banjiId , Integer currentPage, Integer pageSize ) {
        Integer start = (currentPage - 1) * pageSize;
        List<VeZsPlanBedVo> list = veFenSusheMapper.getPlanBedList(gradeId, falId, banjiId, start, pageSize);
        Long count = veFenSusheMapper.PlanBedCount(gradeId, falId, banjiId);
        HashMap<String,Object> map = new HashMap<>();
        map.put("list",list);
        map.put("count",count);
        return map;
    }

    //显示选择安排入住的寝室表格
    @Override
    public HashMap<String, Object> getArrangeSusheList(Integer jzwmc, Integer lch, String fjbm,Integer sfwk,Integer xb, Integer currentPage, Integer pageSize) {
        Integer start = (currentPage - 1) * pageSize;
        List<VeZsArrangeSusheVo> list = veFenSusheMapper.getArrangeSusheList(jzwmc,lch,fjbm,sfwk,xb, start, pageSize);
        Long count = veFenSusheMapper.ArrangeSusheCount(jzwmc, lch, fjbm,sfwk,xb);
//        for (int i = 0;i<list.size();i++){
//            VeZsArrangeSusheVo v = new VeZsArrangeSusheVo();
//            v.setZcws(list.get(i).getKzrs());
//            v.setKcws(list.get(i).getKzrs()-list.get(i).getYzrs());
//            list.add(v);
//        }
        HashMap<String,Object> map = new HashMap<>();
        map.put("list",list);
        map.put("count",count);
        return map;
    }

    @Override
    public List<VeZsFenban> getStudent(Integer banjiId) {
        return veFenSusheMapper.getStudent(banjiId);
    }

    @Override
    public boolean fenpeiSushe(Long id,Integer susheId) {
        return veFenSusheMapper.fenpeiSushe(id,susheId);
    }

    @Override
    public VeDormSushe getSusheMessage(Integer susheId) {
        return veFenSusheMapper.getSusheMessage(susheId);
    }

}
