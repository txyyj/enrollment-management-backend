package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.enroll.entity.VeBaseGrade;

public interface VeBaseGradeService extends IService<VeBaseGrade> {
}
