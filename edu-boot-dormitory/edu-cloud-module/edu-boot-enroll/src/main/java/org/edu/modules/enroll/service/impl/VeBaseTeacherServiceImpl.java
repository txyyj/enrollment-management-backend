package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeBaseTeacher;
import org.edu.modules.enroll.mapper.VeBaseTeacherMapper;
import org.edu.modules.enroll.service.VeBaseTeacherService;
import org.springframework.stereotype.Service;

@Service
public class VeBaseTeacherServiceImpl extends ServiceImpl<VeBaseTeacherMapper,VeBaseTeacher> implements VeBaseTeacherService {
}
