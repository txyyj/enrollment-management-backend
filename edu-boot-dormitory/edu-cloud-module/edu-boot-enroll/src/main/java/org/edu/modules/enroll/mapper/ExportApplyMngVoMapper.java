package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeZsRegistration;
import org.edu.modules.enroll.vo.ExportApplyMngVo;

import java.util.List;


public interface ExportApplyMngVoMapper extends BaseMapper<VeZsRegistration> {
    /**
     *获取报名信息管理名单集合（导出）
     * @return 报名信息集合（未审核）
     */
    List<ExportApplyMngVo> getExportApplyMngList(@Param("quarterId") Integer quarterId
            , @Param("facultyId") Long facultyId, @Param("specialtyId") Long specialtyId
            , @Param("keyword") String keyword, @Param("condition") String condition
            , @Param("isAdmit") Integer isAdmit);
}
