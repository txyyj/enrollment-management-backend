package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeZsSingleEnrollExpansion;

import java.util.List;

/**
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.mapper
 * @date 2021/7/28 21:00
 */
public interface SingleAndExpansionMapper extends BaseMapper<VeZsSingleEnrollExpansion> {
    List<VeZsSingleEnrollExpansion> getSingleEnrollExpansion(@Param("specialtyId") Long specialtyId, @Param("stuName") String  stuName,
                                                            @Param("sfzh") String sfzh, @Param("currentPage") Integer currentPage,
                                                             @Param("pageSize") Integer pageSize, @Param("isCheck") Integer isCheck);

    Integer countSingleExpansion(@Param("specialtyId") Long specialtyId, @Param("stuName") String  stuName,
                                 @Param("sfzh") String sfzh,@Param("isCheck") Integer isCheck);
}
