package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeDormSusheEntity;
import org.edu.modules.enroll.mapper.VeDormSusheMapper;
import org.edu.modules.enroll.service.VeDormSusheService;
import org.springframework.stereotype.Service;

@Service
public class VeDormSusheServiceImpl extends ServiceImpl<VeDormSusheMapper, VeDormSusheEntity> implements VeDormSusheService {
}
