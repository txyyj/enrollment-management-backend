package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeBaseFaculty;
import org.edu.modules.enroll.mapper.VeBaseFacultyMapper;
import org.edu.modules.enroll.service.VeBaseFacultyService;
import org.springframework.stereotype.Service;

@Service
public class VeBaseFacultyServiceImpl extends ServiceImpl<VeBaseFacultyMapper, VeBaseFaculty> implements VeBaseFacultyService {

}
