package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.enroll.entity.SysDictItem;

/**
 * 字典选项表接口
 * @author wcj
 */
public interface SysDictItemService extends IService<SysDictItem> {
}
