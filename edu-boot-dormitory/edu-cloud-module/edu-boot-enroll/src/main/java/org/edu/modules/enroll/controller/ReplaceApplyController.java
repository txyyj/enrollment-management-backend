package org.edu.modules.enroll.controller;
import java.util.Date;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.edu.common.api.vo.Result;
import org.edu.modules.enroll.entity.BasicResponseBO;
import org.edu.modules.enroll.entity.VeBaseGrade;
import org.edu.modules.enroll.entity.VeDictQuarter;
import org.edu.modules.enroll.entity.VeZsRegistration;
import org.edu.modules.enroll.service.IVeBaseManageService;
import org.edu.modules.enroll.service.VeBaseGradeService;
import org.edu.modules.enroll.service.VeDictQuarterService;
import org.edu.modules.enroll.service.VeZsRegistrationService;
import org.edu.modules.enroll.vo.ReplaceVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description:  代报名业务控制
 * @Author:  wcj
 * @Date:  2021-04-14
 * @Version:  V1.0
 */
@Api(tags="代报名")
@RestController
@RequestMapping("enroll/ReplaceApply")
@Slf4j
public class ReplaceApplyController {

    @Resource
    private VeZsRegistrationService veZsRegistrationService;

    @Resource
    private VeDictQuarterService veDictQuarterService;

    @Resource
    private VeBaseGradeService veBaseGradeService;

    @Resource
    private IVeBaseManageService iVeBaseManageService;

    @ApiOperation(value="代报名添加")
    @PostMapping(value = "/addApply")
    public Result<?> addMng(ReplaceVo replaceVo){

        System.out.println(replaceVo);
        System.out.println("replaceVo.getSfzh()"+replaceVo.getSfzh());
        //数据校验
        //身份证号唯一
        QueryWrapper<VeZsRegistration> wrapper = new QueryWrapper<>();
        wrapper.eq("SFZH",replaceVo.getSfzh());
        VeZsRegistration veZsRegistration= veZsRegistrationService.getOne(wrapper);
        if(veZsRegistration!=null){
            return Result.error("添加失败，身份证号已存在");
        }else{
            //创建数据库对象
            VeZsRegistration ve=new VeZsRegistration();
            //获取当前招生季
            QueryWrapper<VeDictQuarter> wrapperA = new QueryWrapper<>();
            wrapperA.eq("isCur",1);
            VeDictQuarter veDictQuarter= veDictQuarterService.getOne(wrapperA);
            String rxny=veDictQuarter.getRxny()+"";
            String rxnf=veDictQuarter.getYear();
            //通过年份查找年级ID
            QueryWrapper<VeBaseGrade> wrapperGrade = new QueryWrapper<>();


            wrapperGrade.eq("NJMC",rxnf+"级");
            VeBaseGrade veBaseGrade = veBaseGradeService.getOne(wrapperGrade);//2021.9.1
//            -----------------------林彬辉
//            BasicResponseBO<List<VeBaseGrade>> listBasicResponseBO = iVeBaseManageService.getGradeAll();
//            List<VeBaseGrade> list = listBasicResponseBO.getResult();
//            VeBaseGrade veBaseGrade = null;
//            for (VeBaseGrade v : list){
//                if (v.getNjdm().equals(rxnf)){
//                    veBaseGrade = v;
//                    break;
//                }
//            }
//            -----------------------------
            if(veBaseGrade==null){
                return Result.error("添加失败，招生季年份信息有误");
            }
            long zsj=veDictQuarter.getId();
            ve.setBmh(replaceVo.getSfzh());
            ve.setXm(replaceVo.getXm());
            ve.setCym(replaceVo.getCym());
            ve.setSfzh(replaceVo.getSfzh());
            ve.setZslx(replaceVo.getZslx());
            ve.setKsh(replaceVo.getKsh());
            ve.setZkzh(replaceVo.getZkzh());
            ve.setXbm(replaceVo.getXbm());
            ve.setCsrq(replaceVo.getCsrq());
            ve.setJg(replaceVo.getJg());
            ve.setMzm(replaceVo.getMzm());
            ve.setGatqwm("");
            ve.setJkzkm(replaceVo.getJkzkm());
            ve.setZzmmm(replaceVo.getZzmmm());
            ve.setZsqdm("");
            ve.setXslxdh(replaceVo.getXslxdh());
            ve.setSfsldrk("");
            ve.setDzxx(replaceVo.getDzxx());
            ve.setJstxh(replaceVo.getJstxh());
            ve.setProvince(replaceVo.getProvince());
            ve.setIsAdmit(0);
            ve.setIsReport(0);
            ve.setEnrollNum(0L);
            ve.setCcm("");
            ve.setCity(replaceVo.getCity());
            ve.setZymc(replaceVo.getZymc());
            ve.setCounty(replaceVo.getCounty());
            ve.setJtdz(replaceVo.getJtdz());
            ve.setJtyb(replaceVo.getJtyb());
            ve.setJtlxdh(replaceVo.getJtlxdh());
            ve.setTc(replaceVo.getTc());
            ve.setBmfsm("");
            ve.setYhkh("");
            ve.setByxx(replaceVo.getByxx());
            ve.setCreateTime(new Date());
            ve.setUpdateTime(new Date());
            ve.setTerminalId(1L);
            ve.setSfdb(replaceVo.getSfdb());
            ve.setRxny(rxny);
            ve.setGradeId(veBaseGrade.getId());
            //无须插入
            ve.setClassId(null);
            ve.setRxnf(rxnf);
            ve.setJdfs(replaceVo.getJdfs());
            //无须插入
            ve.setApplyTime(null);
            ve.setZsj(zsj);
            ve.setHklbm(replaceVo.getHklbm());
            ve.setQrcodeUrl(null);
            ve.setYxmc(replaceVo.getYxmc());
            ve.setIsDeleted(0);
            //同网上报名的区别
            ve.setIsCheck(1);
            //数据类型转换
            Double kszf=0.0;
            if(replaceVo.getKszf()!="" && replaceVo.getKszf()!=null){
                kszf=Double.parseDouble(replaceVo.getKszf());
            }
            Long falid=Long.parseLong(replaceVo.getFalId());
            Long specId=Long.parseLong(replaceVo.getSpecId());
            Long xz=Long.parseLong(replaceVo.getXz());
            Long provinceId=Long.parseLong(replaceVo.getProvinceId());
            Long countyId=Long.parseLong(replaceVo.getCountyId());
            Long cityId=Long.parseLong(replaceVo.getCityId());
            ve.setKszf(kszf);
            ve.setFalId(falid);
            ve.setSpecId(specId);
            ve.setXz(xz);
            ve.setProvinceId(provinceId);
            ve.setCityId(cityId);
            ve.setCountyId(countyId);

            //添加数据
            boolean result= veZsRegistrationService.save(ve);
            if(result){
                return Result.OK("添加成功");
            }
            return Result.error("添加数据失败");
        }


    }

    @ApiOperation(value="编辑")
    @PostMapping(value = "/editApply")
    public Result<?> editMng(ReplaceVo replaceVo) {

        System.out.println(replaceVo);
        System.out.println("replaceVo.getSfzh()" + replaceVo.getSfzh());
        //数据校验
        //身份证号唯一
        //如果未改变
        //获取原来身份证信息
        QueryWrapper<VeZsRegistration> oldwrapper = new QueryWrapper<>();
        oldwrapper.eq("id", replaceVo.getId());
        VeZsRegistration old = veZsRegistrationService.getOne(oldwrapper);
        System.out.println(old.getSfzh()+"原身份证");
        System.out.println(replaceVo.getSfzh()+"修改后身份证");
        if(!old.getSfzh().equals(replaceVo.getSfzh())){
            //身份证改变了
            QueryWrapper<VeZsRegistration> wrapper = new QueryWrapper<>();
            wrapper.eq("SFZH", replaceVo.getSfzh());
            VeZsRegistration veZsRegistration = veZsRegistrationService.getOne(wrapper);
            if (veZsRegistration != null ) {
                return Result.error("编辑失败，身份证号已存在");
            }

        }


            //获取当前招生季
            QueryWrapper<VeDictQuarter> wrapperA = new QueryWrapper<>();
            wrapperA.eq("isCur", 1);
            VeDictQuarter veDictQuarter = veDictQuarterService.getOne(wrapperA);
            if(null==veDictQuarter){
                return Result.error("添加失败，当前招生季信息有误");
            }
            String rxny = veDictQuarter.getRxny();
            String rxnf = veDictQuarter.getYear();
            //通过年份查找年级ID
            QueryWrapper<VeBaseGrade> wrapperGrade = new QueryWrapper<>();
            wrapperGrade.eq("NJMC", rxnf + "级");
            VeBaseGrade veBaseGrade = veBaseGradeService.getOne(wrapperGrade);//2021.9.1
//        -------------------------林彬辉
//            BasicResponseBO<List<VeBaseGrade>> listBasicResponseBO = iVeBaseManageService.getGradeAll();
//            List<VeBaseGrade> list = listBasicResponseBO.getResult();
//            VeBaseGrade veBaseGrade = null;
//            for (VeBaseGrade v :list){
//                if (v.getNjmc().equals(rxnf+"级")){
//                    veBaseGrade = v;
//                }
//            }
//            ----------------------
            long zsj = veDictQuarter.getId();



        //设置修改的条件对象
        UpdateWrapper<VeZsRegistration> qw = new UpdateWrapper<>();

        qw.eq("id",replaceVo.getId());
        qw.set("BMH",replaceVo.getSfzh());
        qw.set("XM",replaceVo.getXm());
        qw.set("CYM",replaceVo.getCym());
        qw.set("SFZH",replaceVo.getSfzh());
        qw.set("ZSLX",replaceVo.getZslx());
        qw.set("KSH",replaceVo.getKsh());
        qw.set("ZKZH",replaceVo.getZkzh());
        qw.set("XBM",replaceVo.getXbm());
        qw.set("CSRQ",replaceVo.getCsrq());
        qw.set("JG",replaceVo.getJg());
        qw.set("MZM",replaceVo.getMzm());
        qw.set("JKZKM",replaceVo.getJkzkm());
        qw.set("ZZMMM",replaceVo.getZzmmm());
        qw.set("XSLXDH",replaceVo.getXslxdh());
        qw.set("DZXX",replaceVo.getDzxx());
        qw.set("JSTXH",replaceVo.getJstxh());
        qw.set("province",replaceVo.getProvince());
        qw.set("city",replaceVo.getCity());
        qw.set("ZYMC",replaceVo.getZymc());
        qw.set("county",replaceVo.getCounty());
        qw.set("JTDZ",replaceVo.getJtdz());
        qw.set("JTYB",replaceVo.getJtyb());
        qw.set("TC",replaceVo.getTc());
        qw.set("JTLXDH",replaceVo.getJtlxdh());
        qw.set("BYXX",replaceVo.getByxx());
        qw.set("update_time",new Date());
        qw.set("SFDB",replaceVo.getSfdb());
        qw.set("JDFS",replaceVo.getJdfs());
        qw.set("HKLBM",replaceVo.getHklbm());
        qw.set("YXMC",replaceVo.getYxmc());

            //数据类型转换
            Double kszf = 0.0;
            if (replaceVo.getKszf() != "" && replaceVo.getKszf() != null) {
                kszf = Double.parseDouble(replaceVo.getKszf());
            }
            Long falid = Long.parseLong(replaceVo.getFalId());
            Long specId = Long.parseLong(replaceVo.getSpecId());
            Long xz = Long.parseLong(replaceVo.getXz());
            Long provinceId = Long.parseLong(replaceVo.getProvinceId());
            Long countyId = Long.parseLong(replaceVo.getCountyId());
            Long cityId = Long.parseLong(replaceVo.getCityId());

        qw.set("KSZF",kszf);
        qw.set("falId",falid);
        qw.set("specId",specId);
        qw.set("provinceId",provinceId);
        qw.set("cityId",cityId);
        qw.set("countyId",countyId);

            //编辑数据
            boolean result = veZsRegistrationService.update(qw);
            if (result) {
                return Result.OK("编辑成功");
            }
            return Result.error("编辑数据失败");

    }


}


