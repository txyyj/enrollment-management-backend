package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeZsAdmissionInformation;
import org.edu.modules.enroll.vo.AdmissionTicketInfoVo;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/7/27 20:23
 */
public interface VeZsAdmissionInformationMapper extends BaseMapper<VeZsAdmissionInformationMapper> {
    AdmissionTicketInfoVo selectByXmKshSfzh(@Param("XM") String XM, @Param("KSH") String KSH, @Param("SFZH") String SFZH);
    VeZsAdmissionInformation applyMsg(@Param("XM") String XM, @Param("KSH") String KSH, @Param("SFZH") String SFZH);
}
