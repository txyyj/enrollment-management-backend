package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.enroll.entity.VeBaseBanji;

public interface VeBaseBanjiService extends IService<VeBaseBanji> {
    Integer updateNanAndNvrs(Long classId,Long nanrs,Long nvrs );
}
