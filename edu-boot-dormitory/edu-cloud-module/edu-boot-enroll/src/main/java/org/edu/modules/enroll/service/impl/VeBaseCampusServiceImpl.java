package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeBaseCampus;
import org.edu.modules.enroll.mapper.VeBaseCampusMappper;
import org.edu.modules.enroll.service.VeBaseCampusService;
import org.springframework.stereotype.Service;

@Service
public class VeBaseCampusServiceImpl extends ServiceImpl<VeBaseCampusMappper, VeBaseCampus> implements VeBaseCampusService {
}
