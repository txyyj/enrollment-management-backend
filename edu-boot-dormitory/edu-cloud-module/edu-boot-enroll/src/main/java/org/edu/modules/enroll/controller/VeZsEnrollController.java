package org.edu.modules.enroll.controller;

import com.alibaba.druid.sql.visitor.functions.If;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.models.auth.In;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.handler.IFillRuleHandler;
import org.edu.common.system.vo.LoginUser;
import org.edu.modules.enroll.entity.*;
import org.edu.modules.enroll.service.*;
import org.edu.modules.enroll.vo.*;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;
import javax.annotation.Resource;
import java.io.IOException;
import java.util.*;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/7/24 10:35
 */
@Api(tags = "录取管理")
@RestController
@RequestMapping("enroll/admission")
@Slf4j
public class VeZsEnrollController {
    @Resource
    private VeZsEnrollService veZsEnrollService;
    @Resource
    private IVeBaseManageService iVeBaseManageService;
    @Resource
    private VeBaseFacultyService veBaseFacultyService;
    @Resource
    private VeBaseSemesterService veBaseSemesterService;
    @Resource
    private VeBaseSpecialtyService veBaseSpecialtyService;

    @ApiOperation(value = "获取所有学期")
    @PostMapping("/getSemesterList")
    public Result<?> getSemesterList(){
        QueryWrapper<List<VeBaseSemester>> semesterWrapper = new QueryWrapper<>();//2021.9.1
        List<VeBaseSemester> list = veBaseSemesterService.list();
//        BasicResponseBO<List<VeBaseSemester>> listBasicResponseBO = iVeBaseManageService.getSemesterList();
//        List<VeBaseSemester> list = listBasicResponseBO.getResult();
        return Result.OK(list);
    }


    @AutoLog(value = "录取管理-成绩公示")
    @ApiOperation(value = "成绩公示")
    @PostMapping("/getScoreList")
    public Result<?> getScoreList(@ApiParam("准考证号") @RequestParam("ZKZH") String ZKZH,
                                  @ApiParam("姓名") @RequestParam("XM") String XM,
                                  @ApiParam("学年") @RequestParam("XN") Integer XN,
                                  @ApiParam("学期") @RequestParam("XQ") String XQ,
                                  @ApiParam("当前页数") @RequestParam("currentPage") Integer currentPage,
                                  @ApiParam("分页大小") @RequestParam("pageSize") Integer pageSize) {
        HashMap<String,Object> map = veZsEnrollService.showScoreList(ZKZH, XM, XN, XQ, currentPage, pageSize);
        return Result.OK(map);
    }

    @AutoLog(value = "录取管理-预录取")
    @ApiOperation(value = "预录取")
    @PostMapping("/getPreAdmissionList")
    public Result<?> getPreAdmissionList(@ApiParam("考生号") @RequestParam("KSH") String KSH,
                                         @ApiParam("姓名") @RequestParam("XM") String XM,
                                         @ApiParam("学年") @RequestParam("XN") Integer XN,
                                         @ApiParam("学期") @RequestParam("XQ") String XQ,
                                         @ApiParam("当前页数") @RequestParam("currentPage") Integer currentPage,
                                         @ApiParam("分页大小") @RequestParam("pageSize") Integer pageSize) {
        HashMap<String, Object> map = veZsEnrollService.showPreAdmissionList(KSH, XM, XN, XQ, currentPage, pageSize);
        return Result.OK(map);
    }

    @AutoLog(value = "录取管理-拟录取")
    @ApiOperation(value = "拟录取")
    @PostMapping("/getMockAdmissionList")
    public Result<?> getMockAdmissionList(@ApiParam("考生号") @RequestParam("KSH") String KSH,
                                          @ApiParam("姓名") @RequestParam("XM") String XM,
                                          @ApiParam("学年") @RequestParam("XN") Integer XN,
                                          @ApiParam("学期") @RequestParam("XQ") String XQ,
                                          @ApiParam("当前页数") @RequestParam("currentPage") Integer currentPage,
                                          @ApiParam("分页大小") @RequestParam("pageSize") Integer pageSize) {
        HashMap<String, Object> map = veZsEnrollService.showMockAdmissionList(KSH, XM, XN, XQ, currentPage, pageSize);
        return Result.OK(map);
    }

    @AutoLog(value = "录取管理-正式录取")
    @ApiOperation(value = "正式录取")
    @PostMapping("/getFormalAdmissionList")
    public Result<?> getFormalAdmissionList(@ApiParam("考生号") @RequestParam("KSH") String KSH,
                                            @ApiParam("姓名") @RequestParam("XM") String XM,
                                            @ApiParam("学年") @RequestParam("XN") Integer XN,
                                            @ApiParam("学期") @RequestParam("XQ") String XQ,
                                            @ApiParam("当前页数") @RequestParam("currentPage") Integer currentPage,
                                            @ApiParam("分页大小") @RequestParam("pageSize") Integer pageSize) {
        HashMap<String, Object> map = veZsEnrollService.showFormalAdmissionList(KSH, XM, XN, XQ, currentPage, pageSize);
        return Result.OK(map);
    }

    @AutoLog(value = "录取管理-导出成绩公示")
    @ApiOperation(value = "导出成绩公示")
    @GetMapping("/exportScore")
    public ModelAndView exportScore(@ApiParam("查询条件") @RequestParam("conditions") String conditions) {
        //        ------------林彬辉
        Map<String ,Object> conditionsMap = JSONObject.parseObject(conditions);
        QueryWrapper<VeZsEnroll> wrapper = new QueryWrapper<>();
        if (!conditionsMap.get("ZKZH").toString().equals("")&&conditionsMap.get("ZKZH")!=null){
            wrapper.like("ZKZH",conditionsMap.get("ZKZH"));
        }
        wrapper.eq("LQBS",2);
        if (!conditionsMap.get("XM").toString().equals("")&&conditionsMap.get("XM")!= null){
            wrapper.like("XM",conditionsMap.get("XM"));
        }

        if (!conditionsMap.get("XN").toString().equals("")&&conditionsMap.get("XN")!=null){
            Integer xn = Integer.parseInt(conditionsMap.get("XN").toString());
            wrapper.eq("XN",xn);
        }
        if (!conditionsMap.get("XQ").toString().equals("")&&conditionsMap.get("XQ")!=null){
            wrapper.eq("XQ",conditionsMap.get("XQ"));
        }

        List<VeZsEnroll> enrollList = veZsEnrollService.list(wrapper);
        List<ScoreAnnouncementVo> list = new ArrayList<>();

        for (VeZsEnroll v : enrollList){
            ScoreAnnouncementVo m = new ScoreAnnouncementVo();
            m.setKM1(v.getKM1());
            m.setKM2(v.getKM2());
            m.setKM3(v.getKM3());
//            BasicResponseBO<VeBaseFaculty> faculty = iVeBaseManageService.getFacultyById(Long.parseLong(v.getFaculty()+""));
            BasicResponseBO<VeBaseFaculty> faculty = new BasicResponseBO<>();
            VeBaseFaculty baseFaculty = veBaseFacultyService.getById(Long.parseLong(v.getFaculty()+""));
            faculty.setResult(baseFaculty);//2021.9.1
            if (faculty.getResult()!=null){
                m.setFaculty(faculty.getResult().getYxmc());
            }else {
                m.setFaculty("");
            }
//            BasicResponseBO<VeBaseSpecialty> specialty = iVeBaseManageService.getSpecialtyById(Long.parseLong(v.getFormalmajor()+""));

            BasicResponseBO<VeBaseSpecialty> specialty = new BasicResponseBO<>();
            VeBaseSpecialty baseSpecialty = veBaseSpecialtyService.getById(Long.parseLong(v.getFormalmajor()+""));
            specialty.setResult(baseSpecialty);//2021.9.1
            if (specialty.getResult()!=null){
                m.setFormalmajor(specialty.getResult().getZymc());
            }else {
                m.setFormalmajor("");
            }
//            m.setSFZH(v.getSFZH());
            m.setXM(v.getXM());
            m.setXN(v.getXN());
            m.setXQ(v.getXQ());
            m.setZF(v.getZF());
            m.setPM(v.getPM());
            m.setZKZH(v.getZKZH());
            list.add(m);
        }
//        --------------
        // 创建导出流
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        // 查数据获得加油表格的集合
//        List<ScoreAnnouncementVo> list = veZsEnrollService.exportScore();
        // 文件的文件名
        mv.addObject(NormalExcelConstants.FILE_NAME, "成绩公示");
        // 数据的类型
        mv.addObject(NormalExcelConstants.CLASS, ScoreAnnouncementVo.class);
        // 获取redis存的用户值
        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        // ExportParams  Export工具类   （参数1--》title:表格标题  参数2--》secomdTitle:第二行的标题，  参数3--》secomdTitle:最下面尾部的信息）
        ExportParams exportParams = new ExportParams("成绩公示", "导出人:" + user.getRealname(), "成绩公示");
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, list);
        return mv;
    }

    @AutoLog(value = "录取管理-导出预录取")
    @ApiOperation(value = "导出预录取")
    @GetMapping("/exportPreAdmission")
    public ModelAndView exportPreAdmission(@ApiParam("查询条件") @RequestParam("conditions") String conditions) {//林彬辉添加参数
        //        ------------林彬辉
        Map<String ,Object> conditionsMap = JSONObject.parseObject(conditions);
        QueryWrapper<VeZsEnroll> wrapper = new QueryWrapper<>();
        if (!conditionsMap.get("KSH").toString().equals("")&&conditionsMap.get("KSH")!=null){
            wrapper.like("KSH",conditionsMap.get("KSH"));
        }
        wrapper.eq("LQBS",1);
        if (!conditionsMap.get("XM").toString().equals("")&&conditionsMap.get("XM")!= null){
            wrapper.like("XM",conditionsMap.get("XM"));
        }

        if (!conditionsMap.get("XN").toString().equals("")&&conditionsMap.get("XN")!=null){
            Integer xn = Integer.parseInt(conditionsMap.get("XN").toString());
            wrapper.eq("XN",xn);
        }
        if (!conditionsMap.get("XQ").toString().equals("")&&conditionsMap.get("XQ")!=null){
            wrapper.eq("XQ",conditionsMap.get("XQ"));
        }

        List<VeZsEnroll> enrollList = veZsEnrollService.list(wrapper);
        List<MockAdmissionVo> list = new ArrayList<>();

        for (VeZsEnroll v : enrollList){
            MockAdmissionVo m = new MockAdmissionVo();
            m.setKM1(v.getKM1());
            m.setKM2(v.getKM2());
            m.setKM3(v.getKM3());
            m.setKSH(v.getKSH());
//            BasicResponseBO<VeBaseSpecialty> specialty = iVeBaseManageService.getSpecialtyById(Long.parseLong(v.getPrepmajor()+""));
            BasicResponseBO<VeBaseSpecialty> specialty = new BasicResponseBO<>();
            VeBaseSpecialty baseSpecialty = veBaseSpecialtyService.getById(Long.parseLong(v.getPrepmajor()+""));
            specialty.setResult(baseSpecialty);//2021.9.1
            if (specialty.getResult()!=null){
                m.setMajor(specialty.getResult().getZymc());
            }else {
                m.setMajor("");
            }
            m.setSFZH(v.getSFZH());
            m.setXM(v.getXM());
            m.setXN(v.getXN());
            m.setXQ(v.getXQ());
            m.setZF(v.getZF());
            list.add(m);
        }
        // 创建导出流
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        // 查数据获得加油表格的集合
//        List<PreAdmissionVo> list = veZsEnrollService.exportPreAdmission();
        // 文件的文件名
        mv.addObject(NormalExcelConstants.FILE_NAME, "预录取");
        // 数据的类型
        mv.addObject(NormalExcelConstants.CLASS, PreAdmissionVo.class);
        // 获取redis存的用户值
        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        // ExportParams  Export工具类   （参数1--》title:表格标题  参数2--》secomdTitle:第二行的标题，  参数3--》secomdTitle:最下面尾部的信息）
        ExportParams exportParams = new ExportParams("预录取", "导出人:" + user.getRealname(), "预录取");
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, list);
        return mv;
    }

    @AutoLog(value = "录取管理-导出拟录取")
    @ApiOperation(value = "导出拟录取")
    @GetMapping("/exportMockAdmission")
//    public ModelAndView exportMockAdmission() {
    public ModelAndView exportMockAdmission(@ApiParam("查询条件") @RequestParam("conditions") String conditions) {//林彬辉添加参数

//        ------------林彬辉
        Map<String ,Object> conditionsMap = JSONObject.parseObject(conditions);
        QueryWrapper<VeZsEnroll> wrapper = new QueryWrapper<>();
        if (!conditionsMap.get("KSH").toString().equals("")&&conditionsMap.get("KSH")!=null){
            wrapper.like("KSH",conditionsMap.get("KSH"));
        }
        wrapper.eq("LQBS",0);
        if (!conditionsMap.get("XM").toString().equals("")&&conditionsMap.get("XM")!= null){
            wrapper.like("XM",conditionsMap.get("XM"));
        }

        if (!conditionsMap.get("XN").toString().equals("")&&conditionsMap.get("XN")!=null){
            Integer xn = Integer.parseInt(conditionsMap.get("XN").toString());
            wrapper.eq("XN",xn);
        }
        if (!conditionsMap.get("XQ").toString().equals("")&&conditionsMap.get("XQ")!=null){
            wrapper.eq("XQ",conditionsMap.get("XQ"));
        }

        List<VeZsEnroll> enrollList = veZsEnrollService.list(wrapper);
        List<MockAdmissionVo> list = new ArrayList<>();

        for (VeZsEnroll v : enrollList){
            MockAdmissionVo m = new MockAdmissionVo();
            m.setKM1(v.getKM1());
            m.setKM2(v.getKM2());
            m.setKM3(v.getKM3());
            m.setKSH(v.getKSH());
//            BasicResponseBO<VeBaseSpecialty> specialty = iVeBaseManageService.getSpecialtyById(Long.parseLong(v.getMajor()+""));
            BasicResponseBO<VeBaseSpecialty> specialty = new BasicResponseBO<>();
            VeBaseSpecialty baseSpecialty = veBaseSpecialtyService.getById(Long.parseLong(v.getMajor()+""));
            specialty.setResult(baseSpecialty);//2021.9.1
            if (specialty.getResult()!=null){
                m.setMajor(specialty.getResult().getZymc());
            }else {
                m.setMajor("");
            }
            m.setSFZH(v.getSFZH());
            m.setXM(v.getXM());
            m.setXN(v.getXN());
            m.setXQ(v.getXQ());
            m.setZF(v.getZF());
            list.add(m);
        }
//        --------------
        // 查数据获得加油表格的集合
//        List<MockAdmissionVo> list = veZsEnrollService.exportMockAdmission();
        // 创建导出流
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        // 文件的文件名
        mv.addObject(NormalExcelConstants.FILE_NAME, "拟录取");
        // 数据的类型
        mv.addObject(NormalExcelConstants.CLASS, MockAdmissionVo.class);
        // 获取redis存的用户值
        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        // ExportParams  Export工具类   （参数1--》title:表格标题  参数2--》secomdTitle:第二行的标题，  参数3--》secomdTitle:最下面尾部的信息）
        ExportParams exportParams = new ExportParams("拟录取", "导出人:" + user.getRealname(), "拟录取");
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, list);
        return mv;
    }

    @AutoLog(value = "录取管理-导出正式录取")
    @ApiOperation(value = "导出正式录取")
    @GetMapping("/exportFormalAdmission")
    public ModelAndView exportFormalAdmission(@ApiParam("查询条件") @RequestParam("conditions") String conditions) {//林彬辉添加参数

//        ------------林彬辉
        Map<String ,Object> conditionsMap = JSONObject.parseObject(conditions);
        QueryWrapper<VeZsEnroll> wrapper = new QueryWrapper<>();
        if (!conditionsMap.get("KSH").toString().equals("")&&conditionsMap.get("KSH")!=null){
            wrapper.like("KSH",conditionsMap.get("KSH"));
        }
        wrapper.eq("LQBS",2);
        if (!conditionsMap.get("XM").toString().equals("")&&conditionsMap.get("XM")!= null){
            wrapper.like("XM",conditionsMap.get("XM"));
        }

        if (!conditionsMap.get("XN").toString().equals("")&&conditionsMap.get("XN")!=null){
            Integer xn = Integer.parseInt(conditionsMap.get("XN").toString());
            wrapper.eq("XN",xn);
        }
        if (!conditionsMap.get("XQ").toString().equals("")&&conditionsMap.get("XQ")!=null){
            wrapper.eq("XQ",conditionsMap.get("XQ"));
        }

        List<VeZsEnroll> enrollList = veZsEnrollService.list(wrapper);
        List<MockAdmissionVo> list = new ArrayList<>();

        for (VeZsEnroll v : enrollList){
            MockAdmissionVo m = new MockAdmissionVo();
            m.setKM1(v.getKM1());
            m.setKM2(v.getKM2());
            m.setKM3(v.getKM3());
            m.setKSH(v.getKSH());
//            BasicResponseBO<VeBaseSpecialty> specialty = iVeBaseManageService.getSpecialtyById(Long.parseLong(v.getFormalmajor()+""));
            BasicResponseBO<VeBaseSpecialty> specialty = new BasicResponseBO<>();
            VeBaseSpecialty baseSpecialty = veBaseSpecialtyService.getById(Long.parseLong(v.getFormalmajor()+""));
            specialty.setResult(baseSpecialty);//2021.9.1
            if (specialty.getResult()!=null){
                m.setMajor(specialty.getResult().getZymc());
            }else {
                m.setMajor("");
            }
            m.setSFZH(v.getSFZH());
            m.setXM(v.getXM());
            m.setXN(v.getXN());
            m.setXQ(v.getXQ());
            m.setZF(v.getZF());
            list.add(m);
        }
//        --------------
        // 创建导出流
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        // 查数据获得加油表格的集合
//        List<FormalAdmissionVo> list = veZsEnrollService.exportFormalAdmission();
        // 文件的文件名
        mv.addObject(NormalExcelConstants.FILE_NAME, "正式录取");
        // 数据的类型
        mv.addObject(NormalExcelConstants.CLASS, FormalAdmissionVo.class);
        // 获取redis存的用户值
        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        // ExportParams  Export工具类   （参数1--》title:表格标题  参数2--》secomdTitle:第二行的标题，  参数3--》secomdTitle:最下面尾部的信息）
        ExportParams exportParams = new ExportParams("正式录取", "导出人:" + user.getRealname(), "正式录取");
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, list);
        return mv;
    }

    @AutoLog(value = "录取管理-导入成绩公示")
    @ApiOperation(value = "导入成绩公示", notes = "录取管理—导入成绩公示")
    @PostMapping("/importScore")
    public Result<?> excelImport(MultipartFile file){
        ImportParams params = new ImportParams();
        params.setTitleRows(2);
        params.setHeadRows(1);
        params.setNeedSave(true);
        try {
            List<ScoreAnnouncementVo> list = ExcelImportUtil.importExcel(file.getInputStream(), ScoreAnnouncementVo.class, params);
            return veZsEnrollService.importScore(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error(e.getMessage());
        } finally {
            try {
                file.getInputStream().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @AutoLog(value = "录取管理-导入预录取")
    @ApiOperation(value = "导入预录取", notes = "录取管理—导入预录取")
    @PostMapping("/preAdmissionImport")
    public Result<?> preAdmissionImport(MultipartFile file) {
        ImportParams params = new ImportParams();
        params.setTitleRows(2);
        params.setHeadRows(1);
        params.setNeedSave(true);
        try {
            List<PreAdmissionVo> list = ExcelImportUtil.importExcel(file.getInputStream(), PreAdmissionVo.class, params);
            return veZsEnrollService.preAdmissionImport(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error(e.getMessage());
        } finally {
            try {
                file.getInputStream().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @AutoLog(value = "录取管理-导入正式录取")
    @ApiOperation(value = "导入正式录取", notes = "录取管理—导入正式录取")
    @PostMapping("/formalAdmissionImport")
    public Result<?> formalAdmissionImport(MultipartFile file) {
        ImportParams params = new ImportParams();
        params.setTitleRows(2);
        params.setHeadRows(1);
        params.setNeedSave(true);
        try {
            List<FormalAdmissionVo> list = ExcelImportUtil.importExcel(file.getInputStream(), FormalAdmissionVo.class, params);
            return veZsEnrollService.formalAdmissionImport(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("文件导入失败:" + e.getMessage());
        } finally {
            try {
                file.getInputStream().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @AutoLog(value = "录取管理-导入拟录取")
    @ApiOperation(value = "导入拟录取", notes = "录取管理—导入拟录取")
    @PostMapping("/mockAdmissionImport")
    public Result<?> mockAdmissionImport(MultipartFile file) {
        ImportParams params = new ImportParams();
        params.setTitleRows(2);
        params.setHeadRows(1);
        params.setNeedSave(true);
        try {
            List<MockAdmissionVo> list = ExcelImportUtil.importExcel(file.getInputStream(), MockAdmissionVo.class, params);
            return veZsEnrollService.mockAdmissionImport(list);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            return Result.error("文件导入失败:" + e.getMessage());
        } finally {
            try {
                file.getInputStream().close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @AutoLog(value = "录取管理-成绩公示模板")
    @ApiOperation(value = "成绩公示模板", notes = "录取管理—成绩公示模板")
    @GetMapping("/getScoreTemplate")
    public ModelAndView getScoreTemplate() {
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        List<ScoreAnnouncementVo> list = new ArrayList<>();
        // 文件的文件名
        mv.addObject(NormalExcelConstants.FILE_NAME, "成绩公示");
        // 数据的类型
        mv.addObject(NormalExcelConstants.CLASS, ScoreAnnouncementVo.class);
        // 获取redis存的用户值
        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        // ExportParams  Export工具类   （参数1--》title:表格标题  参数2--》secomdTitle:第二行的标题，  参数3--》secomdTitle:最下面尾部的信息）
        ExportParams exportParams = new ExportParams("成绩公示", "导出人:" + user.getRealname(), "成绩公示");
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, list);
        return mv;
    }

    @AutoLog(value = "录取管理-拟录取模板")
    @ApiOperation(value = "拟录取模板", notes = "录取管理—拟录取模板")
    @GetMapping("/getMockTemplate")
    public ModelAndView getMockTemplate() {
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        List<MockAdmissionVo> list = new ArrayList<>();
        // 文件的文件名
        mv.addObject(NormalExcelConstants.FILE_NAME, "拟录取");
        // 数据的类型
        mv.addObject(NormalExcelConstants.CLASS, MockAdmissionVo.class);
        // 获取redis存的用户值
        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        // ExportParams  Export工具类   （参数1--》title:表格标题  参数2--》secomdTitle:第二行的标题，  参数3--》secomdTitle:最下面尾部的信息）
        ExportParams exportParams = new ExportParams("拟录取", "导出人:" + user.getRealname(), "拟录取");
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, list);
        return mv;
    }

    @AutoLog(value = "录取管理-预录取模板")
    @ApiOperation(value = "预录取模板", notes = "录取管理—预录取模板")
    @GetMapping("/getPreTemplate")
    public ModelAndView getPreTemplate() {
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        List<PreAdmissionVo> list = new ArrayList<>();
        // 文件的文件名
        mv.addObject(NormalExcelConstants.FILE_NAME, "预录取");
        // 数据的类型
        mv.addObject(NormalExcelConstants.CLASS, PreAdmissionVo.class);
        // 获取redis存的用户值
        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        // ExportParams  Export工具类   （参数1--》title:表格标题  参数2--》secomdTitle:第二行的标题，  参数3--》secomdTitle:最下面尾部的信息）
        ExportParams exportParams = new ExportParams("预录取", "导出人:" + user.getRealname(), "预录取");
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, list);
        return mv;
    }

    @AutoLog(value = "录取管理-正式录取模板")
    @ApiOperation(value = "正式录取模板", notes = "录取管理—正式录取模板")
    @GetMapping("/getFormalTemplate")
    public ModelAndView getFormalTemplate() {
        ModelAndView mv = new ModelAndView(new JeecgEntityExcelView());
        List<FormalAdmissionVo> list = new ArrayList<>();
        // 文件的文件名
        mv.addObject(NormalExcelConstants.FILE_NAME, "正式录取");
        // 数据的类型
        mv.addObject(NormalExcelConstants.CLASS, FormalAdmissionVo.class);
        // 获取redis存的用户值
        LoginUser user = (LoginUser) SecurityUtils.getSubject().getPrincipal();
        // ExportParams  Export工具类   （参数1--》title:表格标题  参数2--》secomdTitle:第二行的标题，  参数3--》secomdTitle:最下面尾部的信息）
        ExportParams exportParams = new ExportParams("正式录取", "导出人:" + user.getRealname(), "正式录取");
        mv.addObject(NormalExcelConstants.PARAMS, exportParams);
        mv.addObject(NormalExcelConstants.DATA_LIST, list);
        return mv;
    }
}
