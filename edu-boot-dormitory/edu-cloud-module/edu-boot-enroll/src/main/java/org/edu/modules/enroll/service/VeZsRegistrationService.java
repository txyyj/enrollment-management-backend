package org.edu.modules.enroll.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeZsAdmissionInformation;
import org.edu.modules.enroll.entity.VeZsRegistration;
import org.edu.modules.enroll.vo.ApplyCheckExcelVo;
import org.edu.modules.enroll.vo.ApplyCheckVo;
import org.edu.modules.enroll.vo.ExportStuInfoVo;
import org.edu.modules.enroll.vo.VeZsRegistrationVo;

import java.util.List;

public interface VeZsRegistrationService extends IService<VeZsRegistration> {

    /**
     * 获取报到新生集合
     * @param quarterId 招生季ID
     * @param facultyId 专业部ID
     * @param specialtyId 专业ID
     * @param keyword 关键词
     * @param condition 条件
     * @param currentPage 当前页码
     * @param pageSize 条数
     * @return 报到新生集合
     */
    List<VeZsRegistrationVo> getReportStuList(Integer quarterId, Long facultyId, Long specialtyId,
                                              String keyword, String condition,
                                              Integer currentPage, Integer pageSize);

    /**
     *获取已报到新生集合
     * @param quarterId 招生季ID
     * @param facultyId 专业部ID
     * @param specialtyId 专业ID
     * @param keyword 关键词
     * @param condition 条件
     * @param currentPage 当前页码
     * @param pageSize 条数
     * @return 已报到新生集合
     */
    List<VeZsRegistrationVo> getReportedStuList(Integer quarterId, Long facultyId, Long specialtyId,
                                                String keyword, String condition,
                                                Integer currentPage, Integer pageSize);

    /**
     * 获取未分班的新生集合
     * @param gradeId 年级ID
     * @param facultyId 专业部ID
     * @param specialtyId 专业ID
     * @param keyword 关键词
     * @param condition 条件
     * @param currentPage 当前页码
     * @param pageSize 条数
     * @return 未分班的新生集合
     */
    List<VeZsRegistrationVo> getStuOfNoDivideClass(Long gradeId, Long facultyId, Long specialtyId
            , String keyword, String condition, Integer currentPage, Integer pageSize);

    /**
     * 获取已分班的新生集合（根据班级ID）
     * @param clazzId 班级ID
     * @param currentPage 当前页码
     * @param pageSize 条数
     * @return 已分班的新生集合
     */
    List<VeZsRegistrationVo> getStuOfDivideByClazzId(Long clazzId, Integer currentPage, Integer pageSize);

    /**
     * 获取新生信息集合
     * @param curYear 专当前年份
     * @param facultyId 专业部ID
     * @param specialtyId 专业ID
     * @param clazzId 班级ID
     * @param keyword 关键词
     * @param condition 条件
     * @param currentPage 当前页码
     * @param pageSize 条数
     * @return 新生信息集合
     */
//    List<VeZsRegistrationVo> getStuInfoList(String curYear,Long facultyId, Long specialtyId, Long clazzId
//            , String keyword, String condition, Integer currentPage, Integer pageSize);
//    -------林彬辉，添加参数isNum
    List<VeZsRegistrationVo> getStuInfoList(String curYear,Integer isNum, Long facultyId, Long specialtyId, Long clazzId
            , String keyword, String condition, Integer currentPage, Integer pageSize);

    /**
     * 导出新生信息集合
     * @return 当前年份新生信息集合
     */
    List<ExportStuInfoVo> exportStuInfo(Integer isNum, Long facultyId, Long specialtyId, Long clazzId
            , String keyword, String condition);

    /**
     * 获取打印的学生信息
     * @param id 学生ID
     * @return  打印的学生信息
     */
    VeZsRegistrationVo getPrintStuInfo(Long id);

    /**
     * 获取打印录取通知书的学生信息
     * @param quarterId 招生季ID
     * @param facultyId 专业部ID
     * @param specialtyId 专业ID
     * @param keyword 关键词
     * @param condition 条件
     * @param currentPage 当前页码
     * @param pageSize 条数
     * @param isPrint  是否打印
     * @return 打印录取通知书的学生信息
     */
    List<VeZsRegistrationVo> getAdmissionPrint(Integer quarterId, Long facultyId,
                                               Long specialtyId, String keyword,
                                               String condition, Integer currentPage,
                                               Integer pageSize, Integer isPrint);


    /**
     * 获取网上报名审核集合（未审核）
     * @param quarterId 招生季ID
     * @param facultyId 专业部ID
     *@param specialtyId 专业ID
     *@param keyword 关键词
     * @param condition 条件
     *@param currentPage 当前页码
     * @param pageSize 条数
     * @param isCheck 是否审核
     * @return 网上报名审核集合
     */
    List<ApplyCheckVo> getApplyMsgList(Integer quarterId, Long facultyId, Long specialtyId,
                                       String keyword, String condition,
                                       Integer currentPage, Integer pageSize, Integer isCheck);

    /**
     * 获取报名信息管理集合（已审核）
     * * @param quarterId 招生季ID
     * @param facultyId 专业部ID
     *@param specialtyId 专业ID
     *@param keyword 关键词
     * @param condition 条件
     *@param currentPage 当前页码
     * @param pageSize 条数
     * @param isAdmit 是否录取
     * @return 新生报到集合
     */
    List<VeZsRegistrationVo> getApplyMsgMngList(Integer quarterId, Long facultyId, Long specialtyId,
                                              String keyword, String condition,
                                              Integer currentPage, Integer pageSize, Integer isAdmit);

    /**
     * 获取报到统计人数（应到，实到）
     * @param quarterId 招生季id
     * @param facultyId 院系id
     * @param clazzId 班级id
     * @param isReport 是否报到1报到0未报到
     * @param time 报到时间
     * @return 人数
     */
    Integer getReportStatisticsNum(Integer quarterId, Long facultyId,
                                   Long clazzId, Integer isReport, String time,Long specId);

    //导入excel表格
    void excelImport(List<ApplyCheckExcelVo> list) throws Exception;


    //陈炜凡
    //根据姓名准考证身份证号查找
    ApplyCheckVo selectByXmKshSfzh(String XM, String KSH,String SFZH);

}
