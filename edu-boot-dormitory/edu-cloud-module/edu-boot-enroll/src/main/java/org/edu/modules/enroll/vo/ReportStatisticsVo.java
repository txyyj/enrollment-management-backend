package org.edu.modules.enroll.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * @author 林彬辉
 * @version V1.0
 * @Package org.edu.modules.enroll.vo
 * @date 2021/7/16 9:57
 */
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class ReportStatisticsVo implements Serializable {
    /** facultyName */
    @ApiModelProperty(value = "院系名")
    private String facultyName;

    /** facultyId */
    @ApiModelProperty(value = "院系id")
    private Long facultyId;

    /** className */
    @ApiModelProperty(value = "班级名")
    private String className;

    /** classId */
    @ApiModelProperty(value = "班级id")
    private Long classId;

    /** specialtyName */
    @ApiModelProperty(value = "专业名")
    private String specialtyName;

    /** specialtyId */
    @ApiModelProperty(value = "专业id")
    private Long specialtyId;

    /** theoryNum */
    @ApiModelProperty(value = "应到人数")
    private Integer theoryNum;//理论上人数

    /** realityNum */
    @ApiModelProperty(value = "实际人数")
    private Integer realityNum;

}
