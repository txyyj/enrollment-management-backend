package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.enroll.entity.VeBaseGrade;

/**
 * 年级的Mapper接口
 * @author yhx
 */
public interface VeBaseGradeMapper extends BaseMapper<VeBaseGrade> {
}
