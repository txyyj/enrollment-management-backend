package org.edu.modules.enroll.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

@Data
@TableName(value = "ve_dorm_sushe",schema = "edu_dorm")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
public class VeDormSusheEntity {

    @ApiModelProperty(value = "id")
    private Long id;

    @ApiModelProperty(value = "校区号")
    @TableField(value = "XQH")
    private Long xqh;
    @ApiModelProperty(value = "宿舍楼编号")
    @TableField(value = "JZWH")
    private Long jzwh;
    @ApiModelProperty(value = "楼层号")
    @TableField(value = "LCH")
    private Long lch;
    @ApiModelProperty(value = "房间编号")
    @TableField(value = "FJBM")
    private Long fjbm;
    @ApiModelProperty(value = "入住性别（0待定1男2女）")
    @TableField(value = "RZXB")
    private Long rzxb;
    @ApiModelProperty(value = "是否可用（0否1是）")
    @TableField(value = "SFKY")
    private Long sfky;
    @ApiModelProperty(value = "可住人数")
    @TableField(value = "KZRS")
    private Long kzrs;
    @ApiModelProperty(value = "已住人数")
    @TableField(value = "YZRS")
    private Long yzrs;
    @ApiModelProperty(value = "状态 0禁用 1正常 2维修 3报废 ")
    @TableField(value = "status")
    private Long status;








}
