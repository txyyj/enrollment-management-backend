package org.edu.modules.enroll.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.enroll.entity.VeDictYears;
import org.edu.modules.enroll.vo.VeDictYearsVo;

import java.util.List;

/**
 * 4.20
 *
 * @author zh
 */ //该类用于执行dao语句
public interface VeDictYearsMapper extends BaseMapper<VeDictYears> {
    List<VeDictYearsVo> displayYears();

    // 修改的时候校验相同的年份代码
    List<VeDictYearsVo> CheckYears(@Param("code") String code);

    Integer CheckYears1(@Param("code") String code, @Param("id") Integer id);

    //设置当前招生季
    Integer updateByIds();//全部变为0

    Integer updateById(@Param("code") String code, @Param("id") Integer id);

    Integer addYears(@Param("code") String code);//添加新的当前招生年份

    Integer newaddYears(@Param("code") String code, @Param("id") Integer id);//当选项编辑未否时

    Integer delYears(@Param("code") String code, @Param("id") Integer id);//删除

    //添加用的校验
    List<VeDictYearsVo> AddCheckYear(@Param("code") String code);

}
