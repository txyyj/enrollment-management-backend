package org.edu.modules.enroll.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.graphbuilder.curve.ValueVector;
import jdk.nashorn.internal.ir.annotations.Reference;
import org.edu.common.api.vo.Result;
import org.edu.modules.enroll.config.HttpURLConnectionUtil;
import org.edu.modules.enroll.entity.*;
import org.edu.modules.enroll.mapper.VeZsReportMessageMapper;
import org.edu.modules.enroll.mapper.VeZsStudentLoanMapper;
import org.edu.modules.enroll.service.VeBaseFacultyService;
import org.edu.modules.enroll.service.VeBaseSpecialtyService;
import org.edu.modules.enroll.service.VeZsStudentLoanService;
import org.edu.modules.enroll.vo.VeZsStudentLoanVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/8/5 9:56
 */
@Service
public class VeZsStudentLoanServiceImpl implements VeZsStudentLoanService {
    @Resource
    private VeZsStudentLoanMapper veZsStudentLoanMapper;
    @Resource
    private VeBaseFacultyService veBaseFacultyService;
    @Resource
    private VeBaseSpecialtyService veBaseSpecialtyService;

    @Value("${common.host}")
    private String dirHost;

    @Autowired
    ObjectMapper mapper;

    @Override
    public HashMap<String, Object> showList(Integer state, Integer facultyId, Integer majorId, Integer currentPage, Integer pageSize) {
        Integer start = (currentPage - 1) * pageSize;
        List<VeZsStudentLoanVo> veZsStudentLoanVos = new ArrayList<>();
        List<VeZsStudentLoanVo> list = veZsStudentLoanMapper.showList(state);
        for (int i = 0; i < list.size(); i++) {
            VeZsStudentLoanVo veZsStudentLoanVo = new VeZsStudentLoanVo();
            if (facultyId == null) {
//                String url = "/common/veCommon/queryFacultyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67&facultyId=" + list.get(i).getFalId();
//                BasicResponseBO<VeBaseFaculty> result = getUrl(url, VeBaseFaculty.class);
//                veZsStudentLoanVo.setFalId(result.getResult().getYxmc());
                VeBaseFaculty faculty = veBaseFacultyService.getById(list.get(i).getFalId());
                veZsStudentLoanVo.setFalId(faculty.getYxmc());//2021.9.2 lbh
            } else if (facultyId != null) {
                if (list.get(i).getFalId().equals(facultyId)) {
//                    String url = "/common/veCommon/queryFacultyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67&facultyId=" + list.get(i).getFalId();
//                    BasicResponseBO<VeBaseFaculty> result = getUrl(url, VeBaseFaculty.class);
//                    veZsStudentLoanVo.setFalId(result.getResult().getYxmc());
                    VeBaseFaculty faculty = veBaseFacultyService.getById(list.get(i).getFalId());
                    veZsStudentLoanVo.setFalId(faculty.getYxmc());//2021.9.2
                } else {
                    continue;
                }
            }
            if (majorId == null) {
//                String majorUrl = "/common/veCommon/querySpecialtyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67&specialtyId=" + list.get(i).getSpecId();
//                BasicResponseBO<VeBaseSpecialty> major = getUrl(majorUrl, VeBaseSpecialty.class);
//                veZsStudentLoanVo.setSpecId(major.getResult().getZymc());
                VeBaseSpecialty specialty = veBaseSpecialtyService.getById(list.get(i).getSpecId());
                veZsStudentLoanVo.setSpecId(specialty.getZymc());//2021.9.2
            } else if (majorId != null) {
                if (list.get(i).getSpecId().equals(majorId)) {
//                    String majorUrl = "/common/veCommon/querySpecialtyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67&specialtyId=" + list.get(i).getSpecId();
//                    BasicResponseBO<VeBaseSpecialty> major = getUrl(majorUrl, VeBaseSpecialty.class);
//                    veZsStudentLoanVo.setSpecId(major.getResult().getZymc());
                    VeBaseSpecialty specialty = veBaseSpecialtyService.getById(list.get(i).getSpecId());
                    veZsStudentLoanVo.setSpecId(specialty.getZymc());//2021.9.2
                } else {
                    continue;
                }
            }
            if (0==Integer.parseInt(list.get(i).getIsCheck())) {
                veZsStudentLoanVo.setIsCheck("待审核");
            } else if (2==Integer.parseInt(list.get(i).getIsCheck())) {
                veZsStudentLoanVo.setIsCheck("通过");
            } else if (1==Integer.parseInt(list.get(i).getIsCheck())) {
                veZsStudentLoanVo.setIsCheck("不通过");
            }
            veZsStudentLoanVo.setId(list.get(i).getId());
            veZsStudentLoanVo.setBank(list.get(i).getBank());
            veZsStudentLoanVo.setBankAccount(list.get(i).getBankAccount());
            veZsStudentLoanVo.setBankReceipt(list.get(i).getBankReceipt());
            veZsStudentLoanVo.setBankName(list.get(i).getBankName());
            veZsStudentLoanVo.setFailureReason(list.get(i).getFailureReason());
            veZsStudentLoanVo.setGraduationTime(list.get(i).getGraduationTime());
            veZsStudentLoanVo.setSfzh(list.get(i).getSfzh());
            veZsStudentLoanVo.setXm(list.get(i).getXm());
            veZsStudentLoanVo.setXh(list.get(i).getXh());
            veZsStudentLoanVo.setLoanAmount(list.get(i).getLoanAmount());
            veZsStudentLoanVo.setContractNo(list.get(i).getContractNo());
            veZsStudentLoanVos.add(veZsStudentLoanVo);
        }
        List<VeZsStudentLoanVo> loanList = new ArrayList<>();
        for (int i = start; i < start + pageSize; i++) {
            if (i == veZsStudentLoanVos.size()) {
                break;
            }
            loanList.add(veZsStudentLoanVos.get(i));
        }
        HashMap<String, Object> map = new HashMap<>();
        map.put("list", loanList);
        map.put("count", veZsStudentLoanVos.size());
        return map;
    }

    @Override
    public Boolean addLoan(Integer id, String bank, String bankName, String bankAccount, Long loanAmount, String contractNo,String url) {
        return veZsStudentLoanMapper.addLoan(id, bank, bankName, bankAccount, loanAmount, contractNo,url);
    }

    @Override
    public Result<?> selectById(Integer id) {
        HashMap<String,Object> map = new HashMap<>();
        VeZsStudentLoanVo veZsStudentLoanVo = veZsStudentLoanMapper.selectById(id);
        if (veZsStudentLoanVo == null){
            return Result.ok("未查询到贷款信息");
        }
//        String url = "/common/veCommon/queryFacultyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67&facultyId=" + veZsStudentLoanVo.getFalId();
//        BasicResponseBO<VeBaseFaculty> result = getUrl(url, VeBaseFaculty.class);
//        veZsStudentLoanVo.setFalId(result.getResult().getYxmc());
//        String majorUrl = "/common/veCommon/querySpecialtyById?interfaceUserId=09d5e1e7f9b049008eee645c783a1d67&specialtyId=" + veZsStudentLoanVo.getSpecId();
//        BasicResponseBO<VeBaseSpecialty> major = getUrl(majorUrl, VeBaseSpecialty.class);
//        veZsStudentLoanVo.setSpecId(major.getResult().getZymc());
        VeBaseFaculty faculty = veBaseFacultyService.getById(veZsStudentLoanVo.getFalId());
        veZsStudentLoanVo.setFalId(faculty.getYxmc());//2021.9.2 lbh
        VeBaseSpecialty specialty = veBaseSpecialtyService.getById(veZsStudentLoanVo.getSpecId());
        veZsStudentLoanVo.setSpecId(specialty.getZymc());//2021.9.2
        if (veZsStudentLoanVo.getIsCheck().equals(0)) {
            veZsStudentLoanVo.setIsCheck("待审核");
        } else if (veZsStudentLoanVo.getIsCheck().equals(1)) {
            veZsStudentLoanVo.setIsCheck("通过");
        } else if (veZsStudentLoanVo.getIsCheck().equals(2)) {
            veZsStudentLoanVo.setIsCheck("不通过");
        }
        map.put("result",veZsStudentLoanVo);
        return Result.ok(map);
    }

    @Override
    public Boolean auditPass(Long[] ids) {
        for (Long id : ids){
            veZsStudentLoanMapper.auditPass(id);
        }
        return true;
    }

    @Override
    public Boolean auditFail(Long[] ids,String failureReason) {
        for (Long id : ids){
            veZsStudentLoanMapper.auditFail(id,failureReason);
        }
        return true;
    }

    @Override
    public Boolean updateLoan(Integer id, String bank, String bankName, String bank_account, Long loan_amount, String bank_receipt,String contractNo) {
        return veZsStudentLoanMapper.updateLoan(id, bank, bankName, bank_account, loan_amount, bank_receipt,contractNo);
    }


    /**
     * 调用公告数据接口
     */
    private <T> BasicResponseBO<T> getUrl(String url, Class<T> clazz) {
        try {
            url = dirHost + url;
            String result = HttpURLConnectionUtil.doGet(url);
            BasicResponseBO<T> re = mapper.readValue(result, new TypeReference<BasicResponseBO<T>>() {
            });
            T body = mapper.readValue(mapper.writeValueAsString(re.getResult()), clazz);
            re.setResult(body);
            return re;
        } catch (Exception e) {
            return new BasicResponseBO<T>().setSuccess(false).setMessage("操作失败");
        }
    }
}
