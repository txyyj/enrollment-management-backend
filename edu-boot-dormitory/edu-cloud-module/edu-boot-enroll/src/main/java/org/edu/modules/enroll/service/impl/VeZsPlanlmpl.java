package org.edu.modules.enroll.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.enroll.entity.VeFileFiles;
import org.edu.modules.enroll.entity.VeZsPlan;
import org.edu.modules.enroll.mapper.VeZsPlanMapper;
import org.edu.modules.enroll.service.VeZsPlanService;
import org.edu.modules.enroll.vo.VeZsPlanVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
@Service
public class VeZsPlanlmpl extends ServiceImpl<VeZsPlanMapper, VeZsPlan> implements VeZsPlanService {
    @Resource
    private VeZsPlanMapper veZsPlanMapper;

    @Override
    public List<VeZsPlanVo> lianbiao(String name, Long falId, Long specialId) {
        return veZsPlanMapper.lianbiao(name, falId, specialId);
    }

    @Override
    public Integer delQuarter(Integer id) {
        return veZsPlanMapper.delQuarter(id);
    }

    @Override
    public Integer addZsjQuarter(Integer falId, Integer specId, String ZSNF, Integer ZSJ, Integer BJS, Integer NANSRS, Integer NVSRS, Integer fileId, String ZYYQ, String PYMB, String ZYZYKC, String BXTJ, String remark, Integer ZRS) {
        return veZsPlanMapper.addZsjQuarter(falId, specId, ZSNF, ZSJ, BJS, NANSRS, NVSRS, fileId, ZYYQ, PYMB, ZYZYKC, BXTJ, remark, ZRS);
    }

    @Override
    public Integer updateZsQuarter(Integer falId, Integer specId, String ZSNF, Integer ZSJ, Integer BJS, Integer NANSRS, Integer NVSRS, Integer fileId, String ZYYQ, String PYMB, String ZYZYKC, String BXTJ, String remark, Integer ZRS, Integer id) {
        return veZsPlanMapper.updateZsQuarter(falId, specId, ZSNF, ZSJ, BJS, NANSRS, NVSRS, fileId, ZYYQ, PYMB, ZYZYKC, BXTJ, remark, ZRS, id);
    }

    @Override
    public List<VeZsPlanVo> ZsJy(Integer id) {
        return veZsPlanMapper.ZsJy(id);
    }

    @Override
    public boolean addFujian(VeFileFiles veFileFiles) {
        return veZsPlanMapper.addFujian(veFileFiles);
    }

    @Override
    public Integer findFileId(String fileUrl) {

        return veZsPlanMapper.findFileId(fileUrl);
    }

    @Override
    public String  findUrl(Integer id) {

        return veZsPlanMapper.findUrl(id);
    }


}
