package org.edu.modules.common.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.File;
import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.transaction.Transactional;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.util.PasswordUtil;
import org.edu.modules.common.entity.SysLog;
import org.edu.modules.common.entity.VeBaseAppUser;
import org.edu.modules.common.entity.VeBaseBanJi;
import org.edu.modules.common.entity.VeBaseBanJiBzr;
import org.edu.modules.common.entity.VeBaseCalendar;
import org.edu.modules.common.entity.VeBaseCampus;
import org.edu.modules.common.entity.VeBaseDataApply;
import org.edu.modules.common.entity.VeBaseDepartment;
import org.edu.modules.common.entity.VeBaseDictArea;
import org.edu.modules.common.entity.VeBaseDictData;
import org.edu.modules.common.entity.VeBaseDictionary;
import org.edu.modules.common.entity.VeBaseFaculty;
import org.edu.modules.common.entity.VeBaseGrade;
import org.edu.modules.common.entity.VeBaseInterfaceUser;
import org.edu.modules.common.entity.VeBaseJYZ;
import org.edu.modules.common.entity.VeBaseSemester;
import org.edu.modules.common.entity.VeBaseSpecialty;
import org.edu.modules.common.entity.VeBaseStudent;
import org.edu.modules.common.entity.VeBaseStudentInfo;
import org.edu.modules.common.entity.VeBaseSysConfig;
import org.edu.modules.common.entity.VeBaseTeacher;
import org.edu.modules.common.entity.VeBaseXueZhi;
import org.edu.modules.common.feign.EduJwJxzyFeignInteface;
import org.edu.modules.common.redis.RedisUtils;
import org.edu.modules.common.service.ISysLogService;
import org.edu.modules.common.service.IVeBaseAppUserService;
import org.edu.modules.common.service.IVeBaseBanJiBzrService;
import org.edu.modules.common.service.IVeBaseBanJiService;
import org.edu.modules.common.service.IVeBaseBuildRoomService;
import org.edu.modules.common.service.IVeBaseBuildService;
import org.edu.modules.common.service.IVeBaseCalendarService;
import org.edu.modules.common.service.IVeBaseCampusService;
import org.edu.modules.common.service.IVeBaseDataApplyService;
import org.edu.modules.common.service.IVeBaseDepartmentService;
import org.edu.modules.common.service.IVeBaseDictAreaService;
import org.edu.modules.common.service.IVeBaseDictDataService;
import org.edu.modules.common.service.IVeBaseDictionaryService;
import org.edu.modules.common.service.IVeBaseFacultyService;
import org.edu.modules.common.service.IVeBaseGradeService;
import org.edu.modules.common.service.IVeBaseInterfaceUserService;
import org.edu.modules.common.service.IVeBaseJYZService;
import org.edu.modules.common.service.IVeBaseSemesterService;
import org.edu.modules.common.service.IVeBaseSpecialtyService;
import org.edu.modules.common.service.IVeBaseStudentInfoService;
import org.edu.modules.common.service.IVeBaseStudentService;
import org.edu.modules.common.service.IVeBaseSysConfigService;
import org.edu.modules.common.service.IVeBaseTeacherService;
import org.edu.modules.common.service.IVeBaseXueZhiService;
import org.edu.modules.common.util.DateTimeUtil;
import org.edu.modules.common.util.EmailUtil;
import org.edu.modules.common.util.IdCardUtil;
import org.edu.modules.common.util.PhoneUtil;
import org.edu.modules.common.vo.VoAppUserBySearch;
import org.edu.modules.common.vo.VoKettle;
import org.edu.modules.common.vo.VoStudentAndStudentInfo;
import org.edu.modules.common.we.AliyunOSSUtil;
import org.edu.modules.common.we.FileDTO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Api(tags={"公共数据"})
@RestController
@RequestMapping({"/common/veCommon"})
@ApiSort(60)
public class VeCommonController
{
    private static final Logger log = LoggerFactory.getLogger(VeCommonController.class);
    @Autowired
    private IVeBaseXueZhiService xueZhiService;
    @Autowired
    private IVeBaseGradeService gradeService;
    @Autowired
    private IVeBaseFacultyService facultyService;
    @Autowired
    private IVeBaseSpecialtyService specialtyService;
    @Autowired
    private IVeBaseBanJiService banJiService;
    @Autowired
    private IVeBaseBanJiBzrService veBaseBanJiBzrService;
    @Autowired
    private IVeBaseDepartmentService departmentService;
    @Autowired
    private IVeBaseCampusService veBaseCampusService;
    @Autowired
    private IVeBaseStudentService veBaseStudentService;
    @Autowired
    private IVeBaseStudentInfoService veBaseStudentInfoService;
    @Autowired
    private IVeBaseTeacherService veBaseTeacherService;
    @Autowired
    private IVeBaseSemesterService veBaseSemesterService;
    @Autowired
    private IVeBaseDictAreaService veBaseDictAreaService;
    @Autowired
    private IVeBaseDictionaryService veBaseDictionaryService;
    @Autowired
    private IVeBaseDictDataService veBaseDictDataService;
    @Autowired
    private IVeBaseInterfaceUserService veBaseInterfaceUserService;
    @Autowired
    private IVeBaseAppUserService veBaseAppUserService;
    @Autowired
    private IVeBaseJYZService veBaseJYZService;
    @Autowired
    private ISysLogService sysLogService;
    @Autowired
    private IVeBaseBuildService veBaseBuildService;
    @Autowired
    private IVeBaseBuildRoomService veBaseBuildRoomService;
    @Autowired
    private IVeBaseDataApplyService veBaseDataApplyService;
    @Autowired
    private IVeBaseSysConfigService veBaseSysConfigService;
    @Autowired
    private IVeBaseCalendarService veBaseCalendarService;
    @Autowired
    private EduJwJxzyFeignInteface eduJwJxzyFeignInteface;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private AliyunOSSUtil aliyunOSSUtil;

    private Result<?> interfaceUser(String id, String interfaceName)
    {
        Result result = new Result();
        result.setMessage("");
        if (("".equals(id)) || (id == null))
        {
            result.setMessage("接口用户id不能为空! ");
            result.setCode(Integer.valueOf(500));
            return result;
        }
        VeBaseInterfaceUser veBaseInterfaceUser = (VeBaseInterfaceUser)this.veBaseInterfaceUserService.getById(id);

        List<Map<String, Object>> modelList = this.veBaseInterfaceUserService.getInterfaceUserByIdAndInterfaceName(id, interfaceName);
        if ((veBaseInterfaceUser == null) || (modelList.size() < 1))
        {
            result.setMessage("暂无权限! ");
            result.setCode(Integer.valueOf(500));
        }
        return result;
    }

    @AutoLog("获取所有学制")
    @ApiOperation(value="获取所有学制", notes="获取所有学制")
    @GetMapping({"/queryXueZhiList"})
    public Result<?> queryXueZhiList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryXueZhiList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        boolean key = this.redisUtils.exists("queryXueZhiList");
        List<VeBaseXueZhi> list = null;
        if (key)
        {
            list = (List)this.redisUtils.get("queryXueZhiList");
        }
        else
        {
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.orderByAsc("years");
            list = this.xueZhiService.list(queryWrapper);
            this.redisUtils.set("queryXueZhiList", list);
        }
        return Result.OK(list);
    }

    @AutoLog("获取所有年级")
    @ApiOperation(value="获取所有年级", notes="获取所有年级")
    @GetMapping({"/queryGradeList"})
    public Result<?> queryGradeList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryGradeList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        boolean key = this.redisUtils.exists("queryGradeList");
        List<VeBaseGrade> list = null;
        if (key)
        {
            list = (List)this.redisUtils.get("queryGradeList");
        }
        else
        {
            QueryWrapper queryWrapper = new QueryWrapper();

            queryWrapper.eq("njzt", Integer.valueOf(1));
            queryWrapper.orderByDesc("rxnf");
            list = this.gradeService.list(queryWrapper);
            this.redisUtils.set("queryGradeList", list);
        }
        return Result.OK(list);
    }

    @AutoLog("获取所有院系")
    @ApiOperation(value="获取所有院系", notes="获取所有院系")
    @GetMapping({"/queryFacultyList"})
    public Result<?> queryFacultyList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryFacultyList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        boolean key = this.redisUtils.exists("queryFacultyList");
        List<VeBaseFaculty> list = null;
        if (key)
        {
            list = (List)this.redisUtils.get("queryFacultyList");
        }
        else
        {
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("status", Integer.valueOf(1));
            queryWrapper.orderByAsc("yxdm");
            list = this.facultyService.list(queryWrapper);
            this.redisUtils.set("queryFacultyList", list);
        }
        return Result.OK(list);
    }

    @AutoLog("获取所有部门信息")
    @ApiOperation(value="获取所有部门信息", notes="获取所有部门信息")
    @GetMapping({"/queryDepartmentList"})
    public Result<?> queryDepartmentList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryDepartmentList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        boolean key = this.redisUtils.exists("queryDepartmentList");
        List<VeBaseDepartment> list = null;
        if (key)
        {
            list = (List)this.redisUtils.get("queryDepartmentList");
        }
        else
        {
            list = this.departmentService.getTreeList();
            this.redisUtils.set("queryDepartmentList", list);
        }
        return Result.OK(list);
    }

    @AutoLog("获取所有部门信息和部门下的老师信息")
    @ApiOperation(value="获取所有部门信息和部门下的老师信息", notes="获取所有部门信息和部门下的老师信息")
    @GetMapping({"/queryDepartmentAndTeacherList"})
    public Result<?> queryDepartmentAndTeacherList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryDepartmentAndTeacherList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        boolean key = this.redisUtils.exists("queryDepartmentAndTeacherList");
        List<VeBaseDepartment> list = null;
        if (key)
        {
            list = (List)this.redisUtils.get("queryDepartmentAndTeacherList");
        }
        else
        {
            list = this.departmentService.getDepartmentAndTeacherList();
            this.redisUtils.set("queryDepartmentAndTeacherList", list);
        }
        return Result.OK(list);
    }

    @AutoLog("获取所有校区信息")
    @ApiOperation(value="获取所有校区信息", notes="获取所有校区信息")
    @GetMapping({"/queryCampusList"})
    public Result<?> queryCampusList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryCampusList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        boolean key = this.redisUtils.exists("queryCampusList");
        List<VeBaseCampus> list = null;
        if (key)
        {
            list = (List)this.redisUtils.get("queryCampusList");
        }
        else
        {
            list = this.veBaseCampusService.list();
            this.redisUtils.set("queryCampusList", list);
        }
        return Result.OK(list);
    }

    @AutoLog("获取所有学期信息")
    @ApiOperation(value="获取所有学期信息", notes="获取所有学期信息")
    @GetMapping({"/querySemesterList"})
    public Result<?> querySemesterList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/querySemesterList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        boolean key = this.redisUtils.exists("querySemesterList");
        List<VeBaseSemester> list = null;
        if (key)
        {
            list = (List)this.redisUtils.get("querySemesterList");
        }
        else
        {
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.orderByDesc("xqm");
            list = this.veBaseSemesterService.list(queryWrapper);
            this.redisUtils.set("querySemesterList", list);
        }
        return Result.OK(list);
    }

    @AutoLog("获取所有专业")
    @ApiOperation(value="获取所有专业", notes="获取所有专业")
    @GetMapping({"/querySpecialtyList"})
    public Result<?> querySpecialtyList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/querySpecialtyList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<VeBaseSpecialty> list = this.specialtyService.list();
        return Result.OK(list);
    }

    @AutoLog("根据院系id获取所有专业")
    @ApiOperation(value="根据院系id获取所有专业", notes="根据院系id获取所有专业")
    @GetMapping({"/querySpecialtyListByFalId"})
    public Result<?> querySpecialtyListByFalId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="falId", required=true) Integer falId)
    {
        Result result = interfaceUser(interfaceUserId, "/querySpecialtyListByFalId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<VeBaseSpecialty> list = this.specialtyService.specialtyListByFalid(falId);
        return Result.OK(list);
    }

    @AutoLog("根据专业id获取所有班级")
    @ApiOperation(value="根据专业id获取所有班级", notes="根据专业id获取所有班级")
    @GetMapping({"/queryBanJiListBySpecId"})
    public Result<?> queryBanJiListBySpecId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="specId", required=true) Integer specId, @RequestParam(name="bystatus", required=false) Integer bystatus)
    {
        Result result = interfaceUser(interfaceUserId, "/queryBanJiListBySpecId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<VeBaseBanJi> list = this.banJiService.getBanJiListBySpecId(specId, bystatus);
        return Result.OK(list);
    }

    @AutoLog("根据专业和年级id获取所有班级")
    @ApiOperation(value="根据专业和年级id获取所有班级", notes="根据专业和年级id获取所有班级")
    @GetMapping({"/queryBanJiListBySpecAndGradeId"})
    public Result<?> queryBanJiListBySpecAndGradeId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="specId", required=true) Integer specId, @RequestParam(name="gradeId", required=true) Integer gradeId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryBanJiListBySpecAndGradeId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<VeBaseBanJi> list = this.banJiService.queryBanJiListBySpecAndGradeId(specId, gradeId);
        return Result.OK(list);
    }

    @AutoLog("根据年级id获取所有班级")
    @ApiOperation(value="根据年级id获取所有班级", notes="根据年级id获取所有班级")
    @GetMapping({"/queryBanJiListByGradeId"})
    public Result<?> queryBanJiListByGradeId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="gradeId", required=true) Integer gradeId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryBanJiListByGradeId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<VeBaseBanJi> list = this.banJiService.getBanJiListByGradeId(gradeId);
        return Result.OK(list);
    }

    @AutoLog("根据教师id获取教师信息")
    @ApiOperation(value="根据教师id获取教师信息", notes="根据教师id获取教师信息")
    @GetMapping({"/queryTeacherById"})
    public Result<?> queryTeacherById(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="teacherId", required=true) Integer teacherId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryTeacherById");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseTeacher veBaseTeacher = (VeBaseTeacher)this.veBaseTeacherService.getById(teacherId);
        if (veBaseTeacher != null)
        {
            veBaseTeacher.setSfzjh("******");
            veBaseTeacher.setLxdh("******");
        }
        return Result.OK(veBaseTeacher);
    }

    @AutoLog("根据老师的user_id查找老师的信息")
    @ApiOperation(value="根据老师的user_id查找老师的信息", notes="根据老师的user_id查找老师的信息")
    @GetMapping({"/queryTeacherByUserId"})
    public Result<?> queryTeacherByUserId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="userId", required=true) String userId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryTeacherByUserId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseTeacher model = this.veBaseTeacherService.getByUserId(userId);
        return Result.OK(model);
    }

    @AutoLog("根据老师的工号查找老师的信息")
    @ApiOperation(value="根据老师的工号查找老师的信息", notes="根据老师的工号查找老师的信息")
    @GetMapping({"/queryTeacherByGH"})
    public Result<?> queryTeacherByGH(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="gh", required=true) String gh)
    {
        Result result = interfaceUser(interfaceUserId, "/queryTeacherByGH");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseTeacher model = this.veBaseTeacherService.getByGH(null, gh);
        return Result.OK(model);
    }

    @AutoLog("根据学生userId查找学生信息")
    @ApiOperation(value="根据学生userId查找学生信息", notes="根据学生userId查找学生信息")
    @GetMapping({"/queryStudentByUserId"})
    public Result<?> queryStudentByUserId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="userId", required=true) String userId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryStudentByUserId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseStudent model = this.veBaseStudentService.getModelByUserId(userId);
        return Result.OK(model);
    }

    @AutoLog("查询所有学生信息")
    @ApiOperation(value="查询所有学生信息", notes="查询所有学生信息")
    @GetMapping({"/getStudentList"})
    public Result<?> getStudentList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="name", required=false) String name)
    {
        Result result = interfaceUser(interfaceUserId, "/getStudentList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<Map<String, Object>> list = this.veBaseStudentService.getModelByName(name);
        return Result.OK(list);
    }

    @AutoLog("根据年级id获取年级信息")
    @ApiOperation(value="根据年级id获取年级信息", notes="根据年级id获取年级信息")
    @GetMapping({"/queryGradeById"})
    public Result<?> queryGradeById(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="gradeId", required=true) Integer gradeId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryGradeById");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseGrade veBaseGrade = (VeBaseGrade)this.gradeService.getById(gradeId);
        return Result.OK(veBaseGrade);
    }

    @AutoLog("根据学期id获取学期信息")
    @ApiOperation(value="根据学期id获取学期信息", notes="根据学期id获取学期信息")
    @GetMapping({"/querySemesterById"})
    public Result<?> querySemesterById(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="semesterId", required=true) Integer semesterId)
    {
        Result result = interfaceUser(interfaceUserId, "/querySemesterById");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseSemester veBaseSemester = (VeBaseSemester)this.veBaseSemesterService.getById(semesterId);
        return Result.OK(veBaseSemester);
    }

    @AutoLog("根据学期码学期名称获取学期信息")
    @ApiOperation(value="根据学期码学期名称获取学期信息", notes="根据学期码学期名称获取学期信息")
    @GetMapping({"/getSemesterByCodeName"})
    public Result<?> getSemesterByCodeName(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="semesterCode", required=false) String semesterCode, @RequestParam(name="semesterName", required=false) String semesterName)
    {
        Result result = interfaceUser(interfaceUserId, "/getSemesterByCodeName");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<VeBaseSemester> veBaseSemester = this.veBaseSemesterService.getByCodeName(semesterCode, semesterName);
        return Result.OK(veBaseSemester);
    }

    @AutoLog("根据学制id获取学制信息")
    @ApiOperation(value="根据学制id获取学制信息", notes="根据学制id获取学制信息")
    @GetMapping({"/queryXueZhiById"})
    public Result<?> queryXueZhiById(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="xueZhiId", required=true) Integer xueZhiId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryXueZhiById");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseXueZhi veBaseXueZhi = (VeBaseXueZhi)this.xueZhiService.getById(xueZhiId);
        return Result.OK(veBaseXueZhi);
    }

    @AutoLog("根据院系id获取院系信息")
    @ApiOperation(value="根据院系id获取院系信息", notes="根据院系id获取院系信息")
    @GetMapping({"/queryFacultyById"})
    public Result<?> queryFacultyById(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="facultyId", required=true) Integer facultyId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryFacultyById");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseFaculty veBaseFaculty = (VeBaseFaculty)this.facultyService.getById(facultyId);
        return Result.OK(veBaseFaculty);
    }

    @AutoLog("根据专业id获取专业信息")
    @ApiOperation(value="根据专业id获取专业信息", notes="根据专业id获取专业信息")
    @GetMapping({"/querySpecialtyById"})
    public Result<?> querySpecialtyById(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="specialtyId", required=true) Integer specialtyId)
    {
        Result result = interfaceUser(interfaceUserId, "/querySpecialtyById");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseSpecialty veBaseSpecialty = (VeBaseSpecialty)this.specialtyService.getById(specialtyId);
        return Result.OK(veBaseSpecialty);
    }

    @AutoLog("根据班级id获取班级信息")
    @ApiOperation(value="根据班级id获取班级信息", notes="根据班级id获取班级信息")
    @GetMapping({"/queryBanJiById"})
    public Result<?> queryBanJiById(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="banJiId", required=true) Integer banJiId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryBanJiById");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseBanJi veBaseBanJi = (VeBaseBanJi)this.banJiService.getById(banJiId);
        return Result.OK(veBaseBanJi);
    }

    @AutoLog("获取所有省份信息")
    @ApiOperation(value="获取所有省份信息", notes="获取所有省份信息")
    @GetMapping({"/queryProvinceList"})
    public Result<?> queryProvinceList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryProvinceList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        boolean key = this.redisUtils.exists("queryProvinceList");
        List<VeBaseDictArea> list = null;
        if (key)
        {
            list = (List)this.redisUtils.get("queryProvinceList");
        }
        else
        {
            list = this.veBaseDictAreaService.getProvinceList();
            this.redisUtils.set("queryProvinceList", list);
        }
        return Result.OK(list);
    }

    @AutoLog("根据省份id获取所有市级信息")
    @ApiOperation(value="根据省份id获取所有市级信息", notes="根据省份id获取所有市级信息")
    @GetMapping({"/queryCityListByProvinceId"})
    public Result<?> queryCityList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="provinceId", required=true) Integer provinceId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryCityListByProvinceId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<VeBaseDictArea> list = this.veBaseDictAreaService.getCityList(provinceId);
        return Result.OK(list);
    }

    @AutoLog("根据市级id获取所有县级信息")
    @ApiOperation(value="根据市级id获取所有县级信息", notes="根据市级id获取所有县级信息")
    @GetMapping({"/queryCountyListByCityId"})
    public Result<?> queryCountyList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="cityId", required=true) Integer cityId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryCountyListByCityId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<VeBaseDictArea> list = this.veBaseDictAreaService.getCountyList(cityId);
        return Result.OK(list);
    }

    @AutoLog("根据userId获取当前用户信息")
    @ApiOperation(value="根据userId获取当前用户信息", notes="根据userId获取当前用户信息")
    @GetMapping({"/queryAppUserByUserId"})
    public Result<?> queryAppUserByUserId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="userId", required=true) String userId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryAppUserByUserId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseAppUser veBaseAppUser = this.veBaseAppUserService.getAppUserByUserId(userId);
        if (veBaseAppUser != null) {
            veBaseAppUser.setUserTel("******");
        }
        return Result.OK(veBaseAppUser);
    }

    @AutoLog("编辑个人资料")
    @ApiOperation(value="编辑个人资料", notes="编辑个人资料")
    @PostMapping({"/editAppUser"})
    public Result<?> editAppUser(@RequestBody VeBaseAppUser veBaseAppUser)
            throws ParseException
    {
        Result result = interfaceUser(veBaseAppUser.getInterfaceUserId(), "/editAppUser");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        if ((!"".equals(veBaseAppUser.getBirthdayName())) && (veBaseAppUser.getBirthdayName() != null))
        {
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            veBaseAppUser.setBirthday(simpleDateFormat.parse(veBaseAppUser.getBirthdayName()));
        }
        this.veBaseAppUserService.updateById(veBaseAppUser);
        return Result.ok("修改成功!");
    }

    @PostMapping({"/uploadHead"})
    public FileDTO uploadHead(@RequestParam("file") MultipartFile file, @RequestParam("id") String id)
    {
        String fileName = file.getOriginalFilename();
        String prefix = fileName.substring(0, fileName.indexOf("."));
        String str = fileName.substring(prefix.length() + 1, fileName.length());
        if (("".equals(str)) || (str == null)) {
            return new FileDTO(
                    Long.valueOf(0L), "", "", "", "", "", "", "请重新上传图片! ");
        }
        if ((!str.equals("png")) && (!str.equals("jpg")) && (!str.equals("gif"))) {
            return new FileDTO(
                    Long.valueOf(0L), "", "", "", "", "", "", "图片后缀不正确, 请上传png或jpg或gif格式! ");
        }
        if (file.getSize() > 2097152L) {
            return new FileDTO(
                    Long.valueOf(0L), "", "", "", "", "", "", "图片大小超过2M, 请重新上传! ");
        }
        try
        {
            if ((file != null) &&
                    (!"".equals(fileName.trim())))
            {
                File newFile = new File(fileName);
                FileOutputStream os = new FileOutputStream(newFile);
                os.write(file.getBytes());
                os.close();
                file.transferTo(newFile);

                return this.aliyunOSSUtil.uploadById(newFile, id);
            }
        }
        catch (Exception ex)
        {
            ex.printStackTrace();
        }
        return null;
    }

    @AutoLog("修改密码")
    @ApiOperation(value="修改密码", notes="修改密码")
    @PostMapping({"/editPassword"})
    public Result<?> editPassword(@RequestBody VeBaseAppUser veBaseAppUser)
    {
        VeBaseAppUser appUser = (VeBaseAppUser)this.veBaseAppUserService.getById(veBaseAppUser.getId());
        if (appUser == null) {
            return Result.error("该用户不存在!");
        }
        if (("".equals(veBaseAppUser.getOldPwd())) || (veBaseAppUser.getOldPwd() == null)) {
            return Result.error("旧密码不能为空!");
        }
        if (("".equals(veBaseAppUser.getNewPwd())) || (veBaseAppUser.getNewPwd() == null) || ("".equals(veBaseAppUser.getAffirmPwd())) || (veBaseAppUser.getAffirmPwd() == null)) {
            return Result.error("新密码或确认密码不能为空!");
        }
        if (!veBaseAppUser.getNewPwd().equals(veBaseAppUser.getAffirmPwd())) {
            return Result.error("新密码与确认密码不一致!");
        }
        String oldPwd = PasswordUtil.encrypt(appUser.getUserName(), veBaseAppUser.getOldPwd(), appUser.getSalt());
        if (!appUser.getUserPassword().equals(oldPwd)) {
            return Result.error("旧密码错误, 请重新输入!");
        }
        String pwd = PasswordUtil.encrypt(appUser.getUserName(), veBaseAppUser.getNewPwd(), appUser.getSalt());

        String strength = this.veBaseAppUserService.pwdVerify(veBaseAppUser.getNewPwd());
        this.veBaseAppUserService.updateAppUserPasswordById(appUser.getId(), pwd, strength);
        return Result.ok("修改成功! ");
    }

    @AutoLog("添加地区信息")
    @ApiOperation(value="添加地区信息", notes="添加地区信息")
    @PostMapping({"/addDictArea"})
    @Transactional
    public Result<?> addDictArea(@RequestBody VeBaseDictArea veBaseDictArea)
    {
        Result result = interfaceUser(veBaseDictArea.getInterfaceUserId(), "/addDictArea");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        if (("".equals(veBaseDictArea.getName())) || (veBaseDictArea.getName() == null)) {
            return Result.error("地区不能为空!");
        }
        if (veBaseDictArea.getPid() == null) {
            return Result.error("上级部门不能为空!");
        }
        VeBaseDictArea model = this.veBaseDictAreaService.getDictAreaByPidAndName(null, veBaseDictArea.getPid(), veBaseDictArea.getName());
        if (model != null) {
            return Result.error("地区已存在!");
        }
        this.veBaseDictAreaService.save(veBaseDictArea);
        if (veBaseDictArea.getPid().intValue() == 0) {
            this.redisUtils.set("queryProvinceList", this.veBaseDictAreaService.getProvinceList());
        }
        return Result.ok("添加成功!");
    }

    @AutoLog("编辑地区信息")
    @ApiOperation(value="编辑地区信息", notes="编辑地区信息")
    @PostMapping({"/editDictArea"})
    @Transactional
    public Result<?> editDictArea(@RequestBody VeBaseDictArea veBaseDictArea)
    {
        Result result = interfaceUser(veBaseDictArea.getInterfaceUserId(), "/editDictArea");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        if (("".equals(veBaseDictArea.getName())) || (veBaseDictArea.getName() == null)) {
            return Result.error("地区不能为空!");
        }
        if (veBaseDictArea.getPid() == null) {
            return Result.error("上级部门不能为空!");
        }
        VeBaseDictArea model = this.veBaseDictAreaService.getDictAreaByPidAndName(veBaseDictArea.getId(), veBaseDictArea.getPid(), veBaseDictArea.getName());
        if (model != null) {
            return Result.error("地区已存在!");
        }
        this.veBaseDictAreaService.updateById(veBaseDictArea);
        if (veBaseDictArea.getPid().intValue() == 0) {
            this.redisUtils.set("queryProvinceList", this.veBaseDictAreaService.getProvinceList());
        }
        return Result.ok("修改成功!");
    }

    @AutoLog("地区信息批量删除")
    @ApiOperation(value="地区信息批量删除", notes="地区信息批量删除")
    @PostMapping({"/deleteDictAreaBatch"})
    @Transactional
    public Result<?> deleteDictAreaBatch(@RequestParam(name="ids", required=true) String ids, @RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/deleteDictAreaBatch");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        this.veBaseDictAreaService.removeByIds(Arrays.asList(ids.split(",")));

        this.redisUtils.set("queryProvinceList", this.veBaseDictAreaService.getProvinceList());
        return Result.ok("批量删除成功!");
    }

    @AutoLog("获取所有角色")
    @ApiOperation(value="获取所有角色", notes="获取所有角色")
    @GetMapping({"/querySysRoleList"})
    public Result<?> querySysRoleList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/querySysRoleList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<Map<String, Object>> list = this.veBaseAppUserService.querySysRoleList();
        return Result.ok(list);
    }

    @AutoLog("教师信息-分页列表查询(PageHelper)")
    @ApiOperation(value="教师信息-分页列表查询(PageHelper)", notes="教师信息-分页列表查询(PageHelper)")
    @GetMapping({"/getTeacherPageList"})
    public Result<?> getTeacherPageList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="gh", required=false) String gh, @RequestParam(name="xm", required=false) String xm, @RequestParam(name="sfzjh", required=false) String sfzjh, @RequestParam(name="depId", required=false) Integer depId, @RequestParam(name="jyzId", required=false) Integer jyzId, @RequestParam(name="bzlbm", required=false) String bzlbm, @RequestParam(name="jzglbm", required=false) String jzglbm, @RequestParam(name="teacherIds", required=false) String teacherIds, @RequestParam(name="roleId", required=false) Integer roleId, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        Result result = interfaceUser(interfaceUserId, "/getTeacherPageList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseTeacher veBaseTeacher = new VeBaseTeacher();
        veBaseTeacher.setGh(gh);
        veBaseTeacher.setXm(xm);
        veBaseTeacher.setSfzjh(sfzjh);
        veBaseTeacher.setDepId(depId);
        veBaseTeacher.setJyzId(jyzId);
        veBaseTeacher.setBzlbm(bzlbm);
        veBaseTeacher.setJzglbm(jzglbm);
        if ((!"".equals(teacherIds)) && (teacherIds != null))
        {
            String[] ids = teacherIds.split(",");
            veBaseTeacher.setTeacherIds(ids);
        }
        else
        {
            veBaseTeacher.setTeacherIds(null);
        }
        veBaseTeacher.setRoleId(roleId);

        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseTeacherService.getTeacherPageList(veBaseTeacher);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.ok(pageInfo);
    }

    @AutoLog("教师信息-分页列表查询(IPage)")
    @ApiOperation(value="教师信息-分页列表查询(IPage)", notes="教师信息-分页列表查询(IPage)")
    @GetMapping({"/getTeacherPageListByIPage"})
    public Result<?> getTeacherPageListByIPage(VeBaseTeacher veBaseTeacher, @RequestParam(name="teacherIds", required=false) String teacherIds, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        Result result = interfaceUser(veBaseTeacher.getInterfaceUserId(), "/getTeacherPageListByIPage");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        if ((!"".equals(teacherIds)) && (teacherIds != null))
        {
            String[] ids = teacherIds.split(",");
            veBaseTeacher.setTeacherIds(ids);
        }
        else
        {
            veBaseTeacher.setTeacherIds(null);
        }
        Page<Map<String, Object>> page = new Page(pageNo.intValue(), pageSize.intValue());
        IPage<Map<String, Object>> pageList = this.veBaseTeacherService.getTeacherPageListByIPage(page, veBaseTeacher);
        return Result.ok(pageList);
    }

    @AutoLog("教师信息-列表查询")
    @ApiOperation(value="教师信息-列表查询", notes="教师信息-列表查询")
    @GetMapping({"/getTeacherList"})
    public Result<?> getTeacherList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="gh", required=false) String gh, @RequestParam(name="xm", required=false) String xm, @RequestParam(name="sfzjh", required=false) String sfzjh)
    {
        Result result = interfaceUser(interfaceUserId, "/getTeacherList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseTeacher veBaseTeacher = new VeBaseTeacher();
        veBaseTeacher.setGh(gh);
        veBaseTeacher.setXm(xm);
        veBaseTeacher.setSfzjh(sfzjh);
        List<VeBaseTeacher> list = this.veBaseTeacherService.getTeacherListBySearch(veBaseTeacher);
        if (list.size() > 0) {
            for (VeBaseTeacher model : list)
            {
                model.setSfzjh("******");
                model.setLxdh("******");
            }
        }
        return Result.OK(list);
    }

    @AutoLog("学生信息-分页列表查询")
    @ApiOperation(value="学生信息-分页列表查询", notes="学生信息-分页列表查询")
    @GetMapping({"/getStudentPageList"})
    public Result<?> getStudentPageList(VeBaseStudent veBaseStudent, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        Result result = interfaceUser(veBaseStudent.getInterfaceUserId(), "/getStudentPageList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseStudentService.getStudentPageList(veBaseStudent);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.ok(pageInfo);
    }

    @AutoLog("学生信息-不分页列表查询")
    @ApiOperation(value="学生信息-不分页列表查询", notes="学生信息-不分页列表查询")
    @GetMapping({"/getStudentNotPageList"})
    public Result<?> getStudentNotPageList(VeBaseStudent veBaseStudent)
    {
        Result result = interfaceUser(veBaseStudent.getInterfaceUserId(), "/getStudentNotPageList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<Map<String, Object>> list = this.veBaseStudentService.getStudentPageList(veBaseStudent);
        return Result.ok(list);
    }

    @AutoLog("学生信息-分页列表查询-参数非实体")
    @ApiOperation(value="学生信息-分页列表查询-参数非实体", notes="学生信息-分页列表查询-参数非实体")
    @GetMapping({"/getStudentPageListBySearch"})
    public Result<?> getStudentPageListBySearch(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="xh", required=false) String xh, @RequestParam(name="xm", required=false) String xm, @RequestParam(name="sfzh", required=false) String sfzh, @RequestParam(name="falId", required=false) Integer falId, @RequestParam(name="specId", required=false) Integer specId, @RequestParam(name="gradeId", required=false) Integer gradeId, @RequestParam(name="bjId", required=false) Integer bjId, @RequestParam(name="studentIds", required=false) String studentIds, @RequestParam(name="num", required=true) Integer num, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        Result result = interfaceUser(interfaceUserId, "/getStudentPageListBySearch");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseStudent veBaseStudent = new VeBaseStudent();
        veBaseStudent.setXh(xh);
        veBaseStudent.setXm(xm);
        veBaseStudent.setSfzh(sfzh);
        veBaseStudent.setFalId(falId);
        veBaseStudent.setSpecId(specId);
        veBaseStudent.setGradeId(gradeId);
        veBaseStudent.setBjId(bjId);
        veBaseStudent.setNum(num);
        if ((!"".equals(studentIds)) && (studentIds != null))
        {
            String[] ids = studentIds.split(",");
            veBaseStudent.setStudentIds(ids);
        }
        else
        {
            veBaseStudent.setStudentIds(null);
        }
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseStudentService.getStudentPageList(veBaseStudent);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.ok(pageInfo);
    }

    @AutoLog("教师信息-根据id查询-返回值VeBaseTeacher")
    @ApiOperation(value="教师信息-根据id查询-返回值VeBaseTeacher", notes="教师信息-根据id查询-返回值VeBaseTeacher")
    @GetMapping({"/getTeacherById"})
    public Result<?> getTeacherById(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="teacherId", required=true) Integer teacherId)
    {
        Result result = interfaceUser(interfaceUserId, "/getTeacherById");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseTeacher veBaseTeacher = (VeBaseTeacher)this.veBaseTeacherService.getById(teacherId);
        if (veBaseTeacher != null)
        {
            veBaseTeacher.setSfzjh("******");
            veBaseTeacher.setLxdh("******");
        }
        return Result.ok(veBaseTeacher);
    }

    @AutoLog("学生信息-根据学生id查询")
    @ApiOperation(value="学生信息-根据学生id查询", notes="学生信息-根据学生id查询")
    @GetMapping({"/getStudentById"})
    public Result<?> getStudentById(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="studentId", required=true) Integer studentId)
    {
        Result result = interfaceUser(interfaceUserId, "/getStudentById");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseStudent veBaseStudent = (VeBaseStudent)this.veBaseStudentService.getById(studentId);
        return Result.ok(veBaseStudent);
    }

    @AutoLog("学生基本信息-根据stuId查询")
    @ApiOperation(value="学生基本信息-根据stuId查询", notes="学生基本信息-根据stuId查询")
    @GetMapping({"/getStudentInfoById"})
    public Result<?> getStudentInfoById(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="studentInfoId", required=true) Integer studentInfoId)
    {
        Result result = interfaceUser(interfaceUserId, "/getStudentInfoById");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseStudentInfo veBaseStudentInfo = this.veBaseStudentInfoService.getModelById(studentInfoId);
        if (veBaseStudentInfo != null) {
            veBaseStudentInfo.setXslxdh("******");
        }
        return Result.ok(veBaseStudentInfo);
    }

    @AutoLog("学生基本信息-根据userId查询")
    @ApiOperation(value="学生基本信息-根据userId查询", notes="学生基本信息-根据userId查询")
    @GetMapping({"/getStudentInfoByUserId"})
    public Result<?> getStudentInfoByUserId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="userId", required=true) String userId)
    {
        Result result = interfaceUser(interfaceUserId, "/getStudentInfoByUserId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseStudent veBaseStudent = this.veBaseStudentService.getModelByUserId(userId);
        VeBaseStudentInfo veBaseStudentInfo = null;
        if (veBaseStudent != null) {
            veBaseStudentInfo = (VeBaseStudentInfo)this.veBaseStudentInfoService.getById(veBaseStudent.getId());
        }
        if (veBaseStudentInfo != null) {
            veBaseStudentInfo.setXslxdh("******");
        }
        return Result.ok(veBaseStudentInfo);
    }

    @AutoLog("操作日志-分页列表查询")
    @ApiOperation(value="操作日志-分页列表查询", notes="操作日志-分页列表查询")
    @GetMapping({"/querySysLogPageList"})
    public Result<?> querySysLogPageList(SysLog sysLog, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        Result result = interfaceUser(sysLog.getInterfaceUserId(), "/querySysLogPageList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<SysLog> list = this.sysLogService.getSysLogPageList(sysLog);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.ok(pageInfo);
    }

    @AutoLog("操作日志-分页列表查询(实习调用)")
    @ApiOperation(value="操作日志-分页列表查询(实习调用)", notes="操作日志-分页列表查询(实习调用)")
    @GetMapping({"/querySysLogPageListSX"})
    public Result<?> querySysLogPageListSX(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="userName", required=false) String userName, @RequestParam(name="ip", required=false) String ip, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        Result result = interfaceUser(interfaceUserId, "/querySysLogPageListSX");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        SysLog sysLog = new SysLog();
        sysLog.setUsername(userName);
        sysLog.setIp(ip);
        sysLog.setNum(Integer.valueOf(3));

        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<SysLog> list = this.sysLogService.getSysLogPageList(sysLog);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.ok(pageInfo);
    }

    @AutoLog("根据code编码查找数据字典信息")
    @ApiOperation(value="根据code编码查找数据字典信息", notes="根据code编码查找数据字典信息")
    @GetMapping({"/queryDictionaryByCode"})
    public Result<?> queryDictionaryByCode(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="code", required=true) String code)
    {
        Result result = interfaceUser(interfaceUserId, "/queryDictionaryByCode");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<VeBaseDictionary> model = this.veBaseDictionaryService.getDictionaryListByCode(code);
        return Result.OK(model);
    }

    @AutoLog("根据modelCode和(或code)查找字典数据信息")
    @ApiOperation(value="根据modelCode和(或code)查找字典数据信息", notes="根据modelCode和(或code)查找字典数据信息")
    @GetMapping({"/queryDictDataByModelCode"})
    public Result<?> queryDictDataByModelCode(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="modelCode", required=true) String modelCode, @RequestParam(name="code", required=false) String code)
    {
        Result result = interfaceUser(interfaceUserId, "/queryDictDataByModelCode");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<VeBaseDictData> model = this.veBaseDictDataService.getDictDataByModelCode(modelCode, code);
        return Result.OK(model);
    }

    @AutoLog("根据modelCode和title查找字典数据信息")
    @ApiOperation(value="根据modelCode和title查找字典数据信息", notes="根据modelCode和title查找字典数据信息")
    @GetMapping({"/queryDictDataByModelCodeTitle"})
    public Result<?> queryDictDataByModelCodeTitle(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="modelCode", required=true) String modelCode, @RequestParam(name="title") String title)
    {
        Result result = interfaceUser(interfaceUserId, "/queryDictDataByModelCodeTitle");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<VeBaseDictData> model = this.veBaseDictDataService.getDictDataByModelCodeTitle(modelCode, title);
        return Result.OK(model);
    }

    @AutoLog("数据字典数据-查询菜单")
    @ApiOperation(value="数据字典数据-查询菜单", notes="数据字典数据-查询菜单")
    @GetMapping({"/queryDictionaryTreeList"})
    public Result<?> queryDictionaryTreeList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryDictionaryTreeList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        boolean key = this.redisUtils.exists("queryDictionaryTreeList");
        List<VeBaseDictionary> list = null;
        if (key)
        {
            list = (List)this.redisUtils.get("queryDictionaryTreeList");
        }
        else
        {
            list = this.veBaseDictionaryService.getTreeList();
            this.redisUtils.set("queryDictionaryTreeList", list);
        }
        return Result.OK(list);
    }

    @AutoLog("数据字典-更新缓存")
    @ApiOperation(value="数据字典-更新缓存", notes="数据字典-更新缓存")
    @GetMapping({"/queryDictionaryTreeListRedis"})
    public Result<?> queryDictionaryTreeListRedis(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryDictionaryTreeListRedis");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        this.redisUtils.set("queryDictionaryTreeList", this.veBaseDictionaryService.getTreeList());
        return Result.OK(this.redisUtils.get("queryDictionaryTreeList"));
    }

    @AutoLog("数据字典管理-新增信息")
    @ApiOperation(value="数据字典管理-新增信息", notes="数据字典管理-新增信息")
    @PostMapping({"/addVeBaseDictionary"})
    public Result<?> addVeBaseDictionary(@RequestBody VeBaseDictionary veBaseDictionary)
    {
        Result result = interfaceUser(veBaseDictionary.getInterfaceUserId(), "/addVeBaseDictionary");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        veBaseDictionary.setTerminalId(1);
        this.veBaseDictionaryService.save(veBaseDictionary);

        this.redisUtils.set("queryDictionaryTreeList", this.veBaseDictionaryService.getTreeList());
        return Result.OK("添加成功！");
    }

    @AutoLog("数据字典管理-修改信息")
    @ApiOperation(value="数据字典管理-修改信息", notes="数据字典管理-修改信息")
    @PostMapping({"/editVeBaseDictionary"})
    public Result<?> editVeBaseDictionary(@RequestBody VeBaseDictionary veBaseDictionary)
    {
        Result result = interfaceUser(veBaseDictionary.getInterfaceUserId(), "/editVeBaseDictionary");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        this.veBaseDictionaryService.updateById(veBaseDictionary);

        this.redisUtils.set("queryDictionaryTreeList", this.veBaseDictionaryService.getTreeList());
        return Result.OK("编辑成功!");
    }

    @AutoLog("数据字典管理-通过id删除")
    @ApiOperation(value="数据字典管理-通过id删除", notes="数据字典管理-通过id删除")
    @PostMapping({"/deleteVeBaseDictionary"})
    public Result<?> deleteVeBaseDictionary(@RequestParam(name="id", required=true) String id, @RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/deleteVeBaseDictionary");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        this.veBaseDictionaryService.removeById(id);

        this.redisUtils.set("queryDictionaryTreeList", this.veBaseDictionaryService.getTreeList());
        return Result.OK("删除成功!");
    }

    @AutoLog("数据字典数据-新增信息")
    @ApiOperation(value="数据字典数据-新增信息", notes="数据字典数据管理表-新增信息")
    @PostMapping({"/addVeBaseDictData"})
    public Result<?> addVeBaseDictData(@RequestBody VeBaseDictData veBaseDictData)
    {
        Result result = interfaceUser(veBaseDictData.getInterfaceUserId(), "/addVeBaseDictData");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        veBaseDictData.setTerminalId(1);
        this.veBaseDictDataService.save(veBaseDictData);
        return Result.OK("添加成功！");
    }

    @AutoLog("数据字典数据-修改信息")
    @ApiOperation(value="数据字典数据-修改信息", notes="数据字典数据管理表-修改信息")
    @PostMapping({"/editVeBaseDictData"})
    public Result<?> editVeBaseDictData(@RequestBody VeBaseDictData veBaseDictData)
    {
        Result result = interfaceUser(veBaseDictData.getInterfaceUserId(), "/editVeBaseDictData");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        this.veBaseDictDataService.updateById(veBaseDictData);
        return Result.OK("编辑成功!");
    }

    @AutoLog("数据字典数据-通过id删除")
    @ApiOperation(value="数据字典数据-通过id删除", notes="数据字典数据管理表-通过id删除")
    @PostMapping({"/deleteVeBaseDictData"})
    public Result<?> deleteVeBaseDictData(@RequestParam(name="id", required=true) String id, @RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/deleteVeBaseDictData");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        this.veBaseDictDataService.removeById(id);
        return Result.OK("删除成功!");
    }

    @AutoLog("新增-用户信息")
    @ApiOperation(value="新增-用户信息", notes="新增-用户信息")
    @PostMapping({"/addVeBaseAppUser"})
    @Transactional
    public Result<?> addVeBaseAppUser(@RequestBody VeBaseAppUser veBaseAppUser)
    {
        Result result = interfaceUser(veBaseAppUser.getInterfaceUserId(), "/addVeBaseAppUser");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        if (("".equals(veBaseAppUser.getUserId())) || (veBaseAppUser.getUserId() == null)) {
            return Result.error("用户名不能为空!");
        }
        VeBaseAppUser model = this.veBaseAppUserService.getAppUserByUserId(veBaseAppUser.getUserId());
        if (model != null) {
            return Result.error("用户工号已存在!");
        }
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        veBaseAppUser.setId(uuid);
        veBaseAppUser.setIsDel("0");
        veBaseAppUser.setStatus("0");

        veBaseAppUser.setUserName(veBaseAppUser.getUserId());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:ss:mm");
        Date date = new Date();
        veBaseAppUser.setCreateTime(simpleDateFormat.format(date));
        veBaseAppUser.setUpdateTime(simpleDateFormat.format(date));
        VeBaseAppUser md = new VeBaseAppUser();
        md = this.veBaseAppUserService.getPwdAndSalt(veBaseAppUser);

        md.setStrength(this.veBaseAppUserService.pwdVerify(veBaseAppUser.getUserPassword()));
        this.veBaseAppUserService.save(md);

        this.veBaseAppUserService.addSysUser(md);
        this.veBaseAppUserService.addSysRoleUser(md.getUserId(), veBaseAppUser.getUserRoleId());
        return Result.OK("添加成功！");
    }

    @AutoLog("编辑-用户信息")
    @ApiOperation(value="编辑-用户信息", notes="编辑-用户信息")
    @PostMapping({"/editVeBaseAppUser"})
    @Transactional
    public Result<?> editVeBaseAppUser(@RequestBody VeBaseAppUser veBaseAppUser)
    {
        Result result = interfaceUser(veBaseAppUser.getInterfaceUserId(), "/editVeBaseAppUser");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        if (("".equals(veBaseAppUser.getUserId())) || (veBaseAppUser.getUserId() == null)) {
            return Result.error("用户名不能为空!");
        }
        VeBaseAppUser model = this.veBaseAppUserService.getAppUserByUserId(veBaseAppUser.getUserId());
        if (model == null) {
            return Result.error("用户不存在!");
        }
        model.setUserType(veBaseAppUser.getUserType());
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:ss:mm");
        Date date = new Date();
        model.setUpdateTime(simpleDateFormat.format(date));



        return Result.OK("修改成功！");
    }

    @AutoLog("查询院系->专业->班级-树形列表")
    @ApiOperation(value="查询院系->专业->班级-树形列表", notes="查询院系->专业->班级-树形列表")
    @GetMapping({"/queryFacultyAndSpecialtyAndClassTreeList"})
    public Result<?> queryFacultyAndSpecialtyAndClassTreeList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryFacultyAndSpecialtyAndClassTreeList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        boolean key = this.redisUtils.exists("queryFacultyAndSpecialtyAndClassTreeList");
        List<Map<String, Object>> list = null;
        if (key)
        {
            list = (List)this.redisUtils.get("queryFacultyAndSpecialtyAndClassTreeList");
        }
        else
        {
            list = this.facultyService.getTreeList();
            this.redisUtils.set("queryFacultyAndSpecialtyAndClassTreeList", list);
        }
        return Result.OK(list);
    }

    @AutoLog("查询班级信息-分页列表")
    @ApiOperation(value="查询班级信息-分页列表", notes="查询班级信息-分页列表")
    @GetMapping({"/queryBanJiPageList"})
    public Result<?> queryBanJiPageList(VeBaseBanJi veBaseBanJi, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        Result result = interfaceUser(veBaseBanJi.getInterfaceUserId(), "/queryBanJiPageList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.banJiService.getBanJiPageList(veBaseBanJi);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.OK(pageInfo);
    }

    @AutoLog("查询班级信息-分页列表")
    @ApiOperation(value="查询班级信息-分页列表", notes="查询班级信息-分页列表")
    @GetMapping({"/queryBanJiPageListBySearch"})
    public Result<?> queryBanJiPageListBySearch(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="code", required=false) String code, @RequestParam(name="bjmc", required=false) String bjmc, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        Result result = interfaceUser(interfaceUserId, "/queryBanJiPageListBySearch");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.banJiService.queryBanJiPageListBySearch(code, bjmc);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.OK(pageInfo);
    }

    @AutoLog("查询教研组(专业组)信息-树形列表")
    @ApiOperation(value="查询教研组(专业组)信息-树形列表", notes="查询教研组(专业组)信息-树形列表")
    @GetMapping({"/queryJYZTreeList"})
    public Result<?> queryJYZTreeList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryJYZTreeList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        boolean key = this.redisUtils.exists("queryJYZTreeList");
        List<Map<String, Object>> list = null;
        if (key)
        {
            list = (List)this.redisUtils.get("queryJYZTreeList");
        }
        else
        {
            list = this.veBaseJYZService.getJYZTreeList();
            this.redisUtils.set("queryJYZTreeList", list);
        }
        return Result.ok(list);
    }

    @AutoLog("根据教研组id获取教研组信息")
    @ApiOperation(value="根据教研组id获取教研组信息", notes="根据教研组id获取教研组信息")
    @GetMapping({"/queryJYZById"})
    public Result<?> queryJYZById(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="jyzId", required=true) Integer jyzId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryJYZById");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        Map map = this.veBaseJYZService.getJYZById(jyzId);
        return Result.ok(map);
    }

    @AutoLog("根据教研组id获取教师信息列表")
    @ApiOperation(value="根据教研组id获取教师信息列表", notes="根据教研组id获取教师信息列表")
    @GetMapping({"/queryTeacherListByJYZId"})
    public Result<?> queryTeacherListByJYZId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="jyzId", required=true) Integer jyzId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryTeacherListByJYZId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseTeacher veBaseTeacher = new VeBaseTeacher();
        veBaseTeacher.setJyzId(jyzId);
        List<Map<String, Object>> list = this.veBaseTeacherService.getTeacherPageList(veBaseTeacher);
        if (list.size() > 0) {
            for (Map map : list)
            {
                map.put("sfzjh", "******");
                map.put("lxdh", "******");
            }
        }
        return Result.ok(list);
    }

    @AutoLog("根据校区id查询校区信息")
    @ApiOperation(value="根据校区id查询校区信息", notes="根据校区id查询校区信息")
    @GetMapping({"/getCampusById"})
    public Result<?> getCampusById(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="campusId", required=true) Integer campusId)
    {
        Result result = interfaceUser(interfaceUserId, "/getCampusById");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseCampus veBaseCampus = (VeBaseCampus)this.veBaseCampusService.getById(campusId);
        return Result.ok(veBaseCampus);
    }

    @AutoLog("获取所有部门和部门下的用户信息-树形列表")
    @ApiOperation(value="获取所有部门和部门下的用户信息-树形列表", notes="获取所有部门和部门下的用户信息-树形列表")
    @GetMapping({"/queryOrganUserTreeList"})
    public Result<?> queryOrganUserTreeList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="realName", required=false) String realName)
    {
        Result result = interfaceUser(interfaceUserId, "/queryOrganUserTreeList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<Map<String, Object>> list = null;
        if ((!"".equals(realName)) && (realName != null))
        {
            list = this.veBaseAppUserService.queryOrganUserTreeList(realName);
        }
        else
        {
            boolean key = this.redisUtils.exists("queryOrganUserTreeList");
            if (key)
            {
                list = (List)this.redisUtils.get("queryOrganUserTreeList");
            }
            else
            {
                list = this.veBaseAppUserService.queryOrganUserTreeList(null);
                this.redisUtils.set("queryOrganUserTreeList", list);
            }
        }
        return Result.OK(list);
    }

    @AutoLog("更新机构树形列表缓存-base服务调用")
    @ApiOperation(value="更新机构树形列表缓存-base服务调用", notes="更新机构树形列表缓存-base服务调用")
    @GetMapping({"/updateOrganUserRedis"})
    public Result<?> updateOrganUserRedis()
    {
        List<Map<String, Object>> list = this.veBaseAppUserService.queryOrganUserTreeList(null);
        this.redisUtils.set("queryOrganUserTreeList", list);
        return Result.OK(list);
    }

    @AutoLog("获取所有班级信息")
    @ApiOperation(value="获取所有班级信息", notes="获取所有班级信息")
    @GetMapping({"/queryBanJiList"})
    public Result<?> queryBanJiList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryBanJiList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<VeBaseBanJi> list = this.banJiService.list();
        return Result.OK(list);
    }

    @AutoLog("根据搜索条件获取用户信息")
    @ApiOperation(value="根据搜索条件获取用户信息", notes="根据搜索条件获取用户信息")
    @GetMapping({"/getAppUserBySearch"})
    public Result<?> getAppUserBySearch(VoAppUserBySearch voAppUserBySearch)
    {
        Result result = interfaceUser(voAppUserBySearch.getInterfaceUserId(), "/getAppUserBySearch");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<Map<String, Object>> list = this.veBaseAppUserService.getAppUserBySearch(voAppUserBySearch);
        return Result.ok(list);
    }

    @AutoLog("用户信息-单个删除")
    @ApiOperation(value="用户信息-单个删除", notes="用户信息-单个删除")
    @PostMapping({"/deleteAppUser"})
    @Transactional
    public Result<?> deleteAppUser(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="userId", required=true) String userId)
    {
        Result result = interfaceUser(interfaceUserId, "/deleteAppUser");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        this.veBaseAppUserService.updateAppUserByUserId(userId);

        this.veBaseAppUserService.updateSysUserDel(userId);

        this.veBaseAppUserService.deleteOrgan(userId);

        this.veBaseAppUserService.deleteRole(userId);
        return Result.ok("删除成功!");
    }

    @AutoLog("获取所有接口用户信息")
    @ApiOperation(value="获取所有接口用户信息", notes="获取所有接口用户信息")
    @GetMapping({"/queryInterfaceUserIdList"})
    public Result<?> queryInterfaceUserIdList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryInterfaceUserIdList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<Map<String, Object>> list = this.veBaseInterfaceUserService.getInterfaceUserPageList(null);
        return Result.ok(list);
    }

    @AutoLog("根据userId获取数据申请信息列表")
    @ApiOperation(value="根据userId获取数据申请信息列表", notes="根据userId获取数据申请信息列表")
    @GetMapping({"/queryDataApplyUserId"})
    public Result<?> queryDataApplyUserId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="userId", required=true) String userId, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        Result result = interfaceUser(interfaceUserId, "/queryDataApplyUserId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseDataApplyService.queryDataApplyUserId(userId);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.ok(pageInfo);
    }

    @AutoLog("根据申请表id获取数据库表信息列表")
    @ApiOperation(value="根据申请表id获取数据库表信息列表", notes="根据申请表id获取数据库表信息列表")
    @GetMapping({"/queryCommonListByApplyId"})
    public Result<?> queryCommonListByApplyId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="applyId", required=true) String applyId, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        Result result = interfaceUser(interfaceUserId, "/queryCommonListByApplyId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseDataApply veBaseDataApply = (VeBaseDataApply)this.veBaseDataApplyService.getById(applyId);
        if ((veBaseDataApply != null) && (!"".equals(veBaseDataApply.getTableName())) && (veBaseDataApply.getTableName() != null))
        {
            PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
            List<Map<String, Object>> list = this.veBaseDataApplyService.queryCommonListByApplyId(veBaseDataApply);
            PageInfo<?> pageInfo = new PageInfo(list);
            return Result.ok(pageInfo);
        }
        return Result.ok(null);
    }

    @AutoLog("系统配置列表查询")
    @ApiOperation(value="系统配置列表查询", notes="系统配置列表查询")
    @GetMapping({"/querySysConfigList"})
    public Result<?> querySysConfigList(VeBaseSysConfig veBaseSysConfig)
    {
        Result result = interfaceUser(veBaseSysConfig.getInterfaceUserId(), "/querySysConfigList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<VeBaseSysConfig> list = this.veBaseSysConfigService.querySysConfigPageList(veBaseSysConfig);
        return Result.ok(list);
    }

    @AutoLog("获取用户表的所有教师用户信息")
    @ApiOperation(value="获取用户表的所有教师用户信息", notes="获取用户表的所有教师用户信息")
    @GetMapping({"/queryAppUserTeacherList"})
    public Result<?> queryAppUserTeacherList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryAppUserTeacherList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<Map<String, Object>> list = this.veBaseAppUserService.queryAppUserTeacherList();
        return Result.ok(list);
    }

    @AutoLog("根据班级id获取班主任信息")
    @ApiOperation(value="根据班级id获取班主任信息", notes="根据班级id获取班主任信息")
    @GetMapping({"/queryBzrByBanjiId"})
    public Result<?> queryBzrByBanjiId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="banjiId", required=true) Integer banjiId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryBzrByBanjiId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseTeacher veBaseTeacher = this.veBaseTeacherService.queryBzrByBanjiId(banjiId);
        if (veBaseTeacher != null)
        {
            veBaseTeacher.setSfzjh("******");
            veBaseTeacher.setLxdh("******");
        }
        return Result.OK(veBaseTeacher);
    }

    @AutoLog("根据userId禁用教师和账号信息")
    @ApiOperation(value="根据userId禁用教师和账号信息", notes="根据userId禁用教师和账号信息")
    @PostMapping({"/deleteAppUserAndTeacherByUserId"})
    public Result<?> deleteAppUserAndTeacherByUserId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="userId", required=true) String userId)
    {
        Result result = interfaceUser(interfaceUserId, "/deleteAppUserAndTeacherByUserId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        this.veBaseAppUserService.stopAppUserByUserId(userId);

        this.veBaseTeacherService.stopTeacherByUserId(userId);
        return Result.ok("禁用成功! ");
    }

    @AutoLog("添加学生信息")
    @ApiOperation(value="添加学生信息", notes="添加学生信息")
    @PostMapping({"/addStudent"})
    @Transactional
    public Result<?> addStudent(@RequestBody VoStudentAndStudentInfo voStudentAndStudentInfo)
    {
        Result result = interfaceUser(voStudentAndStudentInfo.getInterfaceUserId(), "/addStudent");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        if (("".equals(voStudentAndStudentInfo.getXh())) || (voStudentAndStudentInfo.getXh() == null)) {
            return Result.error("学号不能为空! ");
        }
        if (("".equals(voStudentAndStudentInfo.getSfzh())) || (voStudentAndStudentInfo.getSfzh() == null)) {
            return Result.error("身份证号不能为空! ");
        }
        if (("".equals(voStudentAndStudentInfo.getXm())) || (voStudentAndStudentInfo.getXm() == null)) {
            return Result.error("姓名不能为空! ");
        }
        if (("".equals(voStudentAndStudentInfo.getXbm())) || (voStudentAndStudentInfo.getXbm() == null)) {
            return Result.error("性别不能为空! ");
        }
        if (!"".equals(IdCardUtil.IdentityCardVerification(voStudentAndStudentInfo.getSfzh()))) {
            return Result.error("身份证号错误! ");
        }
        VeBaseStudent model = this.veBaseStudentService.getStudentBySFZH(null, voStudentAndStudentInfo.getSfzh());
        if (model != null)
        {
            VeBaseStudent model1 = this.veBaseStudentService.getStudentByXH(model.getId(), voStudentAndStudentInfo.getXh());
            if (model1 != null) {
                return Result.error("学号已存在! ");
            }
        }
        if ((!"".equals(voStudentAndStudentInfo.getXslxdh())) && (voStudentAndStudentInfo.getXslxdh() != null) && (!PhoneUtil.isMobile(voStudentAndStudentInfo.getXslxdh()))) {
            return Result.error("学生联系电话错误! ");
        }
        if ((!"".equals(voStudentAndStudentInfo.getJtlxdh())) && (voStudentAndStudentInfo.getJtlxdh() != null) && (!PhoneUtil.isMobile(voStudentAndStudentInfo.getJtlxdh()))) {
            return Result.error("家庭联系电话错误! ");
        }
        if ((!"".equals(voStudentAndStudentInfo.getDzxx())) && (voStudentAndStudentInfo.getDzxx() != null) && (!EmailUtil.isEmail(voStudentAndStudentInfo.getDzxx()))) {
            return Result.error("电子信箱错误! ");
        }
        VeBaseStudent veBaseStudent = new VeBaseStudent();
        veBaseStudent.setSfzh(voStudentAndStudentInfo.getSfzh());
        veBaseStudent.setXm(voStudentAndStudentInfo.getXm());
        veBaseStudent.setXbm(voStudentAndStudentInfo.getXbm());
        veBaseStudent.setXh(voStudentAndStudentInfo.getXh());
        veBaseStudent.setUserId(voStudentAndStudentInfo.getXh());
        veBaseStudent.setMzm(voStudentAndStudentInfo.getMzm());
        veBaseStudent.setJdfs(voStudentAndStudentInfo.getJdfs());
        veBaseStudent.setSfkns(voStudentAndStudentInfo.getSfkns());
        veBaseStudent.setFalId(voStudentAndStudentInfo.getFalId());
        veBaseStudent.setSpecId(voStudentAndStudentInfo.getSpecId());
        veBaseStudent.setBjId(voStudentAndStudentInfo.getBjId());
        veBaseStudent.setXz(voStudentAndStudentInfo.getXz());
        veBaseStudent.setBmh(voStudentAndStudentInfo.getBmh());
        if ((!"".equals(voStudentAndStudentInfo.getBjId())) && (voStudentAndStudentInfo.getBjId() != null))
        {
            VeBaseBanJi veBaseBanJi = (VeBaseBanJi)this.banJiService.getById(voStudentAndStudentInfo.getBjId());
            if (veBaseBanJi != null)
            {
                veBaseStudent.setGradeId(veBaseBanJi.getGradeId());
                if (model != null) {
                    if (model.getXbm().equals("1")) {
                        veBaseBanJi.setNansrs(Integer.valueOf(veBaseBanJi.getNansrs().intValue() - 1));
                    } else {
                        veBaseBanJi.setNvsrs(Integer.valueOf(veBaseBanJi.getNvsrs().intValue() - 1));
                    }
                }
                if (veBaseStudent.getXbm().equals("1")) {
                    veBaseBanJi.setNansrs(Integer.valueOf(veBaseBanJi.getNansrs().intValue() + 1));
                } else {
                    veBaseBanJi.setNvsrs(Integer.valueOf(veBaseBanJi.getNvsrs().intValue() + 1));
                }
                this.banJiService.updateById(veBaseBanJi);
            }
        }
        if ((!"".equals(voStudentAndStudentInfo.getRxnyName())) && (voStudentAndStudentInfo.getRxnyName() != null))
        {
            Long td = Long.valueOf(DateTimeUtil.dateToTimestamp(voStudentAndStudentInfo.getRxnyName()));
            veBaseStudent.setRxny(td);
        }
        if ((!"".equals(voStudentAndStudentInfo.getProvinceId())) && (voStudentAndStudentInfo.getProvinceId() != null))
        {
            veBaseStudent.setProvinceId(voStudentAndStudentInfo.getProvinceId());
            VeBaseDictArea veBaseDictArea = (VeBaseDictArea)this.veBaseDictAreaService.getById(voStudentAndStudentInfo.getProvinceId());
            if (veBaseDictArea != null) {
                veBaseStudent.setProvince(veBaseDictArea.getName());
            }
        }
        if ((!"".equals(voStudentAndStudentInfo.getCityId())) && (voStudentAndStudentInfo.getCityId() != null))
        {
            veBaseStudent.setCityId(voStudentAndStudentInfo.getCityId());
            VeBaseDictArea veBaseDictArea = (VeBaseDictArea)this.veBaseDictAreaService.getById(voStudentAndStudentInfo.getCityId());
            if (veBaseDictArea != null) {
                veBaseStudent.setCity(veBaseDictArea.getName());
            }
        }
        if ((!"".equals(voStudentAndStudentInfo.getCountyId())) && (voStudentAndStudentInfo.getCountyId() != null))
        {
            veBaseStudent.setCountyId(voStudentAndStudentInfo.getCountyId());
            VeBaseDictArea veBaseDictArea = (VeBaseDictArea)this.veBaseDictAreaService.getById(voStudentAndStudentInfo.getCountyId());
            if (veBaseDictArea != null) {
                veBaseStudent.setCounty(veBaseDictArea.getName());
            }
        }
        Long times = Long.valueOf(System.currentTimeMillis() / 1000L);
        veBaseStudent.setUpdateTime(Integer.valueOf(times.intValue()));

        VeBaseStudentInfo veBaseStudentInfo = new VeBaseStudentInfo();

        veBaseStudentInfo.setCym(voStudentAndStudentInfo.getCym());
        if ((!"".equals(voStudentAndStudentInfo.getCsrqName())) && (voStudentAndStudentInfo.getCsrqName() != null))
        {
            Long td = Long.valueOf(DateTimeUtil.dateToTimestamp(voStudentAndStudentInfo.getCsrqName()));
            veBaseStudentInfo.setCsrq(Integer.valueOf(td.intValue()));
        }
        veBaseStudentInfo.setJg(voStudentAndStudentInfo.getJg());
        veBaseStudentInfo.setZzmmm(voStudentAndStudentInfo.getZzmmm());
        veBaseStudentInfo.setZp(voStudentAndStudentInfo.getZp());
        veBaseStudentInfo.setBmfsm(voStudentAndStudentInfo.getBmfsm());
        veBaseStudentInfo.setByxx(voStudentAndStudentInfo.getByxx());
        veBaseStudentInfo.setRxcj(voStudentAndStudentInfo.getRxcj());
        veBaseStudentInfo.setXslxdh(voStudentAndStudentInfo.getXslxdh());
        veBaseStudentInfo.setJtlxdh(voStudentAndStudentInfo.getJtlxdh());
        veBaseStudentInfo.setDzxx(voStudentAndStudentInfo.getDzxx());
        veBaseStudentInfo.setJtdz(voStudentAndStudentInfo.getJtdz());
        veBaseStudentInfo.setJkzkm(voStudentAndStudentInfo.getJkzkm());
        veBaseStudentInfo.setHklbm(voStudentAndStudentInfo.getHklbm());
        veBaseStudentInfo.setTc(voStudentAndStudentInfo.getTc());
        veBaseStudentInfo.setSfsldrk(voStudentAndStudentInfo.getSfsldrk());
        veBaseStudentInfo.setSfdb(voStudentAndStudentInfo.getSfdb());

        veBaseStudentInfo.setUpdateTime(Integer.valueOf(times.intValue()));
        if (model == null)
        {
            veBaseStudent.setCreateTime(Integer.valueOf(times.intValue()));
            this.veBaseStudentService.save(veBaseStudent);
            veBaseStudentInfo.setStuId(veBaseStudent.getId());
            this.veBaseStudentInfoService.save(veBaseStudentInfo);
            VeBaseAppUser veBaseAppUser = new VeBaseAppUser();
            veBaseAppUser.setId(UUID.randomUUID().toString().replace("-", ""));
            veBaseAppUser.setUserId(veBaseStudent.getXh());
            veBaseAppUser.setUserName(veBaseStudent.getXh());
            veBaseAppUser.setRealName(veBaseStudent.getXm());
            veBaseAppUser.setUserType("2");
            veBaseAppUser.setStatus("0");
            veBaseAppUser.setIsDel("0");
            veBaseAppUser.setUserPassword(veBaseStudent.getSfzh().substring(12, 18));
            veBaseAppUser.setStrength(this.veBaseAppUserService.pwdVerify(veBaseAppUser.getUserPassword()));
            VeBaseAppUser vemodel = new VeBaseAppUser();
            vemodel = this.veBaseAppUserService.getPwdAndSalt(veBaseAppUser);
            this.veBaseAppUserService.save(vemodel);
            this.veBaseAppUserService.addSysUser(vemodel);

            this.veBaseAppUserService.addSysRoleUser(vemodel.getUserId(), Integer.valueOf(1));
        }
        else
        {
            veBaseStudent.setId(model.getId());
            this.veBaseStudentService.updateById(veBaseStudent);
            veBaseStudentInfo.setStuId(veBaseStudent.getId());
            this.veBaseStudentInfoService.updateById(veBaseStudentInfo);
        }
        return Result.ok("操作成功!");
    }

    @AutoLog("编辑学生信息-毕业照修改")
    @ApiOperation(value="编辑学生信息-毕业照修改", notes="编辑学生信息-毕业照修改")
    @PostMapping({"/editStudentInfo"})
    public Result<?> editStudentInfo(@RequestBody VoStudentAndStudentInfo voStudentAndStudentInfo)
    {
        Result result = interfaceUser(voStudentAndStudentInfo.getInterfaceUserId(), "/editStudentInfo");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseStudentInfo veBaseStudentInfo = this.veBaseStudentInfoService.getModelById(voStudentAndStudentInfo.getId());
        veBaseStudentInfo.setByzp(voStudentAndStudentInfo.getByzp());
        this.veBaseStudentInfoService.updateById(veBaseStudentInfo);
        return Result.ok("修改成功!");
    }

    @AutoLog("获取专业组信息列表")
    @ApiOperation(value="获取专业组信息列表", notes="获取专业组信息列表")
    @GetMapping({"/queryJYZList"})
    public Result<?> queryJYZList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryJYZList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<VeBaseJYZ> list = this.veBaseJYZService.list();
        return Result.ok(list);
    }

    @AutoLog("获取最近的学期(id倒序)")
    @ApiOperation(value="获取最近的学期(id倒序)", notes="获取最近的学期(id倒序)")
    @GetMapping({"/getSemesterNew"})
    public Result<?> getSemesterNew(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/getSemesterNew");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseSemester veBaseSemester = this.veBaseSemesterService.getSemesterNew();
        return Result.ok(veBaseSemester);
    }

    @AutoLog("查询应用系统信息列表")
    @ApiOperation(value="查询应用系统信息列表", notes="查询应用系统信息列表")
    @GetMapping({"/queryAppManageList"})
    public Result<?> queryAppManageList(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryAppManageList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<Map<String, Object>> list = this.veBaseAppUserService.queryAppManageList();
        return Result.ok(list);
    }

    @AutoLog("根据userId查询铃铛的未读消息数")
    @ApiOperation(value="根据userId查询铃铛的未读消息数", notes="根据userId查询铃铛的未读消息数")
    @GetMapping({"/getUnreadMessageCountByUserId"})
    public Result<?> getUnreadMessageCountByUserId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="userId", required=true) String userId)
    {
        Result result = interfaceUser(interfaceUserId, "/getUnreadMessageCountByUserId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        int msgCount = 0;

        boolean key = this.redisUtils.exists("getUnreadMessageCountByUserId");
        List<Map<String, Object>> list = null;
        if (key)
        {
            list = (List)this.redisUtils.get("getUnreadMessageCountByUserId");
        }
        else
        {
            list = this.veBaseAppUserService.getUnreadMessageCountUserId();
            this.redisUtils.set("getUnreadMessageCountByUserId", list);
        }
        for (Map map : list) {
            if ((!"".equals(map.get("sendTo"))) && (map.get("sendTo") != null) && (userId.equals(map.get("sendTo")))) {
                msgCount = Integer.parseInt(map.get("num").toString());
            }
        }
        return Result.OK(Integer.valueOf(msgCount));
    }

    @AutoLog("根据校区id查询楼栋信息列表")
    @ApiOperation(value="根据校区id查询楼栋信息列表", notes="根据校区id查询楼栋信息列表")
    @GetMapping({"/queryBuildListByCampusId"})
    public Result<?> queryBuildListByCampusId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="campusId", required=true) Integer campusId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryBuildListByCampusId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<Map<String, Object>> list = this.veBaseBuildService.queryJianzhuByCampusId(campusId);
        return Result.OK(list);
    }

    @AutoLog("根据楼栋id查询教室信息列表")
    @ApiOperation(value="根据楼栋id查询教室信息列表", notes="根据楼栋id查询教室信息列表")
    @GetMapping({"/queryRoomListByBuildId"})
    public Result<?> queryRoomListByBuildId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="buildId", required=true) Integer buildId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryRoomListByBuildId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<Map<String, Object>> list = this.veBaseBuildService.queryRoomListByBuildId(buildId);
        return Result.OK(list);
    }

    @AutoLog("根据角色id获取用户信息列表")
    @ApiOperation(value="根据角色id获取用户信息列表", notes="根据角色id获取用户信息列表")
    @GetMapping({"/queryAppUserListByRoleId"})
    public Result<?> queryAppUserListByRoleId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="roleId", required=true) Integer roleId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryAppUserListByRoleId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<Map<String, Object>> list = this.veBaseAppUserService.queryAppUserListByRoleId(roleId);
        return Result.ok(list);
    }

    @AutoLog("根据日期获取当天的校历信息")
    @ApiOperation(value="根据日期获取当天的校历信息", notes="根据日期获取当天的校历信息")
    @GetMapping({"/getCalenderByDates"})
    public Result<?> getCalenderByDates(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="dates", required=true) String dates)
    {
        Result result = interfaceUser(interfaceUserId, "/getCalenderByDates");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseCalendar calendar = this.veBaseCalendarService.getCalenderByDates(dates);
        return Result.ok(calendar);
    }

    @AutoLog("统计各院系专业的已毕业人数")
    @ApiOperation(value="统计各院系专业的已毕业人数", notes="统计各院系专业的已毕业人数")
    @GetMapping({"/getGraduatePeopleCount"})
    public Result<?> getGraduatePeopleCount(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="specName", required=false) String specName)
    {
        Result result = interfaceUser(interfaceUserId, "/getGraduatePeopleCount");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<Map<String, Object>> list = this.specialtyService.getGraduatePeopleCount(specName);
        return Result.ok(list);
    }

    @AutoLog("班主任日志分页列表查询")
    @ApiOperation(value="班主任日志分页列表查询", notes="班主任日志分页列表查询")
    @GetMapping({"/getBanJiBzrPageList"})
    public Result<?> getBanJiBzrPageList(VeBaseBanJiBzr veBaseBanJiBzr, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        Result result = interfaceUser(veBaseBanJiBzr.getInterfaceUserId(), "/getBanJiBzrPageList");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        Page<VeBaseBanJiBzr> page = new Page(pageNo.intValue(), pageSize.intValue());
        IPage<VeBaseBanJiBzr> pageList = this.veBaseBanJiBzrService.getBanJiBzrPageList(page, veBaseBanJiBzr);
        return Result.ok(pageList);
    }

    @AutoLog("根据userId新增角色信息")
    @ApiOperation(value="根据userId新增角色信息", notes="根据userId新增角色信息")
    @PostMapping({"/addUserRole"})
    public Result<?> addUserRole(@RequestParam(name="interfaceUserId") String interfaceUserId, @RequestParam(name="userId") String userId, @RequestParam(name="roleId") Integer roleId)
    {
        Result result = interfaceUser(interfaceUserId, "/addUserRole");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        Map map = this.veBaseAppUserService.getSysRoleUser(userId, roleId);
        if ((map != null) && (map.size() > 0)) {
            return Result.error("该用户角色已存在! ");
        }
        this.veBaseAppUserService.addSysRoleUser(userId, roleId);
        return Result.ok();
    }

    @AutoLog("编辑教师信息")
    @ApiOperation(value="编辑教师信息", notes="编辑教师信息")
    @PostMapping({"/editTeacher"})
    public Result<?> editTeacher(@RequestBody VeBaseTeacher veBaseTeacher)
    {
        Result result = interfaceUser(veBaseTeacher.getInterfaceUserId(), "/editTeacher");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        if (("".equals(veBaseTeacher.getGh())) || (veBaseTeacher.getGh() == null)) {
            return Result.error("工号不能为空! ");
        }
        if (("".equals(veBaseTeacher.getXm())) || (veBaseTeacher.getXm() == null)) {
            return Result.error("姓名不能为空! ");
        }
        if (("".equals(veBaseTeacher.getSfzjh())) || (veBaseTeacher.getSfzjh() == null)) {
            return Result.error("身份证号不能为空! ");
        }
        if (!"".equals(IdCardUtil.IdentityCardVerification(veBaseTeacher.getSfzjh()))) {
            return Result.error("身份证号错误! ");
        }
        VeBaseTeacher model = this.veBaseTeacherService.getByGH(veBaseTeacher.getId(), veBaseTeacher.getGh());
        if (model != null) {
            return Result.error("该工号已存在! ");
        }
        if ((!"".equals(veBaseTeacher.getLxdh())) && (veBaseTeacher.getLxdh() != null) && (!PhoneUtil.isMobile(veBaseTeacher.getLxdh()))) {
            return Result.error("联系电话错误! ");
        }
        if ((!"".equals(veBaseTeacher.getDzxx())) && (veBaseTeacher.getDzxx() != null) && (!EmailUtil.isEmail(veBaseTeacher.getDzxx()))) {
            return Result.error("电子信箱错误! ");
        }
        if ((!"".equals(veBaseTeacher.getCsrqName())) && (veBaseTeacher.getCsrqName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseTeacher.getCsrqName()));
            veBaseTeacher.setCsrq(Integer.valueOf(times.intValue()));
        }
        if ((!"".equals(veBaseTeacher.getCjgznyName())) && (veBaseTeacher.getCjgznyName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseTeacher.getCjgznyName()));
            veBaseTeacher.setCjgzny(Integer.valueOf(times.intValue()));
        }
        if ((!"".equals(veBaseTeacher.getCjnyName())) && (veBaseTeacher.getCjnyName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseTeacher.getCjnyName()));
            veBaseTeacher.setCjny(Integer.valueOf(times.intValue()));
        }
        if ((!"".equals(veBaseTeacher.getLxnyName())) && (veBaseTeacher.getLxnyName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseTeacher.getLxnyName()));
            veBaseTeacher.setLxny(Integer.valueOf(times.intValue()));
        }
        if ((!"".equals(veBaseTeacher.getRdName())) && (veBaseTeacher.getRdName() != null))
        {
            Long times = Long.valueOf(DateTimeUtil.dateToTimestamp(veBaseTeacher.getRdName()));
            veBaseTeacher.setRdDate(Integer.valueOf(times.intValue()));
        }
        veBaseTeacher.setUserId(veBaseTeacher.getGh());
        this.veBaseTeacherService.updateById(veBaseTeacher);

        this.redisUtils.set("queryDepartmentAndTeacherList", this.departmentService.getDepartmentAndTeacherList());
        return Result.ok("修改成功!");
    }

    @AutoLog("教师信息批量删除")
    @ApiOperation(value="教师信息批量删除", notes="教师信息批量删除")
    @PostMapping({"/deleteTeacherBatch"})
    public Result<?> deleteTeacherBatch(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="ids", required=true) String ids)
    {
        Result result = interfaceUser(interfaceUserId, "/deleteTeacherBatch");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        String[] strings = ids.split(",");

        List<VeBaseTeacher> list = new ArrayList();

        List<VeBaseAppUser> detailList = new ArrayList();
        for (String s : strings)
        {
            VeBaseTeacher veBaseTeacher = (VeBaseTeacher)this.veBaseTeacherService.getById(s);
            veBaseTeacher.setIsDeleted(Integer.valueOf(-1));
            veBaseTeacher.setStatus(Integer.valueOf(2));
            list.add(veBaseTeacher);
            VeBaseAppUser veBaseAppUser = this.veBaseAppUserService.getAppUserByUserId(veBaseTeacher.getUserId());
            veBaseAppUser.setStatus("1");
            veBaseAppUser.setIsDel("1");
            detailList.add(veBaseAppUser);
        }
        this.veBaseTeacherService.updateBatchById(list);
        this.veBaseAppUserService.updateBatchById(detailList);

        this.redisUtils.set("queryDepartmentAndTeacherList", this.departmentService.getDepartmentAndTeacherList());

        return Result.ok("删除成功!");
    }

    @AutoLog("根据学年统计贫困的学生")
    @ApiOperation(value="根据学年统计贫困的学生", notes="根据学年统计贫困的学生")
    @GetMapping({"/getStudentStatisticsByYear"})
    public Result<?> getStudentStatisticsByYear(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="year", required=false) String year)
    {
        Result result = interfaceUser(interfaceUserId, "/getStudentStatisticsByYear");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        List<Map<String, Object>> list = this.veBaseStudentService.getStudentStatisticsByYear(year);
        return Result.ok(list);
    }

    @AutoLog("根据班级id修改男女生人数")
    @ApiOperation(value="根据班级id修改男女生人数", notes="根据班级id修改男女生人数")
    @PostMapping({"/updateBjNanNvRSByBjId"})
    public Result<?> updateBjNanNvRSByBjId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="bjId", required=true) Integer bjId, @RequestParam(name="nanSRS", required=true) Integer nanSRS, @RequestParam(name="nvSRS", required=true) Integer nvSRS)
    {
        Result result = interfaceUser(interfaceUserId, "/updateBjNanNvRSByBjId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        VeBaseBanJi veBaseBanJi = (VeBaseBanJi)this.banJiService.getById(bjId);
        if (veBaseBanJi != null)
        {
            veBaseBanJi.setNansrs(nanSRS);
            veBaseBanJi.setNvsrs(nvSRS);
            this.banJiService.updateById(veBaseBanJi);
        }
        return Result.ok("修改成功! ");
    }

    @AutoLog("根据班主任userId查询班级信息列表")
    @ApiOperation(value="根据班主任userId查询班级信息列表", notes="根据班主任userId查询班级信息列表")
    @GetMapping({"/queryBjListByBzrUserId"})
    public Result<?> queryBjListByBzrUserId(@RequestParam(name="interfaceUserId", required=true) String interfaceUserId, @RequestParam(name="bzrUserId", required=true) String bzrUserId)
    {
        Result result = interfaceUser(interfaceUserId, "/queryBjListByBzrUserId");
        if ((!"".equals(result.getMessage())) && (result.getMessage() != null)) {
            return result;
        }
        QueryWrapper<VeBaseBanJi> queryWrapper = new QueryWrapper();
        queryWrapper.eq("bzr_user_id", bzrUserId);
        List<VeBaseBanJi> list = this.banJiService.list(queryWrapper);
        return Result.OK(list);
    }

    @AutoLog("数据转换配置")
    @ApiOperation(value="数据转换配置", notes="数据转换配置")
    @GetMapping({"/convertDataKettle"})
    public Result<?> convertDataKettle(VoKettle voKettle)
            throws Exception
    {
        return Result.ok("转换成功! ");
    }
}
