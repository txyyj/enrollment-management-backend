package org.edu.modules.common.controller;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.modules.common.entity.VeBaseTransformation;
import org.edu.modules.common.entity.VeBaseUserLog;
import org.edu.modules.common.feign.EduBootBaseFeignInterface;
import org.edu.modules.common.service.IVeBaseTransformationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags={"数据采集"})
@RestController
@RequestMapping({"/common/veCollection"})
@ApiSort(60)
public class VeDataCollection
{
    private static final Logger log = LoggerFactory.getLogger(VeDataCollection.class);
    @Autowired
    private IVeBaseTransformationService veBaseTransformationService;
    @Autowired
    private EduBootBaseFeignInterface eduBootBaseFeginInterface;

    @AutoLog("数据采集任务-分页列表查询")
    @ApiOperation(value="数据采集任务-分页列表查询", notes="数据采集任务-分页列表查询")
    @GetMapping({"/getTransformationPageList"})
    public Result<?> getTransformationPageList(VeBaseTransformation veBaseTransformation, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseTransformationService.getTransformationPageList(veBaseTransformation);
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.ok(pageInfo);
    }

    @AutoLog("系统日志信息-分页列表查询")
    @ApiOperation(value="系统日志-分页列表查询", notes="系统日志-分页列表查询")
    @GetMapping({"/userLogPageList"})
    public Result<?> userLogPageList(VeBaseUserLog userLog, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        return this.eduBootBaseFeginInterface.userLogPageList(userLog, pageNo, pageSize);
    }

    @AutoLog("菜单信息查询")
    @ApiOperation(value="菜单信息查询", notes="菜单信息查询")
    @GetMapping({"/roleMenuList"})
    public Result<?> roleMenuList()
    {
        Result list = this.eduBootBaseFeginInterface.roleMenuList();
        return Result.ok(list);
    }

    @AutoLog("通过id查询系统日志信息")
    @ApiOperation(value="通过id查询系统日志信息", notes="通过id查询系统日志信息")
    @GetMapping({"/userLogById"})
    public Result<?> userLogById(@RequestParam(name="id", required=true) String id)
    {
        Result userLog = this.eduBootBaseFeginInterface.userLogById(id);
        return Result.OK(userLog);
    }
}
