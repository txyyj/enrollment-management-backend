package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import org.edu.modules.common.entity.VeBaseFestival;
import org.edu.modules.common.mapper.VeBaseFestivalMapper;
import org.edu.modules.common.service.IVeBaseFestivalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseFestivalServiceImpl
        extends ServiceImpl<VeBaseFestivalMapper, VeBaseFestival>
        implements IVeBaseFestivalService
{
    @Autowired
    private VeBaseFestivalMapper veBaseFestivalMapper;

    public List<VeBaseFestival> getFestivalBySemId(Integer semId)
    {
        return this.veBaseFestivalMapper.getFestivalBySemId(semId);
    }
}
