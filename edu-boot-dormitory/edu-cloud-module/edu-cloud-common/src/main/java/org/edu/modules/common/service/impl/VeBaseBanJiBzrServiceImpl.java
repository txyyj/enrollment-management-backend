package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.common.entity.VeBaseBanJiBzr;
import org.edu.modules.common.mapper.VeBaseBanJiBzrMapper;
import org.edu.modules.common.service.IVeBaseBanJiBzrService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseBanJiBzrServiceImpl
        extends ServiceImpl<VeBaseBanJiBzrMapper, VeBaseBanJiBzr>
        implements IVeBaseBanJiBzrService
{
    @Autowired
    private VeBaseBanJiBzrMapper veBaseBanJiBzrMapper;

    public VeBaseBanJiBzr getByBjIdAndBzrUserId(Integer bjId, String bzrUserId)
    {
        return this.veBaseBanJiBzrMapper.getByBjIdAndBzrUserId(bjId, bzrUserId);
    }

    public IPage<VeBaseBanJiBzr> getBanJiBzrPageList(Page page, VeBaseBanJiBzr veBaseBanJiBzr)
    {
        return this.veBaseBanJiBzrMapper.getBanJiBzrPageList(page, veBaseBanJiBzr);
    }
}
