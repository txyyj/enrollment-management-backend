package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseInterfaceUser;
import org.edu.modules.common.mapper.VeBaseInterfaceUserMapper;
import org.edu.modules.common.service.IVeBaseInterfaceUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseInterfaceUserServiceImpl
        extends ServiceImpl<VeBaseInterfaceUserMapper, VeBaseInterfaceUser>
        implements IVeBaseInterfaceUserService
{
    @Autowired
    private VeBaseInterfaceUserMapper veBaseInterfaceUserMapper;

    public List<Map<String, Object>> getInterfaceUserPageList(VeBaseInterfaceUser veBaseInterfaceUser)
    {
        List<Map<String, Object>> list = this.veBaseInterfaceUserMapper.getInterfaceUserPageList(veBaseInterfaceUser);
        if (list.size() > 0) {
            for (Map map : list)
            {
                List<Map<String, Object>> detailList = this.veBaseInterfaceUserMapper.getInterfaceListByUserId(map.get("id").toString());
                map.put("detailList", detailList);
            }
        }
        return list;
    }

    public List<Map<String, Object>> getInterfaceUserByNameAndPwd(String id, String interfaceName)
    {
        return this.veBaseInterfaceUserMapper.getInterfaceUserByNameAndPwd(id, interfaceName);
    }

    public List<Map<String, Object>> getListByName(String name)
    {
        return this.veBaseInterfaceUserMapper.getListByName(name);
    }

    public List<Map<String, Object>> getInterfaceUserByIdAndInterfaceName(String id, String interfaceName)
    {
        return this.veBaseInterfaceUserMapper.getInterfaceUserByIdAndInterfaceName(id, interfaceName);
    }

    public int deleteInterfaceUserRelById(String interfaceUserId)
    {
        return this.veBaseInterfaceUserMapper.deleteInterfaceUserRelById(interfaceUserId);
    }

    public int addInterfaceUserRelBatch(String interfaceUserId, String[] interfaceIds, String userId)
    {
        return this.veBaseInterfaceUserMapper.addInterfaceUserRelBatch(interfaceUserId, interfaceIds, userId);
    }
}

