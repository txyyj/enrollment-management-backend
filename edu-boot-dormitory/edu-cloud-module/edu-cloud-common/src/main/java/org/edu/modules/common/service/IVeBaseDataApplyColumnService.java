package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseDataApplyColumn;

public abstract interface IVeBaseDataApplyColumnService
        extends IService<VeBaseDataApplyColumn>
{
    public abstract int addDataApplyColumn(String paramString1, String paramString2, List<Map<String, Object>> paramList);

    public abstract int deleteDataApplyColumnByApplyId(String paramString);
}

