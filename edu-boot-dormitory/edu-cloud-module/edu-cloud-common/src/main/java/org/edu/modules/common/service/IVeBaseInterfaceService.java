package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseInterface;

public abstract interface IVeBaseInterfaceService
        extends IService<VeBaseInterface>
{
    public abstract List<Map<String, Object>> getInterfacePageList(VeBaseInterface paramVeBaseInterface);

    public abstract List<Map<String, Object>> getInterfaceList();
}

