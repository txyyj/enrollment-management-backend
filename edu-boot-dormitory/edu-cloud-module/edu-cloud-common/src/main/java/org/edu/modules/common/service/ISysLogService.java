package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.common.entity.SysLog;

public abstract interface ISysLogService
        extends IService<SysLog>
{
    public abstract List<SysLog> getSysLogPageList(SysLog paramSysLog);
}

