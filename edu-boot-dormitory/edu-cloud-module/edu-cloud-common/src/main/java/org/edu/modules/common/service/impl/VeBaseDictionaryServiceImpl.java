package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.common.entity.VeBaseDictionary;
import org.edu.modules.common.mapper.VeBaseDictionaryMapper;
import org.edu.modules.common.service.IVeBaseDictionaryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Auther 李少君
 * @Date 2021-12-13 16:24
 */
@Service
public class VeBaseDictionaryServiceImpl extends ServiceImpl<VeBaseDictionaryMapper, VeBaseDictionary> implements IVeBaseDictionaryService {

    @Autowired
    private VeBaseDictionaryMapper veBaseDictionaryMapper;

    @Override
    public List<VeBaseDictionary> getDictionaryListByCode(String paramString) {
        return veBaseDictionaryMapper.getDictionaryListByCode(paramString);
    }

    @Override
    public List<Map<String, Object>> getDictDataListByCode(String paramString) {
        return veBaseDictionaryMapper.getDictDataListByCode(paramString);
    }

    @Override
    public List<VeBaseDictionary> getTreeList() {
        return null;
    }
}
