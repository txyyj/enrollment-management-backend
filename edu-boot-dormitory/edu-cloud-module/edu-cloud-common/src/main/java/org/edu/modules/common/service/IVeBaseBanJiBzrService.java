package org.edu.modules.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.common.entity.VeBaseBanJiBzr;

public abstract interface IVeBaseBanJiBzrService
        extends IService<VeBaseBanJiBzr>
{
    public abstract VeBaseBanJiBzr getByBjIdAndBzrUserId(Integer paramInteger, String paramString);

    public abstract IPage<VeBaseBanJiBzr> getBanJiBzrPageList(Page paramPage, VeBaseBanJiBzr paramVeBaseBanJiBzr);
}
