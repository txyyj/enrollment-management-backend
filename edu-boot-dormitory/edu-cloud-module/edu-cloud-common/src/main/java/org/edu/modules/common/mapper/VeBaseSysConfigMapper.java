package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.edu.modules.common.entity.VeBaseSysConfig;

public abstract interface VeBaseSysConfigMapper
        extends BaseMapper<VeBaseSysConfig>
{
    public abstract List<VeBaseSysConfig> querySysConfigPageList(VeBaseSysConfig paramVeBaseSysConfig);
}
