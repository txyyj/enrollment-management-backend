package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_banji_bzr")
@ApiModel(value="ve_base_banji_bzr对象", description="班主任日志信息")
public class VeBaseBanJiBzr
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="班级id", width=15.0D)
    @ApiModelProperty("班级id")
    @TableField("bjId")
    private Integer bjId;
    @Excel(name="班主任id", width=15.0D)
    @ApiModelProperty("班主任id")
    @TableField("bzrUserId")
    private String bzrUserId;
    @Excel(name="班主任名称", width=15.0D)
    @ApiModelProperty("班主任名称")
    @TableField("bzrUserName")
    private String bzrUserName;
    @Excel(name="设置时间", width=15.0D)
    @ApiModelProperty("设置时间")
    @TableField("createTime")
    private Integer createTime;
    @Excel(name="终端id", width=15.0D)
    @ApiModelProperty("终端id")
    @TableField("terminalId")
    private Integer terminalId;
    @Excel(name="是否当前", width=15.0D)
    @ApiModelProperty("是否当前0是1否")
    @TableField("isCurrent")
    private Integer isCurrent;
    @Excel(name="创建用户", width=15.0D)
    @ApiModelProperty("创建用户")
    @TableField("createUserId")
    private String createUserId;
    @Excel(name="任职开始时间", width=15.0D)
    @ApiModelProperty("任职开始时间")
    @TableField("startDate")
    private Integer startDate;
    @Excel(name="任职结束时间", width=15.0D)
    @ApiModelProperty("任职结束时间")
    @TableField("endDate")
    private Integer endDate;
    @TableField(exist=false)
    private String xzbmc;
    @TableField(exist=false)
    private String njmc;
    @TableField(exist=false)
    private Integer nansrs;
    @TableField(exist=false)
    private Integer nvsrs;
    @TableField(exist=false)
    private String zymc;
    @TableField(exist=false)
    private String interfaceUserId;

    public VeBaseBanJiBzr setBzrUserId(String bzrUserId)
    {
        this.bzrUserId = bzrUserId;return this;
    }

    public VeBaseBanJiBzr setBjId(Integer bjId)
    {
        this.bjId = bjId;return this;
    }

    public VeBaseBanJiBzr setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseBanJiBzr(id=" + getId() + ", bjId=" + getBjId() + ", bzrUserId=" + getBzrUserId() + ", bzrUserName=" + getBzrUserName() + ", createTime=" + getCreateTime() + ", terminalId=" + getTerminalId() + ", isCurrent=" + getIsCurrent() + ", createUserId=" + getCreateUserId() + ", startDate=" + getStartDate() + ", endDate=" + getEndDate() + ", xzbmc=" + getXzbmc() + ", njmc=" + getNjmc() + ", nansrs=" + getNansrs() + ", nvsrs=" + getNvsrs() + ", zymc=" + getZymc() + ", interfaceUserId=" + getInterfaceUserId() + ")";
    }

    public VeBaseBanJiBzr setInterfaceUserId(String interfaceUserId)
    {
        this.interfaceUserId = interfaceUserId;return this;
    }

    public VeBaseBanJiBzr setZymc(String zymc)
    {
        this.zymc = zymc;return this;
    }

    public VeBaseBanJiBzr setNvsrs(Integer nvsrs)
    {
        this.nvsrs = nvsrs;return this;
    }

    public VeBaseBanJiBzr setNansrs(Integer nansrs)
    {
        this.nansrs = nansrs;return this;
    }

    public VeBaseBanJiBzr setNjmc(String njmc)
    {
        this.njmc = njmc;return this;
    }

    public VeBaseBanJiBzr setXzbmc(String xzbmc)
    {
        this.xzbmc = xzbmc;return this;
    }

    public VeBaseBanJiBzr setEndDate(Integer endDate)
    {
        this.endDate = endDate;return this;
    }

    public VeBaseBanJiBzr setStartDate(Integer startDate)
    {
        this.startDate = startDate;return this;
    }

    public VeBaseBanJiBzr setCreateUserId(String createUserId)
    {
        this.createUserId = createUserId;return this;
    }

    public VeBaseBanJiBzr setIsCurrent(Integer isCurrent)
    {
        this.isCurrent = isCurrent;return this;
    }

    public VeBaseBanJiBzr setTerminalId(Integer terminalId)
    {
        this.terminalId = terminalId;return this;
    }

    public VeBaseBanJiBzr setCreateTime(Integer createTime)
    {
        this.createTime = createTime;return this;
    }

    public VeBaseBanJiBzr setBzrUserName(String bzrUserName)
    {
        this.bzrUserName = bzrUserName;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $bjId = getBjId();result = result * 59 + ($bjId == null ? 43 : $bjId.hashCode());Object $createTime = getCreateTime();result = result * 59 + ($createTime == null ? 43 : $createTime.hashCode());Object $terminalId = getTerminalId();result = result * 59 + ($terminalId == null ? 43 : $terminalId.hashCode());Object $isCurrent = getIsCurrent();result = result * 59 + ($isCurrent == null ? 43 : $isCurrent.hashCode());Object $startDate = getStartDate();result = result * 59 + ($startDate == null ? 43 : $startDate.hashCode());Object $endDate = getEndDate();result = result * 59 + ($endDate == null ? 43 : $endDate.hashCode());Object $nansrs = getNansrs();result = result * 59 + ($nansrs == null ? 43 : $nansrs.hashCode());Object $nvsrs = getNvsrs();result = result * 59 + ($nvsrs == null ? 43 : $nvsrs.hashCode());Object $bzrUserId = getBzrUserId();result = result * 59 + ($bzrUserId == null ? 43 : $bzrUserId.hashCode());Object $bzrUserName = getBzrUserName();result = result * 59 + ($bzrUserName == null ? 43 : $bzrUserName.hashCode());Object $createUserId = getCreateUserId();result = result * 59 + ($createUserId == null ? 43 : $createUserId.hashCode());Object $xzbmc = getXzbmc();result = result * 59 + ($xzbmc == null ? 43 : $xzbmc.hashCode());Object $njmc = getNjmc();result = result * 59 + ($njmc == null ? 43 : $njmc.hashCode());Object $zymc = getZymc();result = result * 59 + ($zymc == null ? 43 : $zymc.hashCode());Object $interfaceUserId = getInterfaceUserId();result = result * 59 + ($interfaceUserId == null ? 43 : $interfaceUserId.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseBanJiBzr;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseBanJiBzr)) {
            return false;
        }
        VeBaseBanJiBzr other = (VeBaseBanJiBzr)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$bjId = getBjId();Object other$bjId = other.getBjId();
        if (this$bjId == null ? other$bjId != null : !this$bjId.equals(other$bjId)) {
            return false;
        }
        Object this$createTime = getCreateTime();Object other$createTime = other.getCreateTime();
        if (this$createTime == null ? other$createTime != null : !this$createTime.equals(other$createTime)) {
            return false;
        }
        Object this$terminalId = getTerminalId();Object other$terminalId = other.getTerminalId();
        if (this$terminalId == null ? other$terminalId != null : !this$terminalId.equals(other$terminalId)) {
            return false;
        }
        Object this$isCurrent = getIsCurrent();Object other$isCurrent = other.getIsCurrent();
        if (this$isCurrent == null ? other$isCurrent != null : !this$isCurrent.equals(other$isCurrent)) {
            return false;
        }
        Object this$startDate = getStartDate();Object other$startDate = other.getStartDate();
        if (this$startDate == null ? other$startDate != null : !this$startDate.equals(other$startDate)) {
            return false;
        }
        Object this$endDate = getEndDate();Object other$endDate = other.getEndDate();
        if (this$endDate == null ? other$endDate != null : !this$endDate.equals(other$endDate)) {
            return false;
        }
        Object this$nansrs = getNansrs();Object other$nansrs = other.getNansrs();
        if (this$nansrs == null ? other$nansrs != null : !this$nansrs.equals(other$nansrs)) {
            return false;
        }
        Object this$nvsrs = getNvsrs();Object other$nvsrs = other.getNvsrs();
        if (this$nvsrs == null ? other$nvsrs != null : !this$nvsrs.equals(other$nvsrs)) {
            return false;
        }
        Object this$bzrUserId = getBzrUserId();Object other$bzrUserId = other.getBzrUserId();
        if (this$bzrUserId == null ? other$bzrUserId != null : !this$bzrUserId.equals(other$bzrUserId)) {
            return false;
        }
        Object this$bzrUserName = getBzrUserName();Object other$bzrUserName = other.getBzrUserName();
        if (this$bzrUserName == null ? other$bzrUserName != null : !this$bzrUserName.equals(other$bzrUserName)) {
            return false;
        }
        Object this$createUserId = getCreateUserId();Object other$createUserId = other.getCreateUserId();
        if (this$createUserId == null ? other$createUserId != null : !this$createUserId.equals(other$createUserId)) {
            return false;
        }
        Object this$xzbmc = getXzbmc();Object other$xzbmc = other.getXzbmc();
        if (this$xzbmc == null ? other$xzbmc != null : !this$xzbmc.equals(other$xzbmc)) {
            return false;
        }
        Object this$njmc = getNjmc();Object other$njmc = other.getNjmc();
        if (this$njmc == null ? other$njmc != null : !this$njmc.equals(other$njmc)) {
            return false;
        }
        Object this$zymc = getZymc();Object other$zymc = other.getZymc();
        if (this$zymc == null ? other$zymc != null : !this$zymc.equals(other$zymc)) {
            return false;
        }
        Object this$interfaceUserId = getInterfaceUserId();Object other$interfaceUserId = other.getInterfaceUserId();return this$interfaceUserId == null ? other$interfaceUserId == null : this$interfaceUserId.equals(other$interfaceUserId);
    }

    public Integer getId()
    {
        return this.id;
    }

    public Integer getBjId()
    {
        return this.bjId;
    }

    public String getBzrUserId()
    {
        return this.bzrUserId;
    }

    public String getBzrUserName()
    {
        return this.bzrUserName;
    }

    public Integer getCreateTime()
    {
        return this.createTime;
    }

    public Integer getTerminalId()
    {
        return this.terminalId;
    }

    public Integer getIsCurrent()
    {
        return this.isCurrent;
    }

    public String getCreateUserId()
    {
        return this.createUserId;
    }

    public Integer getStartDate()
    {
        return this.startDate;
    }

    public Integer getEndDate()
    {
        return this.endDate;
    }

    public String getXzbmc()
    {
        return this.xzbmc;
    }

    public String getNjmc()
    {
        return this.njmc;
    }

    public Integer getNansrs()
    {
        return this.nansrs;
    }

    public Integer getNvsrs()
    {
        return this.nvsrs;
    }

    public String getZymc()
    {
        return this.zymc;
    }

    public String getInterfaceUserId()
    {
        return this.interfaceUserId;
    }
}
