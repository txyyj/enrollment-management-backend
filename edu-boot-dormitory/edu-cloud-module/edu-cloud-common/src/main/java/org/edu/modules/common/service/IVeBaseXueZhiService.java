package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseXueZhi;

public abstract interface IVeBaseXueZhiService
        extends IService<VeBaseXueZhi>
{
    public abstract List<Map<String, Object>> getXueZhiPageList(VeBaseXueZhi paramVeBaseXueZhi);

    public abstract VeBaseXueZhi getXueZhiByName(Integer paramInteger, String paramString);
}
