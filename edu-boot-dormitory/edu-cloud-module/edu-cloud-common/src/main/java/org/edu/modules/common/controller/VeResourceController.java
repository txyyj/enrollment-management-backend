package org.edu.modules.common.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.modules.common.service.IVeBaseAppUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags={"资源目录等"})
@RestController
@RequestMapping({"/common/veResource"})
@ApiSort(60)
public class VeResourceController
{
    private static final Logger log = LoggerFactory.getLogger(VeResourceController.class);
    @Autowired
    private IVeBaseAppUserService veBaseAppUserService;

    @AutoLog("资源目录-分页列表查询")
    @ApiOperation(value="资源目录-分页列表查询", notes="资源目录-分页列表查询")
    @GetMapping({"/queryResourcePageList"})
    public Result<?> queryResourcePageList(@RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        PageHelper.startPage(pageNo.intValue(), pageSize.intValue());
        List<Map<String, Object>> list = this.veBaseAppUserService.queryResourcePageList();
        PageInfo<?> pageInfo = new PageInfo(list);
        return Result.ok(pageInfo);
    }

    @AutoLog("资源目录详情-根据表名查询内容")
    @ApiOperation(value="资源目录详情-根据表名查询内容", notes="资源目录详情-根据表名查询内容")
    @GetMapping({"/getDataListByTableName"})
    public Result<?> getDataListByTableName(@RequestParam(name="tableName", required=true) String tableName, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        List<Map<String, Object>> columnList = this.veBaseAppUserService.queryColumnNameListByTableName(tableName);

        Page<Map<String, Object>> page = new Page(pageNo.intValue(), pageSize.intValue());
        IPage<Map<String, Object>> pageList = this.veBaseAppUserService.getDataListByTableName(page, tableName);
        Map map = new HashMap();
        map.put("columnList", columnList);
        map.put("pageList", pageList);
        return Result.ok(map);
    }

    @AutoLog("资源目录详情-根据表名查询字段信息")
    @ApiOperation(value="资源目录详情-根据表名查询字段信息", notes="资源目录详情-根据表名查询字段信息")
    @GetMapping({"/getColumnListByTableName"})
    public Result<?> getColumnListByTableName(@RequestParam(name="tableName", required=true) String tableName)
    {
        List<Map<String, Object>> list = this.veBaseAppUserService.queryColumnNameListByTableName(tableName);
        for (Map map : list)
        {
            String len = map.get("columnLength").toString().substring(map.get("columnLength").toString().indexOf("("), map.get("columnLength").toString().indexOf(")"));
            map.put("columnLength", len);
        }
        return Result.ok(list);
    }
}
