package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.edu.modules.common.entity.VeBaseDepartment;

public abstract interface VeBaseDepartmentMapper
        extends BaseMapper<VeBaseDepartment>
{
    public abstract List<VeBaseDepartment> getRootList();

    public abstract List<VeBaseDepartment> getBodyList();

    public abstract VeBaseDepartment getDepartmentByName(Integer paramInteger, String paramString);

    public abstract VeBaseDepartment getDepartmentByCode(Integer paramInteger, String paramString);
}
