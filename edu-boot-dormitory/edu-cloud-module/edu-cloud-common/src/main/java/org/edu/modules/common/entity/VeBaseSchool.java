package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_school")
@ApiModel(value="ve_base_school对象", description="学校信息表")
public class VeBaseSchool
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="学校代码", width=15.0D)
    @ApiModelProperty("学校代码")
    private String xxdm;
    @Excel(name="学校名称", width=15.0D)
    @ApiModelProperty("学校名称")
    private String xxmc;
    @Excel(name="学校英文名称", width=15.0D)
    @ApiModelProperty("学校英文名称")
    private String xxywmc;
    @Excel(name="学校地址", width=15.0D)
    @ApiModelProperty("学校地址")
    private String xxdz;
    @Excel(name="学校邮政编", width=15.0D)
    @ApiModelProperty("学校邮政编")
    private String xxyzbm;
    @Excel(name="建校年月", width=15.0D)
    @ApiModelProperty("建校年月")
    private Integer jxny;
    @Excel(name="校庆日", width=15.0D)
    @ApiModelProperty("校庆日")
    private String xqr;
    @Excel(name="学校办学类", width=15.0D)
    @ApiModelProperty("学校办学类")
    private String xxbxlxm;
    @Excel(name="学校举办者", width=15.0D)
    @ApiModelProperty("学校举办者")
    private String xxjbz;
    @Excel(name="法定代表人名称", width=15.0D)
    @ApiModelProperty("法定代表人名称")
    private String fddbrm;
    @Excel(name="法人证书号", width=15.0D)
    @ApiModelProperty("法人证书号")
    private String frzsh;
    @Excel(name="校长姓名", width=15.0D)
    @ApiModelProperty("校长姓名")
    private String xzxm;
    @Excel(name="校长用户ID", width=15.0D)
    @ApiModelProperty("校长用户ID")
    private String xzUserId;
    @Excel(name="党委负责人姓名", width=15.0D)
    @ApiModelProperty("党委负责人姓名")
    private String dwfzrxm;
    @Excel(name="党委负责人用户ID", width=15.0D)
    @ApiModelProperty("党委负责人用户ID")
    private String dwfzrUserId;
    @Excel(name="组织机构码", width=15.0D)
    @ApiModelProperty("组织机构码")
    private String zzjgm;
    @Excel(name="联系电话", width=15.0D)
    @ApiModelProperty("联系电话")
    private String lxdh;
    @Excel(name="传真电话", width=15.0D)
    @ApiModelProperty("传真电话")
    private String czdh;
    @Excel(name="电子信箱", width=15.0D)
    @ApiModelProperty("电子信箱")
    private String dzxx;
    @Excel(name="主页地址", width=15.0D)
    @ApiModelProperty("主页地址")
    private String zydz;
    @Excel(name="历史沿革，学校发展过程中的重大变化或事件", width=15.0D)
    @ApiModelProperty("历史沿革，学校发展过程中的重大变化或事件")
    private String lsyg;
    @Excel(name="学校校区数", width=15.0D)
    @ApiModelProperty("学校校区数")
    private Integer xxxqs;
    @Excel(name="学校评估类型，PGQK 评估情况代码", width=15.0D)
    @ApiModelProperty("学校评估类型，PGQK 评估情况代码")
    private String xxpglx;
    @Excel(name="学校评估情况说明", width=15.0D)
    @ApiModelProperty("学校评估情况说明")
    private String xxpgqksm;
    @Excel(name="系统终端ID", width=15.0D)
    @ApiModelProperty("系统终端ID")
    private Integer terminalid;
    @TableField(exist=false)
    @ApiModelProperty("建校年月")
    private String jxnyName;

    public VeBaseSchool setXxmc(String xxmc)
    {
        this.xxmc = xxmc;return this;
    }

    public VeBaseSchool setXxdm(String xxdm)
    {
        this.xxdm = xxdm;return this;
    }

    public VeBaseSchool setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseSchool(id=" + getId() + ", xxdm=" + getXxdm() + ", xxmc=" + getXxmc() + ", xxywmc=" + getXxywmc() + ", xxdz=" + getXxdz() + ", xxyzbm=" + getXxyzbm() + ", jxny=" + getJxny() + ", xqr=" + getXqr() + ", xxbxlxm=" + getXxbxlxm() + ", xxjbz=" + getXxjbz() + ", fddbrm=" + getFddbrm() + ", frzsh=" + getFrzsh() + ", xzxm=" + getXzxm() + ", xzUserId=" + getXzUserId() + ", dwfzrxm=" + getDwfzrxm() + ", dwfzrUserId=" + getDwfzrUserId() + ", zzjgm=" + getZzjgm() + ", lxdh=" + getLxdh() + ", czdh=" + getCzdh() + ", dzxx=" + getDzxx() + ", zydz=" + getZydz() + ", lsyg=" + getLsyg() + ", xxxqs=" + getXxxqs() + ", xxpglx=" + getXxpglx() + ", xxpgqksm=" + getXxpgqksm() + ", terminalid=" + getTerminalid() + ", jxnyName=" + getJxnyName() + ")";
    }

    public VeBaseSchool setJxnyName(String jxnyName)
    {
        this.jxnyName = jxnyName;return this;
    }

    public VeBaseSchool setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeBaseSchool setXxpgqksm(String xxpgqksm)
    {
        this.xxpgqksm = xxpgqksm;return this;
    }

    public VeBaseSchool setXxpglx(String xxpglx)
    {
        this.xxpglx = xxpglx;return this;
    }

    public VeBaseSchool setXxxqs(Integer xxxqs)
    {
        this.xxxqs = xxxqs;return this;
    }

    public VeBaseSchool setLsyg(String lsyg)
    {
        this.lsyg = lsyg;return this;
    }

    public VeBaseSchool setZydz(String zydz)
    {
        this.zydz = zydz;return this;
    }

    public VeBaseSchool setDzxx(String dzxx)
    {
        this.dzxx = dzxx;return this;
    }

    public VeBaseSchool setCzdh(String czdh)
    {
        this.czdh = czdh;return this;
    }

    public VeBaseSchool setLxdh(String lxdh)
    {
        this.lxdh = lxdh;return this;
    }

    public VeBaseSchool setZzjgm(String zzjgm)
    {
        this.zzjgm = zzjgm;return this;
    }

    public VeBaseSchool setDwfzrUserId(String dwfzrUserId)
    {
        this.dwfzrUserId = dwfzrUserId;return this;
    }

    public VeBaseSchool setDwfzrxm(String dwfzrxm)
    {
        this.dwfzrxm = dwfzrxm;return this;
    }

    public VeBaseSchool setXzUserId(String xzUserId)
    {
        this.xzUserId = xzUserId;return this;
    }

    public VeBaseSchool setXzxm(String xzxm)
    {
        this.xzxm = xzxm;return this;
    }

    public VeBaseSchool setFrzsh(String frzsh)
    {
        this.frzsh = frzsh;return this;
    }

    public VeBaseSchool setFddbrm(String fddbrm)
    {
        this.fddbrm = fddbrm;return this;
    }

    public VeBaseSchool setXxjbz(String xxjbz)
    {
        this.xxjbz = xxjbz;return this;
    }

    public VeBaseSchool setXxbxlxm(String xxbxlxm)
    {
        this.xxbxlxm = xxbxlxm;return this;
    }

    public VeBaseSchool setXqr(String xqr)
    {
        this.xqr = xqr;return this;
    }

    public VeBaseSchool setJxny(Integer jxny)
    {
        this.jxny = jxny;return this;
    }

    public VeBaseSchool setXxyzbm(String xxyzbm)
    {
        this.xxyzbm = xxyzbm;return this;
    }

    public VeBaseSchool setXxdz(String xxdz)
    {
        this.xxdz = xxdz;return this;
    }

    public VeBaseSchool setXxywmc(String xxywmc)
    {
        this.xxywmc = xxywmc;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $jxny = getJxny();result = result * 59 + ($jxny == null ? 43 : $jxny.hashCode());Object $xxxqs = getXxxqs();result = result * 59 + ($xxxqs == null ? 43 : $xxxqs.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $xxdm = getXxdm();result = result * 59 + ($xxdm == null ? 43 : $xxdm.hashCode());Object $xxmc = getXxmc();result = result * 59 + ($xxmc == null ? 43 : $xxmc.hashCode());Object $xxywmc = getXxywmc();result = result * 59 + ($xxywmc == null ? 43 : $xxywmc.hashCode());Object $xxdz = getXxdz();result = result * 59 + ($xxdz == null ? 43 : $xxdz.hashCode());Object $xxyzbm = getXxyzbm();result = result * 59 + ($xxyzbm == null ? 43 : $xxyzbm.hashCode());Object $xqr = getXqr();result = result * 59 + ($xqr == null ? 43 : $xqr.hashCode());Object $xxbxlxm = getXxbxlxm();result = result * 59 + ($xxbxlxm == null ? 43 : $xxbxlxm.hashCode());Object $xxjbz = getXxjbz();result = result * 59 + ($xxjbz == null ? 43 : $xxjbz.hashCode());Object $fddbrm = getFddbrm();result = result * 59 + ($fddbrm == null ? 43 : $fddbrm.hashCode());Object $frzsh = getFrzsh();result = result * 59 + ($frzsh == null ? 43 : $frzsh.hashCode());Object $xzxm = getXzxm();result = result * 59 + ($xzxm == null ? 43 : $xzxm.hashCode());Object $xzUserId = getXzUserId();result = result * 59 + ($xzUserId == null ? 43 : $xzUserId.hashCode());Object $dwfzrxm = getDwfzrxm();result = result * 59 + ($dwfzrxm == null ? 43 : $dwfzrxm.hashCode());Object $dwfzrUserId = getDwfzrUserId();result = result * 59 + ($dwfzrUserId == null ? 43 : $dwfzrUserId.hashCode());Object $zzjgm = getZzjgm();result = result * 59 + ($zzjgm == null ? 43 : $zzjgm.hashCode());Object $lxdh = getLxdh();result = result * 59 + ($lxdh == null ? 43 : $lxdh.hashCode());Object $czdh = getCzdh();result = result * 59 + ($czdh == null ? 43 : $czdh.hashCode());Object $dzxx = getDzxx();result = result * 59 + ($dzxx == null ? 43 : $dzxx.hashCode());Object $zydz = getZydz();result = result * 59 + ($zydz == null ? 43 : $zydz.hashCode());Object $lsyg = getLsyg();result = result * 59 + ($lsyg == null ? 43 : $lsyg.hashCode());Object $xxpglx = getXxpglx();result = result * 59 + ($xxpglx == null ? 43 : $xxpglx.hashCode());Object $xxpgqksm = getXxpgqksm();result = result * 59 + ($xxpgqksm == null ? 43 : $xxpgqksm.hashCode());Object $jxnyName = getJxnyName();result = result * 59 + ($jxnyName == null ? 43 : $jxnyName.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseSchool;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseSchool)) {
            return false;
        }
        VeBaseSchool other = (VeBaseSchool)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$jxny = getJxny();Object other$jxny = other.getJxny();
        if (this$jxny == null ? other$jxny != null : !this$jxny.equals(other$jxny)) {
            return false;
        }
        Object this$xxxqs = getXxxqs();Object other$xxxqs = other.getXxxqs();
        if (this$xxxqs == null ? other$xxxqs != null : !this$xxxqs.equals(other$xxxqs)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$xxdm = getXxdm();Object other$xxdm = other.getXxdm();
        if (this$xxdm == null ? other$xxdm != null : !this$xxdm.equals(other$xxdm)) {
            return false;
        }
        Object this$xxmc = getXxmc();Object other$xxmc = other.getXxmc();
        if (this$xxmc == null ? other$xxmc != null : !this$xxmc.equals(other$xxmc)) {
            return false;
        }
        Object this$xxywmc = getXxywmc();Object other$xxywmc = other.getXxywmc();
        if (this$xxywmc == null ? other$xxywmc != null : !this$xxywmc.equals(other$xxywmc)) {
            return false;
        }
        Object this$xxdz = getXxdz();Object other$xxdz = other.getXxdz();
        if (this$xxdz == null ? other$xxdz != null : !this$xxdz.equals(other$xxdz)) {
            return false;
        }
        Object this$xxyzbm = getXxyzbm();Object other$xxyzbm = other.getXxyzbm();
        if (this$xxyzbm == null ? other$xxyzbm != null : !this$xxyzbm.equals(other$xxyzbm)) {
            return false;
        }
        Object this$xqr = getXqr();Object other$xqr = other.getXqr();
        if (this$xqr == null ? other$xqr != null : !this$xqr.equals(other$xqr)) {
            return false;
        }
        Object this$xxbxlxm = getXxbxlxm();Object other$xxbxlxm = other.getXxbxlxm();
        if (this$xxbxlxm == null ? other$xxbxlxm != null : !this$xxbxlxm.equals(other$xxbxlxm)) {
            return false;
        }
        Object this$xxjbz = getXxjbz();Object other$xxjbz = other.getXxjbz();
        if (this$xxjbz == null ? other$xxjbz != null : !this$xxjbz.equals(other$xxjbz)) {
            return false;
        }
        Object this$fddbrm = getFddbrm();Object other$fddbrm = other.getFddbrm();
        if (this$fddbrm == null ? other$fddbrm != null : !this$fddbrm.equals(other$fddbrm)) {
            return false;
        }
        Object this$frzsh = getFrzsh();Object other$frzsh = other.getFrzsh();
        if (this$frzsh == null ? other$frzsh != null : !this$frzsh.equals(other$frzsh)) {
            return false;
        }
        Object this$xzxm = getXzxm();Object other$xzxm = other.getXzxm();
        if (this$xzxm == null ? other$xzxm != null : !this$xzxm.equals(other$xzxm)) {
            return false;
        }
        Object this$xzUserId = getXzUserId();Object other$xzUserId = other.getXzUserId();
        if (this$xzUserId == null ? other$xzUserId != null : !this$xzUserId.equals(other$xzUserId)) {
            return false;
        }
        Object this$dwfzrxm = getDwfzrxm();Object other$dwfzrxm = other.getDwfzrxm();
        if (this$dwfzrxm == null ? other$dwfzrxm != null : !this$dwfzrxm.equals(other$dwfzrxm)) {
            return false;
        }
        Object this$dwfzrUserId = getDwfzrUserId();Object other$dwfzrUserId = other.getDwfzrUserId();
        if (this$dwfzrUserId == null ? other$dwfzrUserId != null : !this$dwfzrUserId.equals(other$dwfzrUserId)) {
            return false;
        }
        Object this$zzjgm = getZzjgm();Object other$zzjgm = other.getZzjgm();
        if (this$zzjgm == null ? other$zzjgm != null : !this$zzjgm.equals(other$zzjgm)) {
            return false;
        }
        Object this$lxdh = getLxdh();Object other$lxdh = other.getLxdh();
        if (this$lxdh == null ? other$lxdh != null : !this$lxdh.equals(other$lxdh)) {
            return false;
        }
        Object this$czdh = getCzdh();Object other$czdh = other.getCzdh();
        if (this$czdh == null ? other$czdh != null : !this$czdh.equals(other$czdh)) {
            return false;
        }
        Object this$dzxx = getDzxx();Object other$dzxx = other.getDzxx();
        if (this$dzxx == null ? other$dzxx != null : !this$dzxx.equals(other$dzxx)) {
            return false;
        }
        Object this$zydz = getZydz();Object other$zydz = other.getZydz();
        if (this$zydz == null ? other$zydz != null : !this$zydz.equals(other$zydz)) {
            return false;
        }
        Object this$lsyg = getLsyg();Object other$lsyg = other.getLsyg();
        if (this$lsyg == null ? other$lsyg != null : !this$lsyg.equals(other$lsyg)) {
            return false;
        }
        Object this$xxpglx = getXxpglx();Object other$xxpglx = other.getXxpglx();
        if (this$xxpglx == null ? other$xxpglx != null : !this$xxpglx.equals(other$xxpglx)) {
            return false;
        }
        Object this$xxpgqksm = getXxpgqksm();Object other$xxpgqksm = other.getXxpgqksm();
        if (this$xxpgqksm == null ? other$xxpgqksm != null : !this$xxpgqksm.equals(other$xxpgqksm)) {
            return false;
        }
        Object this$jxnyName = getJxnyName();Object other$jxnyName = other.getJxnyName();return this$jxnyName == null ? other$jxnyName == null : this$jxnyName.equals(other$jxnyName);
    }

    public String getId()
    {
        return this.id;
    }

    public String getXxdm()
    {
        return this.xxdm;
    }

    public String getXxmc()
    {
        return this.xxmc;
    }

    public String getXxywmc()
    {
        return this.xxywmc;
    }

    public String getXxdz()
    {
        return this.xxdz;
    }

    public String getXxyzbm()
    {
        return this.xxyzbm;
    }

    public Integer getJxny()
    {
        return this.jxny;
    }

    public String getXqr()
    {
        return this.xqr;
    }

    public String getXxbxlxm()
    {
        return this.xxbxlxm;
    }

    public String getXxjbz()
    {
        return this.xxjbz;
    }

    public String getFddbrm()
    {
        return this.fddbrm;
    }

    public String getFrzsh()
    {
        return this.frzsh;
    }

    public String getXzxm()
    {
        return this.xzxm;
    }

    public String getXzUserId()
    {
        return this.xzUserId;
    }

    public String getDwfzrxm()
    {
        return this.dwfzrxm;
    }

    public String getDwfzrUserId()
    {
        return this.dwfzrUserId;
    }

    public String getZzjgm()
    {
        return this.zzjgm;
    }

    public String getLxdh()
    {
        return this.lxdh;
    }

    public String getCzdh()
    {
        return this.czdh;
    }

    public String getDzxx()
    {
        return this.dzxx;
    }

    public String getZydz()
    {
        return this.zydz;
    }

    public String getLsyg()
    {
        return this.lsyg;
    }

    public Integer getXxxqs()
    {
        return this.xxxqs;
    }

    public String getXxpglx()
    {
        return this.xxpglx;
    }

    public String getXxpgqksm()
    {
        return this.xxpgqksm;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public String getJxnyName()
    {
        return this.jxnyName;
    }
}
