package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseInterface;
import org.edu.modules.common.mapper.VeBaseInterfaceMapper;
import org.edu.modules.common.service.IVeBaseInterfaceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseInterfaceServiceImpl
        extends ServiceImpl<VeBaseInterfaceMapper, VeBaseInterface>
        implements IVeBaseInterfaceService
{
    @Autowired
    private VeBaseInterfaceMapper veBaseInterfaceMapper;

    public List<Map<String, Object>> getInterfacePageList(VeBaseInterface veBaseInterface)
    {
        return this.veBaseInterfaceMapper.getInterfacePageList(veBaseInterface);
    }

    public List<Map<String, Object>> getInterfaceList()
    {
        return this.veBaseInterfaceMapper.getInterfaceList();
    }
}
