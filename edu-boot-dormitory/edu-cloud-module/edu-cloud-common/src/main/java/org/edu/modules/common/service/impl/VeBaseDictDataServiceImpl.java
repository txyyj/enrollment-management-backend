package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import org.edu.modules.common.entity.VeBaseDictData;
import org.edu.modules.common.mapper.VeBaseDictDataMapper;
import org.edu.modules.common.service.IVeBaseDictDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseDictDataServiceImpl
        extends ServiceImpl<VeBaseDictDataMapper, VeBaseDictData>
        implements IVeBaseDictDataService
{
    @Autowired
    private VeBaseDictDataMapper veBaseDictDataMapper;

    public List<VeBaseDictData> getDictDataByModelCode(String modelCode, String code)
    {
        return this.veBaseDictDataMapper.getDictDataByModelCode(modelCode, code);
    }

    public List<VeBaseDictData> getDictDataByModelCodeTitle(String modelCode, String title)
    {
        return this.veBaseDictDataMapper.getDictDataByModelCodeTitle(modelCode, title);
    }
}
