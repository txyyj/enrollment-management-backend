package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseTransformation;
import org.edu.modules.common.mapper.VeBaseTransformationMapper;
import org.edu.modules.common.service.IVeBaseTransformationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseTransformationServiceImpl
        extends ServiceImpl<VeBaseTransformationMapper, VeBaseTransformation>
        implements IVeBaseTransformationService
{
    @Autowired
    private VeBaseTransformationMapper veBaseTransformationMapper;

    public List<Map<String, Object>> getTransformationPageList(VeBaseTransformation veBaseTransformation)
    {
        return this.veBaseTransformationMapper.getTransformationPageList(veBaseTransformation);
    }
}
