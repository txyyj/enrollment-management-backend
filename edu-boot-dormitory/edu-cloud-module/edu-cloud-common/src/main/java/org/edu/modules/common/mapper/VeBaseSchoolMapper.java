package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseSchool;

public abstract interface VeBaseSchoolMapper
        extends BaseMapper<VeBaseSchool>
{
    public abstract List<Map<String, Object>> getSchoolPageList(VeBaseSchool paramVeBaseSchool);

    public abstract VeBaseSchool getSchoolByName(String paramString1, String paramString2);
}
