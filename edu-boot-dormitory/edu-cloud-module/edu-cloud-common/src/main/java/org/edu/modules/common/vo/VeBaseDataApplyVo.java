package org.edu.modules.common.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;

public class VeBaseDataApplyVo
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @ApiModelProperty("公司名称")
    private String companyName;
    @ApiModelProperty("系统名称")
    private String systemName;
    @ApiModelProperty("申请理由")
    private String reason;
    @ApiModelProperty("申请时间")
    @JsonFormat(pattern="yyyy-MM-dd")
    private Date createTime;
    @ApiModelProperty("申请人-账号名称")
    private String user;
    @ApiModelProperty("部门名称")
    private String departmentName;
    @ApiModelProperty("表名称")
    private String tableName;
    @TableField(exist=false)
    @ApiModelProperty("表字段")
    private String columnName;

    public void setSystemName(String systemName)
    {
        this.systemName = systemName;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $companyName = getCompanyName();result = result * 59 + ($companyName == null ? 43 : $companyName.hashCode());Object $systemName = getSystemName();result = result * 59 + ($systemName == null ? 43 : $systemName.hashCode());Object $reason = getReason();result = result * 59 + ($reason == null ? 43 : $reason.hashCode());Object $createTime = getCreateTime();result = result * 59 + ($createTime == null ? 43 : $createTime.hashCode());Object $user = getUser();result = result * 59 + ($user == null ? 43 : $user.hashCode());Object $departmentName = getDepartmentName();result = result * 59 + ($departmentName == null ? 43 : $departmentName.hashCode());Object $tableName = getTableName();result = result * 59 + ($tableName == null ? 43 : $tableName.hashCode());Object $columnName = getColumnName();result = result * 59 + ($columnName == null ? 43 : $columnName.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseDataApplyVo;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseDataApplyVo)) {
            return false;
        }
        VeBaseDataApplyVo other = (VeBaseDataApplyVo)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$companyName = getCompanyName();Object other$companyName = other.getCompanyName();
        if (this$companyName == null ? other$companyName != null : !this$companyName.equals(other$companyName)) {
            return false;
        }
        Object this$systemName = getSystemName();Object other$systemName = other.getSystemName();
        if (this$systemName == null ? other$systemName != null : !this$systemName.equals(other$systemName)) {
            return false;
        }
        Object this$reason = getReason();Object other$reason = other.getReason();
        if (this$reason == null ? other$reason != null : !this$reason.equals(other$reason)) {
            return false;
        }
        Object this$createTime = getCreateTime();Object other$createTime = other.getCreateTime();
        if (this$createTime == null ? other$createTime != null : !this$createTime.equals(other$createTime)) {
            return false;
        }
        Object this$user = getUser();Object other$user = other.getUser();
        if (this$user == null ? other$user != null : !this$user.equals(other$user)) {
            return false;
        }
        Object this$departmentName = getDepartmentName();Object other$departmentName = other.getDepartmentName();
        if (this$departmentName == null ? other$departmentName != null : !this$departmentName.equals(other$departmentName)) {
            return false;
        }
        Object this$tableName = getTableName();Object other$tableName = other.getTableName();
        if (this$tableName == null ? other$tableName != null : !this$tableName.equals(other$tableName)) {
            return false;
        }
        Object this$columnName = getColumnName();Object other$columnName = other.getColumnName();return this$columnName == null ? other$columnName == null : this$columnName.equals(other$columnName);
    }

    public void setColumnName(String columnName)
    {
        this.columnName = columnName;
    }

    public void setTableName(String tableName)
    {
        this.tableName = tableName;
    }

    public void setDepartmentName(String departmentName)
    {
        this.departmentName = departmentName;
    }

    public void setUser(String user)
    {
        this.user = user;
    }

    @JsonFormat(pattern="yyyy-MM-dd")
    public void setCreateTime(Date createTime)
    {
        this.createTime = createTime;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public void setCompanyName(String companyName)
    {
        this.companyName = companyName;
    }

    public String toString()
    {
        return "VeBaseDataApplyVo(id=" + getId() + ", companyName=" + getCompanyName() + ", systemName=" + getSystemName() + ", reason=" + getReason() + ", createTime=" + getCreateTime() + ", user=" + getUser() + ", departmentName=" + getDepartmentName() + ", tableName=" + getTableName() + ", columnName=" + getColumnName() + ")";
    }

    public void setReason(String reason)
    {
        this.reason = reason;
    }

    public String getId()
    {
        return this.id;
    }

    public String getCompanyName()
    {
        return this.companyName;
    }

    public String getSystemName()
    {
        return this.systemName;
    }

    public String getReason()
    {
        return this.reason;
    }

    public Date getCreateTime()
    {
        return this.createTime;
    }

    public String getUser()
    {
        return this.user;
    }

    public String getDepartmentName()
    {
        return this.departmentName;
    }

    public String getTableName()
    {
        return this.tableName;
    }

    public String getColumnName()
    {
        return this.columnName;
    }
}

