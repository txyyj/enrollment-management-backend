package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_jyz")
@ApiModel(value="ve_base_jyz对象", description="教研组(专业组)信息表")
public class VeBaseJYZ
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="编号", width=15.0D)
    @ApiModelProperty("教研组编号")
    private String jyzbh;
    @Excel(name="名称", width=15.0D)
    @ApiModelProperty("教研组名称")
    private String jyzmc;
    @Excel(name="英文名称", width=15.0D)
    @ApiModelProperty("教研组英文名称")
    private String jyzywmc;
    @Excel(name="简称", width=15.0D)
    @ApiModelProperty("教研组简称")
    private String jyzjc;
    @Excel(name="简拼", width=15.0D)
    @ApiModelProperty("教研组简拼")
    private String jyzjp;
    @Excel(name="地址", width=15.0D)
    @ApiModelProperty("教研组地址")
    private String jyzdz;
    @Excel(name="隶属上级教研组id", width=15.0D)
    @ApiModelProperty("隶属上级教研组id")
    private Integer pid;
    @Excel(name="节点路径", width=15.0D)
    @ApiModelProperty("节点路径")
    private String path;
    @Excel(name="院系id", width=15.0D)
    @ApiModelProperty("院系id")
    private Integer falId;
    @Excel(name="专业id", width=15.0D)
    @ApiModelProperty("专业id")
    private Integer specId;
    @Excel(name="校区id", width=15.0D)
    @ApiModelProperty("校区id")
    private Integer campusId;
    @Excel(name="建立年月aa", width=15.0D)
    @ApiModelProperty("建立年月")
    private Integer jlny;
    @Excel(name="负责人工号", width=15.0D)
    @ApiModelProperty("负责人用户ID")
    private String fzrUserId;
    @Excel(name="电话号码", width=15.0D)
    @ApiModelProperty("电话号码")
    private String telephone;
    @Excel(name="状态", width=15.0D)
    @ApiModelProperty("状态（1=启用，0=禁用）")
    private Integer status;
    @Excel(name="终端ID", width=15.0D)
    @ApiModelProperty("终端ID")
    private Integer terminalid;
    @Excel(name="上级名称", width=15.0D)
    @TableField(exist=false)
    private String pidName;
    @Excel(name="隶属校区", width=15.0D)
    @TableField(exist=false)
    private String campusName;
    @ApiModelProperty("建立年月")
    @Excel(name="建立年月", width=15.0D)
    @TableField(exist=false)
    private String jlnyName;

    public VeBaseJYZ setJyzmc(String jyzmc)
    {
        this.jyzmc = jyzmc;return this;
    }

    public VeBaseJYZ setJyzbh(String jyzbh)
    {
        this.jyzbh = jyzbh;return this;
    }

    public VeBaseJYZ setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseJYZ(id=" + getId() + ", jyzbh=" + getJyzbh() + ", jyzmc=" + getJyzmc() + ", jyzywmc=" + getJyzywmc() + ", jyzjc=" + getJyzjc() + ", jyzjp=" + getJyzjp() + ", jyzdz=" + getJyzdz() + ", pid=" + getPid() + ", path=" + getPath() + ", falId=" + getFalId() + ", specId=" + getSpecId() + ", campusId=" + getCampusId() + ", jlny=" + getJlny() + ", fzrUserId=" + getFzrUserId() + ", telephone=" + getTelephone() + ", status=" + getStatus() + ", terminalid=" + getTerminalid() + ", pidName=" + getPidName() + ", campusName=" + getCampusName() + ", jlnyName=" + getJlnyName() + ")";
    }

    public VeBaseJYZ setJlnyName(String jlnyName)
    {
        this.jlnyName = jlnyName;return this;
    }

    public VeBaseJYZ setCampusName(String campusName)
    {
        this.campusName = campusName;return this;
    }

    public VeBaseJYZ setPidName(String pidName)
    {
        this.pidName = pidName;return this;
    }

    public VeBaseJYZ setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeBaseJYZ setStatus(Integer status)
    {
        this.status = status;return this;
    }

    public VeBaseJYZ setTelephone(String telephone)
    {
        this.telephone = telephone;return this;
    }

    public VeBaseJYZ setFzrUserId(String fzrUserId)
    {
        this.fzrUserId = fzrUserId;return this;
    }

    public VeBaseJYZ setJlny(Integer jlny)
    {
        this.jlny = jlny;return this;
    }

    public VeBaseJYZ setCampusId(Integer campusId)
    {
        this.campusId = campusId;return this;
    }

    public VeBaseJYZ setSpecId(Integer specId)
    {
        this.specId = specId;return this;
    }

    public VeBaseJYZ setFalId(Integer falId)
    {
        this.falId = falId;return this;
    }

    public VeBaseJYZ setPath(String path)
    {
        this.path = path;return this;
    }

    public VeBaseJYZ setPid(Integer pid)
    {
        this.pid = pid;return this;
    }

    public VeBaseJYZ setJyzdz(String jyzdz)
    {
        this.jyzdz = jyzdz;return this;
    }

    public VeBaseJYZ setJyzjp(String jyzjp)
    {
        this.jyzjp = jyzjp;return this;
    }

    public VeBaseJYZ setJyzjc(String jyzjc)
    {
        this.jyzjc = jyzjc;return this;
    }

    public VeBaseJYZ setJyzywmc(String jyzywmc)
    {
        this.jyzywmc = jyzywmc;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $pid = getPid();result = result * 59 + ($pid == null ? 43 : $pid.hashCode());Object $falId = getFalId();result = result * 59 + ($falId == null ? 43 : $falId.hashCode());Object $specId = getSpecId();result = result * 59 + ($specId == null ? 43 : $specId.hashCode());Object $campusId = getCampusId();result = result * 59 + ($campusId == null ? 43 : $campusId.hashCode());Object $jlny = getJlny();result = result * 59 + ($jlny == null ? 43 : $jlny.hashCode());Object $status = getStatus();result = result * 59 + ($status == null ? 43 : $status.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $jyzbh = getJyzbh();result = result * 59 + ($jyzbh == null ? 43 : $jyzbh.hashCode());Object $jyzmc = getJyzmc();result = result * 59 + ($jyzmc == null ? 43 : $jyzmc.hashCode());Object $jyzywmc = getJyzywmc();result = result * 59 + ($jyzywmc == null ? 43 : $jyzywmc.hashCode());Object $jyzjc = getJyzjc();result = result * 59 + ($jyzjc == null ? 43 : $jyzjc.hashCode());Object $jyzjp = getJyzjp();result = result * 59 + ($jyzjp == null ? 43 : $jyzjp.hashCode());Object $jyzdz = getJyzdz();result = result * 59 + ($jyzdz == null ? 43 : $jyzdz.hashCode());Object $path = getPath();result = result * 59 + ($path == null ? 43 : $path.hashCode());Object $fzrUserId = getFzrUserId();result = result * 59 + ($fzrUserId == null ? 43 : $fzrUserId.hashCode());Object $telephone = getTelephone();result = result * 59 + ($telephone == null ? 43 : $telephone.hashCode());Object $pidName = getPidName();result = result * 59 + ($pidName == null ? 43 : $pidName.hashCode());Object $campusName = getCampusName();result = result * 59 + ($campusName == null ? 43 : $campusName.hashCode());Object $jlnyName = getJlnyName();result = result * 59 + ($jlnyName == null ? 43 : $jlnyName.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseJYZ;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseJYZ)) {
            return false;
        }
        VeBaseJYZ other = (VeBaseJYZ)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$pid = getPid();Object other$pid = other.getPid();
        if (this$pid == null ? other$pid != null : !this$pid.equals(other$pid)) {
            return false;
        }
        Object this$falId = getFalId();Object other$falId = other.getFalId();
        if (this$falId == null ? other$falId != null : !this$falId.equals(other$falId)) {
            return false;
        }
        Object this$specId = getSpecId();Object other$specId = other.getSpecId();
        if (this$specId == null ? other$specId != null : !this$specId.equals(other$specId)) {
            return false;
        }
        Object this$campusId = getCampusId();Object other$campusId = other.getCampusId();
        if (this$campusId == null ? other$campusId != null : !this$campusId.equals(other$campusId)) {
            return false;
        }
        Object this$jlny = getJlny();Object other$jlny = other.getJlny();
        if (this$jlny == null ? other$jlny != null : !this$jlny.equals(other$jlny)) {
            return false;
        }
        Object this$status = getStatus();Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$jyzbh = getJyzbh();Object other$jyzbh = other.getJyzbh();
        if (this$jyzbh == null ? other$jyzbh != null : !this$jyzbh.equals(other$jyzbh)) {
            return false;
        }
        Object this$jyzmc = getJyzmc();Object other$jyzmc = other.getJyzmc();
        if (this$jyzmc == null ? other$jyzmc != null : !this$jyzmc.equals(other$jyzmc)) {
            return false;
        }
        Object this$jyzywmc = getJyzywmc();Object other$jyzywmc = other.getJyzywmc();
        if (this$jyzywmc == null ? other$jyzywmc != null : !this$jyzywmc.equals(other$jyzywmc)) {
            return false;
        }
        Object this$jyzjc = getJyzjc();Object other$jyzjc = other.getJyzjc();
        if (this$jyzjc == null ? other$jyzjc != null : !this$jyzjc.equals(other$jyzjc)) {
            return false;
        }
        Object this$jyzjp = getJyzjp();Object other$jyzjp = other.getJyzjp();
        if (this$jyzjp == null ? other$jyzjp != null : !this$jyzjp.equals(other$jyzjp)) {
            return false;
        }
        Object this$jyzdz = getJyzdz();Object other$jyzdz = other.getJyzdz();
        if (this$jyzdz == null ? other$jyzdz != null : !this$jyzdz.equals(other$jyzdz)) {
            return false;
        }
        Object this$path = getPath();Object other$path = other.getPath();
        if (this$path == null ? other$path != null : !this$path.equals(other$path)) {
            return false;
        }
        Object this$fzrUserId = getFzrUserId();Object other$fzrUserId = other.getFzrUserId();
        if (this$fzrUserId == null ? other$fzrUserId != null : !this$fzrUserId.equals(other$fzrUserId)) {
            return false;
        }
        Object this$telephone = getTelephone();Object other$telephone = other.getTelephone();
        if (this$telephone == null ? other$telephone != null : !this$telephone.equals(other$telephone)) {
            return false;
        }
        Object this$pidName = getPidName();Object other$pidName = other.getPidName();
        if (this$pidName == null ? other$pidName != null : !this$pidName.equals(other$pidName)) {
            return false;
        }
        Object this$campusName = getCampusName();Object other$campusName = other.getCampusName();
        if (this$campusName == null ? other$campusName != null : !this$campusName.equals(other$campusName)) {
            return false;
        }
        Object this$jlnyName = getJlnyName();Object other$jlnyName = other.getJlnyName();return this$jlnyName == null ? other$jlnyName == null : this$jlnyName.equals(other$jlnyName);
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getJyzbh()
    {
        return this.jyzbh;
    }

    public String getJyzmc()
    {
        return this.jyzmc;
    }

    public String getJyzywmc()
    {
        return this.jyzywmc;
    }

    public String getJyzjc()
    {
        return this.jyzjc;
    }

    public String getJyzjp()
    {
        return this.jyzjp;
    }

    public String getJyzdz()
    {
        return this.jyzdz;
    }

    public Integer getPid()
    {
        return this.pid;
    }

    public String getPath()
    {
        return this.path;
    }

    public Integer getFalId()
    {
        return this.falId;
    }

    public Integer getSpecId()
    {
        return this.specId;
    }

    public Integer getCampusId()
    {
        return this.campusId;
    }

    public Integer getJlny()
    {
        return this.jlny;
    }

    public String getFzrUserId()
    {
        return this.fzrUserId;
    }

    public String getTelephone()
    {
        return this.telephone;
    }

    public Integer getStatus()
    {
        return this.status;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public String getPidName()
    {
        return this.pidName;
    }

    public String getCampusName()
    {
        return this.campusName;
    }

    public String getJlnyName()
    {
        return this.jlnyName;
    }
}
