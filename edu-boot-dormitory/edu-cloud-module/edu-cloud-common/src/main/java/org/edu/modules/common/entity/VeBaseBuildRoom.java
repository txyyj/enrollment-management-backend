package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_build_room")
@ApiModel(value="ve_base_build_room对象", description="教室信息表")
public class VeBaseBuildRoom
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="楼栋类型Code", width=15.0D)
    @ApiModelProperty("楼栋类型Code")
    private String buildType;
    @Excel(name="楼栋类型名称", width=15.0D)
    @ApiModelProperty("楼栋类型名称")
    private String buildTypeName;
    @Excel(name="楼栋id", width=15.0D)
    @ApiModelProperty("楼栋id")
    private String buildTid;
    @Excel(name="楼栋名称", width=15.0D)
    @ApiModelProperty("楼栋名称")
    private String buildName;
    @Excel(name="组织机构编号", width=15.0D)
    @ApiModelProperty("组织机构编号")
    private String orgNo;
    @Excel(name="组织机构名称", width=15.0D)
    @ApiModelProperty("组织机构名称")
    private String orgName;
    @Excel(name="教室名称", width=15.0D)
    @ApiModelProperty("教室名称")
    private String roomName;
    @Excel(name="教室用途Code", width=15.0D)
    @ApiModelProperty("教室用途Code")
    private String roomPurpose;
    @Excel(name="教室用途名称", width=15.0D)
    @ApiModelProperty("教室用途名称")
    private String roomPurposeName;
    @Excel(name="教室类型Code", width=15.0D)
    @ApiModelProperty("教室类型Code")
    private String roomType;
    @Excel(name="教室类型名称", width=15.0D)
    @ApiModelProperty("教室类型名称")
    private String roomTypeName;
    @Excel(name="面积", width=15.0D)
    @ApiModelProperty("面积")
    private Integer square;
    @Excel(name="容量", width=15.0D)
    @ApiModelProperty("容量")
    private Integer capacity;
    @Excel(name="状态（1.启动，0.停用）", width=15.0D)
    @ApiModelProperty("状态（1.启动，0.停用）")
    private Integer status;
    @Excel(name="所属楼层", width=15.0D)
    @ApiModelProperty("所属楼层")
    private Integer floorNum;
    @Excel(name="创建方式", width=15.0D)
    @ApiModelProperty("创建方式")
    private String createBy;
    @Excel(name="创建日期", width=15.0D)
    @ApiModelProperty("创建日期")
    private Date createDate;
    @Excel(name="修改方式", width=15.0D)
    @ApiModelProperty("修改方式")
    private String updateBy;
    @Excel(name="修改日期", width=15.0D)
    @ApiModelProperty("修改日期")
    private Date updateDate;
    @Excel(name="是否删除标志", width=15.0D)
    @ApiModelProperty("是否删除标志")
    private String delFlag;
    @Excel(name="备注", width=15.0D)
    @ApiModelProperty("备注")
    private String remarks;

    public VeBaseBuildRoom setBuildTypeName(String buildTypeName)
    {
        this.buildTypeName = buildTypeName;return this;
    }

    public VeBaseBuildRoom setBuildType(String buildType)
    {
        this.buildType = buildType;return this;
    }

    public VeBaseBuildRoom setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseBuildRoom(id=" + getId() + ", buildType=" + getBuildType() + ", buildTypeName=" + getBuildTypeName() + ", buildTid=" + getBuildTid() + ", buildName=" + getBuildName() + ", orgNo=" + getOrgNo() + ", orgName=" + getOrgName() + ", roomName=" + getRoomName() + ", roomPurpose=" + getRoomPurpose() + ", roomPurposeName=" + getRoomPurposeName() + ", roomType=" + getRoomType() + ", roomTypeName=" + getRoomTypeName() + ", square=" + getSquare() + ", capacity=" + getCapacity() + ", status=" + getStatus() + ", floorNum=" + getFloorNum() + ", createBy=" + getCreateBy() + ", createDate=" + getCreateDate() + ", updateBy=" + getUpdateBy() + ", updateDate=" + getUpdateDate() + ", delFlag=" + getDelFlag() + ", remarks=" + getRemarks() + ")";
    }

    public VeBaseBuildRoom setRemarks(String remarks)
    {
        this.remarks = remarks;return this;
    }

    public VeBaseBuildRoom setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;return this;
    }

    public VeBaseBuildRoom setUpdateDate(Date updateDate)
    {
        this.updateDate = updateDate;return this;
    }

    public VeBaseBuildRoom setUpdateBy(String updateBy)
    {
        this.updateBy = updateBy;return this;
    }

    public VeBaseBuildRoom setCreateDate(Date createDate)
    {
        this.createDate = createDate;return this;
    }

    public VeBaseBuildRoom setCreateBy(String createBy)
    {
        this.createBy = createBy;return this;
    }

    public VeBaseBuildRoom setFloorNum(Integer floorNum)
    {
        this.floorNum = floorNum;return this;
    }

    public VeBaseBuildRoom setStatus(Integer status)
    {
        this.status = status;return this;
    }

    public VeBaseBuildRoom setCapacity(Integer capacity)
    {
        this.capacity = capacity;return this;
    }

    public VeBaseBuildRoom setSquare(Integer square)
    {
        this.square = square;return this;
    }

    public VeBaseBuildRoom setRoomTypeName(String roomTypeName)
    {
        this.roomTypeName = roomTypeName;return this;
    }

    public VeBaseBuildRoom setRoomType(String roomType)
    {
        this.roomType = roomType;return this;
    }

    public VeBaseBuildRoom setRoomPurposeName(String roomPurposeName)
    {
        this.roomPurposeName = roomPurposeName;return this;
    }

    public VeBaseBuildRoom setRoomPurpose(String roomPurpose)
    {
        this.roomPurpose = roomPurpose;return this;
    }

    public VeBaseBuildRoom setRoomName(String roomName)
    {
        this.roomName = roomName;return this;
    }

    public VeBaseBuildRoom setOrgName(String orgName)
    {
        this.orgName = orgName;return this;
    }

    public VeBaseBuildRoom setOrgNo(String orgNo)
    {
        this.orgNo = orgNo;return this;
    }

    public VeBaseBuildRoom setBuildName(String buildName)
    {
        this.buildName = buildName;return this;
    }

    public VeBaseBuildRoom setBuildTid(String buildTid)
    {
        this.buildTid = buildTid;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $square = getSquare();result = result * 59 + ($square == null ? 43 : $square.hashCode());Object $capacity = getCapacity();result = result * 59 + ($capacity == null ? 43 : $capacity.hashCode());Object $status = getStatus();result = result * 59 + ($status == null ? 43 : $status.hashCode());Object $floorNum = getFloorNum();result = result * 59 + ($floorNum == null ? 43 : $floorNum.hashCode());Object $buildType = getBuildType();result = result * 59 + ($buildType == null ? 43 : $buildType.hashCode());Object $buildTypeName = getBuildTypeName();result = result * 59 + ($buildTypeName == null ? 43 : $buildTypeName.hashCode());Object $buildTid = getBuildTid();result = result * 59 + ($buildTid == null ? 43 : $buildTid.hashCode());Object $buildName = getBuildName();result = result * 59 + ($buildName == null ? 43 : $buildName.hashCode());Object $orgNo = getOrgNo();result = result * 59 + ($orgNo == null ? 43 : $orgNo.hashCode());Object $orgName = getOrgName();result = result * 59 + ($orgName == null ? 43 : $orgName.hashCode());Object $roomName = getRoomName();result = result * 59 + ($roomName == null ? 43 : $roomName.hashCode());Object $roomPurpose = getRoomPurpose();result = result * 59 + ($roomPurpose == null ? 43 : $roomPurpose.hashCode());Object $roomPurposeName = getRoomPurposeName();result = result * 59 + ($roomPurposeName == null ? 43 : $roomPurposeName.hashCode());Object $roomType = getRoomType();result = result * 59 + ($roomType == null ? 43 : $roomType.hashCode());Object $roomTypeName = getRoomTypeName();result = result * 59 + ($roomTypeName == null ? 43 : $roomTypeName.hashCode());Object $createBy = getCreateBy();result = result * 59 + ($createBy == null ? 43 : $createBy.hashCode());Object $createDate = getCreateDate();result = result * 59 + ($createDate == null ? 43 : $createDate.hashCode());Object $updateBy = getUpdateBy();result = result * 59 + ($updateBy == null ? 43 : $updateBy.hashCode());Object $updateDate = getUpdateDate();result = result * 59 + ($updateDate == null ? 43 : $updateDate.hashCode());Object $delFlag = getDelFlag();result = result * 59 + ($delFlag == null ? 43 : $delFlag.hashCode());Object $remarks = getRemarks();result = result * 59 + ($remarks == null ? 43 : $remarks.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseBuildRoom;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseBuildRoom)) {
            return false;
        }
        VeBaseBuildRoom other = (VeBaseBuildRoom)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$square = getSquare();Object other$square = other.getSquare();
        if (this$square == null ? other$square != null : !this$square.equals(other$square)) {
            return false;
        }
        Object this$capacity = getCapacity();Object other$capacity = other.getCapacity();
        if (this$capacity == null ? other$capacity != null : !this$capacity.equals(other$capacity)) {
            return false;
        }
        Object this$status = getStatus();Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) {
            return false;
        }
        Object this$floorNum = getFloorNum();Object other$floorNum = other.getFloorNum();
        if (this$floorNum == null ? other$floorNum != null : !this$floorNum.equals(other$floorNum)) {
            return false;
        }
        Object this$buildType = getBuildType();Object other$buildType = other.getBuildType();
        if (this$buildType == null ? other$buildType != null : !this$buildType.equals(other$buildType)) {
            return false;
        }
        Object this$buildTypeName = getBuildTypeName();Object other$buildTypeName = other.getBuildTypeName();
        if (this$buildTypeName == null ? other$buildTypeName != null : !this$buildTypeName.equals(other$buildTypeName)) {
            return false;
        }
        Object this$buildTid = getBuildTid();Object other$buildTid = other.getBuildTid();
        if (this$buildTid == null ? other$buildTid != null : !this$buildTid.equals(other$buildTid)) {
            return false;
        }
        Object this$buildName = getBuildName();Object other$buildName = other.getBuildName();
        if (this$buildName == null ? other$buildName != null : !this$buildName.equals(other$buildName)) {
            return false;
        }
        Object this$orgNo = getOrgNo();Object other$orgNo = other.getOrgNo();
        if (this$orgNo == null ? other$orgNo != null : !this$orgNo.equals(other$orgNo)) {
            return false;
        }
        Object this$orgName = getOrgName();Object other$orgName = other.getOrgName();
        if (this$orgName == null ? other$orgName != null : !this$orgName.equals(other$orgName)) {
            return false;
        }
        Object this$roomName = getRoomName();Object other$roomName = other.getRoomName();
        if (this$roomName == null ? other$roomName != null : !this$roomName.equals(other$roomName)) {
            return false;
        }
        Object this$roomPurpose = getRoomPurpose();Object other$roomPurpose = other.getRoomPurpose();
        if (this$roomPurpose == null ? other$roomPurpose != null : !this$roomPurpose.equals(other$roomPurpose)) {
            return false;
        }
        Object this$roomPurposeName = getRoomPurposeName();Object other$roomPurposeName = other.getRoomPurposeName();
        if (this$roomPurposeName == null ? other$roomPurposeName != null : !this$roomPurposeName.equals(other$roomPurposeName)) {
            return false;
        }
        Object this$roomType = getRoomType();Object other$roomType = other.getRoomType();
        if (this$roomType == null ? other$roomType != null : !this$roomType.equals(other$roomType)) {
            return false;
        }
        Object this$roomTypeName = getRoomTypeName();Object other$roomTypeName = other.getRoomTypeName();
        if (this$roomTypeName == null ? other$roomTypeName != null : !this$roomTypeName.equals(other$roomTypeName)) {
            return false;
        }
        Object this$createBy = getCreateBy();Object other$createBy = other.getCreateBy();
        if (this$createBy == null ? other$createBy != null : !this$createBy.equals(other$createBy)) {
            return false;
        }
        Object this$createDate = getCreateDate();Object other$createDate = other.getCreateDate();
        if (this$createDate == null ? other$createDate != null : !this$createDate.equals(other$createDate)) {
            return false;
        }
        Object this$updateBy = getUpdateBy();Object other$updateBy = other.getUpdateBy();
        if (this$updateBy == null ? other$updateBy != null : !this$updateBy.equals(other$updateBy)) {
            return false;
        }
        Object this$updateDate = getUpdateDate();Object other$updateDate = other.getUpdateDate();
        if (this$updateDate == null ? other$updateDate != null : !this$updateDate.equals(other$updateDate)) {
            return false;
        }
        Object this$delFlag = getDelFlag();Object other$delFlag = other.getDelFlag();
        if (this$delFlag == null ? other$delFlag != null : !this$delFlag.equals(other$delFlag)) {
            return false;
        }
        Object this$remarks = getRemarks();Object other$remarks = other.getRemarks();return this$remarks == null ? other$remarks == null : this$remarks.equals(other$remarks);
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getBuildType()
    {
        return this.buildType;
    }

    public String getBuildTypeName()
    {
        return this.buildTypeName;
    }

    public String getBuildTid()
    {
        return this.buildTid;
    }

    public String getBuildName()
    {
        return this.buildName;
    }

    public String getOrgNo()
    {
        return this.orgNo;
    }

    public String getOrgName()
    {
        return this.orgName;
    }

    public String getRoomName()
    {
        return this.roomName;
    }

    public String getRoomPurpose()
    {
        return this.roomPurpose;
    }

    public String getRoomPurposeName()
    {
        return this.roomPurposeName;
    }

    public String getRoomType()
    {
        return this.roomType;
    }

    public String getRoomTypeName()
    {
        return this.roomTypeName;
    }

    public Integer getSquare()
    {
        return this.square;
    }

    public Integer getCapacity()
    {
        return this.capacity;
    }

    public Integer getStatus()
    {
        return this.status;
    }

    public Integer getFloorNum()
    {
        return this.floorNum;
    }

    public String getCreateBy()
    {
        return this.createBy;
    }

    public Date getCreateDate()
    {
        return this.createDate;
    }

    public String getUpdateBy()
    {
        return this.updateBy;
    }

    public Date getUpdateDate()
    {
        return this.updateDate;
    }

    public String getDelFlag()
    {
        return this.delFlag;
    }

    public String getRemarks()
    {
        return this.remarks;
    }
}
