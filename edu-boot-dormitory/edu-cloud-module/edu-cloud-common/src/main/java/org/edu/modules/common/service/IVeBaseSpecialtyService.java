package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseSpecialty;

public abstract interface IVeBaseSpecialtyService
        extends IService<VeBaseSpecialty>
{
    public abstract List<VeBaseSpecialty> specialtyListByFalid(Integer paramInteger);

    public abstract List<Map<String, Object>> getSpecialtyPageList(VeBaseSpecialty paramVeBaseSpecialty);

    public abstract VeBaseSpecialty getSpecialtyByName(Integer paramInteger, String paramString);

    public abstract VeBaseSpecialty getSpecialtyByBH(Integer paramInteger, String paramString);

    public abstract List<Map<String, Object>> getSpecialtyStudentStatistics();

    public abstract List<Map<String, Object>> getSpecialtyTreeList();

    public abstract List<Map<String, Object>> getSpecialtyByZYMC(String paramString);

    public abstract int stopSpecialtyBatch(String[] paramArrayOfString);

    public abstract int deleteSpecialtyBatch(String[] paramArrayOfString);

    public abstract int deleteSpecialtyByFalId(String[] paramArrayOfString);

    public abstract int stopSpecialtyByFalId(String[] paramArrayOfString);

    public abstract int updateSpecialtyFalIdById(Integer paramInteger1, Integer paramInteger2);

    public abstract List<Map<String, Object>> getGraduatePeopleCount(String paramString);
}
