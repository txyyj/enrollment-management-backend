package org.edu.modules.common.config;

import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
@Component("pageHelperConfig")
public class MybatisPlusConfig
{
    @Bean
    ConfigurationCustomizer mybatisConfigurationCustomizer()
    {
        return new MybatisPlusConfig$1(this);
    }
}

