package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_kc")
@ApiModel(value="ve_base_kc对象", description="课程信息表")
public class VeBaseKC
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="课程号", width=15.0D)
    @ApiModelProperty("课程号")
    private String kch;
    @Excel(name="课程名称", width=15.0D)
    @ApiModelProperty("课程名称")
    private String kcmc;
    @Excel(name="课程英文名称", width=15.0D)
    @ApiModelProperty("课程英文名称")
    private String kcywmc;
    @Excel(name="学分", width=15.0D)
    @ApiModelProperty("学分")
    private String xf;
    @Excel(name="周学时", width=15.0D)
    @ApiModelProperty("周学时")
    private String zxs;
    @Excel(name="总学时", width=15.0D)
    @ApiModelProperty("总学时")
    private String sumXs;
    @Excel(name="理论学时", width=15.0D)
    @ApiModelProperty("理论学时")
    private String llxs;
    @Excel(name="实验学时", width=15.0D)
    @ApiModelProperty("实验学时")
    private String syxs;
    @Excel(name="课程简介", width=15.0D)
    @ApiModelProperty("课程简介")
    private String kcjj;
    @Excel(name="教材", width=15.0D)
    @ApiModelProperty("教材")
    private String jc;
    @Excel(name="参考书目", width=15.0D)
    @ApiModelProperty("参考书目")
    private String cksm;
    @Excel(name="课程开设单位号", width=15.0D)
    @ApiModelProperty("课程开设单位号")
    private String kcksdwh;
    @Excel(name="平台部门编号", width=15.0D)
    @ApiModelProperty("平台部门编号")
    private String platformSysOrgNo;
    @Excel(name="科目种类名称", width=15.0D)
    @ApiModelProperty("科目种类名称")
    private String courseSubjectName;
    @Excel(name="课程科目编号", width=15.0D)
    @ApiModelProperty("课程科目编号")
    private String courseSubjectId;
    @Excel(name="开课学期", width=15.0D)
    @ApiModelProperty("开课学期")
    private String termNumber;
    @Excel(name="学期学分", width=15.0D)
    @ApiModelProperty("学期学分")
    private String termCredit;
    @Excel(name="授课方式", width=15.0D)
    @ApiModelProperty("授课方式")
    private String teachingWay;
    @Excel(name="课时单位", width=15.0D)
    @ApiModelProperty("课时单位")
    private String hourUnit;
    @Excel(name="考核方式", width=15.0D)
    @ApiModelProperty("考核方式")
    private String assessWay;
    @Excel(name="创建方式", width=15.0D)
    @ApiModelProperty("创建方式")
    private String createBy;
    @Excel(name="创建日期", width=15.0D)
    @ApiModelProperty("创建日期")
    private String createDate;
    @Excel(name="修改方式", width=15.0D)
    @ApiModelProperty("修改方式")
    private String uodateBy;
    @Excel(name="修改日期", width=15.0D)
    @ApiModelProperty("修改日期")
    private String updateDate;
    @Excel(name="是否删除标志", width=15.0D)
    @ApiModelProperty("是否删除标志")
    private String delFlag;
    @Excel(name="备注 ", width=15.0D)
    @ApiModelProperty("备注 ")
    private String remarks;

    public VeBaseKC setKcmc(String kcmc)
    {
        this.kcmc = kcmc;return this;
    }

    public VeBaseKC setKch(String kch)
    {
        this.kch = kch;return this;
    }

    public VeBaseKC setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseKC(id=" + getId() + ", kch=" + getKch() + ", kcmc=" + getKcmc() + ", kcywmc=" + getKcywmc() + ", xf=" + getXf() + ", zxs=" + getZxs() + ", sumXs=" + getSumXs() + ", llxs=" + getLlxs() + ", syxs=" + getSyxs() + ", kcjj=" + getKcjj() + ", jc=" + getJc() + ", cksm=" + getCksm() + ", kcksdwh=" + getKcksdwh() + ", platformSysOrgNo=" + getPlatformSysOrgNo() + ", courseSubjectName=" + getCourseSubjectName() + ", courseSubjectId=" + getCourseSubjectId() + ", termNumber=" + getTermNumber() + ", termCredit=" + getTermCredit() + ", teachingWay=" + getTeachingWay() + ", hourUnit=" + getHourUnit() + ", assessWay=" + getAssessWay() + ", createBy=" + getCreateBy() + ", createDate=" + getCreateDate() + ", uodateBy=" + getUodateBy() + ", updateDate=" + getUpdateDate() + ", delFlag=" + getDelFlag() + ", remarks=" + getRemarks() + ")";
    }

    public VeBaseKC setRemarks(String remarks)
    {
        this.remarks = remarks;return this;
    }

    public VeBaseKC setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;return this;
    }

    public VeBaseKC setUpdateDate(String updateDate)
    {
        this.updateDate = updateDate;return this;
    }

    public VeBaseKC setUodateBy(String uodateBy)
    {
        this.uodateBy = uodateBy;return this;
    }

    public VeBaseKC setCreateDate(String createDate)
    {
        this.createDate = createDate;return this;
    }

    public VeBaseKC setCreateBy(String createBy)
    {
        this.createBy = createBy;return this;
    }

    public VeBaseKC setAssessWay(String assessWay)
    {
        this.assessWay = assessWay;return this;
    }

    public VeBaseKC setHourUnit(String hourUnit)
    {
        this.hourUnit = hourUnit;return this;
    }

    public VeBaseKC setTeachingWay(String teachingWay)
    {
        this.teachingWay = teachingWay;return this;
    }

    public VeBaseKC setTermCredit(String termCredit)
    {
        this.termCredit = termCredit;return this;
    }

    public VeBaseKC setTermNumber(String termNumber)
    {
        this.termNumber = termNumber;return this;
    }

    public VeBaseKC setCourseSubjectId(String courseSubjectId)
    {
        this.courseSubjectId = courseSubjectId;return this;
    }

    public VeBaseKC setCourseSubjectName(String courseSubjectName)
    {
        this.courseSubjectName = courseSubjectName;return this;
    }

    public VeBaseKC setPlatformSysOrgNo(String platformSysOrgNo)
    {
        this.platformSysOrgNo = platformSysOrgNo;return this;
    }

    public VeBaseKC setKcksdwh(String kcksdwh)
    {
        this.kcksdwh = kcksdwh;return this;
    }

    public VeBaseKC setCksm(String cksm)
    {
        this.cksm = cksm;return this;
    }

    public VeBaseKC setJc(String jc)
    {
        this.jc = jc;return this;
    }

    public VeBaseKC setKcjj(String kcjj)
    {
        this.kcjj = kcjj;return this;
    }

    public VeBaseKC setSyxs(String syxs)
    {
        this.syxs = syxs;return this;
    }

    public VeBaseKC setLlxs(String llxs)
    {
        this.llxs = llxs;return this;
    }

    public VeBaseKC setSumXs(String sumXs)
    {
        this.sumXs = sumXs;return this;
    }

    public VeBaseKC setZxs(String zxs)
    {
        this.zxs = zxs;return this;
    }

    public VeBaseKC setXf(String xf)
    {
        this.xf = xf;return this;
    }

    public VeBaseKC setKcywmc(String kcywmc)
    {
        this.kcywmc = kcywmc;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $kch = getKch();result = result * 59 + ($kch == null ? 43 : $kch.hashCode());Object $kcmc = getKcmc();result = result * 59 + ($kcmc == null ? 43 : $kcmc.hashCode());Object $kcywmc = getKcywmc();result = result * 59 + ($kcywmc == null ? 43 : $kcywmc.hashCode());Object $xf = getXf();result = result * 59 + ($xf == null ? 43 : $xf.hashCode());Object $zxs = getZxs();result = result * 59 + ($zxs == null ? 43 : $zxs.hashCode());Object $sumXs = getSumXs();result = result * 59 + ($sumXs == null ? 43 : $sumXs.hashCode());Object $llxs = getLlxs();result = result * 59 + ($llxs == null ? 43 : $llxs.hashCode());Object $syxs = getSyxs();result = result * 59 + ($syxs == null ? 43 : $syxs.hashCode());Object $kcjj = getKcjj();result = result * 59 + ($kcjj == null ? 43 : $kcjj.hashCode());Object $jc = getJc();result = result * 59 + ($jc == null ? 43 : $jc.hashCode());Object $cksm = getCksm();result = result * 59 + ($cksm == null ? 43 : $cksm.hashCode());Object $kcksdwh = getKcksdwh();result = result * 59 + ($kcksdwh == null ? 43 : $kcksdwh.hashCode());Object $platformSysOrgNo = getPlatformSysOrgNo();result = result * 59 + ($platformSysOrgNo == null ? 43 : $platformSysOrgNo.hashCode());Object $courseSubjectName = getCourseSubjectName();result = result * 59 + ($courseSubjectName == null ? 43 : $courseSubjectName.hashCode());Object $courseSubjectId = getCourseSubjectId();result = result * 59 + ($courseSubjectId == null ? 43 : $courseSubjectId.hashCode());Object $termNumber = getTermNumber();result = result * 59 + ($termNumber == null ? 43 : $termNumber.hashCode());Object $termCredit = getTermCredit();result = result * 59 + ($termCredit == null ? 43 : $termCredit.hashCode());Object $teachingWay = getTeachingWay();result = result * 59 + ($teachingWay == null ? 43 : $teachingWay.hashCode());Object $hourUnit = getHourUnit();result = result * 59 + ($hourUnit == null ? 43 : $hourUnit.hashCode());Object $assessWay = getAssessWay();result = result * 59 + ($assessWay == null ? 43 : $assessWay.hashCode());Object $createBy = getCreateBy();result = result * 59 + ($createBy == null ? 43 : $createBy.hashCode());Object $createDate = getCreateDate();result = result * 59 + ($createDate == null ? 43 : $createDate.hashCode());Object $uodateBy = getUodateBy();result = result * 59 + ($uodateBy == null ? 43 : $uodateBy.hashCode());Object $updateDate = getUpdateDate();result = result * 59 + ($updateDate == null ? 43 : $updateDate.hashCode());Object $delFlag = getDelFlag();result = result * 59 + ($delFlag == null ? 43 : $delFlag.hashCode());Object $remarks = getRemarks();result = result * 59 + ($remarks == null ? 43 : $remarks.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseKC;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseKC)) {
            return false;
        }
        VeBaseKC other = (VeBaseKC)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$kch = getKch();Object other$kch = other.getKch();
        if (this$kch == null ? other$kch != null : !this$kch.equals(other$kch)) {
            return false;
        }
        Object this$kcmc = getKcmc();Object other$kcmc = other.getKcmc();
        if (this$kcmc == null ? other$kcmc != null : !this$kcmc.equals(other$kcmc)) {
            return false;
        }
        Object this$kcywmc = getKcywmc();Object other$kcywmc = other.getKcywmc();
        if (this$kcywmc == null ? other$kcywmc != null : !this$kcywmc.equals(other$kcywmc)) {
            return false;
        }
        Object this$xf = getXf();Object other$xf = other.getXf();
        if (this$xf == null ? other$xf != null : !this$xf.equals(other$xf)) {
            return false;
        }
        Object this$zxs = getZxs();Object other$zxs = other.getZxs();
        if (this$zxs == null ? other$zxs != null : !this$zxs.equals(other$zxs)) {
            return false;
        }
        Object this$sumXs = getSumXs();Object other$sumXs = other.getSumXs();
        if (this$sumXs == null ? other$sumXs != null : !this$sumXs.equals(other$sumXs)) {
            return false;
        }
        Object this$llxs = getLlxs();Object other$llxs = other.getLlxs();
        if (this$llxs == null ? other$llxs != null : !this$llxs.equals(other$llxs)) {
            return false;
        }
        Object this$syxs = getSyxs();Object other$syxs = other.getSyxs();
        if (this$syxs == null ? other$syxs != null : !this$syxs.equals(other$syxs)) {
            return false;
        }
        Object this$kcjj = getKcjj();Object other$kcjj = other.getKcjj();
        if (this$kcjj == null ? other$kcjj != null : !this$kcjj.equals(other$kcjj)) {
            return false;
        }
        Object this$jc = getJc();Object other$jc = other.getJc();
        if (this$jc == null ? other$jc != null : !this$jc.equals(other$jc)) {
            return false;
        }
        Object this$cksm = getCksm();Object other$cksm = other.getCksm();
        if (this$cksm == null ? other$cksm != null : !this$cksm.equals(other$cksm)) {
            return false;
        }
        Object this$kcksdwh = getKcksdwh();Object other$kcksdwh = other.getKcksdwh();
        if (this$kcksdwh == null ? other$kcksdwh != null : !this$kcksdwh.equals(other$kcksdwh)) {
            return false;
        }
        Object this$platformSysOrgNo = getPlatformSysOrgNo();Object other$platformSysOrgNo = other.getPlatformSysOrgNo();
        if (this$platformSysOrgNo == null ? other$platformSysOrgNo != null : !this$platformSysOrgNo.equals(other$platformSysOrgNo)) {
            return false;
        }
        Object this$courseSubjectName = getCourseSubjectName();Object other$courseSubjectName = other.getCourseSubjectName();
        if (this$courseSubjectName == null ? other$courseSubjectName != null : !this$courseSubjectName.equals(other$courseSubjectName)) {
            return false;
        }
        Object this$courseSubjectId = getCourseSubjectId();Object other$courseSubjectId = other.getCourseSubjectId();
        if (this$courseSubjectId == null ? other$courseSubjectId != null : !this$courseSubjectId.equals(other$courseSubjectId)) {
            return false;
        }
        Object this$termNumber = getTermNumber();Object other$termNumber = other.getTermNumber();
        if (this$termNumber == null ? other$termNumber != null : !this$termNumber.equals(other$termNumber)) {
            return false;
        }
        Object this$termCredit = getTermCredit();Object other$termCredit = other.getTermCredit();
        if (this$termCredit == null ? other$termCredit != null : !this$termCredit.equals(other$termCredit)) {
            return false;
        }
        Object this$teachingWay = getTeachingWay();Object other$teachingWay = other.getTeachingWay();
        if (this$teachingWay == null ? other$teachingWay != null : !this$teachingWay.equals(other$teachingWay)) {
            return false;
        }
        Object this$hourUnit = getHourUnit();Object other$hourUnit = other.getHourUnit();
        if (this$hourUnit == null ? other$hourUnit != null : !this$hourUnit.equals(other$hourUnit)) {
            return false;
        }
        Object this$assessWay = getAssessWay();Object other$assessWay = other.getAssessWay();
        if (this$assessWay == null ? other$assessWay != null : !this$assessWay.equals(other$assessWay)) {
            return false;
        }
        Object this$createBy = getCreateBy();Object other$createBy = other.getCreateBy();
        if (this$createBy == null ? other$createBy != null : !this$createBy.equals(other$createBy)) {
            return false;
        }
        Object this$createDate = getCreateDate();Object other$createDate = other.getCreateDate();
        if (this$createDate == null ? other$createDate != null : !this$createDate.equals(other$createDate)) {
            return false;
        }
        Object this$uodateBy = getUodateBy();Object other$uodateBy = other.getUodateBy();
        if (this$uodateBy == null ? other$uodateBy != null : !this$uodateBy.equals(other$uodateBy)) {
            return false;
        }
        Object this$updateDate = getUpdateDate();Object other$updateDate = other.getUpdateDate();
        if (this$updateDate == null ? other$updateDate != null : !this$updateDate.equals(other$updateDate)) {
            return false;
        }
        Object this$delFlag = getDelFlag();Object other$delFlag = other.getDelFlag();
        if (this$delFlag == null ? other$delFlag != null : !this$delFlag.equals(other$delFlag)) {
            return false;
        }
        Object this$remarks = getRemarks();Object other$remarks = other.getRemarks();return this$remarks == null ? other$remarks == null : this$remarks.equals(other$remarks);
    }

    public String getId()
    {
        return this.id;
    }

    public String getKch()
    {
        return this.kch;
    }

    public String getKcmc()
    {
        return this.kcmc;
    }

    public String getKcywmc()
    {
        return this.kcywmc;
    }

    public String getXf()
    {
        return this.xf;
    }

    public String getZxs()
    {
        return this.zxs;
    }

    public String getSumXs()
    {
        return this.sumXs;
    }

    public String getLlxs()
    {
        return this.llxs;
    }

    public String getSyxs()
    {
        return this.syxs;
    }

    public String getKcjj()
    {
        return this.kcjj;
    }

    public String getJc()
    {
        return this.jc;
    }

    public String getCksm()
    {
        return this.cksm;
    }

    public String getKcksdwh()
    {
        return this.kcksdwh;
    }

    public String getPlatformSysOrgNo()
    {
        return this.platformSysOrgNo;
    }

    public String getCourseSubjectName()
    {
        return this.courseSubjectName;
    }

    public String getCourseSubjectId()
    {
        return this.courseSubjectId;
    }

    public String getTermNumber()
    {
        return this.termNumber;
    }

    public String getTermCredit()
    {
        return this.termCredit;
    }

    public String getTeachingWay()
    {
        return this.teachingWay;
    }

    public String getHourUnit()
    {
        return this.hourUnit;
    }

    public String getAssessWay()
    {
        return this.assessWay;
    }

    public String getCreateBy()
    {
        return this.createBy;
    }

    public String getCreateDate()
    {
        return this.createDate;
    }

    public String getUodateBy()
    {
        return this.uodateBy;
    }

    public String getUpdateDate()
    {
        return this.updateDate;
    }

    public String getDelFlag()
    {
        return this.delFlag;
    }

    public String getRemarks()
    {
        return this.remarks;
    }
}
