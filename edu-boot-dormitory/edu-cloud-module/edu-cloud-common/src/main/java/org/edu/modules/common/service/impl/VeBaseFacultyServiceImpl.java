package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.common.entity.VeBaseFaculty;
import org.edu.modules.common.mapper.VeBaseFacultyMapper;
import org.edu.modules.common.service.IVeBaseFacultyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @Auther 李少君
 * @Date 2021-12-13 16:26
 */
@Service
public class VeBaseFacultyServiceImpl extends ServiceImpl<VeBaseFacultyMapper, VeBaseFaculty> implements IVeBaseFacultyService {

    @Autowired
    private VeBaseFacultyMapper veBaseFacultyMapper;

    @Override
    public List<Map<String, Object>> getFacultyPageList(VeBaseFaculty paramVeBaseFaculty) {
        return veBaseFacultyMapper.getFacultyPageList(paramVeBaseFaculty);
    }

    @Override
    public VeBaseFaculty getFacultyByName(Integer paramInteger, String paramString) {
        return veBaseFacultyMapper.getFacultyByName(paramInteger, paramString);
    }

    @Override
    public VeBaseFaculty getFacultyByCode(Integer paramInteger, String paramString) {
        return veBaseFacultyMapper.getFacultyByCode(paramInteger, paramString);
    }

    @Override
    public List<Map<String, Object>> getTreeList() {
        return null;
    }
}
