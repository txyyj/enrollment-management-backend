package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_build")
@ApiModel(value="ve_base_build对象", description="楼栋信息表")
public class VeBaseBuild
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private Integer buildId;
    @Excel(name="教室楼栋名称", width=15.0D)
    @ApiModelProperty("教室楼栋名称")
    private String buildName;
    @Excel(name="校区id", width=15.0D)
    @ApiModelProperty("校区id")
    private Integer campusId;
    @Excel(name="创建方式", width=15.0D)
    @ApiModelProperty("创建方式")
    private String createBy;
    @Excel(name="创建日期", width=15.0D)
    @ApiModelProperty("创建日期")
    private Date createDate;
    @Excel(name="修改方式", width=15.0D)
    @ApiModelProperty("修改方式")
    private String updateBy;
    @Excel(name="修改日期", width=15.0D)
    @ApiModelProperty("修改日期")
    private Date updateDate;
    @Excel(name="是否删除标志", width=15.0D)
    @ApiModelProperty("是否删除标志")
    private String delFlag;
    @Excel(name="备注", width=15.0D)
    @ApiModelProperty("备注")
    private String remarks;

    public VeBaseBuild setCampusId(Integer campusId)
    {
        this.campusId = campusId;return this;
    }

    public VeBaseBuild setBuildName(String buildName)
    {
        this.buildName = buildName;return this;
    }

    public VeBaseBuild setBuildId(Integer buildId)
    {
        this.buildId = buildId;return this;
    }

    public String toString()
    {
        return "VeBaseBuild(buildId=" + getBuildId() + ", buildName=" + getBuildName() + ", campusId=" + getCampusId() + ", createBy=" + getCreateBy() + ", createDate=" + getCreateDate() + ", updateBy=" + getUpdateBy() + ", updateDate=" + getUpdateDate() + ", delFlag=" + getDelFlag() + ", remarks=" + getRemarks() + ")";
    }

    public VeBaseBuild setRemarks(String remarks)
    {
        this.remarks = remarks;return this;
    }

    public VeBaseBuild setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;return this;
    }

    public VeBaseBuild setUpdateDate(Date updateDate)
    {
        this.updateDate = updateDate;return this;
    }

    public VeBaseBuild setUpdateBy(String updateBy)
    {
        this.updateBy = updateBy;return this;
    }

    public VeBaseBuild setCreateDate(Date createDate)
    {
        this.createDate = createDate;return this;
    }

    public VeBaseBuild setCreateBy(String createBy)
    {
        this.createBy = createBy;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $buildId = getBuildId();result = result * 59 + ($buildId == null ? 43 : $buildId.hashCode());Object $campusId = getCampusId();result = result * 59 + ($campusId == null ? 43 : $campusId.hashCode());Object $buildName = getBuildName();result = result * 59 + ($buildName == null ? 43 : $buildName.hashCode());Object $createBy = getCreateBy();result = result * 59 + ($createBy == null ? 43 : $createBy.hashCode());Object $createDate = getCreateDate();result = result * 59 + ($createDate == null ? 43 : $createDate.hashCode());Object $updateBy = getUpdateBy();result = result * 59 + ($updateBy == null ? 43 : $updateBy.hashCode());Object $updateDate = getUpdateDate();result = result * 59 + ($updateDate == null ? 43 : $updateDate.hashCode());Object $delFlag = getDelFlag();result = result * 59 + ($delFlag == null ? 43 : $delFlag.hashCode());Object $remarks = getRemarks();result = result * 59 + ($remarks == null ? 43 : $remarks.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseBuild;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseBuild)) {
            return false;
        }
        VeBaseBuild other = (VeBaseBuild)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$buildId = getBuildId();Object other$buildId = other.getBuildId();
        if (this$buildId == null ? other$buildId != null : !this$buildId.equals(other$buildId)) {
            return false;
        }
        Object this$campusId = getCampusId();Object other$campusId = other.getCampusId();
        if (this$campusId == null ? other$campusId != null : !this$campusId.equals(other$campusId)) {
            return false;
        }
        Object this$buildName = getBuildName();Object other$buildName = other.getBuildName();
        if (this$buildName == null ? other$buildName != null : !this$buildName.equals(other$buildName)) {
            return false;
        }
        Object this$createBy = getCreateBy();Object other$createBy = other.getCreateBy();
        if (this$createBy == null ? other$createBy != null : !this$createBy.equals(other$createBy)) {
            return false;
        }
        Object this$createDate = getCreateDate();Object other$createDate = other.getCreateDate();
        if (this$createDate == null ? other$createDate != null : !this$createDate.equals(other$createDate)) {
            return false;
        }
        Object this$updateBy = getUpdateBy();Object other$updateBy = other.getUpdateBy();
        if (this$updateBy == null ? other$updateBy != null : !this$updateBy.equals(other$updateBy)) {
            return false;
        }
        Object this$updateDate = getUpdateDate();Object other$updateDate = other.getUpdateDate();
        if (this$updateDate == null ? other$updateDate != null : !this$updateDate.equals(other$updateDate)) {
            return false;
        }
        Object this$delFlag = getDelFlag();Object other$delFlag = other.getDelFlag();
        if (this$delFlag == null ? other$delFlag != null : !this$delFlag.equals(other$delFlag)) {
            return false;
        }
        Object this$remarks = getRemarks();Object other$remarks = other.getRemarks();return this$remarks == null ? other$remarks == null : this$remarks.equals(other$remarks);
    }

    public Integer getBuildId()
    {
        return this.buildId;
    }

    public String getBuildName()
    {
        return this.buildName;
    }

    public Integer getCampusId()
    {
        return this.campusId;
    }

    public String getCreateBy()
    {
        return this.createBy;
    }

    public Date getCreateDate()
    {
        return this.createDate;
    }

    public String getUpdateBy()
    {
        return this.updateBy;
    }

    public Date getUpdateDate()
    {
        return this.updateDate;
    }

    public String getDelFlag()
    {
        return this.delFlag;
    }

    public String getRemarks()
    {
        return this.remarks;
    }
}
