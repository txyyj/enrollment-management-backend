package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import org.edu.modules.common.entity.VeBaseCalendar;
import org.edu.modules.common.mapper.VeBaseCalendarMapper;
import org.edu.modules.common.service.IVeBaseCalendarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseCalendarServiceImpl
        extends ServiceImpl<VeBaseCalendarMapper, VeBaseCalendar>
        implements IVeBaseCalendarService
{
    @Autowired
    private VeBaseCalendarMapper veBaseCalendarMapper;

    public List<VeBaseCalendar> getCalendarListBySemId(Integer semId, Integer year, Integer month)
    {
        return this.veBaseCalendarMapper.getCalendarListBySemId(semId, year, month);
    }

    public VeBaseCalendar getCalenderByDates(String dates)
    {
        return this.veBaseCalendarMapper.getCalenderByDates(dates);
    }
}
