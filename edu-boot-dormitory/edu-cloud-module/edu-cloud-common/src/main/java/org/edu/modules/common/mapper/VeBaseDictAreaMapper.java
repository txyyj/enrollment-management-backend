package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.edu.modules.common.entity.VeBaseDictArea;

public abstract interface VeBaseDictAreaMapper
        extends BaseMapper<VeBaseDictArea>
{
    public abstract List<VeBaseDictArea> getProvinceList();

    public abstract List<VeBaseDictArea> getCityList(Integer paramInteger);

    public abstract List<VeBaseDictArea> getCountyList(Integer paramInteger);

    public abstract VeBaseDictArea getDictAreaByPidAndName(Integer paramInteger1, Integer paramInteger2, String paramString);
}
