package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_department")
@ApiModel(value="ve_base_department对象", description="部门信息表")
public class VeBaseDepartment
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="部门号", width=15.0D)
    @ApiModelProperty("部门号")
    private String jgh;
    @Excel(name="部门名称", width=15.0D)
    @ApiModelProperty("部门名称")
    private String jgmc;
    @Excel(name="部门英文名称", width=15.0D)
    @ApiModelProperty("部门英文名称")
    private String jgywmc;
    @Excel(name="机构简称", width=15.0D)
    @ApiModelProperty("机构简称")
    private String jgjc;
    @Excel(name="部门简拼", width=15.0D)
    @ApiModelProperty("部门简拼")
    private String jgjp;
    @Excel(name="部门地址", width=15.0D)
    @ApiModelProperty("部门地址")
    private String jgdz;
    @Excel(name="隶属上级机构", width=15.0D)
    @ApiModelProperty("隶属上级机构号")
    private String dssjjgh;
    @Excel(name="隶属上级机构id", width=15.0D)
    @ApiModelProperty("隶属上级机构id")
    private Integer pid;
    @Excel(name="节点路径", width=15.0D)
    @ApiModelProperty("节点路径")
    private String path;
    @Excel(name="隶属校区", width=15.0D)
    @ApiModelProperty("隶属校区号")
    private String lsxqh;
    @Excel(name="隶属校区id", width=15.0D)
    @ApiModelProperty("隶属校区id")
    private Integer campusId;
    @Excel(name="33", width=15.0D)
    @ApiModelProperty("建立年月")
    private Integer jlny;
    @Excel(name="邮政编码", width=15.0D)
    @ApiModelProperty("机构邮政编码")
    private String jgyzbm;
    @Excel(name="部门负责人工号", width=15.0D)
    @ApiModelProperty("部门负责人用户ID")
    private String fzrUserId;
    @Excel(name="电话号码", width=15.0D)
    @ApiModelProperty("机构电话号码")
    private String telephone;
    @Excel(name="终端", width=15.0D)
    @ApiModelProperty("终端")
    private Integer terminalid;
    @Excel(name="分管领导", width=15.0D)
    @ApiModelProperty("分管领导")
    private String fgldUserId;
    @TableField(exist=false)
    private List<VeBaseDepartment> children;
    @TableField(exist=false)
    private List<VeBaseTeacher> teacherList;
    @TableField(exist=false)
    @Excel(name="建立年月", width=15.0D)
    private String jlnyName;
    @TableField(exist=false)
    @Excel(name="部门负责人名称", width=15.0D)
    private String fzrName;
    @TableField(exist=false)
    @Excel(name="分管领导名称", width=15.0D)
    private String fgldName;
    @Excel(name="隶属校区名称", width=15.0D)
    @TableField(exist=false)
    private String campusName;
    @TableField(exist=false)
    private String pidName;

    public VeBaseDepartment setJgmc(String jgmc)
    {
        this.jgmc = jgmc;return this;
    }

    public VeBaseDepartment setJgh(String jgh)
    {
        this.jgh = jgh;return this;
    }

    public VeBaseDepartment setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseDepartment(id=" + getId() + ", jgh=" + getJgh() + ", jgmc=" + getJgmc() + ", jgywmc=" + getJgywmc() + ", jgjc=" + getJgjc() + ", jgjp=" + getJgjp() + ", jgdz=" + getJgdz() + ", dssjjgh=" + getDssjjgh() + ", pid=" + getPid() + ", path=" + getPath() + ", lsxqh=" + getLsxqh() + ", campusId=" + getCampusId() + ", jlny=" + getJlny() + ", jgyzbm=" + getJgyzbm() + ", fzrUserId=" + getFzrUserId() + ", telephone=" + getTelephone() + ", terminalid=" + getTerminalid() + ", fgldUserId=" + getFgldUserId() + ", children=" + getChildren() + ", teacherList=" + getTeacherList() + ", jlnyName=" + getJlnyName() + ", fzrName=" + getFzrName() + ", fgldName=" + getFgldName() + ", campusName=" + getCampusName() + ", pidName=" + getPidName() + ")";
    }

    public VeBaseDepartment setPidName(String pidName)
    {
        this.pidName = pidName;return this;
    }

    public VeBaseDepartment setCampusName(String campusName)
    {
        this.campusName = campusName;return this;
    }

    public VeBaseDepartment setFgldName(String fgldName)
    {
        this.fgldName = fgldName;return this;
    }

    public VeBaseDepartment setFzrName(String fzrName)
    {
        this.fzrName = fzrName;return this;
    }

    public VeBaseDepartment setJlnyName(String jlnyName)
    {
        this.jlnyName = jlnyName;return this;
    }

    public VeBaseDepartment setTeacherList(List<VeBaseTeacher> teacherList)
    {
        this.teacherList = teacherList;return this;
    }

    public VeBaseDepartment setChildren(List<VeBaseDepartment> children)
    {
        this.children = children;return this;
    }

    public VeBaseDepartment setFgldUserId(String fgldUserId)
    {
        this.fgldUserId = fgldUserId;return this;
    }

    public VeBaseDepartment setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeBaseDepartment setTelephone(String telephone)
    {
        this.telephone = telephone;return this;
    }

    public VeBaseDepartment setFzrUserId(String fzrUserId)
    {
        this.fzrUserId = fzrUserId;return this;
    }

    public VeBaseDepartment setJgyzbm(String jgyzbm)
    {
        this.jgyzbm = jgyzbm;return this;
    }

    public VeBaseDepartment setJlny(Integer jlny)
    {
        this.jlny = jlny;return this;
    }

    public VeBaseDepartment setCampusId(Integer campusId)
    {
        this.campusId = campusId;return this;
    }

    public VeBaseDepartment setLsxqh(String lsxqh)
    {
        this.lsxqh = lsxqh;return this;
    }

    public VeBaseDepartment setPath(String path)
    {
        this.path = path;return this;
    }

    public VeBaseDepartment setPid(Integer pid)
    {
        this.pid = pid;return this;
    }

    public VeBaseDepartment setDssjjgh(String dssjjgh)
    {
        this.dssjjgh = dssjjgh;return this;
    }

    public VeBaseDepartment setJgdz(String jgdz)
    {
        this.jgdz = jgdz;return this;
    }

    public VeBaseDepartment setJgjp(String jgjp)
    {
        this.jgjp = jgjp;return this;
    }

    public VeBaseDepartment setJgjc(String jgjc)
    {
        this.jgjc = jgjc;return this;
    }

    public VeBaseDepartment setJgywmc(String jgywmc)
    {
        this.jgywmc = jgywmc;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $pid = getPid();result = result * 59 + ($pid == null ? 43 : $pid.hashCode());Object $campusId = getCampusId();result = result * 59 + ($campusId == null ? 43 : $campusId.hashCode());Object $jlny = getJlny();result = result * 59 + ($jlny == null ? 43 : $jlny.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $jgh = getJgh();result = result * 59 + ($jgh == null ? 43 : $jgh.hashCode());Object $jgmc = getJgmc();result = result * 59 + ($jgmc == null ? 43 : $jgmc.hashCode());Object $jgywmc = getJgywmc();result = result * 59 + ($jgywmc == null ? 43 : $jgywmc.hashCode());Object $jgjc = getJgjc();result = result * 59 + ($jgjc == null ? 43 : $jgjc.hashCode());Object $jgjp = getJgjp();result = result * 59 + ($jgjp == null ? 43 : $jgjp.hashCode());Object $jgdz = getJgdz();result = result * 59 + ($jgdz == null ? 43 : $jgdz.hashCode());Object $dssjjgh = getDssjjgh();result = result * 59 + ($dssjjgh == null ? 43 : $dssjjgh.hashCode());Object $path = getPath();result = result * 59 + ($path == null ? 43 : $path.hashCode());Object $lsxqh = getLsxqh();result = result * 59 + ($lsxqh == null ? 43 : $lsxqh.hashCode());Object $jgyzbm = getJgyzbm();result = result * 59 + ($jgyzbm == null ? 43 : $jgyzbm.hashCode());Object $fzrUserId = getFzrUserId();result = result * 59 + ($fzrUserId == null ? 43 : $fzrUserId.hashCode());Object $telephone = getTelephone();result = result * 59 + ($telephone == null ? 43 : $telephone.hashCode());Object $fgldUserId = getFgldUserId();result = result * 59 + ($fgldUserId == null ? 43 : $fgldUserId.hashCode());Object $children = getChildren();result = result * 59 + ($children == null ? 43 : $children.hashCode());Object $teacherList = getTeacherList();result = result * 59 + ($teacherList == null ? 43 : $teacherList.hashCode());Object $jlnyName = getJlnyName();result = result * 59 + ($jlnyName == null ? 43 : $jlnyName.hashCode());Object $fzrName = getFzrName();result = result * 59 + ($fzrName == null ? 43 : $fzrName.hashCode());Object $fgldName = getFgldName();result = result * 59 + ($fgldName == null ? 43 : $fgldName.hashCode());Object $campusName = getCampusName();result = result * 59 + ($campusName == null ? 43 : $campusName.hashCode());Object $pidName = getPidName();result = result * 59 + ($pidName == null ? 43 : $pidName.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseDepartment;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseDepartment)) {
            return false;
        }
        VeBaseDepartment other = (VeBaseDepartment)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$pid = getPid();Object other$pid = other.getPid();
        if (this$pid == null ? other$pid != null : !this$pid.equals(other$pid)) {
            return false;
        }
        Object this$campusId = getCampusId();Object other$campusId = other.getCampusId();
        if (this$campusId == null ? other$campusId != null : !this$campusId.equals(other$campusId)) {
            return false;
        }
        Object this$jlny = getJlny();Object other$jlny = other.getJlny();
        if (this$jlny == null ? other$jlny != null : !this$jlny.equals(other$jlny)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$jgh = getJgh();Object other$jgh = other.getJgh();
        if (this$jgh == null ? other$jgh != null : !this$jgh.equals(other$jgh)) {
            return false;
        }
        Object this$jgmc = getJgmc();Object other$jgmc = other.getJgmc();
        if (this$jgmc == null ? other$jgmc != null : !this$jgmc.equals(other$jgmc)) {
            return false;
        }
        Object this$jgywmc = getJgywmc();Object other$jgywmc = other.getJgywmc();
        if (this$jgywmc == null ? other$jgywmc != null : !this$jgywmc.equals(other$jgywmc)) {
            return false;
        }
        Object this$jgjc = getJgjc();Object other$jgjc = other.getJgjc();
        if (this$jgjc == null ? other$jgjc != null : !this$jgjc.equals(other$jgjc)) {
            return false;
        }
        Object this$jgjp = getJgjp();Object other$jgjp = other.getJgjp();
        if (this$jgjp == null ? other$jgjp != null : !this$jgjp.equals(other$jgjp)) {
            return false;
        }
        Object this$jgdz = getJgdz();Object other$jgdz = other.getJgdz();
        if (this$jgdz == null ? other$jgdz != null : !this$jgdz.equals(other$jgdz)) {
            return false;
        }
        Object this$dssjjgh = getDssjjgh();Object other$dssjjgh = other.getDssjjgh();
        if (this$dssjjgh == null ? other$dssjjgh != null : !this$dssjjgh.equals(other$dssjjgh)) {
            return false;
        }
        Object this$path = getPath();Object other$path = other.getPath();
        if (this$path == null ? other$path != null : !this$path.equals(other$path)) {
            return false;
        }
        Object this$lsxqh = getLsxqh();Object other$lsxqh = other.getLsxqh();
        if (this$lsxqh == null ? other$lsxqh != null : !this$lsxqh.equals(other$lsxqh)) {
            return false;
        }
        Object this$jgyzbm = getJgyzbm();Object other$jgyzbm = other.getJgyzbm();
        if (this$jgyzbm == null ? other$jgyzbm != null : !this$jgyzbm.equals(other$jgyzbm)) {
            return false;
        }
        Object this$fzrUserId = getFzrUserId();Object other$fzrUserId = other.getFzrUserId();
        if (this$fzrUserId == null ? other$fzrUserId != null : !this$fzrUserId.equals(other$fzrUserId)) {
            return false;
        }
        Object this$telephone = getTelephone();Object other$telephone = other.getTelephone();
        if (this$telephone == null ? other$telephone != null : !this$telephone.equals(other$telephone)) {
            return false;
        }
        Object this$fgldUserId = getFgldUserId();Object other$fgldUserId = other.getFgldUserId();
        if (this$fgldUserId == null ? other$fgldUserId != null : !this$fgldUserId.equals(other$fgldUserId)) {
            return false;
        }
        Object this$children = getChildren();Object other$children = other.getChildren();
        if (this$children == null ? other$children != null : !this$children.equals(other$children)) {
            return false;
        }
        Object this$teacherList = getTeacherList();Object other$teacherList = other.getTeacherList();
        if (this$teacherList == null ? other$teacherList != null : !this$teacherList.equals(other$teacherList)) {
            return false;
        }
        Object this$jlnyName = getJlnyName();Object other$jlnyName = other.getJlnyName();
        if (this$jlnyName == null ? other$jlnyName != null : !this$jlnyName.equals(other$jlnyName)) {
            return false;
        }
        Object this$fzrName = getFzrName();Object other$fzrName = other.getFzrName();
        if (this$fzrName == null ? other$fzrName != null : !this$fzrName.equals(other$fzrName)) {
            return false;
        }
        Object this$fgldName = getFgldName();Object other$fgldName = other.getFgldName();
        if (this$fgldName == null ? other$fgldName != null : !this$fgldName.equals(other$fgldName)) {
            return false;
        }
        Object this$campusName = getCampusName();Object other$campusName = other.getCampusName();
        if (this$campusName == null ? other$campusName != null : !this$campusName.equals(other$campusName)) {
            return false;
        }
        Object this$pidName = getPidName();Object other$pidName = other.getPidName();return this$pidName == null ? other$pidName == null : this$pidName.equals(other$pidName);
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getJgh()
    {
        return this.jgh;
    }

    public String getJgmc()
    {
        return this.jgmc;
    }

    public String getJgywmc()
    {
        return this.jgywmc;
    }

    public String getJgjc()
    {
        return this.jgjc;
    }

    public String getJgjp()
    {
        return this.jgjp;
    }

    public String getJgdz()
    {
        return this.jgdz;
    }

    public String getDssjjgh()
    {
        return this.dssjjgh;
    }

    public Integer getPid()
    {
        return this.pid;
    }

    public String getPath()
    {
        return this.path;
    }

    public String getLsxqh()
    {
        return this.lsxqh;
    }

    public Integer getCampusId()
    {
        return this.campusId;
    }

    public Integer getJlny()
    {
        return this.jlny;
    }

    public String getJgyzbm()
    {
        return this.jgyzbm;
    }

    public String getFzrUserId()
    {
        return this.fzrUserId;
    }

    public String getTelephone()
    {
        return this.telephone;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public String getFgldUserId()
    {
        return this.fgldUserId;
    }

    public List<VeBaseDepartment> getChildren()
    {
        return this.children;
    }

    public List<VeBaseTeacher> getTeacherList()
    {
        return this.teacherList;
    }

    public String getJlnyName()
    {
        return this.jlnyName;
    }

    public String getFzrName()
    {
        return this.fzrName;
    }

    public String getFgldName()
    {
        return this.fgldName;
    }

    public String getCampusName()
    {
        return this.campusName;
    }

    public String getPidName()
    {
        return this.pidName;
    }
}
