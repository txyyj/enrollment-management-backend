package org.edu.modules.common.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import org.jeecgframework.poi.excel.annotation.Excel;

public class VoStudentAndStudentInfo
{
    @ApiModelProperty("id")
    @TableId(type=IdType.AUTO)
    private Integer id;
    @Excel(name="身份证号，唯一", width=15.0D)
    @ApiModelProperty("身份证号，唯一")
    private String sfzh;
    @Excel(name="学号", width=15.0D)
    @ApiModelProperty("学号")
    private String xh;
    @Excel(name="姓名", width=15.0D)
    @ApiModelProperty("姓名")
    private String xm;
    @Excel(name="性别", width=15.0D)
    @ApiModelProperty("性别码1男生2女生")
    private String xbm;
    @ApiModelProperty("用户ID")
    private String userId;
    @Excel(name="民族", width=15.0D)
    @ApiModelProperty("民族码")
    private String mzm;
    @Excel(name="报名号", width=15.0D)
    @ApiModelProperty("报名号")
    private String bmh;
    @Excel(name="就读方式", width=15.0D)
    @ApiModelProperty("就读方式：1住校，2走读")
    private Integer jdfs;
    @Excel(name="当前状态码", width=15.0D)
    @ApiModelProperty("当前状态码'XS'=>'新生', 'ZX' => '在校', 'XX' => '休学', 'TX' => '退学', 'KC' => '开除', 'BY' => '毕业', 'YY' => '肄业', 'ZXX' => '转学', 'JY' => '结业'")
    private String xsdqztm;
    @Excel(name="入学年月", width=15.0D)
    @ApiModelProperty("入学年月")
    private Long rxny;
    @Excel(name="学制", width=15.0D)
    @ApiModelProperty("学制;与学制表关联")
    private Integer xz;
    @Excel(name="院系ID", width=15.0D)
    @ApiModelProperty("院系ID")
    private Integer falId;
    @Excel(name="专业ID", width=15.0D)
    @ApiModelProperty("专业ID")
    private Integer specId;
    @Excel(name="班级ID", width=15.0D)
    @ApiModelProperty("班级ID")
    private Integer bjId;
    @Excel(name="年级ID", width=15.0D)
    @ApiModelProperty("年级ID")
    private Integer gradeId;
    @Excel(name="创建时间", width=15.0D)
    @ApiModelProperty("创建时间")
    private Integer createTime;
    @Excel(name="更新时间", width=15.0D)
    @ApiModelProperty("更新时间")
    private Integer updateTime;
    @Excel(name="户口所在省份", width=15.0D)
    @ApiModelProperty("户口所在省份")
    private String province;
    @Excel(name="户口所在省份ID", width=15.0D)
    @ApiModelProperty("户口所在省份ID")
    private Integer provinceId;
    @Excel(name="户口所在市", width=15.0D)
    @ApiModelProperty("户口所在市")
    private String city;
    @Excel(name="户口所在市Id", width=15.0D)
    @ApiModelProperty("户口所在市Id")
    private Integer cityId;
    @Excel(name="户口所在区", width=15.0D)
    @ApiModelProperty("户口所在区")
    private String county;
    @Excel(name="户口所在区ID", width=15.0D)
    @ApiModelProperty("户口所在区ID")
    private Integer countyId;
    @Excel(name="生源地省id", width=15.0D)
    @ApiModelProperty("生源地省id")
    private Integer shengId;
    @Excel(name="生源地市id", width=15.0D)
    @ApiModelProperty("生源地市id")
    private Integer shiId;
    @Excel(name="生源地区id", width=15.0D)
    @ApiModelProperty("生源地区id")
    private Integer quId;
    @Excel(name="是否是困难生", width=15.0D)
    @ApiModelProperty("是否是困难生 0=否  1=是")
    private Integer sfkns;
    @Excel(name="终端ID", width=15.0D)
    @ApiModelProperty("终端ID")
    private Integer terminalId;
    @Excel(name="准考证号", width=15.0D)
    @ApiModelProperty("准考证号")
    private String zkzh;
    @Excel(name="考生号", width=15.0D)
    @ApiModelProperty("考生号")
    private String ksh;
    @Excel(name="更新状态", width=15.0D)
    @ApiModelProperty("更新状态（0：未更新; 1：已更新）")
    private Integer updateStatus;
    @ApiModelProperty("stuId")
    private Integer stuId;
    @Excel(name="曾用名", width=15.0D)
    @ApiModelProperty("曾用名")
    private String cym;
    @Excel(name="出生日期", width=15.0D)
    @ApiModelProperty("出生日期")
    private Integer csrq;
    @Excel(name="籍贯", width=15.0D)
    @ApiModelProperty("籍贯")
    private String jg;
    @Excel(name="政治面貌码", width=15.0D)
    @ApiModelProperty("政治面貌码")
    private String zzmmm;
    @Excel(name="照片", width=15.0D)
    @ApiModelProperty("照片")
    private String zp;
    @Excel(name="毕业照片", width=15.0D)
    @ApiModelProperty("毕业照片")
    private String byzp;
    @Excel(name="毕业学校", width=15.0D)
    @ApiModelProperty("毕业学校")
    private String byxx;
    @Excel(name="报名方式", width=15.0D)
    @ApiModelProperty("报名方式")
    private String bmfsm;
    @Excel(name="入学成绩", width=15.0D)
    @ApiModelProperty("入学成绩")
    private Double rxcj;
    @Excel(name="学生联系电话", width=15.0D)
    @ApiModelProperty("学生联系电话")
    private String xslxdh;
    @Excel(name="家庭联系电话", width=15.0D)
    @ApiModelProperty("家庭联系电话")
    private String jtlxdh;
    @Excel(name="电子信箱", width=15.0D)
    @ApiModelProperty("电子信箱")
    private String dzxx;
    @Excel(name="家庭地址", width=15.0D)
    @ApiModelProperty("家庭地址")
    private String jtdz;
    @Excel(name="是否是流动", width=15.0D)
    @ApiModelProperty("是否是流动")
    private String sfsldrk;
    @Excel(name="是否低保", width=15.0D)
    @ApiModelProperty("是否低保0否1是")
    private Integer sfdb;
    @Excel(name="特长", width=15.0D)
    @ApiModelProperty("特长")
    private String tc;
    @Excel(name="健康状况码", width=15.0D)
    @ApiModelProperty("健康状况码")
    private String jkzkm;
    @Excel(name="户口类别码", width=15.0D)
    @ApiModelProperty("户口类别码")
    private String hklbm;
    @TableField(exist=false)
    @ApiModelProperty("出生日期")
    private String csrqName;
    @TableField(exist=false)
    @ApiModelProperty("入学年月")
    private String rxnyName;
    @ApiModelProperty("接口用户id")
    private String interfaceUserId;

    public void setXh(String xh)
    {
        this.xh = xh;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $jdfs = getJdfs();result = result * 59 + ($jdfs == null ? 43 : $jdfs.hashCode());Object $rxny = getRxny();result = result * 59 + ($rxny == null ? 43 : $rxny.hashCode());Object $xz = getXz();result = result * 59 + ($xz == null ? 43 : $xz.hashCode());Object $falId = getFalId();result = result * 59 + ($falId == null ? 43 : $falId.hashCode());Object $specId = getSpecId();result = result * 59 + ($specId == null ? 43 : $specId.hashCode());Object $bjId = getBjId();result = result * 59 + ($bjId == null ? 43 : $bjId.hashCode());Object $gradeId = getGradeId();result = result * 59 + ($gradeId == null ? 43 : $gradeId.hashCode());Object $createTime = getCreateTime();result = result * 59 + ($createTime == null ? 43 : $createTime.hashCode());Object $updateTime = getUpdateTime();result = result * 59 + ($updateTime == null ? 43 : $updateTime.hashCode());Object $provinceId = getProvinceId();result = result * 59 + ($provinceId == null ? 43 : $provinceId.hashCode());Object $cityId = getCityId();result = result * 59 + ($cityId == null ? 43 : $cityId.hashCode());Object $countyId = getCountyId();result = result * 59 + ($countyId == null ? 43 : $countyId.hashCode());Object $shengId = getShengId();result = result * 59 + ($shengId == null ? 43 : $shengId.hashCode());Object $shiId = getShiId();result = result * 59 + ($shiId == null ? 43 : $shiId.hashCode());Object $quId = getQuId();result = result * 59 + ($quId == null ? 43 : $quId.hashCode());Object $sfkns = getSfkns();result = result * 59 + ($sfkns == null ? 43 : $sfkns.hashCode());Object $terminalId = getTerminalId();result = result * 59 + ($terminalId == null ? 43 : $terminalId.hashCode());Object $updateStatus = getUpdateStatus();result = result * 59 + ($updateStatus == null ? 43 : $updateStatus.hashCode());Object $stuId = getStuId();result = result * 59 + ($stuId == null ? 43 : $stuId.hashCode());Object $csrq = getCsrq();result = result * 59 + ($csrq == null ? 43 : $csrq.hashCode());Object $rxcj = getRxcj();result = result * 59 + ($rxcj == null ? 43 : $rxcj.hashCode());Object $sfdb = getSfdb();result = result * 59 + ($sfdb == null ? 43 : $sfdb.hashCode());Object $sfzh = getSfzh();result = result * 59 + ($sfzh == null ? 43 : $sfzh.hashCode());Object $xh = getXh();result = result * 59 + ($xh == null ? 43 : $xh.hashCode());Object $xm = getXm();result = result * 59 + ($xm == null ? 43 : $xm.hashCode());Object $xbm = getXbm();result = result * 59 + ($xbm == null ? 43 : $xbm.hashCode());Object $userId = getUserId();result = result * 59 + ($userId == null ? 43 : $userId.hashCode());Object $mzm = getMzm();result = result * 59 + ($mzm == null ? 43 : $mzm.hashCode());Object $bmh = getBmh();result = result * 59 + ($bmh == null ? 43 : $bmh.hashCode());Object $xsdqztm = getXsdqztm();result = result * 59 + ($xsdqztm == null ? 43 : $xsdqztm.hashCode());Object $province = getProvince();result = result * 59 + ($province == null ? 43 : $province.hashCode());Object $city = getCity();result = result * 59 + ($city == null ? 43 : $city.hashCode());Object $county = getCounty();result = result * 59 + ($county == null ? 43 : $county.hashCode());Object $zkzh = getZkzh();result = result * 59 + ($zkzh == null ? 43 : $zkzh.hashCode());Object $ksh = getKsh();result = result * 59 + ($ksh == null ? 43 : $ksh.hashCode());Object $cym = getCym();result = result * 59 + ($cym == null ? 43 : $cym.hashCode());Object $jg = getJg();result = result * 59 + ($jg == null ? 43 : $jg.hashCode());Object $zzmmm = getZzmmm();result = result * 59 + ($zzmmm == null ? 43 : $zzmmm.hashCode());Object $zp = getZp();result = result * 59 + ($zp == null ? 43 : $zp.hashCode());Object $byzp = getByzp();result = result * 59 + ($byzp == null ? 43 : $byzp.hashCode());Object $byxx = getByxx();result = result * 59 + ($byxx == null ? 43 : $byxx.hashCode());Object $bmfsm = getBmfsm();result = result * 59 + ($bmfsm == null ? 43 : $bmfsm.hashCode());Object $xslxdh = getXslxdh();result = result * 59 + ($xslxdh == null ? 43 : $xslxdh.hashCode());Object $jtlxdh = getJtlxdh();result = result * 59 + ($jtlxdh == null ? 43 : $jtlxdh.hashCode());Object $dzxx = getDzxx();result = result * 59 + ($dzxx == null ? 43 : $dzxx.hashCode());Object $jtdz = getJtdz();result = result * 59 + ($jtdz == null ? 43 : $jtdz.hashCode());Object $sfsldrk = getSfsldrk();result = result * 59 + ($sfsldrk == null ? 43 : $sfsldrk.hashCode());Object $tc = getTc();result = result * 59 + ($tc == null ? 43 : $tc.hashCode());Object $jkzkm = getJkzkm();result = result * 59 + ($jkzkm == null ? 43 : $jkzkm.hashCode());Object $hklbm = getHklbm();result = result * 59 + ($hklbm == null ? 43 : $hklbm.hashCode());Object $csrqName = getCsrqName();result = result * 59 + ($csrqName == null ? 43 : $csrqName.hashCode());Object $rxnyName = getRxnyName();result = result * 59 + ($rxnyName == null ? 43 : $rxnyName.hashCode());Object $interfaceUserId = getInterfaceUserId();result = result * 59 + ($interfaceUserId == null ? 43 : $interfaceUserId.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VoStudentAndStudentInfo;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VoStudentAndStudentInfo)) {
            return false;
        }
        VoStudentAndStudentInfo other = (VoStudentAndStudentInfo)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$jdfs = getJdfs();Object other$jdfs = other.getJdfs();
        if (this$jdfs == null ? other$jdfs != null : !this$jdfs.equals(other$jdfs)) {
            return false;
        }
        Object this$rxny = getRxny();Object other$rxny = other.getRxny();
        if (this$rxny == null ? other$rxny != null : !this$rxny.equals(other$rxny)) {
            return false;
        }
        Object this$xz = getXz();Object other$xz = other.getXz();
        if (this$xz == null ? other$xz != null : !this$xz.equals(other$xz)) {
            return false;
        }
        Object this$falId = getFalId();Object other$falId = other.getFalId();
        if (this$falId == null ? other$falId != null : !this$falId.equals(other$falId)) {
            return false;
        }
        Object this$specId = getSpecId();Object other$specId = other.getSpecId();
        if (this$specId == null ? other$specId != null : !this$specId.equals(other$specId)) {
            return false;
        }
        Object this$bjId = getBjId();Object other$bjId = other.getBjId();
        if (this$bjId == null ? other$bjId != null : !this$bjId.equals(other$bjId)) {
            return false;
        }
        Object this$gradeId = getGradeId();Object other$gradeId = other.getGradeId();
        if (this$gradeId == null ? other$gradeId != null : !this$gradeId.equals(other$gradeId)) {
            return false;
        }
        Object this$createTime = getCreateTime();Object other$createTime = other.getCreateTime();
        if (this$createTime == null ? other$createTime != null : !this$createTime.equals(other$createTime)) {
            return false;
        }
        Object this$updateTime = getUpdateTime();Object other$updateTime = other.getUpdateTime();
        if (this$updateTime == null ? other$updateTime != null : !this$updateTime.equals(other$updateTime)) {
            return false;
        }
        Object this$provinceId = getProvinceId();Object other$provinceId = other.getProvinceId();
        if (this$provinceId == null ? other$provinceId != null : !this$provinceId.equals(other$provinceId)) {
            return false;
        }
        Object this$cityId = getCityId();Object other$cityId = other.getCityId();
        if (this$cityId == null ? other$cityId != null : !this$cityId.equals(other$cityId)) {
            return false;
        }
        Object this$countyId = getCountyId();Object other$countyId = other.getCountyId();
        if (this$countyId == null ? other$countyId != null : !this$countyId.equals(other$countyId)) {
            return false;
        }
        Object this$shengId = getShengId();Object other$shengId = other.getShengId();
        if (this$shengId == null ? other$shengId != null : !this$shengId.equals(other$shengId)) {
            return false;
        }
        Object this$shiId = getShiId();Object other$shiId = other.getShiId();
        if (this$shiId == null ? other$shiId != null : !this$shiId.equals(other$shiId)) {
            return false;
        }
        Object this$quId = getQuId();Object other$quId = other.getQuId();
        if (this$quId == null ? other$quId != null : !this$quId.equals(other$quId)) {
            return false;
        }
        Object this$sfkns = getSfkns();Object other$sfkns = other.getSfkns();
        if (this$sfkns == null ? other$sfkns != null : !this$sfkns.equals(other$sfkns)) {
            return false;
        }
        Object this$terminalId = getTerminalId();Object other$terminalId = other.getTerminalId();
        if (this$terminalId == null ? other$terminalId != null : !this$terminalId.equals(other$terminalId)) {
            return false;
        }
        Object this$updateStatus = getUpdateStatus();Object other$updateStatus = other.getUpdateStatus();
        if (this$updateStatus == null ? other$updateStatus != null : !this$updateStatus.equals(other$updateStatus)) {
            return false;
        }
        Object this$stuId = getStuId();Object other$stuId = other.getStuId();
        if (this$stuId == null ? other$stuId != null : !this$stuId.equals(other$stuId)) {
            return false;
        }
        Object this$csrq = getCsrq();Object other$csrq = other.getCsrq();
        if (this$csrq == null ? other$csrq != null : !this$csrq.equals(other$csrq)) {
            return false;
        }
        Object this$rxcj = getRxcj();Object other$rxcj = other.getRxcj();
        if (this$rxcj == null ? other$rxcj != null : !this$rxcj.equals(other$rxcj)) {
            return false;
        }
        Object this$sfdb = getSfdb();Object other$sfdb = other.getSfdb();
        if (this$sfdb == null ? other$sfdb != null : !this$sfdb.equals(other$sfdb)) {
            return false;
        }
        Object this$sfzh = getSfzh();Object other$sfzh = other.getSfzh();
        if (this$sfzh == null ? other$sfzh != null : !this$sfzh.equals(other$sfzh)) {
            return false;
        }
        Object this$xh = getXh();Object other$xh = other.getXh();
        if (this$xh == null ? other$xh != null : !this$xh.equals(other$xh)) {
            return false;
        }
        Object this$xm = getXm();Object other$xm = other.getXm();
        if (this$xm == null ? other$xm != null : !this$xm.equals(other$xm)) {
            return false;
        }
        Object this$xbm = getXbm();Object other$xbm = other.getXbm();
        if (this$xbm == null ? other$xbm != null : !this$xbm.equals(other$xbm)) {
            return false;
        }
        Object this$userId = getUserId();Object other$userId = other.getUserId();
        if (this$userId == null ? other$userId != null : !this$userId.equals(other$userId)) {
            return false;
        }
        Object this$mzm = getMzm();Object other$mzm = other.getMzm();
        if (this$mzm == null ? other$mzm != null : !this$mzm.equals(other$mzm)) {
            return false;
        }
        Object this$bmh = getBmh();Object other$bmh = other.getBmh();
        if (this$bmh == null ? other$bmh != null : !this$bmh.equals(other$bmh)) {
            return false;
        }
        Object this$xsdqztm = getXsdqztm();Object other$xsdqztm = other.getXsdqztm();
        if (this$xsdqztm == null ? other$xsdqztm != null : !this$xsdqztm.equals(other$xsdqztm)) {
            return false;
        }
        Object this$province = getProvince();Object other$province = other.getProvince();
        if (this$province == null ? other$province != null : !this$province.equals(other$province)) {
            return false;
        }
        Object this$city = getCity();Object other$city = other.getCity();
        if (this$city == null ? other$city != null : !this$city.equals(other$city)) {
            return false;
        }
        Object this$county = getCounty();Object other$county = other.getCounty();
        if (this$county == null ? other$county != null : !this$county.equals(other$county)) {
            return false;
        }
        Object this$zkzh = getZkzh();Object other$zkzh = other.getZkzh();
        if (this$zkzh == null ? other$zkzh != null : !this$zkzh.equals(other$zkzh)) {
            return false;
        }
        Object this$ksh = getKsh();Object other$ksh = other.getKsh();
        if (this$ksh == null ? other$ksh != null : !this$ksh.equals(other$ksh)) {
            return false;
        }
        Object this$cym = getCym();Object other$cym = other.getCym();
        if (this$cym == null ? other$cym != null : !this$cym.equals(other$cym)) {
            return false;
        }
        Object this$jg = getJg();Object other$jg = other.getJg();
        if (this$jg == null ? other$jg != null : !this$jg.equals(other$jg)) {
            return false;
        }
        Object this$zzmmm = getZzmmm();Object other$zzmmm = other.getZzmmm();
        if (this$zzmmm == null ? other$zzmmm != null : !this$zzmmm.equals(other$zzmmm)) {
            return false;
        }
        Object this$zp = getZp();Object other$zp = other.getZp();
        if (this$zp == null ? other$zp != null : !this$zp.equals(other$zp)) {
            return false;
        }
        Object this$byzp = getByzp();Object other$byzp = other.getByzp();
        if (this$byzp == null ? other$byzp != null : !this$byzp.equals(other$byzp)) {
            return false;
        }
        Object this$byxx = getByxx();Object other$byxx = other.getByxx();
        if (this$byxx == null ? other$byxx != null : !this$byxx.equals(other$byxx)) {
            return false;
        }
        Object this$bmfsm = getBmfsm();Object other$bmfsm = other.getBmfsm();
        if (this$bmfsm == null ? other$bmfsm != null : !this$bmfsm.equals(other$bmfsm)) {
            return false;
        }
        Object this$xslxdh = getXslxdh();Object other$xslxdh = other.getXslxdh();
        if (this$xslxdh == null ? other$xslxdh != null : !this$xslxdh.equals(other$xslxdh)) {
            return false;
        }
        Object this$jtlxdh = getJtlxdh();Object other$jtlxdh = other.getJtlxdh();
        if (this$jtlxdh == null ? other$jtlxdh != null : !this$jtlxdh.equals(other$jtlxdh)) {
            return false;
        }
        Object this$dzxx = getDzxx();Object other$dzxx = other.getDzxx();
        if (this$dzxx == null ? other$dzxx != null : !this$dzxx.equals(other$dzxx)) {
            return false;
        }
        Object this$jtdz = getJtdz();Object other$jtdz = other.getJtdz();
        if (this$jtdz == null ? other$jtdz != null : !this$jtdz.equals(other$jtdz)) {
            return false;
        }
        Object this$sfsldrk = getSfsldrk();Object other$sfsldrk = other.getSfsldrk();
        if (this$sfsldrk == null ? other$sfsldrk != null : !this$sfsldrk.equals(other$sfsldrk)) {
            return false;
        }
        Object this$tc = getTc();Object other$tc = other.getTc();
        if (this$tc == null ? other$tc != null : !this$tc.equals(other$tc)) {
            return false;
        }
        Object this$jkzkm = getJkzkm();Object other$jkzkm = other.getJkzkm();
        if (this$jkzkm == null ? other$jkzkm != null : !this$jkzkm.equals(other$jkzkm)) {
            return false;
        }
        Object this$hklbm = getHklbm();Object other$hklbm = other.getHklbm();
        if (this$hklbm == null ? other$hklbm != null : !this$hklbm.equals(other$hklbm)) {
            return false;
        }
        Object this$csrqName = getCsrqName();Object other$csrqName = other.getCsrqName();
        if (this$csrqName == null ? other$csrqName != null : !this$csrqName.equals(other$csrqName)) {
            return false;
        }
        Object this$rxnyName = getRxnyName();Object other$rxnyName = other.getRxnyName();
        if (this$rxnyName == null ? other$rxnyName != null : !this$rxnyName.equals(other$rxnyName)) {
            return false;
        }
        Object this$interfaceUserId = getInterfaceUserId();Object other$interfaceUserId = other.getInterfaceUserId();return this$interfaceUserId == null ? other$interfaceUserId == null : this$interfaceUserId.equals(other$interfaceUserId);
    }

    public void setInterfaceUserId(String interfaceUserId)
    {
        this.interfaceUserId = interfaceUserId;
    }

    public void setRxnyName(String rxnyName)
    {
        this.rxnyName = rxnyName;
    }

    public void setCsrqName(String csrqName)
    {
        this.csrqName = csrqName;
    }

    public void setHklbm(String hklbm)
    {
        this.hklbm = hklbm;
    }

    public void setJkzkm(String jkzkm)
    {
        this.jkzkm = jkzkm;
    }

    public void setTc(String tc)
    {
        this.tc = tc;
    }

    public void setSfdb(Integer sfdb)
    {
        this.sfdb = sfdb;
    }

    public void setSfsldrk(String sfsldrk)
    {
        this.sfsldrk = sfsldrk;
    }

    public void setJtdz(String jtdz)
    {
        this.jtdz = jtdz;
    }

    public void setDzxx(String dzxx)
    {
        this.dzxx = dzxx;
    }

    public void setJtlxdh(String jtlxdh)
    {
        this.jtlxdh = jtlxdh;
    }

    public void setXslxdh(String xslxdh)
    {
        this.xslxdh = xslxdh;
    }

    public void setRxcj(Double rxcj)
    {
        this.rxcj = rxcj;
    }

    public void setBmfsm(String bmfsm)
    {
        this.bmfsm = bmfsm;
    }

    public void setByxx(String byxx)
    {
        this.byxx = byxx;
    }

    public void setByzp(String byzp)
    {
        this.byzp = byzp;
    }

    public void setZp(String zp)
    {
        this.zp = zp;
    }

    public void setZzmmm(String zzmmm)
    {
        this.zzmmm = zzmmm;
    }

    public void setJg(String jg)
    {
        this.jg = jg;
    }

    public void setCsrq(Integer csrq)
    {
        this.csrq = csrq;
    }

    public void setCym(String cym)
    {
        this.cym = cym;
    }

    public void setStuId(Integer stuId)
    {
        this.stuId = stuId;
    }

    public void setUpdateStatus(Integer updateStatus)
    {
        this.updateStatus = updateStatus;
    }

    public void setKsh(String ksh)
    {
        this.ksh = ksh;
    }

    public void setZkzh(String zkzh)
    {
        this.zkzh = zkzh;
    }

    public void setTerminalId(Integer terminalId)
    {
        this.terminalId = terminalId;
    }

    public void setSfkns(Integer sfkns)
    {
        this.sfkns = sfkns;
    }

    public void setQuId(Integer quId)
    {
        this.quId = quId;
    }

    public void setShiId(Integer shiId)
    {
        this.shiId = shiId;
    }

    public void setShengId(Integer shengId)
    {
        this.shengId = shengId;
    }

    public void setCountyId(Integer countyId)
    {
        this.countyId = countyId;
    }

    public void setCounty(String county)
    {
        this.county = county;
    }

    public void setCityId(Integer cityId)
    {
        this.cityId = cityId;
    }

    public void setCity(String city)
    {
        this.city = city;
    }

    public void setProvinceId(Integer provinceId)
    {
        this.provinceId = provinceId;
    }

    public void setProvince(String province)
    {
        this.province = province;
    }

    public void setUpdateTime(Integer updateTime)
    {
        this.updateTime = updateTime;
    }

    public void setCreateTime(Integer createTime)
    {
        this.createTime = createTime;
    }

    public void setGradeId(Integer gradeId)
    {
        this.gradeId = gradeId;
    }

    public void setBjId(Integer bjId)
    {
        this.bjId = bjId;
    }

    public void setSpecId(Integer specId)
    {
        this.specId = specId;
    }

    public void setFalId(Integer falId)
    {
        this.falId = falId;
    }

    public void setXz(Integer xz)
    {
        this.xz = xz;
    }

    public void setRxny(Long rxny)
    {
        this.rxny = rxny;
    }

    public void setXsdqztm(String xsdqztm)
    {
        this.xsdqztm = xsdqztm;
    }

    public void setJdfs(Integer jdfs)
    {
        this.jdfs = jdfs;
    }

    public void setBmh(String bmh)
    {
        this.bmh = bmh;
    }

    public void setMzm(String mzm)
    {
        this.mzm = mzm;
    }

    public void setUserId(String userId)
    {
        this.userId = userId;
    }

    public void setXbm(String xbm)
    {
        this.xbm = xbm;
    }

    public void setId(Integer id)
    {
        this.id = id;
    }

    public void setSfzh(String sfzh)
    {
        this.sfzh = sfzh;
    }

    public String toString()
    {
        return "VoStudentAndStudentInfo(id=" + getId() + ", sfzh=" + getSfzh() + ", xh=" + getXh() + ", xm=" + getXm() + ", xbm=" + getXbm() + ", userId=" + getUserId() + ", mzm=" + getMzm() + ", bmh=" + getBmh() + ", jdfs=" + getJdfs() + ", xsdqztm=" + getXsdqztm() + ", rxny=" + getRxny() + ", xz=" + getXz() + ", falId=" + getFalId() + ", specId=" + getSpecId() + ", bjId=" + getBjId() + ", gradeId=" + getGradeId() + ", createTime=" + getCreateTime() + ", updateTime=" + getUpdateTime() + ", province=" + getProvince() + ", provinceId=" + getProvinceId() + ", city=" + getCity() + ", cityId=" + getCityId() + ", county=" + getCounty() + ", countyId=" + getCountyId() + ", shengId=" + getShengId() + ", shiId=" + getShiId() + ", quId=" + getQuId() + ", sfkns=" + getSfkns() + ", terminalId=" + getTerminalId() + ", zkzh=" + getZkzh() + ", ksh=" + getKsh() + ", updateStatus=" + getUpdateStatus() + ", stuId=" + getStuId() + ", cym=" + getCym() + ", csrq=" + getCsrq() + ", jg=" + getJg() + ", zzmmm=" + getZzmmm() + ", zp=" + getZp() + ", byzp=" + getByzp() + ", byxx=" + getByxx() + ", bmfsm=" + getBmfsm() + ", rxcj=" + getRxcj() + ", xslxdh=" + getXslxdh() + ", jtlxdh=" + getJtlxdh() + ", dzxx=" + getDzxx() + ", jtdz=" + getJtdz() + ", sfsldrk=" + getSfsldrk() + ", sfdb=" + getSfdb() + ", tc=" + getTc() + ", jkzkm=" + getJkzkm() + ", hklbm=" + getHklbm() + ", csrqName=" + getCsrqName() + ", rxnyName=" + getRxnyName() + ", interfaceUserId=" + getInterfaceUserId() + ")";
    }

    public void setXm(String xm)
    {
        this.xm = xm;
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getSfzh()
    {
        return this.sfzh;
    }

    public String getXh()
    {
        return this.xh;
    }

    public String getXm()
    {
        return this.xm;
    }

    public String getXbm()
    {
        return this.xbm;
    }

    public String getUserId()
    {
        return this.userId;
    }

    public String getMzm()
    {
        return this.mzm;
    }

    public String getBmh()
    {
        return this.bmh;
    }

    public Integer getJdfs()
    {
        return this.jdfs;
    }

    public String getXsdqztm()
    {
        return this.xsdqztm;
    }

    public Long getRxny()
    {
        return this.rxny;
    }

    public Integer getXz()
    {
        return this.xz;
    }

    public Integer getFalId()
    {
        return this.falId;
    }

    public Integer getSpecId()
    {
        return this.specId;
    }

    public Integer getBjId()
    {
        return this.bjId;
    }

    public Integer getGradeId()
    {
        return this.gradeId;
    }

    public Integer getCreateTime()
    {
        return this.createTime;
    }

    public Integer getUpdateTime()
    {
        return this.updateTime;
    }

    public String getProvince()
    {
        return this.province;
    }

    public Integer getProvinceId()
    {
        return this.provinceId;
    }

    public String getCity()
    {
        return this.city;
    }

    public Integer getCityId()
    {
        return this.cityId;
    }

    public String getCounty()
    {
        return this.county;
    }

    public Integer getCountyId()
    {
        return this.countyId;
    }

    public Integer getShengId()
    {
        return this.shengId;
    }

    public Integer getShiId()
    {
        return this.shiId;
    }

    public Integer getQuId()
    {
        return this.quId;
    }

    public Integer getSfkns()
    {
        return this.sfkns;
    }

    public Integer getTerminalId()
    {
        return this.terminalId;
    }

    public String getZkzh()
    {
        return this.zkzh;
    }

    public String getKsh()
    {
        return this.ksh;
    }

    public Integer getUpdateStatus()
    {
        return this.updateStatus;
    }

    public Integer getStuId()
    {
        return this.stuId;
    }

    public String getCym()
    {
        return this.cym;
    }

    public Integer getCsrq()
    {
        return this.csrq;
    }

    public String getJg()
    {
        return this.jg;
    }

    public String getZzmmm()
    {
        return this.zzmmm;
    }

    public String getZp()
    {
        return this.zp;
    }

    public String getByzp()
    {
        return this.byzp;
    }

    public String getByxx()
    {
        return this.byxx;
    }

    public String getBmfsm()
    {
        return this.bmfsm;
    }

    public Double getRxcj()
    {
        return this.rxcj;
    }

    public String getXslxdh()
    {
        return this.xslxdh;
    }

    public String getJtlxdh()
    {
        return this.jtlxdh;
    }

    public String getDzxx()
    {
        return this.dzxx;
    }

    public String getJtdz()
    {
        return this.jtdz;
    }

    public String getSfsldrk()
    {
        return this.sfsldrk;
    }

    public Integer getSfdb()
    {
        return this.sfdb;
    }

    public String getTc()
    {
        return this.tc;
    }

    public String getJkzkm()
    {
        return this.jkzkm;
    }

    public String getHklbm()
    {
        return this.hklbm;
    }

    public String getCsrqName()
    {
        return this.csrqName;
    }

    public String getRxnyName()
    {
        return this.rxnyName;
    }

    public String getInterfaceUserId()
    {
        return this.interfaceUserId;
    }
}
