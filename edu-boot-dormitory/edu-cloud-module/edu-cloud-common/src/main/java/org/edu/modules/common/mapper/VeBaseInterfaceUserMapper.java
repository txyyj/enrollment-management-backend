package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseInterfaceUser;

public abstract interface VeBaseInterfaceUserMapper
        extends BaseMapper<VeBaseInterfaceUser>
{
    public abstract List<Map<String, Object>> getInterfaceUserPageList(VeBaseInterfaceUser paramVeBaseInterfaceUser);

    public abstract List<Map<String, Object>> getInterfaceUserByNameAndPwd(String paramString1, String paramString2);

    public abstract List<Map<String, Object>> getInterfaceListByUserId(String paramString);

    public abstract List<Map<String, Object>> getListByName(String paramString);

    public abstract List<Map<String, Object>> getInterfaceUserByIdAndInterfaceName(String paramString1, String paramString2);

    public abstract int deleteInterfaceUserRelById(String paramString);

    public abstract int addInterfaceUserRelBatch(String paramString1, String[] paramArrayOfString, String paramString2);
}
