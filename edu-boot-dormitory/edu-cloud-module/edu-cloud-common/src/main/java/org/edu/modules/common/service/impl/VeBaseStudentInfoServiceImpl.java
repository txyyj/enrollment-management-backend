package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseStudentInfo;
import org.edu.modules.common.mapper.VeBaseStudentInfoMapper;
import org.edu.modules.common.service.IVeBaseStudentInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseStudentInfoServiceImpl
        extends ServiceImpl<VeBaseStudentInfoMapper, VeBaseStudentInfo>
        implements IVeBaseStudentInfoService
{
    @Autowired
    private VeBaseStudentInfoMapper veBaseStudentInfoMapper;

    public VeBaseStudentInfo getModelById(Integer id)
    {
        return this.veBaseStudentInfoMapper.getModelById(id);
    }

    public List<Map<String, Object>> getStudentPageList(VeBaseStudentInfo veBaseStudentInfo)
    {
        return this.veBaseStudentInfoMapper.getStudentPageList(veBaseStudentInfo);
    }
}
