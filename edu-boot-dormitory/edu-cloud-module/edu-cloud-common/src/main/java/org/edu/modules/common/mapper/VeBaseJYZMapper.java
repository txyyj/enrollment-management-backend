package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseJYZ;

public abstract interface VeBaseJYZMapper
        extends BaseMapper<VeBaseJYZ>
{
    public abstract List<Map<String, Object>> getJYZRootList();

    public abstract List<Map<String, Object>> getJYZBodyList();

    public abstract VeBaseJYZ getJYZByName(Integer paramInteger, String paramString);

    public abstract VeBaseJYZ getJYZByCode(Integer paramInteger, String paramString);

    public abstract Map getJYZById(Integer paramInteger);
}
