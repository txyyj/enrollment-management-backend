package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseKC;

public abstract interface IVeBaseKCService
        extends IService<VeBaseKC>
{
    public abstract List<Map<String, Object>> getKCPageList(VeBaseKC paramVeBaseKC);
}

