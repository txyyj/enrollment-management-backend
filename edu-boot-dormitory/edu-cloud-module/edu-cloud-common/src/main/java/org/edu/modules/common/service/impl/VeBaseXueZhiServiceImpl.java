package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseXueZhi;
import org.edu.modules.common.mapper.VeBaseXueZhiMapper;
import org.edu.modules.common.service.IVeBaseXueZhiService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseXueZhiServiceImpl
        extends ServiceImpl<VeBaseXueZhiMapper, VeBaseXueZhi>
        implements IVeBaseXueZhiService
{
    @Autowired
    private VeBaseXueZhiMapper veBaseXueZhiMapper;

    public List<Map<String, Object>> getXueZhiPageList(VeBaseXueZhi veBaseXueZhi)
    {
        return this.veBaseXueZhiMapper.getXueZhiPageList(veBaseXueZhi);
    }

    public VeBaseXueZhi getXueZhiByName(Integer id, String name)
    {
        return this.veBaseXueZhiMapper.getXueZhiByName(id, name);
    }
}
