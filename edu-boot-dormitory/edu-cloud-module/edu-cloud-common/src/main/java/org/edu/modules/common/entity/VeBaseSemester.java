package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_semester")
@ApiModel(value="ve_base_semester对象", description="学期信息表")
public class VeBaseSemester
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="学期码", width=15.0D)
    @ApiModelProperty("学期码")
    private String xqm;
    @Excel(name="学期名称", width=15.0D)
    @ApiModelProperty("学期名称")
    private String xqmc;
    @Excel(name="学年（度）", width=15.0D)
    @ApiModelProperty("学年（度）")
    private String xn;
    @Excel(name="学期开始日期", width=15.0D)
    @ApiModelProperty("学期开始日期")
    private Integer xqksrq;
    @Excel(name="学期结束日期", width=15.0D)
    @ApiModelProperty("学期结束日期")
    private Integer xqjsrq;
    @Excel(name="是否当前学期(0是, 1否)", width=15.0D)
    @ApiModelProperty("是否当前学期(0是, 1否)")
    private Integer iscurrent;
    @Excel(name="周数", width=15.0D)
    @ApiModelProperty("周数")
    private Integer weekNum;
    @Excel(name="终端系统ID", width=15.0D)
    @ApiModelProperty("终端系统ID")
    private Integer terminalid;
    @TableField(exist=false)
    @ApiModelProperty("学期开始日期")
    private String xqksrqName;
    @TableField(exist=false)
    @ApiModelProperty("学期结束日期")
    private String xqjsrqName;

    public VeBaseSemester setXqmc(String xqmc)
    {
        this.xqmc = xqmc;return this;
    }

    public VeBaseSemester setXqm(String xqm)
    {
        this.xqm = xqm;return this;
    }

    public VeBaseSemester setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseSemester(id=" + getId() + ", xqm=" + getXqm() + ", xqmc=" + getXqmc() + ", xn=" + getXn() + ", xqksrq=" + getXqksrq() + ", xqjsrq=" + getXqjsrq() + ", iscurrent=" + getIscurrent() + ", weekNum=" + getWeekNum() + ", terminalid=" + getTerminalid() + ", xqksrqName=" + getXqksrqName() + ", xqjsrqName=" + getXqjsrqName() + ")";
    }

    public VeBaseSemester setXqjsrqName(String xqjsrqName)
    {
        this.xqjsrqName = xqjsrqName;return this;
    }

    public VeBaseSemester setXqksrqName(String xqksrqName)
    {
        this.xqksrqName = xqksrqName;return this;
    }

    public VeBaseSemester setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeBaseSemester setWeekNum(Integer weekNum)
    {
        this.weekNum = weekNum;return this;
    }

    public VeBaseSemester setIscurrent(Integer iscurrent)
    {
        this.iscurrent = iscurrent;return this;
    }

    public VeBaseSemester setXqjsrq(Integer xqjsrq)
    {
        this.xqjsrq = xqjsrq;return this;
    }

    public VeBaseSemester setXqksrq(Integer xqksrq)
    {
        this.xqksrq = xqksrq;return this;
    }

    public VeBaseSemester setXn(String xn)
    {
        this.xn = xn;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $xqksrq = getXqksrq();result = result * 59 + ($xqksrq == null ? 43 : $xqksrq.hashCode());Object $xqjsrq = getXqjsrq();result = result * 59 + ($xqjsrq == null ? 43 : $xqjsrq.hashCode());Object $iscurrent = getIscurrent();result = result * 59 + ($iscurrent == null ? 43 : $iscurrent.hashCode());Object $weekNum = getWeekNum();result = result * 59 + ($weekNum == null ? 43 : $weekNum.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $xqm = getXqm();result = result * 59 + ($xqm == null ? 43 : $xqm.hashCode());Object $xqmc = getXqmc();result = result * 59 + ($xqmc == null ? 43 : $xqmc.hashCode());Object $xn = getXn();result = result * 59 + ($xn == null ? 43 : $xn.hashCode());Object $xqksrqName = getXqksrqName();result = result * 59 + ($xqksrqName == null ? 43 : $xqksrqName.hashCode());Object $xqjsrqName = getXqjsrqName();result = result * 59 + ($xqjsrqName == null ? 43 : $xqjsrqName.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseSemester;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseSemester)) {
            return false;
        }
        VeBaseSemester other = (VeBaseSemester)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$xqksrq = getXqksrq();Object other$xqksrq = other.getXqksrq();
        if (this$xqksrq == null ? other$xqksrq != null : !this$xqksrq.equals(other$xqksrq)) {
            return false;
        }
        Object this$xqjsrq = getXqjsrq();Object other$xqjsrq = other.getXqjsrq();
        if (this$xqjsrq == null ? other$xqjsrq != null : !this$xqjsrq.equals(other$xqjsrq)) {
            return false;
        }
        Object this$iscurrent = getIscurrent();Object other$iscurrent = other.getIscurrent();
        if (this$iscurrent == null ? other$iscurrent != null : !this$iscurrent.equals(other$iscurrent)) {
            return false;
        }
        Object this$weekNum = getWeekNum();Object other$weekNum = other.getWeekNum();
        if (this$weekNum == null ? other$weekNum != null : !this$weekNum.equals(other$weekNum)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$xqm = getXqm();Object other$xqm = other.getXqm();
        if (this$xqm == null ? other$xqm != null : !this$xqm.equals(other$xqm)) {
            return false;
        }
        Object this$xqmc = getXqmc();Object other$xqmc = other.getXqmc();
        if (this$xqmc == null ? other$xqmc != null : !this$xqmc.equals(other$xqmc)) {
            return false;
        }
        Object this$xn = getXn();Object other$xn = other.getXn();
        if (this$xn == null ? other$xn != null : !this$xn.equals(other$xn)) {
            return false;
        }
        Object this$xqksrqName = getXqksrqName();Object other$xqksrqName = other.getXqksrqName();
        if (this$xqksrqName == null ? other$xqksrqName != null : !this$xqksrqName.equals(other$xqksrqName)) {
            return false;
        }
        Object this$xqjsrqName = getXqjsrqName();Object other$xqjsrqName = other.getXqjsrqName();return this$xqjsrqName == null ? other$xqjsrqName == null : this$xqjsrqName.equals(other$xqjsrqName);
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getXqm()
    {
        return this.xqm;
    }

    public String getXqmc()
    {
        return this.xqmc;
    }

    public String getXn()
    {
        return this.xn;
    }

    public Integer getXqksrq()
    {
        return this.xqksrq;
    }

    public Integer getXqjsrq()
    {
        return this.xqjsrq;
    }

    public Integer getIscurrent()
    {
        return this.iscurrent;
    }

    public Integer getWeekNum()
    {
        return this.weekNum;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public String getXqksrqName()
    {
        return this.xqksrqName;
    }

    public String getXqjsrqName()
    {
        return this.xqjsrqName;
    }
}
