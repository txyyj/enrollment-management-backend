package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.annotations.Mapper;
import org.edu.modules.common.entity.VeBaseDataApplyColumn;

@Mapper
public abstract interface VeBaseDataApplyColumnMapper
        extends BaseMapper<VeBaseDataApplyColumn>
{
    public abstract List<Map<String, Object>> queryDataApplyColumnPageList(VeBaseDataApplyColumn paramVeBaseDataApplyColumn);

    public abstract int addDataApplyColumn(String paramString1, String paramString2, List<Map<String, Object>> paramList);

    public abstract int deleteDataApplyColumnByApplyId(String paramString);

    public abstract List<Map<String, Object>> getDataApplyColumnByApplyId(String paramString);

    public abstract List<Map<String, Object>> getCommonListByTableName(String paramString, List<String> paramList);
}
