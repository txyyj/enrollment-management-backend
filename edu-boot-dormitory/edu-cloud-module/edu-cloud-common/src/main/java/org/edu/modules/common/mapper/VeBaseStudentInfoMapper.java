package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseStudentInfo;

public abstract interface VeBaseStudentInfoMapper
        extends BaseMapper<VeBaseStudentInfo>
{
    public abstract VeBaseStudentInfo getModelById(Integer paramInteger);

    public abstract List<Map<String, Object>> getStudentPageList(VeBaseStudentInfo paramVeBaseStudentInfo);
}
