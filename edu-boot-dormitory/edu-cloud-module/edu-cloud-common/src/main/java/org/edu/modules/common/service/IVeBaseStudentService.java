package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseStudent;

public abstract interface IVeBaseStudentService
        extends IService<VeBaseStudent>
{
    public abstract VeBaseStudent getModelById(Integer paramInteger);

    public abstract List<Map<String, Object>> getStudentPageList(VeBaseStudent paramVeBaseStudent);

    public abstract Map getStudentStatusStatistics();

    public abstract VeBaseStudent getModelByUserId(String paramString);

    public abstract List<Map<String, Object>> getModelByName(String paramString);

    public abstract int addStudent(VeBaseStudent paramVeBaseStudent);

    public abstract VeBaseStudent getStudentBySFZH(Integer paramInteger, String paramString);

    public abstract VeBaseStudent getStudentByXH(Integer paramInteger, String paramString);

    public abstract Map getStudentAndInfoById(Integer paramInteger);

    public abstract List<Map<String, Object>> getStudentStatisticsByYear(String paramString);
}

