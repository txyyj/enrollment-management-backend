package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.common.entity.VeBaseFestival;

public abstract interface IVeBaseFestivalService
        extends IService<VeBaseFestival>
{
    public abstract List<VeBaseFestival> getFestivalBySemId(Integer paramInteger);
}
