package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseXJYD;
import org.edu.modules.common.mapper.VeBaseXJYDMapper;
import org.edu.modules.common.service.IVeBaseXJYDService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseXJYDServiceImpl
        extends ServiceImpl<VeBaseXJYDMapper, VeBaseXJYD>
        implements IVeBaseXJYDService
{
    @Autowired
    private VeBaseXJYDMapper veBaseXJYDMapper;

    public List<Map<String, Object>> getXJYDPageList(VeBaseXJYD veBaseXJYD)
    {
        return this.veBaseXJYDMapper.getXJYDPageList(veBaseXJYD);
    }
}
