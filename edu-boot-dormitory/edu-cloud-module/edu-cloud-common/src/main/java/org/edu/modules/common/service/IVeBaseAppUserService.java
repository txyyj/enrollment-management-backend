package org.edu.modules.common.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseAppUser;
import org.edu.modules.common.vo.VoAppUserBySearch;

public abstract interface IVeBaseAppUserService
        extends IService<VeBaseAppUser>
{
    public abstract int updateAppUserPasswordById(String paramString1, String paramString2, String paramString3);

    public abstract int addSysUser(VeBaseAppUser paramVeBaseAppUser);

    public abstract VeBaseAppUser getPwdAndSalt(VeBaseAppUser paramVeBaseAppUser);

    public abstract String pwdVerify(String paramString);

    public abstract VeBaseAppUser getAppUserByUserId(String paramString);

    public abstract Map getSysRoleUser(String paramString, Integer paramInteger);

    public abstract int addSysRoleUser(String paramString, Integer paramInteger);

    public abstract List<Map<String, Object>> queryOrganUserTreeList(String paramString);

    public abstract List<Map<String, Object>> getAppUserBySearch(VoAppUserBySearch paramVoAppUserBySearch);

    public abstract int updateAppUserByUserId(String paramString);

    public abstract int updateSysUserDel(String paramString);

    public abstract int deleteOrgan(String paramString);

    public abstract int deleteRole(String paramString);

    public abstract List<Map<String, Object>> queryResourcePageList();

    public abstract IPage<Map<String, Object>> getDataListByTableName(Page paramPage, String paramString);

    public abstract List<Map<String, Object>> queryColumnNameListByTableName(String paramString);

    public abstract List<Map<String, Object>> querySysRoleList();

    public abstract List<Map<String, Object>> queryAppUserTeacherList();

    public abstract int stopAppUserByUserId(String paramString);

    public abstract List<Map<String, Object>> queryAppManageList();

    public abstract List<Map<String, Object>> getUnreadMessageCountUserId();

    public abstract List<Map<String, Object>> queryAppUserListByRoleId(Integer paramInteger);
}

