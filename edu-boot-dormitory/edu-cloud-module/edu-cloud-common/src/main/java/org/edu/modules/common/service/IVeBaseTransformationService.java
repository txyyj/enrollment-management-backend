package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseTransformation;

public abstract interface IVeBaseTransformationService
        extends IService<VeBaseTransformation>
{
    public abstract List<Map<String, Object>> getTransformationPageList(VeBaseTransformation paramVeBaseTransformation);
}
