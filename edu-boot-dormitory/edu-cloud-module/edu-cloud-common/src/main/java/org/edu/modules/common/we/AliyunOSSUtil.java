package org.edu.modules.common.we;

import com.aliyun.oss.ClientException;
import com.aliyun.oss.OSSClient;
import com.aliyun.oss.OSSException;
import com.aliyun.oss.model.CannedAccessControlList;
import com.aliyun.oss.model.CreateBucketRequest;
import com.aliyun.oss.model.PutObjectRequest;
import com.aliyun.oss.model.PutObjectResult;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;
import org.edu.modules.common.mapper.VeBaseAppUserMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class AliyunOSSUtil
{
    @Autowired
    private ConstantConfig constantConfig;
    @Autowired
    private VeBaseAppUserMapper veBaseAppUserMapper;
    private static final Logger logger = LoggerFactory.getLogger(AliyunOSSUtil.class);

    public FileDTO upload(File file)
    {
        String endpoint = this.constantConfig.getEndpoint();
        String accessKeyId = this.constantConfig.getAccessKeyId();
        String accessKeySecret = this.constantConfig.getAccessKeySecret();
        String bucketName = this.constantConfig.getBucketName();
        String fileHost = this.constantConfig.getFolder();
        String webUrl = this.constantConfig.getWebUrl();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = simpleDateFormat.format(new Date());
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        String suffix = file.getName().substring(file.getName().lastIndexOf(".") + 1);
        if (file == null) {
            return null;
        }
        OSSClient client = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        try
        {
            if (!client.doesBucketExist(bucketName))
            {
                client.createBucket(bucketName);
                CreateBucketRequest createBucketRequest = new CreateBucketRequest(bucketName);
                createBucketRequest.setCannedACL(CannedAccessControlList.PublicRead);
                client.createBucket(createBucketRequest);
            }
            String fileUrl = fileHost + "/" + dateStr + "/" + uuid + "-" + file.getName();

            client.setBucketAcl(bucketName, CannedAccessControlList.PublicRead);

            PutObjectResult result = client.putObject(new PutObjectRequest(bucketName, fileUrl, file));
            if (result != null)
            {
                logger.info("------OSS文件上传成功------" + fileUrl);
                return new FileDTO(
                        Long.valueOf(file.length()), fileUrl, this.constantConfig

                        .getWebUrl() + "/" + fileUrl, suffix, "", bucketName, fileHost, "");
            }
        }
        catch (OSSException oe)
        {
            logger.error(oe.getMessage());
        }
        catch (ClientException ce)
        {
            logger.error(ce.getErrorMessage());
        }
        finally
        {
            if (client != null) {
                client.shutdown();
            }
        }
        return null;
    }

    public FileDTO uploadById(File file, String id)
    {
        String endpoint = this.constantConfig.getEndpoint();
        String accessKeyId = this.constantConfig.getAccessKeyId();
        String accessKeySecret = this.constantConfig.getAccessKeySecret();
        String bucketName = this.constantConfig.getBucketName();
        String fileHost = this.constantConfig.getFolder();
        String webUrl = this.constantConfig.getWebUrl();

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String dateStr = simpleDateFormat.format(new Date());
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        String suffix = file.getName().substring(file.getName().lastIndexOf(".") + 1);
        if (file == null) {
            return null;
        }
        OSSClient client = new OSSClient(endpoint, accessKeyId, accessKeySecret);
        try
        {
            if (!client.doesBucketExist(bucketName))
            {
                client.createBucket(bucketName);
                CreateBucketRequest createBucketRequest = new CreateBucketRequest(bucketName);
                createBucketRequest.setCannedACL(CannedAccessControlList.PublicRead);
                client.createBucket(createBucketRequest);
            }
            String fileUrl = fileHost + "/" + dateStr + "/" + uuid + "-" + file.getName();

            client.setBucketAcl(bucketName, CannedAccessControlList.PublicRead);

            PutObjectResult result = client.putObject(new PutObjectRequest(bucketName, fileUrl, file));
            if (result != null)
            {
                this.veBaseAppUserMapper.updateAppUserById(id, fileUrl);
                logger.info("------OSS文件上传成功------" + fileUrl);
                return new FileDTO(
                        Long.valueOf(file.length()), fileUrl, this.constantConfig

                        .getWebUrl() + "/" + fileUrl, suffix, "", bucketName, fileHost, "");
            }
        }
        catch (OSSException oe)
        {
            logger.error(oe.getMessage());
        }
        catch (ClientException ce)
        {
            logger.error(ce.getErrorMessage());
        }
        finally
        {
            if (client != null) {
                client.shutdown();
            }
        }
        return null;
    }
}

