package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_xjyd")
@ApiModel(value="ve_base_xjyd对象", description="学籍异动表")
public class VeBaseXJYD
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="学生id", width=15.0D)
    @ApiModelProperty("学生id")
    private String studentId;
    @Excel(name="学号", width=15.0D)
    @ApiModelProperty("学号")
    private String studentNumber;
    @Excel(name="姓名", width=15.0D)
    @ApiModelProperty("姓名")
    private String studentName;
    @Excel(name="异动类型", width=15.0D)
    @ApiModelProperty("异动类型")
    private String changeType;
    @Excel(name="异动类型值", width=15.0D)
    @ApiModelProperty("异动类型值")
    private String changeTypeValue;
    @Excel(name="异动原因", width=15.0D)
    @ApiModelProperty("异动原因")
    private String changeReasion;
    @Excel(name="异动前行政班名称", width=15.0D)
    @ApiModelProperty("异动前行政班名称")
    private String adminClassName;
    @Excel(name="异动前行政班id", width=15.0D)
    @ApiModelProperty("异动前行政班id")
    private String adminClassId;
    @Excel(name="生效时间", width=15.0D)
    @ApiModelProperty("生效时间")
    private String effectiveTime;
    @Excel(name="异动后行政班名称", width=15.0D)
    @ApiModelProperty("异动后行政班名称")
    private String afterClassName;
    @Excel(name="异动后行政班id", width=15.0D)
    @ApiModelProperty("异动后行政班id")
    private String afterClassId;
    @Excel(name="异动后专业名称", width=15.0D)
    @ApiModelProperty("异动后专业名称")
    private String afterSpecialtyName;
    @Excel(name="异动后专业代码", width=15.0D)
    @ApiModelProperty("异动后专业代码")
    private String afterSpecialtyCode;
    @Excel(name="创建方式", width=15.0D)
    @ApiModelProperty("创建方式")
    private String createBy;
    @Excel(name="创建日期", width=15.0D)
    @ApiModelProperty("创建日期")
    private Date createDate;
    @Excel(name="修改方式", width=15.0D)
    @ApiModelProperty("修改方式")
    private String updateBy;
    @Excel(name="修改日期", width=15.0D)
    @ApiModelProperty("修改日期")
    private Date updateDate;
    @Excel(name="是否删除标志", width=15.0D)
    @ApiModelProperty("是否删除标志")
    private String delFlag;
    @Excel(name="备注", width=15.0D)
    @ApiModelProperty("备注")
    private String remarks;

    public VeBaseXJYD setStudentNumber(String studentNumber)
    {
        this.studentNumber = studentNumber;return this;
    }

    public VeBaseXJYD setStudentId(String studentId)
    {
        this.studentId = studentId;return this;
    }

    public VeBaseXJYD setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseXJYD(id=" + getId() + ", studentId=" + getStudentId() + ", studentNumber=" + getStudentNumber() + ", studentName=" + getStudentName() + ", changeType=" + getChangeType() + ", changeTypeValue=" + getChangeTypeValue() + ", changeReasion=" + getChangeReasion() + ", adminClassName=" + getAdminClassName() + ", adminClassId=" + getAdminClassId() + ", effectiveTime=" + getEffectiveTime() + ", afterClassName=" + getAfterClassName() + ", afterClassId=" + getAfterClassId() + ", afterSpecialtyName=" + getAfterSpecialtyName() + ", afterSpecialtyCode=" + getAfterSpecialtyCode() + ", createBy=" + getCreateBy() + ", createDate=" + getCreateDate() + ", updateBy=" + getUpdateBy() + ", updateDate=" + getUpdateDate() + ", delFlag=" + getDelFlag() + ", remarks=" + getRemarks() + ")";
    }

    public VeBaseXJYD setRemarks(String remarks)
    {
        this.remarks = remarks;return this;
    }

    public VeBaseXJYD setDelFlag(String delFlag)
    {
        this.delFlag = delFlag;return this;
    }

    public VeBaseXJYD setUpdateDate(Date updateDate)
    {
        this.updateDate = updateDate;return this;
    }

    public VeBaseXJYD setUpdateBy(String updateBy)
    {
        this.updateBy = updateBy;return this;
    }

    public VeBaseXJYD setCreateDate(Date createDate)
    {
        this.createDate = createDate;return this;
    }

    public VeBaseXJYD setCreateBy(String createBy)
    {
        this.createBy = createBy;return this;
    }

    public VeBaseXJYD setAfterSpecialtyCode(String afterSpecialtyCode)
    {
        this.afterSpecialtyCode = afterSpecialtyCode;return this;
    }

    public VeBaseXJYD setAfterSpecialtyName(String afterSpecialtyName)
    {
        this.afterSpecialtyName = afterSpecialtyName;return this;
    }

    public VeBaseXJYD setAfterClassId(String afterClassId)
    {
        this.afterClassId = afterClassId;return this;
    }

    public VeBaseXJYD setAfterClassName(String afterClassName)
    {
        this.afterClassName = afterClassName;return this;
    }

    public VeBaseXJYD setEffectiveTime(String effectiveTime)
    {
        this.effectiveTime = effectiveTime;return this;
    }

    public VeBaseXJYD setAdminClassId(String adminClassId)
    {
        this.adminClassId = adminClassId;return this;
    }

    public VeBaseXJYD setAdminClassName(String adminClassName)
    {
        this.adminClassName = adminClassName;return this;
    }

    public VeBaseXJYD setChangeReasion(String changeReasion)
    {
        this.changeReasion = changeReasion;return this;
    }

    public VeBaseXJYD setChangeTypeValue(String changeTypeValue)
    {
        this.changeTypeValue = changeTypeValue;return this;
    }

    public VeBaseXJYD setChangeType(String changeType)
    {
        this.changeType = changeType;return this;
    }

    public VeBaseXJYD setStudentName(String studentName)
    {
        this.studentName = studentName;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $studentId = getStudentId();result = result * 59 + ($studentId == null ? 43 : $studentId.hashCode());Object $studentNumber = getStudentNumber();result = result * 59 + ($studentNumber == null ? 43 : $studentNumber.hashCode());Object $studentName = getStudentName();result = result * 59 + ($studentName == null ? 43 : $studentName.hashCode());Object $changeType = getChangeType();result = result * 59 + ($changeType == null ? 43 : $changeType.hashCode());Object $changeTypeValue = getChangeTypeValue();result = result * 59 + ($changeTypeValue == null ? 43 : $changeTypeValue.hashCode());Object $changeReasion = getChangeReasion();result = result * 59 + ($changeReasion == null ? 43 : $changeReasion.hashCode());Object $adminClassName = getAdminClassName();result = result * 59 + ($adminClassName == null ? 43 : $adminClassName.hashCode());Object $adminClassId = getAdminClassId();result = result * 59 + ($adminClassId == null ? 43 : $adminClassId.hashCode());Object $effectiveTime = getEffectiveTime();result = result * 59 + ($effectiveTime == null ? 43 : $effectiveTime.hashCode());Object $afterClassName = getAfterClassName();result = result * 59 + ($afterClassName == null ? 43 : $afterClassName.hashCode());Object $afterClassId = getAfterClassId();result = result * 59 + ($afterClassId == null ? 43 : $afterClassId.hashCode());Object $afterSpecialtyName = getAfterSpecialtyName();result = result * 59 + ($afterSpecialtyName == null ? 43 : $afterSpecialtyName.hashCode());Object $afterSpecialtyCode = getAfterSpecialtyCode();result = result * 59 + ($afterSpecialtyCode == null ? 43 : $afterSpecialtyCode.hashCode());Object $createBy = getCreateBy();result = result * 59 + ($createBy == null ? 43 : $createBy.hashCode());Object $createDate = getCreateDate();result = result * 59 + ($createDate == null ? 43 : $createDate.hashCode());Object $updateBy = getUpdateBy();result = result * 59 + ($updateBy == null ? 43 : $updateBy.hashCode());Object $updateDate = getUpdateDate();result = result * 59 + ($updateDate == null ? 43 : $updateDate.hashCode());Object $delFlag = getDelFlag();result = result * 59 + ($delFlag == null ? 43 : $delFlag.hashCode());Object $remarks = getRemarks();result = result * 59 + ($remarks == null ? 43 : $remarks.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseXJYD;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseXJYD)) {
            return false;
        }
        VeBaseXJYD other = (VeBaseXJYD)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$studentId = getStudentId();Object other$studentId = other.getStudentId();
        if (this$studentId == null ? other$studentId != null : !this$studentId.equals(other$studentId)) {
            return false;
        }
        Object this$studentNumber = getStudentNumber();Object other$studentNumber = other.getStudentNumber();
        if (this$studentNumber == null ? other$studentNumber != null : !this$studentNumber.equals(other$studentNumber)) {
            return false;
        }
        Object this$studentName = getStudentName();Object other$studentName = other.getStudentName();
        if (this$studentName == null ? other$studentName != null : !this$studentName.equals(other$studentName)) {
            return false;
        }
        Object this$changeType = getChangeType();Object other$changeType = other.getChangeType();
        if (this$changeType == null ? other$changeType != null : !this$changeType.equals(other$changeType)) {
            return false;
        }
        Object this$changeTypeValue = getChangeTypeValue();Object other$changeTypeValue = other.getChangeTypeValue();
        if (this$changeTypeValue == null ? other$changeTypeValue != null : !this$changeTypeValue.equals(other$changeTypeValue)) {
            return false;
        }
        Object this$changeReasion = getChangeReasion();Object other$changeReasion = other.getChangeReasion();
        if (this$changeReasion == null ? other$changeReasion != null : !this$changeReasion.equals(other$changeReasion)) {
            return false;
        }
        Object this$adminClassName = getAdminClassName();Object other$adminClassName = other.getAdminClassName();
        if (this$adminClassName == null ? other$adminClassName != null : !this$adminClassName.equals(other$adminClassName)) {
            return false;
        }
        Object this$adminClassId = getAdminClassId();Object other$adminClassId = other.getAdminClassId();
        if (this$adminClassId == null ? other$adminClassId != null : !this$adminClassId.equals(other$adminClassId)) {
            return false;
        }
        Object this$effectiveTime = getEffectiveTime();Object other$effectiveTime = other.getEffectiveTime();
        if (this$effectiveTime == null ? other$effectiveTime != null : !this$effectiveTime.equals(other$effectiveTime)) {
            return false;
        }
        Object this$afterClassName = getAfterClassName();Object other$afterClassName = other.getAfterClassName();
        if (this$afterClassName == null ? other$afterClassName != null : !this$afterClassName.equals(other$afterClassName)) {
            return false;
        }
        Object this$afterClassId = getAfterClassId();Object other$afterClassId = other.getAfterClassId();
        if (this$afterClassId == null ? other$afterClassId != null : !this$afterClassId.equals(other$afterClassId)) {
            return false;
        }
        Object this$afterSpecialtyName = getAfterSpecialtyName();Object other$afterSpecialtyName = other.getAfterSpecialtyName();
        if (this$afterSpecialtyName == null ? other$afterSpecialtyName != null : !this$afterSpecialtyName.equals(other$afterSpecialtyName)) {
            return false;
        }
        Object this$afterSpecialtyCode = getAfterSpecialtyCode();Object other$afterSpecialtyCode = other.getAfterSpecialtyCode();
        if (this$afterSpecialtyCode == null ? other$afterSpecialtyCode != null : !this$afterSpecialtyCode.equals(other$afterSpecialtyCode)) {
            return false;
        }
        Object this$createBy = getCreateBy();Object other$createBy = other.getCreateBy();
        if (this$createBy == null ? other$createBy != null : !this$createBy.equals(other$createBy)) {
            return false;
        }
        Object this$createDate = getCreateDate();Object other$createDate = other.getCreateDate();
        if (this$createDate == null ? other$createDate != null : !this$createDate.equals(other$createDate)) {
            return false;
        }
        Object this$updateBy = getUpdateBy();Object other$updateBy = other.getUpdateBy();
        if (this$updateBy == null ? other$updateBy != null : !this$updateBy.equals(other$updateBy)) {
            return false;
        }
        Object this$updateDate = getUpdateDate();Object other$updateDate = other.getUpdateDate();
        if (this$updateDate == null ? other$updateDate != null : !this$updateDate.equals(other$updateDate)) {
            return false;
        }
        Object this$delFlag = getDelFlag();Object other$delFlag = other.getDelFlag();
        if (this$delFlag == null ? other$delFlag != null : !this$delFlag.equals(other$delFlag)) {
            return false;
        }
        Object this$remarks = getRemarks();Object other$remarks = other.getRemarks();return this$remarks == null ? other$remarks == null : this$remarks.equals(other$remarks);
    }

    public String getId()
    {
        return this.id;
    }

    public String getStudentId()
    {
        return this.studentId;
    }

    public String getStudentNumber()
    {
        return this.studentNumber;
    }

    public String getStudentName()
    {
        return this.studentName;
    }

    public String getChangeType()
    {
        return this.changeType;
    }

    public String getChangeTypeValue()
    {
        return this.changeTypeValue;
    }

    public String getChangeReasion()
    {
        return this.changeReasion;
    }

    public String getAdminClassName()
    {
        return this.adminClassName;
    }

    public String getAdminClassId()
    {
        return this.adminClassId;
    }

    public String getEffectiveTime()
    {
        return this.effectiveTime;
    }

    public String getAfterClassName()
    {
        return this.afterClassName;
    }

    public String getAfterClassId()
    {
        return this.afterClassId;
    }

    public String getAfterSpecialtyName()
    {
        return this.afterSpecialtyName;
    }

    public String getAfterSpecialtyCode()
    {
        return this.afterSpecialtyCode;
    }

    public String getCreateBy()
    {
        return this.createBy;
    }

    public Date getCreateDate()
    {
        return this.createDate;
    }

    public String getUpdateBy()
    {
        return this.updateBy;
    }

    public Date getUpdateDate()
    {
        return this.updateDate;
    }

    public String getDelFlag()
    {
        return this.delFlag;
    }

    public String getRemarks()
    {
        return this.remarks;
    }
}
