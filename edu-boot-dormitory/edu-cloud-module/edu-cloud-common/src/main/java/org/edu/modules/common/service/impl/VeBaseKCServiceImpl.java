package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseKC;
import org.edu.modules.common.mapper.VeBaseKCMapper;
import org.edu.modules.common.service.IVeBaseKCService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseKCServiceImpl
        extends ServiceImpl<VeBaseKCMapper, VeBaseKC>
        implements IVeBaseKCService
{
    @Autowired
    private VeBaseKCMapper veBaseKCMapper;

    public List<Map<String, Object>> getKCPageList(VeBaseKC veBaseKC)
    {
        return this.veBaseKCMapper.getKCPageList(veBaseKC);
    }
}
