package org.edu.modules.common.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Map;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.modules.common.service.IVeBaseSpecialtyService;
import org.edu.modules.common.service.IVeBaseStudentService;
import org.edu.modules.common.service.IVeBaseTeacherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags={"首页数据"})
@RestController
@RequestMapping({"/common/veHomePage"})
@ApiSort(60)
public class VeHomePageController
{
    private static final Logger log = LoggerFactory.getLogger(VeHomePageController.class);
    @Autowired
    private IVeBaseTeacherService veBaseTeacherService;
    @Autowired
    private IVeBaseStudentService veBaseStudentService;
    @Autowired
    private IVeBaseSpecialtyService veBaseSpecialtyService;

    @AutoLog("教师性别统计")
    @ApiOperation(value="教师性别统计", notes="教师性别统计")
    @GetMapping({"/getTeacherSexStatistics"})
    public Result<?> getTeacherSexStatistics()
    {
        Map map = this.veBaseTeacherService.getTeacherSexStatistics();
        return Result.ok(map);
    }

    @AutoLog("教师年龄段统计")
    @ApiOperation(value="教师年龄段统计", notes="教师年龄段统计")
    @GetMapping({"/getTeacherAgeStatistics"})
    public Result<?> getTeacherAgeStatistics()
    {
        Map map = this.veBaseTeacherService.getTeacherAgeStatistics();
        return Result.ok(map);
    }

    @AutoLog("学生状态漏斗图")
    @ApiOperation(value="学生状态漏斗图", notes="学生状态漏斗图")
    @GetMapping({"/getStudentStatusStatistics"})
    public Result<?> getStudentStatusStatistics()
    {
        Map map = this.veBaseStudentService.getStudentStatusStatistics();
        return Result.ok(map);
    }

    @AutoLog("统计各专业的学生人数")
    @ApiOperation(value="统计各专业的学生人数", notes="统计各专业的学生人数")
    @GetMapping({"/getSpecialtyStudentStatistics"})
    public Result<?> getSpecialtyStudentStatistics()
    {
        List<Map<String, Object>> list = this.veBaseSpecialtyService.getSpecialtyStudentStatistics();
        return Result.ok(list);
    }
}
