package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseJYZ;

public abstract interface IVeBaseJYZService
        extends IService<VeBaseJYZ>
{
    public abstract List<Map<String, Object>> getJYZTreeList();

    public abstract VeBaseJYZ getJYZByName(Integer paramInteger, String paramString);

    public abstract VeBaseJYZ getJYZByCode(Integer paramInteger, String paramString);

    public abstract Map getJYZById(Integer paramInteger);
}

