package org.edu.modules.common.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import org.edu.modules.common.entity.VeBaseSysConfig;
import org.edu.modules.common.mapper.VeBaseSysConfigMapper;
import org.edu.modules.common.service.IVeBaseSysConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseSysConfigServiceImpl
        extends ServiceImpl<VeBaseSysConfigMapper, VeBaseSysConfig>
        implements IVeBaseSysConfigService
{
    @Autowired
    private VeBaseSysConfigMapper veBaseSysConfigMapper;

    public List<VeBaseSysConfig> querySysConfigPageList(VeBaseSysConfig veBaseSysConfig)
    {
        return this.veBaseSysConfigMapper.querySysConfigPageList(veBaseSysConfig);
    }
}
