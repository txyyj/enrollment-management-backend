package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_xuezi")
@ApiModel(value="ve_base_xuezi对象", description="学制信息表")
public class VeBaseXueZhi
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="学习年限(年)", width=15.0D)
    @ApiModelProperty("学习年限（以年为单位）")
    private Integer years;
    @Excel(name="学制名称", width=15.0D)
    @ApiModelProperty("学制名称")
    private String xzmc;
    @Excel(name="终端ID", width=15.0D)
    @ApiModelProperty("终端ID")
    private Integer terminalid;

    public VeBaseXueZhi setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeBaseXueZhi setYears(Integer years)
    {
        this.years = years;return this;
    }

    public String toString()
    {
        return "VeBaseXueZhi(id=" + getId() + ", years=" + getYears() + ", xzmc=" + getXzmc() + ", terminalid=" + getTerminalid() + ")";
    }

    public VeBaseXueZhi setId(Integer id)
    {
        this.id = id;return this;
    }

    public VeBaseXueZhi setXzmc(String xzmc)
    {
        this.xzmc = xzmc;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $years = getYears();result = result * 59 + ($years == null ? 43 : $years.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $xzmc = getXzmc();result = result * 59 + ($xzmc == null ? 43 : $xzmc.hashCode());return result;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseXueZhi)) {
            return false;
        }
        VeBaseXueZhi other = (VeBaseXueZhi)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$years = getYears();Object other$years = other.getYears();
        if (this$years == null ? other$years != null : !this$years.equals(other$years)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$xzmc = getXzmc();Object other$xzmc = other.getXzmc();return this$xzmc == null ? other$xzmc == null : this$xzmc.equals(other$xzmc);
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseXueZhi;
    }

    public Integer getId()
    {
        return this.id;
    }

    public Integer getYears()
    {
        return this.years;
    }

    public String getXzmc()
    {
        return this.xzmc;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }
}
