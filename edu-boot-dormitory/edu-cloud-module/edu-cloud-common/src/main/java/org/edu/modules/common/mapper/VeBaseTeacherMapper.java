package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseTeacher;

public abstract interface VeBaseTeacherMapper
        extends BaseMapper<VeBaseTeacher>
{
    public abstract List<VeBaseTeacher> getTeacherListByDepId(Integer paramInteger);

    public abstract List<Map<String, Object>> getTeacherPageList(VeBaseTeacher paramVeBaseTeacher);

    public abstract IPage<Map<String, Object>> getTeacherPageListByIPage(Page paramPage, VeBaseTeacher paramVeBaseTeacher);

    public abstract Map getTeacherSexStatistics();

    public abstract Map getTeacherAgeStatistics();

    public abstract VeBaseTeacher getByGH(Integer paramInteger, String paramString);

    public abstract VeBaseTeacher getByUserId(String paramString);

    public abstract List<VeBaseTeacher> getTeacherListBySearch(VeBaseTeacher paramVeBaseTeacher);

    public abstract int stopTeacherByUserId(String paramString);

    public abstract VeBaseTeacher queryBzrByBanjiId(Integer paramInteger);
}
