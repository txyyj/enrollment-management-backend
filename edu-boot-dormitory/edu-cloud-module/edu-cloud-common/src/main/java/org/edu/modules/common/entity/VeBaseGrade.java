package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_grade")
@ApiModel(value="ve_base_grade对象", description="年级信息表")
public class VeBaseGrade
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="年级代码", width=15.0D)
    @ApiModelProperty("年级代码")
    private String njdm;
    @Excel(name="年级名称", width=15.0D)
    @ApiModelProperty("年级名称")
    private String njmc;
    @Excel(name="入学年份", width=15.0D)
    @ApiModelProperty("入学年份")
    private Integer rxnf;
    @Excel(name="年级状态", width=15.0D)
    @ApiModelProperty("年级状态，0过期1未过期")
    private Integer njzt;
    @Excel(name="报名截止日期", width=15.0D)
    @ApiModelProperty("报名截止日期")
    private Integer bmjzrq;
    @Excel(name="备注", width=15.0D)
    @ApiModelProperty("备注")
    private String remark;
    @Excel(name="终端ID", width=15.0D)
    @ApiModelProperty("终端ID")
    private Integer terminalid;
    @Excel(name="年级组长ID", width=15.0D)
    @ApiModelProperty("年级组长ID")
    private String njzzuserid;
    @TableField(exist=false)
    @ApiModelProperty("报名截止日期")
    private String bmjzrqName;

    public VeBaseGrade setNjmc(String njmc)
    {
        this.njmc = njmc;return this;
    }

    public VeBaseGrade setNjdm(String njdm)
    {
        this.njdm = njdm;return this;
    }

    public VeBaseGrade setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseGrade(id=" + getId() + ", njdm=" + getNjdm() + ", njmc=" + getNjmc() + ", rxnf=" + getRxnf() + ", njzt=" + getNjzt() + ", bmjzrq=" + getBmjzrq() + ", remark=" + getRemark() + ", terminalid=" + getTerminalid() + ", njzzuserid=" + getNjzzuserid() + ", bmjzrqName=" + getBmjzrqName() + ")";
    }

    public VeBaseGrade setBmjzrqName(String bmjzrqName)
    {
        this.bmjzrqName = bmjzrqName;return this;
    }

    public VeBaseGrade setNjzzuserid(String njzzuserid)
    {
        this.njzzuserid = njzzuserid;return this;
    }

    public VeBaseGrade setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeBaseGrade setRemark(String remark)
    {
        this.remark = remark;return this;
    }

    public VeBaseGrade setBmjzrq(Integer bmjzrq)
    {
        this.bmjzrq = bmjzrq;return this;
    }

    public VeBaseGrade setNjzt(Integer njzt)
    {
        this.njzt = njzt;return this;
    }

    public VeBaseGrade setRxnf(Integer rxnf)
    {
        this.rxnf = rxnf;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $rxnf = getRxnf();result = result * 59 + ($rxnf == null ? 43 : $rxnf.hashCode());Object $njzt = getNjzt();result = result * 59 + ($njzt == null ? 43 : $njzt.hashCode());Object $bmjzrq = getBmjzrq();result = result * 59 + ($bmjzrq == null ? 43 : $bmjzrq.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $njdm = getNjdm();result = result * 59 + ($njdm == null ? 43 : $njdm.hashCode());Object $njmc = getNjmc();result = result * 59 + ($njmc == null ? 43 : $njmc.hashCode());Object $remark = getRemark();result = result * 59 + ($remark == null ? 43 : $remark.hashCode());Object $njzzuserid = getNjzzuserid();result = result * 59 + ($njzzuserid == null ? 43 : $njzzuserid.hashCode());Object $bmjzrqName = getBmjzrqName();result = result * 59 + ($bmjzrqName == null ? 43 : $bmjzrqName.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseGrade;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseGrade)) {
            return false;
        }
        VeBaseGrade other = (VeBaseGrade)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$rxnf = getRxnf();Object other$rxnf = other.getRxnf();
        if (this$rxnf == null ? other$rxnf != null : !this$rxnf.equals(other$rxnf)) {
            return false;
        }
        Object this$njzt = getNjzt();Object other$njzt = other.getNjzt();
        if (this$njzt == null ? other$njzt != null : !this$njzt.equals(other$njzt)) {
            return false;
        }
        Object this$bmjzrq = getBmjzrq();Object other$bmjzrq = other.getBmjzrq();
        if (this$bmjzrq == null ? other$bmjzrq != null : !this$bmjzrq.equals(other$bmjzrq)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$njdm = getNjdm();Object other$njdm = other.getNjdm();
        if (this$njdm == null ? other$njdm != null : !this$njdm.equals(other$njdm)) {
            return false;
        }
        Object this$njmc = getNjmc();Object other$njmc = other.getNjmc();
        if (this$njmc == null ? other$njmc != null : !this$njmc.equals(other$njmc)) {
            return false;
        }
        Object this$remark = getRemark();Object other$remark = other.getRemark();
        if (this$remark == null ? other$remark != null : !this$remark.equals(other$remark)) {
            return false;
        }
        Object this$njzzuserid = getNjzzuserid();Object other$njzzuserid = other.getNjzzuserid();
        if (this$njzzuserid == null ? other$njzzuserid != null : !this$njzzuserid.equals(other$njzzuserid)) {
            return false;
        }
        Object this$bmjzrqName = getBmjzrqName();Object other$bmjzrqName = other.getBmjzrqName();return this$bmjzrqName == null ? other$bmjzrqName == null : this$bmjzrqName.equals(other$bmjzrqName);
    }

    public Integer getId()
    {
        return this.id;
    }

    public String getNjdm()
    {
        return this.njdm;
    }

    public String getNjmc()
    {
        return this.njmc;
    }

    public Integer getRxnf()
    {
        return this.rxnf;
    }

    public Integer getNjzt()
    {
        return this.njzt;
    }

    public Integer getBmjzrq()
    {
        return this.bmjzrq;
    }

    public String getRemark()
    {
        return this.remark;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public String getNjzzuserid()
    {
        return this.njzzuserid;
    }

    public String getBmjzrqName()
    {
        return this.bmjzrqName;
    }
}
