package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseDictionary;

public abstract interface IVeBaseDictionaryService
        extends IService<VeBaseDictionary>
{
    public abstract List<VeBaseDictionary> getDictionaryListByCode(String paramString);

    public abstract List<Map<String, Object>> getDictDataListByCode(String paramString);

    public abstract List<VeBaseDictionary> getTreeList();
}
