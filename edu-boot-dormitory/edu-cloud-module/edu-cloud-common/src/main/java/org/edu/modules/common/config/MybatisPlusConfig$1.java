package org.edu.modules.common.config;

import com.baomidou.mybatisplus.autoconfigure.ConfigurationCustomizer;
import com.baomidou.mybatisplus.core.MybatisConfiguration;
import com.github.pagehelper.PageInterceptor;

public class MybatisPlusConfig$1 implements ConfigurationCustomizer
{
    MybatisPlusConfig$1(MybatisPlusConfig mybatisPlusConfig) {}

    @Override
    public void customize(MybatisConfiguration configuration) {
        configuration.setMapUnderscoreToCamelCase(false);
        configuration.addInterceptor(new PageInterceptor());
    }
}
