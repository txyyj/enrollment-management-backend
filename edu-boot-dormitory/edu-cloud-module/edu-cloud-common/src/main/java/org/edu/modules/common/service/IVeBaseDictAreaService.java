package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.common.entity.VeBaseDictArea;

public abstract interface IVeBaseDictAreaService
        extends IService<VeBaseDictArea>
{
    public abstract List<VeBaseDictArea> getProvinceList();

    public abstract List<VeBaseDictArea> getCityList(Integer paramInteger);

    public abstract List<VeBaseDictArea> getCountyList(Integer paramInteger);

    public abstract VeBaseDictArea getDictAreaByPidAndName(Integer paramInteger1, Integer paramInteger2, String paramString);
}
