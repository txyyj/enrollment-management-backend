package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.common.entity.VeBaseCalendar;

public abstract interface IVeBaseCalendarService
        extends IService<VeBaseCalendar>
{
    public abstract List<VeBaseCalendar> getCalendarListBySemId(Integer paramInteger1, Integer paramInteger2, Integer paramInteger3);

    public abstract VeBaseCalendar getCalenderByDates(String paramString);
}
