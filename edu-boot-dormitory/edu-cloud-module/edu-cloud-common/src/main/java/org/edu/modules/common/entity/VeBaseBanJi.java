package org.edu.modules.common.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_banji")
@ApiModel(value="ve_base_banji对象", description="班级信息表")
public class VeBaseBanJi
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="系统终端ID", width=15.0D)
    @ApiModelProperty("系统终端ID")
    private Integer terminalid;
    @Excel(name="行政班代码", width=15.0D)
    @ApiModelProperty("行政班代码")
    private String xzbdm;
    @Excel(name="行政班名称", width=15.0D)
    @ApiModelProperty("行政班名称")
    private String xzbmc;
    @Excel(name="所属年级", width=15.0D)
    @ApiModelProperty("年级代码")
    private String njdm;
    @Excel(name="年级名称", width=15.0D)
    @ApiModelProperty("年级名称")
    private String njmc;
    @Excel(name="年级ID", width=15.0D)
    @ApiModelProperty("年级ID")
    private Integer gradeId;
    @Excel(name="专业代码", width=15.0D)
    @ApiModelProperty("专业代码")
    private String zydm;
    @Excel(name="专业ID", width=15.0D)
    @ApiModelProperty("专业ID")
    private Integer specId;
    @Excel(name="建班年月", width=15.0D)
    @ApiModelProperty("建班年月")
    private String jbny;
    @Excel(name="入学年份", width=15.0D)
    @ApiModelProperty("入学年份,根据年级自动生成")
    private String rxnf;
    @Excel(name="班主任用户ID", width=15.0D)
    @ApiModelProperty("班主任用户ID")
    private String bzrUserId;
    @Excel(name="男生人数", width=15.0D)
    @ApiModelProperty("男生人数")
    private Integer nansrs;
    @Excel(name="女生人数", width=15.0D)
    @ApiModelProperty("女生人数")
    private Integer nvsrs;
    @Excel(name="班级上限人数", width=15.0D)
    @ApiModelProperty("总人数（人数上限）")
    private Integer zrs;
    @Excel(name="班长用户ID", width=15.0D)
    @ApiModelProperty("班长用户ID")
    private String bzUserId;
    @Excel(name="教学计划", width=15.0D)
    @ApiModelProperty("教学计划")
    private String jxjh;
    @Excel(name="机构部门ID", width=15.0D)
    @ApiModelProperty("机构部门ID")
    private Integer depId;
    @Excel(name="校区ID", width=15.0D)
    @ApiModelProperty("校区ID")
    private Integer campusId;
    @Excel(name="教学楼ID（教务系统的建筑物ID）", width=15.0D)
    @ApiModelProperty("教学楼ID（教务系统的建筑物ID）")
    private Integer jzid;
    @Excel(name="教室ID（教务系统的教室ID）", width=15.0D)
    @ApiModelProperty("教室ID（教务系统的教室ID）")
    private Integer jsid;
    @Excel(name="毕业时间int", width=15.0D)
    @ApiModelProperty("毕业时间")
    private Integer bysj;
    @Excel(name="排序", width=15.0D)
    @ApiModelProperty("排序")
    private Integer listsort;
    @Excel(name="状态", width=15.0D)
    @ApiModelProperty("状态;1可用2不可用")
    private Integer status;
    @Excel(name="毕业班级状态", width=15.0D)
    @ApiModelProperty("毕业班级状态，0=>未毕业，1=>毕业审核中， 2=>已毕业")
    private Integer bystatus;
    @Excel(name="所属专业", width=15.0D)
    @TableField(exist=false)
    private String specName;
    @Excel(name="校区名称", width=15.0D)
    @TableField(exist=false)
    private String campusName;
    @Excel(name="教学楼名称", width=15.0D)
    @TableField(exist=false)
    private String jxlName;
    @Excel(name="固定教室名称", width=15.0D)
    @TableField(exist=false)
    private String roomName;
    @TableField(exist=false)
    @ApiModelProperty("查询条件的接口用户id")
    private String interfaceUserId;
    @TableField(exist=false)
    @ApiModelProperty("查询条件的院系id")
    private String falId;
    @TableField(exist=false)
    @ApiModelProperty("查询条件的班主任名称")
    private String bzrUserName;
    @TableField(exist=false)
    @ApiModelProperty("查询条件的班长名称")
    private String bzUserName;
    @TableField(exist=false)
    @ApiModelProperty("查询条件的开始时间")
    private String beginDate;
    @TableField(exist=false)
    @ApiModelProperty("查询条件的结束时间")
    private String endDate;
    @TableField(exist=false)
    @ApiModelProperty("建班年月")
    private String jbnyName;
    @TableField(exist=false)
    @ApiModelProperty("毕业时间")
    private String bysjName;
    @TableField(exist=false)
    @ApiModelProperty("查询当前所有班主任")
    private String isCurrent;

    public VeBaseBanJi setXzbdm(String xzbdm)
    {
        this.xzbdm = xzbdm;return this;
    }

    public VeBaseBanJi setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeBaseBanJi setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseBanJi(id=" + getId() + ", terminalid=" + getTerminalid() + ", xzbdm=" + getXzbdm() + ", xzbmc=" + getXzbmc() + ", njdm=" + getNjdm() + ", njmc=" + getNjmc() + ", gradeId=" + getGradeId() + ", zydm=" + getZydm() + ", specId=" + getSpecId() + ", jbny=" + getJbny() + ", rxnf=" + getRxnf() + ", bzrUserId=" + getBzrUserId() + ", nansrs=" + getNansrs() + ", nvsrs=" + getNvsrs() + ", zrs=" + getZrs() + ", bzUserId=" + getBzUserId() + ", jxjh=" + getJxjh() + ", depId=" + getDepId() + ", campusId=" + getCampusId() + ", jzid=" + getJzid() + ", jsid=" + getJsid() + ", bysj=" + getBysj() + ", listsort=" + getListsort() + ", status=" + getStatus() + ", bystatus=" + getBystatus() + ", specName=" + getSpecName() + ", campusName=" + getCampusName() + ", jxlName=" + getJxlName() + ", roomName=" + getRoomName() + ", interfaceUserId=" + getInterfaceUserId() + ", falId=" + getFalId() + ", bzrUserName=" + getBzrUserName() + ", bzUserName=" + getBzUserName() + ", beginDate=" + getBeginDate() + ", endDate=" + getEndDate() + ", jbnyName=" + getJbnyName() + ", bysjName=" + getBysjName() + ", isCurrent=" + getIsCurrent() + ")";
    }

    public VeBaseBanJi setIsCurrent(String isCurrent)
    {
        this.isCurrent = isCurrent;return this;
    }

    public VeBaseBanJi setBysjName(String bysjName)
    {
        this.bysjName = bysjName;return this;
    }

    public VeBaseBanJi setJbnyName(String jbnyName)
    {
        this.jbnyName = jbnyName;return this;
    }

    public VeBaseBanJi setEndDate(String endDate)
    {
        this.endDate = endDate;return this;
    }

    public VeBaseBanJi setBeginDate(String beginDate)
    {
        this.beginDate = beginDate;return this;
    }

    public VeBaseBanJi setBzUserName(String bzUserName)
    {
        this.bzUserName = bzUserName;return this;
    }

    public VeBaseBanJi setBzrUserName(String bzrUserName)
    {
        this.bzrUserName = bzrUserName;return this;
    }

    public VeBaseBanJi setFalId(String falId)
    {
        this.falId = falId;return this;
    }

    public VeBaseBanJi setInterfaceUserId(String interfaceUserId)
    {
        this.interfaceUserId = interfaceUserId;return this;
    }

    public VeBaseBanJi setRoomName(String roomName)
    {
        this.roomName = roomName;return this;
    }

    public VeBaseBanJi setJxlName(String jxlName)
    {
        this.jxlName = jxlName;return this;
    }

    public VeBaseBanJi setCampusName(String campusName)
    {
        this.campusName = campusName;return this;
    }

    public VeBaseBanJi setSpecName(String specName)
    {
        this.specName = specName;return this;
    }

    public VeBaseBanJi setBystatus(Integer bystatus)
    {
        this.bystatus = bystatus;return this;
    }

    public VeBaseBanJi setStatus(Integer status)
    {
        this.status = status;return this;
    }

    public VeBaseBanJi setListsort(Integer listsort)
    {
        this.listsort = listsort;return this;
    }

    public VeBaseBanJi setBysj(Integer bysj)
    {
        this.bysj = bysj;return this;
    }

    public VeBaseBanJi setJsid(Integer jsid)
    {
        this.jsid = jsid;return this;
    }

    public VeBaseBanJi setJzid(Integer jzid)
    {
        this.jzid = jzid;return this;
    }

    public VeBaseBanJi setCampusId(Integer campusId)
    {
        this.campusId = campusId;return this;
    }

    public VeBaseBanJi setDepId(Integer depId)
    {
        this.depId = depId;return this;
    }

    public VeBaseBanJi setJxjh(String jxjh)
    {
        this.jxjh = jxjh;return this;
    }

    public VeBaseBanJi setBzUserId(String bzUserId)
    {
        this.bzUserId = bzUserId;return this;
    }

    public VeBaseBanJi setZrs(Integer zrs)
    {
        this.zrs = zrs;return this;
    }

    public VeBaseBanJi setNvsrs(Integer nvsrs)
    {
        this.nvsrs = nvsrs;return this;
    }

    public VeBaseBanJi setNansrs(Integer nansrs)
    {
        this.nansrs = nansrs;return this;
    }

    public VeBaseBanJi setBzrUserId(String bzrUserId)
    {
        this.bzrUserId = bzrUserId;return this;
    }

    public VeBaseBanJi setRxnf(String rxnf)
    {
        this.rxnf = rxnf;return this;
    }

    public VeBaseBanJi setJbny(String jbny)
    {
        this.jbny = jbny;return this;
    }

    public VeBaseBanJi setSpecId(Integer specId)
    {
        this.specId = specId;return this;
    }

    public VeBaseBanJi setZydm(String zydm)
    {
        this.zydm = zydm;return this;
    }

    public VeBaseBanJi setGradeId(Integer gradeId)
    {
        this.gradeId = gradeId;return this;
    }

    public VeBaseBanJi setNjmc(String njmc)
    {
        this.njmc = njmc;return this;
    }

    public VeBaseBanJi setNjdm(String njdm)
    {
        this.njdm = njdm;return this;
    }

    public VeBaseBanJi setXzbmc(String xzbmc)
    {
        this.xzbmc = xzbmc;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $gradeId = getGradeId();result = result * 59 + ($gradeId == null ? 43 : $gradeId.hashCode());Object $specId = getSpecId();result = result * 59 + ($specId == null ? 43 : $specId.hashCode());Object $nansrs = getNansrs();result = result * 59 + ($nansrs == null ? 43 : $nansrs.hashCode());Object $nvsrs = getNvsrs();result = result * 59 + ($nvsrs == null ? 43 : $nvsrs.hashCode());Object $zrs = getZrs();result = result * 59 + ($zrs == null ? 43 : $zrs.hashCode());Object $depId = getDepId();result = result * 59 + ($depId == null ? 43 : $depId.hashCode());Object $campusId = getCampusId();result = result * 59 + ($campusId == null ? 43 : $campusId.hashCode());Object $jzid = getJzid();result = result * 59 + ($jzid == null ? 43 : $jzid.hashCode());Object $jsid = getJsid();result = result * 59 + ($jsid == null ? 43 : $jsid.hashCode());Object $bysj = getBysj();result = result * 59 + ($bysj == null ? 43 : $bysj.hashCode());Object $listsort = getListsort();result = result * 59 + ($listsort == null ? 43 : $listsort.hashCode());Object $status = getStatus();result = result * 59 + ($status == null ? 43 : $status.hashCode());Object $bystatus = getBystatus();result = result * 59 + ($bystatus == null ? 43 : $bystatus.hashCode());Object $xzbdm = getXzbdm();result = result * 59 + ($xzbdm == null ? 43 : $xzbdm.hashCode());Object $xzbmc = getXzbmc();result = result * 59 + ($xzbmc == null ? 43 : $xzbmc.hashCode());Object $njdm = getNjdm();result = result * 59 + ($njdm == null ? 43 : $njdm.hashCode());Object $njmc = getNjmc();result = result * 59 + ($njmc == null ? 43 : $njmc.hashCode());Object $zydm = getZydm();result = result * 59 + ($zydm == null ? 43 : $zydm.hashCode());Object $jbny = getJbny();result = result * 59 + ($jbny == null ? 43 : $jbny.hashCode());Object $rxnf = getRxnf();result = result * 59 + ($rxnf == null ? 43 : $rxnf.hashCode());Object $bzrUserId = getBzrUserId();result = result * 59 + ($bzrUserId == null ? 43 : $bzrUserId.hashCode());Object $bzUserId = getBzUserId();result = result * 59 + ($bzUserId == null ? 43 : $bzUserId.hashCode());Object $jxjh = getJxjh();result = result * 59 + ($jxjh == null ? 43 : $jxjh.hashCode());Object $specName = getSpecName();result = result * 59 + ($specName == null ? 43 : $specName.hashCode());Object $campusName = getCampusName();result = result * 59 + ($campusName == null ? 43 : $campusName.hashCode());Object $jxlName = getJxlName();result = result * 59 + ($jxlName == null ? 43 : $jxlName.hashCode());Object $roomName = getRoomName();result = result * 59 + ($roomName == null ? 43 : $roomName.hashCode());Object $interfaceUserId = getInterfaceUserId();result = result * 59 + ($interfaceUserId == null ? 43 : $interfaceUserId.hashCode());Object $falId = getFalId();result = result * 59 + ($falId == null ? 43 : $falId.hashCode());Object $bzrUserName = getBzrUserName();result = result * 59 + ($bzrUserName == null ? 43 : $bzrUserName.hashCode());Object $bzUserName = getBzUserName();result = result * 59 + ($bzUserName == null ? 43 : $bzUserName.hashCode());Object $beginDate = getBeginDate();result = result * 59 + ($beginDate == null ? 43 : $beginDate.hashCode());Object $endDate = getEndDate();result = result * 59 + ($endDate == null ? 43 : $endDate.hashCode());Object $jbnyName = getJbnyName();result = result * 59 + ($jbnyName == null ? 43 : $jbnyName.hashCode());Object $bysjName = getBysjName();result = result * 59 + ($bysjName == null ? 43 : $bysjName.hashCode());Object $isCurrent = getIsCurrent();result = result * 59 + ($isCurrent == null ? 43 : $isCurrent.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseBanJi;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseBanJi)) {
            return false;
        }
        VeBaseBanJi other = (VeBaseBanJi)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$gradeId = getGradeId();Object other$gradeId = other.getGradeId();
        if (this$gradeId == null ? other$gradeId != null : !this$gradeId.equals(other$gradeId)) {
            return false;
        }
        Object this$specId = getSpecId();Object other$specId = other.getSpecId();
        if (this$specId == null ? other$specId != null : !this$specId.equals(other$specId)) {
            return false;
        }
        Object this$nansrs = getNansrs();Object other$nansrs = other.getNansrs();
        if (this$nansrs == null ? other$nansrs != null : !this$nansrs.equals(other$nansrs)) {
            return false;
        }
        Object this$nvsrs = getNvsrs();Object other$nvsrs = other.getNvsrs();
        if (this$nvsrs == null ? other$nvsrs != null : !this$nvsrs.equals(other$nvsrs)) {
            return false;
        }
        Object this$zrs = getZrs();Object other$zrs = other.getZrs();
        if (this$zrs == null ? other$zrs != null : !this$zrs.equals(other$zrs)) {
            return false;
        }
        Object this$depId = getDepId();Object other$depId = other.getDepId();
        if (this$depId == null ? other$depId != null : !this$depId.equals(other$depId)) {
            return false;
        }
        Object this$campusId = getCampusId();Object other$campusId = other.getCampusId();
        if (this$campusId == null ? other$campusId != null : !this$campusId.equals(other$campusId)) {
            return false;
        }
        Object this$jzid = getJzid();Object other$jzid = other.getJzid();
        if (this$jzid == null ? other$jzid != null : !this$jzid.equals(other$jzid)) {
            return false;
        }
        Object this$jsid = getJsid();Object other$jsid = other.getJsid();
        if (this$jsid == null ? other$jsid != null : !this$jsid.equals(other$jsid)) {
            return false;
        }
        Object this$bysj = getBysj();Object other$bysj = other.getBysj();
        if (this$bysj == null ? other$bysj != null : !this$bysj.equals(other$bysj)) {
            return false;
        }
        Object this$listsort = getListsort();Object other$listsort = other.getListsort();
        if (this$listsort == null ? other$listsort != null : !this$listsort.equals(other$listsort)) {
            return false;
        }
        Object this$status = getStatus();Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) {
            return false;
        }
        Object this$bystatus = getBystatus();Object other$bystatus = other.getBystatus();
        if (this$bystatus == null ? other$bystatus != null : !this$bystatus.equals(other$bystatus)) {
            return false;
        }
        Object this$xzbdm = getXzbdm();Object other$xzbdm = other.getXzbdm();
        if (this$xzbdm == null ? other$xzbdm != null : !this$xzbdm.equals(other$xzbdm)) {
            return false;
        }
        Object this$xzbmc = getXzbmc();Object other$xzbmc = other.getXzbmc();
        if (this$xzbmc == null ? other$xzbmc != null : !this$xzbmc.equals(other$xzbmc)) {
            return false;
        }
        Object this$njdm = getNjdm();Object other$njdm = other.getNjdm();
        if (this$njdm == null ? other$njdm != null : !this$njdm.equals(other$njdm)) {
            return false;
        }
        Object this$njmc = getNjmc();Object other$njmc = other.getNjmc();
        if (this$njmc == null ? other$njmc != null : !this$njmc.equals(other$njmc)) {
            return false;
        }
        Object this$zydm = getZydm();Object other$zydm = other.getZydm();
        if (this$zydm == null ? other$zydm != null : !this$zydm.equals(other$zydm)) {
            return false;
        }
        Object this$jbny = getJbny();Object other$jbny = other.getJbny();
        if (this$jbny == null ? other$jbny != null : !this$jbny.equals(other$jbny)) {
            return false;
        }
        Object this$rxnf = getRxnf();Object other$rxnf = other.getRxnf();
        if (this$rxnf == null ? other$rxnf != null : !this$rxnf.equals(other$rxnf)) {
            return false;
        }
        Object this$bzrUserId = getBzrUserId();Object other$bzrUserId = other.getBzrUserId();
        if (this$bzrUserId == null ? other$bzrUserId != null : !this$bzrUserId.equals(other$bzrUserId)) {
            return false;
        }
        Object this$bzUserId = getBzUserId();Object other$bzUserId = other.getBzUserId();
        if (this$bzUserId == null ? other$bzUserId != null : !this$bzUserId.equals(other$bzUserId)) {
            return false;
        }
        Object this$jxjh = getJxjh();Object other$jxjh = other.getJxjh();
        if (this$jxjh == null ? other$jxjh != null : !this$jxjh.equals(other$jxjh)) {
            return false;
        }
        Object this$specName = getSpecName();Object other$specName = other.getSpecName();
        if (this$specName == null ? other$specName != null : !this$specName.equals(other$specName)) {
            return false;
        }
        Object this$campusName = getCampusName();Object other$campusName = other.getCampusName();
        if (this$campusName == null ? other$campusName != null : !this$campusName.equals(other$campusName)) {
            return false;
        }
        Object this$jxlName = getJxlName();Object other$jxlName = other.getJxlName();
        if (this$jxlName == null ? other$jxlName != null : !this$jxlName.equals(other$jxlName)) {
            return false;
        }
        Object this$roomName = getRoomName();Object other$roomName = other.getRoomName();
        if (this$roomName == null ? other$roomName != null : !this$roomName.equals(other$roomName)) {
            return false;
        }
        Object this$interfaceUserId = getInterfaceUserId();Object other$interfaceUserId = other.getInterfaceUserId();
        if (this$interfaceUserId == null ? other$interfaceUserId != null : !this$interfaceUserId.equals(other$interfaceUserId)) {
            return false;
        }
        Object this$falId = getFalId();Object other$falId = other.getFalId();
        if (this$falId == null ? other$falId != null : !this$falId.equals(other$falId)) {
            return false;
        }
        Object this$bzrUserName = getBzrUserName();Object other$bzrUserName = other.getBzrUserName();
        if (this$bzrUserName == null ? other$bzrUserName != null : !this$bzrUserName.equals(other$bzrUserName)) {
            return false;
        }
        Object this$bzUserName = getBzUserName();Object other$bzUserName = other.getBzUserName();
        if (this$bzUserName == null ? other$bzUserName != null : !this$bzUserName.equals(other$bzUserName)) {
            return false;
        }
        Object this$beginDate = getBeginDate();Object other$beginDate = other.getBeginDate();
        if (this$beginDate == null ? other$beginDate != null : !this$beginDate.equals(other$beginDate)) {
            return false;
        }
        Object this$endDate = getEndDate();Object other$endDate = other.getEndDate();
        if (this$endDate == null ? other$endDate != null : !this$endDate.equals(other$endDate)) {
            return false;
        }
        Object this$jbnyName = getJbnyName();Object other$jbnyName = other.getJbnyName();
        if (this$jbnyName == null ? other$jbnyName != null : !this$jbnyName.equals(other$jbnyName)) {
            return false;
        }
        Object this$bysjName = getBysjName();Object other$bysjName = other.getBysjName();
        if (this$bysjName == null ? other$bysjName != null : !this$bysjName.equals(other$bysjName)) {
            return false;
        }
        Object this$isCurrent = getIsCurrent();Object other$isCurrent = other.getIsCurrent();return this$isCurrent == null ? other$isCurrent == null : this$isCurrent.equals(other$isCurrent);
    }

    public Integer getId()
    {
        return this.id;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public String getXzbdm()
    {
        return this.xzbdm;
    }

    public String getXzbmc()
    {
        return this.xzbmc;
    }

    public String getNjdm()
    {
        return this.njdm;
    }

    public String getNjmc()
    {
        return this.njmc;
    }

    public Integer getGradeId()
    {
        return this.gradeId;
    }

    public String getZydm()
    {
        return this.zydm;
    }

    public Integer getSpecId()
    {
        return this.specId;
    }

    public String getJbny()
    {
        return this.jbny;
    }

    public String getRxnf()
    {
        return this.rxnf;
    }

    public String getBzrUserId()
    {
        return this.bzrUserId;
    }

    public Integer getNansrs()
    {
        return this.nansrs;
    }

    public Integer getNvsrs()
    {
        return this.nvsrs;
    }

    public Integer getZrs()
    {
        return this.zrs;
    }

    public String getBzUserId()
    {
        return this.bzUserId;
    }

    public String getJxjh()
    {
        return this.jxjh;
    }

    public Integer getDepId()
    {
        return this.depId;
    }

    public Integer getCampusId()
    {
        return this.campusId;
    }

    public Integer getJzid()
    {
        return this.jzid;
    }

    public Integer getJsid()
    {
        return this.jsid;
    }

    public Integer getBysj()
    {
        return this.bysj;
    }

    public Integer getListsort()
    {
        return this.listsort;
    }

    public Integer getStatus()
    {
        return this.status;
    }

    public Integer getBystatus()
    {
        return this.bystatus;
    }

    public String getSpecName()
    {
        return this.specName;
    }

    public String getCampusName()
    {
        return this.campusName;
    }

    public String getJxlName()
    {
        return this.jxlName;
    }

    public String getRoomName()
    {
        return this.roomName;
    }

    public String getInterfaceUserId()
    {
        return this.interfaceUserId;
    }

    public String getFalId()
    {
        return this.falId;
    }

    public String getBzrUserName()
    {
        return this.bzrUserName;
    }

    public String getBzUserName()
    {
        return this.bzUserName;
    }

    public String getBeginDate()
    {
        return this.beginDate;
    }

    public String getEndDate()
    {
        return this.endDate;
    }

    public String getJbnyName()
    {
        return this.jbnyName;
    }

    public String getBysjName()
    {
        return this.bysjName;
    }

    public String getIsCurrent()
    {
        return this.isCurrent;
    }
}
