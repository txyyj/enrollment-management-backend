package org.edu.modules.common.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseCampus;

public abstract interface VeBaseCampusMapper
        extends BaseMapper<VeBaseCampus>
{
    public abstract List<Map<String, Object>> getCampusPageList(VeBaseCampus paramVeBaseCampus);

    public abstract VeBaseCampus getCampusByName(Integer paramInteger, String paramString);

    public abstract VeBaseCampus getCampusByCode(Integer paramInteger, String paramString);
}
