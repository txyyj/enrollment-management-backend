package org.edu.modules.common.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.common.entity.VeBaseDataApply;
import org.edu.modules.common.vo.VeBaseDataApplyVo;

public abstract interface IVeBaseDataApplyService
        extends IService<VeBaseDataApply>
{
    public abstract List<Map<String, Object>> queryDataApplyPageList(VeBaseDataApply paramVeBaseDataApply);

    public abstract List<Map<String, Object>> queryNotCheckDataApplyPageList(VeBaseDataApply paramVeBaseDataApply);

    public abstract List<Map<String, Object>> queryDataApplyProcessedPageList(VeBaseDataApply paramVeBaseDataApply);

    public abstract void addDataApply(VeBaseDataApplyVo paramVeBaseDataApplyVo);

    public abstract VeBaseDataApplyVo editEchoDataApply(int paramInt);

    public abstract void editDataApply(VeBaseDataApplyVo paramVeBaseDataApplyVo);

    public abstract List<Map<String, Object>> queryCommonListByApplyId(VeBaseDataApply paramVeBaseDataApply);

    public abstract List<Map<String, Object>> queryDataApplyUserId(String paramString);
}
