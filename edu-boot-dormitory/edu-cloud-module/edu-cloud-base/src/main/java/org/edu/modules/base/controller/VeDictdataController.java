package org.edu.modules.base.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.Arrays;
import java.util.List;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.base.entity.PageBean;
import org.edu.modules.base.entity.SysLog;
import org.edu.modules.base.entity.VeDictdata;
import org.edu.modules.base.entity.VeDictionary;
import org.edu.modules.base.feign.EduBootCommonFeignInterface;
import org.edu.modules.base.service.IVeDictdataService;
import org.edu.modules.base.service.IVeDictionaryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags={"数据字典数据 "})
@RestController
@RequestMapping({"/base/veDictdata"})
@ApiSort(60)
public class VeDictdataController
        extends BaseController<VeDictdata, IVeDictdataService>
{
    private static final Logger log = LoggerFactory.getLogger(VeDictdataController.class);
    @Autowired
    private IVeDictdataService veDictdataService;
    @Autowired
    private IVeDictionaryService veDictionaryService;
    @Autowired
    private EduBootCommonFeignInterface eduBootCommonFeignInterface;

    @AutoLog("查询菜单xxxxx")
    @ApiOperation(value="查询菜单xxxxx", notes="查询菜单xxxxx")
    @GetMapping({"/getAPD"})
    public Result<?> getAPD(SysLog sysLog)
    {
        Result result = this.eduBootCommonFeignInterface.querySysLogPageList(sysLog, Integer.valueOf(1), Integer.valueOf(10));
        return Result.OK(result.getResult());
    }

    @AutoLog("数据字典数据-查询菜单")
    @ApiOperation(value="查询菜单", notes="数据字典数据管理表-查询菜单")
    @GetMapping({"/selectTreeList"})
    public Result<?> selectTreeList()
    {
        List<VeDictionary> veDictionaryList = this.veDictionaryService.getTreeList();

        return Result.OK(veDictionaryList);
    }

    @AutoLog("数据字典数据-获得所有数据")
    @ApiOperation(value="获得所有数据", notes="数据字典数据管理表-获得所有数据")
    @GetMapping({"/selectAll"})
    public Result<?> selectCateCode(VeDictionary veDictionary, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        int total = this.veDictdataService.getSumPage(veDictionary);

        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), total);
        pb.setList(this.veDictdataService.selectAll(veDictionary, Integer.valueOf(pb.getStartIndex()), pageSize));
        return Result.ok(pb);
    }

    @AutoLog("数据字典数据-通过id查询")
    @ApiOperation(value="通过id查询", notes="数据字典数据管理表-通过id查询")
    @GetMapping({"/queryById"})
    public Result<?> queryById(@RequestParam(name="id", required=true) String id)
    {
        VeDictdata veDictdata = (VeDictdata)this.veDictdataService.getById(id);
        if (veDictdata == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(veDictdata);
    }

    @AutoLog("数据字典数据-新增信息")
    @ApiOperation(value="新增信息", notes="数据字典数据管理表-新增信息")
    @PostMapping({"/add"})
    public Result<?> add(@RequestBody VeDictdata veDictdata)
    {
        veDictdata.setTerminalId(1);
        this.veDictdataService.save(veDictdata);
        return Result.OK("添加成功！");
    }

    @AutoLog("数据字典数据-修改信息")
    @ApiOperation(value="修改信息", notes="数据字典数据管理表-修改信息")
    @PostMapping({"/edit"})
    public Result<?> edit(@RequestBody VeDictdata veDictdata)
    {
        if (("".equals(veDictdata.getModelCode())) || (veDictdata.getModelCode() == null)) {
            return Result.error("所属字典不能为空! ");
        }
        this.veDictdataService.updateById(veDictdata);
        return Result.OK("编辑成功!");
    }

    @AutoLog("数据字典数据-通过id删除")
    @ApiOperation(value="通过id删除", notes="数据字典数据管理表-通过id删除")
    @PostMapping({"/delete"})
    public Result<?> delete(@RequestParam(name="id", required=true) String id)
    {
        this.veDictdataService.removeById(id);
        return Result.OK("删除成功!");
    }

    @AutoLog("数据字典数据-批量删除")
    @ApiOperation(value="批量删除", notes="数据字典数据管理表-批量删除")
    @PostMapping({"/deleteBatch"})
    public Result<?> deleteBatch(@RequestParam(name="ids", required=true) String ids)
    {
        this.veDictdataService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.OK("批量删除成功!");
    }
}
