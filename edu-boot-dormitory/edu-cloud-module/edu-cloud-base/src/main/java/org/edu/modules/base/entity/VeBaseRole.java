package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Arrays;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_role")
@ApiModel(value="ve_base_role对象", description="角色表")
public class VeBaseRole
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="角色名称", width=15.0D)
    @ApiModelProperty("角色名称")
    private String rolename;
    @Excel(name="排序号", width=15.0D)
    @ApiModelProperty("排序号")
    private Double sort;
    @Excel(name="是否内置", width=15.0D)
    @ApiModelProperty("是否内置(y是，n否)")
    private String inlay;
    @TableField(exist=false)
    private String[] ids;

    public VeBaseRole setSort(Double sort)
    {
        this.sort = sort;return this;
    }

    public VeBaseRole setRolename(String rolename)
    {
        this.rolename = rolename;return this;
    }

    public VeBaseRole setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseRole(id=" + getId() + ", rolename=" + getRolename() + ", sort=" + getSort() + ", inlay=" + getInlay() + ", ids=" + Arrays.deepToString(getIds()) + ")";
    }

    public VeBaseRole setIds(String[] ids)
    {
        this.ids = ids;return this;
    }

    public VeBaseRole setInlay(String inlay)
    {
        this.inlay = inlay;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $sort = getSort();result = result * 59 + ($sort == null ? 43 : $sort.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $rolename = getRolename();result = result * 59 + ($rolename == null ? 43 : $rolename.hashCode());Object $inlay = getInlay();result = result * 59 + ($inlay == null ? 43 : $inlay.hashCode());result = result * 59 + Arrays.deepHashCode(getIds());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseRole;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseRole)) {
            return false;
        }
        VeBaseRole other = (VeBaseRole)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$sort = getSort();Object other$sort = other.getSort();
        if (this$sort == null ? other$sort != null : !this$sort.equals(other$sort)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$rolename = getRolename();Object other$rolename = other.getRolename();
        if (this$rolename == null ? other$rolename != null : !this$rolename.equals(other$rolename)) {
            return false;
        }
        Object this$inlay = getInlay();Object other$inlay = other.getInlay();
        if (this$inlay == null ? other$inlay != null : !this$inlay.equals(other$inlay)) {
            return false;
        }
        return Arrays.deepEquals(getIds(), other.getIds());
    }

    public String getId()
    {
        return this.id;
    }

    public String getRolename()
    {
        return this.rolename;
    }

    public Double getSort()
    {
        return this.sort;
    }

    public String getInlay()
    {
        return this.inlay;
    }

    public String[] getIds()
    {
        return this.ids;
    }
}
