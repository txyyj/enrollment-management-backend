package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseLoginSrategy;

public abstract interface VeBaseLoginSrategyMapper
        extends BaseMapper<VeBaseLoginSrategy>
{
    public abstract int getLoginSrategyAllList(VeBaseLoginSrategy paramVeBaseLoginSrategy);

    public abstract List<Map<String, Object>> getLoginSrategyPageList(VeBaseLoginSrategy paramVeBaseLoginSrategy);
}
