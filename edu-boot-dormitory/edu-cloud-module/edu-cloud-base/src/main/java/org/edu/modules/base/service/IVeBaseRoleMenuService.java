package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.base.entity.VeBaseRoleMenu;

public abstract interface IVeBaseRoleMenuService
        extends IService<VeBaseRoleMenu>
{}
