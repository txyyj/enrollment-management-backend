package org.edu.modules.base.intercept;

import javax.annotation.Resource;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class RequestConfig
        implements WebMvcConfigurer
{
    @Resource
    private RequestUrlIntercept requestUrlIntercept;

    public void addInterceptors(InterceptorRegistry registry)
    {
        registry.addInterceptor(this.requestUrlIntercept).addPathPatterns(new String[] { "/**" });
    }
}
