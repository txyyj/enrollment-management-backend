package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.common.system.base.service.impl.BaseServiceImpl;
import org.edu.modules.base.entity.VeBaseAppUserOrgan;
import org.edu.modules.base.mapper.VeBaseAppUserOrganMapper;
import org.edu.modules.base.service.IVeBaseAppUserOrganService;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/12/13 16:21
 */
@Service
public class VeBaseAppUserOrganServiceImpl extends ServiceImpl<VeBaseAppUserOrganMapper, VeBaseAppUserOrgan>
        implements IVeBaseAppUserOrganService {
    @Override
    public List<VeBaseAppUserOrgan> getTreeList() {
        return baseMapper.getBodyList();
    }
}
