package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseServiceGroup;

public abstract interface VeBaseServiceGroupMapper
        extends BaseMapper<VeBaseServiceGroup>
{
    public abstract int serviceGroupAllList(String paramString);

    public abstract List<Map<String, Object>> serviceGroupPageList(String paramString, Integer paramInteger1, Integer paramInteger2);

    public abstract int deleteGroupAppByGroupId(String paramString);

    public abstract int addGroupApp(String paramString, String[] paramArrayOfString);

    public abstract int deleteServiceGroupAppManage(String paramString, String[] paramArrayOfString);

    public abstract int deleteByAppId(String paramString);

    public abstract VeBaseServiceGroup getServiceGroupByGroupName(String paramString1, String paramString2);
}
