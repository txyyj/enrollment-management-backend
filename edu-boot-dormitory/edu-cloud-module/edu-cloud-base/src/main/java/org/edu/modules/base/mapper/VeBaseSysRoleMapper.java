package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.base.entity.VeBaseSysRole;

public abstract interface VeBaseSysRoleMapper
        extends BaseMapper<VeBaseSysRole>
{
    public abstract VeBaseSysRole getModelByRoleText(String paramString);

    public abstract VeBaseSysRole getModelByIdAndText(Integer paramInteger, String paramString);

    public abstract void deleteSysRoleUserById(String paramString);

    public abstract void addSysRoleUserById(String paramString, String[] paramArrayOfString);

    public abstract int addSysRole(VeBaseSysRole paramVeBaseSysRole);
}
