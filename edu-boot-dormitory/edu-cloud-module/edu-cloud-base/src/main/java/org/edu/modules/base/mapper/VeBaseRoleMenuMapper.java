package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.edu.modules.base.entity.VeBaseRoleMenu;

public abstract interface VeBaseRoleMenuMapper
        extends BaseMapper<VeBaseRoleMenu>
{
    public abstract String[] getArrays(String paramString);

    public abstract void baseRoleMenuDelete(String paramString);
}
