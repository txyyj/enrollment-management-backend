package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Date;
import org.edu.common.aspect.annotation.Dict;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.springframework.format.annotation.DateTimeFormat;

@TableName("sys_log")
@ApiModel(value="sys_log对象", description="系统日志信息")
public class SysLog
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="日志类型", width=15.0D, dicCode="log_type")
    @ApiModelProperty("日志类型（1登录日志，2操作日志）")
    @Dict(dicCode="log_type")
    private Integer logType;
    @Excel(name="日志内容", width=15.0D)
    @ApiModelProperty("日志内容")
    private String logContent;
    @Excel(name="操作类型", width=15.0D)
    @ApiModelProperty("操作类型:insert=添加，update=更新,delete=删除,status=状态")
    private Integer operateType;
    @Excel(name="操作用户账号", width=15.0D)
    @ApiModelProperty("操作用户账号")
    private String userid;
    @Excel(name="操作用户名称", width=15.0D)
    @ApiModelProperty("操作用户名称")
    private String username;
    @Excel(name="ip", width=15.0D)
    @ApiModelProperty("ip")
    private String ip;
    @Excel(name="请求java方法", width=15.0D)
    @ApiModelProperty("请求java方法")
    private String method;
    @Excel(name="请求路径", width=15.0D)
    @ApiModelProperty("请求路径")
    private String requestUrl;
    @Excel(name="请求参数", width=15.0D)
    @ApiModelProperty("请求参数")
    private String requestParam;
    @Excel(name="请求类型", width=15.0D)
    @ApiModelProperty("请求类型")
    private String requestType;
    @Excel(name="耗时", width=15.0D)
    @ApiModelProperty("耗时")
    private Integer costTime;
    @Excel(name="创建人", width=15.0D)
    @ApiModelProperty("创建人")
    private String createBy;
    @Excel(name="创建时间", width=20.0D, format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("创建时间")
    private Date createTime;
    @Excel(name="更新人", width=15.0D)
    @ApiModelProperty("更新人")
    private String updateBy;
    @Excel(name="更新时间", width=20.0D, format="yyyy-MM-dd HH:mm:ss")
    @JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty("更新时间")
    private Date updateTime;
    @ApiModelProperty("num=1为统一认证, 2为公共数据, 3为实习....")
    @TableField(exist=false)
    private Integer num;
    @ApiModelProperty("搜素条件开始日期")
    @TableField(exist=false)
    private String beginDate;
    @ApiModelProperty("搜索条件结束日期")
    @TableField(exist=false)
    private String endDate;
    @TableField(exist=false)
    private Integer startIndex;
    @TableField(exist=false)
    private Integer pageSize;
    @TableField(exist=false)
    private String operateTypeName;

    public SysLog setLogContent(String logContent)
    {
        this.logContent = logContent;return this;
    }

    public SysLog setLogType(Integer logType)
    {
        this.logType = logType;return this;
    }

    public SysLog setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "SysLog(id=" + getId() + ", logType=" + getLogType() + ", logContent=" + getLogContent() + ", operateType=" + getOperateType() + ", userid=" + getUserid() + ", username=" + getUsername() + ", ip=" + getIp() + ", method=" + getMethod() + ", requestUrl=" + getRequestUrl() + ", requestParam=" + getRequestParam() + ", requestType=" + getRequestType() + ", costTime=" + getCostTime() + ", createBy=" + getCreateBy() + ", createTime=" + getCreateTime() + ", updateBy=" + getUpdateBy() + ", updateTime=" + getUpdateTime() + ", num=" + getNum() + ", beginDate=" + getBeginDate() + ", endDate=" + getEndDate() + ", startIndex=" + getStartIndex() + ", pageSize=" + getPageSize() + ", operateTypeName=" + getOperateTypeName() + ")";
    }

    public SysLog setOperateTypeName(String operateTypeName)
    {
        this.operateTypeName = operateTypeName;return this;
    }

    public SysLog setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;return this;
    }

    public SysLog setStartIndex(Integer startIndex)
    {
        this.startIndex = startIndex;return this;
    }

    public SysLog setEndDate(String endDate)
    {
        this.endDate = endDate;return this;
    }

    public SysLog setBeginDate(String beginDate)
    {
        this.beginDate = beginDate;return this;
    }

    public SysLog setNum(Integer num)
    {
        this.num = num;return this;
    }

    @JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    public SysLog setUpdateTime(Date updateTime)
    {
        this.updateTime = updateTime;return this;
    }

    public SysLog setUpdateBy(String updateBy)
    {
        this.updateBy = updateBy;return this;
    }

    @JsonFormat(timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    public SysLog setCreateTime(Date createTime)
    {
        this.createTime = createTime;return this;
    }

    public SysLog setCreateBy(String createBy)
    {
        this.createBy = createBy;return this;
    }

    public SysLog setCostTime(Integer costTime)
    {
        this.costTime = costTime;return this;
    }

    public SysLog setRequestType(String requestType)
    {
        this.requestType = requestType;return this;
    }

    public SysLog setRequestParam(String requestParam)
    {
        this.requestParam = requestParam;return this;
    }

    public SysLog setRequestUrl(String requestUrl)
    {
        this.requestUrl = requestUrl;return this;
    }

    public SysLog setMethod(String method)
    {
        this.method = method;return this;
    }

    public SysLog setIp(String ip)
    {
        this.ip = ip;return this;
    }

    public SysLog setUsername(String username)
    {
        this.username = username;return this;
    }

    public SysLog setUserid(String userid)
    {
        this.userid = userid;return this;
    }

    public SysLog setOperateType(Integer operateType)
    {
        this.operateType = operateType;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $logType = getLogType();result = result * 59 + ($logType == null ? 43 : $logType.hashCode());Object $operateType = getOperateType();result = result * 59 + ($operateType == null ? 43 : $operateType.hashCode());Object $costTime = getCostTime();result = result * 59 + ($costTime == null ? 43 : $costTime.hashCode());Object $num = getNum();result = result * 59 + ($num == null ? 43 : $num.hashCode());Object $startIndex = getStartIndex();result = result * 59 + ($startIndex == null ? 43 : $startIndex.hashCode());Object $pageSize = getPageSize();result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $logContent = getLogContent();result = result * 59 + ($logContent == null ? 43 : $logContent.hashCode());Object $userid = getUserid();result = result * 59 + ($userid == null ? 43 : $userid.hashCode());Object $username = getUsername();result = result * 59 + ($username == null ? 43 : $username.hashCode());Object $ip = getIp();result = result * 59 + ($ip == null ? 43 : $ip.hashCode());Object $method = getMethod();result = result * 59 + ($method == null ? 43 : $method.hashCode());Object $requestUrl = getRequestUrl();result = result * 59 + ($requestUrl == null ? 43 : $requestUrl.hashCode());Object $requestParam = getRequestParam();result = result * 59 + ($requestParam == null ? 43 : $requestParam.hashCode());Object $requestType = getRequestType();result = result * 59 + ($requestType == null ? 43 : $requestType.hashCode());Object $createBy = getCreateBy();result = result * 59 + ($createBy == null ? 43 : $createBy.hashCode());Object $createTime = getCreateTime();result = result * 59 + ($createTime == null ? 43 : $createTime.hashCode());Object $updateBy = getUpdateBy();result = result * 59 + ($updateBy == null ? 43 : $updateBy.hashCode());Object $updateTime = getUpdateTime();result = result * 59 + ($updateTime == null ? 43 : $updateTime.hashCode());Object $beginDate = getBeginDate();result = result * 59 + ($beginDate == null ? 43 : $beginDate.hashCode());Object $endDate = getEndDate();result = result * 59 + ($endDate == null ? 43 : $endDate.hashCode());Object $operateTypeName = getOperateTypeName();result = result * 59 + ($operateTypeName == null ? 43 : $operateTypeName.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof SysLog;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof SysLog)) {
            return false;
        }
        SysLog other = (SysLog)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$logType = getLogType();Object other$logType = other.getLogType();
        if (this$logType == null ? other$logType != null : !this$logType.equals(other$logType)) {
            return false;
        }
        Object this$operateType = getOperateType();Object other$operateType = other.getOperateType();
        if (this$operateType == null ? other$operateType != null : !this$operateType.equals(other$operateType)) {
            return false;
        }
        Object this$costTime = getCostTime();Object other$costTime = other.getCostTime();
        if (this$costTime == null ? other$costTime != null : !this$costTime.equals(other$costTime)) {
            return false;
        }
        Object this$num = getNum();Object other$num = other.getNum();
        if (this$num == null ? other$num != null : !this$num.equals(other$num)) {
            return false;
        }
        Object this$startIndex = getStartIndex();Object other$startIndex = other.getStartIndex();
        if (this$startIndex == null ? other$startIndex != null : !this$startIndex.equals(other$startIndex)) {
            return false;
        }
        Object this$pageSize = getPageSize();Object other$pageSize = other.getPageSize();
        if (this$pageSize == null ? other$pageSize != null : !this$pageSize.equals(other$pageSize)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$logContent = getLogContent();Object other$logContent = other.getLogContent();
        if (this$logContent == null ? other$logContent != null : !this$logContent.equals(other$logContent)) {
            return false;
        }
        Object this$userid = getUserid();Object other$userid = other.getUserid();
        if (this$userid == null ? other$userid != null : !this$userid.equals(other$userid)) {
            return false;
        }
        Object this$username = getUsername();Object other$username = other.getUsername();
        if (this$username == null ? other$username != null : !this$username.equals(other$username)) {
            return false;
        }
        Object this$ip = getIp();Object other$ip = other.getIp();
        if (this$ip == null ? other$ip != null : !this$ip.equals(other$ip)) {
            return false;
        }
        Object this$method = getMethod();Object other$method = other.getMethod();
        if (this$method == null ? other$method != null : !this$method.equals(other$method)) {
            return false;
        }
        Object this$requestUrl = getRequestUrl();Object other$requestUrl = other.getRequestUrl();
        if (this$requestUrl == null ? other$requestUrl != null : !this$requestUrl.equals(other$requestUrl)) {
            return false;
        }
        Object this$requestParam = getRequestParam();Object other$requestParam = other.getRequestParam();
        if (this$requestParam == null ? other$requestParam != null : !this$requestParam.equals(other$requestParam)) {
            return false;
        }
        Object this$requestType = getRequestType();Object other$requestType = other.getRequestType();
        if (this$requestType == null ? other$requestType != null : !this$requestType.equals(other$requestType)) {
            return false;
        }
        Object this$createBy = getCreateBy();Object other$createBy = other.getCreateBy();
        if (this$createBy == null ? other$createBy != null : !this$createBy.equals(other$createBy)) {
            return false;
        }
        Object this$createTime = getCreateTime();Object other$createTime = other.getCreateTime();
        if (this$createTime == null ? other$createTime != null : !this$createTime.equals(other$createTime)) {
            return false;
        }
        Object this$updateBy = getUpdateBy();Object other$updateBy = other.getUpdateBy();
        if (this$updateBy == null ? other$updateBy != null : !this$updateBy.equals(other$updateBy)) {
            return false;
        }
        Object this$updateTime = getUpdateTime();Object other$updateTime = other.getUpdateTime();
        if (this$updateTime == null ? other$updateTime != null : !this$updateTime.equals(other$updateTime)) {
            return false;
        }
        Object this$beginDate = getBeginDate();Object other$beginDate = other.getBeginDate();
        if (this$beginDate == null ? other$beginDate != null : !this$beginDate.equals(other$beginDate)) {
            return false;
        }
        Object this$endDate = getEndDate();Object other$endDate = other.getEndDate();
        if (this$endDate == null ? other$endDate != null : !this$endDate.equals(other$endDate)) {
            return false;
        }
        Object this$operateTypeName = getOperateTypeName();Object other$operateTypeName = other.getOperateTypeName();return this$operateTypeName == null ? other$operateTypeName == null : this$operateTypeName.equals(other$operateTypeName);
    }

    public String getId()
    {
        return this.id;
    }

    public Integer getLogType()
    {
        return this.logType;
    }

    public String getLogContent()
    {
        return this.logContent;
    }

    public Integer getOperateType()
    {
        return this.operateType;
    }

    public String getUserid()
    {
        return this.userid;
    }

    public String getUsername()
    {
        return this.username;
    }

    public String getIp()
    {
        return this.ip;
    }

    public String getMethod()
    {
        return this.method;
    }

    public String getRequestUrl()
    {
        return this.requestUrl;
    }

    public String getRequestParam()
    {
        return this.requestParam;
    }

    public String getRequestType()
    {
        return this.requestType;
    }

    public Integer getCostTime()
    {
        return this.costTime;
    }

    public String getCreateBy()
    {
        return this.createBy;
    }

    public Date getCreateTime()
    {
        return this.createTime;
    }

    public String getUpdateBy()
    {
        return this.updateBy;
    }

    public Date getUpdateTime()
    {
        return this.updateTime;
    }

    public Integer getNum()
    {
        return this.num;
    }

    public String getBeginDate()
    {
        return this.beginDate;
    }

    public String getEndDate()
    {
        return this.endDate;
    }

    public Integer getStartIndex()
    {
        return this.startIndex;
    }

    public Integer getPageSize()
    {
        return this.pageSize;
    }

    public String getOperateTypeName()
    {
        return this.operateTypeName;
    }
}
