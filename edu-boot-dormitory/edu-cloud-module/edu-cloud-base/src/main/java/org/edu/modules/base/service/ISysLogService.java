package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.base.entity.SysLog;

public abstract interface ISysLogService
        extends IService<SysLog>
{
    public abstract int getSysLogAllList(SysLog paramSysLog);

    public abstract List<SysLog> getSysLogPageList(SysLog paramSysLog);
}
