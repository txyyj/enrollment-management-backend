package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseServiceType;

public abstract interface IVeBaseServiceTypeService
        extends IService<VeBaseServiceType>
{
    public abstract int serviceTypeAllList(String paramString);

    public abstract List<Map<String, Object>> serviceTypePageList(String paramString, Integer paramInteger1, Integer paramInteger2);

    public abstract VeBaseServiceType getServiceTypeByName(String paramString1, String paramString2);
}
