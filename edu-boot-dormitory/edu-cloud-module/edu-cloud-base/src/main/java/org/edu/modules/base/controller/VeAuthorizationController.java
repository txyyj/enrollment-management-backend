package org.edu.modules.base.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.modules.base.entity.PageBean;
import org.edu.modules.base.entity.VeBaseAppManage;
import org.edu.modules.base.entity.VeBaseAppUser;
import org.edu.modules.base.entity.VeBaseSysMenu;
import org.edu.modules.base.entity.VeBaseSysRole;
import org.edu.modules.base.entity.VeBaseSysRoleMenu;
import org.edu.modules.base.service.IVeBaseAppManageService;
import org.edu.modules.base.service.IVeBaseAppUserService;
import org.edu.modules.base.service.IVeBaseSysMenuService;
import org.edu.modules.base.service.IVeBaseSysRoleMenuService;
import org.edu.modules.base.service.IVeBaseSysRoleService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags={"权限管理"})
@RestController
@RequestMapping({"/base/veAuthorization"})
@ApiSort(60)
public class VeAuthorizationController
{
    private static final Logger log = LoggerFactory.getLogger(VeAuthorizationController.class);
    @Autowired
    private IVeBaseAppManageService veBaseAppManageService;
    @Autowired
    private IVeBaseSysMenuService veBaseSysMenuService;
    @Autowired
    private IVeBaseSysRoleService veBaseSysRoleService;
    @Autowired
    private IVeBaseSysRoleMenuService veBaseSysRoleMenuService;
    @Autowired
    private IVeBaseAppUserService veBaseAppUserService;

    @AutoLog("获取所有应用系统")
    @ApiOperation(value="获取所有应用系统", notes="获取所有应用系统")
    @GetMapping({"/getAppManageList"})
    public Result<?> getAppManageList()
    {
        List<VeBaseAppManage> list = this.veBaseAppManageService.getAppManageAllList(null, "0");
        return Result.ok(list);
    }

    @AutoLog("获取所有权限菜单")
    @ApiOperation(value="获取所有权限菜单", notes="获取所有权限菜单")
    @GetMapping({"/getSysMenuTreeList"})
    public Result<?> getSysMenuTreeList(@RequestParam(name="id", defaultValue="") String id)
    {
        List<Map<String, Object>> list = this.veBaseSysMenuService.getSysMenuTreeList(id);
        return Result.ok(list);
    }

    @AutoLog("角色管理-分页列表查询")
    @ApiOperation(value="角色管理-分页列表查询", notes="角色管理-分页列表查询")
    @GetMapping({"/getSysRolePageList"})
    public Result<?> getSysRolePageList(VeBaseSysRole veBaseSysRole, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        int num = this.veBaseSysMenuService.getSysRoleAllList(veBaseSysRole);

        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);

        veBaseSysRole.setStartIndex(Integer.valueOf(pb.getStartIndex()));
        veBaseSysRole.setPageSize(pageSize);
        pb.setList(this.veBaseSysMenuService.getSysRolePageList(veBaseSysRole));
        return Result.ok(pb);
    }

    @AutoLog("权限授权-分页列表查询")
    @ApiOperation(value="权限授权-分页列表查询", notes="权限授权-分页列表查询")
    @GetMapping({"/getAppUserPageList"})
    public Result<?> getAppUserPageList(VeBaseAppUser veBaseAppUser, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        int num = this.veBaseAppUserService.getAppUserAndStudentAndTeacherList(veBaseAppUser);

        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);

        veBaseAppUser.setStartIndex(Integer.valueOf(pb.getStartIndex()));
        veBaseAppUser.setPageSize(pageSize);
        pb.setList(this.veBaseAppUserService.getAppUserAndStudentAndTeacherPageList(veBaseAppUser));
        return Result.ok(pb);
    }

    @AutoLog("通过角色id和系统id查询菜单")
    @ApiOperation(value="通过角色id和系统id查询菜单", notes="通过角色id和系统id查询菜单")
    @GetMapping({"/getSysRoleMenuListByRoleIdAppId"})
    public Result<?> getSysRoleMenuListByRoleIdAppId(@RequestParam(name="roleId", required=true) Integer roleId, @RequestParam(name="appId", required=true) String appId)
    {
        List<Map<String, Object>> list = this.veBaseSysRoleMenuService.getSysRoleMenuListByRoleIdAppId(roleId, appId);
        return Result.OK(list);
    }

    @AutoLog("菜单-添加")
    @ApiOperation(value="添加菜单", notes="添加菜单")
    @PostMapping({"/addSysMenu"})
    public Result<?> addSysMenu(@RequestBody VeBaseSysMenu veBaseSysMenu)
    {
        veBaseSysMenu.setPath(null);
        Integer id = this.veBaseSysMenuService.addSysMenu(veBaseSysMenu);
        veBaseSysMenu.setId(veBaseSysMenu.getId());
        if (veBaseSysMenu.getPid().intValue() == 0)
        {
            veBaseSysMenu.setPid(Integer.valueOf(0 - veBaseSysMenu.getId().intValue()));
            veBaseSysMenu.setPath(0 - veBaseSysMenu.getId().intValue() + "." + veBaseSysMenu.getId() + ".");
            this.veBaseSysMenuService.updateById(veBaseSysMenu);
        }
        else
        {
            VeBaseSysMenu model = this.veBaseSysMenuService.getSysMenuByPid(veBaseSysMenu.getPid());
            if (model != null) {
                if (model.getPid().intValue() < 0)
                {
                    veBaseSysMenu.setPath(model.getPid() + "." + model.getId() + "." + veBaseSysMenu.getId() + ".");
                    this.veBaseSysMenuService.updateById(veBaseSysMenu);
                }
                else
                {
                    VeBaseSysMenu model1 = this.veBaseSysMenuService.getSysMenuByPid(model.getPid());
                    if ((model1 != null) &&
                            (model1.getPid().intValue() < 0))
                    {
                        veBaseSysMenu.setPath(model.getPid() + "." + model.getId() + "." + model1.getId() + "." + veBaseSysMenu.getId() + ".");
                        this.veBaseSysMenuService.updateById(veBaseSysMenu);
                    }
                }
            }
        }
        return Result.ok("添加成功!");
    }

    @AutoLog("角色管理添加")
    @ApiOperation(value="角色管理添加", notes="角色管理添加")
    @PostMapping({"/addSysRoleAndMenu"})
    public Result<?> addSysRoleAndMenu(@RequestBody VeBaseSysRole veBaseSysRole)
    {
        if (veBaseSysRole == null) {
            return Result.error("角色不能为空!");
        }
        VeBaseSysRole model = this.veBaseSysRoleService.getModelByRoleText(veBaseSysRole.getText());
        if (model != null) {
            return Result.error("角色号已存在!");
        }
        this.veBaseSysRoleService.addSysRole(veBaseSysRole);
        VeBaseSysRole model1 = this.veBaseSysRoleService.getModelByRoleText(veBaseSysRole.getText());
        if (veBaseSysRole.getIds().length > 0)
        {
            List<VeBaseSysRoleMenu> detailList = new ArrayList();
            for (String menuId : veBaseSysRole.getIds())
            {
                VeBaseSysRoleMenu veBaseSysRoleMenu = new VeBaseSysRoleMenu();
                veBaseSysRoleMenu.setRoleId(model1.getId());
                veBaseSysRoleMenu.setMenuId(Integer.valueOf(Integer.parseInt(menuId)));
                veBaseSysRoleMenu.setAppId(veBaseSysRole.getAppId());
                detailList.add(veBaseSysRoleMenu);
            }
            this.veBaseSysRoleMenuService.saveBatch(detailList);
        }
        return Result.ok("添加成功!");
    }

    @AutoLog("角色管理修改")
    @ApiOperation(value="角色管理修改", notes="角色管理修改")
    @PostMapping({"/editSysRoleAndMenu"})
    public Result<?> editSysRoleAndMenu(@RequestBody VeBaseSysRole veBaseSysRole)
    {
        if (veBaseSysRole == null) {
            return Result.error("角色不能为空!");
        }
        VeBaseSysRole model = this.veBaseSysRoleService.getModelByIdAndText(veBaseSysRole.getId(), veBaseSysRole.getText());
        if (model != null) {
            return Result.error("角色号已存在!");
        }
        this.veBaseSysRoleService.updateById(veBaseSysRole);

        this.veBaseSysRoleMenuService.deleteModelByAppId(veBaseSysRole.getId(), veBaseSysRole.getAppId());
        if (veBaseSysRole.getIds().length > 0)
        {
            List<VeBaseSysRoleMenu> detailList = new ArrayList();
            for (String menuId : veBaseSysRole.getIds())
            {
                VeBaseSysRoleMenu veBaseSysRoleMenu = new VeBaseSysRoleMenu();
                veBaseSysRoleMenu.setRoleId(veBaseSysRole.getId());
                veBaseSysRoleMenu.setMenuId(Integer.valueOf(Integer.parseInt(menuId)));
                veBaseSysRoleMenu.setAppId(veBaseSysRole.getAppId());
                detailList.add(veBaseSysRoleMenu);
            }
            this.veBaseSysRoleMenuService.saveBatch(detailList);
        }
        return Result.ok("修改成功!");
    }

    @AutoLog("权限授权-授权")
    @ApiOperation(value="权限授权-授权", notes="权限授权-授权")
    @PostMapping({"/editSysRoleUser"})
    public Result<?> editSysRoleUser(@RequestBody VeBaseAppUser veBaseAppUser)
    {
        if (veBaseAppUser == null) {
            return Result.error("用户不能为空!");
        }
        if ((veBaseAppUser.getUserId() == null) || ("".equals(veBaseAppUser.getUserId()))) {
            return Result.error("工号不能为空!");
        }
        if ((veBaseAppUser.getRoleId() == null) || ("".equals(veBaseAppUser.getRoleId()))) {
            return Result.error("请选择角色后再行确定!");
        }
        this.veBaseSysRoleService.deleteSysRoleUserById(veBaseAppUser.getUserId());
        String[] idArrray = veBaseAppUser.getRoleId().split(",");
        this.veBaseSysRoleService.addSysRoleUserById(veBaseAppUser.getUserId(), idArrray);
        return Result.ok("授权成功!");
    }

    @AutoLog("修改菜单")
    @ApiOperation(value="修改菜单", notes="修改菜单")
    @PostMapping({"/editSysMenu"})
    public Result<?> editSysMenu(@RequestBody VeBaseSysMenu veBaseSysMenu)
    {
        this.veBaseSysMenuService.updateById(veBaseSysMenu);
        return Result.ok("修改成功!");
    }

    @AutoLog("菜单删除-单个删除")
    @ApiOperation(value="菜单删除-单个删除", notes="菜单删除-单个删除")
    @PostMapping({"/deleteSysMenu"})
    @Transactional
    public Result<?> deleteSysMenu(@RequestParam(name="id", required=true) String id)
    {
        this.veBaseSysMenuService.removeById(id);

        this.veBaseSysRoleMenuService.deleteRoleMenuByMenuId(id);
        return Result.ok("删除成功!");
    }

    @AutoLog("角色管理-单个删除")
    @ApiOperation(value="角色管理-单个删除", notes="角色管理-单个删除")
    @PostMapping({"/deleteSysRole"})
    @Transactional
    public Result<?> deleteSysRole(@RequestParam(name="id", required=true) String id)
    {
        if (id.equals("6")) {
            return Result.error("管理员不能删除! ");
        }
        if ((id.equals("37")) || (id.equals("38"))) {
            return Result.error("企业管理员或者企业教师不能删除! ");
        }
        this.veBaseSysRoleService.removeById(id);


        this.veBaseSysRoleMenuService.deleteRoleUserByRoleId(id);

        this.veBaseSysRoleMenuService.deleteRoleMenuByRoleId(id);
        return Result.ok("删除成功!");
    }

    @AutoLog("菜单-批量删除")
    @ApiOperation(value="菜单-批量删除", notes="菜单-批量删除")
    @PostMapping({"/menuDeleteBatch"})
    public Result<?> menuDeleteBatch(@RequestParam(name="ids", required=true) String ids)
    {
        List<String> list = Arrays.asList(ids.split(","));
        this.veBaseSysMenuService.removeByIds(list);
        if (list.size() > 0) {
            for (String id : list) {
                this.veBaseSysRoleMenuService.deleteRoleMenuByMenuId(id);
            }
        }
        return Result.ok("批量删除成功!");
    }
}
