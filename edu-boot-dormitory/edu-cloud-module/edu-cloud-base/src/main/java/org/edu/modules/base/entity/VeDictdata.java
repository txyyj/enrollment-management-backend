package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_dictdata")
@ApiModel(value="ve_base_dictdata对象", description="数据字典数据管理表")
public class VeDictdata
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private int id;
    @Excel(name="名称", width=15.0D)
    @ApiModelProperty("名称")
    private String title;
    @Excel(name="代码", width=15.0D)
    @ApiModelProperty("代码")
    private String code;
    @Excel(name="字典模型代码", width=15.0D)
    @ApiModelProperty("字典模型代码")
    private String modelCode;
    @Excel(name="排序", width=15.0D)
    @ApiModelProperty("排序")
    private int listSort;
    @Excel(name="终端Id", width=15.0D)
    @ApiModelProperty("终端Id")
    private int terminalId;

    public VeDictdata setCode(String code)
    {
        this.code = code;return this;
    }

    public VeDictdata setTitle(String title)
    {
        this.title = title;return this;
    }

    public VeDictdata setId(int id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeDictdata(id=" + getId() + ", title=" + getTitle() + ", code=" + getCode() + ", modelCode=" + getModelCode() + ", listSort=" + getListSort() + ", terminalId=" + getTerminalId() + ")";
    }

    public VeDictdata setTerminalId(int terminalId)
    {
        this.terminalId = terminalId;return this;
    }

    public VeDictdata setListSort(int listSort)
    {
        this.listSort = listSort;return this;
    }

    public VeDictdata setModelCode(String modelCode)
    {
        this.modelCode = modelCode;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;result = result * 59 + getId();result = result * 59 + getListSort();result = result * 59 + getTerminalId();Object $title = getTitle();result = result * 59 + ($title == null ? 43 : $title.hashCode());Object $code = getCode();result = result * 59 + ($code == null ? 43 : $code.hashCode());Object $modelCode = getModelCode();result = result * 59 + ($modelCode == null ? 43 : $modelCode.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeDictdata;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeDictdata)) {
            return false;
        }
        VeDictdata other = (VeDictdata)o;
        if (!other.canEqual(this)) {
            return false;
        }
        if (getId() != other.getId()) {
            return false;
        }
        if (getListSort() != other.getListSort()) {
            return false;
        }
        if (getTerminalId() != other.getTerminalId()) {
            return false;
        }
        Object this$title = getTitle();Object other$title = other.getTitle();
        if (this$title == null ? other$title != null : !this$title.equals(other$title)) {
            return false;
        }
        Object this$code = getCode();Object other$code = other.getCode();
        if (this$code == null ? other$code != null : !this$code.equals(other$code)) {
            return false;
        }
        Object this$modelCode = getModelCode();Object other$modelCode = other.getModelCode();return this$modelCode == null ? other$modelCode == null : this$modelCode.equals(other$modelCode);
    }

    public int getId()
    {
        return this.id;
    }

    public String getTitle()
    {
        return this.title;
    }

    public String getCode()
    {
        return this.code;
    }

    public String getModelCode()
    {
        return this.modelCode;
    }

    public int getListSort()
    {
        return this.listSort;
    }

    public int getTerminalId()
    {
        return this.terminalId;
    }
}
