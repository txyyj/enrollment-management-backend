package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.base.entity.VeBaseServiceKind;
import org.edu.modules.base.mapper.VeBaseServiceKindMapper;
import org.edu.modules.base.service.IVeBaseServiceKindService;
import org.springframework.stereotype.Service;

@Service
public class VeBaseServiceKindServiceImpl
        extends ServiceImpl<VeBaseServiceKindMapper, VeBaseServiceKind>
        implements IVeBaseServiceKindService
{}
