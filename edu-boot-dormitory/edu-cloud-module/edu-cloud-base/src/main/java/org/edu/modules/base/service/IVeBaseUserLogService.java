package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseUserLog;

public abstract interface IVeBaseUserLogService
        extends IService<VeBaseUserLog>
{
    public abstract int userLogAllList(VeBaseUserLog paramVeBaseUserLog);

    public abstract List<Map<String, Object>> userLogPageList(VeBaseUserLog paramVeBaseUserLog);
}
