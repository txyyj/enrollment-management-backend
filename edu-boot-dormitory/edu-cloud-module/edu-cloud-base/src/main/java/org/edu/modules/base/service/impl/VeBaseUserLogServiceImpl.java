package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseUserLog;
import org.edu.modules.base.mapper.VeBaseUserLogMapper;
import org.edu.modules.base.service.IVeBaseUserLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseUserLogServiceImpl
        extends ServiceImpl<VeBaseUserLogMapper, VeBaseUserLog>
        implements IVeBaseUserLogService
{
    @Autowired
    private VeBaseUserLogMapper userLogMapper;

    public int userLogAllList(VeBaseUserLog veBaseUserLog)
    {
        return this.userLogMapper.userLogAllList(veBaseUserLog);
    }

    public List<Map<String, Object>> userLogPageList(VeBaseUserLog veBaseUserLog)
    {
        return this.userLogMapper.userLogPageList(veBaseUserLog);
    }
}
