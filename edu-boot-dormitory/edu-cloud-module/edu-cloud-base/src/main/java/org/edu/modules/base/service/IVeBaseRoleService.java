package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.base.entity.VeBaseRole;

public abstract interface IVeBaseRoleService
        extends IService<VeBaseRole>
{
    public abstract List<VeBaseRole> getAllList(VeBaseRole paramVeBaseRole);

    public abstract List<VeBaseRole> getPageList(String paramString, Integer paramInteger1, Integer paramInteger2);

    public abstract List<VeBaseRole> getListByRoleId(String paramString);

    public abstract VeBaseRole getModelByRoleId(String paramString);

    public abstract void baseRoleMenuDelete(String paramString);
}
