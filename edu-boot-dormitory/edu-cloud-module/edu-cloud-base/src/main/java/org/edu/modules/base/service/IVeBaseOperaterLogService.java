package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.base.entity.VeBaseOperaterLog;

public abstract interface IVeBaseOperaterLogService
        extends IService<VeBaseOperaterLog>
{
    public abstract List<VeBaseOperaterLog> getOperaterLogAllList(VeBaseOperaterLog paramVeBaseOperaterLog);

    public abstract List<VeBaseOperaterLog> getOperaterLogPageList(VeBaseOperaterLog paramVeBaseOperaterLog);
}
