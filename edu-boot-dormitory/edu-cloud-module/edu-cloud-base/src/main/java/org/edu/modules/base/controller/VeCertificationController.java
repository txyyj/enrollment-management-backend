package org.edu.modules.base.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.transaction.Transactional;

import org.aspectj.weaver.ast.Var;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.modules.base.entity.PageBean;
import org.edu.modules.base.entity.VeBaseAppGroup;
import org.edu.modules.base.entity.VeBaseAppManage;
import org.edu.modules.base.entity.VeBaseAppUser;
import org.edu.modules.base.entity.VeBaseAppUserOrgan;
import org.edu.modules.base.feign.EduBootCommonFeignInterface;
import org.edu.modules.base.service.IVeBaseAppGroupService;
import org.edu.modules.base.service.IVeBaseAppManageService;
import org.edu.modules.base.service.IVeBaseAppUserOrganService;
import org.edu.modules.base.service.IVeBaseAppUserService;
import org.edu.modules.base.service.IVeBaseSysMenuService;
import org.edu.modules.base.service.IVeBaseSysRoleMenuService;
import org.edu.modules.base.util.PhoneUtil;
import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

@Api(tags={"认证管理"})
@RestController
@RequestMapping({"/base/veCertification"})
@ApiSort(60)
public class VeCertificationController
{
    private static final Logger log = LoggerFactory.getLogger(VeCertificationController.class);
    @Autowired
    private IVeBaseAppGroupService appGroupService;
    @Autowired
    private IVeBaseAppManageService appManageService;
    @Autowired
    private IVeBaseAppUserService appUserService;
    @Autowired
    private IVeBaseAppUserOrganService appUserOrganService;
    @Autowired
    private IVeBaseSysMenuService veBaseSysMenuService;
    @Autowired
    private IVeBaseSysRoleMenuService veBaseSysRoleMenuService;
    @Autowired
    private EduBootCommonFeignInterface eduBootCommonFeignInterface;

    @AutoLog("用户组信息-分页列表查询")
    @ApiOperation(value="用户组信息-分页列表查询", notes="用户组信息-分页列表查询")
    @GetMapping({"/appGroupPageList"})
    public Result<?> appGroupPageList(VeBaseAppGroup appGroup, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        int num = this.appGroupService.getAppGroupAllList(appGroup.getRoleName());

        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);

        pb.setList(this.appGroupService.getAppGroupPageList(appGroup.getRoleName(), Integer.valueOf(pb.getStartIndex()), pageSize));
        return Result.ok(pb);
    }

    @AutoLog("应用信息-分页列表查询")
    @ApiOperation(value="应用信息-分页列表查询", notes="应用信息-分页列表查询")
    @GetMapping({"/appManagePageList"})
    public Result<?> appManagePageList(VeBaseAppManage appManage, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        int num = this.appManageService.getAppManageAllListNum(appManage.getAppName(), appManage.getIsEnable());

        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);

        pb.setList(this.appManageService.getAppManagePageList(appManage.getAppName(), appManage.getIsEnable(), Integer.valueOf(pb.getStartIndex()), pageSize));
        return Result.ok(pb);
    }

    @AutoLog("用户信息-分页列表查询")
    @ApiOperation(value="用户信息-分页列表查询", notes="用户信息-分页列表查询")
    @GetMapping({"/appUserPageList"})
    public Result<?> appUserPageList(VeBaseAppUser appUser, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        int num = this.appUserService.getAppUserAllList(appUser);

        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);

        appUser.setStartIndex(Integer.valueOf(pb.getStartIndex()));
        appUser.setPageSize(pageSize);
        pb.setList(this.appUserService.getAppUserPageList(appUser));
        return Result.ok(pb);
    }

    @AutoLog("应用用户机构信息列表")
    @ApiOperation(value="应用用户机构信息列表", notes="应用用户机构信息列表")
    @GetMapping({"/appUserOrganList"})
    public Result<?> appUserOrganList()
    {
        List<VeBaseAppUserOrgan> list = this.appUserOrganService.getTreeList();
        return Result.ok(list);
    }

    @AutoLog("通过id查询用户组信息")
    @ApiOperation(value="通过id查询用户组信息", notes="通过id查询用户组信息")
    @GetMapping({"/appGroupById"})
    public Result<?> appGroupById(@RequestParam(name="id", required=true) String id)
    {
        VeBaseAppGroup appGroup = (VeBaseAppGroup)this.appGroupService.getById(id);
        if (appGroup == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(appGroup);
    }

    @AutoLog("通过id查询应用信息")
    @ApiOperation(value="通过id查询应用信息", notes="通过id查询应用信息")
    @GetMapping({"/appManageById"})
    public Result<?> appManageById(@RequestParam(name="id", required=true) String id)
    {
        VeBaseAppManage appManage = (VeBaseAppManage)this.appManageService.getById(id);
        if (appManage == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(appManage);
    }

    @AutoLog("获取所有应用信息")
    @ApiOperation(value="获取所有应用信息", notes="获取所有应用信息")
    @GetMapping({"/getAppManageList"})
    public Result<?> getAppManageList()
    {
        List<VeBaseAppManage> appManage = this.appManageService.list();
        return Result.OK(appManage);
    }

    @AutoLog("通过id查询用户信息")
    @ApiOperation(value="通过id查询用户信息", notes="通过id查询用户信息")
    @GetMapping({"/appUserById"})
    public Result<?> appUserById(@RequestParam(name="id", required=true) String id)
    {
        VeBaseAppUser appUser = (VeBaseAppUser)this.appUserService.getById(id);
        if (appUser == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(appUser);
    }

    @AutoLog("获取所有部门信息")
    @ApiOperation(value="获取所有部门信息", notes="获取所有部门信息")
    @GetMapping({"/getAppOrganList"})
    public Result<?> getAppOrganList()
    {
        List<Map<String, Object>> list = this.appUserService.getAppOrganList();
        return Result.OK(list);
    }

    @AutoLog("获取所有角色信息")
    @ApiOperation(value="获取所有角色信息", notes="获取所有角色信息")
    @GetMapping({"/getAppRoleList"})
    public Result<?> getAppRoleList()
    {
        List<Map<String, Object>> list = this.appUserService.getSysRoleList();
        return Result.OK(list);
    }

    @AutoLog("用户组信息-添加")
    @ApiOperation(value="用户组信息-添加", notes="用户组信息-添加")
    @PostMapping({"/appGroupAdd"})
    public Result<?> appGroupAdd(@RequestBody VeBaseAppGroup appGroup)
    {
        if (("".equals(appGroup.getRoleName())) || (appGroup.getRoleName() == null)) {
            return Result.error("用户组名称不能为空!");
        }
        VeBaseAppGroup model = this.appGroupService.getAppGroupByName(null, appGroup.getRoleName());
        if (model != null) {
            return Result.error("用户组名称已存在!");
        }
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        appGroup.setId(uuid);
        appGroup.setInlay("n");
        this.appGroupService.save(appGroup);
        if (appGroup.getStrings().length > 0) {
            this.appGroupService.addAppGroupManageBatch(appGroup.getId(), appGroup.getStrings());
        }
        return Result.ok("添加成功!");
    }

    @AutoLog("应用信息-添加")
    @ApiOperation(value="应用信息-添加", notes="应用信息-添加")
    @PostMapping({"/appManageAdd"})
    public Result<?> appManageAdd(@RequestBody VeBaseAppManage appManage)
    {
        VeBaseAppManage model = this.appManageService.getAppManageByName(null, appManage.getAppName());
        if (model != null) {
            return Result.error("应用名称已存在!");
        }
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        appManage.setId(uuid);
        this.appManageService.save(appManage);
        return Result.ok("添加成功!");
    }

    @AutoLog("用户信息-添加")
    @ApiOperation(value="用户信息-添加", notes="用户信息-添加")
    @PostMapping({"/appUserAdd"})
    @Transactional
    public Result<?> appUserAdd(@RequestBody VeBaseAppUser appUser)
    {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        appUser.setId(uuid);
        appUser.setIsDel("0");

        appUser.setUserName(appUser.getUserId());
        if ((!"".equals(appUser.getUserType())) && (appUser.getUserType() != null)) {
            appUser.setUserType(appUser.getUserType().substring(0, appUser.getUserType().length() - 1));
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:ss:mm");
        Date date = new Date();
        appUser.setCreateTime(simpleDateFormat.format(date));
        appUser.setUpdateTime(simpleDateFormat.format(date));
        if (("".equals(appUser.getUserId())) || (appUser.getUserId() == null)) {
            return Result.error("用户工号不能为空!");
        }
        List<Map<String, Object>> userIdList = this.appUserService.getAppUserByUserId(appUser.getUserId(), null);
        if (userIdList.size() > 0) {
            return Result.error("用户工号已存在!");
        }
        List<Map<String, Object>> userIdAllList = this.appUserService.getAppUserAllListByUserId(appUser.getUserId(), null);
        if (userIdAllList.size() > 0) {
            return Result.error("工号已被占用!");
        }
        if (("".equals(appUser.getUserTel())) || (appUser.getUserTel() == null)) {
            return Result.error("用户手机号不能为空!");
        }
        if (!PhoneUtil.isMobile(appUser.getUserTel())) {
            return Result.error("用户手机号错误!");
        }
        List<Map<String, Object>> phoneList = this.appUserService.getAppUserByUserTel(appUser.getUserTel(), null);
        if (phoneList.size() > 0) {
            return Result.error("用户手机号已存在!");
        }
        if (appUser.getUserType().contains("1"))
        {
            List<Map<String, Object>> list = this.appUserService.getTeacherListByUserId(appUser.getUserId());
            if (list.size() == 0) {
                return Result.error("该工号教师不存在!");
            }
        }
        if (appUser.getUserType().contains("2"))
        {
            List<Map<String, Object>> list = this.appUserService.getStudentListByUserId(appUser.getUserId());
            if (list.size() == 0) {
                return Result.error("该工号学生不存在!");
            }
        }
        this.appUserService.addOrgan(appUser.getUserId(), appUser.getDeptId());

        String[] roleIds = appUser.getRoleId().split(",");
        for (String roleId : roleIds) {
            this.appUserService.addRole(appUser.getUserId(), roleId);
        }
        VeBaseAppUser veBaseAppUser = new VeBaseAppUser();
        veBaseAppUser = this.appUserService.getPwdAndSalt(appUser);

        veBaseAppUser.setStrength(this.appUserService.pwdVerify(appUser.getUserPassword()));
        this.appUserService.save(veBaseAppUser);

        this.appUserService.addSysUser(veBaseAppUser);
        return Result.ok("添加成功!");
    }

    @AutoLog("用户机构-添加")
    @ApiOperation(value="用户机构-添加", notes="用户机构表-添加")
    @PostMapping({"/appUserOrganAdd"})
    public Result<?> appUserOrganAdd(@RequestBody VeBaseAppUserOrgan appUserOrgan)
    {
        appUserOrgan.setId(UUID.randomUUID().toString().replace("-", ""));
        this.appUserOrganService.save(appUserOrgan);

        Result result = this.eduBootCommonFeignInterface.updateOrganUserRedis();
        return Result.ok("添加成功!");
    }

    @AutoLog("用户组信息-修改")
    @ApiOperation(value="用户组信息-修改", notes="用户组信息-修改")
    @PostMapping({"/appGroupEdit"})
    public Result<?> appGroupEdit(@RequestBody VeBaseAppGroup appGroup)
    {
        appGroup.setInlay("n");
        this.appGroupService.updateById(appGroup);
        this.appGroupService.deleteAppGroupManageById(appGroup.getId());
        if (appGroup.getStrings().length > 0) {
            this.appGroupService.addAppGroupManageBatch(appGroup.getId(), appGroup.getStrings());
        }
        return Result.ok("修改成功!");
    }

    @AutoLog("应用信息-修改")
    @ApiOperation(value="应用信息-修改", notes="应用信息-修改")
    @PostMapping({"/appManageEdit"})
    public Result<?> appManageEdit(@RequestBody VeBaseAppManage appManage)
    {
        this.appManageService.updateById(appManage);
        return Result.ok("修改成功!");
    }

    @AutoLog("用户管理-修改密码")
    @ApiOperation(value="用户管理-修改密码", notes="用户管理-修改密码")
    @PostMapping({"/appUserPasswordEdit"})
    public Result<?> appUserPasswordEdit(@RequestBody VeBaseAppUser veBaseAppUser)
    {
        VeBaseAppUser appUser = (VeBaseAppUser)this.appUserService.getById(veBaseAppUser.getId());
        if (appUser == null) {
            return Result.error("该用户不存在!");
        }
        if (("".equals(veBaseAppUser.getPwd())) || (veBaseAppUser.getPwd() == null) || ("".equals(veBaseAppUser.getAffirmPwd())) || (veBaseAppUser.getAffirmPwd() == null)) {
            return Result.error("新密码或确认密码不能为空!");
        }
        if (!veBaseAppUser.getPwd().equals(veBaseAppUser.getAffirmPwd())) {
            return Result.error("新密码与确认密码不一致!");
        }
        veBaseAppUser.setStrength(this.appUserService.pwdVerify(veBaseAppUser.getPwd()));
        return this.appUserService.updateAppUserPasswordById(appUser.getId(), appUser.getUserName(), veBaseAppUser.getPwd(), veBaseAppUser.getStrength());
    }

    @AutoLog("应用信息-禁用功能")
    @ApiOperation(value="应用信息-禁用功能", notes="应用信息-禁用功能")
    @PostMapping({"/appManageStop"})
    public Result<?> appManageStop(@RequestParam(name="id", required=true) String id)
    {
        VeBaseAppManage appManage = (VeBaseAppManage)this.appManageService.getById(id);
        if (appManage != null)
        {
            appManage.setIsEnable("1");
            this.appManageService.updateById(appManage);
        }
        return Result.ok("禁用成功!");
    }

    @AutoLog("应用信息-启用功能")
    @ApiOperation(value="应用信息-启用功能", notes="应用信息-启用功能")
    @PostMapping({"/appManageStart"})
    public Result<?> appManageStart(@RequestParam(name="id", required=true) String id)
    {
        VeBaseAppManage appManage = (VeBaseAppManage)this.appManageService.getById(id);
        if (appManage != null)
        {
            appManage.setIsEnable("0");
            this.appManageService.updateById(appManage);
        }
        return Result.ok("启用成功!");
    }

    @AutoLog("用户信息-修改")
    @ApiOperation(value="用户信息-修改", notes="用户信息-修改")
    @PostMapping({"/appUserEdit"})
    @Transactional
    public Result<?> appUserEdit(@RequestBody VeBaseAppUser appUser)
    {
        appUser.setIsDel("0");
        if (("".equals(appUser.getUserId())) || (appUser.getUserId() == null)) {
            return Result.error("用户工号不能为空!");
        }
        List<Map<String, Object>> userIdList = this.appUserService.getAppUserByUserId(appUser.getUserId(), appUser.getId());
        if (userIdList.size() > 0) {
            return Result.error("用户工号已存在!");
        }
        List<Map<String, Object>> userIdAllList = this.appUserService.getAppUserAllListByUserId(appUser.getUserId(), appUser.getId());
        if (userIdAllList.size() > 0) {
            return Result.error("工号已被占用!");
        }
        if (("".equals(appUser.getUserTel())) || (appUser.getUserTel() == null)) {
            return Result.error("用户手机号不能为空!");
        }
        List<Map<String, Object>> phoneList = this.appUserService.getAppUserByUserTel(appUser.getUserTel(), appUser.getId());
        if (phoneList.size() > 0) {
            return Result.error("用户手机号已存在!");
        }
        if ((!"".equals(appUser.getUserType())) && (appUser.getUserType() != null))
        {
            List<Map<String, Object>> list = this.appUserService.getAppUserByUserId(appUser.getUserId(), null);
            if (!((Map)list.get(0)).get("USER_TYPE").toString().equals(appUser.getUserType())) {
                appUser.setUserType(appUser.getUserType().substring(0, appUser.getUserType().length() - 1));
            }
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:ss:mm");
        Date date = new Date();
        appUser.setUpdateTime(simpleDateFormat.format(date));
        this.appUserService.updateById(appUser);
        this.appUserService.deleteOrgan(appUser.getUserId());
        this.appUserService.deleteRole(appUser.getUserId());
        this.appUserService.addOrgan(appUser.getUserId(), appUser.getDeptId());

        String[] roleIds = appUser.getRoleId().split(",");
        for (String roleId : roleIds) {
            this.appUserService.addRole(appUser.getUserId(), roleId);
        }
        Object list = this.appUserService.getAppUserByUserId(appUser.getUserId(), null);
        if (!appUser.getStatus().equals(((Map)((List)list).get(0)).get("STATUS").toString())) {
            if (appUser.getStatus().equals("0")) {
                this.appUserService.updateSysUserStatus(appUser.getUserId(), Integer.valueOf(1));
            } else {
                this.appUserService.updateSysUserStatus(appUser.getUserId(), Integer.valueOf(2));
            }
        }
        this.appUserService.updatePwdAndSalt(appUser.getUserId(), ((Map)((List)list).get(0)).get("USER_PASSWORD").toString(), ((Map)((List)list).get(0)).get("salt").toString());
        return Result.ok("修改成功!");
    }

    @AutoLog("用户机构-修改")
    @ApiOperation(value="用户机构-修改", notes="用户机构-修改")
    @PostMapping({"/appUserOrganEdit"})
    public Result<?> appUserOrganEdit(@RequestBody VeBaseAppUserOrgan appUserOrgan)
    {
        if (appUserOrgan.getId().equals(appUserOrgan.getPid())) {
            return Result.error("修改失败, 上级不能是自己!");
        }
        this.appUserOrganService.updateById(appUserOrgan);

        Result result = this.eduBootCommonFeignInterface.updateOrganUserRedis();
        return Result.ok("修改成功!");
    }

    @AutoLog("用户组信息-单个删除")
    @ApiOperation(value="用户组信息-单个删除", notes="用户组信息-单个删除")
    @PostMapping({"/appGroupDelete"})
    public Result<?> appGroupDelete(@RequestParam(name="id", required=true) String id)
    {
        this.appGroupService.removeById(id);
        return Result.ok("删除成功!");
    }

    @AutoLog("用户信息-单个删除")
    @ApiOperation(value="用户信息-单个删除", notes="用户信息-单个删除")
    @PostMapping({"/appUserDelete"})
    @Transactional
    public Result<?> appUserDelete(@RequestParam(name="id", required=true) String id)
    {
        VeBaseAppUser appUser = new VeBaseAppUser();
        appUser.setId(id);
        appUser.setIsDel("1");
        this.appUserService.updateById(appUser);
        VeBaseAppUser veBaseAppUser = (VeBaseAppUser)this.appUserService.getById(id);
        this.appUserService.updateSysUserDel(veBaseAppUser.getUserId());

        this.appUserService.deleteRole(veBaseAppUser.getUserId());

        this.appUserService.deleteOrgan(veBaseAppUser.getUserId());
        return Result.ok("删除成功!");
    }

    @AutoLog("应用信息-单个删除")
    @ApiOperation(value="应用信息-单个删除", notes="应用信息-单个删除")
    @PostMapping({"/appManageDelete"})
    @Transactional
    public Result<?> appManageDelete(@RequestParam(name="id", required=true) String id)
    {
        this.appManageService.removeById(id);

        this.veBaseSysMenuService.deleteByAppId(id);

        this.veBaseSysRoleMenuService.deleteModelByAppId(null, id);
        return Result.ok("删除成功!");
    }

    @AutoLog("用户机构-单个删除")
    @ApiOperation(value="用户机构-单个删除", notes="用户机构-单个删除")
    @PostMapping({"/appUserOrganDelete"})
    public Result<?> appUserOrganDelete(@RequestParam(name="id", required=true) String id)
    {
        this.appUserOrganService.removeById(id);

        Result result = this.eduBootCommonFeignInterface.updateOrganUserRedis();
        return Result.ok("删除成功!");
    }

    @AutoLog("用户组信息-批量删除")
    @ApiOperation(value="用户组信息-批量删除", notes="用户组信息-批量删除")
    @PostMapping({"/appGroupDeleteBatch"})
    public Result<?> appGroupDeleteBatch(@RequestParam(name="ids", required=true) String ids)
    {
        this.appGroupService.removeByIds(Arrays.asList(ids.split(",")));
        return Result.ok("批量删除成功!");
    }

    @AutoLog("应用信息-批量删除")
    @ApiOperation(value="应用信息-批量删除", notes="应用信息-批量删除")
    @PostMapping({"/appManageDeleteBatch"})
    @Transactional
    public Result<?> appManageDeleteBatch(@RequestParam(name="ids", required=true) String ids)
    {
        List<String> list = Arrays.asList(ids.split(","));
        this.appManageService.removeByIds(list);
        if (list.size() > 0) {
            for (String id : list)
            {
                this.veBaseSysMenuService.deleteByAppId(id);

                this.veBaseSysRoleMenuService.deleteModelByAppId(null, id);
            }
        }
        return Result.ok("批量删除成功!");
    }

    @AutoLog("用户信息-批量逻辑删除")
    @ApiOperation(value="用户信息-批量逻辑删除", notes="用户信息-批量逻辑删除")
    @PostMapping({"/appUserDeleteBatch"})
    public Result<?> appUserDeleteBatch(@RequestParam(name="ids", required=true) String ids)
    {
        String[] idArrray = ids.split(",");
        for (String id : idArrray)
        {
            VeBaseAppUser appUser = (VeBaseAppUser)this.appUserService.getById(id);
            if (appUser != null)
            {
                appUser.setIsDel("1");
                this.appUserService.updateById(appUser);
            }
        }
        return Result.ok("批量删除成功!");
    }

    @AutoLog("用户机构-批量删除")
    @ApiOperation(value="用户机构-批量删除", notes="用户机构-批量删除")
    @PostMapping({"/appUserOrganDeleteBatch"})
    public Result<?> appUserOrganDeleteBatch(@RequestParam(name="ids", required=true) String ids)
    {
        this.appUserOrganService.removeByIds(Arrays.asList(ids.split(",")));

        Result result = this.eduBootCommonFeignInterface.updateOrganUserRedis();
        return Result.ok("批量删除成功!");
    }

    @RequestMapping(value={"/appUserImportExcel"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
    public Result<?> appUserImportExcel(HttpServletRequest request, HttpServletResponse response)
    {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        Iterator localIterator = fileMap.entrySet().iterator();
        if (localIterator.hasNext())
        {
            Map.Entry<String, MultipartFile> entity = (Map.Entry)localIterator.next();
            MultipartFile file = (MultipartFile)entity.getValue();
            ImportParams params = new ImportParams();
            params.setTitleRows(0);
            params.setHeadRows(1);
            params.setNeedSave(true);
            try
            {
                int i = 0;
                List<VeBaseAppUser> listSysVeBaseAppUsers = ExcelImportUtil.importExcel(file.getInputStream(), VeBaseAppUser.class, params);
                Object localObject1;
                if (listSysVeBaseAppUsers.size() > 0) {
                    for (localObject1 = listSysVeBaseAppUsers.iterator(); ((Iterator)localObject1).hasNext();)
                    {
                        VeBaseAppUser veBaseAppUser = (VeBaseAppUser)((Iterator)localObject1).next();
                        if ((!"".equals(veBaseAppUser.getUserType())) && (veBaseAppUser.getUserType() != null)) {
                            if ((veBaseAppUser.getUserType().equals("教师")) || (veBaseAppUser.getUserType().equals("js"))) {
                                veBaseAppUser.setUserType("1");
                            } else if ((veBaseAppUser.getUserType().equals("学生")) || (veBaseAppUser.getUserType().equals("xs"))) {
                                veBaseAppUser.setUserType("2");
                            } else {
                                veBaseAppUser.setUserType("0");
                            }
                        }
                        if ((!"".equals(veBaseAppUser.getStatusVal())) && (veBaseAppUser.getStatusVal() != null)) {
                            if (veBaseAppUser.getStatusVal().equals("招聘")) {
                                veBaseAppUser.setStatusVal("0");
                            } else if (veBaseAppUser.getStatusVal().equals("在职")) {
                                veBaseAppUser.setStatusVal("1");
                            } else if (veBaseAppUser.getStatusVal().equals("退休")) {
                                veBaseAppUser.setStatusVal("2");
                            } else {
                                veBaseAppUser.setStatusVal("3");
                            }
                        }
                        if ((!"".equals(veBaseAppUser.getUserId())) && (veBaseAppUser.getUserId() != null) && (!"".equals(veBaseAppUser.getUserPassword())) && (veBaseAppUser.getUserPassword() != null))
                        {
                            veBaseAppUser.setStatus("0");
                            veBaseAppUser.setIsDel("0");
                            List<Map<String, Object>> list = this.appUserService.getAppUserByUserId(veBaseAppUser.getUserId(), null);
                            if (list.size() < 1)
                            {
                                this.appUserService.save(veBaseAppUser);
                                i++;
                            }
                        }
                    }
                }
                return Result.ok("文件导入成功！数据行数：" + i);
            }
            catch (Exception e)
            {
                List<VeBaseAppUser> listSysVeBaseAppUsers;
                log.error(e.getMessage(), e);
                return Result.error("文件导入失败:" + e.getMessage());
            }
            finally
            {
                try
                {
                    file.getInputStream().close();
                }
                catch (IOException e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return Result.error("文件导入失败！");
    }

    @RequestMapping(value={"/departmentImportExcel"}, method={org.springframework.web.bind.annotation.RequestMethod.POST})
    public Result<?> departmentImportExcel(HttpServletRequest request, HttpServletResponse response)
    {
        MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
        Map<String, MultipartFile> fileMap = multipartRequest.getFileMap();
        Iterator localIterator = fileMap.entrySet().iterator();
        if (localIterator.hasNext())
        {
            Map.Entry<String, MultipartFile> entity = (Map.Entry)localIterator.next();
            MultipartFile file = (MultipartFile)entity.getValue();
            ImportParams params = new ImportParams();
            params.setTitleRows(0);
            params.setHeadRows(1);
            params.setNeedSave(true);
            try
            {
                int i = 0;
                List<VeBaseAppUserOrgan> appUserOrganList = ExcelImportUtil.importExcel(file.getInputStream(), VeBaseAppUserOrgan.class, params);
                Object localObject1;
                if (appUserOrganList.size() > 0) {
                    for (localObject1 = appUserOrganList.iterator(); ((Iterator)localObject1).hasNext();)
                    {
                        VeBaseAppUserOrgan veBaseAppUserOrgan = (VeBaseAppUserOrgan)((Iterator)localObject1).next();
                        this.appUserOrganService.save(veBaseAppUserOrgan);
                        i++;
                    }
                }
                if (i == 0) {
                    return Result.error("文件导入失败, 请检查数据是否正确！");
                }
                Result result = this.eduBootCommonFeignInterface.updateOrganUserRedis();
                return Result.ok("文件导入成功！数据行数：" + i);
            }
            catch (Exception e)
            {
                List<VeBaseAppUserOrgan> appUserOrganList;
                log.error(e.getMessage(), e);
                return Result.error("文件导入失败:" + e.getMessage());
            }
            finally
            {
                try
                {
                    file.getInputStream().close();
                }
                catch (IOException e)
                {
                    log.error(e.getMessage(), e);
                }
            }
        }
        return Result.error("文件导入失败！");
    }
}
