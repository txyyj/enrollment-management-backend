package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.base.entity.VeBaseServiceKind;

public abstract interface IVeBaseServiceKindService
        extends IService<VeBaseServiceKind>
{}
