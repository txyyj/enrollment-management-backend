package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import org.edu.modules.base.entity.VeBaseUserAccess;
import org.edu.modules.base.mapper.VeBaseUserAccessMapper;
import org.edu.modules.base.service.IVeBaseUserAccessService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseUserAccessServiceImpl
        extends ServiceImpl<VeBaseUserAccessMapper, VeBaseUserAccess>
        implements IVeBaseUserAccessService
{
    @Autowired
    private VeBaseUserAccessMapper userAccessMapper;

    public List<Map<String, Object>> userAccessStatistics(String beginDate, String endDate)
    {
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        Date b_date = null;
        Date dt = null;
        List<Date> dates = new ArrayList();
        try
        {
            b_date = df.parse(beginDate);
            dates.add(b_date);

            long dayLength = (df.parse(endDate).getTime() - df.parse(beginDate).getTime()) / 86400000L;
            for (int i = 0; i < dayLength; i++)
            {
                dt = new Date(b_date.getTime() + 86400000L);
                dates.add(dt);
                b_date = dt;
            }
        }
        catch (ParseException e)
        {
            e.printStackTrace();
        }
        return this.userAccessMapper.userAccessStatistics(dates);
    }

    public int errorUserAccessAllList(VeBaseUserAccess veBaseUserAccess)
    {
        return this.userAccessMapper.errorUserAccessAllList(veBaseUserAccess);
    }

    public List<Map<String, Object>> errorUserAccessPageList(VeBaseUserAccess veBaseUserAccess)
    {
        return this.userAccessMapper.errorUserAccessPageList(veBaseUserAccess);
    }

    public int userAccessAllList(VeBaseUserAccess veBaseUserAccess)
    {
        return this.userAccessMapper.userAccessAllList(veBaseUserAccess);
    }

    public List<Map<String, Object>> userAccessPageList(VeBaseUserAccess veBaseUserAccess)
    {
        return this.userAccessMapper.userAccessPageList(veBaseUserAccess);
    }

    public List<Map<String, Object>> userAccessHourStatistics()
    {
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long current = System.currentTimeMillis();

        long zero = current / 86400000L * 86400000L - TimeZone.getDefault().getRawOffset();


        List<String> strings = new ArrayList();
        for (int i = 0; i < 24; i++)
        {
            strings.add(sd.format(new Date(zero + 3600000L)));
            zero += 3600000L;
        }
        return this.userAccessMapper.userAccessHourStatistics(strings);
    }

    public Map userAccessDayStatistics()
    {
        SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        long current = System.currentTimeMillis();

        long zero = current / 86400000L * 86400000L - TimeZone.getDefault().getRawOffset();
        String dt = sd.format(new Date(zero));
        return this.userAccessMapper.userAccessDayStatistics(dt);
    }

    public Map userAppAccessStatistics()
    {
        Map map = new HashMap();

        int allUser = this.userAccessMapper.userAppALLStatistics();

        int normalUser = this.userAccessMapper.userAppNormalStatistics();

        int lockUser = this.userAccessMapper.userAppLockStatistics();

        Map onUser = userAccessDayStatistics();
        long a_user = ((Long)onUser.get("onUser")).longValue();
        map.put("allUser", Integer.valueOf(allUser));
        map.put("normalUser", Integer.valueOf(normalUser));
        map.put("lockUser", Integer.valueOf(lockUser));
        map.put("onUser", Long.valueOf(a_user));
        map.put("offUser", Long.valueOf(normalUser - a_user));
        return map;
    }
}
