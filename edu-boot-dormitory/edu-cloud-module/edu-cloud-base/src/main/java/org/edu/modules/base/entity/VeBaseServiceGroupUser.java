package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_service_group_user")
@ApiModel(value="ve_base_service_group_user对象", description="群组关系表")
public class VeBaseServiceGroupUser
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="群组id", width=15.0D)
    @ApiModelProperty("群组id")
    private String groupId;
    @Excel(name="用户id", width=15.0D)
    @ApiModelProperty("用户id")
    private String userId;

    public String toString()
    {
        return "VeBaseServiceGroupUser(id=" + getId() + ", groupId=" + getGroupId() + ", userId=" + getUserId() + ")";
    }

    public VeBaseServiceGroupUser setGroupId(String groupId)
    {
        this.groupId = groupId;return this;
    }

    public VeBaseServiceGroupUser setId(String id)
    {
        this.id = id;return this;
    }

    public VeBaseServiceGroupUser setUserId(String userId)
    {
        this.userId = userId;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $groupId = getGroupId();result = result * 59 + ($groupId == null ? 43 : $groupId.hashCode());Object $userId = getUserId();result = result * 59 + ($userId == null ? 43 : $userId.hashCode());return result;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseServiceGroupUser)) {
            return false;
        }
        VeBaseServiceGroupUser other = (VeBaseServiceGroupUser)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$groupId = getGroupId();Object other$groupId = other.getGroupId();
        if (this$groupId == null ? other$groupId != null : !this$groupId.equals(other$groupId)) {
            return false;
        }
        Object this$userId = getUserId();Object other$userId = other.getUserId();return this$userId == null ? other$userId == null : this$userId.equals(other$userId);
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseServiceGroupUser;
    }

    public String getId()
    {
        return this.id;
    }

    public String getGroupId()
    {
        return this.groupId;
    }

    public String getUserId()
    {
        return this.userId;
    }
}
