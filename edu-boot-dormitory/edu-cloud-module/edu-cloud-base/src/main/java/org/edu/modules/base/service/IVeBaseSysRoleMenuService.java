package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseSysRoleMenu;

public abstract interface IVeBaseSysRoleMenuService
        extends IService<VeBaseSysRoleMenu>
{
    public abstract int deleteByRoleId(Integer paramInteger);

    public abstract int deleteModelByAppId(Integer paramInteger, String paramString);

    public abstract List<Map<String, Object>> getSysRoleMenuListByRoleIdAppId(Integer paramInteger, String paramString);

    public abstract int deleteRoleMenuByMenuId(String paramString);

    public abstract int deleteRoleUserByRoleId(String paramString);

    public abstract int deleteRoleMenuByRoleId(String paramString);
}
