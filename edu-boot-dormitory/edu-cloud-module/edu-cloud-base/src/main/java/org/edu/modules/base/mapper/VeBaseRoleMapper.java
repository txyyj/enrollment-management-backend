package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.edu.modules.base.entity.VeBaseRole;

public abstract interface VeBaseRoleMapper
        extends BaseMapper<VeBaseRole>
{
    public abstract List<VeBaseRole> getAllList(VeBaseRole paramVeBaseRole);

    public abstract List<VeBaseRole> getPageList(String paramString, Integer paramInteger1, Integer paramInteger2);

    public abstract List<VeBaseRole> getListByRoleId(String paramString);

    public abstract VeBaseRole getModelByRoleId(String paramString);
}
