package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_app_group")
@ApiModel(value="ve_base_app_group对象", description="用户组信息")
public class VeBaseAppGroup
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="角色名称", width=15.0D)
    @ApiModelProperty("角色名称")
    private String roleName;
    @Excel(name="排序", width=15.0D)
    @ApiModelProperty("排序")
    private Double sort;
    @Excel(name="是否内置", width=15.0D)
    @ApiModelProperty("是否内置(y是，n否)")
    private String inlay;
    @TableField(exist=false)
    private List<Map<String, Object>> mapList;
    @TableField(exist=false)
    private String[] strings;

    public VeBaseAppGroup setSort(Double sort)
    {
        this.sort = sort;return this;
    }

    public VeBaseAppGroup setRoleName(String roleName)
    {
        this.roleName = roleName;return this;
    }

    public VeBaseAppGroup setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseAppGroup(id=" + getId() + ", roleName=" + getRoleName() + ", sort=" + getSort() + ", inlay=" + getInlay() + ", mapList=" + getMapList() + ", strings=" + Arrays.deepToString(getStrings()) + ")";
    }

    public VeBaseAppGroup setStrings(String[] strings)
    {
        this.strings = strings;return this;
    }

    public VeBaseAppGroup setMapList(List<Map<String, Object>> mapList)
    {
        this.mapList = mapList;return this;
    }

    public VeBaseAppGroup setInlay(String inlay)
    {
        this.inlay = inlay;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $sort = getSort();result = result * 59 + ($sort == null ? 43 : $sort.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $roleName = getRoleName();result = result * 59 + ($roleName == null ? 43 : $roleName.hashCode());Object $inlay = getInlay();result = result * 59 + ($inlay == null ? 43 : $inlay.hashCode());Object $mapList = getMapList();result = result * 59 + ($mapList == null ? 43 : $mapList.hashCode());result = result * 59 + Arrays.deepHashCode(getStrings());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseAppGroup;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseAppGroup)) {
            return false;
        }
        VeBaseAppGroup other = (VeBaseAppGroup)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$sort = getSort();Object other$sort = other.getSort();
        if (this$sort == null ? other$sort != null : !this$sort.equals(other$sort)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$roleName = getRoleName();Object other$roleName = other.getRoleName();
        if (this$roleName == null ? other$roleName != null : !this$roleName.equals(other$roleName)) {
            return false;
        }
        Object this$inlay = getInlay();Object other$inlay = other.getInlay();
        if (this$inlay == null ? other$inlay != null : !this$inlay.equals(other$inlay)) {
            return false;
        }
        Object this$mapList = getMapList();Object other$mapList = other.getMapList();
        if (this$mapList == null ? other$mapList != null : !this$mapList.equals(other$mapList)) {
            return false;
        }
        return Arrays.deepEquals(getStrings(), other.getStrings());
    }

    public String getId()
    {
        return this.id;
    }

    public String getRoleName()
    {
        return this.roleName;
    }

    public Double getSort()
    {
        return this.sort;
    }

    public String getInlay()
    {
        return this.inlay;
    }

    public List<Map<String, Object>> getMapList()
    {
        return this.mapList;
    }

    public String[] getStrings()
    {
        return this.strings;
    }
}
