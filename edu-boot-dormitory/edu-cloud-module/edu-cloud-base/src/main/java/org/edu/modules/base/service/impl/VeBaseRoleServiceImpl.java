package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import org.edu.modules.base.entity.VeBaseRole;
import org.edu.modules.base.mapper.VeBaseRoleMapper;
import org.edu.modules.base.mapper.VeBaseRoleMenuMapper;
import org.edu.modules.base.service.IVeBaseRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseRoleServiceImpl
        extends ServiceImpl<VeBaseRoleMapper, VeBaseRole>
        implements IVeBaseRoleService
{
    @Autowired
    private VeBaseRoleMapper veBaseRoleMapper;
    @Autowired
    private VeBaseRoleMenuMapper veBaseRoleMenuMapper;

    public List<VeBaseRole> getAllList(VeBaseRole baseRole)
    {
        List<VeBaseRole> list = this.veBaseRoleMapper.getAllList(baseRole);
        for (VeBaseRole role : list) {
            role.setIds(this.veBaseRoleMenuMapper.getArrays(role.getId()));
        }
        return list;
    }

    public List<VeBaseRole> getPageList(String rolename, Integer startIndex, Integer pageSize)
    {
        List<VeBaseRole> list = this.veBaseRoleMapper.getPageList(rolename, startIndex, pageSize);
        for (VeBaseRole role : list) {
            role.setIds(this.veBaseRoleMenuMapper.getArrays(role.getId()));
        }
        return list;
    }

    public List<VeBaseRole> getListByRoleId(String roleId)
    {
        return this.veBaseRoleMapper.getListByRoleId(roleId);
    }

    public VeBaseRole getModelByRoleId(String roleId)
    {
        return this.veBaseRoleMapper.getModelByRoleId(roleId);
    }

    public void baseRoleMenuDelete(String role_id)
    {
        this.veBaseRoleMenuMapper.baseRoleMenuDelete(role_id);
    }
}
