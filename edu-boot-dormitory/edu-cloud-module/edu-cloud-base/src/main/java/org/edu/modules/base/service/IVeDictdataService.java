package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.base.entity.VeDictdata;
import org.edu.modules.base.entity.VeDictionary;

public abstract interface IVeDictdataService
        extends IService<VeDictdata>
{
    public abstract List<VeDictdata> selectAll(VeDictionary paramVeDictionary, Integer paramInteger1, Integer paramInteger2);

    public abstract int getSumPage(VeDictionary paramVeDictionary);

    public abstract List<String> getEnterNatures();

    public abstract List<String> getEnterScale();
}
