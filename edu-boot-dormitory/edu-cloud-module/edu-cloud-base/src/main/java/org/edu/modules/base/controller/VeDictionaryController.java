package org.edu.modules.base.controller;

import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import javax.transaction.Transactional;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.base.controller.BaseController;
import org.edu.modules.base.entity.VeDictionary;
import org.edu.modules.base.feign.EduBootCommonFeignInterface;
import org.edu.modules.base.redis.RedisUtils;
import org.edu.modules.base.service.IVeDictionaryService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags={"数据字典管理 "})
@RestController
@RequestMapping({"/base/veDictionary"})
@ApiSort(60)
public class VeDictionaryController
        extends BaseController<VeDictionary, IVeDictionaryService>
{
    private static final Logger log = LoggerFactory.getLogger(VeDictionaryController.class);
    @Autowired
    private IVeDictionaryService veDictionaryService;
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private EduBootCommonFeignInterface eduBootCommonFeignInterface;

    @AutoLog("数据字典管理-查询菜单")
    @ApiOperation(value="查询菜单", notes="数据字典管理-查询菜单")
    @GetMapping({"/selectTreeList"})
    public Result<?> selectTreeList()
    {
        List<VeDictionary> veDictionaryList = this.veDictionaryService.getTreeList();
        return Result.OK(veDictionaryList);
    }

    @AutoLog("数据字典管理-新增信息")
    @ApiOperation(value="新增信息", notes="数据字典管理-新增信息")
    @PostMapping({"/add"})
    public Result<?> add(@RequestBody VeDictionary veDictionary)
    {
        veDictionary.setTerminalId(1);
        this.veDictionaryService.saves(veDictionary);
        veDictionary.setInterfaceUserId("09d5e1e7f9b049008eee645c783a1d66");

        this.redisUtils.set("queryDictionaryTreeList", this.eduBootCommonFeignInterface.queryDictionaryTreeListRedis(veDictionary.getInterfaceUserId()));
        return Result.OK("添加成功！");
    }

    @AutoLog("数据字典管理-修改信息")
    @ApiOperation(value="修改信息", notes="数据字典管理-修改信息")
    @PostMapping({"/edit"})
    public Result<?> edit(@RequestBody VeDictionary veDictionary)
    {
        this.veDictionaryService.updateById(veDictionary);

        veDictionary.setInterfaceUserId("09d5e1e7f9b049008eee645c783a1d66");
        this.redisUtils.set("queryDictionaryTreeList", this.eduBootCommonFeignInterface.queryDictionaryTreeListRedis(veDictionary.getInterfaceUserId()));
        return Result.OK("编辑成功!");
    }

    @AutoLog("数据字典管理-通过id删除")
    @ApiOperation(value="通过id删除", notes="数据字典管理-通过id删除")
    @PostMapping({"/delete"})
    @Transactional
    public Result<?> delete(@RequestParam(name="id", required=true) String id, @RequestParam(name="interfaceUserId", required=true) String interfaceUserId)
    {
        VeDictionary veDictionary = (VeDictionary)this.veDictionaryService.getById(id);

        this.veDictionaryService.deleteDictionary(id);
        if (veDictionary != null) {
            this.veDictionaryService.deleteDictData(veDictionary.getCode());
        }
        interfaceUserId = "09d5e1e7f9b049008eee645c783a1d66";
        this.redisUtils.set("queryDictionaryTreeList", this.eduBootCommonFeignInterface.queryDictionaryTreeListRedis(interfaceUserId));
        return Result.OK("删除成功!");
    }
}
