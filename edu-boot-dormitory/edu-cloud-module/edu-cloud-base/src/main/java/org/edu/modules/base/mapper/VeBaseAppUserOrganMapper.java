package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.edu.modules.base.entity.VeBaseAppUserOrgan;

public abstract interface VeBaseAppUserOrganMapper
        extends BaseMapper<VeBaseAppUserOrgan>
{
    public abstract List<VeBaseAppUserOrgan> getRootList();

    public abstract List<VeBaseAppUserOrgan> getBodyList();
}
