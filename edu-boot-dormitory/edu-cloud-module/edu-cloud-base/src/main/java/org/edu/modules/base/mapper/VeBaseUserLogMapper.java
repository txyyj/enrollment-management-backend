package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseUserLog;

public abstract interface VeBaseUserLogMapper
        extends BaseMapper<VeBaseUserLog>
{
    public abstract int userLogAllList(VeBaseUserLog paramVeBaseUserLog);

    public abstract List<Map<String, Object>> userLogPageList(VeBaseUserLog paramVeBaseUserLog);
}
