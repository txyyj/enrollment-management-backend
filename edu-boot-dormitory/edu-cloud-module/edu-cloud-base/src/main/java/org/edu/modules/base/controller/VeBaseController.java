package org.edu.modules.base.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.xiaoymin.knife4j.annotations.ApiSort;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.edu.common.api.vo.Result;
import org.edu.common.aspect.annotation.AutoLog;
import org.edu.common.system.query.QueryGenerator;
import org.edu.common.system.vo.LoginUser;
import org.edu.modules.base.entity.PageBean;
import org.edu.modules.base.entity.VeBaseMenu;
import org.edu.modules.base.entity.VeBaseOperaterLog;
import org.edu.modules.base.entity.VeBaseRole;
import org.edu.modules.base.entity.VeBaseRoleMenu;
import org.edu.modules.base.entity.VeBaseUserLog;
import org.edu.modules.base.service.IVeBaseMenuService;
import org.edu.modules.base.service.IVeBaseOperaterLogService;
import org.edu.modules.base.service.IVeBaseRoleMenuService;
import org.edu.modules.base.service.IVeBaseRoleService;
import org.edu.modules.base.service.IVeBaseUserLogService;
import org.edu.modules.base.util.ServersMessage;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@Api(tags={"基础数据"})
@RestController
@RequestMapping({"/base/veBase"})
@ApiSort(60)
public class VeBaseController
{
    private static final Logger log = LoggerFactory.getLogger(VeBaseController.class);
    @Autowired
    private IVeBaseUserLogService userLogService;
    @Autowired
    private IVeBaseRoleService baseRoleService;
    @Autowired
    private IVeBaseOperaterLogService operaterLogService;
    @Autowired
    private IVeBaseMenuService menuService;
    @Autowired
    private IVeBaseRoleMenuService veBaseRoleMenuService;

    @AutoLog("系统日志信息-分页列表查询")
    @ApiOperation(value="系统日志-分页列表查询", notes="系统日志-分页列表查询")
    @GetMapping({"/userLogPageList"})
    public Result<?> userLogPageList(VeBaseUserLog userLog, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize)
    {
        int num = this.userLogService.userLogAllList(userLog);

        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), num);

        userLog.setStartIndex(Integer.valueOf(pb.getStartIndex()));
        userLog.setPageSize(pageSize);
        pb.setList(this.userLogService.userLogPageList(userLog));
        return Result.ok(pb);
    }

    @AutoLog("角色管理-分页列表查询")
    @ApiOperation(value="角色管理-分页列表查询", notes="角色管理-分页列表查询")
    @GetMapping({"/baseRolePageList"})
    public Result<?> baseRolePageList(VeBaseRole baseRole, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        List<VeBaseRole> list = this.baseRoleService.getAllList(baseRole);
        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), list.size());
        pb.setList(this.baseRoleService.getPageList(baseRole.getRolename(), Integer.valueOf(pb.getStartIndex()), pageSize));
        return Result.ok(pb);
    }

    @AutoLog("操作日志-分页列表查询")
    @ApiOperation(value="操作日志-分页列表查询", notes="操作日志-分页列表查询")
    @GetMapping({"/operaterLogPageList"})
    public Result<?> operaterLogPageList(VeBaseOperaterLog operaterLog, @RequestParam(name="pageNo", defaultValue="1") Integer pageNo, @RequestParam(name="pageSize", defaultValue="10") Integer pageSize, HttpServletRequest req)
    {
        List<VeBaseOperaterLog> list = this.operaterLogService.getOperaterLogAllList(operaterLog);

        PageBean pb = new PageBean(pageNo.intValue(), pageSize.intValue(), list.size());

        operaterLog.setStartIndex(Integer.valueOf(pb.getStartIndex()));
        operaterLog.setPageSize(pageSize);
        pb.setList(this.operaterLogService.getOperaterLogPageList(operaterLog));
        return Result.ok(pb);
    }

    @AutoLog("获取服务器信息")
    @ApiOperation(value="获取服务器信息", notes="获取服务器信息")
    @GetMapping({"/serverMessage"})
    public Result<?> serverMessage()
            throws UnknownHostException
    {
        ServersMessage serversMessage = new ServersMessage();
        Map map = serversMessage.queryServersMessage();
        return Result.ok(map);
    }

    @AutoLog("通过id查询系统日志信息")
    @ApiOperation(value="通过id查询系统日志信息", notes="通过id查询系统日志信息")
    @GetMapping({"/userLogById"})
    public Result<?> userLogById(@RequestParam(name="id", required=true) String id)
    {
        VeBaseUserLog userLog = (VeBaseUserLog)this.userLogService.getById(id);
        if (userLog == null) {
            return Result.error("未找到对应数据");
        }
        return Result.OK(userLog);
    }

    @AutoLog("菜单信息查询")
    @ApiOperation(value="菜单信息查询", notes="菜单信息查询")
    @GetMapping({"/roleMenuList"})
    public Result<?> roleMenuList()
    {
        List<VeBaseMenu> list = this.menuService.getTreeList();
        return Result.ok(list);
    }

    @AutoLog("添加操作日志-添加")
    @ApiOperation(value="添加操作日志-添加", notes="添加操作日志-添加")
    @PostMapping({"/operaterLogAdd"})
    public Result<?> operaterLogAdd(@RequestBody VeBaseOperaterLog operaterLog)
    {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        operaterLog.setId(uuid);
        this.operaterLogService.save(operaterLog);
        return Result.ok("添加成功!");
    }

    @AutoLog("添加角色和角色的菜单-添加")
    @ApiOperation(value="添加角色和角色菜单", notes="添加角色和角色菜单")
    @PostMapping({"/roleMenuAdd"})
    @Transactional
    public Result<?> roleMenuAdd(@RequestBody VeBaseRole role)
    {
        if (role == null) {
            return Result.ok("角色不能为空!");
        }
        List<VeBaseRole> list = this.baseRoleService.getListByRoleId(role.getId());
        if (list.size() > 0) {
            return Result.ok("角色号已存在!");
        }
        this.baseRoleService.save(role);
        VeBaseRole veBaseRole = this.baseRoleService.getModelByRoleId(role.getId());
        if (role.getIds().length > 0)
        {
            List<VeBaseRoleMenu> detailList = new ArrayList();
            for (String id : role.getIds())
            {
                VeBaseRoleMenu roleMenu = new VeBaseRoleMenu();
                roleMenu.setRoleid(veBaseRole.getId());
                roleMenu.setMenuid(id);
                detailList.add(roleMenu);
            }
            this.veBaseRoleMenuService.saveBatch(detailList);
        }
        return Result.ok("添加成功!");
    }

    @AutoLog("修改角色和角色的菜单-添加")
    @ApiOperation(value="修改角色和角色的菜单", notes="修改角色和角色的菜单")
    @PostMapping({"/roleMenuEdit"})
    @Transactional
    public Result<?> roleMenuEdit(@RequestBody VeBaseRole role)
    {
        if (role == null) {
            return Result.ok("角色不能为空!");
        }
        this.baseRoleService.updateById(role);
        this.baseRoleService.baseRoleMenuDelete(role.getId());
        if (role.getIds().length > 0)
        {
            List<VeBaseRoleMenu> detailList = new ArrayList();
            for (String id : role.getIds())
            {
                VeBaseRoleMenu roleMenu = new VeBaseRoleMenu();
                roleMenu.setRoleid(role.getId());
                roleMenu.setMenuid(id);
                detailList.add(roleMenu);
            }
            this.veBaseRoleMenuService.saveBatch(detailList);
        }
        return Result.ok("授权成功!");
    }

    @AutoLog("角色信息-单个删除")
    @ApiOperation(value="角色信息-单个删除", notes="角色信息-单个删除")
    @PostMapping({"/baseRoleDelete"})
    public Result<?> baseRoleDelete(@RequestParam(name="id", required=true) String id)
    {
        this.baseRoleService.removeById(id);
        return Result.ok("删除成功!");
    }

    @RequestMapping({"/exportXls"})
    public ModelAndView exportXls(VeBaseOperaterLog operatorLog, HttpServletRequest request)
    {
        QueryWrapper<VeBaseOperaterLog> queryWrapper = QueryGenerator.initQueryWrapper(operatorLog, request.getParameterMap());

        ModelAndView modelAndView = new ModelAndView(new JeecgEntityExcelView());
        List<VeBaseOperaterLog> pageList = this.operaterLogService.list(queryWrapper);

        modelAndView.addObject("fileName", "操作日志信息");
        modelAndView.addObject("entity", VeBaseOperaterLog.class);
        LoginUser user = (LoginUser)SecurityUtils.getSubject().getPrincipal();
        modelAndView.addObject("params", new ExportParams("操作日志列表", "导出人:" + user.getRealname(), "导出信息"));
        modelAndView.addObject("data", pageList);
        return modelAndView;
    }
}
