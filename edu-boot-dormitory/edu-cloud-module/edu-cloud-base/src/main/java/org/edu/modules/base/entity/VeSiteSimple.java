package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_site_simple")
@ApiModel(value="ve_site_simple对象", description="单页面信息")
public class VeSiteSimple
        implements Serializable
{
    private static final long serialVersionUID = 1L;
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="内容栏目ID", width=15.0D)
    @ApiModelProperty("内容栏目ID")
    private Integer cateid;
    @Excel(name="摘要", width=15.0D)
    @ApiModelProperty("摘要")
    private String description;
    @Excel(name="内容", width=15.0D)
    @ApiModelProperty("内容")
    private String content;
    @Excel(name="更新用户ID", width=15.0D)
    @ApiModelProperty("更新用户ID")
    private Integer userid;
    @Excel(name="添加时间", width=15.0D)
    @ApiModelProperty("添加时间")
    private Integer addtime;
    @Excel(name="更新时间", width=15.0D)
    @ApiModelProperty("更新时间")
    private Integer updatetime;
    @Excel(name="站点ID", width=15.0D)
    @ApiModelProperty("站点ID")
    private Integer siteid;
    @Excel(name="终端系统ID", width=15.0D)
    @ApiModelProperty("终端系统ID")
    private Integer terminalid;
    @Excel(name="多租户id", width=15.0D)
    @ApiModelProperty("多租户id")
    private Integer placeId;

    public VeSiteSimple setDescription(String description)
    {
        this.description = description;return this;
    }

    public VeSiteSimple setCateid(Integer cateid)
    {
        this.cateid = cateid;return this;
    }

    public VeSiteSimple setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeSiteSimple(id=" + getId() + ", cateid=" + getCateid() + ", description=" + getDescription() + ", content=" + getContent() + ", userid=" + getUserid() + ", addtime=" + getAddtime() + ", updatetime=" + getUpdatetime() + ", siteid=" + getSiteid() + ", terminalid=" + getTerminalid() + ", placeId=" + getPlaceId() + ")";
    }

    public VeSiteSimple setPlaceId(Integer placeId)
    {
        this.placeId = placeId;return this;
    }

    public VeSiteSimple setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public VeSiteSimple setSiteid(Integer siteid)
    {
        this.siteid = siteid;return this;
    }

    public VeSiteSimple setUpdatetime(Integer updatetime)
    {
        this.updatetime = updatetime;return this;
    }

    public VeSiteSimple setAddtime(Integer addtime)
    {
        this.addtime = addtime;return this;
    }

    public VeSiteSimple setUserid(Integer userid)
    {
        this.userid = userid;return this;
    }

    public VeSiteSimple setContent(String content)
    {
        this.content = content;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $cateid = getCateid();result = result * 59 + ($cateid == null ? 43 : $cateid.hashCode());Object $userid = getUserid();result = result * 59 + ($userid == null ? 43 : $userid.hashCode());Object $addtime = getAddtime();result = result * 59 + ($addtime == null ? 43 : $addtime.hashCode());Object $updatetime = getUpdatetime();result = result * 59 + ($updatetime == null ? 43 : $updatetime.hashCode());Object $siteid = getSiteid();result = result * 59 + ($siteid == null ? 43 : $siteid.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $placeId = getPlaceId();result = result * 59 + ($placeId == null ? 43 : $placeId.hashCode());Object $description = getDescription();result = result * 59 + ($description == null ? 43 : $description.hashCode());Object $content = getContent();result = result * 59 + ($content == null ? 43 : $content.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeSiteSimple;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeSiteSimple)) {
            return false;
        }
        VeSiteSimple other = (VeSiteSimple)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$cateid = getCateid();Object other$cateid = other.getCateid();
        if (this$cateid == null ? other$cateid != null : !this$cateid.equals(other$cateid)) {
            return false;
        }
        Object this$userid = getUserid();Object other$userid = other.getUserid();
        if (this$userid == null ? other$userid != null : !this$userid.equals(other$userid)) {
            return false;
        }
        Object this$addtime = getAddtime();Object other$addtime = other.getAddtime();
        if (this$addtime == null ? other$addtime != null : !this$addtime.equals(other$addtime)) {
            return false;
        }
        Object this$updatetime = getUpdatetime();Object other$updatetime = other.getUpdatetime();
        if (this$updatetime == null ? other$updatetime != null : !this$updatetime.equals(other$updatetime)) {
            return false;
        }
        Object this$siteid = getSiteid();Object other$siteid = other.getSiteid();
        if (this$siteid == null ? other$siteid != null : !this$siteid.equals(other$siteid)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$placeId = getPlaceId();Object other$placeId = other.getPlaceId();
        if (this$placeId == null ? other$placeId != null : !this$placeId.equals(other$placeId)) {
            return false;
        }
        Object this$description = getDescription();Object other$description = other.getDescription();
        if (this$description == null ? other$description != null : !this$description.equals(other$description)) {
            return false;
        }
        Object this$content = getContent();Object other$content = other.getContent();return this$content == null ? other$content == null : this$content.equals(other$content);
    }

    public Integer getId()
    {
        return this.id;
    }

    public Integer getCateid()
    {
        return this.cateid;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getContent()
    {
        return this.content;
    }

    public Integer getUserid()
    {
        return this.userid;
    }

    public Integer getAddtime()
    {
        return this.addtime;
    }

    public Integer getUpdatetime()
    {
        return this.updatetime;
    }

    public Integer getSiteid()
    {
        return this.siteid;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public Integer getPlaceId()
    {
        return this.placeId;
    }
}
