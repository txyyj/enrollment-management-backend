package org.edu.modules.base.entity;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/12/13 14:34
 */
public class PageBean<T>
        implements Serializable
{
    private int pageNum;
    private int pageSize;
    private int totalRecord;
    private int totalPage;
    private int startIndex;
    private List<T> list;
    private int start;
    private int end;

    public PageBean(int pageNum, int pageSize, int totalRecord)
    {
        this.pageNum = pageNum;
        this.pageSize = pageSize;
        this.totalRecord = totalRecord;
        if (totalRecord % pageSize == 0) {
            this.totalPage = (totalRecord / pageSize);
        } else {
            this.totalPage = (totalRecord / pageSize + 1);
        }
        this.startIndex = ((pageNum - 1) * pageSize);
        if (this.totalPage <= 5)
        {
            this.end = this.totalPage;
        }
        else
        {
            this.start = (pageNum - 2);
            this.end = (pageNum + 2);
            if (this.start < 0)
            {
                this.start = 1;
                this.end = 5;
            }
            if (this.end > this.totalPage)
            {
                this.end = this.totalPage;
                this.start = (this.end - 5);
            }
        }
    }

    public int getPageNum()
    {
        return this.pageNum;
    }

    public void setPageNum(int pageNum)
    {
        this.pageNum = pageNum;
    }

    public int getPageSize()
    {
        return this.pageSize;
    }

    public void setPageSize(int pageSize)
    {
        this.pageSize = pageSize;
    }

    public int getTotalRecord()
    {
        return this.totalRecord;
    }

    public void setTotalRecord(int totalRecord)
    {
        this.totalRecord = totalRecord;
    }

    public int getTotalPage()
    {
        return this.totalPage;
    }

    public void setTotalPage(int totalPage)
    {
        this.totalPage = totalPage;
    }

    public int getStartIndex()
    {
        return this.startIndex;
    }

    public void setStartIndex(int startIndex)
    {
        this.startIndex = startIndex;
    }

    public List<T> getList()
    {
        return this.list;
    }

    public void setList(List<T> list)
    {
        this.list = list;
    }

    public int getStart()
    {
        return this.start;
    }

    public void setStart(int start)
    {
        this.start = start;
    }

    public int getEnd()
    {
        return this.end;
    }

    public void setEnd(int end)
    {
        this.end = end;
    }
}

