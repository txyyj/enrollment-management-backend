package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseService;
import org.edu.modules.base.mapper.VeBaseServiceMapper;
import org.edu.modules.base.service.IVeBaseServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseServiceServiceImpl
        extends ServiceImpl<VeBaseServiceMapper, VeBaseService>
        implements IVeBaseServiceService
{
    @Autowired
    private VeBaseServiceMapper serviceModelMapper;

    public int serviceModelAllList(VeBaseService serviceModel)
    {
        return this.serviceModelMapper.serviceModelAllList(serviceModel);
    }

    public List<Map<String, Object>> serviceModelPageList(VeBaseService serviceModel)
    {
        return this.serviceModelMapper.serviceModelPageList(serviceModel);
    }

    public int serviceGroupAppManageAllList(VeBaseService veBaseService)
    {
        return this.serviceModelMapper.serviceGroupAppManageAllList(veBaseService);
    }

    public List<Map<String, Object>> serviceGroupAppManagePageList(VeBaseService veBaseService)
    {
        return this.serviceModelMapper.serviceGroupAppManagePageList(veBaseService);
    }

    public int deleteServiceGroupAppManage(String groupId)
    {
        return 0;
    }

    public int serviceGroupAppManageAdd(String groupId, String[] appId)
    {
        return 0;
    }

    public VeBaseService getServiceByName(String id, String name)
    {
        return this.serviceModelMapper.getServiceByName(id, name);
    }
}
