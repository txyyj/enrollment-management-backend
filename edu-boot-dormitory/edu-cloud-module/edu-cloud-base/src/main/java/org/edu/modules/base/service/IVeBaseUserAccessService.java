package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseUserAccess;

public abstract interface IVeBaseUserAccessService
        extends IService<VeBaseUserAccess>
{
    public abstract List<Map<String, Object>> userAccessStatistics(String paramString1, String paramString2);

    public abstract int errorUserAccessAllList(VeBaseUserAccess paramVeBaseUserAccess);

    public abstract List<Map<String, Object>> errorUserAccessPageList(VeBaseUserAccess paramVeBaseUserAccess);

    public abstract int userAccessAllList(VeBaseUserAccess paramVeBaseUserAccess);

    public abstract List<Map<String, Object>> userAccessPageList(VeBaseUserAccess paramVeBaseUserAccess);

    public abstract List<Map<String, Object>> userAccessHourStatistics();

    public abstract Map userAccessDayStatistics();

    public abstract Map userAppAccessStatistics();
}
