package org.edu.modules.base.redis;

import java.lang.reflect.Method;
import org.springframework.cache.interceptor.KeyGenerator;

public class RedisConfig$1 implements KeyGenerator
{


    RedisConfig$1(RedisConfig this$0) {}

    public Object generate(Object target, Method method, Object... params)
    {
        StringBuilder sb = new StringBuilder();
        sb.append(target.getClass().getName());
        sb.append(method.getName());
        for (Object obj : params) {
            sb.append(obj.toString());
        }
        return sb.toString();
    }
}
