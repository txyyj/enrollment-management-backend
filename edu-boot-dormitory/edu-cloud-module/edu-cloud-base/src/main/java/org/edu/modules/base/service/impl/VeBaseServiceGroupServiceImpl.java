package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseServiceGroup;
import org.edu.modules.base.mapper.VeBaseServiceGroupMapper;
import org.edu.modules.base.service.IVeBaseServiceGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseServiceGroupServiceImpl
        extends ServiceImpl<VeBaseServiceGroupMapper, VeBaseServiceGroup>
        implements IVeBaseServiceGroupService
{
    @Autowired
    private VeBaseServiceGroupMapper veBaseServiceGroupMapper;

    public int serviceGroupAllList(String groupName)
    {
        return this.veBaseServiceGroupMapper.serviceGroupAllList(groupName);
    }

    public List<Map<String, Object>> serviceGroupPageList(String groupName, Integer startIndex, Integer pageSize)
    {
        return this.veBaseServiceGroupMapper.serviceGroupPageList(groupName, startIndex, pageSize);
    }

    public int deleteGroupAppByGroupId(String groupId)
    {
        return this.veBaseServiceGroupMapper.deleteGroupAppByGroupId(groupId);
    }

    public int addGroupApp(String groupId, String[] appId)
    {
        return this.veBaseServiceGroupMapper.addGroupApp(groupId, appId);
    }

    public int deleteServiceGroupAppManage(String groupId, String[] appId)
    {
        return this.veBaseServiceGroupMapper.deleteServiceGroupAppManage(groupId, appId);
    }

    public int deleteByAppId(String appId)
    {
        return this.veBaseServiceGroupMapper.deleteByAppId(appId);
    }

    public VeBaseServiceGroup getServiceGroupByGroupName(String id, String groupName)
    {
        return this.veBaseServiceGroupMapper.getServiceGroupByGroupName(id, groupName);
    }
}
