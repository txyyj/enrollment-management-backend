package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseSysMenu;
import org.edu.modules.base.entity.VeBaseSysRole;

public abstract interface VeBaseSysMenuMapper
        extends BaseMapper<VeBaseSysMenu>
{
    public abstract List<Map<String, Object>> getRootList(String paramString);

    public abstract List<Map<String, Object>> getBodyList(String paramString);

    public abstract int getSysRoleAllList(VeBaseSysRole paramVeBaseSysRole);

    public abstract List<Map<String, Object>> getSysRolePageList(VeBaseSysRole paramVeBaseSysRole);

    public abstract Integer addSysMenu(VeBaseSysMenu paramVeBaseSysMenu);

    public abstract VeBaseSysMenu getSysMenuByPid(Integer paramInteger);

    public abstract int deleteByAppId(String paramString);
}
