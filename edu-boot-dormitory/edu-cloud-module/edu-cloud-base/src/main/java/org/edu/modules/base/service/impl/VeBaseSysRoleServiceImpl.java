package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.base.entity.VeBaseSysRole;
import org.edu.modules.base.mapper.VeBaseSysRoleMapper;
import org.edu.modules.base.service.IVeBaseSysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseSysRoleServiceImpl
        extends ServiceImpl<VeBaseSysRoleMapper, VeBaseSysRole>
        implements IVeBaseSysRoleService
{
    @Autowired
    private VeBaseSysRoleMapper veBaseSysRoleMapper;

    public VeBaseSysRole getModelByRoleText(String text)
    {
        return this.veBaseSysRoleMapper.getModelByRoleText(text);
    }

    public VeBaseSysRole getModelByIdAndText(Integer id, String text)
    {
        return this.veBaseSysRoleMapper.getModelByIdAndText(id, text);
    }

    public void deleteSysRoleUserById(String userId)
    {
        this.veBaseSysRoleMapper.deleteSysRoleUserById(userId);
    }

    public void addSysRoleUserById(String userId, String[] id)
    {
        this.veBaseSysRoleMapper.addSysRoleUserById(userId, id);
    }

    public int addSysRole(VeBaseSysRole veBaseSysRole)
    {
        return this.veBaseSysRoleMapper.addSysRole(veBaseSysRole);
    }
}
