package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.util.Map;
import org.edu.common.api.vo.Result;
import org.edu.modules.base.entity.VeBaseAppUser;

public abstract interface IVeBaseAppUserService
        extends IService<VeBaseAppUser>
{
    public abstract List<VeBaseAppUser> zombieUserAllList(VeBaseAppUser paramVeBaseAppUser);

    public abstract int getLoginUserAllList(VeBaseAppUser paramVeBaseAppUser);

    public abstract List<VeBaseAppUser> getLoginUserPageList(VeBaseAppUser paramVeBaseAppUser);

    public abstract int getAppUserAllList(VeBaseAppUser paramVeBaseAppUser);

    public abstract List<Map<String, Object>> getAppUserPageList(VeBaseAppUser paramVeBaseAppUser);

    public abstract Result updateAppUserPasswordById(String paramString1, String paramString2, String paramString3, String paramString4);

    public abstract VeBaseAppUser getPwdAndSalt(VeBaseAppUser paramVeBaseAppUser);

    public abstract int serviceGroupAppUserAllList(VeBaseAppUser paramVeBaseAppUser);

    public abstract List<Map<String, Object>> serviceGroupAppUserPageList(VeBaseAppUser paramVeBaseAppUser);

    public abstract int deleteServiceGroupAppUser(String paramString);

    public abstract int serviceGroupAppUserAdd(String paramString, String[] paramArrayOfString);

    public abstract int deleteGroupAppUser(String paramString, String[] paramArrayOfString);

    public abstract List<Map<String, Object>> getAppOrganList();

    public abstract List<Map<String, Object>> getSysRoleList();

    public abstract int addOrgan(String paramString1, String paramString2);

    public abstract int addRole(String paramString1, String paramString2);

    public abstract int deleteOrgan(String paramString);

    public abstract int deleteRole(String paramString);

    public abstract List<Map<String, Object>> getAppUserByUserId(String paramString1, String paramString2);

    public abstract List<Map<String, Object>> getAppUserAllListByUserId(String paramString1, String paramString2);

    public abstract List<Map<String, Object>> getAppUserByUserTel(String paramString1, String paramString2);

    public abstract int getAppUserAndStudentAndTeacherList(VeBaseAppUser paramVeBaseAppUser);

    public abstract List<Map<String, Object>> getAppUserAndStudentAndTeacherPageList(VeBaseAppUser paramVeBaseAppUser);

    public abstract int addSysUser(VeBaseAppUser paramVeBaseAppUser);

    public abstract int updateSysUserStatus(String paramString, Integer paramInteger);

    public abstract int updateSysUserDel(String paramString);

    public abstract List<Map<String, Object>> getTeacherListByUserId(String paramString);

    public abstract List<Map<String, Object>> getStudentListByUserId(String paramString);

    public abstract String pwdVerify(String paramString);

    public abstract int updatePwdAndSalt(String paramString1, String paramString2, String paramString3);
}
