package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.edu.modules.base.entity.VeDictionary;

@Mapper
public abstract interface VeDictionaryMapper
        extends BaseMapper<VeDictionary>
{
    public abstract List<VeDictionary> selectAll(@Param("veDictionary") VeDictionary paramVeDictionary, @Param("startLine") Integer paramInteger1, @Param("pageSize") Integer paramInteger2);

    public abstract List<VeDictionary> getRootList();

    public abstract List<VeDictionary> getBodyList();

    public abstract void saves(VeDictionary paramVeDictionary);

    public abstract int deleteDictionary(String paramString);

    public abstract int deleteDictData(String paramString);
}
