package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.base.entity.VeBaseMenu;
import org.edu.modules.base.mapper.VeBaseMenuMapper;
import org.edu.modules.base.service.IVeBaseMenuService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/12/13 16:50
 */
@Service
public class VeBaseMenuServiceImpl extends ServiceImpl<VeBaseMenuMapper, VeBaseMenu> implements IVeBaseMenuService {
    @Override
    public List<VeBaseMenu> getTreeList() {
        return baseMapper.getBodyList();
    }
}
