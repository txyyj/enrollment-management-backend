package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_sys_role_menu")
@ApiModel(value="ve_base_sys_role_menu对象", description="权限管理-角色与菜单关系表")
public class VeBaseSysRoleMenu
        implements Serializable
{
    @TableId(type=IdType.AUTO)
    @ApiModelProperty("id")
    private Integer id;
    @Excel(name="角色ID", width=15.0D)
    @ApiModelProperty("角色ID")
    private Integer roleId;
    @Excel(name="菜单ID", width=15.0D)
    @ApiModelProperty("菜单ID")
    private Integer menuId;
    @Excel(name="系统终端ID", width=15.0D)
    @ApiModelProperty("系统终端ID")
    private Integer terminalid;
    @Excel(name="应用ID", width=15.0D)
    @ApiModelProperty("应用ID")
    private String appId;

    public VeBaseSysRoleMenu setMenuId(Integer menuId)
    {
        this.menuId = menuId;return this;
    }

    public VeBaseSysRoleMenu setRoleId(Integer roleId)
    {
        this.roleId = roleId;return this;
    }

    public VeBaseSysRoleMenu setId(Integer id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseSysRoleMenu(id=" + getId() + ", roleId=" + getRoleId() + ", menuId=" + getMenuId() + ", terminalid=" + getTerminalid() + ", appId=" + getAppId() + ")";
    }

    public VeBaseSysRoleMenu setAppId(String appId)
    {
        this.appId = appId;return this;
    }

    public VeBaseSysRoleMenu setTerminalid(Integer terminalid)
    {
        this.terminalid = terminalid;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $roleId = getRoleId();result = result * 59 + ($roleId == null ? 43 : $roleId.hashCode());Object $menuId = getMenuId();result = result * 59 + ($menuId == null ? 43 : $menuId.hashCode());Object $terminalid = getTerminalid();result = result * 59 + ($terminalid == null ? 43 : $terminalid.hashCode());Object $appId = getAppId();result = result * 59 + ($appId == null ? 43 : $appId.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseSysRoleMenu;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseSysRoleMenu)) {
            return false;
        }
        VeBaseSysRoleMenu other = (VeBaseSysRoleMenu)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$roleId = getRoleId();Object other$roleId = other.getRoleId();
        if (this$roleId == null ? other$roleId != null : !this$roleId.equals(other$roleId)) {
            return false;
        }
        Object this$menuId = getMenuId();Object other$menuId = other.getMenuId();
        if (this$menuId == null ? other$menuId != null : !this$menuId.equals(other$menuId)) {
            return false;
        }
        Object this$terminalid = getTerminalid();Object other$terminalid = other.getTerminalid();
        if (this$terminalid == null ? other$terminalid != null : !this$terminalid.equals(other$terminalid)) {
            return false;
        }
        Object this$appId = getAppId();Object other$appId = other.getAppId();return this$appId == null ? other$appId == null : this$appId.equals(other$appId);
    }

    public Integer getId()
    {
        return this.id;
    }

    public Integer getRoleId()
    {
        return this.roleId;
    }

    public Integer getMenuId()
    {
        return this.menuId;
    }

    public Integer getTerminalid()
    {
        return this.terminalid;
    }

    public String getAppId()
    {
        return this.appId;
    }
}
