package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.edu.modules.base.entity.VeBaseSysRole;

public abstract interface IVeBaseSysRoleService
        extends IService<VeBaseSysRole>
{
    public abstract VeBaseSysRole getModelByRoleText(String paramString);

    public abstract VeBaseSysRole getModelByIdAndText(Integer paramInteger, String paramString);

    public abstract void deleteSysRoleUserById(String paramString);

    public abstract void addSysRoleUserById(String paramString, String[] paramArrayOfString);

    public abstract int addSysRole(VeBaseSysRole paramVeBaseSysRole);
}
