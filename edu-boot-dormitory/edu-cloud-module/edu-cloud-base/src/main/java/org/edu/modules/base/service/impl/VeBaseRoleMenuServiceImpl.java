package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.base.entity.VeBaseRoleMenu;
import org.edu.modules.base.mapper.VeBaseRoleMenuMapper;
import org.edu.modules.base.service.IVeBaseRoleMenuService;
import org.springframework.stereotype.Service;

@Service
public class VeBaseRoleMenuServiceImpl
        extends ServiceImpl<VeBaseRoleMenuMapper, VeBaseRoleMenu>
        implements IVeBaseRoleMenuService
{}
