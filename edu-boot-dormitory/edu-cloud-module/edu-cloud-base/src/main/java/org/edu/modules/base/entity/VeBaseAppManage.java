package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_app_manage")
@ApiModel(value="ve_base_app_manage对象", description="应用信息")
public class VeBaseAppManage
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="应用名称", width=15.0D)
    @ApiModelProperty("应用名称")
    private String appName;
    @Excel(name="状态", width=15.0D)
    @ApiModelProperty("状态|0:启用,1:禁用")
    private String isEnable;
    @Excel(name="描述", width=15.0D)
    @ApiModelProperty("描述")
    private String description;
    @Excel(name="应用地址", width=15.0D)
    @ApiModelProperty("应用地址")
    private String appAddress;
    @Excel(name="秘钥", width=15.0D)
    @ApiModelProperty("秘钥")
    private String auth;
    @Excel(name="认证方式", width=15.0D)
    @ApiModelProperty("认证方式：1-cas，2-oauth2，3-ldap\023")
    private String authmethod;
    @TableField(exist=false)
    private Integer startIndex;
    @TableField(exist=false)
    private Integer pageSize;
    @TableField(exist=false)
    private String groupId;

    public VeBaseAppManage setIsEnable(String isEnable)
    {
        this.isEnable = isEnable;return this;
    }

    public VeBaseAppManage setAppName(String appName)
    {
        this.appName = appName;return this;
    }

    public VeBaseAppManage setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseAppManage(id=" + getId() + ", appName=" + getAppName() + ", isEnable=" + getIsEnable() + ", description=" + getDescription() + ", appAddress=" + getAppAddress() + ", auth=" + getAuth() + ", authmethod=" + getAuthmethod() + ", startIndex=" + getStartIndex() + ", pageSize=" + getPageSize() + ", groupId=" + getGroupId() + ")";
    }

    public VeBaseAppManage setGroupId(String groupId)
    {
        this.groupId = groupId;return this;
    }

    public VeBaseAppManage setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;return this;
    }

    public VeBaseAppManage setStartIndex(Integer startIndex)
    {
        this.startIndex = startIndex;return this;
    }

    public VeBaseAppManage setAuthmethod(String authmethod)
    {
        this.authmethod = authmethod;return this;
    }

    public VeBaseAppManage setAuth(String auth)
    {
        this.auth = auth;return this;
    }

    public VeBaseAppManage setAppAddress(String appAddress)
    {
        this.appAddress = appAddress;return this;
    }

    public VeBaseAppManage setDescription(String description)
    {
        this.description = description;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $startIndex = getStartIndex();result = result * 59 + ($startIndex == null ? 43 : $startIndex.hashCode());Object $pageSize = getPageSize();result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $appName = getAppName();result = result * 59 + ($appName == null ? 43 : $appName.hashCode());Object $isEnable = getIsEnable();result = result * 59 + ($isEnable == null ? 43 : $isEnable.hashCode());Object $description = getDescription();result = result * 59 + ($description == null ? 43 : $description.hashCode());Object $appAddress = getAppAddress();result = result * 59 + ($appAddress == null ? 43 : $appAddress.hashCode());Object $auth = getAuth();result = result * 59 + ($auth == null ? 43 : $auth.hashCode());Object $authmethod = getAuthmethod();result = result * 59 + ($authmethod == null ? 43 : $authmethod.hashCode());Object $groupId = getGroupId();result = result * 59 + ($groupId == null ? 43 : $groupId.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseAppManage;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseAppManage)) {
            return false;
        }
        VeBaseAppManage other = (VeBaseAppManage)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$startIndex = getStartIndex();Object other$startIndex = other.getStartIndex();
        if (this$startIndex == null ? other$startIndex != null : !this$startIndex.equals(other$startIndex)) {
            return false;
        }
        Object this$pageSize = getPageSize();Object other$pageSize = other.getPageSize();
        if (this$pageSize == null ? other$pageSize != null : !this$pageSize.equals(other$pageSize)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$appName = getAppName();Object other$appName = other.getAppName();
        if (this$appName == null ? other$appName != null : !this$appName.equals(other$appName)) {
            return false;
        }
        Object this$isEnable = getIsEnable();Object other$isEnable = other.getIsEnable();
        if (this$isEnable == null ? other$isEnable != null : !this$isEnable.equals(other$isEnable)) {
            return false;
        }
        Object this$description = getDescription();Object other$description = other.getDescription();
        if (this$description == null ? other$description != null : !this$description.equals(other$description)) {
            return false;
        }
        Object this$appAddress = getAppAddress();Object other$appAddress = other.getAppAddress();
        if (this$appAddress == null ? other$appAddress != null : !this$appAddress.equals(other$appAddress)) {
            return false;
        }
        Object this$auth = getAuth();Object other$auth = other.getAuth();
        if (this$auth == null ? other$auth != null : !this$auth.equals(other$auth)) {
            return false;
        }
        Object this$authmethod = getAuthmethod();Object other$authmethod = other.getAuthmethod();
        if (this$authmethod == null ? other$authmethod != null : !this$authmethod.equals(other$authmethod)) {
            return false;
        }
        Object this$groupId = getGroupId();Object other$groupId = other.getGroupId();return this$groupId == null ? other$groupId == null : this$groupId.equals(other$groupId);
    }

    public String getId()
    {
        return this.id;
    }

    public String getAppName()
    {
        return this.appName;
    }

    public String getIsEnable()
    {
        return this.isEnable;
    }

    public String getDescription()
    {
        return this.description;
    }

    public String getAppAddress()
    {
        return this.appAddress;
    }

    public String getAuth()
    {
        return this.auth;
    }

    public String getAuthmethod()
    {
        return this.authmethod;
    }

    public Integer getStartIndex()
    {
        return this.startIndex;
    }

    public Integer getPageSize()
    {
        return this.pageSize;
    }

    public String getGroupId()
    {
        return this.groupId;
    }
}
