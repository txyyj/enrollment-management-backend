package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.edu.modules.base.entity.VeDictionary;
import org.edu.modules.base.mapper.VeDictionaryMapper;
import org.edu.modules.base.service.IVeDictionaryService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: 陈炜凡
 * @Description: TODO
 * @DateTime: 2021/12/13 17:00
 */
@Service
public class VeDictionaryServiceImpl extends ServiceImpl<VeDictionaryMapper, VeDictionary> implements IVeDictionaryService {
    @Override
    public List<VeDictionary> selectAll(VeDictionary paramVeDictionary, Integer paramInteger1, Integer paramInteger2) {
        return baseMapper.selectAll(paramVeDictionary, paramInteger1, paramInteger2);
    }

    @Override
    public List<VeDictionary> getTreeList() {
        return baseMapper.getBodyList();
    }

    @Override
    public void saves(VeDictionary paramVeDictionary) {
        baseMapper.saves(paramVeDictionary);
    }

    @Override
    public int deleteDictionary(String paramString) {
        return baseMapper.deleteDictionary(paramString);
    }

    @Override
    public int deleteDictData(String paramString) {
        return baseMapper.deleteDictData(paramString);
    }
}
