package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_user_log")
@ApiModel(value="ve_base_user_log对象", description="系统日志信息")
public class VeBaseUserLog
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="操作人工号", width=15.0D)
    @ApiModelProperty("操作人工号")
    private String userid;
    @Excel(name="操作人姓名", width=15.0D)
    @ApiModelProperty("操作人姓名")
    private String realname;
    @Excel(name="执行动作 (URL路径)", width=15.0D)
    @ApiModelProperty("执行动作 (URL路径)")
    private String action;
    @Excel(name="ip", width=15.0D)
    @ApiModelProperty("ip")
    private String ip;
    @Excel(name="浏览器版本", width=15.0D)
    @ApiModelProperty("浏览器版本")
    private String browser;
    @Excel(name="操作时间", width=15.0D)
    @ApiModelProperty("操作时间")
    private String opttime;
    @Excel(name="执行结果", width=15.0D)
    @ApiModelProperty("执行结果( y成功 ,n失败)")
    private String result;
    @Excel(name="服务器mac地址", width=15.0D)
    @ApiModelProperty("服务器mac地址")
    private String mac;
    @TableField(exist=false)
    private String beginDate;
    @TableField(exist=false)
    private String endDate;
    @TableField(exist=false)
    private Integer startIndex;
    @TableField(exist=false)
    private Integer pageSize;

    public VeBaseUserLog setRealname(String realname)
    {
        this.realname = realname;return this;
    }

    public VeBaseUserLog setUserid(String userid)
    {
        this.userid = userid;return this;
    }

    public VeBaseUserLog setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseUserLog(id=" + getId() + ", userid=" + getUserid() + ", realname=" + getRealname() + ", action=" + getAction() + ", ip=" + getIp() + ", browser=" + getBrowser() + ", opttime=" + getOpttime() + ", result=" + getResult() + ", mac=" + getMac() + ", beginDate=" + getBeginDate() + ", endDate=" + getEndDate() + ", startIndex=" + getStartIndex() + ", pageSize=" + getPageSize() + ")";
    }

    public VeBaseUserLog setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;return this;
    }

    public VeBaseUserLog setStartIndex(Integer startIndex)
    {
        this.startIndex = startIndex;return this;
    }

    public VeBaseUserLog setEndDate(String endDate)
    {
        this.endDate = endDate;return this;
    }

    public VeBaseUserLog setBeginDate(String beginDate)
    {
        this.beginDate = beginDate;return this;
    }

    public VeBaseUserLog setMac(String mac)
    {
        this.mac = mac;return this;
    }

    public VeBaseUserLog setResult(String result)
    {
        this.result = result;return this;
    }

    public VeBaseUserLog setOpttime(String opttime)
    {
        this.opttime = opttime;return this;
    }

    public VeBaseUserLog setBrowser(String browser)
    {
        this.browser = browser;return this;
    }

    public VeBaseUserLog setIp(String ip)
    {
        this.ip = ip;return this;
    }

    public VeBaseUserLog setAction(String action)
    {
        this.action = action;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $startIndex = getStartIndex();result = result * 59 + ($startIndex == null ? 43 : $startIndex.hashCode());Object $pageSize = getPageSize();result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $userid = getUserid();result = result * 59 + ($userid == null ? 43 : $userid.hashCode());Object $realname = getRealname();result = result * 59 + ($realname == null ? 43 : $realname.hashCode());Object $action = getAction();result = result * 59 + ($action == null ? 43 : $action.hashCode());Object $ip = getIp();result = result * 59 + ($ip == null ? 43 : $ip.hashCode());Object $browser = getBrowser();result = result * 59 + ($browser == null ? 43 : $browser.hashCode());Object $opttime = getOpttime();result = result * 59 + ($opttime == null ? 43 : $opttime.hashCode());Object $result = getResult();result = result * 59 + ($result == null ? 43 : $result.hashCode());Object $mac = getMac();result = result * 59 + ($mac == null ? 43 : $mac.hashCode());Object $beginDate = getBeginDate();result = result * 59 + ($beginDate == null ? 43 : $beginDate.hashCode());Object $endDate = getEndDate();result = result * 59 + ($endDate == null ? 43 : $endDate.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseUserLog;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseUserLog)) {
            return false;
        }
        VeBaseUserLog other = (VeBaseUserLog)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$startIndex = getStartIndex();Object other$startIndex = other.getStartIndex();
        if (this$startIndex == null ? other$startIndex != null : !this$startIndex.equals(other$startIndex)) {
            return false;
        }
        Object this$pageSize = getPageSize();Object other$pageSize = other.getPageSize();
        if (this$pageSize == null ? other$pageSize != null : !this$pageSize.equals(other$pageSize)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$userid = getUserid();Object other$userid = other.getUserid();
        if (this$userid == null ? other$userid != null : !this$userid.equals(other$userid)) {
            return false;
        }
        Object this$realname = getRealname();Object other$realname = other.getRealname();
        if (this$realname == null ? other$realname != null : !this$realname.equals(other$realname)) {
            return false;
        }
        Object this$action = getAction();Object other$action = other.getAction();
        if (this$action == null ? other$action != null : !this$action.equals(other$action)) {
            return false;
        }
        Object this$ip = getIp();Object other$ip = other.getIp();
        if (this$ip == null ? other$ip != null : !this$ip.equals(other$ip)) {
            return false;
        }
        Object this$browser = getBrowser();Object other$browser = other.getBrowser();
        if (this$browser == null ? other$browser != null : !this$browser.equals(other$browser)) {
            return false;
        }
        Object this$opttime = getOpttime();Object other$opttime = other.getOpttime();
        if (this$opttime == null ? other$opttime != null : !this$opttime.equals(other$opttime)) {
            return false;
        }
        Object this$result = getResult();Object other$result = other.getResult();
        if (this$result == null ? other$result != null : !this$result.equals(other$result)) {
            return false;
        }
        Object this$mac = getMac();Object other$mac = other.getMac();
        if (this$mac == null ? other$mac != null : !this$mac.equals(other$mac)) {
            return false;
        }
        Object this$beginDate = getBeginDate();Object other$beginDate = other.getBeginDate();
        if (this$beginDate == null ? other$beginDate != null : !this$beginDate.equals(other$beginDate)) {
            return false;
        }
        Object this$endDate = getEndDate();Object other$endDate = other.getEndDate();return this$endDate == null ? other$endDate == null : this$endDate.equals(other$endDate);
    }

    public String getId()
    {
        return this.id;
    }

    public String getUserid()
    {
        return this.userid;
    }

    public String getRealname()
    {
        return this.realname;
    }

    public String getAction()
    {
        return this.action;
    }

    public String getIp()
    {
        return this.ip;
    }

    public String getBrowser()
    {
        return this.browser;
    }

    public String getOpttime()
    {
        return this.opttime;
    }

    public String getResult()
    {
        return this.result;
    }

    public String getMac()
    {
        return this.mac;
    }

    public String getBeginDate()
    {
        return this.beginDate;
    }

    public String getEndDate()
    {
        return this.endDate;
    }

    public Integer getStartIndex()
    {
        return this.startIndex;
    }

    public Integer getPageSize()
    {
        return this.pageSize;
    }
}
