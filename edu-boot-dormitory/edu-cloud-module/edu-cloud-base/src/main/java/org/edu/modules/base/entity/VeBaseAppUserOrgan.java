package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.List;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_app_user_organ")
@ApiModel(value="ve_base_app_user_organ对象", description="应用用户机构信息")
public class VeBaseAppUserOrgan
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    @Excel(name="部门编号", width=15.0D)
    private String id;
    @Excel(name="部门名称", width=15.0D)
    @ApiModelProperty("部门名称")
    private String organName;
    @Excel(name="上级部门编号", width=15.0D)
    @ApiModelProperty("父部门编号")
    private String pid;
    @Excel(name="是否可用", width=15.0D)
    @ApiModelProperty("是否可用")
    private String status;
    @TableField(exist=false)
    private List<VeBaseAppUserOrgan> list;

    public VeBaseAppUserOrgan setPid(String pid)
    {
        this.pid = pid;return this;
    }

    public VeBaseAppUserOrgan setOrganName(String organName)
    {
        this.organName = organName;return this;
    }

    public VeBaseAppUserOrgan setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseAppUserOrgan(id=" + getId() + ", organName=" + getOrganName() + ", pid=" + getPid() + ", status=" + getStatus() + ", list=" + getList() + ")";
    }

    public VeBaseAppUserOrgan setList(List<VeBaseAppUserOrgan> list)
    {
        this.list = list;return this;
    }

    public VeBaseAppUserOrgan setStatus(String status)
    {
        this.status = status;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $organName = getOrganName();result = result * 59 + ($organName == null ? 43 : $organName.hashCode());Object $pid = getPid();result = result * 59 + ($pid == null ? 43 : $pid.hashCode());Object $status = getStatus();result = result * 59 + ($status == null ? 43 : $status.hashCode());Object $list = getList();result = result * 59 + ($list == null ? 43 : $list.hashCode());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseAppUserOrgan;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseAppUserOrgan)) {
            return false;
        }
        VeBaseAppUserOrgan other = (VeBaseAppUserOrgan)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$organName = getOrganName();Object other$organName = other.getOrganName();
        if (this$organName == null ? other$organName != null : !this$organName.equals(other$organName)) {
            return false;
        }
        Object this$pid = getPid();Object other$pid = other.getPid();
        if (this$pid == null ? other$pid != null : !this$pid.equals(other$pid)) {
            return false;
        }
        Object this$status = getStatus();Object other$status = other.getStatus();
        if (this$status == null ? other$status != null : !this$status.equals(other$status)) {
            return false;
        }
        Object this$list = getList();Object other$list = other.getList();return this$list == null ? other$list == null : this$list.equals(other$list);
    }

    public String getId()
    {
        return this.id;
    }

    public String getOrganName()
    {
        return this.organName;
    }

    public String getPid()
    {
        return this.pid;
    }

    public String getStatus()
    {
        return this.status;
    }

    public List<VeBaseAppUserOrgan> getList()
    {
        return this.list;
    }
}
