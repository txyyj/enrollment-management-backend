package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseServiceType;
import org.edu.modules.base.mapper.VeBaseServiceTypeMapper;
import org.edu.modules.base.service.IVeBaseServiceTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseServiceTypeServiceImpl
        extends ServiceImpl<VeBaseServiceTypeMapper, VeBaseServiceType>
        implements IVeBaseServiceTypeService
{
    @Autowired
    private VeBaseServiceTypeMapper veBaseServiceTypeMapper;

    public int serviceTypeAllList(String name)
    {
        return this.veBaseServiceTypeMapper.serviceTypeAllList(name);
    }

    public List<Map<String, Object>> serviceTypePageList(String name, Integer startIndex, Integer pageSize)
    {
        return this.veBaseServiceTypeMapper.serviceTypePageList(name, startIndex, pageSize);
    }

    public VeBaseServiceType getServiceTypeByName(String id, String name)
    {
        return this.veBaseServiceTypeMapper.getServiceTypeByName(id, name);
    }
}
