package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;
import java.util.Arrays;
import org.jeecgframework.poi.excel.annotation.Excel;

@TableName("ve_base_ip_strategy")
@ApiModel(value="ve_base_ip_strategy对象", description="IP策略表")
public class VeBaseIpSrategy
        implements Serializable
{
    @TableId(type=IdType.ASSIGN_ID)
    @ApiModelProperty("id")
    private String id;
    @Excel(name="ip起点", width=15.0D)
    @ApiModelProperty("ip起点")
    private String ipStart;
    @Excel(name="ip终点", width=15.0D)
    @ApiModelProperty("ip终点")
    private String ipEnd;
    @Excel(name="是否启用", width=15.0D)
    @ApiModelProperty("是否启用|0:启用,1:禁用")
    private String isEnable;
    @TableField(exist=false)
    private Integer startIndex;
    @TableField(exist=false)
    private Integer pageSize;
    @TableField(exist=false)
    private String[] arrays;

    public VeBaseIpSrategy setIpEnd(String ipEnd)
    {
        this.ipEnd = ipEnd;return this;
    }

    public VeBaseIpSrategy setIpStart(String ipStart)
    {
        this.ipStart = ipStart;return this;
    }

    public VeBaseIpSrategy setId(String id)
    {
        this.id = id;return this;
    }

    public String toString()
    {
        return "VeBaseIpSrategy(id=" + getId() + ", ipStart=" + getIpStart() + ", ipEnd=" + getIpEnd() + ", isEnable=" + getIsEnable() + ", startIndex=" + getStartIndex() + ", pageSize=" + getPageSize() + ", arrays=" + Arrays.deepToString(getArrays()) + ")";
    }

    public VeBaseIpSrategy setArrays(String[] arrays)
    {
        this.arrays = arrays;return this;
    }

    public VeBaseIpSrategy setPageSize(Integer pageSize)
    {
        this.pageSize = pageSize;return this;
    }

    public VeBaseIpSrategy setStartIndex(Integer startIndex)
    {
        this.startIndex = startIndex;return this;
    }

    public VeBaseIpSrategy setIsEnable(String isEnable)
    {
        this.isEnable = isEnable;return this;
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $startIndex = getStartIndex();result = result * 59 + ($startIndex == null ? 43 : $startIndex.hashCode());Object $pageSize = getPageSize();result = result * 59 + ($pageSize == null ? 43 : $pageSize.hashCode());Object $id = getId();result = result * 59 + ($id == null ? 43 : $id.hashCode());Object $ipStart = getIpStart();result = result * 59 + ($ipStart == null ? 43 : $ipStart.hashCode());Object $ipEnd = getIpEnd();result = result * 59 + ($ipEnd == null ? 43 : $ipEnd.hashCode());Object $isEnable = getIsEnable();result = result * 59 + ($isEnable == null ? 43 : $isEnable.hashCode());result = result * 59 + Arrays.deepHashCode(getArrays());return result;
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseIpSrategy;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseIpSrategy)) {
            return false;
        }
        VeBaseIpSrategy other = (VeBaseIpSrategy)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$startIndex = getStartIndex();Object other$startIndex = other.getStartIndex();
        if (this$startIndex == null ? other$startIndex != null : !this$startIndex.equals(other$startIndex)) {
            return false;
        }
        Object this$pageSize = getPageSize();Object other$pageSize = other.getPageSize();
        if (this$pageSize == null ? other$pageSize != null : !this$pageSize.equals(other$pageSize)) {
            return false;
        }
        Object this$id = getId();Object other$id = other.getId();
        if (this$id == null ? other$id != null : !this$id.equals(other$id)) {
            return false;
        }
        Object this$ipStart = getIpStart();Object other$ipStart = other.getIpStart();
        if (this$ipStart == null ? other$ipStart != null : !this$ipStart.equals(other$ipStart)) {
            return false;
        }
        Object this$ipEnd = getIpEnd();Object other$ipEnd = other.getIpEnd();
        if (this$ipEnd == null ? other$ipEnd != null : !this$ipEnd.equals(other$ipEnd)) {
            return false;
        }
        Object this$isEnable = getIsEnable();Object other$isEnable = other.getIsEnable();
        if (this$isEnable == null ? other$isEnable != null : !this$isEnable.equals(other$isEnable)) {
            return false;
        }
        return Arrays.deepEquals(getArrays(), other.getArrays());
    }

    public String getId()
    {
        return this.id;
    }

    public String getIpStart()
    {
        return this.ipStart;
    }

    public String getIpEnd()
    {
        return this.ipEnd;
    }

    public String getIsEnable()
    {
        return this.isEnable;
    }

    public Integer getStartIndex()
    {
        return this.startIndex;
    }

    public Integer getPageSize()
    {
        return this.pageSize;
    }

    public String[] getArrays()
    {
        return this.arrays;
    }
}
