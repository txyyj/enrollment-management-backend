package org.edu.modules.base.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseService;

public abstract interface VeBaseServiceMapper
        extends BaseMapper<VeBaseService>
{
    public abstract int serviceModelAllList(VeBaseService paramVeBaseService);

    public abstract List<Map<String, Object>> serviceModelPageList(VeBaseService paramVeBaseService);

    public abstract int serviceGroupAppManageAllList(VeBaseService paramVeBaseService);

    public abstract List<Map<String, Object>> serviceGroupAppManagePageList(VeBaseService paramVeBaseService);

    public abstract int deleteServiceGroupAppManage(String paramString);

    public abstract int serviceGroupAppManageAdd(String paramString, String[] paramArrayOfString);

    public abstract VeBaseService getServiceByName(String paramString1, String paramString2);
}
