package org.edu.modules.base.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.io.Serializable;

@TableName("ve_base_role_menu")
@ApiModel(value="ve_base_role_menu对象", description="角色的所属菜单")
public class VeBaseRoleMenu
        implements Serializable
{
    @ApiModelProperty("id")
    private String roleid;
    @ApiModelProperty("id")
    private String menuid;

    public VeBaseRoleMenu setMenuid(String menuid)
    {
        this.menuid = menuid;return this;
    }

    public VeBaseRoleMenu setRoleid(String roleid)
    {
        this.roleid = roleid;return this;
    }

    public String toString()
    {
        return "VeBaseRoleMenu(roleid=" + getRoleid() + ", menuid=" + getMenuid() + ")";
    }

    public int hashCode()
    {
        int PRIME = 59;int result = 1;Object $roleid = getRoleid();result = result * 59 + ($roleid == null ? 43 : $roleid.hashCode());Object $menuid = getMenuid();result = result * 59 + ($menuid == null ? 43 : $menuid.hashCode());return result;
    }

    public boolean equals(Object o)
    {
        if (o == this) {
            return true;
        }
        if (!(o instanceof VeBaseRoleMenu)) {
            return false;
        }
        VeBaseRoleMenu other = (VeBaseRoleMenu)o;
        if (!other.canEqual(this)) {
            return false;
        }
        Object this$roleid = getRoleid();Object other$roleid = other.getRoleid();
        if (this$roleid == null ? other$roleid != null : !this$roleid.equals(other$roleid)) {
            return false;
        }
        Object this$menuid = getMenuid();Object other$menuid = other.getMenuid();return this$menuid == null ? other$menuid == null : this$menuid.equals(other$menuid);
    }

    protected boolean canEqual(Object other)
    {
        return other instanceof VeBaseRoleMenu;
    }

    public String getRoleid()
    {
        return this.roleid;
    }

    public String getMenuid()
    {
        return this.menuid;
    }
}
