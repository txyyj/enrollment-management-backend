package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseAppManage;
import org.edu.modules.base.mapper.VeBaseAppManageMapper;
import org.edu.modules.base.service.IVeBaseAppManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseAppManageServiceImpl
        extends ServiceImpl<VeBaseAppManageMapper, VeBaseAppManage>
        implements IVeBaseAppManageService
{
    @Autowired
    private VeBaseAppManageMapper veBaseAppManageMapper;

    public int getAppManageAllListNum(String appName, String isEnable)
    {
        return this.veBaseAppManageMapper.getAppManageAllListNum(appName, isEnable);
    }

    public List<VeBaseAppManage> getAppManageAllList(String appName, String isEnable)
    {
        return this.veBaseAppManageMapper.getAppManageAllList(appName, isEnable);
    }

    public List<VeBaseAppManage> getAppManagePageList(String appName, String isEnable, Integer startIndex, Integer pageSize)
    {
        return this.veBaseAppManageMapper.getAppManagePageList(appName, isEnable, startIndex, pageSize);
    }

    public List<Map<String, Object>> serviceGroupAppManageAllList(VeBaseAppManage veBaseAppManage)
    {
        return this.veBaseAppManageMapper.serviceGroupAppManageAllList(veBaseAppManage);
    }

    public List<Map<String, Object>> serviceGroupAppManagePageList(VeBaseAppManage veBaseAppManage)
    {
        return this.veBaseAppManageMapper.serviceGroupAppManagePageList(veBaseAppManage);
    }

    public int deleteServiceGroupAppManage(String groupId)
    {
        return this.veBaseAppManageMapper.deleteServiceGroupAppManage(groupId);
    }

    public int serviceGroupAppManageAdd(String groupId, String[] appId)
    {
        return this.veBaseAppManageMapper.serviceGroupAppManageAdd(groupId, appId);
    }

    public VeBaseAppManage getAppManageByName(String id, String appName)
    {
        return this.veBaseAppManageMapper.getAppManageByName(id, appName);
    }
}
