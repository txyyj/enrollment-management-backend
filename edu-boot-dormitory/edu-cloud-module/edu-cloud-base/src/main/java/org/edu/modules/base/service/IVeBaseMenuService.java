package org.edu.modules.base.service;

import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import org.edu.modules.base.entity.VeBaseMenu;

public abstract interface IVeBaseMenuService
        extends IService<VeBaseMenu>
{
    public abstract List<VeBaseMenu> getTreeList();
}
