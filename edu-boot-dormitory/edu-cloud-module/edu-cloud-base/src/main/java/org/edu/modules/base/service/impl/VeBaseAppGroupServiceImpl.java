package org.edu.modules.base.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import java.util.List;
import java.util.Map;
import org.edu.modules.base.entity.VeBaseAppGroup;
import org.edu.modules.base.mapper.VeBaseAppGroupMapper;
import org.edu.modules.base.service.IVeBaseAppGroupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class VeBaseAppGroupServiceImpl
        extends ServiceImpl<VeBaseAppGroupMapper, VeBaseAppGroup>
        implements IVeBaseAppGroupService
{
    @Autowired
    private VeBaseAppGroupMapper veBaseAppGroupMapper;

    public int getAppGroupAllList(String roleName)
    {
        return this.veBaseAppGroupMapper.getAppGroupAllList(roleName);
    }

    public List<VeBaseAppGroup> getAppGroupPageList(String roleName, Integer startIndex, Integer pageSize)
    {
        List<VeBaseAppGroup> veBaseAppGroups = this.veBaseAppGroupMapper.getAppGroupPageList(roleName, startIndex, pageSize);
        if (veBaseAppGroups.size() > 0) {
            for (VeBaseAppGroup v : veBaseAppGroups)
            {
                List<Map<String, Object>> list = this.veBaseAppGroupMapper.getAppManageList(v.getId());
                v.setMapList(list);
            }
        }
        return veBaseAppGroups;
    }

    public VeBaseAppGroup getAppGroupByName(String id, String roleName)
    {
        return this.veBaseAppGroupMapper.getAppGroupByName(id, roleName);
    }

    public int addAppGroupManageBatch(String groupId, String[] appId)
    {
        return this.veBaseAppGroupMapper.addAppGroupManageBatch(groupId, appId);
    }

    public int deleteAppGroupManageById(String groupId)
    {
        return this.veBaseAppGroupMapper.deleteAppGroupManageById(groupId);
    }
}
