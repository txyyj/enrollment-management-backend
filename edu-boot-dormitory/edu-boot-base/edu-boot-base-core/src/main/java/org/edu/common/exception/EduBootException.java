package org.edu.common.exception;

public class EduBootException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	public EduBootException(String message){
		super(message);
	}
	
	public EduBootException(Throwable cause)
	{
		super(cause);
	}
	
	public EduBootException(String message,Throwable cause)
	{
		super(message,cause);
	}
}
