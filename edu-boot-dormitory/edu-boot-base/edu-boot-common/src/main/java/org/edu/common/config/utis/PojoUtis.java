package org.edu.common.config.utis;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author:Jin_long
 * @Date:2021/1/20 11:27
 * @explain: object转换类型工具类
 */
public class PojoUtis {
    /**
     * 将Object转换成List
     * @param obj
     * @param clazz
     * @param <T>
     * @return
     */
    public static <T> List<T> castList(Object obj, Class<T> clazz)
    {
        List<T> result = new ArrayList<T>();
        if(obj instanceof List<?>)
        {
            for (Object o : (List<?>) obj)
            {
                result.add(clazz.cast(o));
            }
            return result;
        }
        return null;
    }
}
